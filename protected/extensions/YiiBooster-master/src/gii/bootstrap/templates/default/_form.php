<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<div class="content-fluid">
	<div class="row-fluid">
		
	
<?php
foreach ($this->tableSchema->columns as $column) {
	if ($column->autoIncrement) {
		continue;
	}
	?>
	<div class="span3">
	<?php echo "	<?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n"; ?>
	</div>
<?php
}
?>

	</div>
</div>
<div class="form-actions">
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>\$model->isNewRecord ? 'Create' : 'Save',
		)); ?>\n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
