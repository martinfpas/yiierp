/**
 * v 1.0
 * Codigo para los formularios autogenerados personalizados con Gii
 * 
 */

$(document).ready(function(){
	$('.SaveAsyncMas').click(function(){
		var idNuevo = $(this).attr('idNuevo');
		$('#'+idNuevo).toggle('slow');
		if($(this).val() === 'Agregar'){
	    	$(this).val('Cerrar');
		}else{
			$(this).val('Agregar');
		}	
	});
	
	$('.SaveAsyncForm').submit(function(e){
		e.preventDefault();
		var datos = $(this).serialize();
		console.log(datos);
		var respuesta ;
		var url = $(this).attr('action');
		var divError = $(this).find('.SaveAsyncError');
		var divOk = $(this).find('.SaveAsyncOk');
		var idGrid =  $(this).attr('idGrilla');
		var idMas = $(this).attr('idMas');
		var idNuevo = $(this).attr('idNuevo');
		
		var form = $(this);
		
		$(divError).hide('slow');
		$(divOk).hide('slow');
		
		
		$.ajax({
			url: url,
            type: 'Post',
            data: datos,
            success: function (html) {
               	$(divOk).show('slow');
               	$.fn.yiiGridView.update(idGrid);
				$('#'+idNuevo).hide('slow');
				console.log($('#'+idNuevo));
				$('#'+idMas).val('Agregar');
               	$(form)[0].reset();
				
			},
			error: function (x,y,z){
				// TODO: MANEJAR LOS ERRORES
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$(divError).html(htmlError);
				$(divOk).hide('slow');
				$(divError).show('slow');
			}
		});
		return false;
	});
});
