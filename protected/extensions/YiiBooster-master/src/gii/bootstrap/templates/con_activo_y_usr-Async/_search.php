<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl(\$this->route),
	'method'=>'get',
)); ?>\n"; ?>

<?php foreach ($this->tableSchema->columns as $column): ?>
	<?php
	$field = $this->generateInputField($this->modelClass, $column);
	if (strpos($field, 'password') !== false) {
		continue;
	}
	?>
	<?php 
		if($column->name == 'activo'){
			echo "<?php echo \$form->labelEx(\$model,'activo'); ?>\n";
			echo "<?php echo \$form->dropDownList(\$model,'activo', array('0' => ".$this->modelClass."::inactivo,'1' => ".$this->modelClass."::activo),	array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ?> ";
			//echo "<?php echo \$form->dropDownList(\$model,'activo', array('".$this->modelClass."::iInactivo' => ".$this->modelClass."::inactivo,'".$this->modelClass."::iActivo' => ".$this->modelClass."::activo),	array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ";	
		}else {
			echo  "<?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n";	
		}
		 
	
	?>

<?php endforeach; ?>
	<div class="form-actions">
		<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>\n"; ?>
	</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>