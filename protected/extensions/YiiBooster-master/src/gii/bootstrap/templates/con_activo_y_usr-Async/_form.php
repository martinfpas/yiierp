<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="error<?=$this->modelClass?>"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="ok<?=$this->modelClass?>">Datos Guardados Correctamente !</div>
	
<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<div class="content-fluid">
	<div class="row-fluid">

	<?php
	foreach ($this->tableSchema->columns as $column) {
		if ($column->autoIncrement) {
			continue;
		}
		?>
		<div class="span3">
		<?php 
			if($column->name != 'activo'){
				echo "	<?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n";
			}else {
				echo "	<?php 
				if(!\$model->isNewRecord){
				echo \$form->labelEx(\$model,'activo'); \n";
				echo "	echo \$form->dropDownList(\$model,'activo', array('0' => ".$this->modelClass."::inactivo,'1' => ".$this->modelClass."::activo),	array( 'class'=>'span3')); 
				}
				?> ";
			} 
		?>
		</div>
	<?php
	}
	?>
		
	</div>
</div>


<div class="form-actions">
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>\$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>\n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
