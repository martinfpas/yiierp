<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 *  v 1.2
 * Dinamica de los titulos
 * Mejora la ayuda de la grilla
 * Cambio en el tamaño del titulo
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php

echo "\$this->breadcrumbs=array(
	".$this->modelClass."::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);\n";
?>

$this->menu=array(
	array('label'=>'Nuevo '.<?php echo $this->modelClass; ?>::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?php echo '<?='.$this->modelClass."::model()->getAttributeLabel('models') ?>" ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo "<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>"; ?>

<div class="search-form" style="display:none">
<?php echo "<?php \$this->renderPartial('_search',array(
	'model'=>\$model,
)); ?>\n"; ?>
</div><!-- search-form -->

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if(++$count==7)
		echo "\t\t/*\n";
	echo "\t\t'".$column->name."',\n";
}
if($count>=7)
	echo "\t\t*/\n";
?>
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
