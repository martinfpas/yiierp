
<?php
/*
 *v 1.3
 * Correccion en error de filtro
 * Se mejora la ubicacion de los campos del formulario
 * */
echo 

'<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
';

?>
<input idNuevo="nuevo<?=$this->modelClass?>" id="mas<?=$this->modelClass?>" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevo<?=$this->modelClass?>" style="display: none;">

	<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'" . $this->class2id($this->modelClass) . "-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('" . $this->modelClass . "/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'" . $this->class2id($this->modelClass) . "-grid',
			'idMas' => 'mas".$this->modelClass."',
			'idDivNuevo' => 'nuevo".$this->modelClass."',
		),
	)); ?>\n"; ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="error<?=$this->modelClass?>"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="ok<?=$this->modelClass?>">Datos Guardados Correctamente !</div>
		
	<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
	<?php
    $i=0;
	foreach ($this->tableSchema->columns as $column) {
		$i++;
        if ($column->autoIncrement) {
            continue;
        }else{
            $i++;
        }
		?>
		<div class="span3">
		<?php 
			if($column->name != 'activo'){
				echo "	<?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n";
			}else {
				echo "	<?php 
				if(!\$model->isNewRecord){
					echo \$form->labelEx(\$model,'activo'); \n";
				echo "	echo \$form->dropDownList(\$model,'activo', array('0' => ".$this->modelClass."::inactivo,'1' => ".$this->modelClass."::activo),	array( 'class'=>'span3')); 
				}
				?> ";
			} 
		?>
		</div>
	<?php
if (($i % 4) == 0 && sizeof($this->tableSchema->columns > $i)){
    echo '
</div>
<div class="row-fluid">
';
}
	}
	?>

        <div class="span3">
            <div class="form-actions">
                <?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>\$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>\n"; ?>
            </div>
        </div>
    </div>
</div>

	<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
</div>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
'dataProvider'=>$a<?php echo $this->modelClass; ?>->search(),
'columns'=>array(
<?php
$count=0;
foreach($this->tableSchema->columns as $column)
{
	if($column->name == 'activo'){
		echo "
			array(
				'name' => 'activo',
				'value' => '".$this->modelClass."::\$aEstado[\$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => ".$this->modelClass."::\$aEstado,
			),
		";
	}else {
		if(++$count==7)
			echo "\t\t/*\n";
		echo "\t\t'".$column->name."',\n";		
	}

}
if($count>=7)
	echo "\t\t*/\n";
?>
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("<?php echo $this->modelClass; ?>/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("<?php echo $this->modelClass; ?>/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>