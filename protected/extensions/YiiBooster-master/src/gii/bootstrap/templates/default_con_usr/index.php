<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 *
 * V 1.2
 * Cambio en el tamaño del titulo
 */
?>

/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $dataProvider CActiveDataProvider */

<?php
echo "<?php\n";
$label = $this->modelClass."::model()->getAttributeLabel('models')";
echo "\$this->breadcrumbs=array(
	'$label',
);\n";
?>

$this->menu=array(
	array('label'=>'Nuevo <?php echo $this->modelClass; ?>', 'url'=>array('create')),
	array('label'=>'Gestion de <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>


<h3><?php echo '$label'; ?></h3>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>