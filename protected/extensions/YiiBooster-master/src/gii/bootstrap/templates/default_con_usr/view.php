<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 * v 1.2
 *
 * Cambio en el tamaño del titulo
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->modelClass."::model()->getAttributeLabel('models')";
$label2 = $this->modelClass."::model()->getAttributeLabel('model')";
echo "\$this->breadcrumbs=array(
	$label =>array('admin'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>'Nuevo '.<?=$label2?>, 'url'=>array('create')),
	array('label'=>'Modificar '.<?=$label2?>, 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Borrar '.<?=$label2?>, 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.<?=$label?>, 'url'=>array('admin')),
);
?>

<h3>Ver <?php echo "<?=".$this->modelClass."::model()->getAttributeLabel('model') ?> #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h3>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
<?php
foreach ($this->tableSchema->columns as $column) {
	echo "\t\t'" . $column->name . "',\n";
}
?>
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
