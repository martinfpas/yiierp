<?php
/* @
 * v 1.2
 * Dinamica de los titulos
 * Cambio en el tamaño del titulo
 * */
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->modelClass."::model()->getAttributeLabel('models')";
$label2 = $this->modelClass."::model()->getAttributeLabel('model')";
echo "\$this->breadcrumbs=array(
	$label => array('admin'),
	$label2 => array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Modificando',
);\n";
?>

$this->menu=array(
	array('label'=>'Nuevo '.<?=$label2?>, 'url'=>array('create')),
	array('label'=>'Ver '.<?=$label2?>, 'url'=>array('view', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Gestion de '.<?=$label?>, 'url'=>array('admin')),
);
?>

	<h3>Modificando <?php echo "<?=".$this->modelClass."::model()->getAttributeLabel('model') ?> <?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h3>

<?php echo "<?php echo \$this->renderPartial('_form',array('model'=>\$model)); ?>"; ?>