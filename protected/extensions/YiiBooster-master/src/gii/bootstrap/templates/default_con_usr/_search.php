<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 *  v 1.2
 *  Se mejora la ubicacion de los campos del formulario
 */
?>


    <?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl(\$this->route),
        'method'=>'get',
    )); ?>\n"; ?>
        <div class="content-fluid">
            <div class="row-fluid">
    <?php
    $i = 0;
    foreach ($this->tableSchema->columns as $column): ?>
        <?php
        $i++;
        $field = $this->generateInputField($this->modelClass, $column);
        if (strpos($field, 'password') !== false) {
            continue;
        }
        ?>
        <?php
            echo "<div class='span4'><?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n</div>";
            if (($i % 3) == 0 && sizeof($this->tableSchema->columns > $i)){
                echo '
                    </div>
                    <div class="row-fluid">';
            }
        ?>

    <?php endforeach; ?>
                <div class='span4'>
                    <div class="form-actions">
                        <?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>Yii::t('application','Buscar'),
                        )); ?>\n"; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php echo "<?php \$this->endWidget(); ?>\n"; ?>

