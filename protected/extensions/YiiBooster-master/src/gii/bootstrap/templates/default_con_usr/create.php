<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 *  v 1.2
 * Dinamica de los titulos
 * Cambio en el tamaño del titulo
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->modelClass."::model()->getAttributeLabel('models')";
echo "\$this->breadcrumbs=array(
	$label=>array('admin'),
	'Creando ',
);\n";
?>

$this->menu=array(
	array('label'=>'Gestion de '.<?php echo $this->modelClass; ?>::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?php echo "<?=".$this->modelClass."::model()->getAttributeLabel('model') ?>"?></h3>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
