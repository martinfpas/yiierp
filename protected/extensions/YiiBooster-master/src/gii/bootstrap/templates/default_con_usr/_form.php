<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 *  v 1.1
 *  Se mejora la ubicacion de los campos del formulario
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="error<?=$this->modelClass?>"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="ok<?=$this->modelClass?>">Datos Guardados Correctamente !</div>

<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<div class="content-fluid">
	<div class="row-fluid">

<?php
$i = 0;
foreach ($this->tableSchema->columns as $column) {

	if ($column->autoIncrement) {
		continue;
	}else{
        $i++;
    }
	?>
		<div class="span3">
		<?php echo "	<?php echo " . $this->generateActiveRow($this->modelClass, $column) . "; ?>\n"; ?>
		</div>
<?php
    if (($i % 4) == 0 && sizeof($this->tableSchema->columns > $i)){
        echo '
                    </div>
                    <div class="row-fluid">';
    }
}
?>
        <div class="span3">
            <div class="form-actions">
                <?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>\$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>\n"; ?>
            </div>
        </div>
    </div>
</div>


<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
