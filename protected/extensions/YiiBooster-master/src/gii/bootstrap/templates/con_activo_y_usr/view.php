<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$nameColumn = $this->guessNameColumn($this->tableSchema->columns);
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('admin'),
	\$model->{$nameColumn},
);\n";
?>

$this->menu=array(
	array('label'=>'Nuevo <?php echo $this->modelClass; ?>', 'url'=>array('create')),
	array('label'=>'Modificar <?php echo $this->modelClass; ?>', 'url'=>array('update', 'id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>)),
	array('label'=>'Borrar <?php echo $this->modelClass; ?>', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>

<h1>Ver <?php echo $this->modelClass . " #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?></h1>

<?php echo "<?php"; ?> $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
<?php
foreach ($this->tableSchema->columns as $column) {
		if($column->name == 'activo'){
		echo "
			array(
				'name' => 'activo',
				'header' => 'Estado', 
				'value' => ".$this->modelClass."::\$aEstado[\$model->activo],
			),
		";
	}else {
		echo "\t\t'" . $column->name . "',\n";
	}
}
?>
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
