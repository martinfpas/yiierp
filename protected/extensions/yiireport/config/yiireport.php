<?php
/**
 * Configuration file of PHPReport Yii extension.
 *
 * @author K'iin Balam <kbalam@upnfm.edu.hn>
 */

return array(
    'pdfRenderer' => /*'tcpdf',*/'mpdf',//or 'dompdf', 'tcpdf'
    // CAMBIA SEGUN EL SERVIDOR
    //'pdfRendererPath' => (preg_match('(conmacsrl.com.ar)',$_SERVER['SERVER_NAME'])!= false)? 'application.vendors.mpdf60' : 'application.vendors.MPDF57' ,
    'pdfRendererPath' => 'vendor.mpdf.mpdf.src',
    //'pdfRendererPath' => 'application.vendors.TCPDF'  ,
    //'pdfRendererPath' => 'application.vendors.tcpdf5'  ,
    'templatePath' => 'application.views.reports'
);
