-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-10-2019 a las 19:51:28
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequePropioMovBancario`
--

CREATE TABLE `chequePropioMovBancario` (
  `id` int(6) NOT NULL,
  `idChequePropio` int(7) NOT NULL,
  `idMovimiento` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `chequePropioMovBancario`
--
ALTER TABLE `chequePropioMovBancario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ChequePropio` (`idChequePropio`),
  ADD KEY `MovimientoBancario` (`idMovimiento`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `chequePropioMovBancario`
--
ALTER TABLE `chequePropioMovBancario`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `chequePropioMovBancario`
--
ALTER TABLE `chequePropioMovBancario`
  ADD CONSTRAINT `Cheque` FOREIGN KEY (`idChequePropio`) REFERENCES `chequePropio` (`id`),
  ADD CONSTRAINT `Movimiento` FOREIGN KEY (`idMovimiento`) REFERENCES `movimientoCuentasBanc` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
