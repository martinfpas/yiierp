CREATE TABLE IF NOT EXISTS `afipVoucherItem` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `idVoucher` int(5) NOT NULL,
  `codigo` varchar(14) DEFAULT NULL,
  `descripcion` varchar(40) NOT NULL,
  `cantidad` float NOT NULL DEFAULT '1',
  `codigoUnidadMedida` int(3) DEFAULT NULL,
  `precioUnitario` float NOT NULL,
  `impBonif` float DEFAULT NULL,
  `importeItem` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idVoucher` (`idVoucher`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `afipVoucherItem`
--
ALTER TABLE `afipVoucherItem`
  ADD CONSTRAINT `afipVoucherItem_ibfk_1` FOREIGN KEY (`idVoucher`) REFERENCES `afipVoucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
