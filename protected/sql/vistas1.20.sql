--
-- Estructura para la vista `vArtCotbyEc`
--
DROP TABLE IF EXISTS `vArtCotbyEc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyEc` AS select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `ec`.`codCot`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyPc`
--
DROP TABLE IF EXISTS `vArtCotbyPc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyPc` AS select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `pc`.`id`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vArticuloVenta`
--
DROP TABLE IF EXISTS `vArticuloVenta`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArticuloVenta` AS select `av`.`id_articulo_vta` AS `id_articulo_vta`,`av`.`descripcion` AS `descripcion`,`av`.`id_presentacion` AS `id_presentacion`,`a`.`id_articulo` AS `id_articulo`,`av`.`contenido` AS `contenido`,`av`.`uMedida` AS `uMedida`,`av`.`codBarra` AS `codBarra`,`av`.`precio` AS `precio`,`av`.`costo` AS `costo`,`av`.`enLista` AS `enLista`,`av`.`stockMinimo` AS `stockMinimo`,`a`.`id_rubro` AS `id_rubro` from (`articuloVenta` `av` left join `articulo` `a` on((`a`.`id` = `av`.`id_articulo`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `vCargaDocumentos`
--
DROP TABLE IF EXISTS `vCargaDocumentos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCargaDocumentos` AS select `cd`.`id` AS `id`,`cd`.`idEntregaDoc` AS `idEntregaDoc`,`cl`.`id` AS `idCliente`,`cl`.`razonSocial` AS `razonSocial`,`cp`.`nombrelocalidad` AS `nombrelocalidad`,`cl`.`cp` AS `codigoPostal`,`cl`.`zp` AS `zp` from ((((`cargaDocumentos` `cd` left join `comprobante` `fa` on(((`cd`.`idComprobante` = `fa`.`id`) and (`cd`.`tipoDoc` = 'FAC')))) left join `notaEntrega` `ne` on(((`cd`.`idComprobante` = `ne`.`id`) and (`cd`.`tipoDoc` = 'NE')))) left join `cliente` `cl` on(((`cl`.`id` = `fa`.`Id_Cliente`) or (`cl`.`id` = `ne`.`idCliente`)))) left join `codigoPostal` `cp` on(((`cl`.`cp` = `cp`.`codigoPostal`) and (`cl`.`zp` = `cp`.`zp`))));

-- --------------------------------------------------------

--
-- Estructura para la vista `vCargaDocumentosCiudades`
--
DROP TABLE IF EXISTS `vCargaDocumentosCiudades`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCargaDocumentosCiudades` AS select `cd`.`id` AS `id`,`cd`.`idEntregaDoc` AS `idEntregaDoc`,`cl`.`id` AS `idCliente`,`cl`.`razonSocial` AS `razonSocial`,`cp`.`nombrelocalidad` AS `nombrelocalidad`,`cl`.`cp` AS `codigoPostal`,`cl`.`zp` AS `zp`,`cd`.`orden` AS `orden` from ((((`cargaDocumentos` `cd` left join `comprobante` `fa` on(((`cd`.`idComprobante` = `fa`.`id`) and (`cd`.`tipoDoc` = 'FAC')))) left join `notaEntrega` `ne` on(((`cd`.`idComprobante` = `ne`.`id`) and (`cd`.`tipoDoc` = 'NE')))) left join `cliente` `cl` on(((`cl`.`id` = `fa`.`Id_Cliente`) or (`cl`.`id` = `ne`.`idCliente`)))) left join `codigoPostal` `cp` on(((`cl`.`cp` = `cp`.`codigoPostal`) and (`cl`.`zp` = `cp`.`zp`)))) group by `cp`.`codigoPostal`,`cl`.`zp` order by `cd`.`orden`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vCtaCteCliente`
--
DROP TABLE IF EXISTS `vCtaCteCliente`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCtaCteCliente`  AS  select uuid() AS `id`,'NE' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`NE`.`fecha` AS `fecha`,`NE`.`totalFinal` AS `debe`,NULL AS `haber`,`NE`.`id` AS `neId`,NULL AS `faId`,NULL AS `pcId`,NULL AS `ncId`,NULL AS `ndId` from (`notaEntrega` `NE` left join `cliente` `CL` on((`CL`.`id` = `NE`.`idCliente`))) union select uuid() AS `id`,`CO`.`tipo` AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`CO`.`fecha` AS `fecha`,if((`CO`.`tipo` <> 'NC'),`CO`.`Total_Final`,NULL) AS `debe`,if((`CO`.`tipo` = 'NC'),`CO`.`Total_Final`,NULL) AS `haber`,NULL AS `neId`,if((`CO`.`tipo` = 'F'),`CO`.`id`,NULL) AS `faId`,NULL AS `pcId`,if((`CO`.`tipo` = 'NC'),`CO`.`id`,NULL) AS `ncId`,if((`CO`.`tipo` = 'ND'),`CO`.`id`,NULL) AS `ndId` from (`comprobante` `CO` left join `cliente` `CL` on((`CL`.`id` = `CO`.`Id_Cliente`))) union select uuid() AS `id`,'PC' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`PC`.`fecha` AS `fecha`,NULL AS `debe`,`PC`.`monto` AS `haber`,NULL AS `neId`,NULL AS `faId`,`PC`.`id` AS `pcId`,NULL AS `ncId`,NULL AS `ndId` from (`pagoCliente` `PC` left join `cliente` `CL` on((`CL`.`id` = `PC`.`idCliente`))) order by `fecha` desc ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vDocumentosCarga`
--
DROP TABLE IF EXISTS `vDocumentosCarga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vDocumentosCarga` AS select uuid() AS `id`,`Fa`.`id` AS `idFa`,NULL AS `idNe`,`Fa`.`idNotaPedido` AS `idNotaPedido`,`Fa`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`comprobante` `Fa` left join `notaPedido` `Np` on((`Fa`.`idNotaPedido` = `Np`.`id`))) union select uuid() AS `id`,NULL AS `idFa`,`Ne`.`id` AS `idNe`,`Ne`.`idNotaPedido` AS `idNotaPedido`,`Ne`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`notaEntrega` `Ne` left join `notaPedido` `Np` on((`Ne`.`idNotaPedido` = `Np`.`id`))) order by `fecha` desc;

-- --------------------------------------------------------

--
-- Estructura para la vista `vDocumentosDeCarga`
--
DROP TABLE IF EXISTS `vDocumentosDeCarga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vDocumentosDeCarga` AS select uuid() AS `id`,`Fa`.`id` AS `idFa`,NULL AS `idNe`,`Fa`.`idNotaPedido` AS `idNotaPedido`,`Fa`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`comprobante` `Fa` left join `notaPedido` `Np` on((`Fa`.`idNotaPedido` = `Np`.`id`))) union select uuid() AS `id`,NULL AS `idFa`,`Ne`.`id` AS `idNe`,`Ne`.`idNotaPedido` AS `idNotaPedido`,`Ne`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`notaEntrega` `Ne` left join `notaPedido` `Np` on((`Ne`.`idNotaPedido` = `Np`.`id`))) order by `fecha` desc;

-- --------------------------------------------------------

--
-- Estructura para la vista `vPagosDeEntregaDoc`
--
DROP TABLE IF EXISTS `vPagosDeEntregaDoc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vPagosDeEntregaDoc` AS select `e`.`id` AS `idEntregaDoc`,`cd`.`id` AS `idCargaDoc`,`f`.`id` AS `idFactura`,`ne`.`id` AS `idNotaEntrega`,`pc`.`id` AS `idPago`,`pc`.`idCliente` AS `idCliente`,`pc`.`monto` AS `monto`,`pcch`.`idChequedeTercero` AS `idChequedeTercero` from (((((`entregaDoc` `e` left join `cargaDocumentos` `cd` on((`e`.`id` = `cd`.`idEntregaDoc`))) left join `comprobante` `f` on(((`cd`.`tipoDoc` = 'FAC') and (`cd`.`idComprobante` = `f`.`id`)))) left join `notaEntrega` `ne` on(((`cd`.`tipoDoc` = 'NE') and (`cd`.`idComprobante` = `ne`.`id`)))) left join `pagoCliente` `pc` on(((`pc`.`id_factura` = `f`.`id`) or (`pc`.`idNotaEntrega` = `ne`.`id`)))) left join `pagoConCheque` `pcch` on((`pcch`.`idPagodeCliente` = `pc`.`id`)));
