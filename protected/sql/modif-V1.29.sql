ALTER TABLE `cargaDocumentos` ADD `razonSocial` VARCHAR(40) NULL AFTER `idComprobante`;
ALTER TABLE `comprobante` ADD `idPagoRelacionado` INT(7) NULL AFTER `condicion`;
ALTER TABLE `notaEntrega` ADD `idPagoRelacionado` INT(7) NULL AFTER `impSinPrecio`;

####
ALTER TABLE `notaPedido` DROP FOREIGN KEY `notaPedido_ibfk_3`; ALTER TABLE `notaPedido` ADD CONSTRAINT `notaPedido_Carga` FOREIGN KEY (`idCarga`) REFERENCES `carga`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE `notaPedido` DROP FOREIGN KEY `notaPedido_ibfk_4`; ALTER TABLE `notaPedido` ADD CONSTRAINT `notaPedido_Cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `notaPedido` DROP FOREIGN KEY `notaPedido_ibfk_5`; ALTER TABLE `notaPedido` ADD CONSTRAINT `notaPedido_Camion` FOREIGN KEY (`camion`) REFERENCES `vehiculo`(`id_vehiculo`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `comprobante` ADD CONSTRAINT `comprobante_PagoRel` FOREIGN KEY (`idPagoRelacionado`) REFERENCES `pagoCliente`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;


#ALTER TABLE `comprobante`
#  ADD CONSTRAINT `comprobante_Camion` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`),
#  ADD CONSTRAINT `comprobante_NotaPedido` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido` (`id`),
#  ADD CONSTRAINT `comprobante_VoucherAfip` FOREIGN KEY (`idVoucherAfip`) REFERENCES `afipVoucher` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
#  ADD CONSTRAINT `comprobante_PuntoVenta` FOREIGN KEY (`Nro_Puesto_Venta`) REFERENCES `puntoDeVenta` (`num`),
#  ADD CONSTRAINT `comprobante_CategIva` FOREIGN KEY (`Iva_Responsable`) REFERENCES `categoriaIva` (`id_categoria`) ON UPDATE NO ACTION,
#  ADD CONSTRAINT `comprobante_Cliente` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`);