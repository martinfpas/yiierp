ALTER TABLE `notaEntrega` ADD `estado` INT(1) NOT NULL DEFAULT '0' AFTER `impSinPrecio`;
ALTER TABLE `pagoCliente` ADD `observacion` TEXT NULL AFTER `idCliente`;
ALTER TABLE `comprobante` ADD `condicion` INT(1) NOT NULL DEFAULT '1' AFTER `Total_Final`;
ALTER TABLE `comprobante` CHANGE `condicion` `condicion` INT(1) NOT NULL DEFAULT '1' COMMENT '1->Contado 2->Cuenta Corriente';
ALTER TABLE `chequeDeTerceros` ADD `fechaDeposito` DATE NULL AFTER `propio`, ADD `estadi` INT(1) NOT NULL DEFAULT '0' AFTER `fechaDeposito`, ADD `idProveedor` INT(4) NULL AFTER `estadi`;
ALTER TABLE `chequeDeTerceros` ADD `observacion` TEXT NULL AFTER `idProveedor`;
ALTER TABLE `chequeDeTerceros` CHANGE `estadi` `estado` INT(1) NOT NULL DEFAULT '0';
ALTER TABLE `chequeDeTerceros` ADD INDEX(`idProveedor`);
UPDATE `chequeDeTerceros` SET `fecha`= '2002-12-14' WHERE fecha < '2002-12-14';
ALTER TABLE `chequeDeTerceros` ADD CONSTRAINT `chequeDeTerceros_ibfk_3` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE `cliente` ADD `pieFactura` TEXT NULL AFTER `exeptuaIIB`;