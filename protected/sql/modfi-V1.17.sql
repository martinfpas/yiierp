RENAME TABLE  `erca`.`factura` TO  `erca`.`comprobante` ;
ALTER TABLE  `comprobante` CHANGE  `Fecha_Factura`  `fecha` DATE NULL DEFAULT NULL;
ALTER TABLE  `comprobante` MODIFY COLUMN `Total_Final` float AFTER `idVoucherAfip`;
ALTER TABLE  `comprobante` MODIFY COLUMN `Total_Neto` float AFTER `idVoucherAfip`;
ALTER TABLE  `comprobante` MODIFY COLUMN `Ing_Brutos` float AFTER `idVoucherAfip`;
ALTER TABLE  `comprobante` ADD  `alicuotaIva` FLOAT NOT NULL DEFAULT  '21' AFTER  `idVoucherAfip` ,
ADD  `subtotalIva` FLOAT NOT NULL DEFAULT  '0' AFTER  `alicuotaIva` ,
ADD  `alicuotaIIBB` FLOAT NULL DEFAULT  '0' AFTER  `subtotalIva` ,
ADD  `subtotal` FLOAT NOT NULL DEFAULT  '0' AFTER  `alicuotaIIBB`;
ALTER TABLE  `comprobante` ADD  `tipo` VARCHAR( 2 ) NOT NULL DEFAULT  'F' COMMENT  'F - NC - ND' AFTER  `id`;
ALTER TABLE  `comprobante` ADD  `razonSocial` VARCHAR( 50 ) NULL AFTER  `Id_Cliente` ,
ADD  `direccion` VARCHAR( 50 ) NULL AFTER  `razonSocial` ,
ADD  `localidad` VARCHAR( 65 ) NULL AFTER  `direccion` ,
ADD  `provincia` VARCHAR( 40 ) NULL AFTER  `localidad` ,
ADD  `rubro` VARCHAR( 35 ) NULL AFTER  `provincia` ,
ADD  `zona` VARCHAR( 4 ) NULL AFTER  `rubro` ,
ADD  `tipoResponsable` VARCHAR( 30 ) NULL AFTER  `zona` ,
ADD  `cuit` VARCHAR( 13 ) NULL AFTER  `tipoResponsable` ,
ADD  `perIIBB` BOOLEAN NOT NULL DEFAULT  '0' AFTER  `cuit` ,
ADD  `nroInscripIIBB` VARCHAR( 30 ) NULL AFTER  `perIIBB`;
RENAME TABLE  `erca`.`prodFactura` TO  `erca`.`prodComprobante` ;
ALTER TABLE  `prodComprobante` CHANGE  `idFactura`  `idComprobante` INT( 7 ) NOT NULL;
ALTER TABLE  `prodComprobante` ADD  `nroItem` INT( 3 ) NULL DEFAULT  '0' AFTER  `idComprobante`;
DELETE FROM `notaCredito` WHERE 1;

ALTER TABLE  `comprobante` CHANGE  `perIIBB`  `perIIBB` FLOAT NOT NULL DEFAULT  '0';
ALTER TABLE  `comprobante` DROP FOREIGN KEY  `comprobante_ibfk_12` ,
ADD FOREIGN KEY (  `idVoucherAfip` ) REFERENCES  `erca`.`afipVoucher` (
`id`
) ON DELETE SET NULL ON UPDATE SET NULL; 
ALTER TABLE  `afipSubtotivas` CHANGE  `codigo`  `codigo` INT( 2 ) NOT NULL DEFAULT  '5';
