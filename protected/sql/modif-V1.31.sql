ALTER TABLE `pagoConCheque` CHANGE `monto` `monto` FLOAT(8,2) NOT NULL;
ALTER TABLE `pagoAlContado` CHANGE `monto` `monto` FLOAT(8,2) NOT NULL;
ALTER TABLE `pagoCliente` CHANGE `monto` `monto` FLOAT(8,2) NOT NULL;
ALTER TABLE `chequeDeTerceros` CHANGE `importe` `importe` FLOAT(8,2) NULL DEFAULT NULL;
ALTER TABLE `comprobante` CHANGE `subtotalIva` `subtotalIva` FLOAT(8,2) NOT NULL DEFAULT '0', 
	CHANGE `alicuotaIIBB` `alicuotaIIBB` FLOAT(2,2) NULL DEFAULT '0', CHANGE `subtotal` `subtotal` FLOAT(8,2) NOT NULL DEFAULT '0', 
	CHANGE `Ing_Brutos` `Ing_Brutos` FLOAT(8,2) NULL DEFAULT NULL, CHANGE `Total_Neto` `Total_Neto` FLOAT(8,2) NULL DEFAULT NULL, 
	CHANGE `Total_Final` `Total_Final` FLOAT(8,2) NULL DEFAULT NULL;
ALTER TABLE `afipSubtotivas` CHANGE `BaseImp` `BaseImp` FLOAT(8,2) NOT NULL, CHANGE `importe` `importe` FLOAT(8,2) NOT NULL;	
ALTER TABLE `afipTributo` CHANGE `BaseImp` `BaseImp` FLOAT(8,2) NOT NULL, CHANGE `Importe` `Importe` FLOAT(8,2) NOT NULL;
ALTER TABLE `afipVoucher` CHANGE `importeOtrosTributos` `importeOtrosTributos` FLOAT(8,2) NULL DEFAULT '0' COMMENT 'ImpTrib', 
	CHANGE `importeIVA` `importeIVA` FLOAT(8,2) NOT NULL COMMENT 'ImpIVA', CHANGE `importeNoGravado` `importeNoGravado` FLOAT(8,2) NOT NULL COMMENT 'ImpTotConc', 
	CHANGE `importeExento` `importeExento` FLOAT(8,2) NULL DEFAULT '0' COMMENT 'ImpOpEx', CHANGE `importeGravado` `importeGravado` FLOAT(8,2) NOT NULL COMMENT 'ImpNeto', 
	CHANGE `importeTotal` `importeTotal` FLOAT(8,2) NOT NULL COMMENT 'ImpTotal';
ALTER TABLE `afipVoucherItem` CHANGE `precioUnitario` `precioUnitario` FLOAT(8,2) NOT NULL, 
	CHANGE `impBonif` `impBonif` FLOAT(8,2) NULL DEFAULT NULL, CHANGE `importeItem` `importeItem` FLOAT(8,2) NOT NULL;	
ALTER TABLE `articuloVenta` CHANGE `precio` `precio` FLOAT(8,2) NOT NULL, CHANGE `costo` `costo` FLOAT(8,2) NULL DEFAULT NULL;
ALTER TABLE `prodNotaPedido` CHANGE `precioUnit` `precioUnit` FLOAT(8,2) NOT NULL, CHANGE `costo` `costo` FLOAT(8,2) NULL DEFAULT NULL, CHANGE `subTotal` `subTotal` FLOAT(8,2) NULL DEFAULT NULL;
ALTER TABLE `prodComprobante` CHANGE `precioUnitario` `precioUnitario` FLOAT(8,2) NOT NULL DEFAULT '0';

ALTER TABLE `paquete` ADD `listaComercial` BOOLEAN NULL DEFAULT TRUE AFTER `cantidad`, ADD `listaInstitucional` BOOLEAN NULL DEFAULT TRUE AFTER `listaComercial`;
ALTER TABLE `comprobante` CHANGE `alicuotaIIBB` `alicuotaIIBB` FLOAT(3,2) NULL DEFAULT '0.00';
	