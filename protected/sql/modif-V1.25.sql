CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `vArtCotbyEc`
 AS select `pc`.`idCarga` AS `idCarga`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,SUM(`pc`.`cantidad`) AS `cantidad`,av.contenido, `pc`.`id` AS `id` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on(`pc`.`idArtVenta` = `ec`.`idArticuloVenta`) LEFT JOIN articuloVenta av on (pc.idArtVenta = av.id_articulo_vta ) ) GROUP by 
`pc`.`idCarga`,`ec`.`codCot`,`ec`.`descripcion`,`pc`.`idArtVenta`,`pc`.`idPaquete`,`pc`.`cantidad`,av.contenido,`pc`.`id`


ALTER TABLE `prodCarga` ADD `cantDepurada` INT NULL DEFAULT '0' AFTER `cantidad`;

CREATE TABLE `depuracion` (
  `id` int(7) NOT NULL,
  `fecha` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `idCarga` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `depuracion` ADD PRIMARY KEY (`id`), ADD KEY `idCarga` (`idCarga`);
  
ALTER TABLE `depuracion` MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `depuracion` ADD CONSTRAINT `depuracion_idCarga` FOREIGN KEY (`idCarga`) REFERENCES `carga` (`id`);

ALTER TABLE `cliente` CHANGE `PDpto` `PDpto` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

ALTER TABLE `cliente` DROP FOREIGN KEY `cliente_ibfk_6`; ALTER TABLE `cliente` ADD CONSTRAINT `cliente_Rubro` FOREIGN KEY (`id_rubro`) REFERENCES `rubroClientes`(`id_rubro`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `cliente` DROP FOREIGN KEY `cliente_ibfk_7`; ALTER TABLE `cliente` ADD CONSTRAINT `cliente_viajante` FOREIGN KEY (`codViajante`) REFERENCES `viajante`(`numero`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `cliente` DROP FOREIGN KEY `cliente_ibfk_8`; ALTER TABLE `cliente` ADD CONSTRAINT `cliente_codPostal` FOREIGN KEY (`cp`) REFERENCES `codigoPostal`(`codigoPostal`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `cliente` DROP FOREIGN KEY `cliente_ibfk_9`; ALTER TABLE `cliente` ADD CONSTRAINT `cliente_catIva` FOREIGN KEY (`categoriaIva`) REFERENCES `categoriaIva`(`id_categoria`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `cliente` CHANGE `fax` `fax` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

