-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-06-2019 a las 23:34:47
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `migra_vCliente`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `migra_vCliente`  AS  select `mce`.`Id_Cliente` AS `id`,`mce`.`Razon_Social` AS `razonSocial`,`mce`.`Direccion` AS `direccion`,`mce`.`Nombre_Fantasia` AS `nombreFantasia`,`mce`.`Contacto` AS `contacto`,`mce`.`CUIT` AS `cuit`,`mce`.`Telefonos` AS `telefonos`,`mce`.`Piso_Departamento` AS `PDpto`,`mce`.`E-mail` AS `email`,`mce`.`Fax` AS `fax`,`mce`.`Observaciones` AS `observaciones`,`mce`.`ID_Rubro` AS `id_rubro`,`mce`.`Direccion_Deposito` AS `direccionDeposito`,`mc`.`Nro_Viajante` AS `codViajante`,`mc`.`Codigo_Postal` AS `cp`,`mc`.`Zona_Postal` AS `zp`,`mc`.`Iva_Responsable` AS `categoriaIva`,`mc`.`Tipo_cliente` AS `tipoCliente`,`mc`.`Saldo_Actual` AS `saldoActual` from (`migra_cli_empresa` `mce` left join `migra_cliente` `mc` on((`mce`.`Id_Cliente` = `mc`.`Id_Cliente`))) ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
