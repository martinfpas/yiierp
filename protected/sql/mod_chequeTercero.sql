ALTER TABLE  `chequeDeTerceros` ADD  `id_banco` INT( 7 ) NULL AFTER  `fecha`;
ALTER TABLE  `chequeDeTerceros` ADD INDEX (  `id_banco` );
ALTER TABLE  `chequeDeTerceros` ADD FOREIGN KEY (  `id_banco` ) REFERENCES  `erca`.`banco` (
`id`
) ON DELETE SET NULL ON UPDATE SET NULL ;

ALTER TABLE  `chequeDeTerceros` DROP FOREIGN KEY  `chequeDeTerceros_ibfk_1` ,
ADD FOREIGN KEY (  `id_sucursal` ) REFERENCES  `erca`.`sucursalBanco` (
`id`
) ON DELETE SET NULL ON UPDATE SET NULL ;

