ALTER TABLE `cargaDocumentos` CHANGE `notaCredito` `notaCredito` DECIMAL(9,2) NOT NULL DEFAULT '0';
ALTER TABLE `pagoConCheque` CHANGE `monto` `monto` DECIMAL(10,2) NOT NULL;

CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `vCargaDocDePago`
 AS select `e`.`id` AS `idEntregaDoc`,`cd`.`id` AS `idCargaDoc`,`cd`.`tipoDoc`,`f`.`id` AS `idFactura`,`ne`.`id` AS `idNotaEntrega`,`pc`.`id` AS `idPago` from 
 (((((`entregaDoc` `e` left join `cargaDocumentos` `cd` on((`e`.`id` = `cd`.`idEntregaDoc`))) 
 left join `comprobante` `f` on((((`cd`.`tipoDoc` = 'FAC') or (`cd`.`tipoDoc` = 'NC')) and (`cd`.`idComprobante` = `f`.`id`)))) 
 left join `notaEntrega` `ne` on(((`cd`.`tipoDoc` = 'NE') and (`cd`.`idComprobante` = `ne`.`id`)))) 
 left join `pagoCliente` `pc` on(((`pc`.`id_factura` = `f`.`id`) or (`pc`.`idNotaEntrega` = `ne`.`id`) or (`pc`.`id` = `f`.`idPagoRelacionado`) or (`pc`.`id` = `ne`.`idPagoRelacionado`)))) 
 left join `pagoConCheque` `pcch` on((`pcch`.`idPagodeCliente` = `pc`.`id`))) 
 group by  `e`.`id` ,`cd`.`id`,`cd`.`tipoDoc`,`f`.`id`  ,`ne`.`id`  ,`pc`.`id`;
 
 
ALTER TABLE `cliente` CHANGE `saldoActual` `saldoActual` DECIMAL(10,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `cliente` ADD `saldoConciliado` DECIMAL(10,2) NOT NULL DEFAULT '0' AFTER `saldoActual`, ADD `fechaConciliacion` DATE NOT NULL DEFAULT '2020-08-26' AFTER `saldoConciliado`;

CREATE
 ALGORITHM = UNDEFINED
 VIEW `vDocumentosConSaldo3`
 AS select uuid() AS `uid`,'ND' AS `tipo`,`c`.`Nro_Comprobante` AS `nroComprobante`,c.estado,`c`.`id` AS `id`,`c`.`fecha` AS `fecha`,`c`.`Id_Cliente` AS `Id_Cliente`,cl.codViajante,`c`.`Total_Final` AS `total`,(`c`.`Total_Final` - `c`.`montoPagado`) AS `saldo` 
from `comprobante` `c` left join cliente cl on c.Id_Cliente = cl.id where ((`c`.`tipo` = 'ND') and (`c`.`Total_Final` > `c`.`montoPagado`) and c.estado = 2) 
union 
select uuid() AS `uid`,'F' AS `tipo`,`c`.`Nro_Comprobante` AS `nroComprobante`,c.estado,`c`.`id` AS `id`,`c`.`fecha` AS `fecha`,`c`.`Id_Cliente` AS `Id_Cliente`,cl.codViajante,`c`.`Total_Final` AS `total`,(`c`.`Total_Final` - `c`.`montoPagado`) AS `saldo` 
from `comprobante` `c` left join cliente cl on c.Id_Cliente = cl.id where ((`c`.`tipo` = 'F') and (`c`.`Total_Final` > `c`.`montoPagado`) and c.estado = 2) 
union 
select uuid() AS `uid`,'NE' AS `tipo`,`ne`.`nroComprobante` AS `nroComprobante`,ne.estado,`ne`.`id` AS `id`,`ne`.`fecha` AS `fecha`,`ne`.`idCliente` AS `idCliente`,cl.codViajante,`ne`.`totalFinal` AS `total`,(`ne`.`totalFinal` - `ne`.`montoPagado`) AS `saldo` 
from `notaEntrega` `ne` left join cliente cl on `ne`.`idCliente` = cl.id where (`ne`.`montoPagado` < `ne`.`totalFinal` and ne.estado = 2) 
order by `fecha` DESC

//conciliar 1
//conciliar 2
//conciliarEnCero
//conciliarNegativo