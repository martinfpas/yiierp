-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-10-2018 a las 11:42:49
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaDebito`
--

CREATE TABLE IF NOT EXISTS `notaDebito` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `clase` varchar(1) NOT NULL,
  `Nro_Puesto_Venta` int(3) NOT NULL,
  `Nro_Comprobante` varchar(8) NOT NULL,
  `Iva_Responsable` int(3) NOT NULL,
  `Nro_Viajante` int(2) NOT NULL,
  `Id_Cliente` int(7) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Observaciones` text,
  `Id_Camion` int(4) NOT NULL,
  `Total_Final` float DEFAULT NULL,
  `Enviada` tinyint(1) NOT NULL DEFAULT '0',
  `Recibida` tinyint(1) NOT NULL DEFAULT '0',
  `id_factura` int(7) NOT NULL,
  `Descuento1` float DEFAULT NULL,
  `Descuento2` float DEFAULT NULL,
  `Total_Neto` float DEFAULT NULL,
  `Otro_Iva` float DEFAULT NULL,
  `CAE` varchar(14) NOT NULL DEFAULT '00000000000000',
  `idVoucherAfip` int(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `Id_Cliente` (`Id_Cliente`),
  KEY `Id_Camion` (`Id_Camion`),
  KEY `Nro_Puesto_Venta` (`Nro_Puesto_Venta`),
  KEY `idVoucherAfip` (`idVoucherAfip`),
  KEY `Iva_Responsable` (`Iva_Responsable`),
  KEY `Nro_Viajante` (`Nro_Viajante`),
  KEY `id_factura` (`id_factura`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notaDebito`
--
ALTER TABLE `notaDebito`
  ADD CONSTRAINT `notaDebito_ibfk_1` FOREIGN KEY (`Nro_Puesto_Venta`) REFERENCES `puntoDeVenta` (`num`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_2` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_3` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_4` FOREIGN KEY (`idVoucherAfip`) REFERENCES `afipVoucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_5` FOREIGN KEY (`Nro_Viajante`) REFERENCES `viajante` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_6` FOREIGN KEY (`id_factura`) REFERENCES `factura` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_7` FOREIGN KEY (`Iva_Responsable`) REFERENCES `categoriaIva` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
