-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-03-2019 a las 09:34:52
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoTributoIibb`
--

CREATE TABLE IF NOT EXISTS `tipoTributoIibb` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `key` varchar(5) NOT NULL,
  `alicuota` float NOT NULL,
  `codProvincia` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='No modificar esta tabla sobre todo los tributo-id' AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `tipoTributoIibb`
--

INSERT INTO `tipoTributoIibb` (`id`, `name`, `key`, `alicuota`, `codProvincia`) VALUES
(1, 'Inscripto IIBB ER', 'I', 3, 'E'),
(2, 'NO Inscripto IIBB ER', 'NI', 6, 'E'),
(3, 'Coperativa Eléctrica IIBB ER', 'Cp', 1, 'E'),
(4, 'Exento', 'Ex', 0, 'E'),
(5, 'Inscripto IIBB ER Alicuto Reducida', 'AR', 0.5, 'E'),
(6, 'Sector Industrial', 'ID', 0.25, 'E');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
