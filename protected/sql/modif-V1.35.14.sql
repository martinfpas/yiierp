RENAME TABLE `erca`.`detalleFacturaProv` TO `erca`.`detalleComprobanteProv`;
RENAME TABLE `erca`.`facturaProveedor` TO `erca`.`comprobanteProveedor`;
ALTER TABLE `detalleComprobanteProv` CHANGE `idFacturaProveedor` `idComprobanteProveedor` INT(7) NOT NULL;
ALTER TABLE `pago2FactProv` CHANGE `idFacturaProveedor` `idComprobanteProveedor` INT(7) NOT NULL;
