ALTER TABLE  `entregaDoc` ADD  `idCarga` INT( 6 ) NULL AFTER  `otrasDevoluciones` ,
ADD INDEX (  `idCarga` );

ALTER TABLE  `entregaDoc` ADD FOREIGN KEY (  `idCarga` ) REFERENCES  `erca`.`carga` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;
