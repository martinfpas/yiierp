-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 13-06-2020 a las 00:37:45
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyEc`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyEc`  AS  select `pc`.`idCarga` AS `idCarga`,`ec`.`codCot` AS `codCot`,`pc`.`idArtVenta` AS `idArtVenta`,`ec`.`descripcion` AS `descripcion`,`pc`.`cantidad` AS `cantidad`,`av`.`contenido` AS `contenido`,concat(`pc`.`idCarga`,'-',`pc`.`idArtVenta`) AS `id` from ((`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) left join `articuloVenta` `av` on((`pc`.`idArtVenta` = `av`.`id_articulo_vta`))) group by `pc`.`idCarga`,`ec`.`codCot`,`ec`.`descripcion`,`pc`.`idArtVenta`,`av`.`contenido`,`pc`.`cantidad` order by `ec`.`descripcion` ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
