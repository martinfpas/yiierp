ALTER TABLE `facturaProveedor` ADD `montoPagado` FLOAT NOT NULL DEFAULT '0' AFTER `Cant_alicuotas_iva`;
ALTER TABLE `pagoProveedor` DROP FOREIGN KEY `proveedor`; ALTER TABLE `pagoProveedor` ADD CONSTRAINT `proveedor` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `pagoProvEfectivo` ADD CONSTRAINT `pagoProveedor2` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE
 ALGORITHM = UNDEFINED
 VIEW `vPagosContadoRelacionado`
 AS SELECT pc.*,c.id cid,ne.id neid, (CASE WHEN ne.idPagoRelacionado = null then c.idPagoRelacionado ELSE ne.idPagoRelacionado END) idPagoRelacionado 
FROM `pagoAlContado` as pc 
INNER JOIN pagoCliente as p on pc.idPagodeCliente = p.id 
LEFT JOIN comprobante as c on p.id_factura = c.id 
LEFT JOIN notaEntrega as ne on ne.id = p.idNotaEntrega


UPDATE `cuentasBancarias` SET `fechaCierre` = '2019-11-19' WHERE `cuentasBancarias`.`id` = 3;

