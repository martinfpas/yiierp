-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-02-2019 a las 16:39:15
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vArtCotbyEc`
--
CREATE TABLE IF NOT EXISTS `vArtCotbyEc` (
`id` int(6)
,`idCarga` int(6)
,`idArtVenta` int(6)
,`idPaquete` int(11)
,`cantidad` int(11)
,`codCot` varchar(6)
,`descripcion` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vArtCotbyPc`
--
CREATE TABLE IF NOT EXISTS `vArtCotbyPc` (
`id` int(6)
,`idCarga` int(6)
,`idArtVenta` int(6)
,`idPaquete` int(11)
,`cantidad` int(11)
,`codCot` varchar(6)
,`descripcion` varchar(50)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyEc`
--
DROP TABLE IF EXISTS `vArtCotbyEc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyEc` AS select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `ec`.`codCot`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyPc`
--
DROP TABLE IF EXISTS `vArtCotbyPc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyPc` AS select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `pc`.`id`;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
