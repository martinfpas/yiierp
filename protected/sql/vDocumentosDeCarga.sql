-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-04-2019 a las 14:10:13
-- Versión del servidor: 5.7.25-0ubuntu0.18.04.2
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vDocumentosDeCarga`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vDocumentosDeCarga`  AS  select uuid() AS `id`,`Fa`.`id` AS `idFa`,NULL AS `idNe`,`Fa`.`idNotaPedido` AS `idNotaPedido`,`Fa`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`comprobante` `Fa` left join `notaPedido` `Np` on((`Fa`.`idNotaPedido` = `Np`.`id`))) union select uuid() AS `id`,NULL AS `idFa`,`Ne`.`id` AS `idNe`,`Ne`.`idNotaPedido` AS `idNotaPedido`,`Ne`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`notaEntrega` `Ne` left join `notaPedido` `Np` on((`Ne`.`idNotaPedido` = `Np`.`id`))) order by `fecha` desc ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
