DELETE FROM `categoriaIva` WHERE `categoriaIva`.`id_categoria` = 8";
UPDATE `cliente` SET `codViajante`=1 WHERE isnull(`codViajante`);
ALTER TABLE `cliente` CHANGE `codViajante` `codViajante` INT(2) NOT NULL;
ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_PagoRel` FOREIGN KEY (`idPagoRelacionado`) REFERENCES `pagoCliente`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE `notaEntrega` DROP FOREIGN KEY `notaEntrega_ibfk_9`; ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_Camion` FOREIGN KEY (`idCamion`) REFERENCES `vehiculo`(`id_vehiculo`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `notaEntrega` DROP FOREIGN KEY `notaEntrega_ibfk_10`; ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_Viajante` FOREIGN KEY (`nroViajante`) REFERENCES `viajante`(`numero`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `notaEntrega` DROP FOREIGN KEY `notaEntrega_ibfk_8`; ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_Cliente` FOREIGN KEY (`idCliente`) REFERENCES `cliente`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `notaEntrega` DROP FOREIGN KEY `notaEntrega_ibfk_7`; ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_NotaPedido` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `notaEntrega` DROP FOREIGN KEY `notaEntrega_ibfk_6`; ALTER TABLE `notaEntrega` ADD CONSTRAINT `notaEntrega_PuestoVenta` FOREIGN KEY (`nroPuestoVenta`) REFERENCES `puntoDeVenta`(`num`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE ALGORITHM = UNDEFINED VIEW `vPagosDeEntregaDoc2` AS select `e`.`id` AS `idEntregaDoc`,`cd`.`id` AS `idCargaDoc`,`f`.`id` AS `idFactura`,`ne`.`id` AS `idNotaEntrega`,`pc`.`id` AS `idPago`,`pc`.`idCliente` AS `idCliente`,`pc`.`monto` AS `monto`,`pcch`.`idChequedeTercero` AS `idChequedeTercero` from (((((`entregaDoc` `e` left join `cargaDocumentos` `cd` on((`e`.`id` = `cd`.`idEntregaDoc`))) left join `comprobante` `f` on(((`cd`.`tipoDoc` = 'FAC') and (`cd`.`idComprobante` = `f`.`id`)))) left join `notaEntrega` `ne` on(((`cd`.`tipoDoc` = 'NE') and (`cd`.`idComprobante` = `ne`.`id`)))) left join `pagoCliente` `pc` on(((`pc`.`id_factura` = `f`.`id`) or (`pc`.`idNotaEntrega` = `ne`.`id`) or (`pc`.`id` = `f`.`idPagoRelacionado`) or (`pc`.`id` = `ne`.`idPagoRelacionado`) ))) left join `pagoConCheque` `pcch` on((`pcch`.`idPagodeCliente` = `pc`.`id`)));

select `e`.`id` AS `idEntregaDoc`,`cd`.`id` AS `idCargaDoc`,`f`.`id` AS `idFactura`,`ne`.`id` AS `idNotaEntrega`,`pc`.`id` AS `idPago`,`pc`.`idCliente` AS `idCliente`,`pc`.`monto` AS `monto`,`pcch`.`idChequedeTercero` AS `idChequedeTercero` 
	from (((((`entregaDoc` `e` left join `cargaDocumentos` `cd` on((`e`.`id` = `cd`.`idEntregaDoc`))) 
	left join `comprobante` `f` on(((`cd`.`tipoDoc` = 'FAC') and (`cd`.`idComprobante` = `f`.`id`)))) 
	left join `notaEntrega` `ne` on(((`cd`.`tipoDoc` = 'NE') and (`cd`.`idComprobante` = `ne`.`id`)))) 
	left join `pagoCliente` `pc` on(((`pc`.`id_factura` = `f`.`id`) or (`pc`.`idNotaEntrega` = `ne`.`id`) or (`pc`.`id` = `f`.`idPagoRelacionado`) or (`pc`.`id` = `ne`.`idPagoRelacionado`) ))) 
	left join `pagoConCheque` `pcch` on((`pcch`.`idPagodeCliente` = `pc`.`id`)));
	
ALTER TABLE `medioMovimiento` ADD `activo` BOOLEAN NOT NULL DEFAULT TRUE AFTER `Demora`;	

CREATE TABLE `cheque3RoMovBancario` (
  `id` int(6) NOT NULL,
  `idCheque3ro` int(7) NOT NULL,
  `idMovimiento` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `cheque3RoMovBancario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCheque3ro` (`idCheque3ro`),
  ADD KEY `idMovimiento` (`idMovimiento`);

ALTER TABLE `cheque3RoMovBancario`  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

ALTER TABLE `cheque3RoMovBancario`
  ADD CONSTRAINT `cheque3Ro` FOREIGN KEY (`idCheque3ro`) REFERENCES `chequeDeTerceros` (`id`),
  ADD CONSTRAINT `movimientoCB` FOREIGN KEY (`idMovimiento`) REFERENCES `movimientoCuentasBanc` (`id`);