CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW  `vDocumentosCarga` AS
SELECT UUID( ) AS id, Fa.id AS idFa, NULL AS idNe, Fa.idNotaPedido AS idNotaPedido, Fa.fecha AS fecha, Np.id AS idNp, Np.idCarga AS idCarga
FROM comprobante AS Fa
LEFT JOIN notaPedido AS Np ON idNotaPedido = Np.id
UNION 
SELECT UUID( ) AS id, NULL AS idFa, Ne.id AS idNe, Ne.idNotaPedido AS idNotaPedido, Ne.fecha AS fecha, Np.id AS idNp, Np.idCarga AS idCarga
FROM notaEntrega AS Ne
LEFT JOIN notaPedido AS Np ON idNotaPedido = Np.id
ORDER BY fecha DESC