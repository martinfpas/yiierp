-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-02-2019 a las 14:18:14
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equivalenciaCot`
--

CREATE TABLE IF NOT EXISTS `equivalenciaCot` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `idArticuloVenta` int(6) NOT NULL,
  `codCot` varchar(6) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idArticuloVenta` (`idArticuloVenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equivalenciaCot`
--
ALTER TABLE `equivalenciaCot`
  ADD CONSTRAINT `equivalenciaCot_ibfk_1` FOREIGN KEY (`idArticuloVenta`) REFERENCES `articuloVenta` (`id_articulo`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
