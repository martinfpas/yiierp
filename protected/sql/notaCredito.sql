-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-10-2018 a las 14:12:37
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaCredito`
--

CREATE TABLE IF NOT EXISTS `notaCredito` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `clase` varchar(1) NOT NULL,
  `nroPuestoVta` int(3) NOT NULL,
  `nroComprobante` int(8) NOT NULL,
  `ivaResponsable` int(2) NOT NULL,
  `nroViajante` int(2) NOT NULL,
  `idCliente` int(7) NOT NULL,
  `fecha` date DEFAULT NULL,
  `idCamion` int(4) NOT NULL,
  `totalFinal` float NOT NULL,
  `enviada` tinyint(1) NOT NULL,
  `recibida` tinyint(1) DEFAULT NULL,
  `observaciones` varchar(30) NOT NULL,
  `otroIva` float DEFAULT NULL,
  `descuento1` float DEFAULT '0',
  `descuento2` float DEFAULT NULL,
  `totalNeto` float DEFAULT NULL,
  `cae` int(15) NOT NULL,
  `vencimCae` datetime NOT NULL,
  `ingBrutos` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ivaResponsable` (`ivaResponsable`),
  KEY `nroViajante` (`nroViajante`),
  KEY `idCliente` (`idCliente`),
  KEY `idCamion` (`idCamion`),
  KEY `nroPuestoVta` (`nroPuestoVta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notaCredito`
--
ALTER TABLE `notaCredito`
  ADD CONSTRAINT `notaCredito_ibfk_1` FOREIGN KEY (`nroViajante`) REFERENCES `viajante` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaCredito_ibfk_2` FOREIGN KEY (`idCamion`) REFERENCES `vehiculo` (`id_vehiculo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaCredito_ibfk_3` FOREIGN KEY (`nroPuestoVta`) REFERENCES `puntoDeVenta` (`num`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaCredito_ibfk_4` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notaCredito_ibfk_5` FOREIGN KEY (`ivaResponsable`) REFERENCES `categoriaIva` (`id_categoria`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
