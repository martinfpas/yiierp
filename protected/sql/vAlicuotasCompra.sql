CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `vAlicuotasCompra`
 AS SELECT COUNT(DISTINCT(alicuota)) as totalIvas,idComprobanteCompra FROM `alicuotasCompra` group by idComprobanteCompra;