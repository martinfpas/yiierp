-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 18-06-2020 a las 22:45:39
-- Versión del servidor: 5.7.29-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vCtaCteCliente`
--

CREATE OR REPLACE
ALGORITHM = UNDEFINED VIEW `vCtaCteCliente`  AS  

select uuid() AS `id`,'NE' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`NE`.`fecha` AS `fecha`,`NE`.`totalFinal` AS `debe`,NULL AS `haber`,`NE`.`id` AS `neId`,NULL AS `faId`,NULL AS `pcId`,NULL AS `ncId`,NULL AS `ndId` 
from (`notaEntrega` `NE` left join `cliente` `CL` on((`CL`.`id` = `NE`.`idCliente` and `NE`.`estado` >= 2))) 
union select uuid() AS `id`,`CO`.`tipo` AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`CO`.`fecha` AS `fecha`,if((`CO`.`tipo` <> 'NC'),`CO`.`Total_Final`,NULL) AS `debe`,if((`CO`.`tipo` = 'NC'),`CO`.`Total_Final`,NULL) AS `haber`,NULL AS `neId`,if((`CO`.`tipo` = 'F'),`CO`.`id`,NULL) AS `faId`,NULL AS `pcId`,if((`CO`.`tipo` = 'NC'),`CO`.`id`,NULL) AS `ncId`,if((`CO`.`tipo` = 'ND'),`CO`.`id`,NULL) AS `ndId` from (`comprobante` `CO` left join `cliente` `CL` on((`CL`.`id` = `CO`.`Id_Cliente` and `CO`.`CAE` <> '00000000000000' ))) 
union select uuid() AS `id`,'PC' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`PC`.`fecha` AS `fecha`,NULL AS `debe`,`PC`.`monto` AS `haber`,NULL AS `neId`,NULL AS `faId`,`PC`.`id` AS `pcId`,NULL AS `ncId`,NULL AS `ndId` from (`pagoCliente` `PC` left join `cliente` `CL` on((`CL`.`id` = `PC`.`idCliente`))) order by `fecha` desc ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
