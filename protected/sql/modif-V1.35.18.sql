ALTER TABLE `facturaOrdenPago` CHANGE `idFacturaProveedor` `idComprobanteProveedor` INT(6) NOT NULL;
ALTER TABLE `categoriaIva` ADD `agregaIva` BOOLEAN NOT NULL DEFAULT FALSE AFTER `alicuota`;
UPDATE `categoriaIva` SET `agregaIva` = '1' WHERE `categoriaIva`.`id_categoria` = 6;
ALTER TABLE `categoriaIva` CHANGE `descrip` `descrip` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL COMMENT 'Descripcion';