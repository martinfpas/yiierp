-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-04-2019 a las 14:09:27
-- Versión del servidor: 5.7.25-0ubuntu0.18.04.2
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vComprobantesSinCargaDoc`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vComprobantesSinCargaDoc`  AS  select `c`.`id` AS `id`,`c`.`tipo` AS `tipo`,`c`.`Id_Camion` AS `idCamion` from (`comprobante` `c` left join `cargaDocumentos` `cd` on(((`c`.`id` = `cd`.`idComprobante`) and ((`cd`.`tipoDoc` = 'FA') or (`cd`.`tipoDoc` = 'NC') or (`cd`.`tipoDoc` = 'ND'))))) where isnull(`cd`.`idComprobante`) union select `ne`.`id` AS `id`,'NE' AS `tipo`,`ne`.`idCamion` AS `idCamion` from (`notaEntrega` `ne` left join `cargaDocumentos` `cd` on(((`ne`.`id` = `cd`.`idComprobante`) and (`cd`.`tipoDoc` = 'NE')))) where isnull(`cd`.`idComprobante`) order by `id` desc ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
