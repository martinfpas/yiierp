ALTER TABLE `comprobanteProveedor` CHANGE `TotalNeto` `TotalNeto` FLOAT(8,2) NOT NULL DEFAULT '0';
ALTER TABLE `comprobanteProveedor` CHANGE `Subtotal` `Subtotal` FLOAT(8,2) NOT NULL DEFAULT '0';

SELECT pc.*,r.descripcion as nombre_rubro,a.descripcion as nombre_articulo,av.descripcion,p.cantidad as xBulto FROM `prodCarga` pc left JOIN articuloVenta av on pc.idArtVenta = av.id_articulo_vta LEFT JOIN articulo a on a.id = av.id_articulo LEFT JOIN rubro r on a.id_rubro = r.id_rubro LEFT JOIN paquete p on pc.idPaquete=p.id order by r.descripcion,a.descripcion,p.cantidad;

ALTER TABLE `paquete` ADD `activo` BOOLEAN NOT NULL DEFAULT TRUE AFTER `listaInstitucional`;

CREATE TABLE `remitoProveedor` (
  `id` int(8) NOT NULL,
  `idProveedor` int(4) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `fecha` date NOT NULL,
  `detalle` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `remitoProveedor` ADD PRIMARY KEY (`id`),  ADD KEY `remito_proveedor` (`idProveedor`);

ALTER TABLE `remitoProveedor`  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT;
ALTER TABLE `remitoProveedor`  ADD CONSTRAINT `remito_proveedor` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`id`);