-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-12-2019 a las 19:58:48
-- Versión del servidor: 5.7.28-0ubuntu0.18.04.4
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaOrdenPago`
--

CREATE TABLE `facturaOrdenPago` (
  `id` int(6) NOT NULL,
  `idFacturaProveedor` int(6) NOT NULL,
  `idOrdeDePago` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ordenDePago`
--

CREATE TABLE `ordenDePago` (
  `id` int(6) NOT NULL,
  `observacion` text,
  `fecha` date NOT NULL,
  `idPagoProveedor` int(6) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `facturaOrdenPago`
--
ALTER TABLE `facturaOrdenPago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `OPD_FacturaProv` (`idFacturaProveedor`),
  ADD KEY `Detalle_OrdenDePago` (`idOrdeDePago`);

--
-- Indices de la tabla `ordenDePago`
--
ALTER TABLE `ordenDePago`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ODP_user` (`user_id`),
  ADD KEY `idPagoProveedor` (`idPagoProveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `facturaOrdenPago`
--
ALTER TABLE `facturaOrdenPago`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ordenDePago`
--
ALTER TABLE `ordenDePago`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `facturaOrdenPago`
--
ALTER TABLE `facturaOrdenPago`
  ADD CONSTRAINT `Fact_ODP` FOREIGN KEY (`idOrdeDePago`) REFERENCES `ordenDePago` (`id`),
  ADD CONSTRAINT `ODP_Fact` FOREIGN KEY (`idFacturaProveedor`) REFERENCES `facturaProveedor` (`id`);

--
-- Filtros para la tabla `ordenDePago`
--
ALTER TABLE `ordenDePago`
  ADD CONSTRAINT `ODP_PagoProv` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`),
  ADD CONSTRAINT `ODP_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
