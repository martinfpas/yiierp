ALTER TABLE  `factura` ADD  `idVoucherAfip` INT( 5 ) NULL AFTER  `idNotaPedido`;
ALTER TABLE  `factura` ADD INDEX (  `idVoucherAfip` );
ALTER TABLE  `factura` ADD FOREIGN KEY (  `idVoucherAfip` ) REFERENCES  `erca`.`afipVoucher` (
`id`
) ON DELETE SET NULL ON UPDATE SET NULL ;
ALTER TABLE  `afipVoucher` CHANGE  `importeOtrosTributos`  `importeOtrosTributos` FLOAT NULL DEFAULT  '0' COMMENT  'ImpTrib';
ALTER TABLE  `afipVoucher` CHANGE  `importeExento`  `importeExento` FLOAT NULL DEFAULT  '0' COMMENT  'ImpOpEx';
ALTER TABLE  `factura` CHANGE  `Iva_Responsable`  `Iva_Responsable` INT( 3 ) NOT NULL;
ALTER TABLE  `factura` ADD FOREIGN KEY (  `Iva_Responsable` ) REFERENCES  `erca`.`categoriaIva` (
`id_categoria`
) ON DELETE CASCADE ON UPDATE CASCADE ;