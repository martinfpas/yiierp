ALTER TABLE `movimientoCuentasBanc` ADD `saldo_acumulado` DECIMAL(9,2) NULL DEFAULT '0' AFTER `Nro_operacion`;
DELETE FROM `cargaDocumentos` WHERE `id` = 875;
ALTER TABLE `pagoAlContado` CHANGE `monto` `monto` DECIMAL(9,2) NOT NULL;

ALTER TABLE `cargaDocumentos` CHANGE `totalFinal` `totalFinal` DECIMAL(9,2) NOT NULL, CHANGE `pagado` `pagado` DECIMAL(9,2) NOT NULL DEFAULT '0', CHANGE `efectivo` `efectivo` DECIMAL(9,2) NOT NULL DEFAULT '0.00', CHANGE `cheque` `cheque` DECIMAL(9,2) NOT NULL DEFAULT '0.00', CHANGE `devolucion` `devolucion` DECIMAL(9,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `cargaDocumentos` CHANGE `pagado` `pagado` FLOAT NOT NULL DEFAULT '0';
ALTER TABLE `cargaDocumentos` ADD UNIQUE( `tipoDoc`, `idComprobante`);
ALTER TABLE `chequePropio` CHANGE `importe` `importe` DECIMAL(9,2) NULL DEFAULT NULL;

UPDATE `comprobante` SET `idNotaDeCredito` = '453' WHERE `comprobante`.`id` = 458;