-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-11-2019 a las 02:19:19
-- Versión del servidor: 5.7.28-0ubuntu0.18.04.4
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago2FactProv`
--

CREATE TABLE IF NOT EXISTS `pago2FactProv` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `idFacturaProveedor` int(7) NOT NULL,
  `idPagoProveedor` int(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idFacturaProveedor` (`idFacturaProveedor`),
  KEY `idPagoProveedor` (`idPagoProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvCheque`
--

CREATE TABLE IF NOT EXISTS `pagoProvCheque` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `idCheque3ro` int(11) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `idPagoProv` int(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cheque3ro` (`idCheque3ro`),
  KEY `pagoProveedor` (`idPagoProv`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProveedor`
--

CREATE TABLE IF NOT EXISTS `pagoProveedor` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `fecha` date NOT NULL,
  `idProveedor` int(4) NOT NULL,
  `idPago2Factura` int(7) DEFAULT NULL,
  `estado` smallint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idProveedor` (`idProveedor`),
  KEY `idPago2Factura` (`idPago2Factura`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvEfectivo`
--

CREATE TABLE IF NOT EXISTS `pagoProvEfectivo` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `idPagoProveedor` int(7) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `pagoProv` (`idPagoProveedor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvMovimientoBanc`
--

CREATE TABLE IF NOT EXISTS `pagoProvMovimientoBanc` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `idMovBanc` int(7) NOT NULL,
  `idPagoProveedor` int(7) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `movBanc` (`idMovBanc`),
  KEY `pagoProv` (`idPagoProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pago2FactProv`
--
ALTER TABLE `pago2FactProv`
  ADD CONSTRAINT `facturaProveedor` FOREIGN KEY (`idFacturaProveedor`) REFERENCES `facturaProveedor` (`id`),
  ADD CONSTRAINT `pagoProveedor` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`);

--
-- Filtros para la tabla `pagoProvCheque`
--
ALTER TABLE `pagoProvCheque`
  ADD CONSTRAINT `chequeDeTercero` FOREIGN KEY (`idCheque3ro`) REFERENCES `chequeDeTerceros` (`id`),
  ADD CONSTRAINT `pagoProvee` FOREIGN KEY (`idPagoProv`) REFERENCES `pagoProveedor` (`id`);

--
-- Filtros para la tabla `pagoProveedor`
--
ALTER TABLE `pagoProveedor`
  ADD CONSTRAINT `proveedor` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`id`),
  ADD CONSTRAINT `tablaIntermedia` FOREIGN KEY (`idPago2Factura`) REFERENCES `pago2FactProv` (`id`);

--
-- Filtros para la tabla `pagoProvEfectivo`
--
ALTER TABLE `pagoProvEfectivo`
  ADD CONSTRAINT `pagoProveedor2` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`);

--
-- Filtros para la tabla `pagoProvMovimientoBanc`
--
ALTER TABLE `pagoProvMovimientoBanc`
  ADD CONSTRAINT `movimientoBanc` FOREIGN KEY (`idMovBanc`) REFERENCES `movimientoCuentasBanc` (`id`),
  ADD CONSTRAINT `pagoProv` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
