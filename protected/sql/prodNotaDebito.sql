-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2018 a las 20:29:26
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodNotaDebito`
--

CREATE TABLE IF NOT EXISTS `prodNotaDebito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idNotaDebito` int(8) NOT NULL,
  `nroItem` int(2) NOT NULL,
  `idArticuloVta` int(6) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idArticuloVta` (`idArticuloVta`),
  KEY `idNotaDebito` (`idNotaDebito`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `prodNotaDebito`
--
ALTER TABLE `prodNotaDebito`
  ADD CONSTRAINT `prodNotaDebito_ibfk_1` FOREIGN KEY (`idNotaDebito`) REFERENCES `notaDebito` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaDebito_ibfk_2` FOREIGN KEY (`idArticuloVta`) REFERENCES `articuloVenta` (`id_articulo_vta`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
