CREATE
 ALGORITHM = UNDEFINED
 VIEW `vCtaCteProv`
 AS SELECT cp.id AS cp_id,'' AS rp_id   ,cp.TipoComprobante as TipoComprobante,cp.Nro_Comprobante as Nro_Comprobante,cp.id_Proveedor AS id_Proveedor,cp.FechaFactura AS fecha ,cp.TotalNeto AS TotalNeto FROM comprobanteProveedor cp 
UNION
SELECT uuid() AS cp_id   ,rp.id as rp_id,'001' as TipoComprobante             ,'' as Nro_Comprobante                ,rp.idProveedor AS id_Proveedor,rp.fecha as fecha,rp.monto as TotalNeto FROM remitoProveedor rp  
ORDER BY `fecha`  DESC


ALTER TABLE `alicuotasCompra` CHANGE `alicuota` `alicuota` FLOAT(5,2) NOT NULL;
ALTER TABLE `alicuotasCompra` CHANGE `alicuota` `alicuota` FLOAT(3,2) NOT NULL;
ALTER TABLE `comprobanteProveedor` CHANGE `TotalNeto` `TotalNeto` FLOAT(9,2) NOT NULL DEFAULT '0.00';