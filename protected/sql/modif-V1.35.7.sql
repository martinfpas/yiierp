CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `vComprobantesSinCargaDoc`
 AS select c.id AS id,c.tipo AS tipo,c.Id_Camion AS idCamion,cd.idComprobante AS idComprobante,c.Id_Cliente as idCliente
    	from (comprobante c left join cargaDocumentos cd on(((c.id = cd.idComprobante) and ((cd.tipoDoc = 'FAC') or (cd.tipoDoc = 'NC') or (cd.tipoDoc = 'ND'))))) where isnull(cd.idComprobante) 
union select ne.id AS id,'NE' AS tipo,ne.idCamion AS idCamion,cd.idComprobante AS idComprobante, ne.idCliente as idCliente from (notaEntrega ne left join cargaDocumentos cd on(((ne.id = cd.idComprobante) and (cd.tipoDoc = 'NE')))) where isnull(cd.idComprobante) order by id desc

ALTER TABLE `ordenDePago` DROP FOREIGN KEY `ODP_PagoProv`; ALTER TABLE `ordenDePago` ADD CONSTRAINT `ODP_PagoProv` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor`(`id`) ON DELETE SET NULL ON UPDATE RESTRICT;

