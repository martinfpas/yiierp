-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 27-11-2019 a las 10:30:23
-- Versión del servidor: 5.7.28-0ubuntu0.18.04.4
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProveedorChequePpio`
--

CREATE TABLE `pagoProveedorChequePpio` (
  `id` int(7) NOT NULL,
  `idChequePpio` int(11) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `idPagoProv` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pagoProveedorChequePpio`
--
ALTER TABLE `pagoProveedorChequePpio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idChequePpio` (`idChequePpio`),
  ADD KEY `idPagoProveedor` (`idPagoProv`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pagoProveedorChequePpio`
--
ALTER TABLE `pagoProveedorChequePpio`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pagoProveedorChequePpio`
--
ALTER TABLE `pagoProveedorChequePpio`
  ADD CONSTRAINT `ChequePropio` FOREIGN KEY (`idChequePpio`) REFERENCES `chequePropio` (`id`),
  ADD CONSTRAINT `pagoChPpio_pagoProveedor` FOREIGN KEY (`idPagoProv`) REFERENCES `pagoProveedor` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
