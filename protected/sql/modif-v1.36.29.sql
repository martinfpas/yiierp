ALTER TABLE `cuentasBancarias` ADD `saldoInicial` DECIMAL(9,2) NOT NULL DEFAULT '0' AFTER `cbu`;
UPDATE `cuentasBancarias` SET `saldoInicial` = '1044420.98' WHERE `cuentasBancarias`.`id` = 2;
UPDATE `cuentasBancarias` SET `saldoInicial` = '87751.47' WHERE `cuentasBancarias`.`id` = 5;
UPDATE `cuentasBancarias` SET `saldoInicial` = '905841.05' WHERE `cuentasBancarias`.`id` = 6;
