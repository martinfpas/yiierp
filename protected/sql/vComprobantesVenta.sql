-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generaci�n: 11-10-2019 a las 22:50:43
-- Versi�n del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versi�n de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vComprobantesVenta`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vComprobantesVenta`  AS  select `c`.`fecha` AS `fecha`,concat(`c`.`tipo`,'|',`c`.`clase`) AS `tipo`,`c`.`Nro_Puesto_Venta` AS `Nro_Puesto_Venta`,`c`.`Nro_Comprobante` AS `Nro_Comprobante`,0 AS `hasta`,`a`.`codigoTipoDocumento` AS `codigoTipoDocumento`,`c`.`cuit` AS `cuit`,`c`.`razonSocial` AS `razonSocial`,`c`.`Total_Final` AS `Total_Final`,0 AS `totalConc`,0 AS `percepNoCateg`,0 AS `impOpExentas`,0 AS `impPercImpNac`,`c`.`Ing_Brutos` AS `Ing_Brutos`,0 AS `impMunic`,0 AS `impInternos`,`c`.`Moneda` AS `Moneda`,`a`.`cotizacionMoneda` AS `tipoCambio`,1 AS `cantAlicIva`,'A' AS `codOperacion`,0 AS `otrosTributos`,`a`.`fechaVtoPago` AS `fechaVtoPago`,date_format(`c`.`fecha`,'%m-%Y') AS `periodo`,`c`.`alicuotaIva` AS `alicuotaIva`,'' AS `codigoOperacionElectronica`,(`c`.`Total_Final` - `c`.`subtotalIva`) AS `importeNetoGravado`,`c`.`subtotalIva` AS `impuestoLiquidado` from (`comprobante` `c` left join `afipVoucher` `a` on((`c`.`idVoucherAfip` = `a`.`id`))) where ((`c`.`CAE` <> '00000000000000') and (`c`.`CAE` is not null)) ;

