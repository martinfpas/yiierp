ALTER TABLE `cuentasBancarias` CHANGE `id_sucursal` `id_sucursal` INT(7) NULL;
ALTER TABLE `cuentasBancarias` CHANGE `cbu` `cbu` VARCHAR(22) NULL DEFAULT NULL;
ALTER TABLE `cuentasBancarias` DROP FOREIGN KEY `cuentasBancarias_ibfk_5`; ALTER TABLE `cuentasBancarias` ADD CONSTRAINT `cuentasBancarias_sucursal` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursalBanco`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `cuentasBancarias` DROP FOREIGN KEY `cuentasBancarias_ibfk_6`; ALTER TABLE `cuentasBancarias` ADD CONSTRAINT `cuentasBancarias_tipoCuenta` FOREIGN KEY (`codigoTipoCta`) REFERENCES `tipoCuenta`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `cuentasBancarias` DROP FOREIGN KEY `cuentasBancarias_ibfk_7`; ALTER TABLE `cuentasBancarias` ADD CONSTRAINT `cuentasBancarias_moneda` FOREIGN KEY (`codigoMoneda`) REFERENCES `moneda`(`codigo_moneda`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `cuentasBancarias` ADD `numero` VARCHAR(30) NOT NULL AFTER `id`;
INSERT INTO `cuentasBancarias` (`id`, `numero`, `id_banco`, `id_sucursal`, `observaciones`, `codigoTipoCta`, `saldo`, `fechaSaldo`, `codigoMoneda`, `fechaApertura`, `fechaCierre`, `cbu`) VALUES
(2, '1564/08', 1, 1, '', 1, -15355.2, '2010-07-26', 1, '1989-09-01', NULL, '3300504915040001564086'),
(3, '2138/09', 1, 1, '', 1, 21657.5, '2010-07-26', 1, '2006-06-01', NULL, ''),
(4, '49100452/55', 11, NULL, '', 1, 4470.45, '2010-02-11', 1, '2002-08-12', NULL, '0110491620049100452553');

CREATE TABLE `medioMovimiento` (
  `CodMedio` int(2) NOT NULL COMMENT 'Codigo iedntificador del tipo de medio utilizado para el movimento en cuenta bancaria',
  `Descripcion` varchar(20) NOT NULL COMMENT 'Medios utilizados para los movimientos bancarios',
  `Demora` int(2) NOT NULL DEFAULT '0' COMMENT 'Especifica la demora en asentarse en la cuenta bancaria de este medio, tiempo minimo especificado en dias'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `medioMovimiento` (`CodMedio`, `Descripcion`, `Demora`) VALUES
(1, 'EFECTIVO', 0),
(2, 'CHEQUE', 1),
(3, 'CHEQUE BIS', 1),
(4, 'LECOP', 0),
(5, 'FEDERAL', 0),
(6, 'CUPON', 1);
ALTER TABLE `medioMovimiento` ADD PRIMARY KEY (`CodMedio`);


CREATE TABLE `clasifMovimientoCuentas` (
  `codigo` int(2) NOT NULL,
  `Descripcion` varchar(30) NOT NULL,
  `Ingreso` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Clasificaciones para los movimientos en las cuentas bancaria';
INSERT INTO `clasifMovimientoCuentas` (`codigo`, `Descripcion`, `Ingreso`) VALUES
(1, 'EXTRACCIONES', 0),
(2, 'DEPOSITOS', 1),
(3, 'DEBITO', 1),
(4, 'CREDITO', 0),
(5, 'TRANSFERENCIAS', 1);
ALTER TABLE `clasifMovimientoCuentas`  ADD PRIMARY KEY (`codigo`);

ALTER TABLE `cuentasBancarias` CHANGE `id` `id` INT(3) NOT NULL AUTO_INCREMENT;


CREATE TABLE `movimientoCuentasBanc` (
  `id` int(6) NOT NULL,
  `idCuenta` int(3) NOT NULL,
  `Importe` float NOT NULL,
  `tipoMovimiento` int(1) NOT NULL COMMENT 'Identificador �nico del tipo de movimiento bancario',
  `codigoMovimiento` int(7) NOT NULL COMMENT 'Identificador unico del movimiento bancario, segun la cuenta.',
  `idMedio` int(2) NOT NULL,
  `fechaMovimiento` date NOT NULL,
  `Nro_operacion` varchar(9) DEFAULT NULL,
  `Observacion` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `movimientoCuentasBanc`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idCuenta` (`idCuenta`,`codigoMovimiento`),
  ADD KEY `movimientoCB_Cuenta` (`idCuenta`),
  ADD KEY `movimientoCB_medio` (`idMedio`),
  ADD KEY `movimientoCB_tipo` (`tipoMovimiento`);
ALTER TABLE `movimientoCuentasBanc` MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
ALTER TABLE `movimientoCuentasBanc`
  ADD CONSTRAINT `movimientoCB_Cuenta` FOREIGN KEY (`idCuenta`) REFERENCES `cuentasBancarias` (`id`),
  ADD CONSTRAINT `movimientoCB_medio` FOREIGN KEY (`idMedio`) REFERENCES `medioMovimiento` (`CodMedio`),
  ADD CONSTRAINT `movimientoCB_tipo` FOREIGN KEY (`tipoMovimiento`) REFERENCES `clasifMovimientoCuentas` (`codigo`);
ALTER TABLE `movimientoCuentasBanc` DROP INDEX `idCuenta`;
ALTER TABLE `movimientoCuentasBanc` CHANGE `codigoMovimiento` `codigoMovimiento` INT(7) NULL COMMENT 'Identificador unico del movimiento bancario, segun la cuenta.'; 	 

ALTER TABLE `chequePropio` DROP FOREIGN KEY `chequePropio_ibfk_2`; ALTER TABLE `chequePropio` ADD CONSTRAINT `chequePropio_CuentaBan` FOREIGN KEY (`id_cuenta`) REFERENCES `cuentasBancarias`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;  

ALTER TABLE `chequePropio` CHANGE `nroCheque` `nroCheque` INT(8) NOT NULL;
  
  