-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 10-06-2019 a las 19:52:10
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiaPrima`
--

CREATE TABLE `materiaPrima` (
  `id` int(6) NOT NULL,
  `id_rubro` int(3) NOT NULL,
  `Descripcion` varchar(40) NOT NULL COMMENT 'Descripción de la materia prima',
  `id_articulo` int(5) NOT NULL COMMENT 'Identificación única de artículos',
  `Id_Proveedor` int(4) NOT NULL,
  `Unidad_Medida` varchar(4) NOT NULL,
  `Precio` float NOT NULL DEFAULT '0',
  `fecha_act_precio` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_rubro` (`id_rubro`),
  ADD KEY `materiaPrima_Articulo` (`id_articulo`),
  ADD KEY `materiaPrima_Proveedor` (`Id_Proveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `materiaPrima`
--
ALTER TABLE `materiaPrima`
  ADD CONSTRAINT `materiaPrima_Proveedor` FOREIGN KEY (`Id_Proveedor`) REFERENCES `proveedor` (`id`),
  ADD CONSTRAINT `materiaPrima_rubro` FOREIGN KEY (`id_rubro`) REFERENCES `rubro` (`id_rubro`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
