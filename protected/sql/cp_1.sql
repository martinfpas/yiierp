ALTER TABLE  `codigoPostal` ADD INDEX (  `zp` );


CREATE TABLE IF NOT EXISTS `camionero` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `Nro_documento` varchar(11) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Nombres` varchar(40) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Codigo_Postal` varchar(4) DEFAULT NULL,
  `Zona_Postal` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Nro_documento` (`Nro_documento`),
  KEY `Codigo_Postal` (`Codigo_Postal`),
  KEY `Zona_Postal` (`Zona_Postal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
ALTER TABLE `camionero`
  ADD CONSTRAINT `camionero_ibfk_1` FOREIGN KEY (`Codigo_Postal`) REFERENCES `codigoPostal` (`codigoPostal`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `camionero_ibfk_2` FOREIGN KEY (`Zona_Postal`) REFERENCES `codigoPostal` (`zp`) ON DELETE SET NULL ON UPDATE SET NULL;


CREATE TABLE IF NOT EXISTS `cliEmpresa` (
  `Id_Cliente` int(9) NOT NULL AUTO_INCREMENT,
  `Razon_Social` varchar(40) DEFAULT NULL,
  `Direccion` varchar(30) DEFAULT NULL,
  `Nombre_Fantasia` varchar(40) DEFAULT NULL,
  `Contacto` varchar(30) DEFAULT NULL,
  `CUIT` varchar(13) DEFAULT NULL,
  `Telefonos` varchar(40) DEFAULT NULL,
  `Piso_Departamento` varchar(30) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Fax` varchar(30) DEFAULT NULL,
  `Observaciones` text NOT NULL,
  `ID_Rubro` int(9) NOT NULL,
  `Direccion_Deposito` varchar(40) NOT NULL,
  PRIMARY KEY (`Id_Cliente`),
  KEY `ID_Rubro` (`ID_Rubro`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliEmpresa`
--
ALTER TABLE `cliEmpresa`
  ADD CONSTRAINT `cliEmpresa_ibfk_1` FOREIGN KEY (`ID_Rubro`) REFERENCES `rubroClientes` (`id_rubro`) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `clase` varchar(1) NOT NULL,
  `Nro_Puesto_Venta` int(3) NOT NULL,
  `Nro_Comprobante` varchar(8) NOT NULL,
  `Iva_Responsable` varchar(1) NOT NULL,
  `Nro_Viajante` int(2) NOT NULL,
  `Id_Cliente` int(7) NOT NULL,
  `Fecha_Factura` date DEFAULT NULL,
  `Moneda` varchar(8) DEFAULT NULL,
  `Total_Neto` float NOT NULL,
  `Observaciones` text NOT NULL,
  `Id_Camion` int(4) NOT NULL,
  `Descuento1` float DEFAULT NULL,
  `Descuento2` float DEFAULT NULL,
  `Total_Final` float NOT NULL,
  `Enviada` tinyint(1) NOT NULL DEFAULT '0',
  `Recibida` tinyint(1) NOT NULL DEFAULT '0',
  `Otro_Iva` float DEFAULT NULL,
  `CAE` varchar(14) NOT NULL DEFAULT '00000000000000',
  `Vencim_CAE` date NOT NULL DEFAULT '1970-01-01',
  `Ing_Brutos` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Id_Cliente` (`Id_Cliente`),
  KEY `Id_Camion` (`Id_Camion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`) ON DELETE CASCADE ON UPDATE CASCADE;