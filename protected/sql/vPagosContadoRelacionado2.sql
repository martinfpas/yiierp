-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 17-12-2019 a las 01:23:54
-- Versión del servidor: 5.7.28-0ubuntu0.18.04.4
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vPagosContadoRelacionado2`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vPagosContadoRelacionado2`  AS  select `pc`.`id` AS `id`,`pc`.`monto` AS `monto`,`pc`.`idPagodeCliente` AS `idPagodeCliente`,`c`.`id` AS `cid`,`ne`.`id` AS `neid`,(case when (`ne`.`idPagoRelacionado` is not null) then `ne`.`idPagoRelacionado` when (`c`.`idPagoRelacionado` is not null) then `c`.`idPagoRelacionado` else `pc`.`id` end) AS `idPagoRelacionado` from (((`pagoAlContado` `pc` join `pagoCliente` `p` on((`pc`.`idPagodeCliente` = `p`.`id`))) left join `comprobante` `c` on((`p`.`id_factura` = `c`.`id`))) left join `notaEntrega` `ne` on((`ne`.`id` = `p`.`idNotaEntrega`))) ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
