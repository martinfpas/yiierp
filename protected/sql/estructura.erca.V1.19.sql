-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 04-04-2019 a las 02:20:23
-- Versión del servidor: 5.7.25-0ubuntu0.18.04.2
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afipSubtotivas`
--

CREATE TABLE `afipSubtotivas` (
  `id` int(7) NOT NULL,
  `codigo` int(2) NOT NULL DEFAULT '5',
  `BaseImp` float NOT NULL,
  `importe` float NOT NULL,
  `idVoucher` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afipTributo`
--

CREATE TABLE `afipTributo` (
  `idTributo` int(7) NOT NULL,
  `Id` int(2) NOT NULL,
  `Desc` varchar(80) DEFAULT NULL,
  `BaseImp` float NOT NULL,
  `Importe` float NOT NULL,
  `Alic` float NOT NULL,
  `idVoucher` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afipVoucher`
--

CREATE TABLE `afipVoucher` (
  `id` int(5) NOT NULL,
  `numeroPuntoVenta` int(4) NOT NULL COMMENT 'PtoVta de facturacion',
  `codigoTipoComprobante` int(3) DEFAULT NULL COMMENT 'CbteTipo',
  `codigoTipoDocumento` int(2) DEFAULT NULL COMMENT 'DocTipo',
  `codigoConcepto` int(2) DEFAULT NULL COMMENT 'Concepto',
  `numeroDocumento` varchar(11) DEFAULT NULL COMMENT 'DocNro',
  `caea` varchar(14) DEFAULT NULL COMMENT 'caea',
  `fechaVencimientoCAE` date DEFAULT NULL,
  `numeroComprobante` int(8) DEFAULT NULL COMMENT 'CbteDesde',
  `fechaComprobante` varchar(8) NOT NULL COMMENT 'CbteFch',
  `importeOtrosTributos` float DEFAULT '0' COMMENT 'ImpTrib',
  `fechaDesde` varchar(8) DEFAULT NULL COMMENT 'FchServDesde',
  `fechaHasta` varchar(8) DEFAULT NULL COMMENT 'FchServHasta',
  `fechaVtoPago` varchar(8) DEFAULT NULL COMMENT 'FchVtoPago',
  `codigoMoneda` varchar(3) DEFAULT NULL COMMENT 'MonId',
  `cotizacionMoneda` float DEFAULT NULL COMMENT 'MonCotiz',
  `importeIVA` float NOT NULL COMMENT 'ImpIVA',
  `importeNoGravado` float NOT NULL COMMENT 'ImpTotConc',
  `importeExento` float DEFAULT '0' COMMENT 'ImpOpEx',
  `importeGravado` float NOT NULL COMMENT 'ImpNeto',
  `importeTotal` float NOT NULL COMMENT 'ImpTotal',
  `result` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afipVoucherItem`
--

CREATE TABLE `afipVoucherItem` (
  `id` int(6) NOT NULL,
  `idVoucher` int(5) NOT NULL,
  `codigo` varchar(14) DEFAULT NULL,
  `descripcion` varchar(40) NOT NULL,
  `cantidad` float NOT NULL DEFAULT '1',
  `codigoUnidadMedida` int(3) DEFAULT NULL,
  `precioUnitario` float NOT NULL,
  `impBonif` float DEFAULT NULL,
  `importeItem` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE `articulo` (
  `id` int(5) NOT NULL,
  `id_articulo` int(5) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `id_rubro` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articuloVenta`
--

CREATE TABLE `articuloVenta` (
  `id_articulo_vta` int(6) NOT NULL,
  `descripcion` varchar(70) NOT NULL,
  `id_presentacion` int(3) NOT NULL,
  `id_articulo` int(5) NOT NULL,
  `contenido` float NOT NULL,
  `uMedida` varchar(6) NOT NULL,
  `codBarra` varchar(60) DEFAULT NULL,
  `precio` float NOT NULL,
  `costo` float DEFAULT NULL,
  `enLista` tinyint(1) NOT NULL DEFAULT '1',
  `stockMinimo` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AuthAssignment`
--

CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AuthItem`
--

CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AuthItemChild`
--

CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE `banco` (
  `id` int(7) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `camionero`
--

CREATE TABLE `camionero` (
  `id` int(3) NOT NULL,
  `Nro_documento` varchar(11) NOT NULL,
  `Apellido` varchar(30) NOT NULL,
  `Nombres` varchar(40) DEFAULT NULL,
  `Domicilio` varchar(50) DEFAULT NULL,
  `Telefono` varchar(15) DEFAULT NULL,
  `Codigo_Postal` varchar(4) DEFAULT NULL,
  `Zona_Postal` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carga`
--

CREATE TABLE `carga` (
  `id` int(6) NOT NULL,
  `fecha` date NOT NULL,
  `idVehiculo` int(4) NOT NULL,
  `observacion` text,
  `estado` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargaDocumentos`
--

CREATE TABLE `cargaDocumentos` (
  `id` int(7) NOT NULL,
  `idEntregaDoc` int(9) NOT NULL,
  `fechaCarga` date NOT NULL,
  `tipoDoc` varchar(4) NOT NULL COMMENT 'NE, FAC, NC, ND',
  `idComprobante` int(7) NOT NULL,
  `totalFinal` float NOT NULL,
  `pagado` float NOT NULL DEFAULT '0',
  `efectivo` float NOT NULL DEFAULT '0',
  `cheque` float NOT NULL DEFAULT '0',
  `devolucion` float NOT NULL DEFAULT '0',
  `orden` int(9) NOT NULL,
  `notaCredito` float NOT NULL DEFAULT '0',
  `origen` varchar(8) NOT NULL DEFAULT 'ORIG',
  `comision` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoriaIva`
--

CREATE TABLE `categoriaIva` (
  `id_categoria` int(3) NOT NULL,
  `nombre` varchar(30) NOT NULL COMMENT 'Iva_Responsable',
  `descrip` varchar(30) NOT NULL COMMENT 'Descripcion',
  `discIva` tinyint(1) DEFAULT NULL COMMENT 'Discrima_Iva',
  `tipoFact` char(1) DEFAULT NULL COMMENT 'Clase',
  `fechaVigencia` date DEFAULT NULL COMMENT 'Fecha de vigencia del tipo de responsable',
  `nCopias` int(1) NOT NULL DEFAULT '0' COMMENT 'Cant_Copias',
  `catCorto` varchar(5) DEFAULT NULL COMMENT 'sigla',
  `alicuota` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequeDeTerceros`
--

CREATE TABLE `chequeDeTerceros` (
  `nroCheque` int(12) NOT NULL,
  `fecha` date DEFAULT NULL,
  `id_banco` int(7) DEFAULT NULL,
  `id_sucursal` int(7) DEFAULT NULL,
  `importe` float DEFAULT NULL,
  `id_cliente` int(7) NOT NULL,
  `propio` tinyint(1) DEFAULT NULL,
  `id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chequePropio`
--

CREATE TABLE `chequePropio` (
  `id` int(7) NOT NULL,
  `nroCheque` int(12) NOT NULL,
  `fecha` date NOT NULL,
  `id_cuenta` int(7) NOT NULL,
  `importe` float DEFAULT NULL,
  `fechaDebito` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliEmpresa`
--

CREATE TABLE `cliEmpresa` (
  `Id_Cliente` int(9) NOT NULL,
  `Razon_Social` varchar(40) DEFAULT NULL,
  `Direccion` varchar(30) DEFAULT NULL,
  `Nombre_Fantasia` varchar(40) DEFAULT NULL,
  `Contacto` varchar(30) DEFAULT NULL,
  `CUIT` varchar(13) DEFAULT NULL,
  `Telefonos` varchar(40) DEFAULT NULL,
  `Piso_Departamento` varchar(30) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `Fax` varchar(30) DEFAULT NULL,
  `Observaciones` text NOT NULL,
  `ID_Rubro` int(9) NOT NULL,
  `Direccion_Deposito` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(7) NOT NULL,
  `razonSocial` varchar(40) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `nombreFantasia` varchar(60) DEFAULT NULL,
  `contacto` varchar(50) DEFAULT NULL,
  `cuit` varchar(13) DEFAULT NULL,
  `telefonos` varchar(40) DEFAULT NULL,
  `PDpto` varchar(13) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `observaciones` text,
  `id_rubro` int(3) DEFAULT NULL,
  `direccionDeposito` varchar(60) DEFAULT NULL,
  `codViajante` int(2) DEFAULT NULL,
  `cp` varchar(4) NOT NULL,
  `zp` varchar(4) NOT NULL,
  `categoriaIva` int(3) DEFAULT NULL,
  `tipoCliente` varchar(2) DEFAULT NULL,
  `exeptuaIIB` tinyint(1) NOT NULL DEFAULT '0',
  `saldoActual` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientePersona`
--

CREATE TABLE `clientePersona` (
  `id` int(7) NOT NULL,
  `apellido` varchar(30) DEFAULT NULL,
  `nombres` varchar(30) DEFAULT NULL,
  `direccion` varchar(30) DEFAULT NULL,
  `tipoDocumento` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `telefonos` varchar(30) DEFAULT NULL,
  `piso` varchar(30) DEFAULT NULL,
  `observaciones` varchar(50) DEFAULT NULL,
  `cuil` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigoPostal`
--

CREATE TABLE `codigoPostal` (
  `codigoPostal` varchar(4) NOT NULL,
  `nombrelocalidad` varchar(65) NOT NULL DEFAULT '',
  `id_provincia` varchar(1) NOT NULL,
  `zp` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comisionViajante`
--

CREATE TABLE `comisionViajante` (
  `id` int(7) NOT NULL,
  `numeroViajante` int(2) NOT NULL,
  `fechaInicio` date NOT NULL,
  `fechaFinal` date NOT NULL,
  `porcentaje` float NOT NULL,
  `importeTotal` float NOT NULL,
  `fechaPago` date NOT NULL,
  `importePago` float NOT NULL,
  `modoPago` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobante`
--

CREATE TABLE `comprobante` (
  `id` int(7) NOT NULL,
  `tipo` varchar(2) NOT NULL DEFAULT 'F' COMMENT 'F - NC - ND',
  `clase` varchar(1) NOT NULL,
  `Nro_Puesto_Venta` int(3) NOT NULL DEFAULT '9',
  `Nro_Comprobante` varchar(8) DEFAULT NULL,
  `Iva_Responsable` int(3) NOT NULL,
  `Nro_Viajante` int(2) NOT NULL,
  `Id_Cliente` int(7) NOT NULL,
  `razonSocial` varchar(50) DEFAULT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `localidad` varchar(65) DEFAULT NULL,
  `provincia` varchar(40) DEFAULT NULL,
  `rubro` varchar(35) DEFAULT NULL,
  `zona` varchar(4) DEFAULT NULL,
  `tipoResponsable` varchar(30) DEFAULT NULL,
  `cuit` varchar(13) DEFAULT NULL,
  `perIIBB` float NOT NULL DEFAULT '0',
  `nroInscripIIBB` varchar(30) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `Moneda` varchar(8) DEFAULT 'PES',
  `Observaciones` text,
  `Id_Camion` int(4) NOT NULL,
  `Descuento1` float DEFAULT NULL,
  `Descuento2` float DEFAULT NULL,
  `Enviada` tinyint(1) NOT NULL DEFAULT '0',
  `Recibida` tinyint(1) NOT NULL DEFAULT '0',
  `Otro_Iva` float DEFAULT NULL,
  `CAE` varchar(14) NOT NULL DEFAULT '00000000000000',
  `Vencim_CAE` date DEFAULT NULL,
  `idNotaPedido` int(6) DEFAULT NULL,
  `idVoucherAfip` int(5) DEFAULT NULL,
  `alicuotaIva` float NOT NULL DEFAULT '21',
  `subtotalIva` float NOT NULL DEFAULT '0',
  `alicuotaIIBB` float DEFAULT '0',
  `subtotal` float NOT NULL DEFAULT '0',
  `Ing_Brutos` float DEFAULT NULL,
  `Total_Neto` float DEFAULT NULL,
  `Total_Final` float DEFAULT NULL,
  `estado` int(1) NOT NULL DEFAULT '0' COMMENT '0->borrador 1->guardado 2->cerrada 3->archivada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuentasBancarias`
--

CREATE TABLE `cuentasBancarias` (
  `id` int(7) NOT NULL,
  `id_banco` int(7) NOT NULL,
  `id_sucursal` int(7) NOT NULL,
  `observaciones` varchar(50) DEFAULT NULL,
  `codigoTipoCta` int(3) NOT NULL DEFAULT '1',
  `saldo` float DEFAULT NULL,
  `fechaSaldo` date DEFAULT NULL,
  `codigoMoneda` int(3) NOT NULL,
  `fechaApertura` date NOT NULL,
  `fechaCierre` date DEFAULT NULL,
  `cbu` int(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleFacturaProv`
--

CREATE TABLE `detalleFacturaProv` (
  `id` int(7) NOT NULL,
  `idArticulo` int(7) NOT NULL,
  `descripcion` varchar(8) NOT NULL,
  `fechaFactura` date NOT NULL,
  `precioUnitario` float NOT NULL,
  `cantidadRecibida` int(11) NOT NULL,
  `nroComprobante` int(11) NOT NULL,
  `nroPestoVta` varchar(1) NOT NULL,
  `clase` date NOT NULL,
  `nroItem` int(7) NOT NULL,
  `alicuota` int(3) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `idMateriaPrima` int(7) NOT NULL,
  `idFacturaProveedor` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleViaticos`
--

CREATE TABLE `detalleViaticos` (
  `id` int(7) NOT NULL,
  `numeroViatico` int(7) NOT NULL,
  `importe` int(7) DEFAULT NULL,
  `concepto` varchar(40) DEFAULT NULL,
  `fechaGasto` date DEFAULT NULL,
  `numeroItem` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(7) NOT NULL,
  `demominacion` varchar(20) NOT NULL,
  `nombreFantasia` varchar(50) DEFAULT NULL,
  `actividad` varchar(50) DEFAULT NULL,
  `publica` tinyint(1) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `email2` varchar(50) DEFAULT NULL,
  `calle` varchar(30) DEFAULT NULL,
  `numero` int(4) DEFAULT NULL,
  `codigoPostal` int(6) NOT NULL,
  `zonaPostal` int(6) NOT NULL,
  `paginaWeb` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entregaCheque`
--

CREATE TABLE `entregaCheque` (
  `id` int(7) NOT NULL,
  `id_carga_doc` int(7) NOT NULL,
  `id_cheque_3ro` int(9) DEFAULT NULL,
  `id_cheque_ppio` int(9) DEFAULT NULL,
  `nroItem` int(9) NOT NULL,
  `tipoDoc` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entregaDoc`
--

CREATE TABLE `entregaDoc` (
  `id` int(7) NOT NULL,
  `tipoPersona` varchar(4) NOT NULL DEFAULT 'CAM' COMMENT 'CAM,VIA',
  `nroCamViaj` int(7) NOT NULL,
  `observacion` varchar(400) DEFAULT NULL,
  `montoInicial` float DEFAULT NULL,
  `kmRecorridos` float DEFAULT '0',
  `litrosCargados` float NOT NULL DEFAULT '0',
  `otrosPagos` float NOT NULL DEFAULT '0',
  `cerrada` tinyint(1) NOT NULL DEFAULT '0',
  `otrasDevoluciones` float NOT NULL DEFAULT '0',
  `idCarga` int(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entregaGtos`
--

CREATE TABLE `entregaGtos` (
  `id` int(7) NOT NULL,
  `idEntregaDoc` int(9) NOT NULL,
  `gasto` varchar(20) NOT NULL,
  `montoGasto` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `envase`
--

CREATE TABLE `envase` (
  `id_envase` int(3) NOT NULL,
  `descripcion` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equivalenciaCot`
--

CREATE TABLE `equivalenciaCot` (
  `id` int(6) NOT NULL,
  `idArticuloVenta` int(6) NOT NULL,
  `codCot` varchar(6) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaProveedor`
--

CREATE TABLE `facturaProveedor` (
  `id` int(7) NOT NULL,
  `Nro_Comprobante` varchar(8) NOT NULL,
  `id_Proveedor` int(11) NOT NULL,
  `FechaFactura` int(11) NOT NULL,
  `Clase` date NOT NULL,
  `Nro_Puesto_Vta` varchar(1) NOT NULL,
  `Nograv` float NOT NULL DEFAULT '0',
  `TotalNeto` float NOT NULL DEFAULT '0',
  `MesCarga` date NOT NULL,
  `Subtotal` float NOT NULL DEFAULT '0',
  `Descuento` float NOT NULL DEFAULT '0',
  `Iva` float NOT NULL DEFAULT '0',
  `IngBrutos` float NOT NULL DEFAULT '0',
  `Rg3337` float NOT NULL DEFAULT '0',
  `FechaCarga` date NOT NULL,
  `id_Cuenta` int(7) NOT NULL,
  `PercepIB` float NOT NULL DEFAULT '0',
  `ImpGanancias` float NOT NULL DEFAULT '0',
  `TipoComprobante` int(2) NOT NULL,
  `Cai` varchar(15) NOT NULL,
  `Fecha_Vencimiento` date NOT NULL,
  `Cant_alicuotas_iva` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migra_chequeDe3Ro`
--

CREATE TABLE `migra_chequeDe3Ro` (
  `id` int(7) NOT NULL,
  `nroCheque` int(7) NOT NULL,
  `id_banco` int(7) NOT NULL,
  `id_sucursal` int(7) NOT NULL COMMENT 'el id original'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migra_Factura`
--

CREATE TABLE `migra_Factura` (
  `id` int(11) NOT NULL,
  `clase` varchar(1) NOT NULL,
  `nroPuestoVta` int(3) NOT NULL,
  `nroComprobante` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migra_sucursal`
--

CREATE TABLE `migra_sucursal` (
  `id` int(11) NOT NULL,
  `id_sucursal` int(11) NOT NULL,
  `id_banco` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moneda`
--

CREATE TABLE `moneda` (
  `codigo_moneda` int(3) NOT NULL,
  `nombre` varchar(20) NOT NULL,
  `sigla` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaCredito`
--

CREATE TABLE `notaCredito` (
  `id` int(7) NOT NULL,
  `clase` varchar(1) NOT NULL,
  `nroPuestoVta` int(3) NOT NULL,
  `nroComprobante` int(8) DEFAULT NULL,
  `ivaResponsable` int(2) NOT NULL,
  `nroViajante` int(2) NOT NULL,
  `idCliente` int(7) NOT NULL,
  `fecha` date DEFAULT NULL,
  `idCamion` int(4) NOT NULL,
  `totalFinal` float NOT NULL,
  `enviada` tinyint(1) NOT NULL DEFAULT '0',
  `recibida` tinyint(1) DEFAULT NULL,
  `observaciones` varchar(30) DEFAULT NULL,
  `otroIva` float DEFAULT NULL,
  `descuento1` float DEFAULT '0',
  `descuento2` float DEFAULT NULL,
  `totalNeto` float DEFAULT NULL,
  `cae` varchar(14) DEFAULT NULL,
  `vencimCae` datetime DEFAULT NULL,
  `ingBrutos` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaDebito`
--

CREATE TABLE `notaDebito` (
  `id` int(7) NOT NULL,
  `clase` varchar(1) NOT NULL,
  `Nro_Puesto_Venta` int(3) NOT NULL,
  `Nro_Comprobante` varchar(8) NOT NULL,
  `Iva_Responsable` int(3) NOT NULL,
  `Nro_Viajante` int(2) NOT NULL,
  `Id_Cliente` int(7) NOT NULL,
  `Fecha` date DEFAULT NULL,
  `Observaciones` text,
  `Id_Camion` int(4) NOT NULL,
  `Total_Final` float DEFAULT NULL,
  `Enviada` tinyint(1) NOT NULL DEFAULT '0',
  `Recibida` tinyint(1) NOT NULL DEFAULT '0',
  `id_factura` int(7) NOT NULL,
  `Descuento1` float DEFAULT NULL,
  `Descuento2` float DEFAULT NULL,
  `Total_Neto` float DEFAULT NULL,
  `Otro_Iva` float DEFAULT NULL,
  `CAE` varchar(14) NOT NULL DEFAULT '00000000000000',
  `idVoucherAfip` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaEntrega`
--

CREATE TABLE `notaEntrega` (
  `id` int(6) NOT NULL,
  `clase` varchar(1) NOT NULL,
  `nroPuestoVenta` int(3) NOT NULL DEFAULT '1',
  `nroComprobante` varchar(8) DEFAULT NULL,
  `idNotaPedido` int(6) DEFAULT NULL,
  `idCliente` int(7) NOT NULL,
  `idCamion` int(4) NOT NULL,
  `totalFinal` float NOT NULL DEFAULT '0',
  `enviada` tinyint(1) NOT NULL DEFAULT '0',
  `recibida` tinyint(1) NOT NULL DEFAULT '0',
  `fecha` date NOT NULL,
  `descuento1` float DEFAULT '0',
  `descuento2` float DEFAULT '0',
  `nroViajante` int(2) NOT NULL,
  `observaciones` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaPedido`
--

CREATE TABLE `notaPedido` (
  `id` int(6) NOT NULL,
  `clase` varchar(1) DEFAULT NULL,
  `nroCombrobante` varchar(6) NOT NULL,
  `dig` int(1) NOT NULL,
  `Nro_Viajante` int(2) DEFAULT NULL,
  `idCliente` int(7) NOT NULL,
  `camion` int(4) DEFAULT NULL COMMENT 'idVehiculo',
  `Orden_Carga` int(3) DEFAULT '1',
  `fecha` date NOT NULL,
  `idCarga` int(6) DEFAULT NULL,
  `impSinPrecio` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoAlContado`
--

CREATE TABLE `pagoAlContado` (
  `id` int(7) NOT NULL,
  `monto` float NOT NULL,
  `idPagodeCliente` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoCliente`
--

CREATE TABLE `pagoCliente` (
  `id` int(7) NOT NULL,
  `monto` float NOT NULL,
  `id_factura` int(7) DEFAULT NULL,
  `idNotaEntrega` int(6) DEFAULT NULL,
  `fecha` date NOT NULL,
  `idCliente` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoConCheque`
--

CREATE TABLE `pagoConCheque` (
  `id` int(7) NOT NULL,
  `monto` float NOT NULL,
  `idChequedeTercero` int(7) DEFAULT NULL,
  `idPagodeCliente` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paquete`
--

CREATE TABLE `paquete` (
  `id` int(11) NOT NULL,
  `id_artVenta` int(11) NOT NULL,
  `id_envase` int(11) NOT NULL,
  `cantidad` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `porcentajeIva`
--

CREATE TABLE `porcentajeIva` (
  `idImpuesto` int(2) NOT NULL,
  `Porcentaje` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodCarga`
--

CREATE TABLE `prodCarga` (
  `id` int(6) NOT NULL,
  `idCarga` int(6) NOT NULL,
  `idArtVenta` int(6) DEFAULT NULL,
  `idPaquete` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodComprobante`
--

CREATE TABLE `prodComprobante` (
  `id` int(9) NOT NULL,
  `idComprobante` int(7) NOT NULL,
  `nroItem` int(3) DEFAULT '0',
  `idArtVenta` int(6) NOT NULL,
  `cantidad` float NOT NULL DEFAULT '1',
  `fechaEntrega` date DEFAULT NULL,
  `precioUnitario` float NOT NULL DEFAULT '0',
  `alicuotaIva` float NOT NULL DEFAULT '21',
  `cantidadEntregada` int(11) DEFAULT NULL,
  `nroPuestoVenta` int(3) DEFAULT NULL,
  `descripcion` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodNotaCredito`
--

CREATE TABLE `prodNotaCredito` (
  `id` int(11) NOT NULL,
  `idNotaCredito` int(8) NOT NULL,
  `nroItem` int(2) NOT NULL,
  `idArticuloVta` int(6) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodNotaDebito`
--

CREATE TABLE `prodNotaDebito` (
  `id` int(11) NOT NULL,
  `idNotaDebito` int(8) NOT NULL,
  `nroItem` int(2) NOT NULL,
  `idArticuloVta` int(6) NOT NULL,
  `descripcion` varchar(30) DEFAULT NULL,
  `cantidad` float NOT NULL,
  `precio` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodNotaEntrega`
--

CREATE TABLE `prodNotaEntrega` (
  `id` int(7) NOT NULL,
  `nroItem` int(3) DEFAULT NULL,
  `idNotaEntrega` int(6) NOT NULL,
  `idArtVenta` int(6) NOT NULL,
  `precioUnitario` float NOT NULL,
  `cantidad` float NOT NULL,
  `descripcion` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prodNotaPedido`
--

CREATE TABLE `prodNotaPedido` (
  `id` int(7) NOT NULL,
  `idNotaPedido` int(6) NOT NULL,
  `idArtVenta` int(6) NOT NULL,
  `cantidad` float NOT NULL,
  `precioUnit` float NOT NULL,
  `costo` float DEFAULT NULL,
  `subTotal` float DEFAULT NULL,
  `idPaquete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id` int(4) NOT NULL,
  `nbreFantasia` varchar(50) NOT NULL,
  `contacto` varchar(80) DEFAULT NULL,
  `razonSocial` varchar(60) NOT NULL,
  `cuit` varchar(11) DEFAULT NULL,
  `ivaResponsable` int(3) NOT NULL,
  `direccion` varchar(60) DEFAULT NULL,
  `pisoDto` varchar(8) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `cp` varchar(4) DEFAULT NULL,
  `zp` varchar(4) DEFAULT NULL,
  `tel` varchar(35) DEFAULT NULL,
  `fax` varchar(35) DEFAULT NULL,
  `obs` text,
  `saldo` float DEFAULT NULL,
  `idTipo` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `id_provincia` varchar(1) NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puntoDeVenta`
--

CREATE TABLE `puntoDeVenta` (
  `num` int(2) NOT NULL,
  `descripcion` varchar(40) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubro`
--

CREATE TABLE `rubro` (
  `id_rubro` int(3) NOT NULL,
  `descripcion` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubroClientes`
--

CREATE TABLE `rubroClientes` (
  `id_rubro` int(3) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `Descripcion` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursalBanco`
--

CREATE TABLE `sucursalBanco` (
  `id` int(7) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `id_banco` int(7) NOT NULL,
  `localidad` varchar(60) DEFAULT NULL,
  `provincia` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoCuenta`
--

CREATE TABLE `tipoCuenta` (
  `id` int(7) NOT NULL,
  `nombre` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoTributoIibb`
--

CREATE TABLE `tipoTributoIibb` (
  `id` int(4) NOT NULL,
  `name` varchar(50) NOT NULL,
  `key` varchar(5) NOT NULL,
  `alicuota` float NOT NULL,
  `codProvincia` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='No modificar esta tabla sobre todo los tributo-id';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tributos`
--

CREATE TABLE `tributos` (
  `id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `udm`
--

CREATE TABLE `udm` (
  `id` varchar(8) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `kilosXudm` float NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL,
  `puntoDeVenta` int(2) DEFAULT '1',
  `password` varchar(250) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` timestamp NULL DEFAULT NULL,
  `user_type` varchar(64) DEFAULT 'vendedor',
  `telephone` varchar(100) DEFAULT NULL,
  `mobile` varchar(30) DEFAULT NULL,
  `address` text,
  `county` int(11) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `salt` varchar(250) DEFAULT NULL,
  `adress_number` int(6) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `salutation` varchar(10) DEFAULT NULL,
  `birthdate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vArtCotbyEc`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vArtCotbyEc` (
`id` int(6)
,`idCarga` int(6)
,`idArtVenta` int(6)
,`idPaquete` int(11)
,`cantidad` int(11)
,`codCot` varchar(6)
,`descripcion` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vArtCotbyPc`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vArtCotbyPc` (
`id` int(6)
,`idCarga` int(6)
,`idArtVenta` int(6)
,`idPaquete` int(11)
,`cantidad` int(11)
,`codCot` varchar(6)
,`descripcion` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vArticuloVenta`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vArticuloVenta` (
`id_articulo_vta` int(6)
,`descripcion` varchar(70)
,`id_presentacion` int(3)
,`id_articulo` int(5)
,`contenido` float
,`uMedida` varchar(6)
,`codBarra` varchar(60)
,`precio` float
,`costo` float
,`enLista` tinyint(1)
,`stockMinimo` float
,`id_rubro` int(3)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vCargaDocumentos`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vCargaDocumentos` (
`id` int(7)
,`idEntregaDoc` int(9)
,`idCliente` int(7)
,`razonSocial` varchar(40)
,`nombrelocalidad` varchar(65)
,`codigoPostal` varchar(4)
,`zp` varchar(4)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vCargaDocumentosCiudades`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vCargaDocumentosCiudades` (
`id` int(7)
,`idEntregaDoc` int(9)
,`idCliente` int(7)
,`razonSocial` varchar(40)
,`nombrelocalidad` varchar(65)
,`codigoPostal` varchar(4)
,`zp` varchar(4)
,`orden` int(9)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vCtaCteCliente`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vCtaCteCliente` (
`id` varchar(36)
,`tipo` varchar(2)
,`clId` int(11)
,`razonSocial` varchar(40)
,`fecha` date
,`debe` float
,`haber` float
,`neId` int(11)
,`faId` int(11)
,`pcId` int(11)
,`ncId` int(11)
,`ndId` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vDocumentosCarga`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vDocumentosCarga` (
`id` varchar(36)
,`idFa` int(11)
,`idNe` int(11)
,`idNotaPedido` int(11)
,`fecha` date
,`idNp` int(11)
,`idCarga` int(11)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `id_vehiculo` int(4) NOT NULL,
  `nroDocumento` varchar(11) DEFAULT NULL,
  `marca` varchar(40) DEFAULT NULL,
  `modelo` varchar(40) DEFAULT NULL,
  `patente` varchar(8) DEFAULT NULL,
  `tara` float DEFAULT NULL,
  `capMaxima` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viajante`
--

CREATE TABLE `viajante` (
  `numero` int(2) NOT NULL,
  `nroZona` int(4) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `tipoYdoc` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `viaticos`
--

CREATE TABLE `viaticos` (
  `id` int(7) NOT NULL,
  `efectivo` float DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `bonos` float DEFAULT NULL,
  `numeroViajante` int(2) NOT NULL,
  `tickets` float DEFAULT NULL,
  `cheques` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vPagosDeEntregaDoc`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `vPagosDeEntregaDoc` (
`idEntregaDoc` int(7)
,`idCargaDoc` int(7)
,`idFactura` int(7)
,`idNotaEntrega` int(6)
,`idPago` int(7)
,`idCliente` int(7)
,`monto` float
,`idChequedeTercero` int(7)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyEc`
--
DROP TABLE IF EXISTS `vArtCotbyEc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyEc`  AS  select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `ec`.`codCot` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vArtCotbyPc`
--
DROP TABLE IF EXISTS `vArtCotbyPc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArtCotbyPc`  AS  select `pc`.`id` AS `id`,`pc`.`idCarga` AS `idCarga`,`pc`.`idArtVenta` AS `idArtVenta`,`pc`.`idPaquete` AS `idPaquete`,`pc`.`cantidad` AS `cantidad`,`ec`.`codCot` AS `codCot`,`ec`.`descripcion` AS `descripcion` from (`prodCarga` `pc` left join `equivalenciaCot` `ec` on((`pc`.`idArtVenta` = `ec`.`idArticuloVenta`))) group by `pc`.`id` ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vArticuloVenta`
--
DROP TABLE IF EXISTS `vArticuloVenta`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vArticuloVenta`  AS  select `av`.`id_articulo_vta` AS `id_articulo_vta`,`av`.`descripcion` AS `descripcion`,`av`.`id_presentacion` AS `id_presentacion`,`a`.`id_articulo` AS `id_articulo`,`av`.`contenido` AS `contenido`,`av`.`uMedida` AS `uMedida`,`av`.`codBarra` AS `codBarra`,`av`.`precio` AS `precio`,`av`.`costo` AS `costo`,`av`.`enLista` AS `enLista`,`av`.`stockMinimo` AS `stockMinimo`,`a`.`id_rubro` AS `id_rubro` from (`articuloVenta` `av` left join `articulo` `a` on((`a`.`id` = `av`.`id_articulo`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vCargaDocumentos`
--
DROP TABLE IF EXISTS `vCargaDocumentos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCargaDocumentos`  AS  select `cd`.`id` AS `id`,`cd`.`idEntregaDoc` AS `idEntregaDoc`,`cl`.`id` AS `idCliente`,`cl`.`razonSocial` AS `razonSocial`,`cp`.`nombrelocalidad` AS `nombrelocalidad`,`cl`.`cp` AS `codigoPostal`,`cl`.`zp` AS `zp` from ((((`cargaDocumentos` `cd` left join `comprobante` `fa` on(((`cd`.`idComprobante` = `fa`.`id`) and (`cd`.`tipoDoc` = 'FAC')))) left join `notaEntrega` `ne` on(((`cd`.`idComprobante` = `ne`.`id`) and (`cd`.`tipoDoc` = 'NE')))) left join `cliente` `cl` on(((`cl`.`id` = `fa`.`Id_Cliente`) or (`cl`.`id` = `ne`.`idCliente`)))) left join `codigoPostal` `cp` on(((`cl`.`cp` = `cp`.`codigoPostal`) and (`cl`.`zp` = `cp`.`zp`)))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vCargaDocumentosCiudades`
--
DROP TABLE IF EXISTS `vCargaDocumentosCiudades`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCargaDocumentosCiudades`  AS  

select `cp`.`nombrelocalidad` AS `nombrelocalidad`,`cp`.`codigoPostal` AS `codigoPostal`,`cl`.`zp` AS `zp`,`cd`.`id` ,`cd`.`idEntregaDoc` AS `idEntregaDoc`

from ((((`cargaDocumentos` `cd` 
        left join `comprobante` `fa` on(((`cd`.`idComprobante` = `fa`.`id`) and (`cd`.`tipoDoc` = 'FAC')))) 
        left join `notaEntrega` `ne` on(((`cd`.`idComprobante` = `ne`.`id`) and (`cd`.`tipoDoc` = 'NE')))) 
        left join `cliente` `cl` on(((`cl`.`id` = `fa`.`Id_Cliente`) or (`cl`.`id` = `ne`.`idCliente`)))) 

      left join `codigoPostal` `cp` on(((`cl`.`cp` = `cp`.`codigoPostal`) and (`cl`.`zp` = `cp`.`zp`)))) 
      
      group by `cp`.`codigoPostal`,`cl`.`zp`,`cp`.`nombrelocalidad`,`cd`.`orden`,`cd`.`idEntregaDoc`,`cd`.`id` order by `cd`.`orden`

-- --------------------------------------------------------

--
-- Estructura para la vista `vCtaCteCliente`
--
DROP TABLE IF EXISTS `vCtaCteCliente`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vCtaCteCliente`  AS  select uuid() AS `id`,'NE' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`NE`.`fecha` AS `fecha`,`NE`.`totalFinal` AS `debe`,NULL AS `haber`,`NE`.`id` AS `neId`,NULL AS `faId`,NULL AS `pcId`,NULL AS `ncId`,NULL AS `ndId` from (`notaEntrega` `NE` left join `cliente` `CL` on((`CL`.`id` = `NE`.`idCliente`))) union select uuid() AS `id`,`CO`.`tipo` AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`CO`.`fecha` AS `fecha`,if((`CO`.`tipo` <> 'NC'),`CO`.`Total_Final`,NULL) AS `debe`,if((`CO`.`tipo` = 'NC'),`CO`.`Total_Final`,NULL) AS `haber`,NULL AS `neId`,if((`CO`.`tipo` = 'F'),`CO`.`id`,NULL) AS `faId`,NULL AS `pcId`,if((`CO`.`tipo` = 'NC'),`CO`.`id`,NULL) AS `ncId`,if((`CO`.`tipo` = 'ND'),`CO`.`id`,NULL) AS `ndId` from (`comprobante` `CO` left join `cliente` `CL` on((`CL`.`id` = `CO`.`Id_Cliente`))) union select uuid() AS `id`,'PC' AS `tipo`,`CL`.`id` AS `clId`,`CL`.`razonSocial` AS `razonSocial`,`PC`.`fecha` AS `fecha`,NULL AS `debe`,`PC`.`monto` AS `haber`,NULL AS `neId`,NULL AS `faId`,`PC`.`id` AS `pcId`,NULL AS `ncId`,NULL AS `ndId` from (`pagoCliente` `PC` left join `cliente` `CL` on((`CL`.`id` = `PC`.`idCliente`))) order by `fecha` desc ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vDocumentosCarga`
--
DROP TABLE IF EXISTS `vDocumentosCarga`;

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vDocumentosCarga`  AS  select uuid() AS `id`,`Fa`.`id` AS `idFa`,NULL AS `idNe`,`Fa`.`idNotaPedido` AS `idNotaPedido`,`Fa`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`comprobante` `Fa` left join `notaPedido` `Np` on((`Fa`.`idNotaPedido` = `Np`.`id`))) union select uuid() AS `id`,NULL AS `idFa`,`Ne`.`id` AS `idNe`,`Ne`.`idNotaPedido` AS `idNotaPedido`,`Ne`.`fecha` AS `fecha`,`Np`.`id` AS `idNp`,`Np`.`idCarga` AS `idCarga` from (`notaEntrega` `Ne` left join `notaPedido` `Np` on((`Ne`.`idNotaPedido` = `Np`.`id`))) order by `fecha` desc ;

-- --------------------------------------------------------

--
-- Estructura para la vista `vPagosDeEntregaDoc`
--
DROP TABLE IF EXISTS `vPagosDeEntregaDoc`;

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vPagosDeEntregaDoc`  AS  select `e`.`id` AS `idEntregaDoc`,`cd`.`id` AS `idCargaDoc`,`f`.`id` AS `idFactura`,`ne`.`id` AS `idNotaEntrega`,`pc`.`id` AS `idPago`,`pc`.`idCliente` AS `idCliente`,`pc`.`monto` AS `monto`,`pcch`.`idChequedeTercero` AS `idChequedeTercero` from (((((`entregaDoc` `e` left join `cargaDocumentos` `cd` on((`e`.`id` = `cd`.`idEntregaDoc`))) left join `comprobante` `f` on(((`cd`.`tipoDoc` = 'FAC') and (`cd`.`idComprobante` = `f`.`id`)))) left join `notaEntrega` `ne` on(((`cd`.`tipoDoc` = 'NE') and (`cd`.`idComprobante` = `ne`.`id`)))) left join `pagoCliente` `pc` on(((`pc`.`id_factura` = `f`.`id`) or (`pc`.`idNotaEntrega` = `ne`.`id`)))) left join `pagoConCheque` `pcch` on((`pcch`.`idPagodeCliente` = `pc`.`id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `afipSubtotivas`
--
ALTER TABLE `afipSubtotivas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idVoucher` (`idVoucher`);

--
-- Indices de la tabla `afipTributo`
--
ALTER TABLE `afipTributo`
  ADD PRIMARY KEY (`idTributo`),
  ADD KEY `idVoucher` (`idVoucher`);

--
-- Indices de la tabla `afipVoucher`
--
ALTER TABLE `afipVoucher`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `afipVoucherItem`
--
ALTER TABLE `afipVoucherItem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idVoucher` (`idVoucher`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_articulo` (`id_articulo`,`id_rubro`),
  ADD KEY `id_rubro` (`id_rubro`);

--
-- Indices de la tabla `articuloVenta`
--
ALTER TABLE `articuloVenta`
  ADD PRIMARY KEY (`id_articulo_vta`),
  ADD KEY `id_presentacion` (`id_presentacion`),
  ADD KEY `id_articulo` (`id_articulo`);

--
-- Indices de la tabla `AuthAssignment`
--
ALTER TABLE `AuthAssignment`
  ADD PRIMARY KEY (`itemname`,`userid`);

--
-- Indices de la tabla `AuthItem`
--
ALTER TABLE `AuthItem`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `AuthItemChild`
--
ALTER TABLE `AuthItemChild`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indices de la tabla `banco`
--
ALTER TABLE `banco`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `camionero`
--
ALTER TABLE `camionero`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Nro_documento` (`Nro_documento`),
  ADD KEY `Codigo_Postal` (`Codigo_Postal`),
  ADD KEY `Zona_Postal` (`Zona_Postal`);

--
-- Indices de la tabla `carga`
--
ALTER TABLE `carga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idVehiculo` (`idVehiculo`);

--
-- Indices de la tabla `cargaDocumentos`
--
ALTER TABLE `cargaDocumentos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idComprobante` (`idComprobante`),
  ADD KEY `idEntregaDoc` (`idEntregaDoc`);

--
-- Indices de la tabla `categoriaIva`
--
ALTER TABLE `categoriaIva`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `chequeDeTerceros`
--
ALTER TABLE `chequeDeTerceros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sucursal` (`id_sucursal`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `id_banco` (`id_banco`);

--
-- Indices de la tabla `chequePropio`
--
ALTER TABLE `chequePropio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_cuenta` (`id_cuenta`);

--
-- Indices de la tabla `cliEmpresa`
--
ALTER TABLE `cliEmpresa`
  ADD PRIMARY KEY (`Id_Cliente`),
  ADD KEY `ID_Rubro` (`ID_Rubro`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cp` (`cp`),
  ADD KEY `codViajante` (`codViajante`),
  ADD KEY `id_rubro` (`id_rubro`),
  ADD KEY `categoriaIva` (`categoriaIva`),
  ADD KEY `tipoCliente` (`tipoCliente`);

--
-- Indices de la tabla `clientePersona`
--
ALTER TABLE `clientePersona`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `codigoPostal`
--
ALTER TABLE `codigoPostal`
  ADD PRIMARY KEY (`codigoPostal`,`nombrelocalidad`),
  ADD KEY `id_provincia` (`id_provincia`),
  ADD KEY `zp` (`zp`);

--
-- Indices de la tabla `comisionViajante`
--
ALTER TABLE `comisionViajante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numeroViajante` (`numeroViajante`);

--
-- Indices de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_Cliente` (`Id_Cliente`),
  ADD KEY `Id_Camion` (`Id_Camion`),
  ADD KEY `Nro_Puesto_Venta` (`Nro_Puesto_Venta`),
  ADD KEY `idNotaPedido` (`idNotaPedido`),
  ADD KEY `idVoucherAfip` (`idVoucherAfip`),
  ADD KEY `Iva_Responsable` (`Iva_Responsable`);

--
-- Indices de la tabla `cuentasBancarias`
--
ALTER TABLE `cuentasBancarias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sucursal` (`id_sucursal`),
  ADD KEY `codigoTipoCta` (`codigoTipoCta`),
  ADD KEY `codigoMoneda` (`codigoMoneda`);

--
-- Indices de la tabla `detalleFacturaProv`
--
ALTER TABLE `detalleFacturaProv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idFacturaProveedor` (`idFacturaProveedor`);

--
-- Indices de la tabla `detalleViaticos`
--
ALTER TABLE `detalleViaticos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numeroViatico` (`numeroViatico`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entregaCheque`
--
ALTER TABLE `entregaCheque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_carga_doc` (`id_carga_doc`);

--
-- Indices de la tabla `entregaDoc`
--
ALTER TABLE `entregaDoc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCarga` (`idCarga`);

--
-- Indices de la tabla `entregaGtos`
--
ALTER TABLE `entregaGtos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEntregaDoc` (`idEntregaDoc`);

--
-- Indices de la tabla `envase`
--
ALTER TABLE `envase`
  ADD PRIMARY KEY (`id_envase`);

--
-- Indices de la tabla `equivalenciaCot`
--
ALTER TABLE `equivalenciaCot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArticuloVenta` (`idArticuloVenta`);

--
-- Indices de la tabla `facturaProveedor`
--
ALTER TABLE `facturaProveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_Proveedor` (`id_Proveedor`);

--
-- Indices de la tabla `migra_chequeDe3Ro`
--
ALTER TABLE `migra_chequeDe3Ro`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migra_Factura`
--
ALTER TABLE `migra_Factura`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migra_sucursal`
--
ALTER TABLE `migra_sucursal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sucursal` (`id_sucursal`),
  ADD KEY `id_banco` (`id_banco`);

--
-- Indices de la tabla `moneda`
--
ALTER TABLE `moneda`
  ADD PRIMARY KEY (`codigo_moneda`);

--
-- Indices de la tabla `notaCredito`
--
ALTER TABLE `notaCredito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ivaResponsable` (`ivaResponsable`),
  ADD KEY `nroViajante` (`nroViajante`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idCamion` (`idCamion`),
  ADD KEY `nroPuestoVta` (`nroPuestoVta`);

--
-- Indices de la tabla `notaDebito`
--
ALTER TABLE `notaDebito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Id_Cliente` (`Id_Cliente`),
  ADD KEY `Id_Camion` (`Id_Camion`),
  ADD KEY `Nro_Puesto_Venta` (`Nro_Puesto_Venta`),
  ADD KEY `idVoucherAfip` (`idVoucherAfip`),
  ADD KEY `Iva_Responsable` (`Iva_Responsable`),
  ADD KEY `Nro_Viajante` (`Nro_Viajante`),
  ADD KEY `id_factura` (`id_factura`);

--
-- Indices de la tabla `notaEntrega`
--
ALTER TABLE `notaEntrega`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idCamion` (`idCamion`),
  ADD KEY `nroViajante` (`nroViajante`),
  ADD KEY `nroPuestoVenta` (`nroPuestoVenta`),
  ADD KEY `idNotaPedido` (`idNotaPedido`);

--
-- Indices de la tabla `notaPedido`
--
ALTER TABLE `notaPedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `camion` (`camion`),
  ADD KEY `idCarga` (`idCarga`);

--
-- Indices de la tabla `pagoAlContado`
--
ALTER TABLE `pagoAlContado`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPagodeCliente` (`idPagodeCliente`);

--
-- Indices de la tabla `pagoCliente`
--
ALTER TABLE `pagoCliente`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_factura` (`id_factura`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idNotaEntrega` (`idNotaEntrega`);

--
-- Indices de la tabla `pagoConCheque`
--
ALTER TABLE `pagoConCheque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idChequedeTercero` (`idChequedeTercero`),
  ADD KEY `idPagodeCliente` (`idPagodeCliente`);

--
-- Indices de la tabla `paquete`
--
ALTER TABLE `paquete`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_artVenta` (`id_artVenta`),
  ADD KEY `id_envase` (`id_envase`);

--
-- Indices de la tabla `porcentajeIva`
--
ALTER TABLE `porcentajeIva`
  ADD PRIMARY KEY (`idImpuesto`);

--
-- Indices de la tabla `prodCarga`
--
ALTER TABLE `prodCarga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCarga` (`idCarga`),
  ADD KEY `idArtVenta` (`idArtVenta`),
  ADD KEY `idPaquete` (`idPaquete`);

--
-- Indices de la tabla `prodComprobante`
--
ALTER TABLE `prodComprobante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idFactura` (`idComprobante`),
  ADD KEY `idArtVenta` (`idArtVenta`),
  ADD KEY `nroPuestoVenta` (`nroPuestoVenta`);

--
-- Indices de la tabla `prodNotaCredito`
--
ALTER TABLE `prodNotaCredito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArticuloVta` (`idArticuloVta`),
  ADD KEY `idNotaCredito` (`idNotaCredito`);

--
-- Indices de la tabla `prodNotaDebito`
--
ALTER TABLE `prodNotaDebito`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idArticuloVta` (`idArticuloVta`),
  ADD KEY `idNotaDebito` (`idNotaDebito`);

--
-- Indices de la tabla `prodNotaEntrega`
--
ALTER TABLE `prodNotaEntrega`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idNotaEntrega` (`idNotaEntrega`),
  ADD KEY `idArtVenta` (`idArtVenta`);

--
-- Indices de la tabla `prodNotaPedido`
--
ALTER TABLE `prodNotaPedido`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idNotaPedido` (`idNotaPedido`),
  ADD KEY `idArtVenta` (`idArtVenta`),
  ADD KEY `idPaquete` (`idPaquete`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ivaResponsable` (`ivaResponsable`),
  ADD KEY `cp` (`cp`),
  ADD KEY `zp` (`zp`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`id_provincia`);

--
-- Indices de la tabla `puntoDeVenta`
--
ALTER TABLE `puntoDeVenta`
  ADD PRIMARY KEY (`num`);

--
-- Indices de la tabla `rubro`
--
ALTER TABLE `rubro`
  ADD PRIMARY KEY (`id_rubro`);

--
-- Indices de la tabla `rubroClientes`
--
ALTER TABLE `rubroClientes`
  ADD PRIMARY KEY (`id_rubro`);

--
-- Indices de la tabla `sucursalBanco`
--
ALTER TABLE `sucursalBanco`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_banco` (`id_banco`),
  ADD KEY `provincia` (`provincia`);

--
-- Indices de la tabla `tipoCuenta`
--
ALTER TABLE `tipoCuenta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipoTributoIibb`
--
ALTER TABLE `tipoTributoIibb`
  ADD PRIMARY KEY (`id`),
  ADD KEY `codProvincia` (`codProvincia`);

--
-- Indices de la tabla `tributos`
--
ALTER TABLE `tributos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `udm`
--
ALTER TABLE `udm`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `city` (`city`),
  ADD KEY `county` (`county`),
  ADD KEY `country` (`country`),
  ADD KEY `puntoDeVenta` (`puntoDeVenta`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`id_vehiculo`);

--
-- Indices de la tabla `viajante`
--
ALTER TABLE `viajante`
  ADD PRIMARY KEY (`numero`);

--
-- Indices de la tabla `viaticos`
--
ALTER TABLE `viaticos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `numeroViajante` (`numeroViajante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `afipSubtotivas`
--
ALTER TABLE `afipSubtotivas`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT de la tabla `afipTributo`
--
ALTER TABLE `afipTributo`
  MODIFY `idTributo` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `afipVoucher`
--
ALTER TABLE `afipVoucher`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;
--
-- AUTO_INCREMENT de la tabla `afipVoucherItem`
--
ALTER TABLE `afipVoucherItem`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=285;
--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=318;
--
-- AUTO_INCREMENT de la tabla `articuloVenta`
--
ALTER TABLE `articuloVenta`
  MODIFY `id_articulo_vta` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2843;
--
-- AUTO_INCREMENT de la tabla `banco`
--
ALTER TABLE `banco`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=326;
--
-- AUTO_INCREMENT de la tabla `camionero`
--
ALTER TABLE `camionero`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `carga`
--
ALTER TABLE `carga`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `cargaDocumentos`
--
ALTER TABLE `cargaDocumentos`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `categoriaIva`
--
ALTER TABLE `categoriaIva`
  MODIFY `id_categoria` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `chequeDeTerceros`
--
ALTER TABLE `chequeDeTerceros`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4936;
--
-- AUTO_INCREMENT de la tabla `chequePropio`
--
ALTER TABLE `chequePropio`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliEmpresa`
--
ALTER TABLE `cliEmpresa`
  MODIFY `Id_Cliente` int(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88889;
--
-- AUTO_INCREMENT de la tabla `clientePersona`
--
ALTER TABLE `clientePersona`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comisionViajante`
--
ALTER TABLE `comisionViajante`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `comprobante`
--
ALTER TABLE `comprobante`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;
--
-- AUTO_INCREMENT de la tabla `cuentasBancarias`
--
ALTER TABLE `cuentasBancarias`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalleFacturaProv`
--
ALTER TABLE `detalleFacturaProv`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `detalleViaticos`
--
ALTER TABLE `detalleViaticos`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `entregaCheque`
--
ALTER TABLE `entregaCheque`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `entregaDoc`
--
ALTER TABLE `entregaDoc`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `entregaGtos`
--
ALTER TABLE `entregaGtos`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `envase`
--
ALTER TABLE `envase`
  MODIFY `id_envase` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `equivalenciaCot`
--
ALTER TABLE `equivalenciaCot`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1253;
--
-- AUTO_INCREMENT de la tabla `facturaProveedor`
--
ALTER TABLE `facturaProveedor`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `moneda`
--
ALTER TABLE `moneda`
  MODIFY `codigo_moneda` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `notaCredito`
--
ALTER TABLE `notaCredito`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `notaDebito`
--
ALTER TABLE `notaDebito`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `notaEntrega`
--
ALTER TABLE `notaEntrega`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `notaPedido`
--
ALTER TABLE `notaPedido`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT de la tabla `pagoAlContado`
--
ALTER TABLE `pagoAlContado`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `pagoCliente`
--
ALTER TABLE `pagoCliente`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `pagoConCheque`
--
ALTER TABLE `pagoConCheque`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `paquete`
--
ALTER TABLE `paquete`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2006;
--
-- AUTO_INCREMENT de la tabla `prodCarga`
--
ALTER TABLE `prodCarga`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=522;
--
-- AUTO_INCREMENT de la tabla `prodComprobante`
--
ALTER TABLE `prodComprobante`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT de la tabla `prodNotaCredito`
--
ALTER TABLE `prodNotaCredito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prodNotaDebito`
--
ALTER TABLE `prodNotaDebito`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `prodNotaEntrega`
--
ALTER TABLE `prodNotaEntrega`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT de la tabla `prodNotaPedido`
--
ALTER TABLE `prodNotaPedido`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;
--
-- AUTO_INCREMENT de la tabla `rubro`
--
ALTER TABLE `rubro`
  MODIFY `id_rubro` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `rubroClientes`
--
ALTER TABLE `rubroClientes`
  MODIFY `id_rubro` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT de la tabla `sucursalBanco`
--
ALTER TABLE `sucursalBanco`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1264;
--
-- AUTO_INCREMENT de la tabla `tipoCuenta`
--
ALTER TABLE `tipoCuenta`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipoTributoIibb`
--
ALTER TABLE `tipoTributoIibb`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tributos`
--
ALTER TABLE `tributos`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `viaticos`
--
ALTER TABLE `viaticos`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `afipSubtotivas`
--
ALTER TABLE `afipSubtotivas`
  ADD CONSTRAINT `afipSubtotivas_ibfk_3` FOREIGN KEY (`idVoucher`) REFERENCES `afipVoucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `afipTributo`
--
ALTER TABLE `afipTributo`
  ADD CONSTRAINT `afipTributo_ibfk_3` FOREIGN KEY (`idVoucher`) REFERENCES `afipVoucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `afipVoucherItem`
--
ALTER TABLE `afipVoucherItem`
  ADD CONSTRAINT `afipVoucherItem_ibfk_3` FOREIGN KEY (`idVoucher`) REFERENCES `afipVoucher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD CONSTRAINT `articulo_ibfk_2` FOREIGN KEY (`id_rubro`) REFERENCES `rubro` (`id_rubro`);

--
-- Filtros para la tabla `articuloVenta`
--
ALTER TABLE `articuloVenta`
  ADD CONSTRAINT `articuloVenta_ibfk_3` FOREIGN KEY (`id_articulo`) REFERENCES `articulo` (`id`);

--
-- Filtros para la tabla `AuthAssignment`
--
ALTER TABLE `AuthAssignment`
  ADD CONSTRAINT `AuthAssignment_ibfk_2` FOREIGN KEY (`itemname`) REFERENCES `AuthItem` (`name`);

--
-- Filtros para la tabla `AuthItemChild`
--
ALTER TABLE `AuthItemChild`
  ADD CONSTRAINT `AuthItemChild_ibfk_3` FOREIGN KEY (`parent`) REFERENCES `AuthItem` (`name`),
  ADD CONSTRAINT `AuthItemChild_ibfk_4` FOREIGN KEY (`child`) REFERENCES `AuthItem` (`name`);

--
-- Filtros para la tabla `camionero`
--
ALTER TABLE `camionero`
  ADD CONSTRAINT `camionero_ibfk_3` FOREIGN KEY (`Codigo_Postal`) REFERENCES `codigoPostal` (`codigoPostal`),
  ADD CONSTRAINT `camionero_ibfk_4` FOREIGN KEY (`Zona_Postal`) REFERENCES `codigoPostal` (`zp`);

--
-- Filtros para la tabla `carga`
--
ALTER TABLE `carga`
  ADD CONSTRAINT `carga_ibfk_2` FOREIGN KEY (`idVehiculo`) REFERENCES `vehiculo` (`id_vehiculo`);

--
-- Filtros para la tabla `cargaDocumentos`
--
ALTER TABLE `cargaDocumentos`
  ADD CONSTRAINT `cargaDocumentos_ibfk_1` FOREIGN KEY (`idEntregaDoc`) REFERENCES `entregaDoc` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `chequeDeTerceros`
--
ALTER TABLE `chequeDeTerceros`
  ADD CONSTRAINT `chequeDeTerceros_ibfk_2` FOREIGN KEY (`id_banco`) REFERENCES `banco` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `chequeDeTerceros_ibfk_3` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursalBanco` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `chequePropio`
--
ALTER TABLE `chequePropio`
  ADD CONSTRAINT `chequePropio_ibfk_2` FOREIGN KEY (`id_cuenta`) REFERENCES `cuentasBancarias` (`id`);

--
-- Filtros para la tabla `cliEmpresa`
--
ALTER TABLE `cliEmpresa`
  ADD CONSTRAINT `cliEmpresa_ibfk_2` FOREIGN KEY (`ID_Rubro`) REFERENCES `rubroClientes` (`id_rubro`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_6` FOREIGN KEY (`id_rubro`) REFERENCES `rubroClientes` (`id_rubro`),
  ADD CONSTRAINT `cliente_ibfk_7` FOREIGN KEY (`codViajante`) REFERENCES `viajante` (`numero`),
  ADD CONSTRAINT `cliente_ibfk_8` FOREIGN KEY (`cp`) REFERENCES `codigoPostal` (`codigoPostal`),
  ADD CONSTRAINT `cliente_ibfk_9` FOREIGN KEY (`categoriaIva`) REFERENCES `categoriaIva` (`id_categoria`);

--
-- Filtros para la tabla `codigoPostal`
--
ALTER TABLE `codigoPostal`
  ADD CONSTRAINT `codigoPostal_ibfk_2` FOREIGN KEY (`id_provincia`) REFERENCES `provincia` (`id_provincia`);

--
-- Filtros para la tabla `comisionViajante`
--
ALTER TABLE `comisionViajante`
  ADD CONSTRAINT `comisionViajante_ibfk_1` FOREIGN KEY (`numeroViajante`) REFERENCES `viajante` (`numero`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `comprobante`
--
ALTER TABLE `comprobante`
  ADD CONSTRAINT `comprobante_ibfk_10` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`),
  ADD CONSTRAINT `comprobante_ibfk_11` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido` (`id`),
  ADD CONSTRAINT `comprobante_ibfk_13` FOREIGN KEY (`idVoucherAfip`) REFERENCES `afipVoucher` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `comprobante_ibfk_7` FOREIGN KEY (`Nro_Puesto_Venta`) REFERENCES `puntoDeVenta` (`num`),
  ADD CONSTRAINT `comprobante_ibfk_8` FOREIGN KEY (`Iva_Responsable`) REFERENCES `categoriaIva` (`id_categoria`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `comprobante_ibfk_9` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `cuentasBancarias`
--
ALTER TABLE `cuentasBancarias`
  ADD CONSTRAINT `cuentasBancarias_ibfk_5` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursalBanco` (`id`),
  ADD CONSTRAINT `cuentasBancarias_ibfk_6` FOREIGN KEY (`codigoTipoCta`) REFERENCES `tipoCuenta` (`id`),
  ADD CONSTRAINT `cuentasBancarias_ibfk_7` FOREIGN KEY (`codigoMoneda`) REFERENCES `moneda` (`codigo_moneda`);

--
-- Filtros para la tabla `detalleFacturaProv`
--
ALTER TABLE `detalleFacturaProv`
  ADD CONSTRAINT `detalleFacturaProv_ibfk_2` FOREIGN KEY (`idFacturaProveedor`) REFERENCES `facturaProveedor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalleViaticos`
--
ALTER TABLE `detalleViaticos`
  ADD CONSTRAINT `detalleViaticos_ibfk_2` FOREIGN KEY (`numeroViatico`) REFERENCES `viaticos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `entregaCheque`
--
ALTER TABLE `entregaCheque`
  ADD CONSTRAINT `entregaCheque_ibfk_1` FOREIGN KEY (`id_carga_doc`) REFERENCES `entregaDoc` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `entregaDoc`
--
ALTER TABLE `entregaDoc`
  ADD CONSTRAINT `entregaDoc_ibfk_2` FOREIGN KEY (`idCarga`) REFERENCES `carga` (`id`);

--
-- Filtros para la tabla `entregaGtos`
--
ALTER TABLE `entregaGtos`
  ADD CONSTRAINT `entregaGtos_ibfk_3` FOREIGN KEY (`idEntregaDoc`) REFERENCES `entregaDoc` (`id`);

--
-- Filtros para la tabla `equivalenciaCot`
--
ALTER TABLE `equivalenciaCot`
  ADD CONSTRAINT `equivalenciaCot_ibfk_3` FOREIGN KEY (`idArticuloVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`);

--
-- Filtros para la tabla `facturaProveedor`
--
ALTER TABLE `facturaProveedor`
  ADD CONSTRAINT `facturaProveedor_ibfk_2` FOREIGN KEY (`id_Proveedor`) REFERENCES `proveedor` (`id`);

--
-- Filtros para la tabla `migra_chequeDe3Ro`
--
ALTER TABLE `migra_chequeDe3Ro`
  ADD CONSTRAINT `migra_chequeDe3Ro_ibfk_1` FOREIGN KEY (`id`) REFERENCES `chequeDeTerceros` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `migra_sucursal`
--
ALTER TABLE `migra_sucursal`
  ADD CONSTRAINT `migra_sucursal_ibfk_1` FOREIGN KEY (`id`) REFERENCES `sucursalBanco` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notaCredito`
--
ALTER TABLE `notaCredito`
  ADD CONSTRAINT `notaCredito_ibfk_10` FOREIGN KEY (`idCamion`) REFERENCES `vehiculo` (`id_vehiculo`),
  ADD CONSTRAINT `notaCredito_ibfk_6` FOREIGN KEY (`nroPuestoVta`) REFERENCES `puntoDeVenta` (`num`),
  ADD CONSTRAINT `notaCredito_ibfk_7` FOREIGN KEY (`ivaResponsable`) REFERENCES `categoriaIva` (`id_categoria`),
  ADD CONSTRAINT `notaCredito_ibfk_8` FOREIGN KEY (`nroViajante`) REFERENCES `viajante` (`numero`),
  ADD CONSTRAINT `notaCredito_ibfk_9` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `notaDebito`
--
ALTER TABLE `notaDebito`
  ADD CONSTRAINT `notaDebito_ibfk_10` FOREIGN KEY (`Nro_Viajante`) REFERENCES `viajante` (`numero`),
  ADD CONSTRAINT `notaDebito_ibfk_11` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `notaDebito_ibfk_12` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`),
  ADD CONSTRAINT `notaDebito_ibfk_13` FOREIGN KEY (`id_factura`) REFERENCES `comprobante` (`id`),
  ADD CONSTRAINT `notaDebito_ibfk_14` FOREIGN KEY (`idVoucherAfip`) REFERENCES `afipVoucher` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `notaDebito_ibfk_8` FOREIGN KEY (`Nro_Puesto_Venta`) REFERENCES `puntoDeVenta` (`num`),
  ADD CONSTRAINT `notaDebito_ibfk_9` FOREIGN KEY (`Iva_Responsable`) REFERENCES `categoriaIva` (`id_categoria`);

--
-- Filtros para la tabla `notaEntrega`
--
ALTER TABLE `notaEntrega`
  ADD CONSTRAINT `notaEntrega_ibfk_10` FOREIGN KEY (`nroViajante`) REFERENCES `viajante` (`numero`),
  ADD CONSTRAINT `notaEntrega_ibfk_6` FOREIGN KEY (`nroPuestoVenta`) REFERENCES `puntoDeVenta` (`num`),
  ADD CONSTRAINT `notaEntrega_ibfk_7` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido` (`id`),
  ADD CONSTRAINT `notaEntrega_ibfk_8` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `notaEntrega_ibfk_9` FOREIGN KEY (`idCamion`) REFERENCES `vehiculo` (`id_vehiculo`);

--
-- Filtros para la tabla `notaPedido`
--
ALTER TABLE `notaPedido`
  ADD CONSTRAINT `notaPedido_ibfk_3` FOREIGN KEY (`idCarga`) REFERENCES `carga` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `notaPedido_ibfk_4` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `notaPedido_ibfk_5` FOREIGN KEY (`camion`) REFERENCES `vehiculo` (`id_vehiculo`);

--
-- Filtros para la tabla `pagoAlContado`
--
ALTER TABLE `pagoAlContado`
  ADD CONSTRAINT `pagoAlContado_ibfk_2` FOREIGN KEY (`idPagodeCliente`) REFERENCES `pagoCliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `pagoCliente`
--
ALTER TABLE `pagoCliente`
  ADD CONSTRAINT `pagoCliente_ibfk_4` FOREIGN KEY (`id_factura`) REFERENCES `comprobante` (`id`),
  ADD CONSTRAINT `pagoCliente_ibfk_5` FOREIGN KEY (`idNotaEntrega`) REFERENCES `notaEntrega` (`id`),
  ADD CONSTRAINT `pagoCliente_ibfk_6` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `pagoConCheque`
--
ALTER TABLE `pagoConCheque`
  ADD CONSTRAINT `pagoConCheque_ibfk_2` FOREIGN KEY (`idPagodeCliente`) REFERENCES `pagoCliente` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pagoConCheque_ibfk_3` FOREIGN KEY (`idChequedeTercero`) REFERENCES `chequeDeTerceros` (`id`);

--
-- Filtros para la tabla `paquete`
--
ALTER TABLE `paquete`
  ADD CONSTRAINT `paquete_ibfk_3` FOREIGN KEY (`id_artVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `paquete_ibfk_4` FOREIGN KEY (`id_envase`) REFERENCES `envase` (`id_envase`);

--
-- Filtros para la tabla `prodCarga`
--
ALTER TABLE `prodCarga`
  ADD CONSTRAINT `prodCarga_ibfk_1` FOREIGN KEY (`idCarga`) REFERENCES `carga` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodCarga_ibfk_4` FOREIGN KEY (`idArtVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`),
  ADD CONSTRAINT `prodCarga_ibfk_5` FOREIGN KEY (`idPaquete`) REFERENCES `paquete` (`id`);

--
-- Filtros para la tabla `prodComprobante`
--
ALTER TABLE `prodComprobante`
  ADD CONSTRAINT `prodComprobante_ibfk_1` FOREIGN KEY (`idComprobante`) REFERENCES `comprobante` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodComprobante_ibfk_4` FOREIGN KEY (`idArtVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`),
  ADD CONSTRAINT `prodComprobante_ibfk_5` FOREIGN KEY (`nroPuestoVenta`) REFERENCES `puntoDeVenta` (`num`);

--
-- Filtros para la tabla `prodNotaCredito`
--
ALTER TABLE `prodNotaCredito`
  ADD CONSTRAINT `prodNotaCredito_ibfk_3` FOREIGN KEY (`idNotaCredito`) REFERENCES `notaCredito` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaCredito_ibfk_4` FOREIGN KEY (`idArticuloVta`) REFERENCES `articuloVenta` (`id_articulo_vta`);

--
-- Filtros para la tabla `prodNotaDebito`
--
ALTER TABLE `prodNotaDebito`
  ADD CONSTRAINT `prodNotaDebito_ibfk_1` FOREIGN KEY (`idNotaDebito`) REFERENCES `notaDebito` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaDebito_ibfk_3` FOREIGN KEY (`idArticuloVta`) REFERENCES `articuloVenta` (`id_articulo_vta`);

--
-- Filtros para la tabla `prodNotaEntrega`
--
ALTER TABLE `prodNotaEntrega`
  ADD CONSTRAINT `prodNotaEntrega_ibfk_1` FOREIGN KEY (`idNotaEntrega`) REFERENCES `notaEntrega` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaEntrega_ibfk_2` FOREIGN KEY (`idArtVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `prodNotaPedido`
--
ALTER TABLE `prodNotaPedido`
  ADD CONSTRAINT `prodNotaPedido_ibfk_1` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaPedido_ibfk_2` FOREIGN KEY (`idArtVenta`) REFERENCES `articuloVenta` (`id_articulo_vta`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prodNotaPedido_ibfk_3` FOREIGN KEY (`idPaquete`) REFERENCES `paquete` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD CONSTRAINT `proveedor_ibfk_2` FOREIGN KEY (`ivaResponsable`) REFERENCES `categoriaIva` (`id_categoria`);

--
-- Filtros para la tabla `sucursalBanco`
--
ALTER TABLE `sucursalBanco`
  ADD CONSTRAINT `sucursalBanco_ibfk_3` FOREIGN KEY (`id_banco`) REFERENCES `banco` (`id`),
  ADD CONSTRAINT `sucursalBanco_ibfk_4` FOREIGN KEY (`provincia`) REFERENCES `provincia` (`id_provincia`) ON DELETE NO ACTION;

--
-- Filtros para la tabla `tipoTributoIibb`
--
ALTER TABLE `tipoTributoIibb`
  ADD CONSTRAINT `tipoTributoIibb_ibfk_1` FOREIGN KEY (`codProvincia`) REFERENCES `provincia` (`id_provincia`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`puntoDeVenta`) REFERENCES `puntoDeVenta` (`num`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Filtros para la tabla `viaticos`
--
ALTER TABLE `viaticos`
  ADD CONSTRAINT `viaticos_ibfk_2` FOREIGN KEY (`numeroViajante`) REFERENCES `viajante` (`numero`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
