-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 19-04-2019 a las 14:11:21
-- Versión del servidor: 5.7.25-0ubuntu0.18.04.2
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vCargaDocumentosCiudades`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vCargaDocumentosCiudades`  AS  select `cp`.`nombrelocalidad` AS `nombrelocalidad`,`cp`.`codigoPostal` AS `codigoPostal`,`cl`.`zp` AS `zp`,`cd`.`idEntregaDoc` AS `idEntregaDoc` from ((((`cargaDocumentos` `cd` left join `comprobante` `fa` on(((`cd`.`idComprobante` = `fa`.`id`) and (`cd`.`tipoDoc` = 'FAC')))) left join `notaEntrega` `ne` on(((`cd`.`idComprobante` = `ne`.`id`) and (`cd`.`tipoDoc` = 'NE')))) left join `cliente` `cl` on(((`cl`.`id` = `fa`.`Id_Cliente`) or (`cl`.`id` = `ne`.`idCliente`)))) left join `codigoPostal` `cp` on(((`cl`.`cp` = `cp`.`codigoPostal`) and (`cl`.`zp` = `cp`.`zp`)))) group by `cp`.`nombrelocalidad`,`cp`.`codigoPostal`,`cl`.`zp`,`cd`.`idEntregaDoc` order by `cp`.`nombrelocalidad` ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
