CREATE
 ALGORITHM = UNDEFINED
 VIEW `vArtCotbyDep`
 AS select `pd`.`idDepuracion` AS `idDepuracion`,`ec`.`codCot` AS `codCot`,`pd`.`idArtVenta` AS `idArtVenta`,`ec`.`descripcion` AS `descripcion`,`pd`.`cantidad` AS `cantidad`,`av`.`contenido` AS `contenido`,concat(`pd`.`idDepuracion`,'-',`pd`.`idArtVenta`) AS `id` 
from ((`prodDepuracion` `pd` left join `equivalenciaCot` `ec` on((`pd`.`idArtVenta` = `ec`.`idArticuloVenta`))) 
left join `articuloVenta` `av` on((`pd`.`idArtVenta` = `av`.`id_articulo_vta`))) 
group by `pd`.`idDepuracion`,`ec`.`codCot`,`ec`.`descripcion`,`pd`.`idArtVenta`,`av`.`contenido`,`pd`.`cantidad` order by `ec`.`descripcion`