-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 09-11-2019 a las 11:37:37
-- Versión del servidor: 5.7.27-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago2FactProv`
--

CREATE TABLE `pago2FactProv` (
  `id` int(7) NOT NULL,
  `idFacturaProveedor` int(7) NOT NULL,
  `idPagoProveedor` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvCheque`
--

CREATE TABLE `pagoProvCheque` (
  `id` int(7) NOT NULL,
  `idCheque3ro` int(11) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `idPagoProv` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProveedor`
--

CREATE TABLE `pagoProveedor` (
  `id` int(7) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00',
  `fecha` date NOT NULL,
  `idProveedor` int(4) NOT NULL,
  `idPago2Factura` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvEfectivo`
--

CREATE TABLE `pagoProvEfectivo` (
  `id` int(7) NOT NULL,
  `idPagoProveedor` int(7) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagoProvMovimientoBanc`
--

CREATE TABLE `pagoProvMovimientoBanc` (
  `id` int(7) NOT NULL,
  `idMovBanc` int(7) NOT NULL,
  `idPagoProveedor` int(7) NOT NULL,
  `monto` float(8,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pago2FactProv`
--
ALTER TABLE `pago2FactProv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idFacturaProveedor` (`idFacturaProveedor`),
  ADD KEY `idPagoProveedor` (`idPagoProveedor`);

--
-- Indices de la tabla `pagoProvCheque`
--
ALTER TABLE `pagoProvCheque`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cheque3ro` (`idCheque3ro`),
  ADD KEY `pagoProveedor` (`idPagoProv`);

--
-- Indices de la tabla `pagoProveedor`
--
ALTER TABLE `pagoProveedor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idProveedor` (`idProveedor`),
  ADD KEY `idPago2Factura` (`idPago2Factura`);

--
-- Indices de la tabla `pagoProvEfectivo`
--
ALTER TABLE `pagoProvEfectivo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pagoProv` (`idPagoProveedor`);

--
-- Indices de la tabla `pagoProvMovimientoBanc`
--
ALTER TABLE `pagoProvMovimientoBanc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `movBanc` (`idMovBanc`),
  ADD KEY `pagoProv` (`idPagoProveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pago2FactProv`
--
ALTER TABLE `pago2FactProv`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagoProvCheque`
--
ALTER TABLE `pagoProvCheque`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagoProveedor`
--
ALTER TABLE `pagoProveedor`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagoProvEfectivo`
--
ALTER TABLE `pagoProvEfectivo`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pagoProvMovimientoBanc`
--
ALTER TABLE `pagoProvMovimientoBanc`
  MODIFY `id` int(7) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pago2FactProv`
--
ALTER TABLE `pago2FactProv`
  ADD CONSTRAINT `facturaProveedor` FOREIGN KEY (`idFacturaProveedor`) REFERENCES `facturaProveedor` (`id`),
  ADD CONSTRAINT `pagoProveedor` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`);

--
-- Filtros para la tabla `pagoProvCheque`
--
ALTER TABLE `pagoProvCheque`
  ADD CONSTRAINT `chequeDeTercero` FOREIGN KEY (`idCheque3ro`) REFERENCES `chequeDeTerceros` (`id`),
  ADD CONSTRAINT `pagoProvee` FOREIGN KEY (`idPagoProv`) REFERENCES `pagoProveedor` (`id`);

--
-- Filtros para la tabla `pagoProveedor`
--
ALTER TABLE `pagoProveedor`
  ADD CONSTRAINT `proveedor` FOREIGN KEY (`idPago2Factura`) REFERENCES `proveedor` (`id`),
  ADD CONSTRAINT `tablaIntermedia` FOREIGN KEY (`idPago2Factura`) REFERENCES `pago2FactProv` (`id`);

--
-- Filtros para la tabla `pagoProvMovimientoBanc`
--
ALTER TABLE `pagoProvMovimientoBanc`
  ADD CONSTRAINT `movimientoBanc` FOREIGN KEY (`idMovBanc`) REFERENCES `movimientoCuentasBanc` (`id`),
  ADD CONSTRAINT `pagoProv` FOREIGN KEY (`idPagoProveedor`) REFERENCES `pagoProveedor` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
