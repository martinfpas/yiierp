
ALTER TABLE `comprobante` ADD CONSTRAINT `comprobante_PagoRel` FOREIGN KEY (`idPagoRelacionado`) REFERENCES `pagoCliente`(`id`) ON DELETE SET NULL ON UPDATE SET NULL;


#ALTER TABLE `comprobante`
#  ADD CONSTRAINT `comprobante_Camion` FOREIGN KEY (`Id_Camion`) REFERENCES `vehiculo` (`id_vehiculo`),
#  ADD CONSTRAINT `comprobante_NotaPedido` FOREIGN KEY (`idNotaPedido`) REFERENCES `notaPedido` (`id`),
#  ADD CONSTRAINT `comprobante_VoucherAfip` FOREIGN KEY (`idVoucherAfip`) REFERENCES `afipVoucher` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
#  ADD CONSTRAINT `comprobante_PuntoVenta` FOREIGN KEY (`Nro_Puesto_Venta`) REFERENCES `puntoDeVenta` (`num`),
#  ADD CONSTRAINT `comprobante_CategIva` FOREIGN KEY (`Iva_Responsable`) REFERENCES `categoriaIva` (`id_categoria`) ON UPDATE NO ACTION,
#  ADD CONSTRAINT `comprobante_Cliente` FOREIGN KEY (`Id_Cliente`) REFERENCES `cliente` (`id`);