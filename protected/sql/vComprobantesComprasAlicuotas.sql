-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-08-2020 a las 03:16:25
-- Versión del servidor: 5.7.31-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vComprobantesComprasAlicuotas`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vComprobantesComprasAlicuotas`  AS  select `cp`.`TipoComprobante` AS `TipoComprobante`,`cp`.`Nro_Puesto_Vta` AS `Nro_Puesto_Vta`,`cp`.`Nro_Comprobante` AS `Nro_Comprobante`,80 AS `codigo_de_documento_del_vendedor`,`p`.`cuit` AS `numero_de_identificacion_del_vendedor`,`vD`.`netoGravado` AS `importe_neto_gravado`,`vD`.`alicuota` AS `alicuota_de_IVA`,`vD`.`iva` AS `impuesto_liquidado`,`cp`.`MesCarga` AS `MesCarga` from ((`vDetallesIvaCompra` `vD` left join `comprobanteProveedor` `cp` on((`vD`.`idComprobanteProveedor` = `cp`.`id`))) left join `proveedor` `p` on((`cp`.`id_Proveedor` = `p`.`id`))) ;

--
-- VIEW  `vComprobantesComprasAlicuotas`
-- Datos: Ninguna
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
