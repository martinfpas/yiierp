-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-08-2020 a las 03:18:27
-- Versión del servidor: 5.7.31-0ubuntu0.18.04.1
-- Versión de PHP: 7.0.33-5+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura para la vista `vDetallesIvaCompra`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`phpmyadminuser`@`localhost` SQL SECURITY DEFINER VIEW `vDetallesIvaCompra`  AS  select `cp`.`id_Proveedor` AS `id_Proveedor`,`d`.`idComprobanteProveedor` AS `idComprobanteProveedor`,`d`.`alicuota` AS `alicuota`,round(((((`d`.`alicuota` * `d`.`cantidadRecibida`) * `d`.`precioUnitario`) * ((100 - `cp`.`Descuento`) / 100)) / 100),2) AS `iva`,round(((`d`.`cantidadRecibida` * `d`.`precioUnitario`) * ((100 - `cp`.`Descuento`) / 100)),2) AS `netoGravado`,`cp`.`Descuento` AS `Descuento` from (`detalleComprobanteProv` `d` left join `comprobanteProveedor` `cp` on((`cp`.`id` = `d`.`idComprobanteProveedor`))) group by `cp`.`id_Proveedor`,`d`.`idComprobanteProveedor`,`d`.`alicuota`,`d`.`precioUnitario`,`d`.`cantidadRecibida` ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
