-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2+deb7u8
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2019 a las 07:38:37
-- Versión del servidor: 5.6.38
-- Versión de PHP: 5.6.33-1~dotdeb+7.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `erca`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaCredito`
--

CREATE TABLE IF NOT EXISTS `notaCredito` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `clase` varchar(1) NOT NULL,
  `nroPuestoVta` int(3) NOT NULL,
  `nroComprobante` int(8) DEFAULT NULL,
  `ivaResponsable` int(2) NOT NULL,
  `nroViajante` int(2) NOT NULL,
  `idCliente` int(7) NOT NULL,
  `fecha` date DEFAULT NULL,
  `idCamion` int(4) NOT NULL,
  `totalFinal` float NOT NULL,
  `enviada` tinyint(1) NOT NULL DEFAULT '0',
  `recibida` tinyint(1) DEFAULT NULL,
  `observaciones` varchar(30) DEFAULT NULL,
  `otroIva` float DEFAULT NULL,
  `descuento1` float DEFAULT '0',
  `descuento2` float DEFAULT NULL,
  `totalNeto` float DEFAULT NULL,
  `cae` varchar(14) DEFAULT NULL,
  `vencimCae` datetime DEFAULT NULL,
  `ingBrutos` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ivaResponsable` (`ivaResponsable`),
  KEY `nroViajante` (`nroViajante`),
  KEY `idCliente` (`idCliente`),
  KEY `idCamion` (`idCamion`),
  KEY `nroPuestoVta` (`nroPuestoVta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `notaCredito`
--
ALTER TABLE `notaCredito`
  ADD CONSTRAINT `notaCredito_ibfk_10` FOREIGN KEY (`idCamion`) REFERENCES `vehiculo` (`id_vehiculo`),
  ADD CONSTRAINT `notaCredito_ibfk_6` FOREIGN KEY (`nroPuestoVta`) REFERENCES `puntoDeVenta` (`num`),
  ADD CONSTRAINT `notaCredito_ibfk_7` FOREIGN KEY (`ivaResponsable`) REFERENCES `categoriaIva` (`id_categoria`),
  ADD CONSTRAINT `notaCredito_ibfk_8` FOREIGN KEY (`nroViajante`) REFERENCES `viajante` (`numero`),
  ADD CONSTRAINT `notaCredito_ibfk_9` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
