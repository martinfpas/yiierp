select e.id AS idEntregaDoc,cd.id AS idCargaDoc,f.id AS idFactura,ne.id AS idNotaEntrega,pc.id AS idPago,pc.idCliente AS idCliente,pc.monto AS monto,pcch.idChequedeTercero AS idChequedeTercero,cd.tipoDoc AS tipoDoc, cd.totalFinal as totalFinal
	from (((((entregaDoc e left join cargaDocumentos cd on((e.id = cd.idEntregaDoc))) 
	left join comprobante f on((((cd.tipoDoc = 'FAC') or (cd.tipoDoc = 'NC')) and (cd.idComprobante = f.id)))) 
	left join notaEntrega ne on(((cd.tipoDoc = 'NE') and (cd.idComprobante = ne.id)))) 
	left join pagoCliente pc on(((pc.id_factura = f.id) or (pc.idNotaEntrega = ne.id) or (pc.id = f.idPagoRelacionado) or (pc.id = ne.idPagoRelacionado)))) 
	left join pagoConCheque pcch on((pcch.idPagodeCliente = pc.id)));
	