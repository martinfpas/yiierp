<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 30/9/2018
 * Time: 19:20
 */

Yii::import('zii.widgets.CPortlet');

class PagoClienteWid extends CPortlet{

    public $js_comp = '';
    public $is_modal = true;
    public $id_carga = null;

    protected function renderContent(){
        if ($this->is_modal){
            $this->render('pagoClienteWidget',array(
                'js_comp' => $this->js_comp,
                'is_modal' => $this->is_modal,
            ));
        }else{
            $this->render('pagoClienteWidget',array(
                'js_comp' => $this->js_comp,
                'is_modal' => $this->is_modal,
            ),false);
        }

    }

}

?>