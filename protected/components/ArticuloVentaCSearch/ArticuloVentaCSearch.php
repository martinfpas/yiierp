<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 9/3/2019
 * Time: 11:00

 @param $model          ej: objeto ProdNotaPedido. Instancia de objeto de nuevo producto 
 @param $idDivNuevo     ej: 'nuevoProdNotaPedido'. Id del boton de nuevo producto 
 @param $idMasProd      ej: 'masProdNotaPedido'. id del div para agregar un producto 
 @param $cantidad       ej: 'ProdNotaPedido_cantidad'. Id de cantidad nuevo producto 
 @param $precioUnit     ej: 'ProdNotaPedido_precioUnit'. Id del precio unitario nuevo producto 
 @param $idArtVenta     ej: 'ProdNotaPedido_idArtVenta'. Id de Articulo de Venta de nuevo producto 
 @param $id_form        ej: 'prod-nota-pedido-form'. Id del formulario de nuevo producto 
 @param $urlSaveAsync   ej: 'ProdNotaPedido/SaveAsync'. url del action savesync para guardar el producto 
 @param $id_grid        ej: 'prod-nota-pedido-grid'. id de la grilla de productos 
 @param $aHiddenFields  Array ids de campos hidden que van dentro del formulario 
 @param $conBuscador    booleano que indica si se incorpora o no el buscador de productos 
 @param $is_modal       booleano que indica si es modal o no */

Yii::import('zii.widgets.CPortlet');


class ArticuloVentaCSearch extends CPortlet{

    public $js_comp = '';
    public $is_modal = true;

    public $model          ;
    public $idDivNuevo     ;
    public $idMasProd      ;
    public $cantidad       ;
    public $precioUnit     ;
    public $descripcion    ;
    public $idPaquete      ;
    public $idArtVenta     ;
    public $id_form        ;
    public $urlSaveAsync   ;
    public $id_grid        ;
    public $aHiddenFields   = array() ;
    public $conBuscador     = false  ;
    public $modelParent    ;
    public $alicuota        = 21;
    


    protected function renderContent(){

        $this->render('artFormFilaCSearch',array(
            'model'=> $this->model,
            'js_comp' => $this->js_comp,
            'is_modal' => $this->is_modal,
            'model' => $this->model ,
            'idDivNuevo' => $this->idDivNuevo,
            'idMasProd'  => $this->idMasProd,
            'cantidad' => $this->cantidad,
            'precioUnit'  => $this->precioUnit,
            'idPaquete' => $this->idPaquete,
            'idArtVenta'  => $this->idArtVenta,
            'id_form'  => $this->id_form,
            'urlSaveAsync' => $this->urlSaveAsync,
            'id_grid'  => $this->id_grid,
            'aHiddenFields'  => $this->aHiddenFields,
            'conBuscador'  => $this->conBuscador,
            'oArticuloVenta' => new ArticuloVenta(),
            'descripcion' => $this->descripcion,
            'modelParent' => $this->modelParent,
            'alicuota' =>   $this->alicuota,
        ));
    }

}
