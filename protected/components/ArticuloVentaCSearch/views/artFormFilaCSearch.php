<style>
#nombreArticulo{
	font-weight: bold;
	font-style: italic;
}
</style>
<?php

/* @param $model          ej: objeto ProdNotaPedido. Instancia de objeto de nuevo producto */
/* @param $modelParent    ej: objeto NotaPedido. Instancia de objeto de Nota de pedido */
/* @param $idDivNuevo     ej: 'nuevoProdNotaPedido'. Id del boton de nuevo producto */
/* @param $idMasProd      ej: 'masProdNotaPedido'. id del div para agregar un producto */
/* @param $cantidad       ej: 'ProdNotaPedido_cantidad'. Id de cantidad nuevo producto */
/* @param $precioUnit     ej: 'ProdNotaPedido_precioUnit'. Id del precio unitario nuevo producto */
/* @param $idArtVenta     ej: 'ProdNotaPedido_idArtVenta'. Id de Articulo de Venta de nuevo producto */
/* @param $id_form        ej: 'prod-nota-pedido-form'. Id del formulario de nuevo producto */
/* @param $urlSaveAsync   ej: 'ProdNotaPedido/SaveAsync'. url del action savesync para guardar el producto */
/* @param $id_grid        ej: 'prod-nota-pedido-grid'. id de la grilla de productos */
/* @param $aHiddenFields  Array ids de campos hidden que van dentro del formulario */
/* @param $conBuscador    booleano que indica si se incorpora o no el buscador de productos */
/* @param $is_modal       booleano que indica si se incorpora o no el buscador de productos */


$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();

	$cs->registerScriptFile($baseUrl."/js/tabindex.js");
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
    $cs->registerScriptFile($baseUrl."/js/music.js");
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );
	//$cs->registerCssFile($baseUrl."/css/customFineUploader.css");

	Yii::app()->clientScript->registerScript('general2',"
		$(document).ready(function(){
            // SELECCIONA EL CONTENIDO CUANDO RECIBE FOCO...
            window.prevFocus = $();            
            bindArtVentaHandlers()
            //TODO: EVALUAR ELIMINAR LO SIGUIENTE
            try{
                //$('label').tooltip();
                //$('input[type=\"text\"]').tooltip();
            }catch(e){                
                console.warn(e);
            }
            
		});


		function refreshComprobanteDetails(){            
		    // TODO: MEJORAR LA SIGUIENTE LINEA PARA QUE idComprobante no esa necesario.
            var datos = 'id=".$model->idComprobante."';
            $.ajax({
                url:'".Yii::app()->createUrl(get_class($modelParent).'/Json')."',
                data: datos,
                type: 'GET',
                success: function (data){                    
                    
                    let response = JSON.parse(data);
                    $('#spanSubTotalNeto').html(response.Total_Neto);
                 	$('#spanSubTotal').html(response.subtotal);
                 	if(response.Total_Final === undefined){
                        $('#spanTotal').html(response.totalFinal);
                    }else{
                        $('#spanTotal').html(response.Total_Final);
                    }
                    
                    $('#Ing_Brutos').html(response.Ing_Brutos);
                    $('#Otro_Iva').html(response.Otro_Iva);
                    $('#subtotalIva').html(response.subtotalIva);
                    $('#subtotalIva').html(response.subtotalIva);
                    $('#AlicuotaItem').html(response.AlicuotaItem);
                },
            });
        }

		function bindArtVentaHandlers(){
            $(document).off('change.yiiGridView keydown.yiiGridView');
            try{                
                //$('label').tooltip({track: true});
			    //$('input').tooltip({track: true});
            }catch(e){
                console.warn(e);
            }		    

            // SELECCIONA EL CONTENIDO CUANDO RECIBE FOCO...
            window.prevFocus = $();

            $('#".$id_form." input[type=\"text\"]').focus(function(){
            	let selected = false;
                $(this).on('mouseup.a keyup.a', function(e){
                    //$(this).off('mouseup.a keyup.a').select();
                    if($(prevFocus).attr('id') != $(this).attr('id') && !selected){                        
                        $(this).off('mouseup.a keyup.a').select();
                        selected = true;
                    };
                });
                $(this).on('keypress.a', function(e){
					selected = true;
                });
            });

            $('#".$id_form." input[type=\"text\"]').blur(function(){
                window.prevFocus = $(this);
            });

		    $('#ArticuloVenta_id_rubro').focus();
		    try{
                //$('label').tooltip();
                //$('input').tooltip();
            }catch(e){
                console.warn(e);
            }

            $('#".$idArtVenta."').change(function(ev){
                let idArt = $(this).val();
                var datos = 'id='+idArt;
                $.ajax({
                    url: '".Yii::app()->createUrl('ArticuloVenta/GetAsyncById')."',
                    data: {
                        id: idArt,
                        alicuota: '".$alicuota."'
                    },
                    type: 'GET',
                    success: function(data){

                        let response = JSON.parse(data);

                        $('#ok_id_rubro').html(response.Rubro);
                        $('#ok_id_rubro').show('slow');

                        $('#ok_id_articulo').html(response.Articulo);
                        $('#ok_id_articulo').show('slow');

                        $('#ok_id_presentacion').html(response.descripcion);
                        $('#ok_id_presentacion').show('slow');
                        $('#".$cantidad."').focus();
                        $('#".$precioUnit."').val(parseFloat(response.precio));
                        $('#".$idArtVenta."').val(response.id_articulo_vta);
                        $('#".$descripcion."').val(response.descripcion);
                    },
                    error: function(x,y,z){
                        ev.stopPropagation();
                        ev.preventDefault();
                        $('#".$idArtVenta."').focus();
                        beep();
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        var htmlError = $(errorDiv).html();
                        $('#error_id_rubro').html(htmlError);
                        $('#error_id_rubro').show('slow');
                    }
                });
            });



             $('#ArticuloVenta_id_rubro').keypress(function (ev) {
                $('#error_id_rubro').hide('slow');
                $('#ok_id_rubro').hide('slow');

                var keycode = (ev.keyCode ? ev.keyCode : ev.which);                
                if (keycode == '13') {
                    //$('#".$id_form."').submit();
                    let datos = 'idRubro='+$(this).val();
                    $.ajax({
                        url: '".Yii::app()->createUrl('Rubro/GetAsync')."',
                        data: datos,
                        type: 'GET',
                        success: function(html){
                            $('#ok_id_rubro').html(html);
                            $('#ok_id_rubro').show('slow');
                            $('#ArticuloVenta_id_articulo').focus();
                        },
                        error: function(x,y,z){
                            ev.stopPropagation();
                            ev.preventDefault();
                            beep();
                            $('#ArticuloVenta_id_rubro').focus();
                            respuesta =  $.parseHTML(x.responseText);
                            errorDiv = $(respuesta).find('.errorSpan');
                            var htmlError = $(errorDiv).html();
                            $('#error_id_rubro').html(htmlError);
                            $('#error_id_rubro').show('slow');
                        }
                    });
                    return false;
                }else if(keycode == '43'){
                    $('#modalBuscaArticulos').modal();
                    return false;
                }else{
                    return true;
                }
            })

             $('#ArticuloVenta_id_articulo').keypress(function (ev) {
                $('#error_id_articulo').hide('slow');
                $('#ok_id_articulo').hide('slow');

                var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode == '13') {
                    //$('#".$id_form."').submit();
                    let datos = 'idRubro='+$('#ArticuloVenta_id_rubro').val()+'&idArticulo='+$(this).val();
                    $.ajax({
                        url: '".Yii::app()->createUrl('Articulo/GetAsync')."',
                        data: datos,
                        type: 'GET',
                        success: function(html){
                            $('#ok_id_articulo').html(html);
                            $('#ok_id_articulo').show('slow');
                            $('#ArticuloVenta_id_presentacion').focus();
                        },
                        error: function(x,y,z){
                            ev.stopPropagation();
                            ev.preventDefault();
                            beep();
                            respuesta =  $.parseHTML(x.responseText);
                            errorDiv = $(respuesta).find('.errorSpan');
                            var htmlError = $(errorDiv).html();
                            $('#error_id_articulo').html(htmlError);
                            $('#error_id_articulo').show('slow');
                            $('#ArticuloVenta_id_articulo').focus();
                        }
                    });
                    return false;
                }else if(keycode == '43'){
                    $('#modalBuscaArticulos').modal();
                    return false;
                }else{
                    return true;
                }
            })
            $('#ArticuloVenta_id_presentacion').focus(function(){
                if($('#error_id_articulo').is(':visible')){
                    $('#ArticuloVenta_id_articulo').focus();
                }
            });

             $('#ArticuloVenta_id_presentacion').keypress(function (ev) {
                $('#error_id_presentacion').hide('slow');
                $('#ok_id_presentacion').hide('slow');

                var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                if (keycode == '13') {
                    //$('#".$id_form."').submit();
                    let datos = 'idRubro='+$('#ArticuloVenta_id_rubro').val()+'&idArticulo='+$(this).val();
                    $.ajax({
                        url: '".Yii::app()->createUrl('ArticuloVenta/GetAsync')."',
                        data: {
                            articulo: $('#ArticuloVenta_id_articulo').val(),
                            rubro: $('#ArticuloVenta_id_rubro').val(),
                            presentacion: $(this).val(),
                            alicuota: '".$alicuota."',
                        },
                        //dataType: 'json',
                        success: function(data){
                            let response = JSON.parse(data);
                            ev.stopPropagation();
                            ev.preventDefault();                            
                            $('#ok_id_presentacion').html(response.descripcion);
                            $('#ok_id_presentacion').show('slow');
                            $('#".$cantidad."').focus();
                            $('#".$precioUnit."').val(parseFloat(response.precio));
                            $('#".$idArtVenta."').val(response.id_articulo_vta);
                            $('#".$descripcion."').val(response.descripcion);
                            /*
                            //$('#nombreArticulo').html($('#ArticuloVenta_id_presentacion option:selected').attr('art'));

                            //$('#".$precioUnit."').val(parseFloat($('#ArticuloVenta_id_presentacion option:selected').val()));


                            */
                        },
                        error: function(x,y,z){
                            respuesta =  $.parseHTML(x.responseText);
                            errorDiv = $(respuesta).find('.errorSpan');
                            beep();
                            var htmlError = $(errorDiv).html();
                            $('#error_id_presentacion').html(htmlError);
                            $('#error_id_presentacion').show('slow');
                            $('#ArticuloVenta_id_presentacion').focus();
                        }
                    });
                    return false;
                }else if(keycode == '43'){
                    $('#modalBuscaArticulos').modal();
                    return false;
                }else{
                    return true;
                }
            })
		}




	",CClientScript::POS_READY);

?>
<style>
.tooltip-inner {
    white-space: pre-wrap;
}
.tooltip {
    white-space: pre-wrap;
}

</style>
&#013;
<div id="<?=$idDivNuevo?>" style="">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=> $id_form,
		'enableAjaxValidation'=>false,
		'action' => $urlSaveAsync,
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=> $id_grid,
			'idMas' => $idMasProd,
			'idDivNuevo' => $idDivNuevo,
            'style' => 'margin-bottom:0px;'
		),
	)); ?>


		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProd"></div>

	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
    <?php

        foreach ($aHiddenFields as $index => $hiddenField) {
            echo $form->hiddenField($model,$hiddenField);
        }


	?>
	<div class="row-fluid">
		<div class="span3">
			<?php
                echo $form->textFieldRow($oArticuloVenta,'id_rubro',array('class'=>'span12','tabindex'=>'1','autocomplete'=>"off",'title' => 'Ingrese el codigo o presione + para buscar','data-placement' => "top"));
			?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_rubro"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_rubro"></div>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($oArticuloVenta,'id_articulo',array('class'=>'span12','tabindex'=>'2','autocomplete'=>"off",'title' => 'Ingrese el codigo o presione + para buscar','data-placement' => "top")); ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_articulo"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_articulo"></div>
		</div>
		<div class="span5">
			<?php echo $form->textFieldRow($oArticuloVenta,'id_presentacion',array('class'=>'span12','tabindex'=>'3','autocomplete'=>"off",'title' => 'Ingrese el codigo o presione + para buscar','data-placement' => "top")); ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_presentacion"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_presentacion"></div>
		</div>
	</div>
	<div class="row-fluid">
		<!--<div class="span3" id="nombreArticulo">-->
        <div class="span3" >
            <?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12','tabindex'=>'4','autocomplete'=>"off")); ?>
		</div>
        <div class="span4">
            <?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','tabindex'=>'5','autocomplete'=>"off")); ?>
        </div>
		<div class="span2" style="text-align: center">
            <?php echo $form->textFieldRow($model,str_replace(get_class($model).'_','',$precioUnit),array('class'=>'span12','tabindex'=>'6','autocomplete'=>"off")); ?>
		</div>

        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Cargar' : 'Guardar',
                'htmlOptions' => array(
                    'id' => 'guardaArticulo',
                    'style' => 'margin-top:25px;'
                )
            )); ?>
        </div>

	</div>
</div>


    <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProd">Datos Guardados Correctamente !</div>
	<?php $this->endWidget(); ?>
</div>


<?php

if($conBuscador){
    $this->widget('BuscaArtVenta', array('js_comp'=>'#'.$idArtVenta,'is_modal' => $is_modal));
}

?>
