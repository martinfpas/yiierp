<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 30/9/2018
 * Time: 19:20
 */

Yii::import('zii.widgets.CPortlet');

class BuscaArtVenta extends CPortlet{

    public $js_comp = '';
    public $is_modal = true;
    public $ajaxMode = false;

    protected function renderContent(){
        $model=new ArticuloVenta('search');

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['ArticuloVenta'])){
            $model->attributes=$_GET['ArticuloVenta'];
        }

        $this->render('adminWidget',array(
            'model'=>$model,
            'js_comp' => $this->js_comp,
            'is_modal' => $this->is_modal,
            'ajaxMode' => $this->ajaxMode,
        ));
    }

}

?>