<?php
    class CropAvatarPort {
        private $src;
        private $data;
        private $file;
        private $dst;
        private $type;
        private $extension;
        private $field;
        private $srcDir = '/var/www/app/upload'; // UPLOAD
        private $dstDir = '/var/www/app/upload'; // AVATAR
        private $msg;

        function __construct($src, $data, $file,$field, $webFolder,$srcFolder) {
        	//die('$src :'.$src);
        	//die('$data :'.$data);
        	//print_r($file);
        	//die('$file :');
        	//die('$webFolder '.$webFolder.' > '.$srcFolder);
            $this -> srcDir = $webFolder;
            $this -> dstDir = $srcFolder;
            $this -> field = strtoupper($field);
        	$this -> setSrc($src);
            $this -> setData($data);
            $this -> setFile($file);
            $this -> crop($this -> src, $this -> dst, $this -> data);
        }

        private function setSrc($src) {
            if (!empty($src)) {
                $type = exif_imagetype($src);

                if ($type) {
                    $this -> src = $src;
                    $this -> type = $type;
                    $this -> extension = image_type_to_extension($type);
                    $this -> setDst();
                }
            }
        }

        private function setData($data) {
            if (!empty($data)) {
                $this -> data = json_decode(stripslashes($data));
            }
        }
        
		/*
		 private function setSrcDir($src) {
            if (!empty($src)) {
				
            }
        }

        private function setWeb($data) {
            if (!empty($data)) {
                $this -> data = json_decode(stripslashes($data));
            }
        } 
        */

        private function setFile($file) {
            $errorCode = $file['error'];

            if ($errorCode === UPLOAD_ERR_OK) {
                $type = exif_imagetype($file['tmp_name']);
                
                // 
                if ($type) {
                    //die('$this -> dstDir '.$this -> dstDir);
                	$dir = $this -> dstDir;
					
                    //die('$dir '.$dir);
                    if (!file_exists($dir)) {
                        mkdir($dir, 0777);
                    }
					//OBTENEMOS LA EXTENSION EN MAYUSCULA
                    $extension = strtoupper(image_type_to_extension($type));
                    
                    // SETEAR EL NOMBRE
                    $src = $dir .  $this->field . $extension;

                    if ($type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {

                        // BORRAR TODAS LAS EXTENSIONES SI EXISTEN
                    	$srcPNG = $dir .  $this->field . 'PNG';
                    	if (file_exists($srcPNG)) {
                            unlink($src);
                        }
                        
						$srcJPG = $dir .  $this->field . 'JPG';
                    	if (file_exists($srcJPG)) {
                            unlink($src);
                        }
                        
                    	$srcJPEG = $dir .  $this->field . 'JPEG';
                    	if (file_exists($srcJPEG)) {
                            unlink($srcJPEG);
                        }

                        $result = move_uploaded_file($file['tmp_name'], $src);

                        
                        if ($result) {
                            $this -> src = $src;
                            $this -> type = $type;
                            $this -> extension = $extension;
                            $this -> setDst();
                            //echo '>>'.$this -> dst;
                            //die(' >> '.$this->dstDir.'TMB_'.$this->field.$extension);
                            

							//echo '$image->save '.$sResult;
                        } else {
                             $this -> msg = 'Failed to save file';
                        }
                    } else {
                        $this -> msg = 'Please upload image with the following types: JPG, PNG, JPEG';
                    }
                } else {
                    $this -> msg = 'Please upload image file';
                }
            } else {
                $this -> msg = $this -> codeToMessage($errorCode);
            }
        }

        private function setDst() {
            $dir = $this -> dstDir;

            if (!file_exists($dir)) {
                mkdir($dir, 0777);
            }

            $this -> dst = $dir . $this -> field. $this -> extension;
        }

        private function crop($src, $dst, $data) {
        	//die($src);
            if (!empty($src) && !empty($dst) && !empty($data)) {
                switch ($this -> type) {
                    

                    case IMAGETYPE_JPEG:
                        $src_img = imagecreatefromjpeg($src);
                        break;

                    case IMAGETYPE_PNG:
                        $src_img = imagecreatefrompng($src);
                        break;
                }

                if (!$src_img) {
                    $this -> msg = "Failed to read the image file";
                    return;
                }
                

                //$dst_img = imagecreatetruecolor(220, 220);
                $dst_img = imagecreatetruecolor($data -> width, $data -> height);
                $result = imagecopyresampled($dst_img, $src_img, 0, 0, $data -> x, $data -> y, $data -> width, $data -> height, $data -> width, $data -> height);

                if ($result) {
                    switch ($this -> type) {
                        

                        case IMAGETYPE_JPEG:
                            $result = imagejpeg($dst_img, $dst);
                            break;

                        case IMAGETYPE_PNG:
                            $result = imagepng($dst_img, $dst);
                            break;
                    }
                    
					$image = new EasyImage($this -> dst);
					$image->resize(187, 274);
					$sResult = $image->save($this->dstDir.'TMB_'.$this->field.$this -> extension);

                    if (!$result) {
                    	
                        $this -> msg = "Failed to save the cropped image file";
                    }
                } else {
                    $this -> msg = "Failed to crop the image file";
                }

                imagedestroy($src_img);
                imagedestroy($dst_img);
            }
        }

        private function codeToMessage($code) {
            switch ($code) {
                case UPLOAD_ERR_INI_SIZE:
                    $message = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
                    break;

                case UPLOAD_ERR_FORM_SIZE:
                    $message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                    break;

                case UPLOAD_ERR_PARTIAL:
                    $message = 'The uploaded file was only partially uploaded';
                    break;

                case UPLOAD_ERR_NO_FILE:
                    $message = 'No file was uploaded';
                    break;

                case UPLOAD_ERR_NO_TMP_DIR:
                    $message = 'Missing a temporary folder';
                    break;

                case UPLOAD_ERR_CANT_WRITE:
                    $message = 'Failed to write file to disk';
                    break;

                case UPLOAD_ERR_EXTENSION:
                    $message = 'File upload stopped by extension';
                    break;

                default:
                    $message = 'Unknown upload error';
            }

            return $message;
        }

        public function getResult() {
            return !empty($this -> data) ? $this->srcDir.'TMB_'.$this->field.$this -> extension : $this -> src;
        }

        public function getMsg() {
            return $this -> msg;
        }
    }
?>