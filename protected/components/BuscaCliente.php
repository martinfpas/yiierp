<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 30/9/2018
 * Time: 19:20
 */

Yii::import('zii.widgets.CPortlet');

class BuscaCliente extends CPortlet{

    public $js_comp = '';
    public $is_modal = true;

    protected function renderContent(){
        $model=new Cliente('search');

        if(isset($_GET['Cliente'])){
            $model->unsetAttributes();  // clear any default values
            $model->attributes=$_GET['Cliente'];
        }

        $this->render('adminClienteWidget',array(
            'model'=>$model,
            'js_comp' => $this->js_comp,
            'is_modal' => $this->is_modal,
        ));
    }

}

?>