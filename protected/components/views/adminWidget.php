<?php
/* @var $this ArticuloVentaController */
/* @var $model ArticuloVenta */

$cs = Yii::app()->getClientScript();


if ($ajaxMode){
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );
}else{

}

Yii::app()->clientScript->registerScript('scriptModalBuscaArticulos', "


$('#formBuscaArt').submit(function(){
	$('#wd-articulo-venta-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$(document).ready(function(){
    bindHandlers();
});

function bindHandlers(){    
    $('#wd-articulo-venta-grid').delegate('tbody>tr', 'dblclick', function(e){
        let id = $(this).find('td').html()        
        $('".$js_comp."').val(id).trigger(\"change\");
        $('#modalBuscaArticulos').modal('hide');
        //$('#modalBuscaArticulos').modal('toggle');
            
    });
    
    $('#modalBuscaArticulos').on('hidden.bs.modal', function () {
        //$('#formBuscaArt')[0].reset();
    })
    
     $('#modalBuscaArticulos').on('shown.bs.modal', function () {
        $('#formBuscaArt')[0].reset();
        $('#formBuscaArt').find('#ArticuloVenta_descripcion').focus();        
    })
}

");
?>
<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    .modal-dialog {
        margin: 2.5vh auto;
    }

    /*Sets the maximum height of the entire modal to 95% of the screen height*/
    .modal-content {
        max-height: 95vh;
        overflow: scroll;
    }

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    .modal-body {
        max-height: 90vh;
    }
    /*Sets the maximum height of the modal image to 69% of the screen height*/
    .modal-body img {
        max-height: 69vh;
    }

    .modal{
        width: 94%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);

    }

    .modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    .modal.fade{
        top:100%;
    }

    .tooltip {
        white-space: pre-line;
    }

</style>

<div id="modalBuscaArticulos" class="modal fade"  tabindex="-1">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buscar y seleccionar por nombre </h4>

            </div>
            <div class="modal-body">
                <div class="search-form" style="">

                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        //'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions' => array(
                            'id' => 'formBuscaArt',
                        ),

                    )); ?>
                    <div class="content-fuid">
                        <div class="row-fuid">
                            <div class="span3"><?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?> </div>

                            <div class="span3"><?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?> </div>

                            <div class="span4">
                                <?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70)); ?>
                            </div>
                            <div class="span2">
                                <?php $this->widget('bootstrap.widgets.TbButton', array(
                                    'buttonType' => 'submit',
                                    'type'=>'primary',
                                    'label'=>'Buscar',
                                    'htmlOptions' => array(
                                        'style' => 'margin-top:25px;'
                                    )
                                )); ?>
                            </div>
                        </div>
                    </div>

                    <?php $this->endWidget(); ?>
                </div><!-- search-form -->

                <?php $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'wd-articulo-venta-grid',
                    'dataProvider'=>$model->searchShort(),
                    'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate...");bindHandlers()}',
                    'columns'=>array(
                        // NO TOCAR EL ORDEN DEL "ID". SIEMPRE VA PRIMERO
                        array(
                            'name' => 'id_articulo_vta',
                            'header' => 'id.',
                            //'cssClassExpression' => 'idTd',
                            'headerHtmlOptions' => array(
                                'style' => 'width:50px',
                            ),
                        ),
                        array(
                            'name' => 'codigo',
                            'headerHtmlOptions' => array(
                                'style' => 'width:70px',
                            ),
                        ),

                        array(
                            'name' => 't.id_rubro',
                            'value' => '$data->oArticulo->oRubro->descripcion',
                            'htmlOptions' => array('style' => ''),
                            'filter' => CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),
                        ),
                        array(
                            'name' => 'id_articulo',
                            'value' => '$data->oArticulo->descripcion',
                            'header' => 'articulo',
                            'htmlOptions' => array('style' => ''),
                        ),
                        'descripcion',

                    ),
                )); ?>
            </div>
        </div>

    </div>
</div>


