<?php
/* @var $this ClienteController */
/* @var $model Cliente */

$cs = Yii::app()->getClientScript();
$cs->scriptMap=array(
    //'jquery.ba-bbq.js'=>false,
    //'jquery.js'=>false,
    //'jquery.yiigridview.js'=>false,
);

Yii::app()->clientScript->registerScript('search', "


$('#formBuscaCli').submit(function(){
	$('#wd-cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
$(document).ready(function(){
    bindHandlers();
});

function bindHandlers(){    
    $('#wd-cliente-grid').delegate('tbody>tr', 'dblclick', function(){        
        let id = $(this).find('td').html()        
        $('".$js_comp."').val(id).trigger(\"change\");
        $('#modalBuscaClientes').modal('hide');    
    });
    
    $('#modalBuscaClientes').on('hidden.bs.modal', function () {
        //$('#formBuscaCli')[0].reset();        
    })
    
    $('#modalBuscaClientes').on('shown.bs.modal', function () {
        $('#formBuscaCli')[0].reset();
        $('#formBuscaCli').find('#Cliente_razonSocial').focus();        
    })
}

");
?>
<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    .modal-dialog {
        margin: 2.5vh auto;
    }

    /*Sets the maximum height of the entire modal to 95% of the screen height*/
    .modal-content {
        max-height: 95vh;
        overflow: scroll;
    }

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    .modal-body {
        max-height: 90vh;
    }
    /*Sets the maximum height of the modal image to 69% of the screen height*/
    .modal-body img {
        max-height: 69vh;
    }

    .modal{
        width: 94%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);

    }

    .modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    .modal.fade{
        top:100%;
    }

    .tooltip {
        white-space: pre-line;
    }

</style>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalBuscaClientes" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buscar y seleccionar </h4>

            </div>
            <div class="modal-body">
                <div class="search-form" style="">

                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        //'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions' => array(
                            'id' => 'formBuscaCli',
                        ),

                    )); ?>
                    <div class="content-fuid">
                        <div class="row-fuid">
                            <div class="span3">
                                <?php echo $form->textFieldRow($model,'id',array('class'=>'span12','maxlength'=>15)); ?>
                            </div>
                            <div class="span3">
                                <?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>40)); ?>
                            </div>
                            <div class="span3">
                                <?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>60)); ?>
                            </div>
                            <div class="span3">
                                <?php echo $form->textFieldRow($model,'cuit',array('class'=>'span12','maxlength'=>13)); ?>
                            </div>

                        </div>
                    </div>

                    <div class="form-actions">
                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>'Buscar',

                        )); ?>
                    </div>

                    <?php $this->endWidget(); ?>
                </div><!-- search-form -->

                <?php $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'wd-cliente-grid',
                    'dataProvider'=>$model->searchShort(),
                    //'dataProvider'=>$model->search(),
                    'afterAjaxUpdate' => 'js:function(){console.log("afterAjaxUpdate");bindHandlers()}',
                    'columns'=>array(
                        'id',
                        'razonSocial',
                        'direccion',
                        'nombreFantasia',
                        'contacto',
                        'cuit',

                    ),
                )); ?>
            </div>
        </div>

    </div>
</div>


