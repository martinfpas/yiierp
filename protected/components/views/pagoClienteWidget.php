<style>
    .portlet-content{
        background-color: transparent;
    }
</style>

<?php
/* @var $this ClienteController */
/* @var $model Cliente */

$cs = Yii::app()->getClientScript();
$cs->scriptMap=array(
    //'jquery.ba-bbq.js'=>false,
    //'jquery.js'=>false,
    //'jquery.yiigridview.js'=>false,
);

Yii::app()->clientScript->registerScript('search', "

$(document).ready(function(){
       
    bindHandlers();
    
    ".
    (($is_modal)?
        "$('#modalPagoClientes').on('show.bs.modal', function () {
        var idPago = $('#idPagoWid').val();       
                console.log('#idPagoWid:: '+idPago);
                loadForm(idPago);
                
            })"
        :   "var idPago = $('#idPagoWid').val();
            loadForm(idPago);")
    ."
});

$('#formBuscaCli').submit(function(){
	$('#wd-pagoCliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


function loadForm(idPago){
    if(idPago > 0){
        var url = '".Yii::app()->createUrl('pagoCliente/Form',array('idPago'=>''))."'+idPago+".(($is_modal)?"'&isModal=true'":"'&isModal=false'")
    .";
    }else{
        var idCargaDoc = $('#idCargaDocWid').val();
        var url = '".Yii::app()->createUrl('pagoCliente/Form',array('idPago'=>''))."&idCargaDoc='+idCargaDoc;
    }
    
    $.ajax({
        type:'POST',
        url:url,
        data: '',
        success:function(html){
            ".
            (($is_modal)?
                "$('#modalPagoClientes').find('.modal-body').html(html);
                bindHandlers();
                "
            :   "$('#noModal').html(html);
                bindHandlers();
            ")
            ."
            
            
            
        },
        error:function(x,y,z){
            console.log('errorrrrr');
        },        
    });
    
}                             

function bindHandlers(){
    console.log('out');
    $('#wd-cliente-grid').delegate('tbody>tr', 'dblclick', function(){
        console.log('in');
        let id = $(this).find('td').html()
        console.log(id);
        $('".$js_comp."').val(id).trigger(\"change\");;
        $('#modalBuscaClientes').modal('hide');    
    });
    
    $('#modalPagoClientes').on('hidden.bs.modal', function () {
        //$('#formBuscaCli')[0].reset();
        //$('#modalPagoClientes').find('.modal-body').html('');
        let val = $('".$js_comp."').val();
        $('".$js_comp."').val(val+'.').trigger(\"change\");
    })
}



");
?>
<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    .modal-dialog {
        margin: 2.5vh auto;
    }

    <?php
        if($is_modal){
            echo '
                /*Sets the maximum height of the entire modal to 95% of the screen height*/
                .modal-content {
                    max-height: 80vh;
                    overflow: scroll;
                }';
        }else{
            echo '
                /*Sets the maximum height of the entire modal to 95% of the screen height*/
                .modal-content {
                    overflow: scroll;
                    display: inline;
                }';
        }
    ?>

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    .modal-body {
        max-height: unset;
        height: -webkit-fill-available;
    }

    .modal{
        width: 94%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);

    }

    .modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    .modal.fade{
        top:100%;
    }

    .tooltip {
        white-space: pre-line;
    }

</style>
<input type="hidden" id="idPagoWid">
<input type="hidden" id="idCargaDocWid">

<div class="modal-content" id="noModal"></div>
<div id="modalPagoClientes" class="modal fade" style="height: 90vh;" tabindex="-1">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Pago </h4>

            </div>
            <div class="modal-body">

            </div>
        </div>

    </div>
</div>


