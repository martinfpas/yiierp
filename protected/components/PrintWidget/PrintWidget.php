<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 17/10/2019
 * Time: 21:39
 *
 * v1.1
 * agrega el logueo del comando
 */

Yii::import('zii.widgets.CPortlet');

class PrintWidget extends CPortlet
{
    public $url_print;
    public $url_download;
    public $controller_action;

    protected function renderContent()
    {
        $this->render('widget',array(
            'url_print' => $this->url_print,
            'url_download' => $this->url_download,
            'controller_action' => $this->controller_action,
        ));

    }

    public static function print($printer,$fullPathFile,$fromPage="",$toPage="",$media="FanFoldGerman",$bottom=25,$left=18,$right=18,$top=25){
        $page_ranges = '';
        if($fromPage=="" && $toPage==""){
            $page_ranges = '-o number-up='.$fromPage;
        }elseif ($fromPage =="" && $toPage!=""){
            $page_ranges = '-o page-ranges=1-'.$toPage;
        }else{
            $page_ranges = '-o page-ranges='.$fromPage.'-'.$toPage;
        }
        $command = ' lp -d "'. $printer.'" -o media='.$media.' '.$page_ranges.' -o page-bottom='.$bottom.' -o page-left='.$left.' -o page-right='.$right.' -o page-top='.$top.' "'.$fullPathFile.'"';
        Yii::log($command,'warning');
        Yii::log(shell_exec($command),'warning');

    }

}