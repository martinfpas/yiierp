<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 17/10/2019
 * Time: 21:46
 */
?>

<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    #modalPrint .modal-dialog {
        margin: 2.5vh auto;
    }

    /*Sets the maximum height of the entire modal to 95% of the screen height*/
    #modalPrint .modal-content {
        max-height: 95vh;
        overflow: scroll;
    }

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    #modalPrint .modal-body {
        max-height: 90vh;
    }
    /*Sets the maximum height of the modal image to 69% of the screen height*/
    #modalPrint .modal-body img {
        max-height: 69vh;
    }

    #modalPrint.modal{
        width: 50%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);
        z-index: 11150;

    }

    #modalPrint.modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    #modalPrint.modal.fade{
        top:100%;
    }

    .tooltip {
        white-space: pre-line;
    }

</style>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalPrint" class="modal fade"  >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Seleccionar e Imprimir </h4>
            </div>
            <div class="modal-body">
                <div class="content-fluid">
                    <div class="row-fluid bordeRedondo">
                        <?php
                        $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                            'id' => 'print-form',
                            'action' => $url_print,
                            'htmlOptions' => array(
                                'target' => '_blank',
                            )

                        ));
                        ?>
                        <div class="span5">
                            <?php
                            echo CHtml::label('Impresora','printer');
                            echo CHtml::dropDownList('printer','',CHtml::listData(ComponentesComunes::getActivePrinters(),'name','name'),array('empty' => 'Seleccione una impresora','style' => 'margin: 5px 5px 5px 5px;', "class"=>"span12"));
                            ?>
                        </div>
                        <div class="span2">
                            <?php
                                echo CHtml::label('Desde página (1)','fromPage');
                                echo CHtml::textField('fromPage',1,array("class"=>"span12"));
                            ?>
                        </div>
                        <div class="span2">
                            <?php
                                echo CHtml::label('Hasta página (N)','toPage');
                                echo  CHtml::textField('toPage',null,array("class"=>"span12"));
                            ?>
                        </div>
                        <div class="span2">
                            <?php
                                $this->widget('bootstrap.widgets.TbButton', array(
                                    'buttonType'=>'submit',
                                    'type'=>'primary',
                                    'icon' => 'white print',
                                    'label'=> 'Imprimir',
                                    'htmlOptions' => array(
                                        'style' => 'margin: 25px 0px 0px 0px;',
                                        "class"=>"span12",
                                    )
                                ));
                            ?>
                        </div>
                        <?php
                            $this->endWidget();
                        ?>
                    </div>
                    <div class="row-fluid">
                        <?php
                        $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                            'id' => 'download-form',
                            'action' => $url_download,
                            'htmlOptions' => array(
                                'target' => '_blank',
                                'style' => 'margin: 5px 5px 5px 15px;'
                            )

                        ));
                        $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType'=>'submit',
                            'type'=>'primary',
                            'icon' => 'white download-alt',
                            'label'=> 'Descargar',
                        ));
                        $this->endWidget();

                        ?>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>