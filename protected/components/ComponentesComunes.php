<?php
	/* 
	 * version 1.16
	 * adecuada a php 7.0
	 * agrega funcion para mostrar fecha
	 * agrega arrays constantes
	 * puede mostrar fechas comparables
	 * se le puede poner parametro de formato de fecha
	 * se agrego la funcion numerosAletras
	 * compatibilidad con la separacion con / en jpicker2db
	 * se agrega la palabra "con" para los decimales
	 * agrega print_array
	 * correccion en fechaComparada, si no viene simbolo comparacion
	 * agrega parametro a print_array
	 * correccion en jpicker2db, para fechas cuyo dia es un solo digito habia problemas
	 * se agrega validatesAsInt
	 * Se agrega getActivePrinters que trae las impresoras activas
	 */
	class ComponentesComunes extends CApplicationComponent {
		
		const iInactivo = 0;
		const iActivo = 1; 
		
		const inactivo = 'Inactivo';
		const activo = 'Activo'; 
		
		public static $aEstado = array(
			self::iInactivo => self::inactivo,
			self::iActivo => self::activo,
		);
		
		const iNo = 0;
		const iSi = 1; 
		
		const no = 'No';
		const si = 'Si'; 
		
		public static $aSiNo = array(
			self::iNo => self::no,
			self::iSi => self::si,
		);
	
		public static function jpicker2db($date,$conHMS=true) {
			if ($date == NULL || $date == '' ) {
				return NULL;
			}

			$date = str_replace('/','-',$date);
			
			$iMatch = preg_match('/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $date,$coincidencias);
			
			$iMatch2 = preg_match('/[0-9]{4}-[0-9]{1,2}-[0-9]{2}/', $date,$coincidencias2);

			
			// FORMATO NO AMERICANO
			if ($iMatch == 1) {
				$sNewDate = '';


                $aFecha = explode('-', $coincidencias[0]);
                $aFecha = array_reverse($aFecha);

				if ($conHMS) {
					$sNewDate = implode('-', $aFecha).' 00:00:00';	
				}else {
					$sNewDate = implode('-', $aFecha);
				}
				
				return $sNewDate;
			// TIENE FORMATO AMERICANO					
			}elseif ($iMatch2 > 0){
				// SI VA CON HORA MIN Y SEG
				if ($conHMS) {
					$iMatch3 = preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} /', $date,$coincidencias3);
					if ($iMatch3 = 1) {
						return $date;
					}else {
						return $coincidencias2[0].' 00:00:00';
					}
				// SOLO FECHA	
				}else {
					return $coincidencias2[0];
				}
				
			}else {
			    Yii::log('NO TIENE NI FORMATO AMERICANO O NO SE DETECTO >'.$date.'<','error');
				return $date;	
			}


		}

		public static function jpicker2dbMA($date,$conHMS=true) {
			if ($date == NULL || $date == '' ) {
				return NULL;
			}
			
			$iMatch = preg_match('/[0-9]{2}-[0-9]{4}/', $date,$coincidencias);
			
			$iMatch2 = preg_match('/[0-9]{4}-[0-9]{2}/', $date,$coincidencias2);
			
			// FORMATO NO AMERICANO
			if ($iMatch == 1) {
				$sNewDate = '';
				$aFecha = explode('-', $coincidencias[0]);
				$aFecha = array_reverse($aFecha);
				if ($conHMS) {
					$sNewDate = implode('-', $aFecha).'-01 00:00:00';	
				}else {
					$sNewDate = implode('-', $aFecha.'-01');
				}
				
				return $sNewDate;
			// TIENE FORMATO AMERICANO					
			}elseif ($iMatch2 > 0){
				// SI VA CON HORA MIN Y SEG
				if ($conHMS) {
					$iMatch3 = preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} /', $date,$coincidencias3);
					if ($iMatch3 = 1) {
						return $date;
					}else {
						return $coincidencias2[0].' 00:00:00';
					}
				// SOLO FECHA	
				}else {
					return $coincidencias2[0];
				}
				
			}else {
				return $date;	
			}


		}
		
		// EVITA VALORES NULOS Y DEVUELVE LA FECHA FORMATEADA
		public static function fechaFormateada($fecha,$formato="d-m-Y") {
			
			if (($fecha != null && $fecha != '0000:00:00' && $fecha != '' && $fecha != '0000-00-00' )) {
				return date_format(date_create($fecha),$formato);
			}else {
				return '';
			}
		}
		

		public static function fechaComparada($fecha,$conHMS=true){
			//if (preg_match('/^(<>)|(<=)|(>=)|(<)|(>)|(=)(.*)/', $fecha,$matches)) {
			if (preg_match('/^((<>)|(<=)|(>=)|(<)|(>)|(=))/', $fecha,$comparador)) {
				$fechaLimpia = str_replace($comparador[0], '', $fecha);
				return $comparador[0].' '.self::jpicker2db($fechaLimpia);
			}else{
                return self::jpicker2db($fecha);
            }
					
		}
		
			//------    CONVERTIR NUMEROS A LETRAS         ---------------
		//------    M�xima cifra soportada: 18 d�gitos con 2 decimales
		//------    999,999,999,999,999,999.99
		// NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE BILLONES
		// NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE MILLONES
		// NOVECIENTOS NOVENTA Y NUEVE MIL NOVECIENTOS NOVENTA Y NUEVE PESOS 99/100 M.N.
		//------    Creada por:                        ---------------
		//------             ULTIMINIO RAMOS GAL�N     ---------------
		//------            uramos@gmail.com           ---------------
		//------    10 de junio de 2009. M�xico, D.F.  ---------------
		//------    PHP Version 4.3.1 o mayores (aunque podr�a funcionar en versiones anteriores, tendr�as que probar)
		public static function numtoletras($xcifra)
		{
		    $xarray = array(0 => "Cero",
		        1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
		        "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
		        "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
		        100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		    );
		//
		    $xcifra = trim($xcifra);
		    $xlength = strlen($xcifra);
		    $xpos_punto = strpos($xcifra, ".");
		    $xaux_int = $xcifra;
		    $xdecimales = "00";
		    if (!($xpos_punto === false)) {
		        if ($xpos_punto == 0) {
		            $xcifra = "0" . $xcifra;
		            $xpos_punto = strpos($xcifra, ".");
		        }
		        $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
		        $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		    }
		
		    $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		    $xcadena = "";
		    for ($xz = 0; $xz < 3; $xz++) {
		        $xaux = substr($XAUX, $xz * 6, 6);
		        $xi = 0;
		        $xlimite = 6; // inicializo el contador de centenas xi y establezco el l�mite a 6 d�gitos en la parte entera
		        $xexit = true; // bandera para controlar el ciclo del While
		        while ($xexit) {
		            if ($xi == $xlimite) { // si ya lleg� al l�mite m�ximo de enteros
		                break; // termina el ciclo
		            }
		
		            $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
		            $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres d�gitos)
		            for ($xy = 1; $xy < 4; $xy++) { // ciclo para revisar centenas, decenas y unidades, en ese orden
		                switch ($xy) {
		                    case 1: // checa las centenas
		                        if (substr($xaux, 0, 3) < 100) { // si el grupo de tres d�gitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
		                            
		                        } else {
		                            $key = (int) substr($xaux, 0, 3);
		                            if (TRUE === array_key_exists($key, $xarray)){  // busco si la centena es n�mero redondo (100, 200, 300, 400, etc..)
		                                $xseek = $xarray[$key];
		                                $xsub = self::subfijo($xaux); // devuelve el subfijo correspondiente (Mill�n, Millones, Mil o nada)
		                                if (substr($xaux, 0, 3) == 100)
		                                    $xcadena = " " . $xcadena . " CIEN " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
		                            }
		                            else { // entra aqu� si la centena no fue numero redondo (101, 253, 120, 980, etc.)
		                                $key = (int) substr($xaux, 0, 1) * 100;
		                                $xseek = $xarray[$key]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
		                                $xcadena = " " . $xcadena . " " . $xseek;
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 0, 3) < 100)
		                        break;
		                    case 2: // checa las decenas (con la misma l�gica que las centenas)
		                        if (substr($xaux, 1, 2) < 10) {
		                            
		                        } else {
		                            $key = (int) substr($xaux, 1, 2);
		                            if (TRUE === array_key_exists($key, $xarray)) {
		                                $xseek = $xarray[$key];
		                                $xsub = self::subfijo($xaux);
		                                if (substr($xaux, 1, 2) == 20)
		                                    $xcadena = " " . $xcadena . " VEINTE " . $xsub;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                                $xy = 3;
		                            }
		                            else {
		                                $key = (int) substr($xaux, 1, 1) * 10;
		                                $xseek = $xarray[$key];
		                                if (20 == substr($xaux, 1, 1) * 10)
		                                    $xcadena = " " . $xcadena . " " . $xseek;
		                                else
		                                    $xcadena = " " . $xcadena . " " . $xseek . " Y ";
		                            } // ENDIF ($xseek)
		                        } // ENDIF (substr($xaux, 1, 2) < 10)
		                        break;
		                    case 3: // checa las unidades
		                        if (substr($xaux, 2, 1) < 1) { // si la unidad es cero, ya no hace nada
		                            
		                        } else {
		                            $key = (int) substr($xaux, 2, 1);
		                            $xseek = $xarray[$key]; // obtengo directamente el valor de la unidad (del uno al nueve)
		                            $xsub = self::subfijo($xaux);
		                            $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
		                        } // ENDIF (substr($xaux, 2, 1) < 1)
		                        break;
		                } // END SWITCH
		            } // END FOR
		            $xi = $xi + 3;
		        } // ENDDO
		
		        if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
		            $xcadena.= " DE";
		
		        if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
		            $xcadena.= " DE";
		
		        // ----------- esta l�nea la puedes cambiar de acuerdo a tus necesidades o a tu pa�s -------
		        if (trim($xaux) != "") {
		            switch ($xz) {
		                case 0:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN BILLON ";
		                    else
		                        $xcadena.= " BILLONES ";
		                    break;
		                case 1:
		                    if (trim(substr($XAUX, $xz * 6, 6)) == "1")
		                        $xcadena.= "UN MILLON ";
		                    else
		                        $xcadena.= " MILLONES ";
		                    break;
		                case 2:
		                    if ($xcifra < 1) {
		                        $xcadena = "CERO PESOS con $xdecimales/100 ";
		                    }
		                    if ($xcifra >= 1 && $xcifra < 2) {
		                        $xcadena = "UN PESO con $xdecimales/100 ";
		                    }
		                    if ($xcifra >= 2) {
		                        $xcadena.= " PESOS con $xdecimales/100 "; //
		                    }
		                    break;
		            } // endswitch ($xz)
		        } // ENDIF (trim($xaux) != "")
		        // ------------------      en este caso, para M�xico se usa esta leyenda     ----------------
		        $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
		        $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
		        $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
		        $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		    } // ENDFOR ($xz)
		    return trim($xcadena);
		}
		
		// END FUNCTION
		
		public static function subfijo($xx)
		{ // esta funci�n regresa un subfijo para la cifra
		    $xx = trim($xx);
		    $xstrlen = strlen($xx);
		    if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
		        $xsub = "";
		    //
		    if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
		        $xsub = "MIL";
		    //
		    return $xsub;
		}

        public static function print_array($arr,$return = null){
		    if ($return == true){
                return '<pre>'.print_r($arr,$return).'</pre>';
            }else{
                echo '<pre>';
                print_r($arr,$return);
                echo '</pre>';
            }

        }

        public static function validatesAsInt($number)
        {
            $number = filter_var($number, FILTER_VALIDATE_INT);
            return ($number !== FALSE);
        }

        public static function getActivePrinters() {

            $o = shell_exec("lpstat -d -p");
            $res = explode("\n", $o);
            $i = 0;
            foreach ($res as $r) {
                $active = 0;
                if (strpos($r, "printer") !== FALSE) {
                    $r = str_replace("printer ", "", $r);
                    if (strpos($r, "is idle") !== FALSE)
                        $active = 1;

                    $r = explode(" ", $r);

                    $printers[$i]['name'] = $r[0];
                    $printers[$i]['active'] = $active;
                    $i++;
                }
            }
            return $printers;
        }
    }



?>