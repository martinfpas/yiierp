<?php

class MateriaPrimaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','ImportarDatosCsv','SaveAsync','Select2'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionImportarDatosCsv() {


        $fp = fopen('/var/www/erca/upload/MateriaPrima.csv', 'r');
        if($fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;

            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                echo '<br>Procesando indice '.$ix.' * >'.$line[0];
                /*
                echo '<br>';
                ComponentesComunes::print_array($line);
                die;
                */

                $oMateriaPrima = MateriaPrima::model()->findByAttributes(
                    array(
                        'id_materia_prima'=>$line[0],
                        'id_rubro' => $line[2],
                        'id_articulo' => $line[3],
                        'Id_Proveedor' => $line[6],
                    )
                );
                $nuevo = false;
                if ($oMateriaPrima == null){
                    $nuevo = true;
                    $oMateriaPrima = new MateriaPrima();
                    $oMateriaPrima->id_materia_prima = $line[0];
                    $oMateriaPrima->id_rubro = $line[2];
                    $oMateriaPrima->id_articulo = $line[3];
                    $oMateriaPrima->Id_Proveedor = $line[6];
                }

                $oMateriaPrima->Descripcion = utf8_encode($line[1]);
                /*
                $oArticulo = Articulo::model()->findByAttributes(array('id_rubro' => $line[1],'id_articulo' => $line[3]));
                if ($oArticulo != null){
                    $oMateriaPrima->id_articulo = $line[3];
                }
                */

                $oMateriaPrima->Unidad_Medida = utf8_encode($line[4]);
                $oMateriaPrima->Id_Proveedor = $line[6];
                $oMateriaPrima->Precio = floatval($line[5]);

                if($line[7] != '' && $line[7] != '?'){
                    $oMateriaPrima->fecha_act_precio = ComponentesComunes::jpicker2db($line[7]);
                }else{
                    $oMateriaPrima->fecha_act_precio = null;
                }

                ComponentesComunes::print_array($oMateriaPrima->attributes);

                if (!$oMateriaPrima->validate()) {
                    die();
                }
                try{
                    if (!$oMateriaPrima->save()) {
                        echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                        ComponentesComunes::print_array($oMateriaPrima->attributes);
                        print_r($oMateriaPrima->getErrors());
                    }else{
                        if ($nuevo){
                            echo ' Nuevo Registro !! :: '.$line[0].' - '.$line[2].' - '.$line[3].':: ';
                        }else{
                            echo ' SE actualizo '.$oMateriaPrima->id.' :: '.$line[0].' - '.$line[2].' - '.$line[3].':: ';
                        }
                    }
                }catch(CDbException $e){
                    die($e->getMessage());
                }



            };

        }
    }

    public function actionSelect2(){

        $criteria = new CDbCriteria();

        if (isset($_GET['id_rubro']) && $_GET['id_rubro'] > 0){
            $criteria->compare('id_rubro',$_GET['id_rubro']);
        }

        if (isset($_GET['id_articulo']) && $_GET['id_articulo'] > 0){
            $criteria->compare('id_articulo',$_GET['id_articulo']);
        }

        if(ComponentesComunes::validatesAsInt($_GET['q'])){
            $criteria->compare('id_materia_prima',$_GET['q']);
        }else{
            $criteria->compare('Descripcion',$_GET['q'],true);
            $criteria->limit = $_GET['page_limit'];
            //$criteria->offset = $_GET['page'] * $_GET['page_limit'];
        }

        $models = MateriaPrima::model()->todas()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id,'text' => $oModel->Descripcion);
        }

        echo CJSON::encode($aModels);
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.MateriaPrima::model()->getAttributeLabel('model').' #'.$id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MateriaPrima;
		$this->pageTitle = "Nuevo ".MateriaPrima::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MateriaPrima']))
		{
			$model->attributes=$_POST['MateriaPrima'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new MateriaPrima;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MateriaPrima']))
		{
			$model->attributes=$_POST['MateriaPrima'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.MateriaPrima::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MateriaPrima']))
		{
			$model->attributes=$_POST['MateriaPrima'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new MateriaPrima('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MateriaPrima']))
			$model->attributes=$_GET['MateriaPrima'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MateriaPrima('search');
		$this->pageTitle = 'Administrando '.MateriaPrima::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MateriaPrima'])){
			$model->attributes=$_GET['MateriaPrima'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MateriaPrima the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MateriaPrima::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MateriaPrima $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='materia-prima-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
