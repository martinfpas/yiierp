<?php

class ArtVentaEnvaseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','viewFilaGrillaAsync','SaveAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_art_envases")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver ArtVentaEnvase #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionViewFilaGrillaAsync($id_artVenta) {
		$model = new ArtVentaEnvase();
		//TODO: CONTROLAR SI EL ARTICULO_VTA CON ID = $id_artVenta EXISTE
		$model->id_artVenta = $id_artVenta;
		
		$aArtVentaEnvase = new ArtVentaEnvase('search');
		$aArtVentaEnvase->unsetAttributes();
		$aArtVentaEnvase->id_artVenta = $id_artVenta;
				
		$this->renderPartial('_formFila',array(
			'aArtVentaEnvase' => $aArtVentaEnvase,
			'model' =>$model,			
		),false,true);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ArtVentaEnvase;
		$this->pageTitle = "Nuevo ArtVentaEnvase";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArtVentaEnvase']))
		{
			$model->attributes=$_POST['ArtVentaEnvase'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionSaveAsync()
    {
        $model=new ArtVentaEnvase;
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['ArtVentaEnvase']))
        {
            $model->attributes=$_POST['ArtVentaEnvase'];
            if($model->save()){
                echo $model->id;
            }else {
                throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }
        }
    }
	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando ArtVentaEnvase #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArtVentaEnvase']))
		{
			$model->attributes=$_POST['ArtVentaEnvase'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ArtVentaEnvase('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ArtVentaEnvase']))
			$model->attributes=$_GET['ArtVentaEnvase'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ArtVentaEnvase('search');
		$this->pageTitle = 'Administrando Art Venta Envases ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ArtVentaEnvase'])){
			$model->attributes=$_GET['ArtVentaEnvase'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArtVentaEnvase the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ArtVentaEnvase::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArtVentaEnvase $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='art-venta-envase-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
