<?php

class NotaEntregaController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','FormAsync','PdfxCarga','SaveFieldAsync','Json','Pdf','PdfSinPrecio'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_notaEntrega")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionJson($id){
        $oComprobante = $this->loadModel($id);
        /*
        if($oComprobante->estado < Comprobante::iCerrada){
            $oComprobante->recalcular();
            if (!$oComprobante->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobante));
            }
        }*/
        $attributes = $oComprobante->attributes;
        $attributes['AlicuotaItem'] = $oComprobante->getAlicuotaItem();
        $attributes['subtotal'] = number_format($oComprobante->SubTotal,2,',','.');
        $attributes['totalFinal'] =  number_format($attributes['totalFinal'],2,',','.');
        echo json_encode($attributes);
    }

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oNotaEntrega = NotaEntrega::model()->findByPk($_POST['pk']);
        if ($oNotaEntrega != null) {
            $oNotaEntrega->setAttribute($_POST['name'],$_POST['value']);
            $oNotaEntrega->fecha = (ComponentesComunes::jpicker2db($oNotaEntrega->fecha) == '0000-00-00')? null : ComponentesComunes::jpicker2db($oNotaEntrega->fecha);
            if($_POST['name'] == 'descuento1' || $_POST['name'] == 'descuento2'){
                Yii::log('$model->totalFinal:: ','warning');
                $oNotaEntrega->totalFinal = $oNotaEntrega->getTotalFinal();
            }
            if (!$oNotaEntrega->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oNotaEntrega));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver NotaEntrega #'.$id;
		$aProdNotaEntrega = new ProdNotaEntrega('search');
        $aProdNotaEntrega->unsetAttributes();
        $aProdNotaEntrega->idNotaEntrega = $id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'aProdNotaEntrega' => $aProdNotaEntrega,
		));
	}



	public function actionPdfxCarga($idCarga = null,$print=false)
    {
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,10,5,0,5,0,'P');
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','11','8pin', 6,0,5,5,5,0,'P');
            }
        }else{
            //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','couriernew', 15,15,20,20,5,0,'P');
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,10,5,0,5,0,'P');
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','11','8pin', 6,0,5,5,5,0,'P');
        }

        $this->layout = 'column1Pdf';


        //TODO: FILTRAR POR CARGA $mPDF1->AddPage();
        $criteria = new CDbCriteria();
        $criteria->join = 'LEFT JOIN notaPedido np on t.idNotaPedido=np.id and np.idCarga ='.$idCarga;
        //$criteria->condition = 'np.idCarga ='.$idCarga;

        //TODO: FILTRAR POR CARGA
        $aNotasEntrega = NotaEntrega::model()->findAll($criteria);

        foreach ($aNotasEntrega as $index => $oNotasEntrega) {

            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,10,5,0,5,0,'P');
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew' , 15,15,5,5,5,0,'P');
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfNotaEntrega.css');
            $mPDF1->WriteHTML($stylesheet, 1);
            $mPDF1->shrink_tables_to_fit=0;

            /* @var $oNotasEntrega NotaEntrega */
            $aProds = new ProdNotaEntrega('search');
            $aProds->unsetAttributes();
            $aProds->idNotaEntrega = $oNotasEntrega->id;
            if ($oNotasEntrega->impSinPrecio){
                $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                        'model'=>$oNotasEntrega,
                        'aProdNotaEntrega' => $aProds,
                    ),true)
                );
            }else{
                $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                        'model'=>$oNotasEntrega,
                        'aProdNotaEntrega' => $aProds,
                    ),true)
                );
            }

            //$mPDF1->AddPage();
            $filename = 'NotaDeEntrega_'.$oNotasEntrega->id.'.pdf';
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

        }

        $mPDF1->adjustFontDescLineheight = 1;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedTextBaseline = false;


        /*
        $filename = 'NotasDeEntrega_'.$idCarga.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
        */

        if ($print){

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
    }


    /**
     * Displays a particular model  in pdf.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionPdf($id,$print=false)
    {
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 12,7,10,0,0,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 12,7,10,0,0,0,'P');
        }



        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfNotaEntrega.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        //$this->layout = 'column1Pdf';

        $aProds = new ProdNotaEntrega('search');
        $aProds->unsetAttributes();
        $aProds->idNotaEntrega = $id;

/*
        echo $this->renderPartial('viewPdf',array(
            'model'=>$this->loadModel($id),
            'aProdNotaEntrega' => $aProds,
        ));

        die('2');
*/


        $mPDF1->adjustFontDescLineheight = 1;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedTextBaseline = false;

        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$this->loadModel($id),
                'aProdNotaEntrega' => $aProds,
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'NotaDeEntrega_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

    /**
     * Displays a particular model  in pdf.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionPdfSinPrecio($id,$print=false)
    {

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','8pin', 15,15,20,20,5,0,'P');//
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','14','8pin', 10,0,5,5,5,0,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 12,7,10,0,0,0,'P');
            }
        }else{
            //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','couriernew', 15,15,20,20,5,0,'P');
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew' , 15,15,5,5,5,0,'P');
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','14','8pin', 10,0,5,5,5,0,'P');
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 12,7,10,0,0,0,'P');
        }

        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfNotaEntrega.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $this->layout = 'column1Pdf';

        $aProds = new ProdNotaEntrega('search');
        $aProds->unsetAttributes();
        $aProds->idNotaEntrega = $id;

        $mPDF1->adjustFontDescLineheight = 1;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedTextBaseline = false;

        $mPDF1->WriteHTML($this->renderPartial('viewPdfSPrecios',array(
                'model'=>$this->loadModel($id),
                'aProdNotaEntrega' => $aProds,
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'NotaDeEntrega_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }
    /**
     * Displays a particular model partialy to use in ajax call
     *
     * @param integer $id the ID of the model to be displayed (if you will update)
     * @param integer $idNotaPedido the ID of the associated NotaPedido
     */
    public function actionFormAsync($idNotaPedido,$id = null)
    {

        $is_modal = isset($_GET['is_modal']);

        $aProdNotaEntrega = new ProdNotaEntrega('search');
        $aProdNotaEntrega->unsetAttributes();
        $modelProd = new ProdNotaEntrega();

        if ($id > 0){
            $model = $this->loadModel($id);
            $aProdNotaEntrega->idNotaEntrega = $id;
            $modelProd->idNotaEntrega = $id;
        }else{
            if ($idNotaPedido > 0){
                $model = NotaEntrega::generarNe($idNotaPedido);
            }else{
                //TODO: IMPLEMENTAR...
                $model = new NotaEntrega();
                Yii::log('new NotaEntrega()','warning');
                if(isset($_GET['idCliente'])){
                    $oCliente = Cliente::model()->findByPk($_GET['idCliente']);
                    $model->idCliente = $_GET['idCliente'];
                    if ($oCliente != null){
                        $model->nroViajante = $oCliente->codViajante;
                    }else{
                        throw new CHttpException('500','Cliente inexistente');
                    }
                }

                $model->clase = 'X';
                $model->idCamion = 6;
                $model->fecha = date('Y-m-d');
                if(!$model->save()){
                    throw new CHttpException('500',CHtml::errorSummary($model));
                }
            }
            $modelProd->idNotaEntrega = $model->id;
            $aProdNotaEntrega->idNotaEntrega = $model->id;

        }

        $this->renderPartial('_formFila',array(
            'model'=>$model,
            'aProdNotaEntrega' => $aProdNotaEntrega,
            'modelProd' => $modelProd,
            'ajaxMode' => true,
            'is_modal' => $is_modal
        ), false,true);
    }



	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new NotaEntrega;
		$this->pageTitle = "Nueva Nota de Entrega";
        $model->fecha = date('Y-m-d');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaEntrega']))
		{
			$model->attributes=$_POST['NotaEntrega'];
			if($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }else{
                throw new CHttpException('500',CHtml::errorSummary($model));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaEntrega']))
		{
		    if (isset($_POST['NotaEntrega']['id']) && $_POST['NotaEntrega']['id'] > 0 ){
		        $model = $this->loadModel($_POST['NotaEntrega']['id']);
            }else{
                $model=new NotaEntrega;
            }

            // SI VIENE EL PARAMETRO CERRAR, SE CIERRA LA NOTA DE ENTREGA
            if (isset($_POST['cerrar']) && $model->estado <= NotaEntrega::iGuardada ){
                $model->estado = NotaEntrega::iCerrada;
            }

			$model->attributes=$_POST['NotaEntrega'];
			if($model->save()){
                $model->totalFinal = $model->getTotalFinal();

                if($model->save()){
                    $model->totalFinal = $model->getTotalFinal();
                    echo $model->id;
                }else {
                    throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
                }

			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando NotaEntrega #'.$id;

        $aProdNotaEntrega = new ProdNotaEntrega('search');
        $aProdNotaEntrega->unsetAttributes();
        $aProdNotaEntrega->idNotaEntrega = $id;
        $modelProd = new ProdNotaEntrega();
        $modelProd->idNotaEntrega = $id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaEntrega']))
		{
			$model->attributes=$_POST['NotaEntrega'];
			if($model->save()){
                $this->redirect(array('view','id'=>$model->id));
            }else{
                throw new CHttpException('500',CHtml::errorSummary($model));
            }

		}

		$this->render('update',array(
			'model'=>$model,
            'aProdNotaEntrega' => $aProdNotaEntrega,
            'modelProd' => $modelProd,
            'is_modal' => false,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$oNE = NotaEntrega::model()->findByPk($id);
        if ($oNE != null){
            $oNE->delete();
        }else{
            Yii::log('SE INTENTO BORRAR DOS VECES ID:'+$id,'warning');
        }
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new NotaEntrega('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaEntrega']))
			$model->attributes=$_GET['NotaEntrega'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

	    $model=new NotaEntrega('search');
		$this->pageTitle = 'Administrando Nota Entregas ';

		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['NotaEntrega'])){
			$model->attributes=$_GET['NotaEntrega'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));

	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NotaEntrega the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NotaEntrega::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NotaEntrega $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='nota-entrega-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
