<?php

class EntregaDocController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print';
    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="27%" align="center">{PAGENO}/{nbpg}</td>
						<td width="30%" style="text-align: right;">{DATE j-m-Y}</td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ListarCamionesViajantes','ViewPrevia',
                    'Rendir','SaveFieldAsync','viewPdf','GetParamVal','PdfRendicion','DocRendicion','CrearFromCarga','Vincular','ViewDocReport',
                    'ViewDocReport2','HojasDeRuta','PlanillasRendicion','PrintRendicion','print','RendirPartial'
                ),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_carga_camiones")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionGetParamVal($id,$param){
	    $model = $this->loadModel($id);
	    if (isset($model[$param])){
            echo $model[$param];
        }
    }


    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oEntregaDoc = new EntregaDoc($_POST['scenario']);

        $oEntregaDoc = EntregaDoc::model()->findByPk($_POST['pk']);
        if ($oEntregaDoc != null) {
            $oEntregaDoc->setAttribute($_POST['name'],$_POST['value']);

            if (!$oEntregaDoc->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oEntregaDoc));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }


	public function actionListarCamionesViajantes(){
	    if (isset($_POST['EntregaDoc']['tipoPersona'])){
            if($_POST['EntregaDoc']['tipoPersona'] == 'CAM'){
                $camiones = Vehiculo::model()->findAll();
                $data=CHtml::listData($camiones,'id_vehiculo','IdNombre');
            }elseif ($_POST['EntregaDoc']['tipoPersona'] == 'VIA'){
                $viajantes = Viajante::model()->findAll();
                $data=CHtml::listData($viajantes,'numero','IdTitulo');
            }else{
                // TODO: DEVOLVER ERROR
                $data = array();
            }
        }else{
	        // TODO: DEVOLVER ERROR
            $data = array();
        }
        echo CHtml::tag('option', array('value'=>'','name'=>''),CHtml::encode('-- Seleccionar -- >'),true);
        foreach($data as $value=>$name)
        {
            echo CHtml::tag('option',
                array('value'=>$value),CHtml::encode($name),true);
        }


    }

    public function actionCrearFromCarga($idCarga){
	    $oCarga = Carga::model()->findByPk($idCarga);
	    if($oCarga == null){
	        Yii::log('Carga no encontrada :'.$idCarga,'error');
	        throw new CHttpException('500','Carga no encontrada :'.$idCarga );
        }else{
	        if ($oCarga->oEntregaDoc != null){
	            $this->redirect(array('view','id'=> $oCarga->oEntregaDoc->id));
            }
        }

        $aDocs = VDocumentosDeCarga::model()->findAllByAttributes(array('idCarga'=> $idCarga));
	    if (!(sizeof($aDocs) > 0)){
            throw new CHttpException('500','No Hay docs te carga');
        }
        $oDoc = new CargaDocumentos();

        $oEntregaDoc = new EntregaDoc();
        $oEntregaDoc->idCarga = $idCarga;
        $oEntregaDoc->tipoPersona = EntregaDoc::camion;
        $oEntregaDoc->nroCamViaj = $oCarga->idVehiculo;
        if (!$oEntregaDoc->save()){
            throw new CHttpException('500',CHtml::errorSummary($oEntregaDoc));
        }



        $i = 0;
        foreach ($aDocs as $index => $oDoc) {
            //$oDoc = new VDocumentosDeCarga();
            $oCargaDoc = new CargaDocumentos();
            Yii::log('$oEntregaDoc->id :'.$oEntregaDoc->id,'warning');

            if($oDoc->idFa != null){
                $oFactura = Factura::model()->findByPk($oDoc->idFa);
                $oCargaDoc->totalFinal = $oFactura->Total_Final;
                $oCargaDoc->idComprobante = $oDoc->idFa;
                $oCargaDoc->tipoDoc = CargaDocumentos::FAC;
            }else{
                $oNotaEntrega = NotaEntrega::model()->findByPk($oDoc->idNe);
                $oCargaDoc->totalFinal = $oNotaEntrega->totalFinal;
                $oCargaDoc->idComprobante = $oDoc->idNe;
                $oCargaDoc->tipoDoc = CargaDocumentos::NE;
            }
            $oCargaDoc->idEntregaDoc = $oEntregaDoc->id;
            $oCargaDoc->orden = $i;
            $oCargaDoc->comision = 1;
            $oCargaDoc->fechaCarga = date('Y-m-d');
            Yii::log('$oCargaDoc-> :****************'.ComponentesComunes::print_array($oCargaDoc->attributes,true),'warning');
            try{
                if (!$oCargaDoc->save()){
                    throw new CHttpException('500',CHtml::errorSummary($oCargaDoc));
                }
            }catch (CDbException $exception){
                Yii::log('CDbException :'.$exception->getMessage(),'warning');
            }
            $i++;
        }

        $this->redirect(array('viewPrevia','id' => $oEntregaDoc->id));


    }

    /**
    @params
        "id"=> id del comprobante
        "tipo"=>$data->tipo,
        "idEntregaDoc"

    */
    public function actionVincular($id,$tipo,$idEntregaDoc){
        /* @var $oCargaDocumentos CargaDocumentos */
        $oEntregaDoc = $this->loadModel($idEntregaDoc);
        $oCargaDoc = new CargaDocumentos();
        $oCargaDoc->idComprobante = $id;
        $oCargaDoc->idEntregaDoc = $idEntregaDoc;
        if($tipo == 'F'){
            $oCargaDoc->tipoDoc = CargaDocumentos::FAC;
        }else{
            $oCargaDoc->tipoDoc = $tipo;
        }

        if($tipo != 'NE'){
            if ($tipo == 'NC'){
                $oComprobante = Comprobante::model()->findByPk($id);
                $oCargaDoc->totalFinal = (-1) * abs($oComprobante->Total_Final);
            }else{
                $oComprobante = Comprobante::model()->findByPk($id);
                $oCargaDoc->totalFinal = $oComprobante->Total_Final;
            }
        }else{
            $oNotaEntrega = NotaEntrega::model()->findByPk($id);
            if ($oNotaEntrega != null){
                $oNotaEntrega->estado = NotaEntrega::iCerrada;
                if (!$oNotaEntrega->save()){
                    throw new CHttpException(500,'NotaEntrega no se pudo Cerrar :'.CHtml::errorSummary($oNotaEntrega));
                }
                $oCargaDoc->totalFinal = $oNotaEntrega->totalFinal;
            }else{
                throw new CHttpException(500,'NotaEntrega no encontrada');
            }
        }

        $oCargaDoc->orden = sizeof($oEntregaDoc->aCargaDocumentos)+1;
        foreach ($oEntregaDoc->aCargaDocumentos as $index => $oCargaDocumentos) {
            $nuevoCli = $oCargaDoc->getCliente();
            $oCli = $oCargaDocumentos->getCliente();
            
            if($nuevoCli && $oCli){
                if ($nuevoCli->cp == $oCli->cp && $nuevoCli->zp == $oCli->zp){
                    $oCargaDoc->orden = $oCargaDocumentos->orden;
                    break;
                }
            }

        }

        $oCargaDoc->comision = 1;
        $oCargaDoc->fechaCarga = date('Y-m-d');

        if (!$oCargaDoc->save()){
            throw new CHttpException('500',CHtml::errorSummary($oCargaDoc));
        }

        //$oCargaDoc->totalFinal

    }

    public function actionRendir($id){
        $this->pageTitle = 'Ver EntregaDoc #'.$id;
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->idEntregaDoc = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;
        $aCargaDocumentosCiudades = new VCargaDocumentosCiudades();
        $aCargaDocumentosCiudades->unsetAttributes();
        $aCargaDocumentosCiudades->idEntregaDoc = $id;

        $oEntregaGastos = new EntregaGtos();
        $oEntregaGastos->idEntregaDoc = $id;
        $aEntregaGastos = new EntregaGtos('search');
        $aEntregaGastos->unsetAttributes();
        $aEntregaGastos->idEntregaDoc = $id;

        $this->layout = '//layouts/column1';


        $this->render('rendir',array(
            'model'=>$this->loadModel($id),
            'oCargaDocumento' => $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'aCargaDocumentosCiudades' => $aCargaDocumentosCiudades,
            'oEntregaGastos' => $oEntregaGastos,
            'aEntregaGastos' => $aEntregaGastos,
        ));
    }

    public function actionRendirPartial($id,$idEntregaDoc){
        
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->id = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;

        $this->renderPartial('_formFilaRendir',array(
            'model'=> $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'oEntregaDoc' => $this->loadModel($idEntregaDoc),
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewPrevia($id)
    {
        $model = $this->loadModel($id);
        $this->pageTitle = 'Ver EntregaDoc #'.$id;
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->idEntregaDoc = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;
        $aCargaDocumentosCiudades = new VCargaDocumentosCiudades();
        $aCargaDocumentosCiudades->unsetAttributes();
        $aCargaDocumentosCiudades->idEntregaDoc = $id;
        $aComprobantesLibres = new VComprobantesSinCargaDoc('search');
        //$aComprobantesLibres->idComprobante = null;
        $aComprobantesLibres->unsetAttributes();

        if(isset($_GET['VComprobantesSinCargaDoc'])){
            $aComprobantesLibres->attributes = $_GET['VComprobantesSinCargaDoc'];
        }

        if ($model->oCarga != null){
            $aComprobantesLibres->idCamion = $model->oCarga->idVehiculo;
        }

        $this->render('viewPrevia',array(
            'model'=>$model,
            'oCargaDocumento' => $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'aCargaDocumentosCiudades' => $aCargaDocumentosCiudades,
            'aComprobantesLibres' => $aComprobantesLibres,
        ));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
	    if($model->oCarga->estado == Carga::iFacturada || $model->oCarga->estado == Carga::iBorrador){
            $this->redirect(array('ViewPrevia','id' =>$id));
        }else{
            $this->redirect(array('Rendir','id' =>$id));
        }

        $this->pageTitle = 'Ver EntregaDoc #'.$id;
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->idEntregaDoc = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;


        $this->render('view',array(
            'model'=> $model,
            'oCargaDocumento' => $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos
        ));
	}

	public function actionPdfRendicion($id,$print=false){

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','8pin', 15,15,20,20,5,0,'P');//
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','moderndot', 15,15,25,0,0,0,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','11','couriernew', 15,15,20,10,5,0,'P');
            }
        }else{
            //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','couriernew', 15,15,20,20,5,0,'P');
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','11','couriernew', 15,15,20,10,5,0,'P');
        }


        //$mPDF1 = new mPDF();
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $mPDF1->SetTitle('Rendicion Entrega Nº '.$id.'');


        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);


        //SE FILTRAN EN LA BUSQUEDA
        $aPagosCheque = new VPagosDeEntregaDoc('search');
        $aPagosCheque->unsetAttributes();
        $aPagosCheque->idEntregaDoc = $id;


        $this->pageTitle = 'Ver EntregaDoc #'.$id;
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->idEntregaDoc = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;
        $aEntregaGtos = new EntregaGtos('search');
        $aEntregaGtos->unsetAttributes();
        $aEntregaGtos->idEntregaDoc = $id;

        $this->layout = 'column1Pdf';

        $mPDF1->WriteHTML($this->renderPartial('viewpdfRendir',array(
            'model'=>$this->loadModel($id),
            'aEntregaGtos' => $aEntregaGtos,
            'oCargaDocumento' => $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'aPagosCheque' => $aPagosCheque,
        ),true));

        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'EntregaDoc_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

    public function actionDocRendicion($id){
        /* @var $oCargaDocumento CargaDocumentos */

        $this->layout = '//layouts/column1Doc';

        $oEntregaDoc = $this->loadModel($id);

        $aPagosCheque = new VPagosDeEntregaDoc('search');
        $aPagosCheque->unsetAttributes();
        $aPagosCheque->idEntregaDoc = $id;

        $this->pageTitle = 'Ver EntregaDoc #'.$id;
        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $aCargaDocumentos->idEntregaDoc = $id;
        $oCargaDocumento = new CargaDocumentos();
        $oCargaDocumento->idEntregaDoc = $id;
        $aEntregaGtos = new EntregaGtos('search');
        $aEntregaGtos->unsetAttributes();
        $aEntregaGtos->idEntregaDoc = $id;


        $models = CargaDocumentos::model()->findAll($aCargaDocumentos->searchWP()->criteria);

        foreach ($models as $index => $oCargaDocumento) {
            echo '<br>'.$oCargaDocumento->getMontoPasibleComision().' :: '.$oCargaDocumento->Cliente->oRubro->nombre.' :: '.$oCargaDocumento->Cliente->id_rubro;
            ComponentesComunes::print_array($oCargaDocumento->attributes);
        }

        ComponentesComunes::print_array($aCargaDocumentos);
        die();
        $this->render('viewDocRendir',array(
            'model'=>$oEntregaDoc,
            'aEntregaGtos' => $aEntregaGtos,
            'oCargaDocumento' => $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'aPagosCheque' => $aPagosCheque,
        )).'';


    }

    // FUNCIONA OK. MUESTRA LA HOJA DE RUTA EN PDF
    public function actionViewPdf($id,$print=false)
    {
        $printer = '';
        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }
        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','11','couriernew', 15,10,30,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','11','couriernew', 10,10,30,25,8,0,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHeader($this->pdfHeader,null,true);
        $this->layout = 'column1Pdf';

        $mPDF1->shrink_tables_to_fit=0;

        $oEntregaDoc = $this->loadModel($id);

        $criteria=new CDbCriteria;
        $criteria->with = array('oComprobante');
        $criteria->compare('idEntregaDoc',$id);
        $criteria->order = 't.orden,t.razonSocial';
        $aCargaDocumentos2 = CargaDocumentos::model()->findAll($criteria);

        $mPDF1->WriteHTML($this->renderPartial('viewpdfHojaDeRuta',array(
                'model'=>$this->loadModel($id),
                //'aProds' => $aProds,
                'aCargaDocumentos' => $aCargaDocumentos2
            ),true)
        );


        $filename = 'HojaDeRuta_'.$oEntregaDoc->idCarga.'.pdf';

        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage,"FanFoldGerman",25,18,0); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

    //
    public function actionPrint($id)
    {
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ($printer == ''){
            die('Definir impresora');
        }

        $oEntregaDoc = $this->loadModel($id);

        $criteria=new CDbCriteria;
        $criteria->with = array('oComprobante');
        $criteria->compare('idEntregaDoc',$id);
        $criteria->order = 't.orden,t.razonSocial';
        $aCargaDocumentos2 = CargaDocumentos::model()->findAll($criteria);
        $aCargaDocumentos = array();
        $i = 0;
        $nCliAnterior = null;
        $xlsOffset = 4;
        $sinBordeUp = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_NONE,
                    'color' => array('argb' => 'FFFF0000'),
                ),
            ),
        );
        $Borde2 = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('argb' => PHPExcel_Style_Color::COLOR_RED),
                ),
                'left' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('argb' => PHPExcel_Style_Color::COLOR_RED),
                ),
            ),
        );
        $r = new YiiReport(array('template'=> 'DocsAEntregar.xlsx'));
        $aStylesRow = array();
        foreach ($aCargaDocumentos2 as $index => $oCargaDocumento) {
            $nCli = $oCargaDocumento->getCliente()->id;
            $rownum = $i + $xlsOffset;
            if(($nCliAnterior == null) || $nCliAnterior != $nCli){ //
                $aCargaDocumentos[$i]['nCli'] = $nCli;
                $aCargaDocumentos[$i]['razonSocial'] = $oCargaDocumento->getCliente()->title;
                $aCargaDocumentos[$i]['localidad'] = $oCargaDocumento->getCliente()->Localidad;
            }else{
                Yii::log("A".$rownum.":E".$rownum,'warning');
                $aStylesRow[] = "A".$rownum.":E".$rownum;
                //$r->getobjPHPExcel()->getActiveSheet()->getStyle("A".$rownum.":E".$rownum)->applyFromArray($Borde2);
                //$r->getobjPHPExcel()->getActiveSheet()->getStyle("A4:E4")->applyFromArray($sinBordeUp);
                $aCargaDocumentos[$i]['nCli'] = '';
                $aCargaDocumentos[$i]['razonSocial'] = '';
                $aCargaDocumentos[$i]['localidad'] = '';
            }

            $aCargaDocumentos[$i]['importe'] = number_format($oCargaDocumento->totalFinal,2,',','.');
            $aCargaDocumentos[$i]['observacion'] = '*'.$rownum;

            $nCliAnterior = $nCli;
            $i++;
        }
        //$r->getobjPHPExcel()->getActiveSheet()->getStyle("A4:E4")->applyFromArray($sinBordeUp);

        $filename = 'HojaDeRuta_'.$oEntregaDoc->idCarga;
        $fullPathFile = $this->printFilesPath.'/'.$filename.'.xlsx';

        if(file_exists($fullPathFile)){
            Yii::log('Paso eliminar','warning');
            //unlink($fullPathFile);
            chmod($fullPathFile,0777);
        }

        $r->load(array(
                array(
                    'id' => 'doc',
                    'repeat'=>true,
                    'data' => $aCargaDocumentos,
                    'minRows'=>1,
                ),
            )
        );



        $objPHPExcel = PHPExcel_IOFactory::createWriter($r->getobjPHPExcel(), 'Excel2007');

        $r->render('Excel2007', $this->printFilesPath.'/'.$filename,true);
        chmod($fullPathFile,0777);

        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($fullPathFile);

        foreach ($aStylesRow as $index => $sRow) {
            Yii::log('$sRow:'.$sRow,'warning');
            $excel2->getActiveSheet()->getStyle($sRow)->applyFromArray($sinBordeUp);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        $objWriter->save($fullPathFile);
        chmod($fullPathFile,0777);

        //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

        echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
        //echo $r->render('excel2007',$filename);

    }

    // ES DE PRUEBA. MUESTRA LA HOJA DE RUTA EN HTML PURO
    public function actionViewPdf2($id)
    {
        $mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'11','couriernew', 5,5,25,5,5,0,'P');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHeader($this->pdfHeader,null,true);
        $this->layout = 'column1Doc';

        $oEntregaDoc = $this->loadModel($id);

        $criteria=new CDbCriteria;
        $criteria->with = array('oComprobante');
        $criteria->compare('idEntregaDoc',$id);
        $criteria->order = 't.orden,t.razonSocial';
        $aCargaDocumentos2 = CargaDocumentos::model()->findAll($criteria);

        $this->render('viewpdfForEach',array(
                'model'=>$oEntregaDoc,
                //'aProds' => $aProds,
                'aCargaDocumentos' => $aCargaDocumentos2
            ),false);

    }

    // EXPORTA EN DOC PERO NO TOMA LOS CSS
    public function actionViewDocReport2($id)
    {

        //FUNCIONA PERO SOLO DESCARGA EL ARCHIVO
        require_once Yii::getPathOfAlias('application.extensions')."/PhpWord/vendor/autoload.php";

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->setDefaultFontName('courier');
        /* Note: any element you append to a document must reside inside of a Section. */



        // Adding an empty Section to the document...
        $section = $phpWord->addSection(array(
            'marginLeft'   => 200,
            'marginRight'  => 200,
            'marginTop'    => 200,
            'marginBottom' => 200,
            'headerHeight' => 150,
            'footerHeight' => 150,
        ));

        $header = $section->addHeader();
        $header = $section->addHeader();
        $header->addText(htmlspecialchars('Subsequent pages in Section 1 will Have this!'));


        // Adding Text element to the Section having font styled by default...
        $this->layout = 'column1Doc';
        $oEntregaDoc = $this->loadModel($id);

        $criteria=new CDbCriteria;
        $criteria->with = array('oComprobante');
        $criteria->compare('idEntregaDoc',$id);
        $criteria->order = 't.orden,t.razonSocial';
        $aCargaDocumentos2 = CargaDocumentos::model()->findAll($criteria);

        $filename = "HojaDeRuta_$oEntregaDoc->idCarga.docx";

        header("Pragma: no-cache");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment; filename=\"$filename");
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');

        $html = $this->renderPartial('viewpdfForEach',array(
            'model'=>$this->loadModel($id),
            'aCargaDocumentos' => $aCargaDocumentos2
        ),true);

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, '<div>'.$html.'</div>', false);



        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        ob_clean();
        $objWriter->save("php://output");
        flush();
        Yii::app()->end();

    }

    public function actionViewDocReport($id)
    {
                $this->layout = 'column1Doc';
                $oEntregaDoc = $this->loadModel($id);

                $criteria=new CDbCriteria;
                $criteria->with = array('oComprobante');
                $criteria->compare('idEntregaDoc',$id);
                $criteria->order = 't.orden,t.razonSocial';
                $aCargaDocumentos2 = CargaDocumentos::model()->findAll($criteria);

                //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');

                $html = ''.$this->renderPartial('viewDocForEach',array(
                    'model'=>$oEntregaDoc,
                    'aCargaDocumentos' => $aCargaDocumentos2
                ),true).'';

                $filename = "HojaDeRuta_$oEntregaDoc->idCarga.doc";

                header("Pragma: no-cache");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Cache-Control: private", false);
                header("Content-type: application/vnd.ms-word");
                header('Content-Disposition: attachment; filename="'.$filename.'"');
                header("Content-Transfer-Encoding: binary");
                ob_clean();
                flush();
                echo $html;
                Yii::app()->end();
                ////////////////////////////////////*/
    }


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EntregaDoc;
		$this->pageTitle = "Nuevo EntregaDoc";
        $model->tipoPersona = null;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EntregaDoc']))
		{
			$model->attributes=$_POST['EntregaDoc'];
			if($model->save()){
                $idCarga = $model->cargarDocumentos();
                if($idCarga > 0){
                    $model->idCarga = $idCarga;
                    if(!$model->save()){
                        throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
                    }
                }
                $this->redirect(array('ViewPrevia','id'=>$model->id));
            }else{
			    //TODO: VER QUE HACER EN ESTE CASO
                Yii::log('Error al guardar idCarga '.CHtml::errorSummary($model),'error');
                throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new EntregaDoc;
		$this->pageTitle = "Nuevo EntregaDoc";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EntregaDoc']))
		{
			$model->attributes=$_POST['EntregaDoc'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando EntregaDoc #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EntregaDoc']))
		{
			$model->attributes=$_POST['EntregaDoc'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new EntregaDoc('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EntregaDoc']))
			$model->attributes=$_GET['EntregaDoc'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EntregaDoc('search');
		$this->pageTitle = 'Administrando Entrega Docs ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EntregaDoc'])){
			$model->attributes=$_GET['EntregaDoc'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}
    /**
     * Manages all models.
     */
    public function actionPlanillasRendicion()
    {
        $model=new EntregaDoc('search');
        $this->pageTitle = 'Administrando Entrega Docs ';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['EntregaDoc'])){
            $model->attributes=$_GET['EntregaDoc'];
        }else{

        }

        $this->render('planillasRendicion',array(
            'model'=>$model,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionHojasDeRuta()
    {
        $model=new EntregaDoc('search');
        $this->pageTitle = 'Administrando Entrega Docs ';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['EntregaDoc'])){
            $model->attributes=$_GET['EntregaDoc'];
        }else{

        }

        $this->render('hojasDeRuta',array(
            'model'=>$model,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EntregaDoc the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EntregaDoc::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EntregaDoc $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='entrega-doc-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
