<?php

class NotaPedidoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','adminHistorico','SaveAsync','SinNcarga','SaveFieldAsync','AsignCargaAsync','Existe','Pdf','DesvincularCarga'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/* @var $oProdNotaPedido ProdNotaPedido*/
	public function actionDesvincularCarga($id){
        $model = $this->loadModel($id);

        if ($model->oCarga != null){
            foreach ($model->aProdNotaPedido as $index => $oProdNotaPedido) {
                $model->oCarga->descontarProd($oProdNotaPedido->idArtVenta, $oProdNotaPedido->cantidad,$oProdNotaPedido->idPaquete);
            }
        }

        try{
            if($model->oCarga != null){
				$model->oCarga->recalcularDepuracion = true;
				if (!$model->oCarga->save()){
					// TODO: MANEJAR LA EXCEPCION
					Yii::log('Error:'.CHtml::errorSummary($model->oCarga),'error');
				}
            }
        }catch (Exception $e){

        }

        $idCargaPrevia = $model->idCarga;
        $model->idCarga = null;
        if(!$model->save()){
            Yii::log('Error '.CHtml::errorSummary($model),'error');
        }

        //TODO: FALTA DEFINIR BIEN A DONDE DEBE IR
        $this->redirect(array('carga/view','id'=> $idCargaPrevia));

    }

	 public function actionAsignCargaAsync($idNotaPedido,$idCarga){

	    $oNotaPedido = NotaPedido::model()->findByPk($idNotaPedido);

        $oCarga = Carga::model()->findByPk($idCarga);

        if ($idNotaPedido == null || $idCarga == null){
            Yii::log('Error de Parametros','warning');
            throw CHttpException('500','Error de Parametros');
        }
        if ($oNotaPedido == null ){
            Yii::log('La Nota No Existe','warning');
            throw CHttpException('500','La Nota No Existe');
        }
        if ($oCarga == null ){
            Yii::log('La Carga No Existe','warning');
            throw CHttpException('500','La Carga No Existe');
        }
         Yii::log(' > $idNotaPedido > '.$idNotaPedido.' $idCarga->'.$idCarga,'warning');
        $oNotaPedido->saveIdCarga($idCarga);
         Yii::log(' > $idNotaPedido *****','warning');

    }


	public function actionSinNcarga($idCarga = null, $agregar = false,$idCamion = null){

        if (isset($_POST['Carga']) || $idCamion != null){
            if(isset($_POST['Carga']['id'])){
                $idCarga = $_POST['Carga']['id'];
            }

            if (isset($_POST['Carga']['idVehiculo'])){
                $camion = $_POST['Carga']['idVehiculo'];
            }else{
                $camion = $idCamion;
            }
            $aNotaPedido = new NotaPedido('search');
            $aNotaPedido->camion = $camion;

            $this->renderPartial('_adminSelect', array('aNotaPedido' => $aNotaPedido, 'idCarga' => $idCarga, 'bAgregar' => isset($agregar) ), false,true);
        }else{
            throw new CHttpException('500','Error de parámetros');
        }

    }

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oNotaPedido = new NotaPedido($_POST['scenario']);

        $oNotaPedido = NotaPedido::model()->findByPk($_POST['pk']);
        if ($oNotaPedido != null) {
            $oNotaPedido->setAttribute($_POST['name'],$_POST['value']);

            if (!$oNotaPedido->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oNotaPedido));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }

    /**
     * Displays pdf a particular model.
     * @param integer $id the ID of the model to be displayed
     */
	public function actionPdf($id)
	{
        $mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'12','couriernew');

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        $mPDF1->WriteHTML($stylesheet, 1);

		$this->pageTitle = 'Imprimir Nota de Pedido #'.$id;

		$aProds = new ProdNotaPedido('search');
		$aProds->unsetAttributes();
		$aProds->idNotaPedido = $id;
        $this->layout = 'column1Pdf';

        $mPDF1->WriteHTML(
            $this->renderPartial('viewPdf',array(
                'model'=>$this->loadModel($id),
                'aProdNotaPedido' => $aProds,
            ),true)
        );

        $mPDF1->Output('NotaDePedido_'.$id.'.pdf','I');

	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Ver NotaPedido #'.$id;

        $aProds = new ProdNotaPedido('search');
        $aProds->unsetAttributes();
        $aProds->idNotaPedido = $id;

        $this->render('view',array(
            'model'=>$this->loadModel($id),
            'aProdNotaPedido' => $aProds,
        ));
    }



    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionExiste($nroCombrobante,$dig)
    {
        $model = NotaPedido::model()->findByAttributes(array('nroCombrobante'=> $nroCombrobante,'dig'=> $dig));
        if ($model != null){// && ($model->oCarga == null || $model->oCarga->estado == Carga::iBorrador )){
            echo $model->id;
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($idCamion=null)
	{
		$model=new NotaPedido;
		$this->pageTitle = "Nueva Nota de Pedido";
		$model->fecha = date("Y-m-d H:i:s");
		if ($idCamion != null){
		    $model->camion = $idCamion;
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaPedido']))
		{
			$model->attributes=$_POST['NotaPedido'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new NotaPedido;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaPedido']))
		{
			if(isset($_POST['NotaPedido']['id']) && $_POST['NotaPedido']['id'] > 0){
				$model = $this->loadModel($_POST['NotaPedido']['id']);
			}
			$model->attributes=$_POST['NotaPedido'];
			$model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save()){
				echo $model->id;
				die();
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando NotaPedido #'.$id;


        if ($model->oCarga != null ){
            if ($model->oCarga->estado != Carga::iBorrador){
                // TODO: TIRAR EXCEPCION
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        if ($model->oFactura != null || $model->oNotaEntrega != null){
            $this->redirect(array('view','id'=>$model->id));
        }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaPedido']))
		{
			$model->attributes=$_POST['NotaPedido'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new NotaPedido('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaPedido']))
			$model->attributes=$_GET['NotaPedido'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new NotaPedido('search');
		$this->pageTitle = 'Administrando Nota Pedidos ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaPedido'])){
			$model->attributes=$_GET['NotaPedido'];
		}else{
			//$model->activo = NotaPedido::iActivo;
		}	

		$this->render('adminSinCarga',array(
			'model'=>$model,
		));
	}
    /**
     * Manages all models.
     */
    public function actionAdminHistorico()
    {
        $model=new NotaPedido('search');
        $this->pageTitle = 'Administrando Nota Pedidos ';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['NotaPedido'])){
            $model->attributes=$_GET['NotaPedido'];
        }else{
            //$model->activo = NotaPedido::iActivo;
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NotaPedido the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NotaPedido::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NotaPedido $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='nota-pedido-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
