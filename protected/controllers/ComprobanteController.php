<?php

class ComprobanteController extends Controller
{
    public $pdfHeader = '<table width="100%">
					<tr style="">
						<td width="33%" ><div style="width: 500px;float: left;font-size: small;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="33%" style="text-align: right;"></td>
						<td width="33%" align="center">{PAGENO} de {nbpg}</td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('ListadoIva','IvaVentas','FixExentos'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionFixExentos(){
        $criteria = new CDbCriteria;
        $criteria->addCondition("`fecha` LIKE '%2021-02%'");
        $model = Comprobante::model()->findAll($criteria);
        $i=0;
        foreach($model as $key => $oComprobante){
            if($oComprobante->netoGravado != $oComprobante->subtotalIva / 0.21){
                ComponentesComunes::print_array($oComprobante->attributes);
                //echo $i++." >".number_format($oComprobante->netoGravado * 0.21,2).' ->->'.number_format($oComprobante->subtotalIva,2);
                echo $i++." >".($oComprobante->netoGravado * 0.21).' ->->'.$oComprobante->subtotalIva;
                $oComprobante->netoGravado = $oComprobante->subtotalIva /0.21;
                $oComprobante->save();
            }

            /*
            $oComprobante->alicuotaIva = 21;
            $oComprobante->netoGravado = $oComprobante->Total_Final /1.21;
            $oComprobante->subtotalIva = $oComprobante->Total_Final - ($oComprobante->Total_Final /1.21);
            $oComprobante->save();
            */
        }
    }

	public function actionIvaVentas(){
        $model = new Comprobante();
	    $this->render('_form',array('model'=> $model));

    }

	public function actionListadoIva()
	{
	    /* @var $aComprobante Comprobante
         * @var $oResponable CategoriaIva
         */
		$model=new Comprobante('search');
		$this->pageTitle = 'Administrando '.Comprobante::model()->getAttributeLabel('models');

        $aModels = array();
        $startDate = '';
        $puesto = '';



        if(isset($_POST['Comprobante'])){
            $model->unsetAttributes();  // clear any default values
            $model->attributes = $_POST['Comprobante'];
            $puesto = ' del Puesto :'.$_POST['Comprobante']['Nro_Puesto_Venta'];
            $model->fecha = null;
            $criteria = $model->search()->criteria; //$_GET['Comprobante']
            $startDate = ComponentesComunes::fechaComparada('01-'.$_POST['Comprobante']['fecha']);
            $criteria->addCondition('estado = '.Comprobante::iCerrada);
            $criteria->addCondition('CAE  IS NOT NULL');
            $criteria->addCondition("CAE <> '00000000000000'");
            $criteria->addBetweenCondition('fecha',$startDate,date("Y-m-t", strtotime($startDate)));
            $criteria->order = 't.fecha,t.Nro_Puesto_Venta,t.Nro_Comprobante asc' ;
            $aComprobantes = Comprobante::model()->findAll($criteria);
		}else{
            //$model->estado = Comprobante::iCerrada;
            $aComprobantes = Comprobante::model()->findAll('estado = '.Comprobante::iCerrada,array('order'=>'t.fecha,t.Nro_Puesto_Venta,t.Nro_Comprobante asc'));

		}

        $aIvaDebito = array();
        $aResponable = CategoriaIva::model()->findAll();
        foreach ($aResponable as $index => $oResponable) {
            $aIvaDebito[$oResponable->id_categoria]['id'] = $oResponable->id_categoria;
            $aIvaDebito[$oResponable->id_categoria]['nombre'] = $oResponable->nombre;
            $aIvaDebito[$oResponable->id_categoria]['monto'] = 0;
        }

        $aTotales= array();
        $aTotales['netoGrav'] = 0.00;
        $aTotales['iva'] = 0.00;
        $aTotales['percRet'] = 0.00 ;
        $aTotales['noGrav'] = 0.00;
        $aTotales['total'] = 0.00 ;

        $aBasePcias= array();
        $aKeyProvincias = CHtml::listData(Provincia::model()->findAll(),'id_provincia','nombre');

        foreach ($aComprobantes as $index => $aComprobante) {
            $aModels[$index]['id'] = $aComprobante->id;
            $aModels[$index]['fecha'] = ComponentesComunes::fechaFormateada($aComprobante->fecha);
            $aModels[$index]['numero'] = $aComprobante->Nro_Puesto_Venta.'-'.$aComprobante->Nro_Comprobante;
            $aModels[$index]['doc'] = $aComprobante->tipo.$aComprobante->clase;
            if($aComprobante->razonSocial == ''){
                $aModels[$index]['cliente'] = substr($aComprobante->oCliente->Title,0,20);
            }else{
                if(strlen($aComprobante->razonSocial) < 20 || ctype_print($aComprobante->razonSocial[19])){
                    $aModels[$index]['cliente'] = substr( $aComprobante->razonSocial ,0,20);
                
                }else{
                    $aModels[$index]['cliente'] = substr( str_replace('º',' ',$aComprobante->razonSocial) ,0,20);
                }
            }

            $aModels[$index]['cuit'] = $aComprobante->cuit;

            $aModels[$index]['netoGrav'] = number_format($aComprobante->getNetoGravado(),2,'.','');
            Yii::log($aModels[$index]['numero'].' $aComprobante->getNetoGravado:'.$aComprobante->getNetoGravado(),'warning');
            /*
            if($aComprobante->clase == 'A'){
                $aModels[$index]['netoGrav'] = number_format($aComprobante->subtotal,2,'.','');
            }else{
                $aModels[$index]['netoGrav'] = number_format($aComprobante->getSubTotalGravado(),2,'.','');
            }
            */
            if ($aComprobante->tipo == 'NC'){
                $aModels[$index]['netoGrav'] *= (-1);
            }
            $aTotales['netoGrav'] += $aModels[$index]['netoGrav'];
            $provinceKey = array_search($aComprobante->provincia,$aKeyProvincias);
            if ($provinceKey){

                $provinceKey = ord($provinceKey);
                $aBasePcias[$provinceKey]['id'] = $provinceKey;
                $aBasePcias[$provinceKey]['nombre'] = $aComprobante->provincia;
                if($aComprobante->Iva_Responsable == 4){
                    if (isset($aBasePcias[$provinceKey]['base'])){
                        $aBasePcias[$provinceKey]['base'] += $aModels[$index]['netoGrav'] ;
                    }else{
                        $aBasePcias[$provinceKey]['base'] = $aModels[$index]['netoGrav'] ;
                    }
                }else{
                    if (isset($aBasePcias[$provinceKey]['baseOtros'])){
                        $aBasePcias[$provinceKey]['baseOtros'] += $aModels[$index]['netoGrav'] ;
                    }else{
                        $aBasePcias[$provinceKey]['baseOtros'] = $aModels[$index]['netoGrav'] ;
                    }
                }


            }

            Yii::log($aModels[$index]['numero'].' $aComprobante->subtotalIva:'.$aComprobante->subtotalIva,'warning');
            $aModels[$index]['iva'] = number_format($aComprobante->subtotalIva,2,'.','');
            if ($aComprobante->tipo == 'NC'){
                $aModels[$index]['iva'] *= (-1);
            }
            $aIvaDebito[$aComprobante->Iva_Responsable]['monto'] += $aModels[$index]['iva'];

            $aTotales['iva'] += $aModels[$index]['iva'];

            $aModels[$index]['percRet'] = number_format($aComprobante->Ing_Brutos,2,'.','') ;
            if ($aComprobante->tipo == 'NC'){
                $aModels[$index]['percRet'] *= (-1);
            }

            $aTotales['percRet'] += $aModels[$index]['percRet'];
            $aModels[$index]['noGrav'] = 0.00; // TODO: CALCULAR
            $aTotales['noGrav'] += $aModels[$index]['noGrav'];
            $aModels[$index]['total'] = number_format($aComprobante->Total_Final,2,'.','') ;
            if ($aComprobante->tipo == 'NC'){
                $aModels[$index]['total'] *= (-1);
            }
            $aTotales['total'] += $aModels[$index]['total'];
        }

        $mPDF1 = Yii::app()->ePdf->mpdf('', "A4",'8','couriernew', 5,5,25,5,5,0,'P');

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        $mPDF1->WriteHTML($stylesheet, 1);


        $mPDF1->WriteHTML($this->renderPartial('listaIvaVentas',array(
                'model'=>$model,
                'aModels' => $aModels,
                'aIvaDebito' => $aIvaDebito,
                'aTotales' => $aTotales,
                'aBasePcias' => $aBasePcias,
                'mes' => (isset($_POST['Comprobante']['fecha'])? $_POST['Comprobante']['fecha'] : "-"),
                'puesto' => $puesto,
            ),true)
        );


        $mPDF1->Output('LibroIvaVenta_'.$startDate.'.pdf','I');
        /*

        $this->renderPartial('listaIvaVentas',array(
            'model'=>$model,
            'aModels' => $aModels,
            'aIvaDebito' => $aIvaDebito,
            'aTotales' => $aTotales,
            'aBasePcias' => $aBasePcias,
            'mes' => (isset($_POST['Comprobante']['fecha'])? $_POST['Comprobante']['fecha'] : "-"),
            'puesto' => $puesto,
        ),false,false);
        */
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Comprobante the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Comprobante::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Comprobante $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='comprobante-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
