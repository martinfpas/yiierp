<?php

class SucursalBancoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ImportarDatosCsv','ListarSucursales'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionListarSucursales(){

        if (isset($_POST['SucursalBanco']['id_banco'])) {
            $data=SucursalBanco::model()->findAll('id_banco=:parent_id',
                array(':parent_id'=>(int)$_POST['SucursalBanco']['id_banco']),array('order' => 'nombre'));
        }else if(isset($_POST['ChequeDeTerceros']['id_banco'])){
            $data=SucursalBanco::model()->findAll('id_banco=:parent_id',
                array(':parent_id'=>(int)$_POST['ChequeDeTerceros']['id_banco']),array('order' => 'nombre'));

        }else if(isset($_POST['CuentasBancarias']['id_banco'])){
            $data=SucursalBanco::model()->findAll('id_banco=:parent_id',
                array(':parent_id'=>(int)$_POST['CuentasBancarias']['id_banco']),array('order' => 'nombre'));

        }else{
            $data=SucursalBanco::model()->findAll('id_banco=:parent_id',
                array(':parent_id'=>(int)$_POST['id_banco']),array('order' => 'nombre'));
        }


        $data=CHtml::listData($data,'id','Title');
        echo CHtml::tag('option', array('value'=>'','art'=>''),CHtml::encode('-- Seleccionar -- >'),true);
        foreach($data as $value=>$name)
        {
            echo CHtml::tag('option',
                array('value'=>$value),CHtml::encode($name),true);
        }
    }


    public function actionImportarDatosCsv() {

        $aBanco = Banco::model()->findAll();

        $aBancos = array();
        foreach ($aBanco as $key => $oBanco) {
            $aBancos[$oBanco->id] = $oBanco->id;
        }

        $fp = fopen('/var/www/erca/upload/sucursal.csv', 'r');
        if($fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;
            $first_time = true;
            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                /*echo '<br>';
                print_r($line);
                die;*/

                if (isset($aBancos[$line[0]])){
                    $oSucurBan = new SucursalBanco();
                    $oSucurBan->id_banco = $line[0];
                    $oSucurBan->nombre = $line[1];
                    $oSucurBan->localidad = $line[2];
                    $oSucurBan->provincia = $line[4];
                    if (!$oSucurBan->save()) {

                        echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                        print_r($oSucurBan->getErrors());
                    }else{
                        $oMigraSucBanco = new MigraSucursal();
                        $oMigraSucBanco->id = $oSucurBan->id;
                        $oMigraSucBanco->id_banco = $oSucurBan->id_banco;
                        $oMigraSucBanco->id_sucursal = $line[3];
                        if (!$oMigraSucBanco->save()){
                            echo '<br> NO se guardo MIGRA :: '.$oSucurBan->id_banco.'-'.$line[3].'<br>';
                            print_r($oMigraSucBanco->getErrors());
                            print_r($oMigraSucBanco->attributes);
                        }else{
                            echo '--> MIGRA :: '.$oSucurBan->id_banco.'-'.$line[2].'<br>';
                        }

                    }
                }else{
                    echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                }




            };

        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver SucursalBanco #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SucursalBanco;
		$this->pageTitle = "Nuevo SucursalBanco";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SucursalBanco']))
		{
			$model->attributes=$_POST['SucursalBanco'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new SucursalBanco;
		$this->pageTitle = "Nuevo SucursalBanco";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SucursalBanco']))
		{
			$model->attributes=$_POST['SucursalBanco'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando SucursalBanco #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SucursalBanco']))
		{
			$model->attributes=$_POST['SucursalBanco'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new SucursalBanco('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SucursalBanco']))
			$model->attributes=$_GET['SucursalBanco'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SucursalBanco('search');
		$this->pageTitle = 'Administrando Sucursal Bancos ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SucursalBanco'])){
			$model->attributes=$_GET['SucursalBanco'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SucursalBanco the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SucursalBanco::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SucursalBanco $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sucursal-banco-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
