<?php

class FacturaProveedorController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','adminSelect','delete','index','view','admin','ImportarDatosCsv','SaveAsync','SaveFieldAsync','FormAsync',
                    'Json','Terminar','Select2'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSelect2(){
        /** POR DEFECTO SOLO TRAE LOS QUE ESTAN EN CARTERA
         * @var $oModel FacturaProveedor
         * @var $aModel FacturaProveedor[]
         * @var $models FacturaProveedor  */

        $criteria = new CDbCriteria();

        if(isset($_GET['id_Proveedor'])){
            $criteria->compare('id_Proveedor',$_GET['id_Proveedor'],false);
        }

        if(isset($_GET['Nro_Comprobante'])){
            $criteria->compare('Nro_Comprobante',$_GET['Nro_Comprobante'],false);
        }

        if (isset($_GET['sort'])){
            $criteria->order = $_GET['sort'];
        }else{
            $criteria->order = 'FechaFactura desc';
        }

        if (isset($_GET['condImpaga'])){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }


        $models = FacturaProveedor::model()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id,'text' => FacturaProveedor::$aClase[$oModel->TipoComprobante].' º'.$oModel->Nro_Comprobante.' : '.ComponentesComunes::fechaFormateada($oModel->FechaFactura).' : $'.number_format($oModel->TotalNeto,2,'.',''),'importe' => $oModel->montoPagado);
        }

        echo CJSON::encode($aModels);
    }

    public function actionTerminar($id){

        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null || !$transaction->Active){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }

        $oFacturaProveedor=$this->loadModel($id);

        if(isset($_POST['FacturaProveedor']))
        {
            $oFacturaProveedor->attributes=$_POST['FacturaProveedor'];
            $oFacturaProveedor->FechaFactura = ComponentesComunes::jpicker2db($oFacturaProveedor->FechaFactura);
            $oFacturaProveedor->montoPagado = $oFacturaProveedor->TotalNeto;
            if (!$oFacturaProveedor->save()) {
                $transaction->rollback();
                throw new CHttpException('500','Error'.CHtml::errorSummary($oFacturaProveedor));
            }else{
                $oPagoProveedor = new PagoProveedor();
                $oPagoProveedor->monto = $oFacturaProveedor->montoPagado;
                $oPagoProveedor->idProveedor = $oFacturaProveedor->id_Proveedor;
                $oPagoProveedor->fecha = date('Y-m-d');

                if(!$oPagoProveedor->save()){
                    //TODO: MANEJAR ERROR
                    $transaction->rollback();
                    throw new CHttpException('500','Error'.CHtml::errorSummary($oPagoProveedor));
                }else{
                    //$oFacturaProveedor->
                    $oPagoRel = new Pago2FactProv();
                    $oPagoRel->idPagoProveedor = $oPagoProveedor->id;
                    $oPagoRel->idFacturaProveedor = $oFacturaProveedor->id;
                    if (!$oPagoRel->save()){
                        //TODO: MANEJAR ERROR
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oPagoRel));

                    }

                    $oPagoProvFt = new PagoProvEfectivo();
                    $oPagoProvFt->monto = $oFacturaProveedor->montoPagado;
                    $oPagoProvFt->idPagoProveedor = $oPagoProveedor->id;
                    if (!$oPagoProvFt->save()){
                        //TODO: MANEJAR ERROR
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oPagoProvFt));
                    }else{

                    }
                }
            }
        }else {
            throw new CHttpException('500','No es una '.FacturaProveedor::model()->getAttributeLabel('model').' existente');
        }
        if ($shouldCommit){
            $transaction->commit();
        }
        echo $id;

    }
    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oFacturaProveedor = FacturaProveedor::model()->findByPk($_POST['pk']);
        if ($oFacturaProveedor != null) {
            $oFacturaProveedor->setAttribute($_POST['name'],$_POST['value']);
            if($_POST['name'] == 'FechaCarga'){
                $oFacturaProveedor->FechaCarga = ComponentesComunes::jpicker2db($oFacturaProveedor->FechaCarga);
            }
            if($_POST['name'] == 'Fecha_Vencimiento'){
                $oFacturaProveedor->Fecha_Vencimiento = ComponentesComunes::jpicker2db($oFacturaProveedor->Fecha_Vencimiento);
            }
            if($_POST['name'] == 'FechaFactura'){
                $oFacturaProveedor->FechaFactura = ComponentesComunes::jpicker2db($oFacturaProveedor->FechaFactura);
            }


            if($_POST['name'] == 'Descuento' || $_POST['name'] == 'Nograv' || $_POST['name'] == 'IngBrutos' || $_POST['name'] == 'Rg3337' || $_POST['name'] == 'PercepIB' || $_POST['name'] == 'ImpGanancias'){
                $oFacturaProveedor->recalcular();
            }

            if (!$oFacturaProveedor->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oFacturaProveedor));
            }
        }else {
            throw new CHttpException('500','No es una '.FacturaProveedor::model()->getAttributeLabel('model').' existente');
        }
        echo $_POST['value'];
        
    } 
	
    public function actionImportarDatosCsv() {


        $fp = fopen('/var/www/erca/upload/Proveedor.csv', 'r');
        if(false && $fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;

            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                echo '<br>Procesando indice '.$ix.' * >'.$line[0];
                /*
                echo '<br>';
                ComponentesComunes::print_array($line);
                die;
                */
                $oProveedor = Proveedor::model()->findByPk($line[0]);

                if ($oProveedor == null){
                    $oProveedor = new Proveedor();
                    $oProveedor->id = $line[0];
                }

                $oProveedor->cp = $line[1];
                $oProveedor->zp = $line[2];
                $oProveedor->direccion = utf8_encode($line[3]);
                $oProveedor->nbreFantasia = utf8_encode($line[4]);
                $oProveedor->contacto = utf8_encode($line[5]);
                $oProveedor->cuit = $line[6];
                $oProveedor->tel = utf8_encode($line[7]);
                if(strlen($line[8] )> 8){
                    $line[11] .= utf8_encode($line[8]);
                }else{
                    $oProveedor->pisoDto = utf8_encode($line[8]);
                }

                $oProveedor->email = utf8_encode($line[9]);
                $oProveedor->fax = utf8_encode($line[10]);
                $oProveedor->obs = utf8_encode($line[11]);
                if ($line[12] <> ''){
                    $oProveedor->razonSocial = utf8_encode($line[12]);
                }else{
                    $oProveedor->razonSocial = '.';
                }

                $oProveedor->idTipo = $line[13];
                $oProveedor->ivaResponsable = ($line[14] != '')? $line[14] : 5;
                $oProveedor->saldo = $line[15];

                if (!$oProveedor->save()) {
                    echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                    ComponentesComunes::print_array($oProveedor->attributes);
                    print_r($oProveedor->getErrors());
                }else{
                    if ($oProveedor->isNewRecord){
                        echo ' Nuevo Registro !! :: ';
                    }else{
                        echo ' SE actualizo :: '.$line[0].':: ';
                    }
                }

            };

        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.FacturaProveedor::model()->getAttributeLabel('model').' #'.$id;

		$aPago2FactProv = new Pago2FactProv('search');
        $aPago2FactProv->unsetAttributes();
        $aPago2FactProv->idFacturaProveedor = $id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'aPago2FactProv' => $aPago2FactProv,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FacturaProveedor;
		$this->pageTitle = "Nuevo ".FacturaProveedor::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FacturaProveedor']))
		{
			$model->attributes=$_POST['FacturaProveedor'];
            $model->FechaFactura = ComponentesComunes::jpicker2db($model->FechaFactura);
            $model->Fecha_Vencimiento = ComponentesComunes::jpicker2db($model->Fecha_Vencimiento);
            $model->FechaCarga = date('Y-m-d');

            if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new FacturaProveedor;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FacturaProveedor']))
		{
		    if(isset($_POST['FacturaProveedor']['id']) && $_POST['FacturaProveedor']['id'] > 0){
                $model = $this->loadModel($_POST['FacturaProveedor']['id']);
            }
			$model->attributes=$_POST['FacturaProveedor'];
            $model->FechaFactura = ComponentesComunes::jpicker2db($model->FechaFactura);
            $model->Fecha_Vencimiento = ComponentesComunes::jpicker2db($model->Fecha_Vencimiento);
            $model->FechaCarga = date('Y-m-d');
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    public function actionFormAsync($id = null,$idProveedor = null){
	    if ($id == null && $idProveedor != null){
	        $oFacturaProv = new FacturaProveedor();
            $oFacturaProv->id_Proveedor = $idProveedor;

            if ($oFacturaProv->save()){
                $this->renderPartial('_form',array(
                    'model' => $oFacturaProv
                ));
            }else{

            }
        }
    }

    public function actionJson($id){
        $oFacturaProveedor = $this->loadModel($id);
        /*
        if($oFacturaProveedor->estado < Comprobante::iCerrada){
            $oFacturaProveedor->recalcular();
            if (!$oFacturaProveedor->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oFacturaProveedor));
            }
        }
        */
        $attributes = $oFacturaProveedor->attributes;
        //$attributes['AlicuotaItem'] = $oFacturaProveedor->getAlicuotaItem();
        echo json_encode($attributes);
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.FacturaProveedor::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FacturaProveedor']))
		{
			$model->attributes=$_POST['FacturaProveedor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

        $oDetalleFacturaProv = new DetalleFacturaProv();
        $aDetalleFacturaProv = new DetalleFacturaProv('search');
        $aDetalleFacturaProv->unsetAttributes();
        $oDetalleFacturaProv->idFacturaProveedor = $id;
        $aDetalleFacturaProv->idFacturaProveedor = $id;

		$this->render('update',array(
			'model'=>$model,
            'is_modal'=>false,
            'oDetalleFacturaProv' => $oDetalleFacturaProv,
            'aDetalleFacturaProv' => $aDetalleFacturaProv,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new FacturaProveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FacturaProveedor']))
			$model->attributes=$_GET['FacturaProveedor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FacturaProveedor('search');
		$this->pageTitle = 'Administrando '.FacturaProveedor::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FacturaProveedor'])){
			$model->attributes=$_GET['FacturaProveedor'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Manages all models.
     */
    public function actionAdminSelect()
    {
        $model=new FacturaProveedor('search');
        $this->pageTitle = 'Administrando '.FacturaProveedor::model()->getAttributeLabel('models');

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['FacturaProveedor'])){
            $model->attributes=$_GET['FacturaProveedor'];
        }else{

        }

        $this->renderPartial('adminSelect',array(
            'model'=>$model,
        ),false,true);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FacturaProveedor the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FacturaProveedor::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param FacturaProveedor $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='factura-proveedor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
