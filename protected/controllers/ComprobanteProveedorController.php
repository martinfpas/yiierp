<?php

class ComprobanteProveedorController extends Controller
{
    public $pdfHeader = '<table width="100%">
					<tr style="">
						<td width="33%" ><div style="width: 500px;float: left;font-size: small;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="33%" style="text-align: right;"></td>
						<td width="33%" align="center">{PAGENO} de {nbpg}</td>
					</tr>
				</table>';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','adminSelect','delete','index','view','admin','ImportarDatosCsv','SaveAsync','SaveFieldAsync','FormAsync',
                    'Json','Terminar','Select2','ListadoIva','IvaCompras','ComprasPorCuentas','ListadoComprasPorCuentas'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIvaCompras(){
        $model = new ComprobanteProveedor();
        $this->render('_formIva',array('model'=> $model));
    }

    public function actionComprasPorCuentas(){
        $model = new ComprobanteProveedor();
        $this->render('_formComprasPorCuenta',array('model'=> $model));
    }

    public function actionListadoComprasPorCuentas($print=false){
        /* @var $aComprobante ComprobanteProveedor
         * @var $oResponable CategoriaIva
         */
        $model=new ComprobanteProveedor('search');
        $model->unsetAttributes();
        
        if(isset($_POST['ComprobanteProveedor'])){
            $model->attributes = $_POST['ComprobanteProveedor'];
        }
        if(isset($_GET['ComprobanteProveedor'])){
            $model->attributes = $_GET['ComprobanteProveedor'];
        }

        
        $mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'8','couriernew', 5,5,25,5,5,0,'P');

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML( $this->renderPartial('listaComprasPorCuentas',array(
            'model'=>$model
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;


        $filename = 'ComprobantesPorCuentas_'.$model->MesCarga .'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            // Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-ranges=1-3 -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            //Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
    

    }

    public function actionListadoIva()
    {
        /* @var $aComprobante ComprobanteProveedor
         * @var $oResponable CategoriaIva
         */
        $model=new ComprobanteProveedor('search');
        $this->pageTitle = 'Administrando '.Comprobante::model()->getAttributeLabel('models');

        $aModels = array();
        $startDate = '';
        $puesto = '';

        if(isset($_POST['ComprobanteProveedor'])){
            $model->unsetAttributes();  // clear any default values
            $model->attributes = $_POST['ComprobanteProveedor'];

            $model->FechaFactura = null;
            $criteria = $model->search()->criteria; //$_GET['Comprobante']
            //$startDate = ComponentesComunes::fechaComparada('01-'.$_POST['ComprobanteProveedor']['MesCarga']);
            //$criteria->addCondition('estado = '.Comprobante::iCerrada);
            //$criteria->addCondition('CAE  IS NOT NULL');
            $criteria->addCondition("MesCarga = '".$_POST['ComprobanteProveedor']['MesCarga']."'");
            //$criteria->addBetweenCondition('fecha',$startDate,date("Y-m-t", strtotime($startDate)));
            $criteria->order = 't.FechaFactura,t.Nro_Puesto_Vta,t.Nro_Comprobante asc' ;
            $aComprobantes = ComprobanteProveedor::model()->findAll($criteria);
        }else{
            //$model->estado = Comprobante::iCerrada;
            $aComprobantes = Comprobante::model()->findAll('',array('order'=>'FechaFactura asc'));

        }

        $aIvaDebito = array();
        $aResponable = CategoriaIva::model()->findAll();
        foreach ($aResponable as $index => $oResponable) {
            $aIvaDebito[$oResponable->id_categoria]['id'] = $oResponable->id_categoria;
            $aIvaDebito[$oResponable->id_categoria]['nombre'] = $oResponable->nombre;
            $aIvaDebito[$oResponable->id_categoria]['monto'] = 0;
        }

        $aTotales= array();
        $aTotales['netoGrav'] = 0.00;
        $aTotales['iva'] = 0.00;
        $aTotales['percRet'] = 0.00 ;
        $aTotales['noGrav'] = 0.00;
        $aTotales['iibb'] = 0;
        $aTotales['impGanancias'] = 0.00 ;
        $aTotales['total'] = 0.00 ;


        $aBasePcias= array();
        $aKeyProvincias = CHtml::listData(Provincia::model()->findAll(),'id_provincia','nombre');

        foreach ($aComprobantes as $index => $aComprobante) {
            //$aComprobante->recalcular();
            $aModels[$index]['id'] = $aComprobante->id;
            $aModels[$index]['fecha'] = ComponentesComunes::fechaFormateada($aComprobante->FechaFactura);
            $aModels[$index]['numero'] = $aComprobante->Nro_Puesto_Vta.'-'.$aComprobante->Nro_Comprobante;
            $aModels[$index]['doc'] = ComprobanteProveedor::$aClaseShort[$aComprobante->TipoComprobante];
            $aModels[$index]['Proveedor'] = substr($aComprobante->oProveedor->Title,0,20);
            $aModels[$index]['cuit'] = $aComprobante->oProveedor->cuit;

            $aModels[$index]['netoGrav'] = number_format($aComprobante->getNetoGravadoConDescuento(),2,'.','');
            $aModels[$index]['netoGrav'] *= ComprobanteProveedor::$aFactorMultip[$aComprobante->TipoComprobante];
            $aTotales['netoGrav'] += $aModels[$index]['netoGrav'];

            if($aComprobante->Iva == 0){
                $aComprobante->recalcular();
            }
            $aModels[$index]['iva'] = number_format(($aComprobante->Iva * ComprobanteProveedor::$aFactorMultip[$aComprobante->TipoComprobante]),2,'.','' );
            $aTotales['impGanancias'] += $aComprobante->ImpGanancias ;
            $aTotales['iva'] += $aModels[$index]['iva'];
            $aModels[$index]['percRet'] = number_format($aComprobante->Rg3337,2,'.','') * ComprobanteProveedor::$aFactorMultip[$aComprobante->TipoComprobante];
            $aModels[$index]['iibb'] = number_format($aComprobante->IngBrutos,2,'.','') * ComprobanteProveedor::$aFactorMultip[$aComprobante->TipoComprobante];

            $aTotales['percRet'] += $aModels[$index]['percRet'];

            $aModels[$index]['noGrav'] = number_format($aComprobante->Nograv,2,'.','') * ComprobanteProveedor::$aFactorMultip[$aComprobante->TipoComprobante];

            $aTotales['noGrav'] += $aModels[$index]['noGrav'];
            //$aModels[$index]['total'] = number_format($aComprobante->getTotalConFactor(),2,'.','') ;
            $aModels[$index]['total'] = number_format($aComprobante->getTotalNetoConFactor(),2,'.','') ;

            $aTotales['iibb'] += number_format($aComprobante->IngBrutos,2,'.','') ;

            $aTotales['total'] += $aModels[$index]['total'];
        }

        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','8','couriernew', 5,5,25,5,5,0,'P');

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('listaIvaCompras',array( 
                'model'=>$model,
                'aModels' => $aModels,
                'aIvaDebito' => $aIvaDebito,
                'aTotales' => $aTotales,
                'aBasePcias' => $aBasePcias,
                'mes' => (isset($_POST['ComprobanteProveedor']['MesCarga'])? $_POST['ComprobanteProveedor']['MesCarga'] : "-"),
            ),true)
        );

        $mPDF1->Output('LibroIvaCompras_'.$startDate.'.pdf','I');
    }

	public function actionSelect2(){
        /** POR DEFECTO SOLO TRAE LOS QUE ESTAN EN CARTERA
         * @var $oModel ComprobanteProveedor
         * @var $aModel ComprobanteProveedor[]
         * @var $models ComprobanteProveedor  */

        $criteria = new CDbCriteria();

        if(isset($_GET['id_Proveedor'])){
            $criteria->compare('id_Proveedor',$_GET['id_Proveedor'],false);
        }

        if(isset($_GET['Nro_Comprobante'])){
            $criteria->compare('Nro_Comprobante',$_GET['Nro_Comprobante'],false);
        }

        if (isset($_GET['sort'])){
            $criteria->order = $_GET['sort'];
        }else{
            $criteria->order = 'FechaFactura desc';
        }

        if (isset($_GET['condImpaga'])){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }


        $models = ComprobanteProveedor::model()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $importe = number_format((ComprobanteProveedor::$aFactorMultip[$oModel->TipoComprobante] * $oModel->TotalNeto),2,'.','');
            $aModels[] = array('id' => $oModel->id,'text' => ComprobanteProveedor::$aClase[$oModel->TipoComprobante].' º'.$oModel->Nro_Comprobante.' : '.ComponentesComunes::fechaFormateada($oModel->FechaFactura).' : $'.$importe,'importe' => $importe /*$oModel->montoPagado*/);
        }

        echo CJSON::encode($aModels);
    }

    public function actionTerminar($id){

        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null || !$transaction->Active){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }

        $oComprobanteProveedor=$this->loadModel($id);

        if(isset($_POST['ComprobanteProveedor']))
        {
            $oComprobanteProveedor->attributes=$_POST['ComprobanteProveedor'];
            $oComprobanteProveedor->FechaFactura = ComponentesComunes::jpicker2db($oComprobanteProveedor->FechaFactura);
            $oComprobanteProveedor->montoPagado = $oComprobanteProveedor->TotalNeto;
            if (!$oComprobanteProveedor->save()) {
                $transaction->rollback();
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobanteProveedor));
            }else{
                $oPagoProveedor = new PagoProveedor();
                $oPagoProveedor->monto = $oComprobanteProveedor->montoPagado;
                $oPagoProveedor->idProveedor = $oComprobanteProveedor->id_Proveedor;
                $oPagoProveedor->fecha = date('Y-m-d');

                if(!$oPagoProveedor->save()){
                    //TODO: MANEJAR ERROR
                    $transaction->rollback();
                    throw new CHttpException('500','Error'.CHtml::errorSummary($oPagoProveedor));
                }else{
                    //$oComprobanteProveedor->
                    $oPagoRel = new Pago2FactProv();
                    $oPagoRel->idPagoProveedor = $oPagoProveedor->id;
                    $oPagoRel->idComprobanteProveedor = $oComprobanteProveedor->id;
                    if (!$oPagoRel->save()){
                        //TODO: MANEJAR ERROR
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oPagoRel));

                    }

                    $oPagoProvFt = new PagoProvEfectivo();
                    $oPagoProvFt->monto = $oComprobanteProveedor->montoPagado;
                    $oPagoProvFt->idPagoProveedor = $oPagoProveedor->id;
                    if (!$oPagoProvFt->save()){
                        //TODO: MANEJAR ERROR
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oPagoProvFt));
                    }else{

                    }
                }
            }
        }else {
            throw new CHttpException('500','No es una '.ComprobanteProveedor::model()->getAttributeLabel('model').' existente');
        }
        if ($shouldCommit){
            $transaction->commit();
        }
        echo $id;

    }
    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oComprobanteProveedor = ComprobanteProveedor::model()->findByPk($_POST['pk']);
        if ($oComprobanteProveedor != null) {
            $oComprobanteProveedor->setAttribute($_POST['name'],$_POST['value']);
            if($_POST['name'] == 'FechaCarga'){
                $oComprobanteProveedor->FechaCarga = ComponentesComunes::jpicker2db($oComprobanteProveedor->FechaCarga);
            }
            if($_POST['name'] == 'Fecha_Vencimiento'){
                $oComprobanteProveedor->Fecha_Vencimiento = ComponentesComunes::jpicker2db($oComprobanteProveedor->Fecha_Vencimiento);
            }
            if($_POST['name'] == 'FechaFactura'){
                $oComprobanteProveedor->FechaFactura = ComponentesComunes::jpicker2db($oComprobanteProveedor->FechaFactura);
            }


            if($_POST['name'] == 'Descuento' || $_POST['name'] == 'Nograv' || $_POST['name'] == 'IngBrutos' || $_POST['name'] == 'Rg3337' || $_POST['name'] == 'PercepIB' || $_POST['name'] == 'ImpGanancias'){
                $oComprobanteProveedor->recalcular();
            }

            if (!$oComprobanteProveedor->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobanteProveedor));
            }
        }else {
            throw new CHttpException('500','No es una '.ComprobanteProveedor::model()->getAttributeLabel('model').' existente');
        }
        echo $_POST['value'];
        
    } 
	
    public function actionImportarDatosCsv() {


        $fp = fopen('/var/www/erca/upload/Proveedor.csv', 'r');
        if(false && $fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;

            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                echo '<br>Procesando indice '.$ix.' * >'.$line[0];
                /*
                echo '<br>';
                ComponentesComunes::print_array($line);
                die;
                */
                $oProveedor = Proveedor::model()->findByPk($line[0]);

                if ($oProveedor == null){
                    $oProveedor = new Proveedor();
                    $oProveedor->id = $line[0];
                }

                $oProveedor->cp = $line[1];
                $oProveedor->zp = $line[2];
                $oProveedor->direccion = utf8_encode($line[3]);
                $oProveedor->nbreFantasia = utf8_encode($line[4]);
                $oProveedor->contacto = utf8_encode($line[5]);
                $oProveedor->cuit = $line[6];
                $oProveedor->tel = utf8_encode($line[7]);
                if(strlen($line[8] )> 8){
                    $line[11] .= utf8_encode($line[8]);
                }else{
                    $oProveedor->pisoDto = utf8_encode($line[8]);
                }

                $oProveedor->email = utf8_encode($line[9]);
                $oProveedor->fax = utf8_encode($line[10]);
                $oProveedor->obs = utf8_encode($line[11]);
                if ($line[12] <> ''){
                    $oProveedor->razonSocial = utf8_encode($line[12]);
                }else{
                    $oProveedor->razonSocial = '.';
                }

                $oProveedor->idTipo = $line[13];
                $oProveedor->ivaResponsable = ($line[14] != '')? $line[14] : 5;
                $oProveedor->saldo = $line[15];

                if (!$oProveedor->save()) {
                    echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                    ComponentesComunes::print_array($oProveedor->attributes);
                    print_r($oProveedor->getErrors());
                }else{
                    if ($oProveedor->isNewRecord){
                        echo ' Nuevo Registro !! :: ';
                    }else{
                        echo ' SE actualizo :: '.$line[0].':: ';
                    }
                }

            };

        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = ComprobanteProveedor::model()->with('aAlicuotasCompra')->findByPk($id);
		$this->pageTitle = 'Ver '.ComprobanteProveedor::model()->getAttributeLabel('model').' #'.$id;

		$aPago2FactProv = new Pago2FactProv('search');
        $aPago2FactProv->unsetAttributes();
        $aPago2FactProv->idComprobanteProveedor = $id;
        $aIvaFooter = array();

        /**
         * @var $aAlicuotasCompra AlicuotasCompra
         */
        foreach ($model->aAlicuotasCompra as $index => $aAlicuotasCompra) {
            $aIvaFooter[] = array(
                'name' => 'Iva '.number_format($aAlicuotasCompra->alicuota,2,',','.'),
                'value' => '$'.number_format($aAlicuotasCompra->impuestoLiquidado,2),
            );
        }

		$this->render('view',array(
			'model'=>$model,
            'aPago2FactProv' => $aPago2FactProv,
            'aIvaFooter' => $aIvaFooter,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ComprobanteProveedor;
		$this->pageTitle = "Nuevo ".ComprobanteProveedor::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ComprobanteProveedor']))
		{
            $model->attributes=$_POST['ComprobanteProveedor'];

		    $oComprobanteExistente = ComprobanteProveedor::model()->findByAttributes(array('id_Proveedor'=>$model->id_Proveedor,'Nro_Comprobante'=>$model->Nro_Comprobante,'TipoComprobante'=>$model->TipoComprobante,'Nro_Puesto_Vta' => $model->Nro_Puesto_Vta));
            if($oComprobanteExistente != null){
                $model->addError('Nro_Comprobante',' El comprobante ya fue cargado. id:'.$oComprobanteExistente->id);
            }else{
                $model->FechaFactura = ComponentesComunes::jpicker2db($model->FechaFactura);
                $model->Fecha_Vencimiento = ComponentesComunes::jpicker2db($model->Fecha_Vencimiento);
                $model->FechaCarga = date('Y-m-d');

                if($model->save())
                    $this->redirect(array('update','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ComprobanteProveedor;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ComprobanteProveedor']))
		{
		    if(isset($_POST['ComprobanteProveedor']['id']) && $_POST['ComprobanteProveedor']['id'] > 0){
                $model = $this->loadModel($_POST['ComprobanteProveedor']['id']);
            }
			$model->attributes=$_POST['ComprobanteProveedor'];
            $model->FechaFactura = ComponentesComunes::jpicker2db($model->FechaFactura);
            $model->Fecha_Vencimiento = ComponentesComunes::jpicker2db($model->Fecha_Vencimiento);
            $model->FechaCarga = date('Y-m-d');
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    public function actionFormAsync($id = null,$idProveedor = null){
	    if ($id == null && $idProveedor != null){
	        $oFacturaProv = new ComprobanteProveedor();
            $oFacturaProv->id_Proveedor = $idProveedor;

            if ($oFacturaProv->save()){
                $this->renderPartial('_form',array(
                    'model' => $oFacturaProv
                ));
            }else{

            }
        }
    }

    public function actionJson($id){
        /** @var $item ComprobanteProveedor */
        $oComprobanteProveedor = $this->loadModel($id);
        /*
        if($oComprobanteProveedor->estado < Comprobante::iCerrada){
            $oComprobanteProveedor->recalcular();
            if (!$oComprobanteProveedor->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobanteProveedor));
            }
        }
        */
        $attributes = $oComprobanteProveedor->attributes;
        $attributes['AlicuotaItems'] = array();
        foreach ($oComprobanteProveedor->aAlicuotasCompra as $index => $item) {
            $attributes['AlicuotaItems'][] = array(
                'alicuota' => number_format($item->alicuota,2,',','.'),
                'impuestoLiquidado' => number_format($item->impuestoLiquidado,2,',','.'),
            );
        }
        $attributes['Subtotal'] =$oComprobanteProveedor->getSubTotalItems();
        echo json_encode($attributes);
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$oMesCarga = MesesCargaCompra::model()->findByAttributes(array('mesCarga'=>$model->MesCarga));

		if ($oMesCarga != null && $oMesCarga->activo == MesesCargaCompra::iInactivo){
            $this->redirect(array('view','id'=>$model->id));
        }

		$this->pageTitle = 'Modificando '.ComprobanteProveedor::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ComprobanteProveedor']))
		{
			$model->attributes=$_POST['ComprobanteProveedor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

        $oDetalleComprobanteProv = new DetalleComprobanteProv();
        $aDetalleComprobanteProv = new DetalleComprobanteProv('search');
        $aDetalleComprobanteProv->unsetAttributes();
        $oDetalleComprobanteProv->idComprobanteProveedor = $id;
        $aDetalleComprobanteProv->idComprobanteProveedor = $id;

        $ivasCriteria = new CDbCriteria();
        $ivasCriteria->group ='t.alicuota';
        $ivasCriteria->select = 't.alicuota,sum(t.precioUnitario * t.cantidadRecibida) as SubTotalParcial';
        $ivasCriteria->condition =  'idComprobanteProveedor = '.$id;

        $aIvas = DetalleComprobanteProv::model()->findAll($ivasCriteria);

		$this->render('update',array(
			'model'=>$model,
            'is_modal'=>false,
            'oDetalleComprobanteProv' => $oDetalleComprobanteProv,
            'aDetalleComprobanteProv' => $aDetalleComprobanteProv,
            'aIvas' => $aIvas,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $estadoAnterior = $model;
        $model->delete();

		$oMesCarga = MesesCargaCompra::model()->findByAttributes(array('mesCarga'=>$estadoAnterior->MesCarga));

        if ($oMesCarga != null && $oMesCarga->activo == MesesCargaCompra::iInactivo){
            $this->redirect(array('view','id'=>$estadoAnterior->id));
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ComprobanteProveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ComprobanteProveedor']))
			$model->attributes=$_GET['ComprobanteProveedor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	    /*
	    $array = ComprobanteProveedor::model()->findAll();
        foreach ($array as $index => $item) {
            $item->recalcular();
	    }
	    */

		$model=new ComprobanteProveedor('search');
		$this->pageTitle = 'Administrando '.ComprobanteProveedor::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ComprobanteProveedor'])){
			$model->attributes=$_GET['ComprobanteProveedor'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Manages all models.
     */
    public function actionAdminSelect()
    {
        $model=new ComprobanteProveedor('search');
        $this->pageTitle = 'Administrando '.ComprobanteProveedor::model()->getAttributeLabel('models');

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['ComprobanteProveedor'])){
            $model->attributes=$_GET['ComprobanteProveedor'];
        }else{

        }

        $this->renderPartial('adminSelect',array(
            'model'=>$model,
        ),false,true);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ComprobanteProveedor the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ComprobanteProveedor::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ComprobanteProveedor $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='factura-proveedor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
