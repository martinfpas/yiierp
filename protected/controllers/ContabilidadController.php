<?php

class ContabilidadController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/';

	public function actionListadoCiti()
	{
		$this->render('citi');
	}

	public function actionCiti()
    {

        $this->render('_formVentas',array(
            //'model' => $citi,
        ));
    }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}