<?php

class RubroController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','GetAsync','Select2'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_rubros")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Rubro #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionSelect2(){

        $criteria = new CDbCriteria();
        if(ComponentesComunes::validatesAsInt($_GET['q'])){
            $criteria->compare('id_rubro',$_GET['q']);
        }else{
            $criteria->compare('descripcion',$_GET['q'],true);
            $criteria->limit = $_GET['page_limit'];
            //$criteria->offset = $_GET['page'] * $_GET['page_limit'];
        }

        $models = Rubro::model()->todos()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id_rubro,'text' => $oModel->descripcion);
        }

        echo CJSON::encode($aModels);
    }

	public function actionGetAsync($idRubro){
	    $model = Rubro::model()->findByPk($idRubro);
	    if ($model){
	        echo $model->descripcion;
        }else{
	        throw new CHttpException('500','<span class="errorSpan">Error: El Rubro no existe</span>');
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Rubro;
		$this->pageTitle = "Nuevo Rubro";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rubro']))
		{
			$model->attributes=$_POST['Rubro'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_rubro));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Rubro;
		$this->pageTitle = "Nuevo Rubro";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rubro']))
		{
			$model->attributes=$_POST['Rubro'];
			if($model->save()){
				echo 'Ok';
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Rubro #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Rubro']))
		{
			$model->attributes=$_POST['Rubro'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_rubro));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Rubro('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rubro']))
			$model->attributes=$_GET['Rubro'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Rubro('search');
		$this->pageTitle = 'Administrando Rubros ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Rubro'])){
			$model->attributes=$_GET['Rubro'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Rubro the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Rubro::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Rubro $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rubro-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
