<?php

class PaqueteController extends Controller
{

    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="37%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="30%" align="center">{PAGENO}/{nbpg}<br><h2><b>dauby</b></h2></td>
						<td width="33%" style="text-align: right;">Fax: 0342-4904299<br>Tel: : 0342-4904164<br>ercasr@arnetbiz.com.ar</td>
					</tr>
				</table>';

    public $pdfFooter = '<table width="100%">
					<tr>
						<td width="50%"><div style="width: 500px;float: left;"><b>Vigente desde el </b></div></td>
						<td width="50%" align="center"><b>Hoja {PAGENO}/{nbpg}</b></td>						
					</tr>
				</table>';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','viewFilaGrillaAsync','SaveAsync',
                    'ImportarDatosCsv','GetAsync','SaveFieldAsync','ListaComercio','ListaInstituciones'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_art_envases")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionListaComercio(){

        ini_set("memory_limit","1024M");
        ini_set("pcre.backtrack_limit", "1000000");

        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','12','couriernew', 10,10,25,20,15,0,'P');
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','12','couriernew', 10,10,25,5,5,0,'P');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        $mPDF1->SetHTMLFooter($this->pdfFooter,null,true);
        $aPaquetes = new Paquete('search');

        $mPDF1->WriteHTML($this->renderPartial('pdfComercial',array(
                'model' => $aPaquetes
            ),true)
        );

        $mPDF1->Output('ListaComercial_'.date('d-m-Y').'.pdf','I');
    }

    public function actionListaInstituciones(){

        ini_set("memory_limit","1024M");
        ini_set("pcre.backtrack_limit", "1000000");

        $aPaquetes = new Paquete('search');

        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','12','couriernew', 10,10,25,10,5,0,'P');
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','12','couriernew', 10,10,25,5,5,0,'P');
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        $mPDF1->SetHTMLFooter($this->pdfFooter,null,true);
        $aPaquetes = new Paquete('search');

        $mPDF1->WriteHTML($this->renderPartial('pdfInstitucional',array(
                'model' => $aPaquetes
            ),true)
        );

        $mPDF1->Output('ListaInstituciones_'.date('d-m-Y').'.pdf','I');
    }

    public function actionSaveFieldAsync(){
        /* @var $oPaquete Paquete */
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }
        $oPaquete = Paquete::model()->resetScope(true)->findByPk($_POST['pk']);
        if ($oPaquete != null){
            $oPaquete->setAttribute($_POST['name'],$_POST['value']);

            if(!$oPaquete->save()){
                throw new CHttpException('500','Error'.CHtml::errorSummary($oPaquete));
            }else{

            }
        }else{
            throw new CHttpException('500','No es un Paquete existente');
        }
        echo $_POST['value'];
    }

    public function actionGetAsync($idArticulo,$cantidad){
        $model = Paquete::model()->with('oEnvase')->findByAttributes(array('cantidad' => $cantidad,'id_artVenta' => $idArticulo));
        $arr = array();
        if ($model){
            $arr = $model->attributes;
            $arr['envase'] = $model->oEnvase->descripcion;
        }else{
            $disponibles = Paquete::model()->with('oEnvase')->findAllByAttributes(array('id_artVenta' => $idArticulo));
            $sDisponibles = '';
            foreach ($disponibles as $index => $oPaquete) {
                $sDisponibles .= '<br>'.$oPaquete->cantidad.' => '.$oPaquete->oEnvase->descripcion;
            }
            throw new CHttpException('500','<span class="errorSpan">Error: El Paquete no existe '.$sDisponibles.'</span>');
        }
        echo CJSON::encode($arr);
    }

    public function actionImportarDatosCsv() {

        $fp = fopen('/var/www/erca/upload/Paquetes.csv', 'r');
        if($fp)
        {
            /*
            $line = fgetcsv($fp, 1000, ",");
            print_r($line); exit;
            */

            $first_time = true;
            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ",")) != FALSE) {
                $ix++;
                $oArticulo = Articulo::model()->findByAttributes(array('id_rubro'=> $line[0],'id_articulo'=> $line[1]));

                if ($oArticulo == null){
                    echo '<br>'.$line[0].':: el indice '.$line[0].'-'.$line[1].'$oArticulo No Existe...<br>';

                }else {

                    $oArtVenta = ArticuloVenta::model()->findByAttributes(array('id_articulo' => $oArticulo->id, 'id_presentacion' => $line[2]));

                    if ($oArtVenta != null) {
                        $oPaquete = Paquete::model()->findByAttributes(array());
                        $oPaquete = new Paquete();
                        $oPaquete->id_artVenta = $oArtVenta->id_articulo_vta;
                        $oPaquete->id_envase = $line[3];
                        $oPaquete->cantidad = $line[4];
                        if (!$oPaquete->save()) {
                            echo '<br> NO se guardo:: ' . $line[0] . ':: el indice ' . $line[1] . '-' . $line[2] . '<br>';
                            print_r($oPaquete->getErrors());
                        } else {
                            echo '<br>' . $line[0] . ':: el indice ' . $line[1] . '-' . $line[2] . ' => ' . $ix . '<br>';
                        }
                    } else {
                        echo '<br>' . $line[0] . ':: el indice ' . $line[1] . '-' . $line[2] . '$oArtVenta No Existe => ' . $ix . '<br>';
                    }
                }
            };

        }
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Paquete #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function actionViewFilaGrillaAsync($id_artVenta) {
		$model = new Paquete();
		//TODO: CONTROLAR SI EL ARTICULO_VTA CON ID = $id_artVenta EXISTE
		$model->id_artVenta = $id_artVenta;
		
		$aPaquete = new Paquete('search');
		$aPaquete->unsetAttributes();
		$aPaquete->id_artVenta = $id_artVenta;
				
		$this->renderPartial('_formFila',array(
			'aPaquete' => $aPaquete,
			'model' =>$model,			
		),false,true);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Paquete;
		$this->pageTitle = "Nuevo Paquete";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paquete']))
		{
			$model->attributes=$_POST['Paquete'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Paquete;
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paquete']))
		{
			$model->attributes=$_POST['Paquete'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Paquete #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Paquete']))
		{
			$model->attributes=$_POST['Paquete'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Paquete('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Paquete']))
			$model->attributes=$_GET['Paquete'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Paquete('search');
		$this->pageTitle = 'Administrando Paquetes ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Paquete'])){
			$model->attributes=$_GET['Paquete'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Paquete the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Paquete::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Paquete $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='paquete-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
