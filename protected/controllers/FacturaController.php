<?php

class FacturaController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/comprobantes';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','FormAsync','PdfxCarga','Pdf','Pdf2',
                    'SaveFieldAsync','json','CrearNotaCredito','Talonario','DesvincularNP'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_factura")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionDesvincularNP($id){
        $oFactura = $this->loadModel($id);
        $oFactura->idNotaPedido = null;
        //todo: handle error
        $oFactura->save();

        $oComprobante = CargaDocumentos::model()->findByAttributes(array('idComprobante' => $oFactura->id,'tipoDoc'=>'FAC'));
        if($oComprobante != null){
            $oComprobante->delete();
        }
    }

	public function actionCrearNotaCredito($id){
		$oFactura = $this->loadModel($id);

        $oNotaCredito = $oFactura->crearNotaCredito();

        if($oNotaCredito <> null){
            $this->redirect(array('NotaCredito/update','id'=> $oNotaCredito->id));
        }else{
            throw new CHttpException('500','Error al crear la Nota De Credito');
        }

	}

	public function actionJson($id){
        $oComprobante = $this->loadModel($id);
        if($oComprobante->estado < Comprobante::iCerrada){
            $oComprobante->recalcular();
            if (!$oComprobante->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobante));
            }
        }
        $attributes = $oComprobante->attributes;
        $attributes['AlicuotaItem'] = $oComprobante->getAlicuotaItem();
        echo json_encode($attributes);
    }

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oFactura = new Factura($_POST['scenario']);

        $oFactura = Factura::model()->findByPk($_POST['pk']);
        if ($oFactura != null) {
            $oFactura->setAttribute($_POST['name'],$_POST['value']);
            $oFactura->Vencim_CAE = (ComponentesComunes::jpicker2db($oFactura->Vencim_CAE) == '0000-00-00')? null : ComponentesComunes::jpicker2db($oFactura->Vencim_CAE);
            if (!$oFactura->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oFactura));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }



    /**
     * Displays a particular model  in pdf.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionPdf($id,$print=false)
    {

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew',10,10,10,5,9,9,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew',10,10,10,5,9,9,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/facturaPdf.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $this->layout = 'column1Pdf';

        $aProds = new ProdFactura('search');
        $aProds->unsetAttributes();
        $aProds->idComprobante = $id;

        $model = $this->loadModel($id);
        $model->createBarcodeImg();

        //TODO: CUANDO SE ENVIE POR CORREO, ELIMINAR LA IMPRESION DEL ORIGINAL
        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProdFactura' => $aProds,
                'copia' => 'ORIGINAL',
            ),true)
        );

        $mPDF1->AddPage('P','', '','','',10,10,10,5,9,9);

        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProdFactura' => $aProds,
                'copia' => 'DUPLICADO',
            ),true)
        );

        $filename = 'Factura_'.str_pad($model->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT).'-'.str_pad($model->Nro_Comprobante,8,'0',STR_PAD_LEFT).'.pdf';


        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);

            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
    }

    public function actionPdfxCarga($idCarga){
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew',2,2,5,20,9,9,'P');

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/facturaPdf.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $this->layout = 'column1Pdf';

        //TODO: FILTRAR POR CARGA $mPDF1->AddPage();
        $criteria = new CDbCriteria();
        $criteria->join = 'LEFT JOIN notaPedido np on Factura.idNotaPedido=np.id';
        $criteria->condition = 'np.idCarga ='.$idCarga;

        $aFacturas = Factura::model()->findAll($criteria);
        foreach ($aFacturas as $index => $oFactura) {
            $aProds = new ProdFactura('search');
            $aProds->unsetAttributes();
            $aProds->idComprobante = $oFactura->id;
            $model = $oFactura;
            $model->createBarcodeImg();


            //TODO: CUANDO SE ENVIE POR CORREO, ELIMINAR LA IMPRESION DEL ORIGINAL
            $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProdFactura' => $aProds,
                'copia' => 'ORIGINAL',
            ),true)
            );

            $mPDF1->AddPage('P','', '','','',10,10,10,5,9,9);

            $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProdFactura' => $aProds,
                'copia' => 'DUPLICADO',
            ),true)
            );
            $mPDF1->AddPage('P','', '','','',10,10,10,5,9,9);
        }

        $mPDF1->Output('Factura_'.$model->Nro_Comprobante.'-'.$model->clase.'.pdf','I');
    }


    /**
     * Displays a particular model  in pdf.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionPdf2($id)
    {

        $oFactura = $this->loadModel($id);

        $oProds = new ProdFactura('search');
        $oProds->unsetAttributes();
        $oProds->idFactura = $id;

        $datos = $oProds->search();
        $datos->setPagination(false);

        $aProds = ProdFactura::model()->findAll($datos->criteria);
        $aItems = array();

        $id = 0;
        foreach ($aProds as $index => $data) {
            $aItems[$id]['cod']      = $data->oArtVenta->codigo;
            $aItems[$id]['producto'] = $data->oArtVenta->descripcion;
            $aItems[$id]['cant']     = $data->cantidad;
            $aItems[$id]['precio']   = "$".number_format($data->precioUnitario,2);
            $aItems[$id]['subtot']   = "$".$data->SubTotal;
            $id++;
        }



        if ($oFactura->Nro_Puesto_Venta == 10){
            if($oFactura->clase == 'B'){
                $r = new YiiReport(array('template'=> 'modeloFacturaB10.xlsx'));
            }else{
                $r = new YiiReport(array('template'=> 'modeloFacturaA10.xlsx'));
            }
        }else{
            if($oFactura->clase == 'B'){
                $r = new YiiReport(array('template'=> 'modeloFacturaB9.xlsx'));
                //$r = new YiiReport(array('template'=> 'modeloFacturaA.xlsx'));
            }else{
                $r = new YiiReport(array('template'=> 'modeloFacturaA.xlsx'));
            }
        }


        $aDocumento = array(
            'nroPunto' => $oFactura->Nro_Puesto_Venta,
            'nroComp' => $oFactura->Nro_Comprobante,
            'fecha' => ComponentesComunes::fechaFormateada($oFactura->fecha),
            'cliente' => $oFactura->oCliente->title,
            'domicilio' => $oFactura->oCliente->direccion,
            'nroCuenta' => $oFactura->Id_Cliente,
            'respIva' => $oFactura->oIvaResponsable->nombre,
            'cuit' => $oFactura->oCliente->cuit,
            'subtotal' => "$".number_format($oFactura->SubTotal,2),
            'descuento' => "$".number_format($oFactura->Descuento1,2),
            'netoGrav' => "$".number_format($oFactura-> Total_Neto,2),
            'iva' => $oFactura->Otro_Iva,
            'Total' => "$".number_format($oFactura->Total_Final,2),
            'montoEnPalabras' => ComponentesComunes::numtoletras($oFactura->Total_Final),
            'CAE' => $oFactura->CAE,
            'vtoCae' => ComponentesComunes::fechaFormateada($oFactura->Vencim_CAE),
            'CodigoBarras' => $oFactura->codigoDeBarras(),

        );

        /*
        echo '<pre>';
        print_r($aItems);
        echo '</pre>';

        echo '<pre>';
        print_r($aDocumento);
        echo '</pre>';
        die();
*/
        //PARA DATOS ERCA
        //$r->getobjPHPExcel()->getActiveSheet()->mergeCells('B2:C2');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B3:C3');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B4:C4');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B5:C5');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B6:C6');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B7:C7');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B8:C8');
        //NRO FACTURA
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D3:E3');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D4:E4');

        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D6:E6');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D7:E7');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D8:E8');

        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D10:F10');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D11:F11');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('D12:F12');
        //FOOTER "LA MERCADERIA...."
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B42:F42');
        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B43:F43');


        $r->getobjPHPExcel()->getActiveSheet()->mergeCells('B12:C12');

        $r->getobjPHPExcel()->getActiveSheet()->getPageMargins()->setTop(0.1);
        $r->getobjPHPExcel()->getActiveSheet()->getPageMargins()->setRight(0.1);
        $r->getobjPHPExcel()->getActiveSheet()->getPageMargins()->setLeft(0.1);
        $r->getobjPHPExcel()->getActiveSheet()->getPageMargins()->setBottom(1);


        //require_once 'application.components.Barcode';
        //Yii::app()->basePath.'/components/Barcode.php');
        //$imgBarcode = Barcode::generate($oFactura->codigoDeBarras());
        $imgBarcode = I2of5::picture($oFactura->codigoDeBarras(),true);

/*
        $imgBarcode = base64_decode('iVBORw0KGgoAAAANSUhEUgAAABwAAAASCAMAAAB/2U7WAAAABl'
            . 'BMVEUAAAD///+l2Z/dAAAASUlEQVR4XqWQUQoAIAxC2/0vXZDr'
            . 'EX4IJTRkb7lobNUStXsB0jIXIAMSsQnWlsV+wULF4Avk9fLq2r'
            . '8a5HSE35Q3eO2XP1A1wQkZSgETvDtKdQAAAABJRU5ErkJggg==');

        header('Content-Type: image/png');

        imagepng($imgBarcode);
        imagedestroy($imgBarcode);

        */


        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $objDrawing->setDescription('barcode');
        $objDrawing->setImageResource($imgBarcode);
        //$objDrawing->setPath(Yii::app()->request->baseUrl.'/images/ms-icon-310x310.png');
        $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
        $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
        $objDrawing->setHeight(30);
        $objDrawing->setCoordinates('B50');
        $objDrawing->setWorksheet($r->getobjPHPExcel()->getActiveSheet());

        $styleArray = array(
            'font'  => array(
                'name'  => "couriernew"
            ));

        $r->getobjPHPExcel()->getDefaultStyle()->applyFromArray($styleArray);

        $r->load(array(
                array(
                    'id' => 'documento',
                    'repeat'=>false,
                    'data' => $aDocumento,
                ),
                array(
                    'id' => 'item',
                    'repeat'=>true,
                    'data' => $aItems,
                    'minRows'=>1,
                )
            )
        );

        //echo $r->render('pdf', 'Factura_'.$oFactura->clase.'_'.$oFactura->Nro_Puesto_Venta.'-'.$oFactura->Nro_Comprobante);
        echo $r->render('pdf', 'Factura_'.$oFactura->clase.'_'.$oFactura->id);

    }


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Factura #'.$id;

        $is_modal = isset($_GET['is_modal']);

        $aProds = new ProdFactura('search');
        $aProds->unsetAttributes();
        $aProds->idComprobante = $id;
        $model = $this->loadModel($id);
        $model->createBarcodeImg();

        $model->calcularNumero();

        $this->render('view',array(
			'model'=>$model,
            'aProdFactura' => $aProds,
            'is_modal' => $is_modal,
		));
	}

    /**
     * Displays a particular model partialy to use in ajax call
     *
     * @param integer $id the ID of the model to be displayed (if you will update)
     * @param integer $idNotaPedido the ID of the associated NotaPedido
     */

    public function actionFormAsync($idNotaPedido,$id = null,$idCliente = null)
    {

        $is_modal = isset($_GET['is_modal']);

        $aProdFactura = new ProdFactura('search');
        $aProdFactura->unsetAttributes();
        $modelProd = new ProdFactura();

        if ($id > 0){
            $model = $this->loadModel($id);
            $modelProd->idComprobante = $id;
            $aProdFactura->idComprobante = $id;
        }else{
            if ($idNotaPedido > 0){
                $transaction=Yii::app()->db->beginTransaction();
                Yii::log(' $idNotaPedido ::'.$idNotaPedido);
                try
                {

                    $model = Factura::generarFa($idNotaPedido);
                    Yii::log('Factura::generarFa :','warning');
                    if ($model == null){
                        $transaction->rollback();
                        throw new CHttpException('500','Error');

                    }
                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollback();
                    throw new CHttpException('500','Error'.$e->getMessage());
                }

            }else{
                //TODO: IMPLEMENTAR...
                $model = Factura::generarFa(null,$idCliente);
                if($model != null){
                    echo $model->id;
                }else{

                    throw new CHttpException('500','Error al generar la factura:'.CHtml::errorSummary($model));
                }
            }
            $modelProd->idComprobante = $model->id;
            $aProdFactura->idComprobante = $model->id;

        }
        //if(!Yii::app()->request->isAjaxRequest){
            $model->calcularNumero();
        //}
        

        if ($model->estado >= Comprobante::iCerrada ){
            $this->renderPartial('view',array(
                'model'=>$model,
                'aProdFactura' => $aProdFactura,
                'is_modal' => $is_modal,
            ),false,true);

        }else{
            $this->renderPartial('_formFila',array(
                'model'=>$model,
                'aProdFactura' => $aProdFactura,
                'modelProd' => $modelProd,
                'ajaxMode' => true,
                'is_modal' => $is_modal
            ), false,true);
        }

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new Factura;
		$this->pageTitle = "Nuevo Factura";
        $aProdFactura = new ProdFactura('search');
        $aProdFactura->unsetAttributes();
        $modelProd = new ProdFactura();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Factura']))
		{
			$model->attributes=$_POST['Factura'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
            'aProdFactura' => $aProdFactura,
            'modelProd' => $modelProd,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Factura;
		$this->pageTitle = "Nuevo Factura";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Factura']))
		{
			$model->attributes=$_POST['Factura'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionTalonario($id)
    {
        $model=$this->loadModel($id);
        if($model->estado != Comprobante::iBorrador && $model->estado != Comprobante::iGuardada){
            $this->redirect(array('view','id'=>$model->id));
        }

        $this->pageTitle = 'Modificando Factura #'.$id;

        $aProdFactura = new ProdFactura('search');
        $aProdFactura->unsetAttributes();
        $aProdFactura->idComprobante = $id;
        $modelProd = new ProdFactura();
        $modelProd->idComprobante = $id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Factura']))
        {
            $model->attributes=$_POST['Factura'];
            if(isset($_POST['cerrar'])){
                $model->estado = Factura::iCerrada;
            }
            if($model->save()){
                $this->redirect(array('view','id'=>$model->id));
            }
        }

        $this->render('talonario',array(
            'model'=>$model,
            'aProdFactura' => $aProdFactura,
            'modelProd' => $modelProd,
            'is_modal' => false,
            'ajaxMode' => true,
        ));

    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
        $model=$this->loadModel($id);
        if($model->estado != Comprobante::iBorrador && $model->estado != Comprobante::iGuardada){
            $this->redirect(array('view','id'=>$model->id));
        }

		$this->pageTitle = 'Modificando Factura #'.$id;

        $aProdFactura = new ProdFactura('search');
        $aProdFactura->unsetAttributes();
        $aProdFactura->idComprobante = $id;
        $modelProd = new ProdFactura();
        $modelProd->idComprobante = $id;

        // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Factura']))
		{
			$model->attributes=$_POST['Factura'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

        $model->calcularNumero();

        $this->render('update',array(
            'model'=>$model,
            'aProdFactura' => $aProdFactura,
            'modelProd' => $modelProd,
            'is_modal' => false,
            'ajaxMode' => true,
        ));

	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$oFactura = Factura::model()->findByPk($id);
		if ($oFactura != null){
            $oFactura->delete();
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Factura('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Factura']))
			$model->attributes=$_GET['Factura'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Factura('search');
		$this->pageTitle = 'Administrando Facturas ';

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Factura'])){
			$model->attributes=$_GET['Factura'];
		}else{

		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Factura the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Factura::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Factura $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='factura-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
