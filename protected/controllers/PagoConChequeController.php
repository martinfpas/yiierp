<?php

class PagoConChequeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','deleteAll','index','view','admin','SaveAsync','SaveFieldAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver PagoConCheque #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PagoConCheque;
		$this->pageTitle = "Nuevo PagoConCheque";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoConCheque']))
		{
			$model->attributes=$_POST['PagoConCheque'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new PagoConCheque;
		$this->pageTitle = "Nuevo PagoConCheque";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoConCheque']))
		{
			$model->attributes=$_POST['PagoConCheque'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    public function actionSaveFieldAsync()
    {
        if (!$_POST['pk']) {
            throw new CHttpException('500', 'Error de parametros');
        }
        $model = PagoConCheque::model()->findByPk($_POST['pk']);
        if ($model != null){
            $model->setAttribute($_POST['name'],$_POST['value']);
            if(!$model->save()){
                throw new CHttpException('500','Error'.CHtml::errorSummary($model));
            }
        }else{
            throw new CHttpException('500','No es un '.PagoConCheque::model()->getAttributeLabel('model').' existente');
        }
        echo $_POST['value'];
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando PagoConCheque #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoConCheque']))
		{
			$model->attributes=$_POST['PagoConCheque'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}


    /**
     * Borra el Pago y el cheque ingresado
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDeleteAll($id)
    {
        $oPagoCH = PagoConCheque::model()->findByPk($id);
        if ($oPagoCH != null){
            $oCheque = $oPagoCH->oChequedeTercero;
            $oPagoCH->delete();
            $oCheque->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }else{
            Yii::log('$oPagoCH == null:'.$id,'error');
        }

    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$oPagoCH = PagoConCheque::model()->findByPk($id);
		if ($oPagoCH != null){
            $oPagoCH->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }else{
		    Yii::log('$oPagoCH == null:'.$id,'error');
        }

    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PagoConCheque('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoConCheque']))
			$model->attributes=$_GET['PagoConCheque'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PagoConCheque('search');
		$this->pageTitle = 'Administrando Pago Con Cheques ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoConCheque'])){
			$model->attributes=$_GET['PagoConCheque'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PagoConCheque the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PagoConCheque::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PagoConCheque $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pago-con-cheque-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
