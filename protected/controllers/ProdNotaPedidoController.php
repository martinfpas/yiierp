<?php

class ProdNotaPedidoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','FormAsync','SaveFieldAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

//      $oProdNotaPedido = new ProdNotaPedido($_POST['scenario']);
        $oProdNotaPedido = ProdNotaPedido::model()->findByPk($_POST['pk']);
        if ($oProdNotaPedido != null) {
            $oProdNotaPedido->setAttribute($_POST['name'],$_POST['value']);
            if (!$oProdNotaPedido->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oProdNotaPedido));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }
	
	public function actionFormAsync($idNotaPedido) {
		
		$oProdNotaPedido = new ProdNotaPedido();
		$oProdNotaPedido->idNotaPedido = $idNotaPedido;
		$aProdNotaPedido = new ProdNotaPedido('search');
		$aProdNotaPedido->idNotaPedido = $idNotaPedido;
		
		
		$oArticuloVenta = new ArticuloVenta();
		
		$this->renderPartial('_formFilaCSearch',array(
			'model' => $oProdNotaPedido,
			'aProdNotaPedido' => $aProdNotaPedido,
			'oArticuloVenta' => $oArticuloVenta,
		),false,true);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver ProdNotaPedido #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProdNotaPedido;
		$this->pageTitle = "Nuevo ProdNotaPedido";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdNotaPedido']))
		{
			$model->attributes=$_POST['ProdNotaPedido'];
			if($model->save()){
                $this->redirect(array('view','id'=>$model->id));
            }else{
			    throw new CHttpException(500,'<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ProdNotaPedido;
		$this->pageTitle = "Nuevo ProdNotaPedido";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdNotaPedido']))
		{
			$model->attributes=$_POST['ProdNotaPedido'];

			if($model->save()){
				die($model->id);
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando ProdNotaPedido #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdNotaPedido']))
		{
			$model->attributes=$_POST['ProdNotaPedido'];
			if($model->save()){

            }else {
                throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        Yii::log('pre delete','warning');
	    try{
	        if($id > 0){
                $model=ProdNotaPedido::model()->findByPk($id);
                Yii::log('pre delete '.$id,'warning');
                if($model != null){
                    Yii::log('MODEL NOT NULLL  '.$id,'warning');
                    if($model->delete()){
                        Yii::log('pudo borrar ...'.$id,'warning');
                        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                        if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                    }else{
                        Yii::log('NO pudo borrar :'.CHtml::errorSummary($model),'error');
                    };

                }
            }

        }catch (Exception $e){
            Yii::log($e->getMessage(),'errror');
        }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ProdNotaPedido('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdNotaPedido']))
			$model->attributes=$_GET['ProdNotaPedido'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProdNotaPedido('search');
		$this->pageTitle = 'Administrando Prod Nota Pedidos ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdNotaPedido'])){
			$model->attributes=$_GET['ProdNotaPedido'];
		}else{
			$model->activo = ProdNotaPedido::iActivo;
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProdNotaPedido the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProdNotaPedido::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ProdNotaPedido $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='prod-nota-pedido-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
