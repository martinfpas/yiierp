<?php

class AuthItemController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','viewRelations','EditRelations','nuevaRelacion','ActualizaFecha'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionActualizaFecha(){	
		echo shell_exec('sh /var/www/asdfasdferca/protected/config/timedatectl.sh');
		$this->render('refreshDate');	
	}


	public function actionNuevaRelacion(){
		$model = new AuthItemChild();

		if (isset($_POST['AuthItemChild'])){
            $model->attributes = $_POST['AuthItemChild'];
            if ($model->save()){
                $this->redirect(array('authItem/viewRelations'));
            }

        }

		$this->render('nuevaRelacion',array('model' => $model));
	}
	
	public function actionViewRelations() {
		$criteria = new CDbCriteria;
		
		$criteria->join = 'LEFT JOIN AuthItemChild AS AIC ON t.name = AIC.child';
		$criteria->select = 't.name';
		$criteria->addCondition('ISNULL( AIC.parent )');
		
		$aPrincipales = AuthItem::model()->findAll($criteria);
		
		$aArbol = array();
		
		foreach ($aPrincipales as $key => $oAI) {
			$aArbol[$oAI->name] = AuthItemChild::model()->getRama($oAI->name); 
			/*
			echo $oAI->name.': <br><pre>';
			print_r($aArbol[$oAI->name]);
			echo '</pre>';
			*/
		}
		
		$this->render('arbolView',array('aArbol' => $aArbol));
		
	}

	public function actionEditRelations($id) {
		
		$oUser = User::model()->findByPk($id);
		if ($oUser == null) {
			throw new CHttpException('500','Usuario no existente');
		}
		
		$aAssignAssignment = AuthAssignment::model()->findAllByAttributes(array('userid' => $id));		
		
		if (isset($_POST['modulo'])) {
			
			foreach ($aAssignAssignment as $key => $oAssign) {
				$oAssign->delete();
			}
			
			foreach ($_POST['modulo'] as $key => $value) {
				$oAuthAssignment = AuthAssignment::model()->findByAttributes(array('userid'=> $id,'itemname' => $value));
				if ($oAuthAssignment == null){
                    $oAssign = new AuthAssignment();
                    $oAssign->userid = $id;
                    $oAssign->itemname = $value;
                    //TODO: LOGUEAR SI HAY ERROR
                    $oAssign->save();
                }
			}
			
			$this->redirect(array('user/view','id'=>$id));
			
			die();
		}
		
		foreach ($aAssignAssignment as $key => $oAssign) {
			$aAssign[$oAssign->itemname] = $oAssign->itemname;
		}
		
		$criteria = new CDbCriteria;
	
		$criteria->join = 'LEFT JOIN AuthItemChild AS AIC ON t.name = AIC.child';
		$criteria->select = 't.name';
		$criteria->addCondition('ISNULL( AIC.parent )');
	
		$aPrincipales = AuthItem::model()->findAll($criteria);
	
		$aArbol = array();
	
		foreach ($aPrincipales as $key => $oAI) {
			$aArbol[$oAI->name] = AuthItemChild::model()->getRama($oAI->name);
			/*
				echo $oAI->name.': <br><pre>';
				print_r($aArbol[$oAI->name]);
				echo '</pre>';
			*/
		}
	
		$this->render('arbolCheck',array('aArbol' => $aArbol,'aAssign' => $aAssign));
	
	}	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver AuthItem #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AuthItem;
		$this->pageTitle = "Nuevo AuthItem";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$model->attributes=$_POST['AuthItem'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->name));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new AuthItem;
		$this->pageTitle = "Nuevo AuthItem";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$model->attributes=$_POST['AuthItem'];
			if($model->save()){
				echo 'Ok';
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando AuthItem #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AuthItem']))
		{
			$model->attributes=$_POST['AuthItem'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->name));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new AuthItem('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AuthItem']))
			$model->attributes=$_GET['AuthItem'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AuthItem('search');
		$this->pageTitle = 'Administrando Auth Items ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AuthItem'])){
			$model->attributes=$_GET['AuthItem'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AuthItem the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AuthItem::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AuthItem $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='auth-item-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
