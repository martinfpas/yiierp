<?php

class UserController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('CronBCK','CronGD','Oauth'),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','resetPass','backup'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionOauth()
	{
		ComponentesComunes::print_array($_GET);
	}

	function actionCronGD($code = null){
		require_once Yii::getPathOfAlias('webroot').'/protected/vendors/google-api-php-client-PHP7.0/vendor/autoload.php';
		/* @var ImagenProducto $oImagen */
		if($code == null){
			Yii::log('actionCronBCK ','warning');		
			echo 'actionCronGD';
			ComponentesComunes::print_array($_GET);	
			return false;
		}

		$gd = new GDrive();	
		$service = new Google_Service_Drive($gd->getClient());


		//return false;

		$aImagenes = ImagenProducto::model()->noIdGDrive()->porIdProductoAsc()->findAll();
		$i=0;
		$idProducto = null;
		$idFolder = null;
		foreach($aImagenes as $key => $oImagen){
			$sourceFile = $oImagen->LocalFolder.strtoupper($oImagen->path);
			if(!file_exists($sourceFile)){
				printf('file not exist: '.$sourceFile);
			}else{
				if($idProducto != $oImagen->idProducto || $idFolder == null){
					$idProducto = $oImagen->idProducto;
					$folder = GDrive::searchFolder($oImagen->idProducto,GDrive::bckImgFolder,$service);
					if(isset($folder->id)){
						echo '$folder->id: '.$folder->id;
						$idFolder = $folder->id;
					}else{
						//crear carpeta
						echo 'crear carpeta: '.$oImagen->idProducto;
						// Create Test folder
						$fileMetadata = new Google_Service_Drive_DriveFile(array(
							'name' => $oImagen->idProducto,
							'parents' => [GDrive::bckImgFolder],
							'mimeType' => 'application/vnd.google-apps.folder'));
						$folder = $service->files->create($fileMetadata, array(
							'fields' => 'id'));
						printf("<br>New Folder ID: %s<br>", $folder->id);
						$idFolder = $folder->id;
					}					
				}
				if($idFolder != null){
					// Now lets try and send the metadata as well using multipart!
					$file = new Google_Service_Drive_DriveFile(array( 'name' => $oImagen->path, 'parents' => [$idFolder]));
					$result = $service->files->create(
						$file,
						array(
							'data' => file_get_contents($sourceFile),
							'mimeType' => 'application/octet-stream',
							'uploadType' => 'multipart'
						)
					);
					if(isset($result->id)){
						$oImagen->idGDrive = $result->id;
						//TODO: HANDLE ERROR
						$oImagen->save();
						// Now lets try and send the metadata as well using multipart!						
						$sourceTmbFile = $oImagen->LocalFolder.strtoupper('TMB_'.$oImagen->path);
						if(file_exists($sourceTmbFile)){
							$file = new Google_Service_Drive_DriveFile(array( 'name' => 'TMB_'.$oImagen->path, 'parents' => [$idFolder]));
							$result = $service->files->create(
								$file,
								array(
									'data' => file_get_contents($sourceTmbFile),
									'mimeType' => 'application/octet-stream',
									'uploadType' => 'multipart'
								)
							);
						}

					}else{
						ComponentesComunes::print_array($result);
						return false;
					}
				}else{
					die('idFolder null');
				}
			}
			ComponentesComunes::print_array($oImagen->attributes);
			echo '$i:'.$i++;
			if($i == 50){
				return false;
			}			
		}
	}

	function actionCronBCK($token = null){
		/*	*/
		Yii::log('actionCronBCK 2 ','warning');	
		
		define('STDIN',fopen("php://stdin","r"));

		if($token != 'G2103Bck'){
			Yii::log('actionCronBCK ','warning');		
			echo 'actionCronBCK';
			return false;
		}	

        Yii::import('ext.dumpDB.dumpDB');		
		$this->bckDb();

		$gd = new GDrive();
		$gd->saveDBs();
		
	}

	function bckDb(){
		$dumper = new dumpDB();
        $bk_file = Yii::app()->basePath.'/bck/erca-prod'.date('YmdHis').'.sql';
        echo $bk_file;
        $fh = fopen($bk_file, 'w+') or die("can't open file");
        fwrite($fh, $dumper->getDump(FALSE));
        fclose($fh);

		$filename = $bk_file.".zip";
		$zip = new ZipArchive();
		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			exit("cannot open <$filename>\n");
		}
		$zip->addFile($bk_file);
		$zip->close();
		unlink($bk_file);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionBackup()
    {
        Yii::import('ext.dumpDB.dumpDB');
        $dumper = new dumpDB();
        echo $dumper->getDump();
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->password = $model->password_1; 
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id));
            }else {
                throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id));
            }else {
                throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	public function actionResetPass($id) {
		$modeluser = new User('update');
		
		$this->performAjaxValidation($modeluser);
		
		if(!empty($_POST['User'])){
			$modeluser = $this->loadModel($id);
			// VER SI LA PASSWORD ANTERIOR COINCIDE
			if (1) {
				$modeluser->password_1 = $_POST['User']['password_1'];
				$modeluser->password_confirm = $_POST['User']['password_confirm'];
				if ($modeluser->save()) {
					$this->redirect(array('user/admin'));
                }else {
                    throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($modeluser));
                }
			}else {
				// TODO: CHEQUEAR EL MENSAJE SIGUIENTE
				$modeluser->addError('password','Old Password does not match');
			}
		}else {
			
			$modeluser = $this->loadModel($id);
		};
		
		$this->render('resetPass',array('modeluser' => $modeluser));
		
		echo 'hola5';
		
	}
	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//die('xxx');
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
