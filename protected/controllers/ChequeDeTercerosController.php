<?php

class ChequeDeTercerosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ImportarDatosCsv','select2','Json'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
    public function actionImportarDatosCsv() {

	    $aMigraSucursales = MigraSucursal::model()->findAll();

        foreach ($aMigraSucursales as $index => $aMigraSucursal) {
            $aSucursales[$aMigraSucursal->id_banco.'-'.$aMigraSucursal->id_sucursal] = $aMigraSucursal->id;
	    }

/*
 * 	    /// TODO: TERMINAR
        $aBanco = Banco::model()->findAll();

        $aBancos = array();
        foreach ($aBanco as $key => $oBanco) {
            $aBancos[$oBanco->id] = $oBanco->id;
        } */


        $fp = fopen('/var/www/erca/upload/cheque.csv', 'r');
        if($fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;
            $first_time = true;
            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                /*echo '<br>';
                print_r($line);
                die;
                0 nrocheque
                1 fecha
                2 importe
                3 id_cliente
                4 del cliente o no
                5 id_banco
                6 id_sucursal


                */

                if (isset($aSucursales[$line[5].'-'.$line[6]])){
                    $oChequeDe3ros = new ChequeDeTerceros();
                    $oChequeDe3ros->nroCheque = $line[0];
                    $sFecha = ComponentesComunes::jpicker2db($line[1]);
                    echo $sFecha.'<br>';
                    $oChequeDe3ros->fecha = $sFecha;
                    $oChequeDe3ros->importe = $line[2];
                    $oChequeDe3ros->id_cliente = $line[3];
                    $oChequeDe3ros->propio = ($line[4] == 'yes')? 1 : 0;
                    $oChequeDe3ros->id_sucursal = $aSucursales[$line[5].'-'.$line[6]];
                    
                    if (!$oChequeDe3ros->save()) {

                        echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                        print_r($oChequeDe3ros->getErrors());
                    }else{
                        $oMigraChequeDe3Ro = new MigraChequeDe3Ro();
                        $oMigraChequeDe3Ro->id = $oChequeDe3ros->id;
                        $oMigraChequeDe3Ro->nroCheque = $oChequeDe3ros->nroCheque;
                        $oMigraChequeDe3Ro->id_banco = $line[5];
                        $oMigraChequeDe3Ro->id_sucursal = $line[6];
                        if (!$oMigraChequeDe3Ro->save()){
                            echo '<br> NO se guardo MIGRA :: '.$line[5].'-'.$line[6].'<br>';
                            print_r($oMigraChequeDe3Ro->getErrors());
                            print_r($oMigraChequeDe3Ro->attributes);
                        }else{
                            echo '--> MIGRA :: '.$line[5].'-'.$line[6].'<br>';
                        }

                    }
                }else{
                    echo '<br> NO existe :: '.$line[0].':: el indice '.$line[5].'-'.$line[6].'<br>';
                }




            };

        }
    }

    public function actionSelect2(){
        /** POR DEFECTO SOLO TRAE LOS QUE ESTAN EN CARTERA
	    * @var $oModel ChequeDeTerceros
        * @var $aModel ChequeDeTerceros[]
        * @var $models ChequeDeTerceros  */

        $criteria = new CDbCriteria();

        if(isset($_GET['partial'])){
            $criteria->compare('nroCheque',$_GET['q'],true);
        }else{
            $criteria->compare('nroCheque',$_GET['q'],false);
        }

        if(isset($_GET['id_banco'])){
            $criteria->compare('id_banco',$_GET['id_banco'],false);
        }
        if(isset($_GET['page_limit'])){
            $criteria->limit = $_GET['page_limit'];
        }
        //$criteria->offset = $_GET['page'] * $_GET['page_limit'];

        if(!isset($_GET['estado'])) {
            $models = ChequeDeTerceros::model()->enCartera()->findAll($criteria);
        }else{
            $criteria->addCondition('estado='.$_GET['estado']);
            $models = ChequeDeTerceros::model()->findAll($criteria);
        }

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $sBanco = ($oModel->oBanco != null)? $oModel->oBanco->nombre : '';
            //$aModels[] = array('id' => $oModel->id,'text' => $oModel->nroCheque.' : '.$sBanco,'importe' => $oModel->importe);
            $aModels[] = array('id' => $oModel->id,'text' => $oModel->nroCheque.' : $'.number_format($oModel->importe,2,'.',''),'importe' => $oModel->importe,'sBanco' => $sBanco);
        }

        echo CJSON::encode($aModels);
    }

    public function actionJson($id){
        $model = $this->loadModel($id);

        echo json_encode(array_merge($model->attributes,array('importeDisponible'=>$model->importeDisponible())));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver ChequeDeTerceros #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ChequeDeTerceros;
		$this->pageTitle = "Nuevo ChequeDeTerceros";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequeDeTerceros']))
		{
			$model->attributes=$_POST['ChequeDeTerceros'];
			$model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            if ($model->id_pagoCliente > 0){
                $oPagoCliente = PagoCliente::model()->findByPk($model->id_pagoCliente);
                $model->id_cliente = $oPagoCliente->idCliente;
                $oPagoConCheque = new PagoConCheque();
                $oPagoConCheque->monto = $model->importe;

            }
			if($model->save()){
                if($oPagoConCheque != null){
                    $oPagoConCheque->idChequedeTercero = $model->id;
                    if(!$oPagoConCheque->save()){
                        //TODO:MANEJAR EL ERROR
                        Yii::log('Error al guardar pagoConCheque::'.CHtml::errorSummary($oPagoConCheque),'error');
                    }
                }
                $this->redirect(array('view','id'=>$model->id));
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ChequeDeTerceros;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequeDeTerceros']))
		{
		    if (isset($_POST['ChequeDeTerceros']['id']) && $_POST['ChequeDeTerceros']['id'] > 0){
		        $model = $this->loadModel($_POST['ChequeDeTerceros']['id']);
            }
			$model->attributes=$_POST['ChequeDeTerceros'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            $model->id_pagoCliente = (isset($_POST['ChequeDeTerceros']['id_pagoCliente'])? $_POST['ChequeDeTerceros']['id_pagoCliente'] : 0);
            if ($model->id_pagoCliente > 0){
                $oPagoCliente = PagoCliente::model()->findByPk($model->id_pagoCliente);
                $model->id_cliente = $oPagoCliente->idCliente;
                $oPagoConCheque = new PagoConCheque();
                $oPagoConCheque->monto = $model->importe;
                $oPagoConCheque->idPagodeCliente = $model->id_pagoCliente;

            }else{
                Yii::log('$model->id_pagoCliente no es mayor a cero','warning');
            }
            ComponentesComunes::print_array($model->attributes);
			if($model->save()){
			    if($oPagoConCheque != null){
			        $oPagoConCheque->idChequedeTercero = $model->id;
                    if(!$oPagoConCheque->save()){
                        //TODO:MANEJAR EL ERROR
                        Yii::log('Error al guardar pagoConCheque::'.CHtml::errorSummary($oPagoConCheque),'error');
                    }
                }
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

        $this->pageTitle = 'Modificando ChequeDeTerceros #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequeDeTerceros']))
		{
			$model->attributes=$_POST['ChequeDeTerceros'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		if($model->oSucursal != null){
            $model->id_banco = $model->oSucursal->id_banco;
        }else{
		    $oSucursal = SucursalBanco::model()->findByPk($model->id_sucursal);
		    if($oSucursal != null){
                $model->id_banco = $oSucursal->id_banco;
            }
        }


		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ChequeDeTerceros('search');
		$model->unsetAttributes();  // clear any default values
        $model->estado = ChequeDeTerceros::iEnCartera;

		if(isset($_GET['ChequeDeTerceros']))
			$model->attributes=$_GET['ChequeDeTerceros'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ChequeDeTerceros('search');
		$this->pageTitle = 'Administrando Cheque De Terceroses ';

		$this->layout ='//layouts/column1';

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ChequeDeTerceros'])){
			$model->attributes=$_GET['ChequeDeTerceros'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ChequeDeTerceros the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ChequeDeTerceros::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ChequeDeTerceros $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cheque-de-terceros-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
