<?php

class CodigoPostalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','CodigosAsync','ZpList','updateMigra'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_codigo_postal")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	function actionUpdateMigra(){
        $user = 'phpmyadminuser';
        $pass = 'P@ssw0rd';
        $db = new PDO('mysql:host=localhost;dbname=erca;', $user, $pass);


        $stmt = $db->prepare("SELECT * FROM migra_Codigos_Postales");
        $stmt->execute();
        $cps = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i = 1;
        foreach ($cps as $cp) {
            if(strlen($cp->Codigo_Postal) != 0 && strlen($cp->Codigo_Postal) > 0){
                $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $cp->Codigo_Postal,'zp'=>$cp->Zona_Postal));
                if($oLocalidad!= null){
                    $dif = (strlen($oLocalidad->nombrelocalidad) - strlen($cp->Localidad));
                    if ($cp->Localidad != $oLocalidad->nombrelocalidad ){ //&& abs($dif) < 3 //&& strpos($oLocalidad->nombrelocalidad,'?') > 0
                        //ComponentesComunes::print_array($oLocalidad->attributes);
                        //ComponentesComunes::print_array($cp);
                        $oLocalidad->nombrelocalidad = $cp->Localidad;
                        //$oLocalidad->save();
                    }else{
                        //echo 'No se encuentra: '.$i++.' '.ComponentesComunes::print_array($cp,true);
                    }
                }else{
                    echo '<br>
                    >';
                    ComponentesComunes::print_array($cp);
                    echo '<';
                }

            }
        }

    }
	
	public function actionZpList()
	{
		//print_r($_POST);
	
		if (isset($_POST['cp'])) {
			$data=CodigoPostal::model()->todosXnombre()->findAll('codigoPostal=:parent_id',
					array(':parent_id'=>$_POST['cp']));
		}else{
			/*
			$data=Articulo::model()->todosId()->findAll('idCat1=:parent_id',
					array(':parent_id'=>(int)$_POST['id_rubro']));
					
			*/		
		}
	
	
		$data=CHtml::listData($data,'zp','IdZp');
	
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionCodigosAsync(){
		$cp = (isset($_GET['term'])? $_GET['term'] : NULL );
	
		
		$arr = array();
		$aCp = array();
		
		$dp = CodigoPostal::model()->sarchCodigos($cp);
		$arr = $dp->getData();
		
		foreach ($arr as $oCp) {
            $aAux = $oCp->attributes;
            $aAux['label'] = $oCp->IdTituloYProv;
            $aAux['value'] = $oCp->codigoPostal;
			$aCp[] = $aAux;

		}
	
		echo CJSON::encode($aCp);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($codigoPostal,$zp)
    {
        $model= CodigoPostal::model()->findByAttributes(array('codigoPostal'=> $codigoPostal , 'zp' => $zp));

        if($model == null){
            throw new CHttpException('500','No existe el código postal');
        }

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CodigoPostal;
		$this->pageTitle = "Nuevo CodigoPostal";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CodigoPostal']))
		{
			$model->attributes=$_POST['CodigoPostal'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->codigoPostal));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new CodigoPostal;
		$this->pageTitle = "Nuevo CodigoPostal";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CodigoPostal']))
		{
			$model->attributes=$_POST['CodigoPostal'];
			if($model->save()){
				echo 'Ok';
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($codigoPostal,$zp)
	{
		$model= CodigoPostal::model()->findByAttributes(array('codigoPostal'=> $codigoPostal , 'zp' => $zp));
		if($model == null){
		    throw new CHttpException('500','No existe el codigo postal');
        }

		$this->pageTitle = 'Modificando CodigoPostal #'.$model->nombrelocalidad;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CodigoPostal']))
		{
			$model->attributes=$_POST['CodigoPostal'];
			if($model->save())
				$this->redirect(array('view','codigoPostal'=> $codigoPostal , 'zp' => $zp));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new CodigoPostal('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CodigoPostal']))
			$model->attributes=$_GET['CodigoPostal'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CodigoPostal('search');
		$this->pageTitle = 'Administrando Codigo Postals ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CodigoPostal'])){
			$model->attributes=$_GET['CodigoPostal'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CodigoPostal the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CodigoPostal::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CodigoPostal $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='codigo-postal-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
