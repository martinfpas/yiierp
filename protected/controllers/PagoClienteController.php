<?php
/* @var $oCargaDoc CargaDocumentos*/
class PagoClienteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Form','NewBlank','Multipago','SumPagoRelacionado',
                    'AplicarPagoRelacionado','JsonMulti','SaveFieldAsync','FormFilaCh'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
    }
    
    public function actionFormFilaCh($id){
        $model =$this->loadModel($id);

        $aPagoConCheque = new PagoConCheque('search');
        $aPagoConCheque->unsetAttributes();
        $aPagoConCheque->idPagodeCliente = $model->id;

        $oChequeTerceros = new ChequeDeTerceros();
        $oChequeTerceros->id_pagoCliente = $id;

        $this->renderPartial('_formFilaCH',array(
            'model'=>$model,
            'model'=>$oChequeTerceros,
            'aPagoConCheque' => $aPagoConCheque,
            'bEditable' => true,
            'isAjaxMode'=> true
		), false,true);
    }

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

//      $oProdNotaPedido = new ProdNotaPedido($_POST['scenario']);
        $oPagoCliente = PagoCliente::model()->findByPk($_POST['pk']);
        if ($oPagoCliente != null) {
            $oPagoCliente->setAttribute($_POST['name'],$_POST['value']);
            if (!$oPagoCliente->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oPagoCliente));
            }
        }else {
            throw new CHttpException('500','No es un Pago de Cliente existente');
        }
        echo $_POST['value'];
    }

	public function actionJsonMulti($idPagoRel){
        $oMultipago = Multipago::model()->findByPk($idPagoRel);
        if($oMultipago == null){
            throw new CHttpException('500','Parametros incorrecctos');
        }
        echo json_encode($oMultipago->attributes);
    }

    public function actionAplicarPagoRelacionado($idPagoRelacionado){
        /**
         * @var $oPagoRelConCheque PagoConCheque
         * @var $oPagosDeEntregaDoc VPagosDeEntregaDoc
         */

        $transaction=Yii::app()->db->beginTransaction();

        $aNC = VPagosDeEntregaDoc::model()->findAll("tipoDoc='".CargaDocumentos::NC."' AND idPago =".$idPagoRelacionado);
        $oPagoRelacionado = PagoCliente::model()->findByPk($idPagoRelacionado);
        if($oPagoRelacionado == null){
            throw new Exception('$oPagoRelacionado es nulo');
        }
        // esto es para restar al relacionado la suma de los otros comprobantes
        $ft = $oPagoRelacionado->getMontoEfectivo();
        $ch = $oPagoRelacionado->getMontoCheque();


        $totalNC = 0;
        foreach ($aNC as $index => $oNC) {
            $totalNC += $oNC->totalFinal * (-1);
        }

        $bPagoRelacionadoSoloEfectivo = false;
        $aPagosDeEntregaDoc = VCargaDocDePago::model()->findAll("tipoDoc != '".CargaDocumentos::NC."' AND idPago =".$idPagoRelacionado);

        $i=0;
        foreach ($aPagosDeEntregaDoc as $index => $oPagosDeEntregaDoc) {
            $esElUltimo = (($i+1) == sizeof($aPagosDeEntregaDoc));
            $saldoComp = $oPagosDeEntregaDoc->oCargaDoc->totalFinal - $oPagosDeEntregaDoc->oCargaDoc->devolucion;
            Yii::log('$saldoComp:'.$saldoComp.' totalFinal:'.$oPagosDeEntregaDoc->oCargaDoc->totalFinal.' devolucion:'.$oPagosDeEntregaDoc->oCargaDoc->devolucion,'warning');
            Yii::log('$i '.$i.' $ch:'.$ch.' ft:'.$ft.' NC:'.$totalNC,'warning');
            $oPagosDeEntregaDoc->oCargaDoc->efectivo = 0;
            $oPagosDeEntregaDoc->oCargaDoc->cheque = 0;
            $oPagosDeEntregaDoc->oCargaDoc->Recibida = 1;

            if(true){
            //if($saldoComp != 0){
                if($totalNC > $saldoComp){ // SI EL MONTO DE LAS NC SUPERAN AL monto MENOS devolucion
                    $oPagosDeEntregaDoc->oCargaDoc->notaCredito = $saldoComp;
    
                    // SI SE COMPLETA CON NOTA DE CREDITO, SE ELIMINA TODO PAGO ASOCIADO
                    //TODO: PONER TRY CATCH
                    //$oPago = $oPagosDeEntregaDoc->oCargaDoc->OPagoCliente->delete();
                    $oPago = $oPagosDeEntregaDoc->oCargaDoc->OPagoCliente;
                    if($oPago != null && $oPago->id != $idPagoRelacionado){
                        if(!$oPago->delete()){
                            $transaction->rollback();
                            throw new Exception(CHtml::errorSummary($oPago));
                        }
                    }
                    $totalNC -= $saldoComp;
                    // TODO: VER SI SE ESTA COMPUTANDO
                    $saldoComp = 0;
    
                }else{
                    $saldoComp -= $totalNC;
                    // ASEGURARSE QUE GUARDE $oPagosDeEntregaDoc->oCargaDoc->save()
                    $oPagosDeEntregaDoc->oCargaDoc->notaCredito = $totalNC;
    
                    //TODO: REVISAR LA SIGUIENTE
                    $totalNC = 0;
                    $oPagoCliente = $oPagosDeEntregaDoc->oCargaDoc->OPagoCliente;
                    if ($oPagoCliente != null){
                        if($oPagoCliente->id != $idPagoRelacionado){
                            // SI EL PAGO EXISTE, y no es el relacionado, BORRO TODOS LOS PAGOS CON CHEQUE
                            foreach ($oPagoCliente->aPagoConCheque as $index => $oPagoConCheque) {
                                if(!$oPagoConCheque->delete()){
                                    $transaction->rollback();
                                    throw new Exception(CHtml::errorSummary($oPagoConCheque));
                                }
                            }
                        }
                        foreach ($oPagoCliente->aPagoAlContado as $index => $oPagoAlContado) {
                            if(!$oPagoAlContado->delete()){
                                $transaction->rollback();
                                throw new Exception(CHtml::errorSummary($oPagoAlContado));
                            }
                        }
    
                        if ($oPagoCliente->id_factura == null && $oPagoCliente->idNotaEntrega == null){
                            $oPagoCliente->id_factura = $oPagosDeEntregaDoc->idFactura;
                            $oPagoCliente->idNotaEntrega = $oPagosDeEntregaDoc->idNotaEntrega;
                            if (!$oPagoCliente->save()){
                                //TODO: VER QUE HACEMOS EN ESTE CASO
                                Yii::log('$oPagoCliente:'.CHtml::errorSummary($oPagoCliente),'error');
                            }
                        }
                        Yii::log('$i '.' $saldoComp:'.$saldoComp,'warning');
                        Yii::log('$i '.$i.ComponentesComunes::print_array($oPagosDeEntregaDoc->oCargaDoc),'warning');
    
                    }else{
                        //CREAR PAGO
                        $oPagoCliente  = PagoCliente::newBlank($oPagosDeEntregaDoc->oCargaDoc->getCliente()->id);
                        $oPagoCliente->id_factura = $oPagosDeEntregaDoc->idFactura;
                        $oPagoCliente->idNotaEntrega = $oPagosDeEntregaDoc->idNotaEntrega;
                        if(!$oPagoCliente->save()){
                            $transaction->rollback();
                            throw new Exception(CHtml::errorSummary($oPagoCliente));
                        }
                    }
    
                    // SOLO SI QUEDA EFECTIVO
                    if ($ft > 0){
                        $oPagoContado = new PagoAlContado();
                        $oPagoContado->idPagodeCliente = $oPagoCliente->id;
                        if ($ft < $saldoComp){ Yii::log('NO ALCANZA CON EL EFECTIVO QUE QUEDA','warning');
                            $oPagosDeEntregaDoc->oCargaDoc->efectivo = $ft;
                            $oPagoContado->monto = $ft;
                            $saldoComp -= $ft;
                            $ft = 0;
                        }else{
                            Yii::log('/**/ SOBRA EFECTIVO DEL TOTAL','warning');
                            Yii::log('*********$i '.$i.' $ch:'.$ch.' ft:'.$ft.' NC:'.$totalNC.' $saldoComp:'.$saldoComp,'warning');
                            $oPagoCliente->monto = (!$esElUltimo)? $saldoComp : $ft;
                            if($oPagoCliente->monto > 0 && !$oPagoCliente->save()){                            
                                Yii::log(CHtml::errorSummary($oPagoCliente),'error');
                                $transaction->rollback();
                                throw new Exception(CHtml::errorSummary($oPagoCliente));
                            }
                            $oPagosDeEntregaDoc->oCargaDoc->efectivo = $oPagoCliente->monto;
                            //$oPagoContado->monto = $saldoComp;
                            if($esElUltimo){
                                $ft = 0;
                            }else{
                                $ft -= $saldoComp;
                            }
                            
                            $saldoComp = 0;
                            $bPagoRelacionadoSoloEfectivo = ($oPagoCliente->id == $idPagoRelacionado);
                            Yii::log('$i '.$i.' $ch:'.$ch.' ft:'.$ft.' NC:'.$totalNC.' $saldoComp:'.$saldoComp,'warning');
                            $oPagoContado->monto = $oPagoCliente->monto;
                        }
                        
                        if(!$oPagoContado->save()){
                            $transaction->rollback();
                            throw new Exception(CHtml::errorSummary($oPagoContado));
                        }
                    }
    
                    Yii::log('// SI QUEDA POR SALDAR Y HAY SALDO EN CHEQUES','warning');
                    if($saldoComp > 0 && $ch > 0){
                        Yii::log('$i '.$i.' $ch:'.$ch.' ft:'.$ft.' NC:'.$totalNC.' $saldoComp:'.$saldoComp,'warning');
                        $iCH = 0;
                        //Yii::log('$saldoComp:'.$saldoComp.' :: '.ComponentesComunes::print_array($oPagoCliente->attributes,true),'warning');
                        if ($saldoComp <= $ch && !$esElUltimo){
                            $oPagosDeEntregaDoc->oCargaDoc->cheque = $saldoComp;
                        }else{
                            $oPagosDeEntregaDoc->oCargaDoc->cheque = $ch;
                        }
                        
                        foreach ($oPagoRelacionado->aPagoConCheque as $index => $oPagoRelConCheque) {
                            
                            if ($oPagoCliente->id != $idPagoRelacionado){ // SI NO ES EL COMPROBANTE DEL PAGO RELACIONADO HAY QUE CREAR EL PAGO CON CHEQUE
                                $oPagoConCheque = new PagoConCheque();
                                $oPagoConCheque->idChequedeTercero = $oPagoRelConCheque->idChequedeTercero;
                                $oPagoConCheque->idPagodeCliente = $oPagoCliente->id;
                                if($iCH == 0){
                                    $oPagoConCheque->monto = ($esElUltimo)? $ch : $saldoComp;
                                }else{
                                    $oPagoConCheque->monto = 0;
                                }
                                if(!$oPagoConCheque->save()){
                                    $transaction->rollback();
                                    throw new Exception(CHtml::errorSummary($oPagoConCheque));
                                }
                            }else{
                                if($iCH == 0){
                                    $oPagoRelConCheque->monto = ($esElUltimo)? $ch : $saldoComp;
                                }else{
                                    $oPagoRelConCheque->monto = 0;
                                }
    
                                if(!$oPagoRelConCheque->save()){
                                    $transaction->rollback();
                                    throw new Exception(CHtml::errorSummary($oPagoRelConCheque));
                                }
    
                            }
                            $ch -= $saldoComp;
                            $saldoComp = 0;
                            $iCH++;
                        }
                    }
                }
    
                // SI ES EL PAGO RELACIONADO, Y EL EFECTIVO CUBRE TODO, SE PONE EL PAGO CON CHEQUE EN CERO
                if(($oPagoCliente->id == $idPagoRelacionado) && $bPagoRelacionadoSoloEfectivo){
                    foreach ($oPagoCliente->aPagoConCheque as $index => $oPagoConCheque) {
                        $oPagoConCheque->monto = 0;
                        if(!$oPagoConCheque->save()){
                            $transaction->rollback();
                            throw new Exception(CHtml::errorSummary($oPagoConCheque));
                        }
                    }
                }
    
                Yii::log('$i '.$i.'  '.$oPagosDeEntregaDoc->oCargaDoc->cheque,'warning');
    
    
                $oPagosDeEntregaDoc->oCargaDoc->recalcular();
    
                Yii::log('XX $i '.$i.'  '.$oPagosDeEntregaDoc->oCargaDoc->cheque,'warning');
    
                $oPagoCliente->recalcular();
    
                if(!$oPagosDeEntregaDoc->oCargaDoc->save()){
                    $transaction->rollback();
                   throw new Exception(CHtml::errorSummary($oPagosDeEntregaDoc->oCargaDoc));
                }
            }
            
            $i++;
        }

        $transaction->commit();
    }



	public function actionSumPagoRelacionado($idPagoRelacionado){
	    //TODO: hay que controlar posibles excepciones
	    $sum = PagoCliente::sumPagoRelacionado($idPagoRelacionado);
        echo $sum;
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.

     */

    public function actionNewBlank($idCliente){
        try{
            $oPagoCliente = PagoCliente::newBlank($idCliente);
            echo $oPagoCliente->id;
        }catch (Exception $exception) {
            throw new CHttpException(500,$exception->getMessage());
        }
    }

    /**
     *
     * @var $_GET['$idPagoRel'] SI SE REQUIERE EDITAR UN MULTIPAGO EN PARTICULAR
     * @var $_GET['CargaDocumentos'] SI SE REQUIERE ARMAR UNO NUEVO
     *
     * @return  * DEVUELVE Y ARMA EL VIEW DEL FORMULARIO Y GRILLA DEL MULTIPAGO
     *
     * */
    public function actionMultipago(){
        /* @var $oCargaDocumento CargaDocumentos */
        $aIdCargaDocumentos = array();

        $total = 0;
        $fTotalCheques = 0;
        $fTotalEfectivo = 0;
        $fTotalNc = 0;
        $idPagoRel = 0;
        $aNcSinPR = array();
        $i = 0;

        $bEditable = true;

        if (isset($_GET['idPagoRel'])){        // SE LLAMA AL
            $idPagoRel = $_GET['idPagoRel'];
            $oMultipago = Multipago::model()->findByPk($idPagoRel);

            //ComponentesComunes::print_array($oMultipago->attributes);

            $aPagosDeEntregaDoc = VCargaDocDePago::model()->findAll('idPago='.$idPagoRel);

            foreach ($aPagosDeEntregaDoc as $index => $oPagosDeEntregaDoc) {
                //Yii::log('Encontre $oPagosDeEntregaDoc idCargaDoc: '.$oPagosDeEntregaDoc->idCargaDoc.' monto:'.$oPagosDeEntregaDoc->monto,'warning');
                array_push($aIdCargaDocumentos,$oPagosDeEntregaDoc->idCargaDoc);
                $oCargaDocumento = $oPagosDeEntregaDoc->oCargaDoc;
                // SE CALCULAN LOS TOTALES PARA PASAR A LA VISTA
                if ($oCargaDocumento->tipoDoc != CargaDocumentos::NC){
                    $total += $oCargaDocumento->totalFinal;
                    $fTotalCheques += $oCargaDocumento->cheque;
                    $fTotalEfectivo += $oCargaDocumento->efectivo;
                    //$TotalEfectivo
                }else{
                    $total = $total + $oCargaDocumento->totalFinal;
                    $fTotalNc += $oCargaDocumento->totalFinal;
                }
            }

        }else{                                  // VA A ARMAR EL MULTIPAGO
            if (isset($_GET['CargaDocumentos']) && sizeof($_GET['CargaDocumentos']) > 0){
                $aIdCargaDocumentos = $_GET['CargaDocumentos'];
                $criteria = new CDbCriteria();
                $criteria->addInCondition('id',$aIdCargaDocumentos);
                $aCargaDocumentos = CargaDocumentos::model()->findAll($criteria);


                foreach ($aCargaDocumentos as $index => $oCargaDocumento) {
                    $oDocumento = $oCargaDocumento->getDocumento();

                    if ($idPagoRel != 0) { // YA HAY UN PAGO RELACIONADO
                        Yii::log('$i:::'.$idPagoRel,'warning');
                        if ($oDocumento->idPagoRelacionado == null){
                            $oDocumento->idPagoRelacionado = $idPagoRel;
                            if (!$oDocumento->save()){
                                Yii::log('$oDocumento->idPagoRelacionado ::'.CHtml::errorSummary($oDocumento),'warning');
                                throw new CHttpException(CHtml::errorSummary($oDocumento));
                            }else{
                                //TODO: VER SI HACEMOS ALGO ACA
                            }
                        }
                    }else{
                        Yii::log('SE VA A CREAR UN PAGO RELACIONADO','warning');
                        if ($oCargaDocumento->tipoDoc == CargaDocumentos::NC) { // (3)
                            Yii::log('$oCargaDocumento->tipoDoc == CargaDocumentos::NC','warning');
                            if ($oDocumento->idPagoRelacionado != null) { // (5)
                                $idPagoRel = $oDocumento->idPagoRelacionado;
                            }else{ // (6)
                                $aNcSinPR[] = $oCargaDocumento;
                            }

                        }else{ //(4)
                            Yii::log('*** $oCargaDocumento->tipoDoc == '.$oCargaDocumento->tipoDoc,'warning');
                            if ($oDocumento->idPagoRelacionado != null) { // (7)
                                if ($idPagoRel == 0){
                                    $idPagoRel = $oDocumento->idPagoRelacionado;
                                }else{
                                    // TODO: VER QUE HACER, PORQUE NO DEBERIA PASAR
                                    throw new CHttpException(500,'VER QUE HACER, PORQUE NO DEBERIA PASAR');
                                }
                            }else{ // (8)
                                $oPago = $oCargaDocumento->getOPagoCliente();
                                if ($oPago == null){ // SI NO TIENE PAGO ASIGNADO
                                    $oPago = PagoCliente::newBlank($oCargaDocumento->getCliente()->id);
                                    if ($oCargaDocumento->tipoDoc == CargaDocumentos::NE) {
                                        $oPago->idNotaEntrega = $oDocumento->id;
                                    }else{
                                        if ($oCargaDocumento->tipoDoc == CargaDocumentos::FAC) {
                                            $oPago->id_factura = $oDocumento->id;
                                        }else if ($oCargaDocumento->tipoDoc == CargaDocumentos::ND) {
                                            $oPago->id_factura = $oDocumento->id;
                                        }

                                    }
                                    if (!$oPago->save()){
                                        throw new CHttpException(CHtml::errorSummary($oPago));
                                    }
                                }
                                if ($idPagoRel == 0){
                                    $idPagoRel = $oPago->id;
                                    $oDocumento->idPagoRelacionado = $oPago->id;
                                }else{
                                    $oDocumento->idPagoRelacionado = $idPagoRel;
                                }
                                if (!$oDocumento->save()){
                                    throw new CHttpException(CHtml::errorSummary($oDocumento));
                                }
                            }
                        }
                    }

                    // SE CALCULAN LOS TOTALES PARA PASAR A LA VISTA
                    if ($oCargaDocumento->tipoDoc != CargaDocumentos::NC){
                        $total += $oCargaDocumento->totalFinal;
                        $fTotalCheques += $oCargaDocumento->cheque;
                        $fTotalEfectivo += $oCargaDocumento->efectivo;
                        //$TotalEfectivo
                    }else{
                        $total = $total + $oCargaDocumento->totalFinal;
                        $fTotalNc += $oCargaDocumento->totalFinal;
                    }
                    $i++;
                }
            }else{
                throw new CHttpException('500','No hay Documentos seleccionados');
            }
        }


        $aCargaDocumentos = new CargaDocumentos('search');
        $aCargaDocumentos->unsetAttributes();
        $oPago = PagoCliente::model()->findByPk($idPagoRel);

        if($oPago == null){
            throw new CHttpException(500,'Error al generar el multipago::'.$idPagoRel);
        }



        $oPagoAlContado = new PagoAlContado();
        $oPagoAlContado->idPagodeCliente = $idPagoRel;
        $oPagoConCheque = new PagoConCheque();
        $oPagoConCheque->idPagodeCliente = $idPagoRel;
        $oChequeTerceros = new ChequeDeTerceros();
        $oChequeTerceros->id_pagoCliente = $idPagoRel;

        $aPagoAlContado = new VPagosContadoRelacionado('search');
        $aPagoAlContado->unsetAttributes();
        $aPagoAlContado->idPagoRelacionado = $idPagoRel;

        $aPagoConCheque = new PagoConCheque('search');
        $aPagoConCheque->unsetAttributes();
        $aPagoConCheque->idPagodeCliente = $idPagoRel;

        $oChequeTerceros = new ChequeDeTerceros();
        $oChequeTerceros->id_pagoCliente = $idPagoRel;

        $totalDev = 0;
        $aCargaDocumentosDevol = CargaDocumentos::model()->findAll($aCargaDocumentos->searchIn($aIdCargaDocumentos)->criteria);
        foreach ($aCargaDocumentosDevol as $index => $aCargaDocumento) {
            $totalDev += $aCargaDocumento->devolucion;
        }

        if(isset($aCargaDocumentosDevol[0])){
            $oCargaDoc = $aCargaDocumentosDevol[0];
            if ($oCargaDoc != null && $oCargaDoc->oEntregaDoc != null && $oCargaDoc->oEntregaDoc->oCarga != null && $oCargaDoc->oEntregaDoc->oCarga->estado >= Carga::iCerrada){
                $bEditable = false;
            }
        }

        // LAS NC SE ESTABAN RESTANDO DOS VECES
        //$saldo = $total - PagoCliente::sumPagoRelacionado($idPagoRel) + $fTotalNc;
        //$saldo = number_format(($total - PagoCliente::sumPagoRelacionado($idPagoRel) - $totalDev),2,',','.');
        $saldo = number_format(($total - $fTotalCheques - $fTotalEfectivo - $totalDev + $fTotalNc),2,',','.');

        $this->renderPartial('multipago',array(
            'aIdCargaDocumentos' => $aIdCargaDocumentos,
            'aCargaDocumentos' => $aCargaDocumentos,
            'total' => $total,
            'saldo' => $saldo,
            'model' => $oPago,
            'aPagoAlContado' => $aPagoAlContado,
            'oPagoAlContado' => $oPagoAlContado,
            'aPagoConCheque' => $aPagoConCheque,
            'oPagoConCheque' => $oPagoConCheque,
            'oChequeTerceros' => $oChequeTerceros,
            'fTotalCheques' => $fTotalCheques,
            'fTotalEfectivo' => $fTotalEfectivo,
            'fTotalNc' => $fTotalNc,
            'bEditable' => $bEditable,
            'isAjaxMode' => true,

        ),false,true);


    }

    public function actionForm($idPago=0,$idDoc=0,$tipo=null,$idCargaDoc=null,$isModal=true){

	    if($isModal== 'false'){
            $isModal = false;
        }

	    $this->layout='//layouts/column2';

        $oCargaDoc = null;
        $bEditable = true;

        if($idPago > 0){
            $model =$this->loadModel($idPago);
            $oPagoAlContado = new PagoAlContado();
            $oPagoAlContado->idPagodeCliente = $model->id;
            $oPagoConCheque = new PagoConCheque();
            $oPagoConCheque->idPagodeCliente = $model->id;
            $oChequeTerceros = new ChequeDeTerceros();
            $oChequeTerceros->id_pagoCliente = $idPago;

            if ($model->id_factura > 0){
                $oCargaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante' => $model->id_factura,'tipoDoc' => CargaDocumentos::FAC));
                if(!$oCargaDoc){
                    $oCargaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante' => $model->id_factura,'tipoDoc' => CargaDocumentos::ND));
                }
            }elseif ($model->idNotaEntrega > 0){
                $oCargaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante' => $model->idNotaEntrega,'tipoDoc' => CargaDocumentos::NE));
            }else{

            }

        }else{


            $js = 'jquery-editable-poshytip.js';
            $css = 'jquery-editable.css';
            $am = Yii::app()->getAssetManager();
            $cs = Yii::app()->getClientScript();
            $poshytipUrl = $am->publish(Yii::getPathOfAlias('editable.assets.poshytip'));
            $cs->registerScriptFile($poshytipUrl . '/jquery.poshytip.js');
            $cs->registerCssFile($poshytipUrl . '/tip-yellowsimple/tip-yellowsimple.css');


            if($idCargaDoc != null){
                $oCargaDoc = CargaDocumentos::model()->findByPk($idCargaDoc);
                if ($oCargaDoc != null){
                    $tipo = $oCargaDoc->tipoDoc;
                    $idDoc = $oCargaDoc->idComprobante;
                }
            }

            if($tipo == CargaDocumentos::NE){
                $oDoc = NotaEntrega::model()->with('oPagoCliente')->findByPk($idDoc);
            }else{
                $oDoc = Factura::model()->with('oPagoCliente')->findByPk($idDoc);
            }
            if ($oDoc != null){
                if($oDoc->oPagoCliente != null){
                    $model = $oDoc->oPagoCliente;
                }else{
                    $model = new PagoCliente();
                    $model->monto = 0;
                    if($tipo == CargaDocumentos::NE){
                        $model->idNotaEntrega = $idDoc;
                    }else{
                        $model->id_factura = $idDoc;
                    }
                    $model->idCliente = $oDoc->oCliente->id;
                    $model->fecha = date('Y-m-d');
                    if (!$model->save()){
                        Yii::log('Error al guardar modelo en Form de PagoCliente'.CHtml::errorSummary($model),'error');
                    }
                }
            }

            $oPagoAlContado = new PagoAlContado();
            $oPagoAlContado->idPagodeCliente = $model->id;
            $oPagoConCheque = new PagoConCheque();
            $oPagoConCheque->idPagodeCliente = $model->id;
            $oChequeTerceros = new ChequeDeTerceros();
            $oChequeTerceros->id_pagoCliente = $model->id;
        }
        $aPagoAlContado = new PagoAlContado('search');
        $aPagoAlContado->unsetAttributes();
        $aPagoAlContado->idPagodeCliente = $model->id;
        $aPagoConCheque = new PagoConCheque('search');
        $aPagoConCheque->unsetAttributes();
        $aPagoConCheque->idPagodeCliente = $model->id;

        if ($oCargaDoc != null && $oCargaDoc->oEntregaDoc != null && $oCargaDoc->oEntregaDoc->oCarga != null && $oCargaDoc->oEntregaDoc->oCarga->estado >= Carga::iCerrada){
            $bEditable = false;
        }

        $this->renderPartial('viewForm',array(
            'model' => $model,
            'aPagoAlContado' => $aPagoAlContado,
            'oPagoAlContado' => $oPagoAlContado,
            'aPagoConCheque' => $aPagoConCheque,
            'oPagoConCheque' => $oPagoConCheque,
            'oChequeTerceros' => $oChequeTerceros,
            'devolucion' => ($oCargaDoc != null)? $oCargaDoc->devolucion : 0,
            'bEditable' => $bEditable,
            'isAjaxMode' => $isModal,

        ),false,true);

    }

    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver PagoCliente #'.$id;
		$aPagosEfectivo = new PagoAlContado('search');
		$aPagosCheque = new PagoConCheque('search');
		$aPagosCheque->unsetAttributes();
        $aPagosEfectivo->unsetAttributes();
        $aPagosCheque->idPagodeCliente = $id;
        $aPagosEfectivo->idPagodeCliente = $id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'aPagoConCheque' => $aPagosCheque,
            'aPagoAlContado' => $aPagosEfectivo,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PagoCliente;
		$this->pageTitle = "Nuevo PagoCliente";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoCliente']))
		{
			$model->attributes=$_POST['PagoCliente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new PagoCliente;
		$this->pageTitle = "Nuevo PagoCliente";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoCliente']))
		{
			$model->attributes=$_POST['PagoCliente'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);
        $this->pageTitle = 'Modificando Pago Cliente #'.$id;

        $this->layout = '//layouts/column1';

        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['PagoCliente']))
        {
            $model->attributes=$_POST['PagoCliente'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	    $oPago = PagoCliente::model()->findByPk($id);
        Yii::log('actionDelete: '.$id,'warning');
        if ($oPago != null){
            $this->loadModel($id)->delete();
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PagoCliente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoCliente']))
			$model->attributes=$_GET['PagoCliente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PagoCliente('search');
		$this->pageTitle = 'Administrando Pago Clientes ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoCliente'])){
			$model->attributes=$_GET['PagoCliente'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PagoCliente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PagoCliente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PagoCliente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pago-cliente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
