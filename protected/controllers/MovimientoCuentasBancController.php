<?php

class MovimientoCuentasBancController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/';
    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="27%" align="center">{PAGENO}/{nbpg}</td>
						<td width="30%" style="text-align: right;"></td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Json','select2','listaPdf','recalc'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_banco")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionRecalc(){
	    /** @var $oMovimiento MovimientoCuentasBanc */


	    $criteria = new CDbCriteria;
            $criteria->addCondition('idCuenta = 6 ');
            $criteria->order = 't.fechaMovimiento asc,t.id asc';
            $aMovimientos = MovimientoCuentasBanc::model()->findAll($criteria);
            foreach ($aMovimientos as $index => $oMovimiento) {
/*
            if($oMovimiento->oTipoMovimiento->Ingreso){
                $oMovimiento->saldo_acumulado = $oMovimiento->oCuenta->saldo + $oMovimiento->Importe;
            }else{
                $oMovimiento->saldo_acumulado = $oMovimiento->oCuenta->saldo - $oMovimiento->Importe;
            }
            echo '<br> * $oMovimiento->saldo_acumulado:'.$oMovimiento->saldo_acumulado;
*/
            $oMovimiento->Observacion .= '';
            if(!$oMovimiento->save()){
                Yii::log('$this->oCuenta::'.CHtml::errorSummary($oMovimiento),'error');

                throw new CHttpException('500',CHtml::errorSummary($oMovimiento));
            }else{
                //ComponentesComunes::print_array($oMovimiento->attributes);
            }

            /*
            $oMovimiento->oCuenta->saldo = $oMovimiento->saldo_acumulado;
            $oMovimiento->oCuenta->fechaSaldo = $oMovimiento->fechaMovimiento;
            if(!$oMovimiento->oCuenta->save()){
                Yii::log('$this->oCuenta::'.CHtml::errorSummary($oMovimiento->oCuenta),'error');

                throw new CHttpException('500',CHtml::errorSummary($oMovimiento->oCuenta));
            }else{
                echo '$oMovimiento->oCuenta->saldo:'.$oMovimiento->oCuenta->saldo;
            }
            */
	    }
    }

    /**
     * Manages all models.
     */
    public function actionListaPdf($print=false)
    {
        /*
        ComponentesComunes::print_array($_POST);
        ComponentesComunes::print_array($_GET);
        die();
        */
        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ($print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 15,15,25,25,8,0,'P');
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,15,25,5,5,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 15,15,25,25,8,0,'P');
        }
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $model=new MovimientoCuentasBanc('search');
        $this->layout = 'column1Pdf';
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['MovimientoCuentasBanc'])){
            $model->attributes=$_GET['MovimientoCuentasBanc'];
        }else{

        }

        $mPDF1->WriteHTML($this->renderPartial('listPdf',array(
            'model'=>$model,
        ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;


        $filename = 'MovimientoCuentasBanc_'.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); // OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

	public function actionJson($id){
	    $model = $this->loadModel($id);
        echo json_encode($model->attributes);
    }

    public function actionSelect2(){
        /** POR DEFECTO SOLO TRAE LOS QUE ESTAN EN CARTERA
         * @var $oModel MovimientoCuentasBanc
         * @var $aModel MovimientoCuentasBanc[]
         * @var $models MovimientoCuentasBanc
         */

        /**
         * @property integer $id
         * @property integer $idCuenta
         * @property double $Importe
         * @property integer $tipoMovimiento
         * @property integer $codigoMovimiento
         * @property integer $idMedio
         * @property string $fechaMovimiento
         * @property string $Nro_operacion
         * @property string $Observacion
         *
         */

        $criteria = new CDbCriteria();


        if (!isset($_GET['q'])){
            if (isset($_GET['codigoMovimiento'])){
                $criteria->compare('codigoMovimiento', $_GET['codigoMovimiento']);
            }

            if (isset($_GET['Nro_operacion'])){
                $criteria->compare('Nro_operacion', $_GET['Nro_operacion']);
            }

            if (isset($_GET['Importe'])){
                $criteria->compare('Importe', $_GET['Importe'],true);
            }
        }else{
            $criteria->addCondition('t.codigoMovimiento like "%'.$_GET['q'].'%" OR t.Nro_operacion like "%'. $_GET['q'].'%"  OR  t.Importe like "%'. $_GET['q'].'%"');
        }


        if(isset($_GET['tipoMovimiento'])){
            $criteria->compare('tipoMovimiento',$_GET['tipoMovimiento']);
        }

        if(isset($_GET['page_limit'])){
            $criteria->limit = $_GET['page_limit'];
        }

        if (isset($_GET['sort'])){
            $criteria->order = $_GET['sort'];
        }else{
            $criteria->order = 'fechaMovimiento desc';
        }

        $models = MovimientoCuentasBanc::model()->activas()->findAll($criteria);

        $aModels = array();

        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id, 'text' => '('.(($oModel->oCuenta != null)? $oModel->oCuenta->numero : $oModel->idCuenta ).' : '.$oModel->Nro_operacion.' : '.ComponentesComunes::fechaFormateada($oModel->fechaMovimiento).') : $'.number_format($oModel->Importe,2,'.',''),'importe' => number_format($oModel->Importe,2,'.','') );
        }

        echo CJSON::encode($aModels);
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.MovimientoCuentasBanc::model()->getAttributeLabel('model').' #'.$id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MovimientoCuentasBanc;
		$this->pageTitle = "Nuevo ".MovimientoCuentasBanc::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

        if(isset($_POST['MovimientoCuentasBanc'])){

            $transaction = Yii::app()->db->getCurrentTransaction();
            $shouldCommit = true;
            if($transaction == null || !$transaction->Active){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
                $transaction=Yii::app()->db->beginTransaction();
            }else{
                $shouldCommit = false;
            }

            $model->attributes=$_POST['MovimientoCuentasBanc'];
            $model->fechaMovimiento = ComponentesComunes::jpicker2db($model->fechaMovimiento);
            if($model->save()){
                if (isset($_POST['MovimientoCuentasBanc']['idsCheques3Ro'])){
                    try{
                        $this->saveCheques3Ro($_POST['MovimientoCuentasBanc']['idsCheques3Ro'],$model->id);
                    }catch (Exception $exception){
                        $transaction->rollback();
                    }
                }

                if (isset($_POST['MovimientoCuentasBanc']['idsChequesPpios'])){
                    try{
                        $this->saveChequesPpio($_POST['MovimientoCuentasBanc']['idsChequesPpios'],$model->id,$model->fechaMovimiento);
                    }catch (Exception $exception){
                        $transaction->rollback();
                    }
                }

                $transaction->commit();
                $this->redirect(array('view','id'=>$model->id));
            }else{
                Yii::log(''.CHtml::errorSummary($model),'error');
                $transaction->rollback();
            }
        }



        $this->render('create',array(
			'model'=>$model,
		));
	}

	public function saveCheques3Ro($idsCheques3Ro,$model_id){
        if(trim($idsCheques3Ro) != ''){
            $aCheques3Ro = explode(',',$idsCheques3Ro);
            foreach ($aCheques3Ro as $index => $idCheque) {
                $oDetalle = new Cheque3RoMovBancario();
                $oDetalle->idCheque3ro = $idCheque;
                $oDetalle->idMovimiento = $model_id;
                if (!$oDetalle->save()){
                    Yii::log('Cheque3RoMovBancario:: '.CHtml::errorSummary($oDetalle),'error');

                    throw new Exception(CHtml::errorSummary($oDetalle));
                }else{
                    $oDetalle->oCheque3ro->estado = ChequeDeTerceros::iDepositado;
                    if (!$oDetalle->oCheque3ro->save()){
                        Yii::log('$oDetalle->oCheque3ro :: '.CHtml::errorSummary($oDetalle->oCheque3ro),'error');

                        throw new Exception(CHtml::errorSummary($oDetalle->oCheque3ro));
                    }
                }
            }
        }
    }

    public function saveChequesPpio($idsChequesPpio,$model_id,$fechaMov){
	    if (trim($idsChequesPpio) != ''){
            $aChequesPpio = explode(',',$idsChequesPpio);
            foreach ($aChequesPpio as $index => $idCheque) {
                $oDetalle = new ChequePropioMovBancario();
                $oDetalle->idChequePropio = $idCheque;
                $oDetalle->idMovimiento = $model_id;
                if (!$oDetalle->save()){
                    Yii::log('ChequePropioMovBancario:: '.CHtml::errorSummary($oDetalle),'error');

                    throw new CHttpException('500',CHtml::errorSummary($oDetalle));
                }else{
                    $oDetalle->oChequePropio->fechaDebito = $fechaMov;
                    if (!$oDetalle->oChequePropio->save()){
                        Yii::log('$oDetalle->oChequePropio :: '.CHtml::errorSummary($oDetalle->oChequePropio),'error');

                        throw new Exception(CHtml::errorSummary($oDetalle->oChequePropio));
                    }
                }
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new MovimientoCuentasBanc;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MovimientoCuentasBanc']))
		{
			$model->attributes=$_POST['MovimientoCuentasBanc'];
            $model->fechaMovimiento = ComponentesComunes::jpicker2db($model->fechaMovimiento);
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.MovimientoCuentasBanc::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MovimientoCuentasBanc']))
		{
            $transaction=Yii::app()->db->beginTransaction();
			$model->attributes=$_POST['MovimientoCuentasBanc'];
            $model->fechaMovimiento = ComponentesComunes::jpicker2db($model->fechaMovimiento);

            if($model->save()){
                if (!empty($_POST['MovimientoCuentasBanc']['idsCheques3Ro']) ){
                    $aCheques3Ro = explode(',',$_POST['MovimientoCuentasBanc']['idsCheques3Ro']);
                    foreach ($aCheques3Ro as $index => $idCheque) {
                        $oDetalle = new Cheque3RoMovBancario();
                        $oDetalle->idCheque3ro = $idCheque;
                        $oDetalle->idMovimiento = $model->id;
                        if (!$oDetalle->save()){
                            Yii::log('Cheque3RoMovBancario:: '.CHtml::errorSummary($oDetalle),'error');
                            $transaction->rollback();
                            throw new CHttpException('500',CHtml::errorSummary($oDetalle));
                        }else{
                            $oDetalle->oCheque3ro->estado = ChequeDeTerceros::iDepositado;
                            if (!$oDetalle->oCheque3ro->save()){
                                Yii::log('$oDetalle->oCheque3ro :: '.CHtml::errorSummary($oDetalle->oCheque3ro),'error');
                                $transaction->rollback();
                                throw new CHttpException('500',CHtml::errorSummary($oDetalle->oCheque3ro));
                            }
                        }
                    }
                }
                $transaction->commit();
                $this->redirect(array('view','id'=>$model->id));
            }else{
                $transaction->rollback();
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new MovimientoCuentasBanc('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MovimientoCuentasBanc']))
			$model->attributes=$_GET['MovimientoCuentasBanc'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new MovimientoCuentasBanc('search');
		$this->pageTitle = 'Administrando '.MovimientoCuentasBanc::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values


		if(isset($_GET['MovimientoCuentasBanc'])){
			$model->attributes=$_GET['MovimientoCuentasBanc'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MovimientoCuentasBanc the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MovimientoCuentasBanc::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MovimientoCuentasBanc $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='movimiento-cuentas-banc-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
