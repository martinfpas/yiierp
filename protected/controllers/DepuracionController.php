<?php

class DepuracionController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print';

    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="27%" align="center">{PAGENO}/{nbpg}</td>
						<td width="30%" style="text-align: right;"></td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','viewPdf','CodigosCot'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Depuracion #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Depuracion;
		$this->pageTitle = "Nuevo Depuracion";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Depuracion']))
		{
			$model->attributes=$_POST['Depuracion'];
			if($model->isNewRecord){
                $model->fecha = date('Y-m-d');
            }
			if($model->save()){
                $this->redirect(array('view','id'=>$model->id));
            }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Depuracion;
		$this->pageTitle = "Nuevo Depuracion";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Depuracion']))
		{
			$model->attributes=$_POST['Depuracion'];
            if($model->isNewRecord){
                $model->fecha = date('Y-m-d');
            }
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    public function actionCodigosCot($idDepuracion){
        $vArtCot = new VArtCotbyDep('search');
        $vArtCot->unsetAttributes();
        $vArtCot->idDepuracion = $idDepuracion;

        $this->render('codigosCot',array(
            'vArtCot' => $vArtCot,
        ));

    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewPdf($id,$print=false)
    {
        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{

                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,0,25,25,8,0,'P');
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','14','8pin', 10,0,5,5,5,0,'P');
            }
        }else{
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,0,5,5,5,0,'P');
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
        }

        $model = $this->loadModel($id);
        $model->oCarga->observacion .= '';

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $aProds = new ProdDepuracion('search');
        $aProds->unsetAttributes();
        $aProds->idDepuracion = $id;
        $this->layout = 'column1Pdf';

        //$mPDF1->SetHTMLHeader($this->pdfHeader,null,true);


        $vArtCot = new VArtCotbyDep('search');
        $vArtCot->unsetAttributes();
        $vArtCot->idDepuracion = $id;

        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProds' => $aProds,
            ),true)
        );

        $mPDF1->AddPage('','', '','12','couriernew',15,0,25,25,8,0,'L');

        $mPDF1->WriteHTML($this->renderPartial('codigosCot',array(
            'vArtCot' => $vArtCot,
            ),true)
        );


        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'Depuracion_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); // OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Depuracion #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Depuracion']))
		{
			$model->attributes=$_POST['Depuracion'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Depuracion('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Depuracion']))
			$model->attributes=$_GET['Depuracion'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Depuracion('search');
		$this->pageTitle = 'Administrando Depuracions ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Depuracion'])){
			$model->attributes=$_GET['Depuracion'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Depuracion the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Depuracion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Depuracion $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='depuracion-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
