<?php

class OrdenDePagoController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','CrearPago'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionCrearPago($id){
        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }
	    $modelOP = $this->loadModel($id);

        $oPagoProveedor = new PagoProveedor();
        $oPagoProveedor->idProveedor = $modelOP->idProveedor;
        $oPagoProveedor->fecha = date('Y-m-d');

        if (!$oPagoProveedor->save()){
            $transaction->rollback();
            throw new CHttpException(500,CHtml::errorSummary($oPagoProveedor));
        }

        foreach ($modelOP->aFacturaOrdenPago as $index => $oFacturaOrdenPago) {
            $oRelFacturaPago = new Pago2FactProv();
            $oRelFacturaPago->idComprobanteProveedor = $oFacturaOrdenPago->idComprobanteProveedor;
            $oRelFacturaPago->idPagoProveedor = $oPagoProveedor->id;
            if (!$oRelFacturaPago->save()){
                $transaction->rollback();
                throw new CHttpException(500,CHtml::errorSummary($oRelFacturaPago));
            }
        }

        $modelOP->idPagoProveedor = $oPagoProveedor->id;
        if (!$modelOP->save()){
            $transaction->rollback();
            throw new CHttpException(500, CHtml::errorSummary($modelOP));
        }

        if ($shouldCommit){
            Yii::log('Commit 1 ','warning');
            $transaction->commit();
        }

        $this->redirect(array('PagoProveedor/update','id'=> $oPagoProveedor->id));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.OrdenDePago::model()->getAttributeLabel('model').' #'.$id;
        $aFactProvOP = new FacturaOrdenPago('search');
        $aFactProvOP->unsetAttributes();
        $aFactProvOP->idOrdeDePago = $id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'aFactProvOP' => $aFactProvOP,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }

		$model=new OrdenDePago;
		$this->pageTitle = "Nuevo ".OrdenDePago::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrdenDePago']))
		{
			$model->attributes=$_POST['OrdenDePago'];
			if ($model->fecha != null){
                $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            }else{
			    $model->fecha = date('Y-m-d');
            }

            Yii::log(' '.ComponentesComunes::print_array($_POST,true),'warning');
			if($model->save()){
			    if (isset($_POST['factura-proveedor-grid_c0'])){

                    foreach ($_POST['factura-proveedor-grid_c0'] as $index => $item) {
                        $relFactOP = new FacturaOrdenPago();
                        $relFactOP->idComprobanteProveedor = $item;
                        $relFactOP->idOrdeDePago = $model->id;
                        Yii::log('$item '.$item);
                        if (!$relFactOP->save()){
                            $transaction->rollback();
                            throw new CHttpException(500,CHtml::errorSummary($relFactOP));
                        }
                    }
                }

                if ($shouldCommit){
                    Yii::log('Commit 2 ','warning');
                    $transaction->commit();
                }

                $this->redirect(array('view','id'=>$model->id));
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new OrdenDePago;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrdenDePago']))
		{
			$model->attributes=$_POST['OrdenDePago'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.OrdenDePago::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OrdenDePago']))
		{
			$model->attributes=$_POST['OrdenDePago'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new OrdenDePago('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OrdenDePago']))
			$model->attributes=$_GET['OrdenDePago'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new OrdenDePago('search');
		$this->pageTitle = 'Administrando '.OrdenDePago::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OrdenDePago'])){
			$model->attributes=$_GET['OrdenDePago'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OrdenDePago the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OrdenDePago::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OrdenDePago $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='orden-de-pago-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
