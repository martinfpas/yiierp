<?php

class NotaDebitoController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/comprobantes';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Pdf','SaveFieldAsync','json','FormAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionJson($id){
        $oComprobante = $this->loadModel($id);
        if($oComprobante->estado < Comprobante::iCerrada){
            $oComprobante->recalcular();
            if (!$oComprobante->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oComprobante));
            }
        }
        $attributes = $oComprobante->attributes;
        $attributes['AlicuotaItem'] = $oComprobante->getAlicuotaItem();
        echo json_encode($attributes);
    }

    /**
     * Displays a particular model partialy to use in ajax call
     *
     * @param integer $id the ID of the model to be displayed (if you will update)
     * @param integer $idNotaPedido the ID of the associated NotaPedido
     */
    public function actionFormAsync($idNotaPedido,$id = null,$idCliente = null)
    {

        $is_modal = isset($_GET['is_modal']);

        $aProdNotaDebito = new ProdNotaDebito('search');
        $aProdNotaDebito->unsetAttributes();
        $modelProd = new ProdNotaDebito();

        if ($id > 0){
            $model = $this->loadModel($id);
            $aProdNotaDebito->idComprobante = $id;
            $modelProd->idComprobante = $id;
        }else{
            // TODO: HACER NC EN BASE A UNA NOTA DE PEDIDO
            if ($idNotaPedido > 0){
                return false;

                $transaction=Yii::app()->db->beginTransaction();
                Yii::log(' $idNotaPedido ::'.$idNotaPedido);
                try
                {

                    $model = NotaDebito::generarFa($idNotaPedido);
                    if ($model == null){
                        $transaction->rollback();
                        throw new CHttpException('500','Error');

                    }
                    $transaction->commit();
                }
                catch(Exception $e)
                {
                    $transaction->rollback();
                    throw new CHttpException('500','Error'.CHtml::errorSummary($e->getMessage()));
                }

            }else{
                //TODO: IMPLEMENTAR...
                try{

                    $model = NotaDebito::generarND($idCliente);

                    if($model != null){
                        //echo $model->id;
                    }else{
                        throw new CHttpException('500','Error al generar la Nota De Débito');
                    }
                }catch (Exception $e){
                    throw new CHttpException('500','Error al generar la Nota De Débito.'.$e->getMessage());
                }
            }

            $modelProd->idComprobante = $model->id;
            $aProdNotaDebito->idComprobante = $model->id;

        }

        $model->calcularNumero();

        $this->renderPartial('_formFila',array(
            'model'=>$model,
            'aProdNotaDebito' => $aProdNotaDebito,
            'modelProd' => $modelProd,
            'ajaxMode' => true,
            'is_modal' => $is_modal
        ), false,true);
    }

    /**
     * Displays a particular model  in pdf.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionPdf($id,$print=false){

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew',10,10,10,5,9,9,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew',10,10,10,5,9,9,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/facturaPdf.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $this->layout = 'column1Pdf';

        $aProds = new ProdNotaCredito('search');
        $aProds->unsetAttributes();
        $aProds->idComprobante = $id;

        $model = $this->loadModel($id);
        $model->createBarcodeImg();

        //TODO: CUANDO SE ENVIE POR CORREO, ELIMINAR LA IMPRESION DEL ORIGINAL
        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
            'model'=>$model,
            'aProdComprobante' => $aProds,
            'copia' => 'ORIGINAL',
        ),true)
        );

        $mPDF1->AddPage('P','', '','','',10,10,10,5,9,9);

        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
            'model'=>$model,
            'aProdComprobante' => $aProds,
            'copia' => 'DUPLICADO',
        ),true)
        );

        $filename = 'NotaDebito_'.str_pad($model->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT).'-'.str_pad($model->Nro_Comprobante,8,'0',STR_PAD_LEFT).'.pdf';

        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);

            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }
    
    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oNotaDebito = new NotaDebito($_POST['scenario']);

        $oNotaDebito = NotaDebito::model()->findByPk($_POST['pk']);
        if ($oNotaDebito != null) {
            $oNotaDebito->setAttribute($_POST['name'],$_POST['value']);

            if (!$oNotaDebito->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oNotaDebito));
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->pageTitle = 'Ver NotaDebito #'.$id;
        $aProdNotaDebito = new ProdNotaDebito('search');
        $aProdNotaDebito->unsetAttributes();
        $aProdNotaDebito->idComprobante = $id;
        $oProdNotaDebito = new ProdNotaDebito();
        $oProdNotaDebito->idComprobante = $id;
        $model = $this->loadModel($id);
        $model->createBarcodeImg();

        $model->calcularNumero();

        $this->render('view',array(
            'model'=> $model,
            'oProdNotaDebito' => $oProdNotaDebito,
            'aProdNotaDebito' => $aProdNotaDebito,
        ));
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new NotaDebito;
		$this->pageTitle = "Nueva ".NotaDebito::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaDebito']))
		{
			$model->attributes=$_POST['NotaDebito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new NotaDebito;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaDebito']))
		{
			$model->attributes=$_POST['NotaDebito'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando NotaDebito #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['NotaDebito']))
		{
			$model->attributes=$_POST['NotaDebito'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

        $model->calcularNumero();

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new NotaDebito('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaDebito']))
			$model->attributes=$_GET['NotaDebito'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new NotaDebito('search');
		$this->pageTitle = 'Administrando Nota Debitos ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['NotaDebito'])){
			$model->attributes=$_GET['NotaDebito'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return NotaDebito the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=NotaDebito::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param NotaDebito $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='nota-debito-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
