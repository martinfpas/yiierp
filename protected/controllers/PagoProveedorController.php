<?php

class PagoProveedorController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','json','ViewPdf'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


    public function actionJson($id){
        $oPago = PagoProveedor::model()->findByPk($id);
        if($oPago != null){
            if($oPago->estado < PagoProveedor::iCerrada){
                try{
                    $oPago->recalcular();
                }catch (Exception $e){
                    throw new CHttpException(500,$e->getMessage());
                }
            }
            $attributes = $oPago->attributes;
            $montoFacturas = 0.00;
            foreach ($oPago->aPago2FactProv as $index => $item) {
                $montoFacturas += number_format($item->oComprobanteProveedor->TotalConFactor,2,',','');
            }
            $attributes['montoFacturas'] = number_format($montoFacturas,2,',','.');
        }else{
            $attributes = array();
        }
        echo json_encode($attributes);
    }


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.PagoProveedor::model()->getAttributeLabel('model').' #'.$id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewPdf($id)
    {
        $model=$this->loadModel($id);

        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','12','couriernew', 15,7,8,15,8,0,'P');

        $this->pageTitle = 'Ver '.PagoProveedor::model()->getAttributeLabel('model').' #'.$id;

        $aPago2FactProv = new Pago2FactProv('search');
        $aPago2FactProv->unsetAttributes();
        $aPago2FactProv->idPagoProveedor = $id;

        /*
        $this->render('viewPdf',array(
            'model'=>$model,
            'aPago2FactProv' => $aPago2FactProv,
        ));
        */

        $filename = 'PagoProveedor_'.$id.'.pdf';
        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
            'model'=>$model,
            'aPago2FactProv' => $aPago2FactProv,
        ),true)
        );

        $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PagoProveedor;
		$this->pageTitle = "Nuevo ".PagoProveedor::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoProveedor']))
		{
			$model->attributes=$_POST['PagoProveedor'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save())
				$this->redirect(array('update','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new PagoProveedor;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoProveedor']))
		{
			$model->attributes=$_POST['PagoProveedor'];
			$model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if($model->estado == PagoProveedor::iCerrada){
		    $this->redirect(array('PagoProveedor/view','id'=> $id));
        }
		$this->pageTitle = 'Modificando '.PagoProveedor::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PagoProveedor']))
		{
			$model->attributes=$_POST['PagoProveedor'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            if(isset($_POST['cerrar'])){
                $model->estado = PagoProveedor::iCerrada;
            }
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$pagoCH = new PagoProvCheque();
        $pagoCH->idPagoProv = $id;

        $pagoEf = new PagoProvEfectivo();
        $pagoEf->idPagoProveedor = $id;

        $pagoMB = new PagoProvMovimientoBanc();
        $pagoMB->idPagoProveedor = $id;

        $pagoCHPpio = new PagoProveedorChequePpio();
        $pagoCHPpio->idPagoProv = $id;

        $aPago2FactProv = new Pago2FactProv('search');
        $aPago2FactProv->unsetAttributes();
        $aPago2FactProv->idPagoProveedor = $id;

        $oPago2FactProv = new Pago2FactProv();
        $oPago2FactProv->unsetAttributes();
        $oPago2FactProv->idPagoProveedor = $id;

		$this->render('update',array(
			'model'=>$model,
            'pagoCH' => $pagoCH,
            'pagoEf' => $pagoEf,
            'pagoMB' => $pagoMB,
            'pagoCHPpio' => $pagoCHPpio,
            'aPago2FactProv' => $aPago2FactProv,
            'oPago2FactProv' => $oPago2FactProv,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new PagoProveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoProveedor']))
			$model->attributes=$_GET['PagoProveedor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PagoProveedor('search');
		$this->pageTitle = 'Administrando '.PagoProveedor::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PagoProveedor'])){
			$model->attributes=$_GET['PagoProveedor'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PagoProveedor the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PagoProveedor::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param PagoProveedor $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pago-proveedor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
