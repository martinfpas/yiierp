<?php
/**@ version 1.0.1 **/

class CargaController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/';
    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="27%" align="center">{PAGENO}/{nbpg}</td>
						<td width="30%" style="text-align: right;"></td>
					</tr>
				</table>';

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','view2','admin','adminHistoricas','SaveAsync',
                    'SaveFieldAsync','pdf','pdf2','Facturada','Despachada','CodigosCot','Cerrar','PrintPlanillaCarga','ClonarPedidosDeCarga',
                    'Revisar','ViewPartial'
                ),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
    }
    
    public function actionRevisar(int $id){
        $oCarga = $this->loadModel($id);
        
        $this->render('revisar',array(
            'model' => $oCarga,
        ));
    }

    public function actionClonarPedidosDeCarga($id,$idCamion = 14){
        /**
         * @var $aNotasPedido NotaPedido
         * @var $itemProd ProdNotaPedido
         * @var $oProdNotaPedido ProdNotaPedido
         *
         */

        $transaction = Yii::app()->db->beginTransaction();
        $aNotasPedido = NotaPedido::model()->findAllByAttributes(array('idCarga'=>$id));
        foreach ($aNotasPedido as $index => $item) {
            $oNotaPedido = new NotaPedido();
            $oNotaPedido->attributes = $item->attributes;
            $oNotaPedido->idCarga = null;
            $oNotaPedido->camion = $idCamion;
            if(!$oNotaPedido->save()){
                $transaction->rollback();
                throw new CHttpException('500' , CHtml::errorSummary($oNotaPedido));
            }

            foreach ($item->aProdNotaPedido as $index => $itemProd) {
                $oProdNotaPedido = new ProdNotaPedido();
                $oProdNotaPedido->attributes = $itemProd->attributes;
                $oProdNotaPedido->idNotaPedido = $oNotaPedido->id;
                if (!$oProdNotaPedido->save()){
                    $transaction->rollback();
                    throw new CHttpException(500,CHtml::errorSummary($oProdNotaPedido));
                }
            }


        }

        $transaction->commit();

    }



	public function actionCodigosCot($idCarga){
	    $vArtCot = new VArtCotbyEc('search');
        $vArtCot->unsetAttributes();
        $vArtCot->idCarga = $idCarga;

        $this->render('codigosCot',array(
            'vArtCot' => $vArtCot,
        ));

    }

    public function actionCerrar($id){
	    /** @var $oCargaDoc CargaDocumentos
            @var $oCarga Carga
         *  @var $oNotaEntrega NotaEntrega
         *  @var $oComprobante Comprobante
         */

	    $oCarga = Carga::model()->with()->findByPk($id);

        $transaction=Yii::app()->db->beginTransaction();
	    if ($oCarga->estado == Carga::iDespachada ){
            foreach ($oCarga->aCargaDocs as $index => $oCargaDoc) {
                //Yii::log('$oCargaDoc attributes '.ComponentesComunes::print_array($oCargaDoc->attributes,true),'warning');
                //NO HAY DEVOLUCION TOTAL
                if ($oCargaDoc->devolucion != $oCargaDoc->totalFinal){
                    $idNE = null;
                    $idFa = null;
                    //TODO: VER COMO CREAR NC O DEVOLUCION POR LA DIFERENCIA
                    if($oCargaDoc->tipoDoc == 'NE'){
                        $oNotaEntrega = NotaEntrega::model()->findByPk($oCargaDoc->idComprobante);
                        $idNE = $oCargaDoc->idComprobante;
                        $oNotaEntrega->recibida = ($oCargaDoc->devolucion <= $oNotaEntrega->totalFinal);
                        $montoPagado = ($oCargaDoc->oPagoCliente != null)? $oCargaDoc->oPagoCliente->monto + $oCargaDoc->devolucion : $oCargaDoc->devolucion;
                        $oNotaEntrega->montoPagado = ($montoPagado > $oNotaEntrega->totalFinal)? $oNotaEntrega->totalFinal : $montoPagado;

                        if (!$oNotaEntrega->save()){
                            Yii::log('Error al guardar NE'.CHtml::errorSummary($oNotaEntrega),'error');
                            $transaction->rollback();
                            throw new CHttpException('500',CHtml::errorSummary($oNotaEntrega));
                        }else{
                            //Yii::log('Guardarndo NE::'.$oNotaEntrega->id,'warning');
                        }
                        //HAY DEVOLUCION PARCIAL Y ES NOTA DE ENTREGA, HAGO UN PAGO EN CONCEPTO DE DEVOLUCION DE MERCADERIA
                        if($oCargaDoc->devolucion <> 0) {
                            try{
                                PagoCliente::devolucionMercaderia($oNotaEntrega->idCliente,$oCargaDoc->devolucion,$idNE,$idFa);
                            }catch (Exception $exception){
                                $transaction->rollback();
                                throw new CHttpException('500',$exception->getMessage());
                            }
                        }
                    }else{ //
                        $oComprobante = Comprobante::model()->findByPk($oCargaDoc->idComprobante);
                        if($oComprobante != null && $oCargaDoc->oPagoCliente != null ){
                            $oComprobante->montoPagado = ($oCargaDoc->oPagoCliente->monto > $oComprobante->Total_Final)? $oComprobante->Total_Final : $oCargaDoc->oPagoCliente->monto;
                        }
                        $oComprobante->Recibida = ($oCargaDoc->devolucion <= $oComprobante->Total_Final);
                        if (!$oComprobante->save()){
                            Yii::log('Error al guardar EL COMPROBANTE'.CHtml::errorSummary($oComprobante),'error');
                            $transaction->rollback();
                            throw new CHttpException('500',CHtml::errorSummary($oComprobante));
                        }else{
                            //Yii::log('Guardarndo FAC ::'.$oComprobante->id,'warning');
                        }
                    }

                }else{ // SE DEVOLVIO COMPLETO EL COMPROBANTE
                    $oCargaDoc->efectivo = 0;
                    $oCargaDoc->pagado = false;
                    $oCargaDoc->save();
                    if($oCargaDoc->tipoDoc == 'NE'){
                        $oNotaEntrega = NotaEntrega::model()->findByPk($oCargaDoc->idComprobante);
                        $oNotaEntrega->estado = NotaEntrega::iAnulada;
                        $oNotaEntrega->recibida = false;
                        $oNotaEntrega->montoPagado = $oNotaEntrega->totalFinal;
                        if (!$oNotaEntrega->save()){
                            Yii::log('Error al guardar NE'.CHtml::errorSummary($oNotaEntrega),'error');
                            $transaction->rollback();
                            throw new CHttpException('500',CHtml::errorSummary($oNotaEntrega));
                        }
                        try{
                            PagoCliente::devolucionMercaderia($oNotaEntrega->idCliente,$oCargaDoc->devolucion,$oNotaEntrega->id);
                        }catch (Exception $exception){
                            $transaction->rollback();
                            throw new CHttpException('500',$exception->getMessage());
                        }
                    }else{
                        if($oCargaDoc->tipoDoc == 'FAC'){
                            $oFactura = Factura::model()->findByPk($oCargaDoc->idComprobante);
                            if($oFactura->Recibida != false){
                                $oFactura->Recibida = false;
                                if (!$oFactura->save()){
                                    Yii::log('Error al guardar FACT'.CHtml::errorSummary($oFactura),'error');
                                    $transaction->rollback();
                                    throw new CHttpException('500',CHtml::errorSummary($oFactura));
                                }
                            }
                            /*
                             try{
                                PagoCliente::devolucionMercaderia($oNotaEntrega->idCliente,$oCargaDoc->devolucion,null,$oFactura->id);
                            }catch (Exception $exception){
                                $transaction->rollback();
                                throw new CHttpException('500',$exception->getMessage());
                            }
                            */
                        }else if($oCargaDoc->tipoDoc == 'NC'){
                            $oNotaCredito = NotaCredito::model()->findByPk($oCargaDoc->idComprobante);
                            if($oNotaCredito->Recibida != true){
                                $oNotaCredito->Recibida = true;
                                if (!$oNotaCredito->save()){
                                    Yii::log('Error al guardar FACT'.CHtml::errorSummary($oNotaCredito),'error');
                                    $transaction->rollback();
                                    throw new CHttpException('500',CHtml::errorSummary($oNotaCredito));
                                }
                            }
                        }
                    }
                }
            }
            $oCarga->estado = Carga::iCerrada;
            if (!$oCarga->save()){
                Yii::log('Error al guardar La Carga'.CHtml::errorSummary($oCarga),'error');
                $transaction->rollback();
                throw new CHttpException('500',CHtml::errorSummary($oCarga));

            }else{
                $transaction->commit();
                $this->redirect(array('view','id'=>$id));
            }
        }else{
	        throw new CHttpException('500','Estado inválido, la a carga debe estar despachada antes de cerrar');
        }
    }

	public function actionDespachada($id){
        /** @var $oNotaEntrega NotaEntrega*/
        $oCarga = $this->loadModel($id);
        $transaction=Yii::app()->db->beginTransaction();
        if ($oCarga->estado == Carga::iFacturada){
            $oCarga->estado = Carga::iDespachada;

            //TODO: MARCAR FACTURAS Y NOTAS DE ENVIO COMO ENVIADAS
            foreach ($oCarga->oEntregaDoc->aCargaDocumentos as $index => $oCargaDocumentos) {

                if($oCargaDocumentos->tipoDoc == 'NE'){
                    $oNotaEntrega = NotaEntrega::model()->findByPk($oCargaDocumentos->idComprobante);
                    $oNotaEntrega->enviada = true;
                    $oNotaEntrega->recibida = true;
                    $oNotaEntrega->estado = NotaEntrega::iCerrada;
                    if (!$oNotaEntrega->save()){
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oNotaEntrega));
                    }
                }else{
                    $oComprobante = Comprobante::model()->findByPk($oCargaDocumentos->idComprobante);
                    $oComprobante->Enviada = true;
                    $oComprobante->Recibida = true;
                    if (!$oComprobante->save()){
                        $transaction->rollback();
                        throw new CHttpException('500',CHtml::errorSummary($oComprobante));
                    }
                }
            }

            if (!$oCarga->save()){
                $transaction->rollback();
                throw new CHttpException('500',CHtml::errorSummary($oCarga));
            }

            if(isset($_GET['ajax'])){

            }else{
                $transaction->commit();
                $this->redirect(array('view','id'=>$id));
            }
        }else{
            $transaction->rollback();
            throw new CHttpException(500,'El estado de la carga debe ser Facturado antes de Despachar.');
        }
    }

    public function actionFacturada($id){
        $oCarga = $this->loadModel($id);
        if ($oCarga->estado == Carga::iBorrador){
            $oCarga->estado = Carga::iFacturada;
            if (!$oCarga->save()){
                throw new CHttpException('500',CHtml::errorSummary($oCarga));
            }


            foreach ($oCarga->aNotaPedido as $index => $oNotaPedido) {
                if($oNotaPedido->oNotaEntrega != null){
                    $oNotaEntrega = $oNotaPedido->oNotaEntrega;
                    if ($oNotaEntrega->estado == NotaEntrega::iBorrador){
                        $oNotaEntrega->estado = NotaEntrega::iGuardada;
                        if (!$oNotaEntrega->save()){
                            //$transaction->rollback();
                            throw new CHttpException('500',CHtml::errorSummary($oNotaEntrega));
                        }
                    }
                }
            }

            if(isset($_GET['ajax'])){

            }else{
                $this->redirect(array('view','id'=>$id));
            }
        }else{
            throw new CHttpException(500,'El estado de la carga debe ser Borrador antes de marcar como Facturada.');
        }
    }

    	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionViewPartial($id,$idNotaPedido)
	{
        $aNotasPedido = new NotaPedido('search');
        $aNotasPedido->unsetAttributes();
        $aNotasPedido->idCarga = $id;
        $aNotasPedido->id = $idNotaPedido;
        $model = $this->loadModel($id);
		
        $this->renderPartial('_notasPedidoGrid',array(
            'model'=> $model,
            'aNotasPedido' => $aNotasPedido,
        ));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Carga #'.$id;
        $aProds = new ProdCarga('search');
        $aProds->unsetAttributes();
        $aProds->idCarga = $id;
        $aNotasPedido = new NotaPedido('search');
        $aNotasPedido->unsetAttributes();
        $aNotasPedido->idCarga = $id;
        $aDepuraciones  = new Depuracion('search');
        $aDepuraciones->unsetAttributes();
        $aDepuraciones->idCarga = $id;

		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'aProds' => $aProds,
            'aNotasPedido' => $aNotasPedido,
            'aDepuraciones' => $aDepuraciones,
		));
	}

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView2($id)
    {
        $this->pageTitle = 'Ver Carga #'.$id;
        $aProds = new ProdCarga('search');
        $aProds->unsetAttributes();
        $aProds->idCarga = $id;
        $aNotasPedido = new NotaPedido('search');
        $aNotasPedido->unsetAttributes();
        $aNotasPedido->idCarga = $id;
        $aDepuraciones  = new Depuracion('search');
        $aDepuraciones->unsetAttributes();
        $aDepuraciones->idCarga = $id;


        $this->render('view2',array(
            'model'=>$this->loadModel($id),
            'aProds' => $aProds,
            'aNotasPedido' => $aNotasPedido,
            'aDepuraciones' => $aDepuraciones,
        ));
    }


    /**
     * Exporta la planilla de carga en pdf. Funciona ok.
     * @param integer $id the ID of the model to be displayed
     *
     */
    public function actionPdf($id,$print=false)
    {
        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                // PERFECTO EN DESARROLLO $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        //$mPDF1->SetHTMLFooter('{PAGENO}/{nbpg}');


        $model = $this->loadModel($id);
        $model->observacion .= ' ';

        $aProds = new ProdCarga('search');
        $aProds->unsetAttributes();
        $aProds->idCarga = $id;
        $this->layout = 'column1Pdf';

        $vArtCot = new VArtCotbyEc('search');
        $vArtCot->unsetAttributes();
        $vArtCot->idCarga = $id;


        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
                'model'=>$model,
                'aProds' => $aProds,
            ),true)
        );

        $mPDF1->AddPage('','', '','12','couriernew',15,7,25,25,10,5,'P');


        $mPDF1->WriteHTML( $this->renderPartial('codigosCot',array(
                'vArtCot' => $vArtCot,
            ),true)
        );
        

        $mPDF1->shrink_tables_to_fit=0;


        $filename = 'Carga_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            // Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-ranges=1-3 -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            //Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

    /**
     * Exporta la planilla de carga en pdf. Funciona ok.
     * @param integer $id the ID of the model to be displayed
     *
     */
    public function actionPdf2($id,$print=false)
    {
        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                // OK    $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,5,25,50,5,5,'P');
                // CASI  $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,40,10,5,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P'); // OK en desarrollo
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,15,25,0,5,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,15,25,0,5,0,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);
        //$mPDF1->SetHTMLFooter('{PAGENO}/{nbpg}');


        $model = $this->loadModel($id);
        $model->observacion .= ' ';

        $aProds = new ProdCarga('search');
        $aProds->unsetAttributes();
        $aProds->idCarga = $id;
        $this->layout = 'column1Pdf';

        $vArtCot = new VArtCotbyEc('search');
        $vArtCot->unsetAttributes();
        $vArtCot->idCarga = $id;


        $mPDF1->WriteHTML($this->renderPartial('viewPdf2',array(
            'model'=>$model,
            'aProds' => $aProds,
        ),true)
        );
        /*
        $mPDF1->AddPage('','', '','12','couriernew',15,7,25,25,10,5,'P');


        $mPDF1->WriteHTML( $this->renderPartial('codigosCot',array(
                'vArtCot' => $vArtCot,
            ),true)
        );
        */

        $mPDF1->shrink_tables_to_fit=0;


        $filename = 'Carga_'.$id.'-2.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            // Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-ranges=1-3 -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            //Yii::log(shell_exec(' lp -d "'. $printer.'" -o media=FanFoldGerman -o page-bottom=25 -o page-left=18 -o page-right=18 -o page-top=25 "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage, "FanFoldGerman",25,$left=0,$right=0,$top=25); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Carga;
		$this->pageTitle = "Nuevo Carga";

		$idCamion = (isset($_GET['idCamion']))? $_GET['idCamion'] : null;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Carga']))
		{
			$model->attributes=$_POST['Carga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}else{
		    if ($model->fecha == null){
                $model->fecha = date("Y-m-d");
                $model->idVehiculo = $idCamion;
            }
        }

		$this->render('create',array(
			'model'=>$model,
            'idCamion' => $idCamion,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Carga;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Carga']))
		{
		    $model->attributes=$_POST['Carga'];
			$model->fecha = ComponentesComunes::jpicker2db($model->fecha);
			if($model->save()){
				echo $model->id;
				die();
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}


    public function actionSaveFieldAsync(){
        /* @var $oNotaPedido NotaPedido */
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oCarga = new Carga($_POST['scenario']);

        $oCarga = Carga::model()->findByPk($_POST['pk']);
        if ($oCarga != null) {
            $oCarga->setAttribute($_POST['name'],$_POST['value']);
            if (!$oCarga->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oCarga));
            }else{
                if($_POST['name'] == 'idVehiculo'){
                    foreach ($oCarga->aNotaPedido as $index => $oNotaPedido) {
                        $oNotaPedido->camion = $_POST['value'];
                        if(!$oNotaPedido->save()){
                            //TODO: VER SI USAMOS TRANSACCION
                            Yii::log('$oNotaPedido'.CHtml::errorSummary($oNotaPedido),'error');
                        }else{
                            Yii::log('$oNotaPedido SAVED','warning');
                            if ($oNotaPedido->oNotaEntrega != null){
                                $oNotaEntrega = $oNotaPedido->oNotaEntrega;
                                $oNotaEntrega->idCamion = $oNotaPedido->camion;
                                if(!$oNotaEntrega->save()){
                                    //TODO: VER SI USAMOS TRANSACCION
                                    Yii::log('$oNotaEntrega'.CHtml::errorSummary($oNotaEntrega),'error');
                                }
                            }else{
                                if ($oNotaPedido->oFactura != null){
                                    $oFactura = $oNotaPedido->oFactura;
                                    $oFactura->Id_Camion = $oNotaPedido->camion;
                                    if (!$oFactura->save()){
                                        //TODO: VER SI USAMOS TRANSACCION
                                        Yii::log('$oFactura'.CHtml::errorSummary($oFactura),'error');
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Carga #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Carga']))
		{
			$model->attributes=$_POST['Carga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}else{
		    $aProds = new ProdCarga('search');
            $aProds->unsetAttributes();
            $aProds->idCarga = $id;
        }

		$this->render('update',array(
			'model'=>$model,
            'aProds' => $aProds,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Carga('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Carga']))
			$model->attributes=$_GET['Carga'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Carga('search');
		$this->pageTitle = 'Administrando Cargas ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Carga'])){
			$model->attributes=$_GET['Carga'];
		}

		$this->render('adminNoDespachadas',array(
			'model'=>$model,
		));
	}
    /**
     * Manages all models.
     */
    public function actionAdminHistoricas()
    {
        $model=new Carga('search');
        $this->pageTitle = 'Administrando Cargas ';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Carga'])){
            $model->attributes=$_GET['Carga'];
        }

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Carga the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Carga::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Carga $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='carga-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
