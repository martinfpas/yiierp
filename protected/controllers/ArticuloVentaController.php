<?php

class ArticuloVentaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('RecalcularNombre','create','update','admin','adminModificarPrecios','delete','index','view','admin','SaveAsync','SaveFieldAsync',
                    'autoDescripcion','ListaArticulos','ImportarDatosCsv','buscarPresentacAsync','GetAsync','GetAsyncById','ListaPresentaciones',
                    'ListaPaquetes','BuscaArtVenta','UpdateFromSgv'),

				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_articulos_vta")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionUpdateFromSgv(){
	    /** @var $oArticuloVenta ArticuloVenta */
        $user = 'phpmyadminuser';
        $pass = 'P@ssw0rd';
        $db = new PDO('mysql:host=localhost;dbname=sgv;', $user, $pass);

        $stmt = $db->prepare("SELECT * FROM Presentacion");
        $stmt->execute();
        $articulos = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($articulos as $articulo){
            $oArticulo = Articulo::model()->findByAttributes(array('id_articulo' => $articulo->id_articulo,'id_rubro' => $articulo->id_rubro));
            if($oArticulo == null){
                echo 'NO EXISTE EL ARTICULO. <br>';
                print_r($articulo);
                echo '<br>';
                //die();
            }else{
                $oArticuloVenta = ArticuloVenta::model()->findByAttributes(array('id_presentacion' => $articulo->Id_presentacion,'id_articulo'=>$oArticulo->id));
                if ($oArticuloVenta != null){
                    //SE ACTUALIZA
                    //echo 'SE ACTUALIZA:'.$articulo->Descripcion ." > ".$oArticuloVenta->id_articulo_vta."<br>";
                    try {
                        if (utf8_encode($articulo->Descripcion) != $oArticuloVenta->descripcion && strpos($oArticuloVenta->descripcion,'CIRUELA D´AGEN') == -1) {
                            echo 'SE ACTUALIZA Descripcion:' . $oArticuloVenta->descripcion . ' > ' . utf8_encode($articulo->Descripcion) . " : " . $oArticuloVenta->id_articulo_vta . "<br>";
                            $oArticuloVenta->descripcion = utf8_encode($articulo->Descripcion);
                            if (!$oArticuloVenta->save()) {
                                echo '<br>' . CHtml::errorSummary($oArticuloVenta);
                            }
                        }
                        if ($articulo->Precio != $oArticuloVenta->precio) {
                            echo 'SE ACTUALIZA Precio:' . $oArticuloVenta->id_articulo_vta . ' :: ' . $oArticuloVenta->precio . " > " . $articulo->Precio . "<br>";
                            $oArticuloVenta->precio = $articulo->Precio;
                            if (!$oArticuloVenta->save()) {
                                echo '<br>' . CHtml::errorSummary($oArticuloVenta);
                            }
                        }
                        if ($articulo->Costo_base != $oArticuloVenta->costo) {
                            echo 'SE ACTUALIZA Costo:' . $oArticuloVenta->id_articulo_vta . ' : ' . $oArticuloVenta->costo . " > " . $articulo->Costo_base . "<br>";
                            $oArticuloVenta->costo = $articulo->Costo_base;
                            if (!$oArticuloVenta->save()) {
                                echo '<br>' . CHtml::errorSummary($oArticuloVenta);
                            }
                        }
                    }catch (CDbException $exception){
                        $exception->getMessage();
                    }
                }else{
                    //nuevo
                    echo 'NUEVO::'.$articulo->Descripcion ." > ".$oArticulo->id."<br>";
                }

            }
        }
    }

	public function actionRecalcularNombre(){
	    $articulos = ArticuloVenta::model()->findAll();
        foreach ($articulos as $index => $articulo) {
            $aDesc = explode(' x',$articulo->descripcion);
            if(sizeof($aDesc) == 1){
                $aDesc = explode(' X',$articulo->descripcion);
            }
            //echo '<br>'.$aDesc[sizeof($aDesc)-1];
            echo '<br>'.$aDesc[0];
            $articulo->nombre = $aDesc[0];
            if ($articulo->save()){

            }
            ComponentesComunes::print_array($articulo->attributes);

            //$articulo->nombre
        }
    }

    function actionBuscaArtVenta($js_comp = '#ProdFactura_idArtVenta'){
        $this->widget('BuscaArtVenta', array('js_comp'=>$js_comp,'is_modal' => true,'ajaxMode' => false));
    }


    /* BUSCA el articulo
     * @familia
     * @subfamilia
     */
    public function actionGetAsync() {

        $idRubro = (isset($_GET['rubro'])? intval($_GET['rubro']) : NULL );
        $idArticulo = (isset($_GET['articulo'])? intval($_GET['articulo']) : NULL );
        $idPresentacion = (isset($_GET['presentacion'])? intval($_GET['presentacion']) : NULL );


        $oArticulo = Articulo::model()->findByAttributes(array('id_rubro' => $idRubro,'id_articulo' => $idArticulo));

        if ($oArticulo == null){
            throw new CHttpException('500','<span class="errorSpan">Error: Error de parametros</span>');
        }

        $item = ArticuloVenta::model()->findByAttributes(array('id_presentacion' => $idPresentacion,'id_articulo' => $oArticulo->id));

        $arr = array();
        if($item ==! null){
            if (isset($_GET['alicuota'])){
                $alicuota = number_format($_GET['alicuota'],2,'.','');
                $item->precio = number_format($item->precio * (1 + ($alicuota /100)),2,'.','');
            }else{
                Yii::log('SIN alicuota:','warning');
            }
            $arr = $item->attributes;
        }else{
            throw new CHttpException('500','<span class="errorSpan">Error: La Presentacion no existe</span>');
        }

        echo CJSON::encode($arr);
    }

    /*
     * DEVUELVE UN JSON CON LOS DATOS DEL ARTICULO
     * El precio va con iva al 21% por defecto
     * */
    public function actionGetAsyncById($id,$alicuota = 21){
        $model = ArticuloVenta::model()->with('oArticulo')->findByPk($id);
        $model->precio = $model->precio * (1 + ($alicuota /100));
        $arr = $model->attributes;
        $arr['Articulo'] = $model->oArticulo->descripcion;
        $arr['Rubro'] = $model->oArticulo->oRubro->descripcion;
        echo CJSON::encode($arr);

    }

	/* BUSCA LOS articulos CON LA POSIBILIDAD DE FILTRAR POR
	 * @proveedor integer
	 * @familia
	 * @subfamilia
	 */
	public function actionBuscarPresentacAsync() {
		$idArticulo = (isset($_GET['articulo'])? intval($_GET['articulo']) : NULL );
		$idPresentacion = (isset($_GET['presentacion'])? intval($_GET['presentacion']) : NULL );
	
		$dataProvider = ArticuloVenta::model()->buscarMultiplesCampos($idPresentacion,$idArticulo);
		$data = $dataProvider->getData();
		
		$arr = array();
		foreach ($data as $item) {
			$arr[] = array(
					'id' => $item->id_articulo_vta,
					
					
					'value' => $item->id_presentacion,
					'label' => $item->id_presentacion.'::'.$item->descripcion,
					'descripcion' => $item->descripcion,
					
					'codBarra' => $item->codBarra,
					'enLista' => $item->enLista,
					'stockMinimo' 	=> $item->stockMinimo,
					'contenido' 	=> $item->contenido,
					'uMedida'		=> $item->uMedida,
					'precio'		=> $item->precio,
					'costo'			=> $item->costo,
					
			);
		}
	
		echo CJSON::encode($arr);
	
	}
	
	public function actionImportarDatosCsv() {
		
		$oArticulos = Articulo::model()->findAll();  
		
		$aArticulo = array();
		foreach ($oArticulos as $key => $oArticulo) {
			$aArticulo[$oArticulo->id_articulo.'-'.$oArticulo->id_rubro] = $oArticulo->id;
		}


        echo '<pre>';
        print_r($aArticulo);
        echo '</pre>';
		//die();

			
		$fp = fopen('/var/www/erca/upload/Presentacion.csv', 'r');
		if($fp)
		{
			//	$line = fgetcsv($fp, 1000, ",");
			//	print_r($line); exit;
			$first_time = true;
			$ix = 0;
			while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
			    /*
				echo '<br>';
				print_r($line);
				*/
			    if (isset($line[1]) && isset($line[2])){
                    $index = $line[1].'-'.$line[2];

                    if (isset($aArticulo[$index])) {
                        $idArticulo = $aArticulo[$index];
                        $id_presentacion = $line[0];
                        $oArtVenta = ArticuloVenta::model()->findByAttributes(array('id_articulo'=> $idArticulo,'id_presentacion' => $id_presentacion));
                        if ($oArtVenta == null){
                            echo '<br>Nuevo Articulo!';
                            $oArtVenta = new ArticuloVenta();
                            $oArtVenta->id_presentacion = $id_presentacion;
                            $oArtVenta->id_articulo = $idArticulo;

                        }else{
                            echo '<br>Update Nº'.$oArtVenta->id_articulo_vta;
                        }

                        if($line[3] == ''){
                            $line[3] = '-';
                        }
                        $oArtVenta->descripcion = utf8_encode($line[3]);
                        $oArtVenta->contenido = floatval($line[8]);
                        $oArtVenta->precio = floatval($line[4]);
                        $oArtVenta->costo = floatval($line[5]);
                        $oArtVenta->codBarra = utf8_encode($line[6]);
                        $oArtVenta->uMedida = $line[7];

                        if (!$oArtVenta->save()) {
                            echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[1].'-'.$line[2].'<br>';
                            print_r($oArtVenta->getErrors());
                        }else{
                            echo '>'.$line[0].':: el indice '.$line[1].'-'.$line[2].' OK :: linea : '.$ix.' '.$idArticulo.' *** '.$aArticulo[$line[1].'-'.$line[2]].'<br>';
                        }

                    }else {
                        echo '<br>'.$line[0].':: el indice '.$line[1].'-'.$line[2].'No Existe :: linea : '.$ix.'<br>';
                    }

                }else{
                    echo '<br> Print Line ';
                    print_r($line);
                }


            };
			
		}
	}
	
	public function actionListaArticulos()
	{
		if (isset($_POST['ArticuloVenta']['id_rubro'])) {
			$data=Articulo::model()->todosId()->findAll('id_rubro=:parent_id',
					array(':parent_id'=>(int)$_POST['ArticuloVenta']['id_rubro']));
		}else{
			$data=Articulo::model()->todosId()->findAll('idCat1=:parent_id',
					array(':parent_id'=>(int)$_POST['id_rubro']));
		}
	
	
		$data=CHtml::listData($data,'id','IdTitulo');
        echo CHtml::tag('option', array('value'=>'','art'=>''),CHtml::encode('-- Seleccionar -- >'),true);
		foreach($data as $value=>$name)
		{
			echo CHtml::tag('option',
					array('value'=>$value),CHtml::encode($name),true);
		}
	}
	
	public function actionListaPresentaciones()
	{
		if (isset($_POST['ArticuloVenta']['id_articulo'])) {
			$data=ArticuloVenta::model()->findAll('id_articulo=:idArticulo', array('idArticulo' => $_POST['ArticuloVenta']['id_articulo']));
		}else{
			if (isset($_POST['ArticuloVenta']['id_articulo'])) {
				$data=ArticuloVenta::model()->findAll('id_articulo=:idArticulo', array('id_articulo' => $_POST['id_articulo']));				
			}else {
				CHttpException('500','<span class="errorSpan">Error de pametros:</span>');
			}

		}

        if (sizeof($data) > 0){
            $data1=CHtml::listData($data,'id_articulo_vta','precio');
            $data2=CHtml::listData($data,'id_articulo_vta','descripcion');
            $listData=CHtml::listData($data,'id_articulo_vta','Presentacion');

            echo CHtml::tag('option', array('value'=>'','art'=>''),CHtml::encode('-- Seleccionar -- >'),true);
            foreach($listData as $value=>$name)
            {
                echo CHtml::tag('option', array('value'=>$value,'art'=>$data2[$value],'precio'=>$data1[$value]),CHtml::encode($name),true);
            }
        }else{
            echo CHtml::tag('option', array('value'=>'','art'=>''),CHtml::encode('- No hay presentaciones -'),true);
        }
		

	}
		
	public function actionListaPaquetes()
	{
		if (isset($_POST['ProdNotaPedido']['idArtVenta'])) {
			$data=Paquete::model()->todos()->findAll('id_artVenta=:idArticulo', array('idArticulo' => $_POST['ProdNotaPedido']['idArtVenta']));
		}else{
			if (isset($_POST['ProdNotaPedido']['idArtVenta'])) {
				$data=Paquete::model()->todos()->findAll('id_artVenta=:idArticulo', array('idArticulo' => $_POST['idArtVenta']));
			}else {
				CHttpException('500','<span class="errorSpan">Error de pametros:</span>');
			}
	
		}

		if (sizeof($data) > 0){
            $listData=CHtml::listData($data,'id','CantEnvase');

            echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Seleccionar --'),true);
            foreach($listData as $value=> $name)
            {
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            }

        }else{
            echo CHtml::tag('option', array('value'=>'','art'=>''),CHtml::encode('- No existen Paquetes -'),true);
        }

	}	
	
	
	public function actionAutoDescripcion() {
		$descripcion = '';
		if (isset($_POST['id_rubro'])) {
			$oRubro = Rubro::model()->findByPk($_POST['id_rubro']);
			if ($oRubro != null) {
				$descripcion = $oRubro->descripcion; 
			}
		}		
		if (isset($_POST['id_articulo'])) {
			$oArticulo = Articulo::model()->findByPk($_POST['id_articulo']);
			if ($oArticulo != null) {
				$descripcion .= ' '.$oArticulo->descripcion;
			}
		}
		
		echo $descripcion; 
	}
	
	

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver ArticuloVenta #'.$id;
		
		$aPaquete = new Paquete('search');
		$aPaquete->unsetAttributes();
		$aPaquete->id_artVenta = $id;
		// EVITA QUE SE APLIQUE EL DEFAULTSCOPE
        $aPaquete->resetScope(true);

		$this->render('view',array(
			'model'=>$this->loadModel($id),
			'aPaquete' => $aPaquete,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ArticuloVenta;
		$this->pageTitle = "Nuevo ArticuloVenta";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArticuloVenta']))
		{
			$model->attributes=$_POST['ArticuloVenta'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_articulo_vta));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ArticuloVenta;
		$this->pageTitle = "Nuevo ArticuloVenta";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArticuloVenta']))
		{
			if(isset($_POST['ArticuloVenta']['id_articulo_vta']) && $_POST['ArticuloVenta']['id_articulo_vta'] > 0){
				$model= ArticuloVenta::model()->findByPk($_POST['ArticuloVenta']['id_articulo_vta']);
			}
			
			$model->attributes=$_POST['ArticuloVenta'];
			if($model->save()){
				echo $model->id_articulo_vta;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

    public function actionSaveFieldAsync(){
        /* @var $oNotaPedido NotaPedido */
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oArticuloVenta = new ArticuloVenta($_POST['scenario']);

        $oArticuloVenta = ArticuloVenta::model()->findByPk($_POST['pk']);
        if ($oArticuloVenta != null) {
            $oArticuloVenta->setAttribute($_POST['name'],$_POST['value']);
            if($_POST['name'] == 'precio'){
                $_POST['value'] = str_replace('$','',$_POST['value']);
            }
            if (!$oArticuloVenta->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oArticuloVenta));
            }else{

            }
        }else {
            throw new CHttpException('500','No es un '.ArticuloVenta::model()->getAttributeLabel('model').' existente');
        }
        echo $_POST['value'];
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando ArticuloVenta #'.$id;
        $model->id_rubro = ($model->oArticulo != null)? $model->oArticulo->id_rubro : '';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ArticuloVenta']))
		{
			$model->attributes=$_POST['ArticuloVenta'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_articulo_vta));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ArticuloVenta('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ArticuloVenta']))
			$model->attributes=$_GET['ArticuloVenta'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ArticuloVenta('search');
        $this->pageTitle = 'Administrando '.ArticuloVenta::model()->getAttributeLabel('models');

        $model->unsetAttributes();  // clear any default values
		if(isset($_GET['ArticuloVenta'])){
			$model->attributes=$_GET['ArticuloVenta'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Manages all models.
     */
    public function actionAdminModificarPrecios()
    {
        $model=new ArticuloVenta('search');
        $this->pageTitle = 'Administrando Precios de '.ArticuloVenta::model()->getAttributeLabel('models');


        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['ArticuloVenta'])){
            $model->attributes=$_GET['ArticuloVenta'];
        }else{

        }

        $this->render('adminModificarPrecios',array(
            'model'=>$model,
        ));
    }


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ArticuloVenta the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ArticuloVenta::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ArticuloVenta $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='articulo-venta-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
