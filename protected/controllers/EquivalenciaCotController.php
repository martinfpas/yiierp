<?php

class EquivalenciaCotController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ImportarDatosCsv'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionImportarDatosCsv() {
        $fp = fopen('/var/www/erca/upload/equivalenciaCot.csv', 'r');

        $ix = 0;
        while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
            $ix++;
            if(isset($line[0]) && isset($line[1]) && isset($line[2]) && isset($line[3])){
                $auxArt = VArticuloVenta::model()->findByAttributes(array('id_rubro'=>$line[0],'id_articulo'=>$line[1],'id_presentacion'=>$line[2] ));
                if($auxArt != null){
                    $oEquivalenciaCot = new EquivalenciaCot();
                    $oEquivalenciaCot->idArticuloVenta = $auxArt->id_articulo_vta;
                    $oEquivalenciaCot->codCot = $line[3];
                    $oEquivalenciaCot->descripcion = $line[4];
                    if ($oEquivalenciaCot->save()){
                        echo '<br> ok:: '.$ix;
                    }else{
                        echo '<br>error al guardar: '.$ix.' >'.CHtml::errorSummary($oEquivalenciaCot);
                    }
                }else{
                    echo '<br>$auxArt es null :'.$ix;
                    echo '<pre>';
                    print_r($line);
                    echo '</pre>';
                }
            }else{
                echo '<pre>'.$ix;
                print_r($line);
                echo '</pre>';
            }

        }

    }
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver EquivalenciaCot #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new EquivalenciaCot;
		$this->pageTitle = "Nuevo EquivalenciaCot";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EquivalenciaCot']))
		{
			$model->attributes=$_POST['EquivalenciaCot'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new EquivalenciaCot;
		$this->pageTitle = "Nuevo EquivalenciaCot";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EquivalenciaCot']))
		{
			$model->attributes=$_POST['EquivalenciaCot'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando EquivalenciaCot #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['EquivalenciaCot']))
		{
			$model->attributes=$_POST['EquivalenciaCot'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new EquivalenciaCot('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EquivalenciaCot']))
			$model->attributes=$_GET['EquivalenciaCot'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new EquivalenciaCot('search');
		$this->pageTitle = 'Administrando Equivalencia Cots ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EquivalenciaCot'])){
			$model->attributes=$_GET['EquivalenciaCot'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return EquivalenciaCot the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=EquivalenciaCot::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param EquivalenciaCot $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='equivalencia-cot-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
