<?php

class ProdCargaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Recalcular'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$this->pageTitle = 'Ver ProdCarga #'.$id;
		$criteria = new CDbCriteria;
		$criteria->with = 'oNotaPedido';
		$criteria->addCondition('oNotaPedido.idCarga ='.$model->idCarga);
		$criteria->addCondition('t.idArtVenta ='.$model->idArtVenta); 
		$criteria->addCondition('t.idPaquete ='.$model->idPaquete); 
		$criteria->order = 'oNotaPedido.idCarga';

		$aProdNotas = ProdNotaPedido::model()->findAll($criteria);
		
		$this->render('view',array(
			'model'=>$model,
			'aProdNotas' => $aProdNotas,
		));
	}

	public function actionRecalcular($id){
		$model = $this->loadModel($id);
		$criteria = new CDbCriteria;
		$criteria->with = 'oNotaPedido';
		$criteria->addCondition('oNotaPedido.idCarga ='.$model->idCarga);
		$criteria->addCondition('t.idArtVenta ='.$model->idArtVenta); 
		$criteria->addCondition('t.idPaquete ='.$model->idPaquete); 
		$criteria->order = 'oNotaPedido.idCarga';

		$cantidad = 0;
		$aProdNotas = ProdNotaPedido::model()->findAll($criteria);
		foreach($aProdNotas as $key => $oProdNota){
			$cantidad += $oProdNota->cantidad;
		}

		if($cantidad == 0){
			$model->delete();
			echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
			die();
		}

		$model->cantidad = $cantidad;
		if(!$model->save()){
			throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
		}else{
			$this->redirect(array('view','id'=>$model->id));
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProdCarga;
		$this->pageTitle = "Nuevo ProdCarga";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdCarga']))
		{
			$model->attributes=$_POST['ProdCarga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ProdCarga;
		$this->pageTitle = "Nuevo ProdCarga";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdCarga']))
		{
			$model->attributes=$_POST['ProdCarga'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando ProdCarga #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProdCarga']))
		{
			$model->attributes=$_POST['ProdCarga'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ProdCarga('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdCarga']))
			$model->attributes=$_GET['ProdCarga'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProdCarga('search');
		$this->pageTitle = 'Administrando Prod Cargas ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProdCarga'])){
			$model->attributes=$_GET['ProdCarga'];
		}else{
			$model->activo = ProdCarga::iActivo;
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProdCarga the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProdCarga::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ProdCarga $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='prod-carga-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
