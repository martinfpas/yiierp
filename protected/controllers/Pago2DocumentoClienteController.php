<?php

class Pago2DocumentoClienteController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','SaveFieldAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Pago2DocumentoCliente #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Pago2DocumentoCliente;
		$this->pageTitle = "Nuevo Pago2DocumentoCliente";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pago2DocumentoCliente']))
		{
			$model->attributes=$_POST['Pago2DocumentoCliente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Pago2DocumentoCliente;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pago2DocumentoCliente']))
		{
			$model->attributes=$_POST['Pago2DocumentoCliente'];
			if($model->save()){
				echo $model->id;
				die();
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		if(isset($_POST['comprobantes']) && isset($_POST['idPago']))
		{
			$aLista = explode(',',$_POST['comprobantes']);
			//TODO: implementar transaccion
			ComponentesComunes::print_array($aLista);
			foreach($aLista as $key => $value){
				$model=new Pago2DocumentoCliente;
				$aComp = explode('_',$value);
				//TODO: HANDLE POSIBLE ERROR
				if(!isset($aComp[0])){
					throw new CHttpException('500','<span class="errorSpan">Error de formato '.$value.'</span>');
				}
				switch ($aComp[0]){
					case 'F':
						$oComprobante = Factura::model()->findByPk($aComp[1]);
						$model->idComprobante = $aComp[1];
					break;
					case 'NE':
						$oComprobante = NotaEntrega::model()->findByPk($aComp[1]);
						$model->idNotaEntrega = $aComp[1];
					break;
					case 'ND':
						$oComprobante = NotaDebito::model()->findByPk($aComp[1]);
						$model->idComprobante = $aComp[1];
					break;
				}

				if($oComprobante == null){
					throw new CHttpException('500','<span class="errorSpan">Error: comprobante no encontrado '.$value.'</span>');
				}
				
				$model->idPago = $_POST['idPago'];
				$model->montoSaldado = $oComprobante->MontoASaldar;
				if(!$model->save()){
					//TODO: HACER ROLLBACK
					throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
				}
				$oComprobante->montoPagado += $model->montoSaldado;
				if(!$oComprobante->save()){
					//TODO: HACER ROLLBACK
					throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($oComprobante));
				}
				ComponentesComunes::print_array($oComprobante->attributes);

			}
			die('.');
		}
		
		throw new CHttpException('500','Request Error');

	}

	public function actionSaveFieldAsync(){
        /* @var $oNotaPedido NotaPedido */
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oPago2DocumentoCliente = new Pago2DocumentoCliente($_POST['scenario']);

        $oPago2DocumentoCliente = Pago2DocumentoCliente::model()->findByPk($_POST['pk']);
        if ($oPago2DocumentoCliente != null) {
			if($_POST['name'] == 'montoSaldado'){
                $_POST['value'] = str_replace('$','',$_POST['value']);
            }
			$oPago2DocumentoCliente->setAttribute($_POST['name'],$_POST['value']);
            if (!$oPago2DocumentoCliente->save()) {
                throw new CHttpException('500','Error'.CHtml::errorSummary($oPago2DocumentoCliente));
            }else{

            }
        }else {
            throw new CHttpException('500','No es un '.Pago2DocumentoCliente::model()->getAttributeLabel('model').' existente');
        }
        echo $_POST['value'];
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Pago2DocumentoCliente #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pago2DocumentoCliente']))
		{
			$model->attributes=$_POST['Pago2DocumentoCliente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Pago2DocumentoCliente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pago2DocumentoCliente']))
			$model->attributes=$_GET['Pago2DocumentoCliente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Pago2DocumentoCliente('search');
		$this->pageTitle = 'Administrando Pago2 Documento Clientes ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pago2DocumentoCliente'])){
			$model->attributes=$_GET['Pago2DocumentoCliente'];
		}else{
			$model->activo = Pago2DocumentoCliente::iActivo;
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pago2DocumentoCliente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pago2DocumentoCliente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pago2DocumentoCliente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pago2-documento-cliente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
