<?php

/**
 * This is the model class for table "cargaDocumentos".
 *
 * The followings are the available columns in table 'cargaDocumentos':
 * @property integer $id
 * @property integer $idEntregaDoc
 * @property string $fechaCarga
 * @property string $tipoDoc
 * @property integer $idComprobante
 * @property string $razonSocial
 * @property double $totalFinal
 * @property double $pagado
 * @property double $efectivo
 * @property double $cheque
 * @property double $devolucion
 * @property integer $orden
 * @property double $notaCredito
 * @property string $origen
 * @property integer $comision
 *
 * The followings are the available model relations:
 * @property EntregaDoc $oCargaDoc
 * @property EntregaDoc $oEntregaDoc
 * @property EntregaGtos[] $aEntregaGtos
 */

class CargaDocumentos extends CActiveRecord
{
    const NE = 'NE';
    const FAC = 'FAC';
    const NC = 'NC';
    const ND = 'ND';

    public static $aTipoDoc = array(
        self::NE => self::NE,
        self::FAC => self::FAC,
        self::NC => self::NC,
        self::ND => self::ND,
    );

    const iSi = 1;
    const iNo = 0;

    public static $aComision = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

    public static $aSiNo = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

    public $Recibida;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CargaDocumentos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cargaDocumentos';
	}

	/**
	 * @return array validation rules for model attributes.
    */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEntregaDoc, fechaCarga, tipoDoc, idComprobante, totalFinal, pagado, efectivo, cheque, devolucion, orden, notaCredito, origen, comision', 'required'),
			array('idEntregaDoc, idComprobante, orden, comision', 'numerical', 'integerOnly'=>true),
			array('totalFinal, pagado, efectivo, cheque, devolucion, notaCredito', 'numerical'),
			array('tipoDoc', 'length', 'max'=>4),
			array('origen', 'length', 'max'=>8),
            array('razonSocial', 'length', 'max'=>40),
			array('Recibida','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idEntregaDoc, fechaCarga, tipoDoc, idComprobante, razonSocial, totalFinal, pagado, efectivo, cheque, devolucion, orden, notaCredito, origen, comision', 'safe', 'on'=>'search'),
		);
	}


    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCargaDoc' => array(self::BELONGS_TO, 'EntregaDoc', 'idEntregaDoc'),
            'oEntregaDoc' => array(self::BELONGS_TO, 'EntregaDoc', 'idEntregaDoc'),
			'aEntregaGtos' => array(self::HAS_MANY, 'EntregaGtos', 'idEntregaDoc'),
            'oNotaEntrega' => array(
                self::BELONGS_TO,
                'NotaEntrega',
                'idComprobante',
            ),

            'oFactura' => array(
                self::BELONGS_TO,
                'Factura',
                'idComprobante',
                /*
                'on' => 't.tipoDoc = :type',
                'params' => array(':type' => self::FAC)
                */
            ),
            'oComprobante' => array(
                self::BELONGS_TO,
                'Comprobante',
                'idComprobante',
                /*
                'on' => 't.tipoDoc = :type',
                'params' => array(':type' => self::FAC)
                */
            ),

/*
            'oFactura' => array(self::BELONGS_TO,'Factura','id','condition' => 'cargaDocumentos.idComprobante = Factura.id AND cargaDocumentos.tipoDoc ="FAC"',), //NE, FAC, NC, ND
            'oNotaCredito' => array(self::BELONGS_TO,'NotaCredito','id','on' => 'idComprobante="NC"'),
            //'oNotaDebito' => array(self::BELONGS_TO,'Factura','id','on' => 'idComprobante="ND"'),
            'oNotaEntrega' => array(self::BELONGS_TO,'NotaEntrega','id','on' => 'idComprobante="NE"'),
*/
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
                'id' => Yii::t('application', 'ID'),
                'idEntregaDoc' => Yii::t('application', 'Id Carga Doc'),
                'fechaCarga' => Yii::t('application', 'Fecha Carga'),
                'tipoDoc' => Yii::t('application', 'Tipo Doc'),
                'idComprobante' => Yii::t('application', 'Id Comprobante'),
                'pagado' => Yii::t('application', 'Pagado'),
                'efectivo' => Yii::t('application', 'Efectivo'),
                'cheque' => Yii::t('application', 'Cheque'),
                'devolucion' => Yii::t('application', 'Devolucion'),
                'orden' => Yii::t('application', 'Orden'),
                'notaCredito' => Yii::t('application', 'Nota Credito'),
                'origen' => Yii::t('application', 'Origen'),
                'comision' => Yii::t('application', 'Comision'),
                'totalFinal' => Yii::t('application', 'Total Final'),
                'razonSocial' => Yii::t('application', 'Razon Social'),
		    );
	}

	/**
     * @return string
     */
    public function getRazonSocial()
    {
        if($this->razonSocial == null || $this->razonSocial == ''){
            if($this->tipoDoc == self::NE){
                //$this->razonSocial = $this->oNotaEntrega->oCliente->razonSocial;
                $this->razonSocial = $this->oNotaEntrega->oCliente->title;
            }else{
                $this->razonSocial = $this->oComprobante->oCliente->title;
            }
            if(!$this->save()){
                Yii::log(''.CHtml::errorSummary($this),'error');
            }
        }

        return $this->razonSocial;
    }


	/* @var $oCliente Cliente */
	public function getMontoPasibleComision(){
	    $fMonto = 0;
	    if(!$this->comision){
            return 0;
        }
        if(true){
            if ($this->tipoDoc != self::NE){
                //if ($this->tipoDoc != self::NE){

                //if($this->oFactura->clase == Factura::$aClase['A']){
                if($this->oComprobante->clase == Factura::$aClase['A']){
                    //$entregado = $this->oComprobante->SubTotalGravado - ( $this->devolucion /1.21);
                    $entregado = $this->oComprobante->subtotal - ( $this->devolucion /1.21);
                    $fMonto = $entregado ;

                }else{ //ES CLASE B U OTRO

                    $oCliente = $this->oComprobante->oCliente;
                    if($oCliente->id_rubro == 10){ // ES UNA ESCUELA
                        $entregado = ( $this->totalFinal -  $this->devolucion) /1.315;
                    }else{
                        $entregado = ( $this->totalFinal -  $this->devolucion) /1.315;
                        //$entregado = ( $this->totalFinal -  $this->devolucion) ;
                    }

                    $fMonto = $entregado;

                }
            }else{ // ES NOTA DE ENTREGA
                $oCliente = $this->oNotaEntrega->oCliente;
                if ($oCliente != null){
                    if($oCliente->esInstitucion()){ // ES UNA ESCUELA
                        $entregado = ($this->totalFinal - $this->devolucion) / 1.315;
                    }else{
                        $entregado = ($this->totalFinal - $this->devolucion);

                    }

                    //$fMonto = ($netoCobrado > $entregado)? $entregado : $netoCobrado;
                    $fMonto = $entregado;
                }else{
                    throw new CException('El cliente no existe');
                }
            }
        }

        if ($this->tipoDoc == self::NC){
            $fMonto =  -1 * abs($fMonto);
            //$fMonto =  0;
        }

        return number_format($fMonto,2,'.','');
    }

	public function getCliente(){
	    if($this->tipoDoc == self::NE){
	        if($this->oNotaEntrega != null){
                return $this->oNotaEntrega->oCliente;
            }
        }else{
            return $this->oComprobante->oCliente;
        }
        return '';
    }


    public function getOPagoCliente(){
	    if($this->tipoDoc == self::NE){
	    	if(!$this->oNotaEntrega){
	    		throw new CHttpException(404,print_r($this->attributes,true));
	    	}
	        return $this->oNotaEntrega->oPagoCliente;
        }else{
            return $this->oComprobante->oPagoCliente;
        }
    }

    public function getDocumento(){
        if($this->tipoDoc == self::NE){
            return $this->oNotaEntrega;
        }else{
            return $this->oComprobante;
        }
    }

    public function crearPago($tipo,$monto){

        $oPagoCliente = new PagoCliente();
        $oPagoCliente->idCliente = $this->Cliente->id;
        $oPagoCliente->monto = $_POST['value'];
        $oPagoCliente->fecha = date("Y-m-d");
        if($this->tipoDoc == self::NE){
            $oPagoCliente->idNotaEntrega = $this->idComprobante;
        }else{
            $oPagoCliente->id_factura = $this->idComprobante;
        }
        if (!$oPagoCliente->save()){
            Yii::log('Error de creacion PagoCLiente'.CHtml::errorSummary($oPagoCliente),'error');
        }else{
            if ($tipo == 'efectivo'){
                $oPagoContado = new PagoAlContado();
                $oPagoContado->monto = $monto;
                $oPagoContado->idPagodeCliente = $oPagoCliente->id;
                if (!$oPagoContado->save()){
                    Yii::log('Error de carga de Pago COntado'.CHtml::errorSummary($oPagoContado),'error');
                }
            }

        }
    }

    /** @var $oPagoCliente PagoCliente */
    public function recalcular($oPagoCliente = null){


        if($oPagoCliente == null){
            $oPagoCliente = $this->getOPagoCliente();
        }
        //SI NINGUNO DE LOS PAGOS DE CLIENTE ES OBJETO
        if($oPagoCliente != null){
            $oPagoCliente->refresh();
            //Yii::log('recalcular cargaDoc :: PAGO.'.$oPagoCliente->id,'warning');
            $this->efectivo = $oPagoCliente->getMontoEfectivo();
            //Yii::log('recalcular cargaDoc :: efectivo=>'.$this->efectivo,'warning');
            $this->cheque = $oPagoCliente->getMontoCheque();
        }

        $sub = ($this->totalFinal - $this->devolucion - $this->efectivo - $this->cheque - $this->notaCredito);

        $this->pagado = ($sub <= 0.01)? 1 : 0;

        Yii::log('recalcular: '.$this->id.' Sub :'.$sub,'warning');

        if (!$this->save()){
            Yii::log('CHtml::errorSummary($this) '.CHtml::errorSummary($this),'error');
        }

    }

    public function esEditable(){
        return ($this->oEntregaDoc != null && $this->oEntregaDoc->esEditable());
    }



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idEntregaDoc',$this->idEntregaDoc);

        if ($this->fechaCarga != null){
            $criteria->compare('fechaCarga',ComponentesComunes::fechaComparada($this->fechaCarga));
        }

		$criteria->compare('tipoDoc',$this->tipoDoc,true);
		$criteria->compare('idComprobante',$this->idComprobante);
		$criteria->compare('totalFinal',$this->totalFinal);
		$criteria->compare('pagado',$this->pagado);
		$criteria->compare('efectivo',$this->efectivo);
		$criteria->compare('cheque',$this->cheque);
		$criteria->compare('devolucion',$this->devolucion);
		$criteria->compare('orden',$this->orden);
		$criteria->compare('notaCredito',$this->notaCredito);
		$criteria->compare('origen',$this->origen,true);
		$criteria->compare('comision',$this->comision);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}



    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchWP()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oComprobante');
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.idEntregaDoc',$this->idEntregaDoc);
        //$criteria->select = '*,count(oComprobante.Id_Cliente) as veces';

        if ($this->fechaCarga != null){
            $criteria->compare('t.fechaCarga',ComponentesComunes::fechaComparada($this->fechaCarga));
        }

        $criteria->compare('t.tipoDoc',$this->tipoDoc,true);

        $criteria->compare('t.idComprobante',$this->idComprobante);
        $criteria->compare('t.totalFinal',$this->totalFinal);
        $criteria->compare('t.pagado',$this->pagado);
        $criteria->compare('t.efectivo',$this->efectivo);
        $criteria->compare('t.cheque',$this->cheque);
        $criteria->compare('t.devolucion',$this->devolucion);
        $criteria->compare('t.orden',$this->orden);
        $criteria->compare('t.notaCredito',$this->notaCredito);
        $criteria->compare('t.origen',$this->origen,true);
        $criteria->compare('t.comision',$this->comision);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
              'defaultOrder' => 't.orden,t.razonSocial ',
            ),
        ));
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchIn($aIdCargaDocumentos)
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oComprobante');


        $criteria->compare('idEntregaDoc',$this->idEntregaDoc);
        //$criteria->select = '*,count(oComprobante.Id_Cliente) as veces';

        if ($this->fechaCarga != null){
            $criteria->compare('fechaCarga',ComponentesComunes::fechaComparada($this->fechaCarga));
        }

        $criteria->compare('tipoDoc',$this->tipoDoc,true);

        $criteria->compare('idComprobante',$this->idComprobante);
        $criteria->compare('totalFinal',$this->totalFinal);
        $criteria->compare('pagado',$this->pagado);
        $criteria->compare('efectivo',$this->efectivo);
        $criteria->compare('cheque',$this->cheque);
        $criteria->compare('devolucion',$this->devolucion);
        $criteria->compare('orden',$this->orden);
        $criteria->compare('notaCredito',$this->notaCredito);
        $criteria->compare('origen',$this->origen,true);
        $criteria->compare('comision',$this->comision);
        $criteria->addInCondition('t.id',$aIdCargaDocumentos);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.orden,t.razonSocial ',
            ),
        ));
    }


    public function beforeSave(){
        /* @var $oCliente Cliente */
        $oCliente = $this->getCliente();
        if($this->razonSocial == null || $this->razonSocial == ''){
            if($this->tipoDoc == self::NE){
                $this->razonSocial = $this->oNotaEntrega->oCliente->razonSocial;
            }else{
                $this->razonSocial = $this->oComprobante->razonSocial;
            }
        }

        if($this->devolucion > 0 && $this->devolucion >= $this->totalFinal){
            $this->pagado = 0;
        }
        return parent::beforeSave();
    }


}