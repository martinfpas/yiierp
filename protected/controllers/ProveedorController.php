<?php

class ProveedorController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync', 'Listado','ImportarDatosCsv','select2',
                    'ViewFicha','PdfCarta','ViewPdfCarta'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionImportarDatosCsv() {


        $fp = fopen('/var/www/erca/upload/Proveedor.csv', 'r');
        if($fp)
        {
            //	$line = fgetcsv($fp, 1000, ",");
            //	print_r($line); exit;

            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;
                echo '<br>Procesando indice '.$ix.' * >'.$line[0];
                /*
                echo '<br>';
                ComponentesComunes::print_array($line);
                die;
                */
                $oProveedor = Proveedor::model()->findByPk($line[0]);

                if ($oProveedor == null){
                    $oProveedor = new Proveedor();
                    $oProveedor->id = $line[0];
                }

                $oProveedor->cp = $line[1];
                $oProveedor->zp = $line[2];
                $oProveedor->direccion = utf8_encode($line[3]);
                $oProveedor->nbreFantasia = utf8_encode($line[4]);
                $oProveedor->contacto = utf8_encode($line[5]);
                $oProveedor->cuit = $line[6];
                $oProveedor->tel = utf8_encode($line[7]);
                if(strlen($line[8] )> 8){
                    $line[11] .= utf8_encode($line[8]);
                }else{
                    $oProveedor->pisoDto = utf8_encode($line[8]);
                }

                $oProveedor->email = utf8_encode($line[9]);
                $oProveedor->fax = utf8_encode($line[10]);
                $oProveedor->obs = utf8_encode($line[11]);
                if ($line[12] <> ''){
                    $oProveedor->razonSocial = utf8_encode($line[12]);
                }else{
                    $oProveedor->razonSocial = '.';
                }

                $oProveedor->idTipo = $line[13];
                $oProveedor->ivaResponsable = ($line[14] != '')? $line[14] : 5;
                $oProveedor->saldo = $line[15];

                if (!$oProveedor->save()) {
                    echo '<br> NO se guardo:: '.$line[0].':: el indice '.$line[0].'-'.$line[3].'<br>';
                    ComponentesComunes::print_array($oProveedor->attributes);
                    print_r($oProveedor->getErrors());
                }else{
                    if ($oProveedor->isNewRecord){
                        echo ' Nuevo Registro !! :: ';
                    }else{
                        echo ' SE actualizo :: '.$line[0].':: ';
                    }
                }

            };

        }
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionViewFicha($id)
    {
        $this->renderPartial('viewFicha',array(
            'model'=>$this->loadModel($id),

        ),false,true);
    }

    public function actionViewPdfCarta($id,$print=false){

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //$mPDF1 = Yii::app()->ePdf->mpdf('', [235,120],'12','couriernew', 10,0,5,5,5,0,'L');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 25,7,15,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 25,7,15,25,8,0,'P');
            //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','10','couriernew' , 15,15,5,5,5,0,'P');
        }

        $model = $this->loadModel($id);
        if (isset($_GET['Proveedor']['atencionSobre'])){
            $model->atencionSobre   = $_GET['Proveedor']['atencionSobre'];
            $model->sLocalidad      = $_GET['Proveedor']['sLocalidad'];
            $model->sProvincia      = $_GET['Proveedor']['sProvincia'];
            $model->attributes      = $_GET['Proveedor'];
        }
        if (isset($_POST['Proveedor']['atencionSobre'])){
            $model->atencionSobre   = $_POST['Proveedor']['atencionSobre'];
            $model->sLocalidad      = $_POST['Proveedor']['sLocalidad'];
            $model->sProvincia      = $_POST['Proveedor']['sProvincia'];
            $model->attributes      = $_POST['Proveedor'];
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('ViewPdfCarta',array(
                'model'=>$model,
            ),true)
        );


        $filename = 'SobreCartaProv_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage,'235mmx120mm');

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
    }

    public function actionPdfCarta($id){
        $model = $this->loadModel($id);

        $model->sLocalidad = $model->Localidad;
        $model->sProvincia = $model->Provincia;

        $this->render('formCarta',array(
                'model' => $model)
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Proveedor #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionSelect2(){

        $criteria = new CDbCriteria();
        if(ComponentesComunes::validatesAsInt($_GET['q'])){
            $criteria->compare('id',$_GET['q']);
        }else{
            $criteria->compare('razonSocial',$_GET['q'],true);
            $criteria->limit = $_GET['page_limit'];
            //$criteria->offset = $_GET['page'] * $_GET['page_limit'];
        }

        $models = Proveedor::model()->todos()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id,'text' => $oModel->id.' :: '.$oModel->Title);
        }

        echo CJSON::encode($aModels);
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

    public function actionListado()
    {
        ini_set("memory_limit","128M");
        $mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'12','couriernew', 5,5,15,5,5,0,'P');
        $model=new Proveedor('search');
        $model->unsetAttributes();
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        $this->layout = 'column1Pdf';

        $mPDF1->SetHeader('{PAGENO}{nbpg}');

        $mPDF1->WriteHTML($this->renderPartial('lista',array(
                'model'=>$model,
            ),true)
        );
        $mPDF1->PackTableData=true;
        $mPDF1->Output('ListadoProveedor.pdf','I');



    }
	public function actionCreate()
	{
		$model=new Proveedor;
		$this->pageTitle = "Nuevo Proveedor";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proveedor']))
		{
			$model->attributes=$_POST['Proveedor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Proveedor;
		$this->pageTitle = "Nuevo Proveedor";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proveedor']))
		{
			$model->attributes=$_POST['Proveedor'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Proveedor #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proveedor']))
		{
			$model->attributes=$_POST['Proveedor'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Proveedor('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proveedor']))
			$model->attributes=$_GET['Proveedor'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Proveedor('search');
		$this->pageTitle = 'Administrando Proveedors ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proveedor'])){
			$model->attributes=$_GET['Proveedor'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Proveedor the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Proveedor::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Proveedor $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proveedor-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
