<?php

class VClientesListaController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print/';
    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%">
						    <div style="width: 500px;float: left;border: black solid 1px;">
						        ERCA S.R.L.<br>
						        Ruta Pcial Nº2 Altura 2350 <br>
						        3014 - MONTE VERA - SANTA FE <br> 
						        E-mail: dauby@argentina.com <br> 
						        Tel.:0342 - 4904299
						        </div></td>
						<td width="42%" align="center"></td>
						<td width="15%" style="text-align: left;">
						    Página: {PAGENO}/{nbpg} <br>
						    Fecha: {DATE d-m-Y}
						</td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ViewPdf','AdminConSaldo','ViewPdfConSaldo'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.VClientesLista::model()->getAttributeLabel('model').' #'.$id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new VClientesLista;
		$this->pageTitle = "Nuevo ".VClientesLista::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VClientesLista']))
		{
			$model->attributes=$_POST['VClientesLista'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new VClientesLista;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VClientesLista']))
		{
			$model->attributes=$_POST['VClientesLista'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.VClientesLista::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['VClientesLista']))
		{
			$model->attributes=$_POST['VClientesLista'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new VClientesLista('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VClientesLista']))
			$model->attributes=$_GET['VClientesLista'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new VClientesLista('search');
		$this->pageTitle = 'Administrando '.VClientesLista::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VClientesLista'])){
			$model->attributes=$_GET['VClientesLista'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	
	/**
	 * Manages all models.
	 */
	public function actionAdminConSaldo()
	{
		$model=new VClientesLista('search');
		$this->pageTitle = 'Administrando '.VClientesLista::model()->getAttributeLabel('models').' con saldo';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['VClientesLista'])){
			$model->attributes=$_GET['VClientesLista'];
		}else{
			
		}	

		$this->render('adminConSaldo',array(
			'model'=>$model,
		));
	}

    /**
     * Manages all models. 
     */
    public function actionViewPdf($print=false)
    {
        $model=new VClientesLista('search');

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','8','couriernew', 15,7,30,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','8','couriernew', 15,7,25,25,0,0,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $this->layout = 'column1Pdf';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['VClientesLista'])){
            $model->attributes=$_GET['VClientesLista'];
        }else{

        }

        $mPDF1->adjustFontDescLineheight = 1;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedTextBaseline = false;

        $mPDF1->WriteHTML($this->renderPartial('viewPdf',array(
            'model'=>$model,
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'ListaClientes_'.(($model->oProvincia != null)? $model->oProvincia->nombre : date('d-m-Y')).'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

	}
	
	    /**
     * Manages all models. viewPdfConSaldo
     */
    public function actionViewPdfConSaldo($print=false)
    {
        $model=new VClientesLista('search');

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 5,5,25,5,5,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 10,7,25,25,0,0,'P');
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $this->layout = 'column1Pdf';

        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['VClientesLista'])){
            $model->attributes=$_GET['VClientesLista'];
        }else{

        }

        $mPDF1->adjustFontDescLineheight = 1;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedNormalLineHeight = false;
        $mPDF1->useFixedTextBaseline = false;

        $mPDF1->WriteHTML($this->renderPartial('viewPdfConSaldo',array(
            'model'=>$model,
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'ListaClientes_'.(($model->oProvincia != null)? $model->oProvincia->nombre : date('d-m-Y')).'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage);

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return VClientesLista the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=VClientesLista::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param VClientesLista $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='vclientes-lista-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
