<?php

class AfipVoucherController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Emitir','TestAfip2','TestTributos','consultarAlicuotasIVA','GetUltimoComprobanteAutorizado'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionConsultarAlicuotasIVA(){

        require_once 'wsfev1.php';
        $cuit = Yii::app()->params['cuit'];
        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);

        $result = $afip->init(); //Crea el cliente SOAP
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->consultarAlicuotasIVA();
            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo

                //ComponentesComunes::print_array($result);

                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar
            } else {
                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo consultarUltimoComprobanteAutorizado :: '.$result["msg"];
            }
        } else {
            echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo
        }
    }

	public function actionEmitir($id) {


        $oComprobante = Comprobante::model()->findByPk($id);

        $idVoucher = $oComprobante->getIdAfipVoucher();

	    $voucher = AfipVoucher::model()->with('Tributos','subtotivas')->findByPk($idVoucher);

	    if ($voucher != null){
            try {
                $voucher->emitir();
            }catch (Exception $e){
                throw new CHttpException('500','<span class="errorSpan">'.$e->getMessage().' =>'.$e->getCode().'</span>');
            }
        }else{
            throw new CHttpException('500','<span class="errorSpan">Error Al crear el voucher '.$idVoucher.'</span>');
        }

	}


    /*
	public function getUltimoComprobanteAutorizado($PV, $TC) {
        Yii::log('wsfev1.php 2222','warning');

        require_once 'wsfev1.php';


        $cuit = Yii::app()->params['cuit'];
        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] );



        $result = $afip->init(); //Crea el cliente SOAP



        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->consultarUltimoComprobanteAutorizado($PV, $TC); //$voucher debe tener la estructura esperada (ver a continuaci�n de la wiki)
            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo

                return ($result);

                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar
            } else {
                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo consultarUltimoComprobanteAutorizado :: '.$result["msg"];
            }
        } else {
            echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo
        }
    }
    */

    public function actionGetUltimoComprobanteAutorizado($PV, $TC) {
        //echo Yii::app()->params['modoAfip'].json_encode($this->getUltimoComprobanteAutorizado($PV, $TC));
        echo json_encode(AfipVoucher::getUltimoComprobanteAutorizado($PV, $TC));
    }

    public function actionTestTributos() {
        require_once 'wsfev1.php';

        $cuit = Yii::app()->params['cuit'];

        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);

        $result = $afip->init(); //Crea el cliente SOAP

        if ($result["code"] === Wsfev1::RESULT_OK) {

            $result = $afip->consultarTiposTributos(); //$voucher debe tener la estructura esperada (ver a continuaci�n de la wiki)

            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo
                echo '<pre>';
                print_r($result);
                echo '<pre>';

                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar

            } else {

                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo consultarTiposTributos :: '.$result["msg"];

            }

        } else {

            echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo

        }

    }

    public function actionTestAfip2($id) {
        $voucher = AfipVoucher::model()->with('Tributos','subtotivas','aAfipVoucherItems')->findByPk($id);
        require_once 'wsfev1.php';
        /* 		$voucher = $this->convertModelToArray($oVoucher);
                echo '<br><pre>';
                print_r($voucher);
                echo '</pre>';
                */

        $cuit = Yii::app()->params['cuit'];//'20285260427';

        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);

        $result = $afip->init(); //Crea el cliente SOAP

        if ($result["code"] === Wsfev1::RESULT_OK) {

            $result = $afip->emitirComprobante($voucher); //$voucher debe tener la estructura esperada (ver a continuaci�n de la wiki)

            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo correctamente

                Yii::log('Wsfev1::RESULT_OK :: $result["cae"] => '.$result["cae"],'warning');
                $voucher->caea = $result["cae"];
                $voucher->fechaVencimientoCAE = $result['fechaVencimientoCAE'];
                $voucher->save();
                echo json_encode($voucher->attributes);

                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar

            } else {

                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo emitirse la factura :: '.$result["msg"];

            }

        } else {

            echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo

        }

    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver AfipVoucher #'.$id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		require_once 'wsfev1.php';
		$cuit = Yii::app()->params['cuit'];//'20285260427';
		$afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);
		$result = $afip->init(); //Crea el cliente SOAP

		$aTipoDocDatos = array();

		if ($result["code"] === Wsfev1::RESULT_OK) {
			$aResult = $afip->consultarTiposDocumento();
			if ($result["code"] === Wsfev1::RESULT_OK) {
				$aTipoDocDatos = $aResult['datos'];
			}

		}


		$model=new AfipVoucher;
		$this->pageTitle = "Nuevo AfipVoucher";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AfipVoucher']))
		{
			$model->attributes=$_POST['AfipVoucher'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
			'aTipoDocDatos' => $aTipoDocDatos,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new AfipVoucher;
		$this->pageTitle = "Nuevo AfipVoucher";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AfipVoucher']))
		{
			$model->attributes=$_POST['AfipVoucher'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		require_once 'wsfev1.php';
		$cuit =  Yii::app()->params['cuit'];//'20285260427';
		$afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);
		$result = $afip->init(); //Crea el cliente SOAP

		$aTipoDocDatos = array();

		if ($result["code"] === Wsfev1::RESULT_OK) {
			$aResult = $afip->consultarTiposDocumento();
			if ($result["code"] === Wsfev1::RESULT_OK) {
				$aTipoDocDatos = $aResult['datos'];
			}

		}

		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando AfipVoucher #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AfipVoucher']))
		{
			$model->attributes=$_POST['AfipVoucher'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
			'aTipoDocDatos' => array(),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new AfipVoucher('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AfipVoucher']))
			$model->attributes=$_GET['AfipVoucher'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AfipVoucher('search');
		$this->pageTitle = 'Administrando Afip Vouchers ';

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AfipVoucher'])){
			$model->attributes=$_GET['AfipVoucher'];
		}else{

		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AfipVoucher the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AfipVoucher::model()->with('subtotivas','Tributos')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AfipVoucher $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='afip-voucher-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function convertModelToArray($models) {
		if (is_array($models))
			$arrayMode = TRUE;
		else {
			$models = array($models);
			$arrayMode = FALSE;
		}

		$result = array();
		foreach ($models as $model) {
			$attributes = $model->getAttributes();
			$relations = array();
			foreach ($model->relations() as $key => $related) {
				if ($model->hasRelated($key)) {
					if ($related[0] == "CStatRelation")
						$relations[$key] = $model->$key;
					else
						$relations[$key] = $this->convertModelToArray($model->$key);
				}
			}
			$all = array_merge(array_filter($attributes,'count'), array_filter($relations,'count'));

			if ($arrayMode)
				array_push($result, $all);
			else
				$result = $all;
		}
		return $result;
	}
}
