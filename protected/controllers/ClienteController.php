<?php

class ClienteController extends Controller
{
    public $printFilesPath = '/var/www/erca/upload/print';

    public $pdfHeader = '<table width="100%">
					<tr style="">
						<td width="33%" ><div style="width: 500px;float: left;font-size: small;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="33%" style="text-align: right;"></td>
						<td width="33%" align="center">{PAGENO} de {nbpg}</td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ImportarDatosMigra','ImportarDatosMigra2',
                    'ClientesAsync','ViewFicha','GetAsync', 'Listado', 'ListadoDeudores', 'Listado2','PdfCarta','ViewPdfCarta','AdminDocumentosConSaldo',
                    'CtaCtePdf','Conciliar','ConciliarEnCero','ConciliarNegativo','forzarSaldo','RevisarSaldos'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
    }

    public function actionForzarSaldo($id,$value){
        $model = $this->loadModel($id);
        $model->saldoActual = $value;
        if(!$model->save()){
            throw new CHttpException(500,CHtml::errorSummary($model));
        }
        $this->redirect(array('Conciliar','id'=>$model->id));
    }

    public function actionAdminDocumentosConSaldo(){
        $this->layout = "//layouts/column1";

        $model= new VDocumentosConSaldo('search');
        $model->unsetAttributes();
        
        if (isset($_GET['VDocumentosConSaldo'])){
            $model->attributes =$_GET['VDocumentosConSaldo'];
            $model->codViajante =$_GET['VDocumentosConSaldo']['codViajante'];
        }

        $total = 0;
        $aDoc = array();

        $cAnterior = '';
        $acumulado = 0;
        $i = 0;

        $this->render('adminListaDocumentosConSaldo',array(
            'model'=>$model,
        ));
    }
    
    public function actionConciliarOLD(){
        $user = 'phpmyadminuser';
        $pass = 'P@ssw0rd';
        $db = new PDO('mysql:host=localhost;dbname=erca29082020;', $user, $pass);

        //#1         $stmt = $db->prepare("SELECT m.*,c.id as idCliente,c.saldoActual FROM cliente c LEFT JOIN migra_cliente_saldo m on c.id = m.id  where m.id is null and c.saldoActual <> 0 ");
        //#2 
        $stmt = $db->prepare("SELECT m.*,c.id as idCliente,c.saldoActual FROM cliente c LEFT JOIN migra_cliente_saldo m on c.id = m.id where m.id is not null ");

        $stmt->execute();
        $conciliados = $stmt->fetchAll(PDO::FETCH_OBJ);
        $i = 0;
        foreach($conciliados as $key => $oClienteAConciliar){
            /** 
             * @var $oCliente Cliente
             */
            //ComponentesComunes::print_array($oCliente);
            $oCliente = Cliente::model()->findByPk($oClienteAConciliar->idCliente);
            //#1             $oCliente->saldoActual = 0;
            //#2 
            $oCliente->saldoConciliado = $oCliente->saldoActual;
            //#2 
            $oCliente->fechaConciliacion = $oClienteAConciliar->fecha;

            /* 
            if(!$oCliente->save()){
                throw new Exception(CHtml::errorSummary($oCliente));
            }else{
                ComponentesComunes::print_array($oCliente->attributes);
            }
            */
            $i++;
        }
        echo '$i :'.$i; 
    }

    /** @var $aClientes Cliente */
    public function actionConciliarEnCero(){
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('saldoConciliado = 0');
        $aClientes = Cliente::model()->findAll($criteria);
        $i = 0;
        foreach($aClientes as $key => $oCliente){
            //ComponentesComunes::print_array($oCliente->attributes);
            foreach($oCliente->aNotaEntrega as $key => $oNotaEntrega){
                if($oNotaEntrega->totalFinal > $oNotaEntrega->montoPagado && $oNotaEntrega->fecha ){
                    //echo ' > '.sizeof($oCliente->aNotaEntrega).' > '.sizeof($oCliente->aComprobante);
                    echo '<br> NE > '.$oNotaEntrega->fecha.' - '.$oCliente->fechaConciliacion;
                    //ComponentesComunes::print_array($oNotaEntrega->attributes);
                    $oNotaEntrega->montoPagado = $oNotaEntrega->totalFinal;
                    //$oNotaEntrega->save();
                    $i++;
                }
            }
            foreach($oCliente->aComprobante as $key => $oComprobante){
                if($oComprobante->Total_Final > $oComprobante->montoPagado && $oComprobante->tipo == 'F' ){
                    //echo ' > '.sizeof($oCliente->aNotaEntrega).' > '.sizeof($oCliente->aComprobante);
                    echo '<br> oComprobante > '.$oComprobante->fecha.' - '.$oCliente->fechaConciliacion;
                    //ComponentesComunes::print_array( $oComprobante->attributes);
                    $oComprobante->montoPagado = $oComprobante->Total_Final;
                    //$oComprobante->save();
                    $i++;
                }
            }
        }
        echo '$i'.$i;
    }

    
    /** @var $aClientes Cliente */
    public function actionConciliarNegativo(){
        
        $criteria = new CDbCriteria();
        $criteria->addCondition('saldoConciliado > 0');
        $aClientes = Cliente::model()->findAll($criteria);
        foreach($aClientes as $key => $oCliente){
            ComponentesComunes::print_array($oCliente->attributes);
            $criteria2 = new CDbCriteria();
            $criteria2->addCondition('Id_Cliente = '.$oCliente->id);
            $criteria2->order = '`Id_Cliente` ASC';
            $aComprobantesConSaldo = VDocumentosConSaldo::model()->findAll($criteria2);

            foreach($aComprobantesConSaldo as $key => $oComprobante){
                if($oCliente->saldoConciliado >= $oComprobante->saldo){
                    echo '<br>$oCliente->saldoConciliado > $oComprobante->saldo '.$oCliente->saldoConciliado.' > '.$oComprobante->saldo;
                    $oCliente->saldoConciliado -= $oComprobante->saldo;
                }else{
                    if($oCliente->saldoConciliado == 0){
                        echo '<br>Conciliar... '.$oCliente->saldoConciliado.' > '.$oComprobante->saldo;
                        if($oComprobante->tipo == 'NE'){
                            $oNotaEntrega = NotaEntrega::model()->findByPk($oComprobante->id);
                            if($oNotaEntrega != null){
                                $oNotaEntrega->montoPagado = $oNotaEntrega->totalFinal;
                                $oNotaEntrega->save();
                            }else{
                                die('>>>>>>>>'.$oComprobante->id);
                            }
                            
                        }else{
                            $oComprobanteF = Comprobante::model()->findByPk($oComprobante->id);
                            $oComprobanteF->montoPagado = $oComprobanteF->Total_Final;
                            $oComprobanteF->save();
                        }
                    }else{
                        echo '<br> CONCILIAR PARCIAL !! '.$oCliente->saldoConciliado.' > '.$oComprobante->saldo;
                        $oCliente->saldoConciliado = 0;
                        if($oComprobante->tipo == 'NE'){
                            $oNotaEntrega = NotaEntrega::model()->findByPk($oComprobante->id);
                            if($oNotaEntrega != null){
                                $oNotaEntrega->montoPagado = $oNotaEntrega->totalFinal - $oCliente->saldoConciliado;
                                $oNotaEntrega->save();
                            }else{
                                die('>>>>>>>>'.$oComprobante->id);
                            }
                            
                        }else{
                            $oComprobanteF = Comprobante::model()->findByPk($oComprobante->id);
                            $oComprobanteF->montoPagado = $oComprobanteF->Total_Final - $oCliente->saldoConciliado;
                            $oComprobanteF->save();
                        }
                    }
                    
                }
            }

            /*
            foreach($oCliente->aNotaEntrega as $key => $oNotaEntrega){
                if($oNotaEntrega->totalFinal > $oNotaEntrega->montoPagado){
                    //echo ' > '.sizeof($oCliente->aNotaEntrega).' > '.sizeof($oCliente->aComprobante);
                    echo '<br> NE > '.$oNotaEntrega->id;
                    //ComponentesComunes::print_array($oCliente->attributes);
                    $oNotaEntrega->montoPagado = $oNotaEntrega->totalFinal;
                    $oNotaEntrega->save();
                }
            }
            foreach($oCliente->aComprobante as $key => $oComprobante){
                if($oComprobante->Total_Final > $oComprobante->montoPagado && $oComprobante->tipo == 'F'){
                    //echo ' > '.sizeof($oCliente->aNotaEntrega).' > '.sizeof($oCliente->aComprobante);
                    echo '<br> oComprobante > '.$oComprobante->id;
                    ComponentesComunes::print_array($oCliente->attributes);
                    $oComprobante->montoPagado = $oComprobante->Total_Final;
                    $oComprobante->save();
                }
            }
            */
        }
    }

	public function actionCtaCtePdf($id){
        /** @var $item VCtaCteCliente */
        $criteria = new CDbCriteria();
        $criteria->addCondition('clId = '.$id);
        $criteria->order = 'fecha desc';
        if(isset($_GET['VCtaCteCliente']['desde'])){
            $desde = $_GET['VCtaCteCliente']['desde'];
            $criteria->addCondition("t.fecha >= '".ComponentesComunes::jpicker2db($desde)." 00:00:00'");
            $aCliente['desde'] = 'Desde:'.ComponentesComunes::fechaFormateada($desde) ;
        }else{
            $aCliente['desde'] = '';
        }

        if (isset($_GET['VCtaCteCliente']['hasta'])){
            $hasta = $_GET['VCtaCteCliente']['hasta'];
            $aCliente['hasta'] = 'Hasta:'.ComponentesComunes::fechaFormateada($hasta) ;
            $criteria->addCondition("t.fecha <= '".ComponentesComunes::jpicker2db($hasta,false)."  23:59:59'");
        }else{
            $aCliente['hasta'] =  '';
        }

	    $CtaCte = VCtaCteCliente::model()->findAll($criteria);
        $model = $this->loadModel($id);
        $aCliente['titulo'] = $model->getTitle();

        $aCtaCte = array();
        foreach ($CtaCte as $index => $item) {
            $aItem = array();
            $aItem['fecha'] = ComponentesComunes::fechaFormateada($item->fecha);
            $aItem['debe'] = number_format($item->debe,2,',','');
            $aItem['haber'] = number_format($item->haber,2,',','');
            $aItem['tipo'] = $item->TipoFull;
            $aItem['comprobante'] = $item->Title;

            $aCtaCte[] = $aItem;
        }

        /*
        print_r($_GET);
        echo '<br>'.$criteria->condition;
        die();
        */

        $r = new YiiReport(array('template' => 'CtaCte.xlsx'));

        $r->load(array(
            array(
                'id' => 'cliente',
                'repeat' => false,
                'data' => $aCliente,
                'minRows' => 1,
            ),
            array(
                'id' => 'item',
                'repeat' => true,
                'data' => $aCtaCte,
                'minRows' => 1,
            ),
        ));

        echo $r->render('excel2007','ResumenCtaCte_'.$id);
    }



    public function actionGetAsync($idCliente){
        $model = Cliente::model()->findByPk($idCliente);
        if ($model){
            echo $model->Title;
        }else{
            throw new CHttpException('500','<span class="errorSpan">Error: El Cliente no existe</span>');
        }
    }

    public function actionImportarDatosMigra(){
        //$criteria = new CDbCriteria();
        //$criteria->condition = 'codViajante = 14';
        //$aMigraVistaClientes = MigraVCliente::model()->findAll($criteria);
        $aMigraVistaClientes = MigraVCliente::model()->findAll();
        foreach ($aMigraVistaClientes as $index => $oMigraVistaCliente) {
            $oCliente = Cliente::model()->findByPk($oMigraVistaCliente->id);

            if($oCliente != null){
                foreach ($oMigraVistaCliente->attributes as $attribute => $value) {

                    if($attribute != 'id'){
                        if($attribute == 'direccion' && ($value=='' || $value==null)){
                            $value = '-';
                        }elseif ($attribute == 'categoriaIva' && ($value=='' || $value==null)){
                            $value = 5;
                        }elseif ($attribute == 'id_rubro' && $value==0){
                            $value = null;
                        }elseif ($attribute == 'codViajante' && $value==0){
                            $value = null;
                        }elseif ($attribute == 'cuit' && $value==0){
                            $value = null;
                        }

                        $oCliente->setAttribute($attribute,$value);
                    }
                }

                try {
                    if ($oCliente->save()) {
                        echo '<br>'.$oCliente->id;
                        ComponentesComunes::print_array($oCliente->attributes);
                    } else {
                        ComponentesComunes::print_array($oCliente->getErrors());
                        ComponentesComunes::print_array($oCliente->attributes);
                        echo('error');
                    }
                }catch (CDbException $exception){
                    echo ('CDbException :'.$exception->getMessage());
                    ComponentesComunes::print_array($oCliente->attributes);
                }
            }else{
                $oCliente = new Cliente();

                foreach ($oMigraVistaCliente->attributes as $attribute => $value) {
                    if($attribute == 'direccion' && ($value=='' || $value==null)){
                        $value = '-';
                    }elseif ($attribute == 'categoriaIva' && ($value=='' || $value==null)){
                        $value = 5;
                    }elseif ($attribute == 'id_rubro' && $value==0){
                        $value = null;
                    }elseif ($attribute == 'codViajante' && $value==0){
                        $value = null;
                    }elseif ($attribute == 'cuit' && $value==0){
                        $value = null;
                    }

                    $oCliente->setAttribute($attribute,$value);
                }
                try {
                    if ($oCliente->save()) {
                        ComponentesComunes::print_array($oCliente->attributes);
                    } else {
                        ComponentesComunes::print_array($oCliente->getErrors());
                        ComponentesComunes::print_array($oCliente->attributes);
                        echo('error');
                    }
                }catch (CDbException $exception){
                    echo ('CDbException :'.$exception->getMessage());
                    ComponentesComunes::print_array($oCliente->attributes);
                }
            }

        }
    }

    public function actionImportarDatosMigra2(){
        //$criteria = new CDbCriteria();
        //$criteria->condition = 'codViajante = 14';
        //$aMigraVistaClientes = MigraVCliente::model()->findAll($criteria);
        $aMigraVistaClientes = MigraVCliente::model()->findAll();
        foreach ($aMigraVistaClientes as $index => $oMigraVistaCliente) {
            $oCliente = Cliente::model()->findByPk($oMigraVistaCliente->id);

            if($oCliente != null){
                if($oCliente->saldoActual != $oMigraVistaCliente->saldoActual){
                    echo $oCliente->saldoActual.'> <pre>';
                    print_r($oMigraVistaCliente->attributes);
                    echo '</pre>';
                }
            }else{

            }

        }
    }
	/**
	 * Busca los items en forma asincronica.
	 * @param integer $id the ID of the model to be displayed
	 */

	// FUNCIONA PERO NO TIENE FILTRO
    public function actionListadoPDF()
    {
        ini_set("memory_limit","1024M");
        ini_set("pcre.backtrack_limit", "1000000");

        //$mPDF1 = Yii::app()->ePdf->mpdf('', 'A4','8','dejavuserif', 5,5,15,5,5,0,'P');
        //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'12','couriernew', 5,5,20,5,5,0,'P');
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 10,10,5,0,5,0,'P');

        $model=new Cliente('search');
        $model->unsetAttributes();
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        $this->pageTitle = 'Listado de Clientes ';

        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $this->layout = 'column1Pdf';

        $mPDF1->WriteHTML($this->renderPartial('lista',array(
            'model'=>$model,

            ),true)
        );
		$mPDF1->simpleTables = true;
		$mPDF1->PackTableData=true;
        $mPDF1->useSubstitutions = false;
        $mPDF1->Output('Listado.pdf','I');

    }

    public function actionListado()
    {

        //ini_set('max_execution_time', '600');

        $model=new Cliente('search');
        $model->unsetAttributes();
        if (isset($_POST['Cliente'])){
            $model->attributes = $_POST['Cliente'];
        }

        $this->pageTitle = 'Listado de Clientes ';


        //$clientes = Cliente::model()->findAll($model->search()->criteria);
        $criteria = $model->search()->criteria;
        $criteria->limit = 4000;
        $clientes = VClientesLista::model()->findAll($criteria);

        $aClientes = array();

        foreach ($clientes as $index => $cliente) {
            Yii::log('$cliente: '.$cliente->id.' :'.$index,'error');
            //echo '<br>** $cliente: '.$cliente->razonSocial.' :'.$index;
            //ComponentesComunes::print_array($cliente->attributes);
            $aClientes[$index] = $cliente->attributes;
            //$aClientes[$index]['title'] = $cliente->razonSocial;
            $aClientes[$index]['rubro'] = substr($cliente->rubro,0,5);
            //unset($clientes[$index]);
        }

        //die();

        $r = new YiiReport(array('template' => 'ListadoClientesParcial.xlsx'));

        $r->load(array(
            array(
                'id' => 'cliente',
                'repeat' => true,
                'data' => $aClientes,
                'minRows' => 1,
            ),
        ));

        echo $r->render('excel2007','ListadoClienteParcial');

    }


    public function actionListadoDeudores($print=false)
    {
        /* @var $doc VDocumentosYPagos */
        //ini_set("memory_limit","1024M");
        //ini_set("pcre.backtrack_limit", "1000000");

        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                // PERFECTO EN DESARROLLO $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew',12,7,25,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 12,7,25,25,8,0,'P');
        }
        
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $model= new VDocumentosConSaldo('search');
        $model->unsetAttributes();

        if (isset($_GET['VDocumentosConSaldo'])){
            $model->attributes =$_GET['VDocumentosConSaldo'];
            $model->codViajante =$_GET['VDocumentosConSaldo']['codViajante'];
        }


        $total = 0;
        $aDoc = array();

        $cAnterior = '';
        $acumulado = 0;
        $i = 0;

        if($model->tipo == 'F'){
            $mPDF1->WriteHTML($this->renderPartial('listaFacturasConSaldo',array(
                    'model'=>$model,
                ),true)
            );
        }else{
            $mPDF1->WriteHTML($this->renderPartial('listadeudores',array(
                    'model'=>$model,
                ),true)
            );
        }
        $mPDF1->shrink_tables_to_fit=0;

        $filename = 'Listado_comprobantes_'.date('Y-m-d_H-i-s').'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);

            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

    public function actionListadoDeudores2()
    {
        /* @var $doc VDocumentosYPagos */
        //ini_set("memory_limit","1024M");
        //ini_set("pcre.backtrack_limit", "1000000");

        //$this->pageTitle = 'Listado de Clientes con deuda ';

        //ComponentesComunes::print_array($_GET);
        //die();

        $criteria = new CDbCriteria();
        $criteria->addCondition('debe > haber ');
        $criteria->addCondition('fecha > 2020-05-01');
        if (isset($_GET['codViajante'])){
            $criteria->addCondition("codViajante = '".$_GET['codViajante']."'");
        }
        $criteria->order = 'razonSocial asc,fecha asc';
        $models= VDocumentosYPagos::model()->findAll($criteria);

        $r = new YiiReport(array('template' => 'ListadoClientesConDeuda.xlsx'));

        $total = 0;
        $aDoc = array();

        $cAnterior = '';
        $acumulado = 0;
        $i = 0;

        foreach ($models as $index => $doc) {
            $saldo = $doc->debe - $doc->haber;
            $total += ($doc->debe - $doc->haber);

            if($cAnterior == '' || $cAnterior != $doc->clId){
                // SI EL SALDO ES DISTINTO DEL ACUMULADO, HUBO MAS DE UNA FACTURA O NE
                //if($i > 1 && $acumulado <> $aDoc[$i - 1]['saldo']){
                if($i > 1 && $acumulado ){
                    $aDoc[$i]['razonSocial'] = '............................';
                    $aDoc[$i]['nombrelocalidad'] = '................';
                    $aDoc[$i]['saldo'] = '.......';
                    $aDoc[$i]['idCliente'] = '......................';
                    $aDoc[$i]['NRODOC'] = '.......';
                    $aDoc[$i]['fecha'] = '......................';
                    $aDoc[$i]['total'] = '......................';
                    $i++;
                }
                $acumulado = $saldo;
                $cAnterior = $doc->clId;
                $aDoc[$i]['razonSocial'] = $doc->razonSocial;
                $aDoc[$i]['nombrelocalidad'] = $doc->razonSocial;
                $aDoc[$i]['saldo'] = $saldo;
            }else{
                $acumulado += $saldo;
                $aDoc[$i]['razonSocial'] = '';
                $aDoc[$i]['nombrelocalidad'] = '';
                $aDoc[$i]['saldo'] = $acumulado;
            }


            $aDoc[$i]['idCliente'] = $doc->clId;
            $sTitleDoc  = '';
            if ($doc->neId != null){
                $oNotaEntrega = NotaEntrega::model()->findByPk($doc->neId);
                if ($oNotaEntrega != null){
                    $sTitleDoc = 'NE#'.$oNotaEntrega->getFullTitle();
                }
            }else{
                if ($doc->faId){
                    $oFactura = Factura::model()->findByPk($doc->faId);
                    if ($oFactura != null){
                        $sTitleDoc = $oFactura->getFullTitle();
                    }
                }else{
                    $oNotaDebito = NotaDebito::model()->findByPk($doc->ndId);
                    if ($oNotaDebito != null){
                        $sTitleDoc = $oNotaDebito->getFullTitle();
                    }
                }
            }
            $aDoc[$i]['NRODOC'] = $sTitleDoc;
            $aDoc[$i]['fecha'] = $doc->fecha;             

            $aDoc[$i]['total'] = $aDoc[$i]['saldo'];
            $i++;
        }

        $r->load(array(
            array(
                'id' => 'general',
                'data' => array(
                    'total' => number_format($total,2,',','.'),
                ),
            ),
            array(
                'id' => 'doc',
                'data' => $aDoc,
                'repeat' => true,
                'minRows '=> 0
            )
        ));

        echo $r->render('excel2007', 'ListadoClientesConDeuda'.date('dmY'));

    }


/**
	 * Busca los items en forma asincronica.
	 * @param integer $id the ID of the model to be displayed
	 */

    public function actionListado2()
    {
        ini_set("memory_limit","1024M");
        ini_set("pcre.backtrack_limit", "1000000");

        $mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'12','couriernew', 5,5 ,15,5,5,0,'P');

        $mPDF1->SetHTMLHeader('<table width="100%">
					<tr>
						<td width="33%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;">My document</td>
					</tr>
				</table>',null,true);

        $model=new Cliente('search');
        $model->unsetAttributes();
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.min.css');
        //$mPDF1->WriteHTML($stylesheet, 1);
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        $this->pageTitle = 'Listado de Clientes #';


        $this->layout = 'column1Pdf';


        $mPDF1->WriteHTML($this->renderPartial('lista2',array(
				'model'=>$model,

			),true)
        );

		$mPDF1->simpleTables = true;
		$mPDF1->PackTableData=true;
        $mPDF1->useSubstitutions = false;
        $mPDF1->Output('Listado.pdf','I');



    }


	public function actionClientesAsync(){
		$dataProvider = Cliente::model()->searchMultiple($_GET['term']);
		$dataProvider->setPagination(false);

		$data = $dataProvider->getData();

		$arr = array();
		foreach ($data as $item) {
			$arr[] = array(
					'id' => $item->id,
					'value' => $item->razonSocial,
					'label' => $item->id.'-'.$item->razonSocial.' '.$item->nombreFantasia,
			);
		}
		echo CJSON::encode($arr);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionViewFicha($id)
	{
		$this->pageTitle = 'Ver Cliente #'.$id;

        $this->renderPartial('viewFicha',array(
			'model'=>$this->loadModel($id),

		),false,true);
    }

    public function actionViewPdfCarta($id,$print=false){

        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ( $print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 25,7,15,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 25,7,15,25,8,0,'P');
        }

	    $model = $this->loadModel($id);
	    if (isset($_GET['Cliente'])){
	        $model->atencionSobre   = $_GET['Cliente']['atencionSobre'];
            $model->sLocalidad      = $_GET['Cliente']['sLocalidad'];
            $model->sProvincia      = $_GET['Cliente']['sProvincia'];
            $model->attributes      = $_GET['Cliente'];
        }
        if (isset($_POST['Cliente']['atencionSobre'])){
            $model->atencionSobre   = $_POST['Cliente']['atencionSobre'];
            $model->sLocalidad      = $_POST['Cliente']['sLocalidad'];
            $model->sProvincia      = $_POST['Cliente']['sProvincia'];
            $model->attributes      = $_POST['Cliente'];
        }

        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);

        $mPDF1->WriteHTML($this->renderPartial('ViewPdfCarta',array(
            'model'=>$model,
            ),true)
        );

        $filename = 'SobreCarta_'.$id.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);
            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage,'235mmx120mm');

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }
    }

    public function actionPdfCarta($id){
        $model = $this->loadModel($id);

        $model->sLocalidad = $model->Localidad;
        $model->sProvincia = $model->Provincia;

        $this->render('formCarta',array(
            'model' => $model)
        );
    }


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Cliente #'.$id;
        $oCtaCte = new VCtaCteCliente('search');
        $oCtaCte->unsetAttributes();
        if (isset($_GET['VCtaCteCliente'])){
            $oCtaCte->attributes = $_GET['VCtaCteCliente'];
        }
        $oCtaCte->clId = $id;

		$this->render('view',array(
			'model'=>$this->loadModel($id),
            'oCtaCte' => $oCtaCte,
		));
    }
    
    	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionConciliar($id)
	{
        $this->pageTitle = 'Ver Conciliacion Cliente #'.$id;
        $model = $this->loadModel($id);
        $oCtaCte = new VCtaCteCliente('search');
        $oCtaCte->unsetAttributes();
        if (isset($_GET['VCtaCteCliente'])){
            $oCtaCte->attributes = $_GET['VCtaCteCliente'];
        }

        $fecha =  new DateTime($model->fechaConciliacion);
        $fecha->modify('+1 day');
        
        $oCtaCte->desde = $fecha->format('Y-m-d');
        $oCtaCte->clId = $id;

		$this->render('viewConciliar',array(
			'model'=>$model,
            'oCtaCte' => $oCtaCte,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Cliente;
		$this->pageTitle = "Nuevo Cliente";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cliente']))
		{
			$model->attributes=$_POST['Cliente'];
			if($model->save()){
			    $model = Cliente::model()->last()->find();
                $this->redirect(array('view','id'=>$model->id));
            }

		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Cliente;
		$this->pageTitle = "Nuevo Cliente";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cliente']))
		{
			$model->attributes=$_POST['Cliente'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Cliente #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Cliente']))
		{
			$model->attributes=$_POST['Cliente'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	    try {
            $this->loadModel($id)->delete();
        }catch (CDbException $e) {
            throw new Exception("NO SE PUEDE BORRAR UN CLIENTE CON REGISTROS ASOCIADOS.");
        }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Cliente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cliente']))
			$model->attributes=$_GET['Cliente'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Cliente('search');
		$this->pageTitle = 'Administrando Clientes ';

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cliente'])){
			$model->attributes=$_GET['Cliente'];
		}else{

		}

		$this->render('admin',array(
			'model'=>$model,
		));
    }
    
    public function actionRevisarSaldos(){
        $model=new Cliente('search');
		$this->pageTitle = 'Administrando Clientes ';

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cliente'])){
			$model->attributes=$_GET['Cliente'];
		}else{

		}

		$this->render('adminRevisarSaldos',array(
			'model'=>$model,
		));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Cliente the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Cliente::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Cliente $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cliente-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
