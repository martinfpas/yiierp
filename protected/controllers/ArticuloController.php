<?php

class ArticuloController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','ImportarDatosCsv','SaveAsync','GetAsync','Select2'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("mod_articulos")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionImportarDatosCsv() {
        $fp = fopen('/var/www/erca/upload/articulos.csv', 'r');
        if($fp){
            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                echo '<br>';
                $oArticulo = Articulo::model()->findByAttributes(array('id_articulo'=>$line[0],'id_rubro'=>$line[1]));
                if ($oArticulo != null){
                    $oArticulo->descripcion = utf8_encode($line[2]);
                    if (!$oArticulo->save()){
                        echo 'No se pudo guardar:';
                        ComponentesComunes::print_array($oArticulo->getErrors());
                    }else{
                        echo '<br>Articulo modificado :';
                        foreach ($oArticulo->attributes as $index => $item) {
                            echo $index.' '.utf8_encode($item).' : ' ;
                        }
                    }
                }else{
                    $oArticulo = new Articulo();
                    $oArticulo->id_articulo = $line[0];
                    $oArticulo->id_rubro = $line[1];
                    $oArticulo->descripcion = utf8_encode($line[2]);
                    if (!$oArticulo->save()){
                        echo 'No se pudo guardar:';
                        ComponentesComunes::print_array($oArticulo->getErrors());
                    }else{
                        echo '<br>NUEVO ARTICULO !!! :';
                        foreach ($oArticulo->attributes as $index => $item) {
                            echo $index.' '.utf8_encode($item).' : ' ;
                        }
                    }
                }
            }
        }
    }

    public function actionGetAsync($idArticulo,$idRubro){
        $model = Articulo::model()->findByAttributes(array('id_rubro' => $idRubro,'id_articulo' => $idArticulo));
        if ($model){
            echo $model->descripcion;
        }else{
            throw new CHttpException('500','<span class="errorSpan">Error: El Articulo no existe</span>');
        }
    }

    public function actionSelect2(){

        $criteria = new CDbCriteria();
        if (isset($_GET['id_rubro'])){
            $criteria->compare('id_rubro',$_GET['id_rubro']);
        }
        if(ComponentesComunes::validatesAsInt($_GET['q'])){
            $criteria->compare('id_articulo',$_GET['q']);
        }else{
            $criteria->compare('descripcion',$_GET['q'],true);
            $criteria->limit = $_GET['page_limit'];
            //$criteria->offset = $_GET['page'] * $_GET['page_limit'];
        }

        $models = Articulo::model()->todos()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id,'text' => $oModel->descripcion);
        }

        echo CJSON::encode($aModels);
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver Articulo #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Articulo;
		$this->pageTitle = "Nuevo Articulo";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Articulo']))
		{
			$model->attributes=$_POST['Articulo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new Articulo;
		$this->pageTitle = "Nuevo Articulo";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Articulo']))
		{
			$model->attributes=$_POST['Articulo'];
			if($model->save()){
				echo 'Ok';
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando Articulo #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Articulo']))
		{
			$model->attributes=$_POST['Articulo'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new Articulo('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Articulo']))
			$model->attributes=$_GET['Articulo'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Articulo('search');
		$this->pageTitle = 'Administrando Articulos ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Articulo'])){
			$model->attributes=$_GET['Articulo'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Articulo the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Articulo::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Articulo $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='articulo-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
