<?php

class CargaDocumentosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','Sort','SaveFieldAsync'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionSaveFieldAsync(){
        if (!$_POST['pk']) {
            throw new CHttpException('500','Error de parametros');
        }

        $oCargaDocumentos = new CargaDocumentos($_POST['scenario']);

        $oCargaDocumentos = CargaDocumentos::model()->with('oFactura')->findByPk($_POST['pk']);
        if ($oCargaDocumentos != null) {
            $oCargaDocumentos->setAttribute($_POST['name'],$_POST['value']);
            // SI NO EXISTE EL PAGO, SE CREA DINAMICAMENTE
            if ($_POST['name'] == 'efectivo' || $_POST['name'] == 'cheque' ){
                $oDocumento = $oCargaDocumentos->getDocumento();
                if ($oDocumento->oPagoCliente == null){
                    $oCargaDocumentos->crearPago($_POST['name'],$_POST['value']);
                    Yii::log('El documento NO existe ','warning');
                }else{
                    Yii::log('El documento existe ','warning');
                    $oPagoCliente = $oDocumento->oPagoCliente;
                    if ($_POST['name'] == 'efectivo'){
                        if (sizeof($oPagoCliente->aPagoAlContado) >= 0){
                            if (isset($oPagoCliente->aPagoAlContado[0])){
                                $oPagoAlContado = $oPagoCliente->aPagoAlContado[0];
                            }else{
                                $oPagoAlContado = new PagoAlContado();
                                $oPagoAlContado->idPagodeCliente = $oPagoCliente->id;
                            }

                            $oPagoAlContado->monto = $_POST['value'];
                            if (!$oPagoAlContado->save()){
                                Yii::log('Error al actualizar PagoAlContado '.CHtml::errorSummary($oPagoAlContado),'error');
                            }else{
                                Yii::log('Pago Guardado ','warning');
                            }
                        }else{
                            // TODO: VER PORQUE NO ACTUALIZA EL ESTADO DEL PAGO
                            Yii::log('sizeof($oPagoCliente->aPagoAlContado) NO es mayor a cero ','warning');
                            $oPagoAlContado = new PagoAlContado();
                            $oPagoAlContado->idPagodeCliente = $oPagoCliente->id;
                            $oPagoAlContado->monto = $_POST['value'];
                            if (!$oPagoAlContado->save()){
                                Yii::log('Error al actualizar PagoAlContado '.CHtml::errorSummary($oPagoAlContado),'error');
                            }else{
                                Yii::log('Pago Guardado ','warning');
                            }
                        }
                    }else{
                        Yii::log('Name No es efectivo ','warning');
                    }
                }
                $oCargaDocumentos->recalcular();
            }else{
                if (!$oCargaDocumentos->save()) {
                    throw new CHttpException('500','Error'.CHtml::errorSummary($oCargaDocumentos));
                }else{
                    if ($_POST['name'] == 'devolucion' || $_POST['name'] == 'notaCredito' ){
                        $oCargaDocumentos->recalcular();
                    }
                }
            }

        }else {
            throw new CHttpException('500','No es una Nota de Pedido existente');
        }
        echo $_POST['value'];
    }

    public function actionSort()
    {
        $orden = 1;
        if (isset($_POST['ciudad']) && isset($_POST['idEntregaDoc'])){
            foreach ($_POST['ciudad'] as $sCiudad) {
                $aCiudad = explode('.', $sCiudad);
                $aCargaDoc = VCargaDocumentos::model()->soloId()->findAllByAttributes(array('codigoPostal'=>$aCiudad[0],'zp'=>$aCiudad[1],'idEntregaDoc'=>$_POST['idEntregaDoc']));
                foreach ($aCargaDoc as $index => $oCargaDoc) {
                    $oCargaDocumento = CargaDocumentos::model()->findByPk($oCargaDoc->id);
                    $oCargaDocumento->orden = $orden;
                    $oCargaDocumento->save();
                }
                $orden++;
            }
        }

    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver CargaDocumentos #'.$id;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CargaDocumentos;
		$this->pageTitle = "Nuevo CargaDocumentos";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CargaDocumentos']))
		{
			$model->attributes=$_POST['CargaDocumentos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new CargaDocumentos;
		$this->pageTitle = "Nuevo CargaDocumentos";

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CargaDocumentos']))
		{
			$model->attributes=$_POST['CargaDocumentos'];
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando CargaDocumentos #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CargaDocumentos']))
		{
			$model->attributes=$_POST['CargaDocumentos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new CargaDocumentos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CargaDocumentos']))
			$model->attributes=$_GET['CargaDocumentos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
	    $aCargaDocumentos = CargaDocumentos::model()->findAll();
        foreach ($aCargaDocumentos as $index => $aCargaDocumento) {
            $aCargaDocumento->razonSocial = '';
            //echo ' $aCargaDocumentos '.$aCargaDocumento->id;
            if (!$aCargaDocumento->save()){
                throw new CHttpException('500',CHtml::errorSummary($aCargaDocumento));
            }
        }

		$model=new CargaDocumentos('search');
		$this->pageTitle = 'Administrando Carga Documentoses ';
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CargaDocumentos'])){
			$model->attributes=$_GET['CargaDocumentos'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CargaDocumentos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CargaDocumentos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CargaDocumentos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='carga-documentos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
