<?php

class ChequePropioController extends Controller
{

    public $printFilesPath = '/var/www/erca/upload/print/';
    public $pdfHeader = '<table width="100%">
					<tr>
						<td width="40%"><div style="width: 500px;float: left;border: black solid 1px;">ERCA S.R.L.<br>Ruta Pcial Nº2 Altura 2350 <br> 3014 - MONTE VERA </div></td>
						<td width="27%" align="center">{PAGENO}/{nbpg}</td>
						<td width="30%" style="text-align: right;"></td>
					</tr>
				</table>';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('?'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('create','update','admin','delete','index','view','admin','SaveAsync','ImportarDatosCsv','select2','listaPdf','UpdateFromSgv','Revisar'),
				//'roles'=>array('admin','superadmin'),
				'expression'=> 'Yii::app()->user->checkAccess("administrador")',
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionUpdateFromSgv(){
        /** @var $oArticuloVenta ArticuloVenta */
        $user = 'phpmyadminuser';
        $pass = 'P@ssw0rd';
        $db = new PDO('mysql:host=localhost;dbname=svg;', $user, $pass);

        $aCtas = array(
            '1564/08' => 2,
            '49100452/55' => 5,
            '6110805' => 6,
        );

        $stmt = $db->prepare("SELECT * FROM ChequePropio");
        $stmt->execute();
        $ChequePropios = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($ChequePropios as $key => $ChequePropio){
            //ComponentesComunes::print_array($ChequePropio);
            if(isset($aCtas[$ChequePropio->Id_cuenta])){
                $id_cuenta = $aCtas[$ChequePropio->Id_cuenta];
                $oChequePpio = ChequePropio::model()->findByAttributes(array('nroCheque'=>$ChequePropio->Nro_Cheque,'id_cuenta'=>$id_cuenta));
                if($oChequePpio != null){
                    echo '<br>Update '.$ChequePropio->Nro_Cheque;
                    ComponentesComunes::print_array($ChequePropio);
                }else{
                    $oChequePpio = new ChequePropio();
                    $oChequePpio->nroCheque = $ChequePropio->Nro_Cheque;
                    $oChequePpio->fecha     = $ChequePropio->Fecha;
                    $oChequePpio->importe   = $ChequePropio->Importe;
                    $oChequePpio->id_cuenta = $id_cuenta;
                    $oChequePpio->fechaDebito = $ChequePropio->Fecha_Debito;
                    if (!$oChequePpio->save()){
                        echo CHtml::errorSummary($oChequePpio);
                    }

                }
            }else{
                echo 'Cuenta no existente:';
                ComponentesComunes::print_array($ChequePropio);
            }

        }
    }

    public function actionRevisar(){
        $aCheques = ChequePropio::model()->findAll();
        foreach($aCheques as $key => $oCheque){
            $oCheque->revisarMovimiento();
        }
    }

	// ESTA OK EN V 1.34
    public function actionImportarDatosCsv() {

        $fp = fopen('/var/www/erca/upload/ChequesPropios.csv', 'r');
        if($fp)
        {
            /*
            $line = fgetcsv($fp, 1000, ",");
            print_r($line); exit;
            */

            $first_time = true;
            $ix = 0;
            while( ($line = fgetcsv($fp, 1000, ";")) != FALSE) {
                $ix++;

                $oCuentaB = CuentasBancarias::model()->findByAttributes(array('numero'=> $line[6]));

                if ($oCuentaB == null){
                    echo '<br>'.$line[6].':: el indice '.$line[3].'-'.$line[4].' $oCuentaB No Existe...<br>';

                }else {

                    $oChequePropio = ChequePropio::model()->findByAttributes(array('id_cuenta' => $oCuentaB->id, 'nroCheque' => $line[0]));

                    if ($oChequePropio != null) {
                        //$oChequePropio = new ChequePropio();
                    } else {
                        $oChequePropio = new ChequePropio();
                    }

                        $oChequePropio->nroCheque = $line[0];
                        if ($line[1] != '?'){
                            $oChequePropio->fecha = ComponentesComunes::jpicker2db($line[1]);
                        }
                        $oChequePropio->importe = $line[2];
                        $oChequePropio->id_cuenta = $oCuentaB->id;
                        if($line[5] != '?'){
                            $oChequePropio->fechaDebito = ComponentesComunes::jpicker2db($line[5]);
                        }
                        if (!$oChequePropio->save()) {
                            echo '<br> NO se guardo:: ' . $line[0] . ':: el indice ' . $ix . '<br>';
                            print_r($oChequePropio->getErrors());
                        } else {
                            echo '<br>' . $line[0] . ':: el indice ' . $line[1] . '-' . $line[2] . ' => ' . $ix . '<br>';
                        }

                }

            };

        }
    }

    public function actionSelect2(){
        /* @var $oBanco Banco */

        $criteria = new CDbCriteria();

        if(isset($_GET['partial'])){
            $criteria->compare('nroCheque',$_GET['q'],true);
        }else{
            $criteria->compare('nroCheque',$_GET['q'],false);
        }
        if(isset($_GET['id_cuenta'])){
            $criteria->compare('id_cuenta',$_GET['id_cuenta'],false);
        }

        if(isset($_GET['page_limit'])){
            $criteria->limit = $_GET['page_limit'];
        }

        if(isset($_GET['sinPagos'])){
            $criteria->addCondition('t.fechaDebito is null');
        }

        $models = ChequePropio::model()->cuentasActivas()->findAll($criteria);

        $aModels = array();
        foreach ($models as $index => $oModel) {
            $aModels[] = array('id' => $oModel->id,'text' => $oModel->nroCheque.' '.$oModel->oCuenta->Title.': $'.number_format($oModel->importe,2,'.',''),'importe' => $oModel->importe,'sCuenta' => $oModel->oCuenta->Title);
        }

        echo CJSON::encode($aModels);
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->pageTitle = 'Ver '.ChequePropio::model()->getAttributeLabel('model').' #'.$id;
        $model = $this->loadModel($id);
        $model->revisarMovimiento();

		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ChequePropio;
		$this->pageTitle = "Nuevo ".ChequePropio::model()->getAttributeLabel('model');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequePropio']))
		{
			$model->attributes=$_POST['ChequePropio'];
			$model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            $model->fechaDebito = ComponentesComunes::jpicker2db($model->fechaDebito);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionSaveAsync()
	{
		$model=new ChequePropio;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequePropio']))
		{
			$model->attributes=$_POST['ChequePropio'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            $model->fechaDebito = ComponentesComunes::jpicker2db($model->fechaDebito);
			if($model->save()){
				echo $model->id;
			}else {
				throw new CHttpException('500','<span class="errorSpan">Error:</span>'.CHtml::errorSummary($model));
			}
		}
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->pageTitle = 'Modificando '.ChequePropio::model()->getAttributeLabel('model').' #'.$id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ChequePropio']))
		{
			$model->attributes=$_POST['ChequePropio'];
            $model->fecha = ComponentesComunes::jpicker2db($model->fecha);
            $model->fechaDebito = ComponentesComunes::jpicker2db($model->fechaDebito);
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$model=new ChequePropio('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ChequePropio']))
			$model->attributes=$_GET['ChequePropio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ChequePropio('search');
		$this->pageTitle = 'Administrando '.ChequePropio::model()->getAttributeLabel('models');
		
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ChequePropio'])){
			$model->attributes=$_GET['ChequePropio'];
		}else{
			
		}	

		$this->render('admin',array(
			'model'=>$model,
		));
	}

    /**
     * Manages all models.
     */
    public function actionListaPdf($print=false)
    {
        ini_set("memory_limit","256M");

        //mode, format, default_font_size, default_font, margin_left (formerly $mgl), margin_right (formerly $mgr), margin_top (formerly $mgt), $margin_bottom (formerly mgb), $margin_header (formerly $mgh), margin_footer (formerly $mgf),
        $printer = '';

        if (isset($_POST['printer'])){
            $printer = $_POST['printer'];
        }

        if ($print){
            if ($printer == ''){
                die('Definir impresora');
            }else{
                //$mPDF1 = Yii::app()->ePdf->mpdf('', array(225,305),'10','8pin', 15,15,20,20,5,0,'P');//
                //$mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','moderndot', 10,10,20,5,0,0,'P');
                $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','12','couriernew', 15,7,25,25,8,0,'P');
            }
        }else{
            $mPDF1 = Yii::app()->ePdf->mpdf('', 'RA4','10','couriernew', 15,7,25,25,8,0,'P');
        }
        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/pdfComunFuenteSmall.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->SetHTMLHeader($this->pdfHeader,null,true);

        $model=new ChequePropio('search');
        $this->layout = 'column1Pdf';
        $model->unsetAttributes();  // clear any default values

        if(isset($_GET['ChequePropio'])){
            $model->attributes=$_GET['ChequePropio'];
        }else{

        }

        $mPDF1->WriteHTML($this->renderPartial('listPdf',array(
            'model'=>$model,
            ),true)
        );

        $mPDF1->shrink_tables_to_fit=0;


        $filename = 'ChequesEmitidos_2'.'.pdf';
        if ($print){
            $fullPathFile = $this->printFilesPath.'/'.$filename;
            $mPDF1->Output($fullPathFile, EYiiPdf::OUTPUT_TO_FILE);

            chmod($fullPathFile,0777);
            //Yii::log(shell_exec('libreoffice --headless --pt "'. $printer.'" "'.$fullPathFile.'"'),'warning');
            $fromPage= (isset($_POST['fromPage']))? $_POST['fromPage'] :'';
            $toPage  = (isset($_POST['toPage']))?   $_POST['toPage'] : '';
            PrintWidget::print($printer,$fullPathFile,$fromPage,$toPage); //OK

            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";

        }else{ // SALE AL NAVEGADOR
            $mPDF1->Output($filename, EYiiPdf::OUTPUT_TO_BROWSER);
        }

    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ChequePropio the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ChequePropio::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'Dirección incorrecta.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ChequePropio $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cheque-propio-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
