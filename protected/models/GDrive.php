<?php
/**
 * @v1.1 
 * path changes
 * 
 */
require_once Yii::app()->basePath.'/vendors/google-api-php-client-PHP7.0/vendor/autoload.php';

class GDrive {
    public $bckFolder       ='';
    public $client          = null;
    public $projectName     = 'Guru Production Bck';
    public $jsonKeyFilePath ;
    public $tokenFile       ;
    const  bckFolder  = '1Iysg0C-gLTPJwt3YKTkWdzdT0sO_wprf';
    const  bckImgFolder  = '1beOdJZv-4c_9654bMjuLiAmm4s9XnLmL';
    
    
    function __construct($token=null ){
        $this->bckFolder        = Yii::app()->basePath.'/bck';
        $this->jsonKeyFilePath  = Yii::app()->basePath.'/config/credentials.json';
        $this->tokenFile        = Yii::app()->basePath.'/config/token.json';
    }



    function saveDBs(){
        

        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Drive($client);
        

        if ($handle = opendir($this->bckFolder)) {
            while (false !== ($file = readdir($handle))) {
                if ('.' === $file) continue;
                if ('..' === $file) continue;
                
                $bckfile = $file;
                $bckPath = $this->bckFolder.'/'.$bckfile;

                echo('<br>'.$bckPath);
                // do something with the file
                echo 'writing... \r\n';
                if(file_exists($bckPath)){
                    echo '...file exists... \r\n';
                    $fileMetadata = new Google_Service_Drive_DriveFile(array( 'name' => $bckfile, 'parents' => [self::bckFolder]));
                    $content = file_get_contents($bckPath); 
                    
                    $file = $service->files->create($fileMetadata, array( 'data' => $content, /* 'mimeType' => 'X-Upload-Content-Type',*/ 'uploadType' => 'resumable', 'fields' => 'id'));
                    printf("File ID: %s\n", $file->id);
                    if(isset($file->id)){
                        unlink($bckPath);
                    }
                }else{
                    echo 'no existe:'.$bckfile.'\r\n';
                    
                }
            }
            closedir($handle);
        }

        return true;

    }
    
    static function searchFolder(string $folderName,string $parentId = null,Google_Service_Drive $service = null) {
        if($service == null){
            // Get the API client and construct the service object.
            $client = self::getClient();
            $service = new Google_Service_Drive($client);
        }
        # code...
        //$folderName = "imagenes"; // Please set the folder name here.
        $optParams = array(
            'pageSize' => 10,
            'fields' => 'nextPageToken, files',
            'q' => ($parentId == null)? "name = '".$folderName."' and mimeType = 'application/vnd.google-apps.folder' " : "name = '".$folderName."' and mimeType = 'application/vnd.google-apps.folder' and '".$parentId."' in parents"
        );
        $results = $service->files->listFiles($optParams);
        
        if(sizeof($results->files)){
            //echo '.*********************************** \r\n'.sizeof($results->files);
            //ComponentesComunes::print_array($results->files[0]);
            return $results->files[0];
        }else{
            return null;
        } 
    }

    /**
     * Returns an authorized API client.
     * @return Google_Client the authorized client object
     */
    function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Guru Production Bck');
        $client->setScopes(Google_Service_Drive::DRIVE);
        $client->setAuthConfig($this->jsonKeyFilePath); 
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');

        //$client->setAccessType('offline');
        //$client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = $this->tokenFile;
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                Yii::log('getRefreshToken 2 ','warning');	
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print '<br>Enter verification code: ';
                $authCode = trim(fgets(STDIN));


                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);//'4/0AY0e-g628BzqRAvPYpZVSywa9PD1cZTXqoFXwN9ATN2G9AwPklQ4rfzsKdHBixaHBeFnGA');
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }

    
}
?>