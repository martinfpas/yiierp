<?php

/**
 * This is the model class for table "cuentasBancarias".
 * public $idBanco;
 *
 * The followings are the available columns in table 'cuentasBancarias':
 * @property integer $id
 * @property string $numero
 * @property integer $id_banco
 * @property integer $id_sucursal
 * @property string $observaciones
 * @property integer $codigoTipoCta
 * @property double $saldo
 * @property string $fechaSaldo
 * @property integer $codigoMoneda
 * @property string $fechaApertura
 * @property string $fechaCierre
 * @property double $saldoInicial
 * @property string $cbu
 *
The followings are the available model relations:
 * @property ChequePropio[] $aChequePropio
 * @property SucursalBanco $oSucursal
 * @property Banco $oBanco
 * @property TipoCuenta $oCodigoTipoCta
 * @property Moneda $oCodigoMoneda
 * @property MovimientoCuentasBanc[] $aMovimientoCuentasBanc
*/


class CuentasBancarias extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CuentasBancarias the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cuentasBancarias';
	}

    public function defaultScope()
    {
        return array(
            'condition' => 'fechaCierre IS NULL ',
        );

    }

    public function scopes(){
	    return array(
	        'activas' => array(
                'condition' => 'fechaCierre IS NULL ',
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero, id_banco, codigoTipoCta, codigoMoneda, fechaApertura', 'required'),
			array('id_banco, id_sucursal, codigoTipoCta, codigoMoneda', 'numerical', 'integerOnly'=>true),
			array('saldo', 'numerical'),
            array('cbu', 'length', 'max'=>22),
            array('numero', 'length', 'max'=>30),
            array('observaciones', 'length', 'max'=>50),
			array('fechaSaldo, fechaCierre', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, numero, id_banco, id_sucursal, observaciones, codigoTipoCta, saldo, fechaSaldo, codigoMoneda, fechaApertura, fechaCierre, cbu,saldoInicial', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'aChequePropio' => array(self::HAS_MANY, 'ChequePropio', 'id_cuenta'),
            'oSucursal' => array(self::BELONGS_TO, 'SucursalBanco', 'id_sucursal'),
            'oBanco' => array(self::BELONGS_TO, 'Banco', 'id_banco'),
            'oCodigoTipoCta' => array(self::BELONGS_TO, 'TipoCuenta', 'codigoTipoCta'),
            'oCodigoMoneda' => array(self::BELONGS_TO, 'Moneda', 'codigoMoneda'),
            'aMovimientoCuentasBanc' => array(self::HAS_MANY, 'MovimientoCuentasBanc', 'idCuenta'),
        );
    }

    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
                'models' => Yii::t('application', 'Cuentas Bancarias'),
                'model' => Yii::t('application', 'Cuenta Bancaria'),
                'id' => Yii::t('application', 'ID'),
                'numero' => Yii::t('application', 'Numero'),
                'id_banco' => Yii::t('application', 'Id Banco'),
                'id_sucursal' => Yii::t('application', 'Id Sucursal'),
                'observaciones' => Yii::t('application', 'Observaciones'),
                'codigoTipoCta' => Yii::t('application', 'Codigo Tipo Cta'),
                'saldo' => Yii::t('application', 'Saldo'),
                'fechaSaldo' => Yii::t('application', 'Fecha Saldo'),
                'codigoMoneda' => Yii::t('application', 'Codigo Moneda'),
                'fechaApertura' => Yii::t('application', 'Fecha Apertura'),
                'fechaCierre' => Yii::t('application', 'Fecha Cierre'),
                'cbu' => Yii::t('application', 'Cbu'),
        );
	}

	public function getTitle(){
	    $sSucursal = ($this->oSucursal != null)? $this->oSucursal->nombre : '';
	    return $this->numero.' ('.$this->oBanco->nombre.'-'.$sSucursal.')';
    }

    public function recalcularSaldo($fecha){
        /** @var $oMovimiento MovimientoCuentasBanc */

        if(!$fecha){
            Yii::log("recalcularSaldo($fecha)",CLogger::LEVEL_ERROR);
            return false;
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.idCuenta = '.$this->id);
        $criteria->addCondition('t.fechaMovimiento < "'.$fecha.'"');
        $criteria->order = 't.fechaMovimiento desc,t.id desc';
        $oMovimiento = MovimientoCuentasBanc::model()->find($criteria);



        $saldoInicial = 0;
        if ($oMovimiento == null){
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.idCuenta = '.$this->id);
            $criteria->order = 't.fechaMovimiento asc,t.id asc';
            $aMovimientos = MovimientoCuentasBanc::model()->findAll($criteria);
            $saldoInicial = $this->saldoInicial;
            $this->fechaSaldo = '2020-06-01';
        }else{
            $criteria = new CDbCriteria;
            $criteria->addCondition('t.idCuenta = '.$this->id);
            $criteria->addCondition('t.fechaMovimiento >= "'.$fecha.'"');

            $criteria->order = 't.fechaMovimiento asc,t.id asc';

            $aMovimientos = MovimientoCuentasBanc::model()->findAll($criteria);
            $saldoInicial = $oMovimiento->saldo_acumulado;
            $this->fechaSaldo = $oMovimiento->fechaMovimiento;
        }
        $this->saldo = $saldoInicial;

        if (!$this->save()){
            Yii::log(CHtml::errorSummary($this),CLogger::LEVEL_ERROR);
        }


        foreach ($aMovimientos as $index => $oMovimiento) {
            //Yii::log($criteria->condition.ComponentesComunes::print_array($oMovimiento->attributes,true),CLogger::LEVEL_ERROR);
            //Yii::log('size='.sizeof($aMovimientos).' .'.$oMovimiento->id.' '.$oMovimiento->Importe,CLogger::LEVEL_ERROR);

            if($oMovimiento->oTipoMovimiento->Ingreso){
                $oMovimiento->saldo_acumulado = $oMovimiento->oCuenta->saldo + $oMovimiento->Importe;
            }else{
                $oMovimiento->saldo_acumulado = $oMovimiento->oCuenta->saldo - $oMovimiento->Importe;
            }
            //Yii::log($oMovimiento->id.' saldo_acumulado '.$oMovimiento->saldo_acumulado.'  >>> $this->saldo '.$this->saldo.' oCuenta->saldo '.$oMovimiento->oCuenta->saldo,CLogger::LEVEL_ERROR);

            $oMovimiento->save();
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('numero',$this->numero,true);
		$criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('id_sucursal',$this->id_sucursal);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('codigoTipoCta',$this->codigoTipoCta);
		$criteria->compare('saldo',$this->saldo);

        if ($this->fechaSaldo != null){
            $criteria->compare('fechaSaldo',ComponentesComunes::fechaComparada($this->fechaSaldo));
        }

		$criteria->compare('codigoMoneda',$this->codigoMoneda);

        if ($this->fechaApertura != null){
            $criteria->compare('fechaApertura',ComponentesComunes::fechaComparada($this->fechaApertura));
        }

        if ($this->fechaCierre != null){
            $criteria->compare('fechaCierre',ComponentesComunes::fechaComparada($this->fechaCierre));
        }

		$criteria->compare('cbu',$this->cbu);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}