<?php

/**
 * This is the model class for table "camionero".
 *
 * The followings are the available columns in table 'camionero':
 * @property integer $id
 * @property string $Nro_documento
 * @property string $Apellido
 * @property string $Nombres
 * @property string $Domicilio
 * @property string $Telefono
 * @property string $Codigo_Postal
 * @property string $Zona_Postal
 *
 * The followings are the available model relations:
 * @property CodigoPostal $codigoPostal
 * @property CodigoPostal $zonaPostal
 * @property Carga[] $aCarga
 */
class Camionero extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'camionero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nro_documento, Apellido', 'required'),
			array('Nro_documento', 'length', 'max'=>11),
			array('Apellido', 'length', 'max'=>30),
			array('Nombres', 'length', 'max'=>40),
			array('Domicilio', 'length', 'max'=>50),
			array('Telefono', 'length', 'max'=>15),
			array('Codigo_Postal, Zona_Postal', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Nro_documento, Apellido, Nombres, Domicilio, Telefono, Codigo_Postal, Zona_Postal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'codigoPostal' => array(self::BELONGS_TO, 'CodigoPostal', 'Codigo_Postal'),
			'zonaPostal' => array(self::BELONGS_TO, 'CodigoPostal', 'Zona_Postal'),
			'aCarga' => array(self::HAS_MANY, 'Carga', 'idCamionero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Camionero',
            'models' => 'Camioneros',
			'id' => Yii::t('application', 'ID'),
			'Nro_documento' => Yii::t('application', 'Nro Documento'),
			'Apellido' => Yii::t('application', 'Apellido'),
			'Nombres' => Yii::t('application', 'Nombres'),
			'Domicilio' => Yii::t('application', 'Domicilio'),
			'Telefono' => Yii::t('application', 'Telefono'),
			'Codigo_Postal' => Yii::t('application', 'Codigo Postal'),
			'Zona_Postal' => Yii::t('application', 'Zona Postal'),
		);
	}

    public function getTitulo() {
        return $this->Nombres.' '.$this->Apellido;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Nro_documento',$this->Nro_documento,true);
		$criteria->compare('Apellido',$this->Apellido,true);
		$criteria->compare('Nombres',$this->Nombres,true);
		$criteria->compare('Domicilio',$this->Domicilio,true);
		$criteria->compare('Telefono',$this->Telefono,true);
		$criteria->compare('Codigo_Postal',$this->Codigo_Postal,true);
		$criteria->compare('Zona_Postal',$this->Zona_Postal,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Camionero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
