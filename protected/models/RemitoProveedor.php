<?php

/**
 * This is the model class for table "remitoProveedor".
 *
 * The followings are the available columns in table 'remitoProveedor':
 * @property integer $id
 * @property integer $idProveedor
 * @property double $monto
 * @property string $fecha
 * @property string $detalle
 *
 * The followings are the available model relations:
 * @property Proveedor $oProveedor
 */
class RemitoProveedor extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'remitoProveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProveedor, fecha', 'required'),
			array('idProveedor', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('detalle', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idProveedor, monto, fecha, detalle', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'idProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Remito Proveedor',
            'models' => 'Remito Proveedores',
			'id' => Yii::t('application', 'ID'),
			'idProveedor' => Yii::t('application', 'Id Proveedor'),
			'monto' => Yii::t('application', 'Monto'),
			'fecha' => Yii::t('application', 'Fecha'),
			'detalle' => Yii::t('application', 'Detalle'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idProveedor',$this->idProveedor);
		$criteria->compare('monto',$this->monto);
        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }
		$criteria->compare('detalle',$this->detalle,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RemitoProveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function afterSave(){
	    $oRemito = RemitoProveedor::model()->findByPk($this->id);
	    $previo = 0;
	    if ($oRemito != null){
	        $previo = $oRemito->monto;
        }
	    $this->oProveedor->saldo = $this->oProveedor->saldo - $previo + $this->monto;
        if (!$this->oProveedor->save()){
            //TODO: MANEJAR EL ERROR
        }

	    return parent::afterSave();
    }
    public  function beforeDelete(){

    }
}
