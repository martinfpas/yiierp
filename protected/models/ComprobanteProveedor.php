<?php

/**
 * This is the model class for table "comprobanteProveedor".
 *
 * The followings are the available columns in table 'comprobanteProveedor':
 * @property integer $id
 * @property string $Nro_Comprobante
 * @property integer $id_Proveedor
 * @property string $FechaFactura
 * @property string $Clase
 * @property string $Nro_Puesto_Vta
 * @property double $Nograv
 * @property double $TotalNeto
 * @property string $MesCarga
 * @property double $Subtotal
 * @property double $Descuento
 * @property double $Iva
 * @property double $IngBrutos
 * @property double $Rg3337
 * @property string $FechaCarga
 * @property integer $id_Cuenta
 * @property double $PercepIB
 * @property double $ImpGanancias
 * @property integer $TipoComprobante
 * @property string $Cai
 * @property string $Fecha_Vencimiento
 * @property integer $Cant_alicuotas_iva
 * @property double $montoPagado
 *
 *
 * The followings are the available model relations:
 * @property AlicuotasCompra[] $aAlicuotasCompra
 * @property Proveedor $oProveedor
 * @property CuentaGastos $oCuentaGastos
 * @property DetalleComprobanteProv[] $aDetalleFactura
 * @property Pago2FactProv[] $aPago2FactProv
 */
class ComprobanteProveedor extends CActiveRecord
{
    public $bPagoInmediado = true;
    public $bIsNew = false;
    public $condImpaga;
    public $subTotalItems;
    // RELACION CODIGO => MONTO DE ALICUOTA EN AFIP
    public static $aIvaAlicuotasMontos =  array
    (
        '0' => '0',
        '10.5' => '10.5',
        '21' => '21',
        '27' => '27',
        '5' => '5',
        '2.5' => '2.5'
    );

    public static $aClase = array(
        '001' => 'FACTURAS A',
        '081' => 'TIQUE FACTURA A',
        '002' => 'NOTAS DE DEBITO A',
        '003' => 'NOTAS DE CREDITO A',
        '004' => 'RECIBOS A',
        '011' => 'FACTURAS C',
        '015' => 'RECIBOS C',
        '118' => 'TIQUE FACTURA M',
        '006' => 'FACTURAS B',
        '008' => 'NOTA DE CREDITO B',
        '007' => 'NOTA DE DEBITO B',
    );

    public static $aClaseShort = array(
        '001' => 'F A',
        '081' => 'TF A',
        '002' => 'ND A',
        '003' => 'NC A',
        '004' => 'RE A',
        '011' => 'FA C',
        '015' => 'RE C',
        '118' => 'TF M',
        '006' => 'FA B',
        '008' => 'NC B',
        '007' => 'ND B',
    );

    public static $aIvaInc = array(
        '001' => false,
        '081' => false,
        '002' => false,
        '003' => false,
        '004' => false,
        '011' => true,
        '015' => true,
        '118' => false,
        '006' => true,
        '008' => true,
        '007' => true,
    );

    public static $aFactorMultip = array(
        '001' => 1,
        '081' => 1,
        '002' => 1,
        '003' => -1,
        '004' => -1,
        //'004' => 0,
        '011' => 1,
        '015' => -1,
        //'015' => 0,
        '118' => 1,
        '006' => 1,
        '008' => 1,
    );

    public static $aClaseTipo = array(
        '001' => 'A',
        '081' => 'A',
        '002' => 'A',
        '003' => 'A',
        '004' => 'A',
        '011' => 'C',
        '015' => 'C',
        '118' => 'M',
        '006' => 'B',
        '008' => 'B',
        '007' => 'B',
    );

    public static $aClaseDiscIva = array(
        '001' => true,
        '081' => true,
        '002' => true,
        '003' => true,
        '004' => true,
        '011' => false,
        '015' => false,
        '118' => false,
        '006' => false,
        '008' => false,
        '007' => false,
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobanteProveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, MesCarga, FechaCarga, id_Cuenta, TipoComprobante, Cant_alicuotas_iva', 'required'),
			array('id_Proveedor, id_Cuenta, Cant_alicuotas_iva', 'numerical', 'integerOnly'=>true),
			array('Nograv, TotalNeto, Subtotal, Descuento, Iva, IngBrutos, Rg3337, PercepIB, ImpGanancias, montoPagado', 'numerical'),
			array('Nro_Comprobante', 'length', 'max'=>8),
            array('MesCarga', 'length', 'max'=>7),
			array('Nro_Puesto_Vta', 'length', 'max'=>4),
            array('TipoComprobante', 'length', 'max'=>3),
			array('Cai', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, Nograv, TotalNeto, MesCarga, Subtotal, Descuento, Iva, IngBrutos, Rg3337, 
			FechaCarga, id_Cuenta, PercepIB, ImpGanancias, TipoComprobante, Cai, Fecha_Vencimiento, Cant_alicuotas_iva, montoPagado, bPagoInmediado,condImpaga', 'safe', 'on'=>'search'),
		);
	}

    public function recalcular(){
	    /* @var $oDetalleFactura DetalleComprobanteProv */
	    $subtotal = 0;
	    $totalPrevio = $this->TotalNeto;
        foreach ($this->aAlicuotasCompra as $index => $item) {
            $item->delete();
        }
        
        $fIva = 0;
        foreach ($this->aDetalleFactura as $index => $oDetalleFactura) {
            // SI EL COMPROBANTE NO DISCRIMINA IVA
            if(self::$aIvaInc[$this->TipoComprobante]){
                $totalLinea = $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario;
                $subtotal += $totalLinea;
                $importeNetoGravado = $totalLinea / (1+($oDetalleFactura->alicuota / 100));

            }else{
                $totalLinea = $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario * (1+($oDetalleFactura->alicuota / 100));
                $subtotal += $totalLinea;
                $importeNetoGravado = $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario;
            }

            $impuestoLiquidado = ($totalLinea - $importeNetoGravado);

            $oAlicuota = AlicuotasCompra::model()->findByAttributes(array('alicuota'=> $oDetalleFactura->alicuota,'idComprobanteCompra' => $this->id));
            if ($oAlicuota == null){
                $oAlicuota = new AlicuotasCompra();
                $oAlicuota->idComprobanteCompra = $this->id;
                $oAlicuota->alicuota = $oDetalleFactura->alicuota;
                $oAlicuota->impNetoGravado = 0; $oAlicuota->impuestoLiquidado = 0;
            }
            $oAlicuota->impNetoGravado += $importeNetoGravado;
            $oAlicuota->impuestoLiquidado += $impuestoLiquidado;
            $fIva += $impuestoLiquidado;
            try{
                // Try to insert
                if(!$oAlicuota->save()){
                    Yii::log(print_r($oAlicuota->attributes,true),'warning');
                    throw new CHttpException(500,CHtml::errorSummary($oAlicuota));
                }
            }catch (CDbException $e){
                // Do something else (eg update record)
                Yii::log(print_r($oAlicuota->attributes,true),'warning');
            }
        }

        /*
        foreach ($this->aAlicuotasCompra as $index => $item) {
            $fIva += $item->impuestoLiquidado;
        }
        */

        $oVAlicuotasCompra = VAlicuotasCompra::model()->findByAttributes(['idComprobanteCompra'=>$this->id]);

        $this->Cant_alicuotas_iva =  ($oVAlicuotasCompra)? $oVAlicuotasCompra->totalIvas : $this->Cant_alicuotas_iva;

        $this->Iva = $fIva;
        $this->Subtotal = $subtotal;
        $this->TotalNeto = $subtotal - $this->Descuento + $this->IngBrutos + $this->Rg3337 + $this->PercepIB  + $this->ImpGanancias + $this->Nograv;

        try{
            if (!$this->save()){
                throw new Exception(CHtml::errorSummary($this));
            }else{
                $this->oProveedor->saldo =  $this->oProveedor->saldo + ($this->TotalNeto - $totalPrevio) * self::$aFactorMultip[$this->TipoComprobante];
                if (!$this->oProveedor->save()){
                    Yii::log('Error al actualizar el saldo: Previo: '.$this->oProveedor->saldo,'error');
                    Yii::log('Error al actualizar el saldo: $totalPrevio: '.$totalPrevio,'error');
                    Yii::log('Error al actualizar el saldo: $this->TotalNeto: '.$this->TotalNeto,'error');
                    throw new Exception('Error al actualizar el saldo: '.CHtml::errorSummary($this->oProveedor));
                }
            }
        }catch (CDbException $e){
            // Do something else (eg update record)
            Yii::log(print_r($this->attributes,true),'warning');
        }

    }

    public function getSubTotalItems(){
        $this->subTotalItems = 0;
        foreach ($this->aDetalleFactura as $index => $oDetalleFactura) {
            // SI EL COMPROBANTE NO DISCRIMINA IVA
            //if (!self::$aIvaInc[$this->TipoComprobante]) {
                $totalLinea = $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario;
                $this->subTotalItems += $totalLinea;
            /*
            } else {
                $totalLinea = $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario * (1 + ($oDetalleFactura->alicuota / 100));
                $this->subTotalItems += $totalLinea;
            }
            */
        }
        return $this->subTotalItems;
    }

    public function getNetoGravadoConDescuento(){
        return ($this->getSubTotalItems() *  (1-($this->Descuento / 100)));
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'aAlicuotasCompra' => array(self::HAS_MANY, 'AlicuotasCompra', 'idComprobanteCompra'),
			'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'id_Proveedor'),
            'aDetalleFactura' => array(self::HAS_MANY,'DetalleComprobanteProv','idComprobanteProveedor'),
            'oCuentaGastos' => array(self::BELONGS_TO, 'CuentaGastos', 'id_Cuenta'),
            'aPago2FactProv' => array(self::HAS_MANY, 'Pago2FactProv', 'idComprobanteProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'model' => 'Comprobante Recibido',
            'models' => 'Comprobantes Recibidos',
            'id' => Yii::t('application', 'ID'),
            'Nro_Comprobante' => Yii::t('application', 'Nro Doc Recib'),
            'id_Proveedor' => Yii::t('application', 'Id Proveedor'),
            'FechaFactura' => Yii::t('application', 'Fecha Comp.'),
            'Clase' => Yii::t('application', 'Clase'),
            'Nro_Puesto_Vta' => Yii::t('application', 'Nro Puesto Vta'),
            'Nograv' => Yii::t('application', 'No Grav'),
            'TotalNeto' => Yii::t('application', 'Total'),
            'MesCarga' => Yii::t('application', 'Mes Carga'),
            'Subtotal' => Yii::t('application', 'Subtotal'),
            'Descuento' => Yii::t('application', 'Descuento'),
            'Iva' => Yii::t('application', 'Iva'),
            'IngBrutos' => Yii::t('application', 'Ing Brutos'),
            'Rg3337' => Yii::t('application', 'Rg3337'),
            'FechaCarga' => Yii::t('application', 'Fecha Carga'),
            'id_Cuenta' => Yii::t('application', 'Id Cuenta'),
            'PercepIB' => Yii::t('application', 'Percep Ib'),
            'ImpGanancias' => Yii::t('application', 'Imp Ganancias'),
            'TipoComprobante' => Yii::t('application', 'Tipo Comprobante'),
            'Cai' => Yii::t('application', 'Cai'),
            'Fecha_Vencimiento' => Yii::t('application', 'Fecha Vencimiento'),
            'Cant_alicuotas_iva' => Yii::t('application', 'Cant Alicuotas Iva'),
            'montoPagado' => Yii::t('application', 'Monto Pagado'),
            'bPagoInmediado'  => Yii::t('application', 'Pago Inmediato'),
		);
	}

	public function getTotalConFactor(){
        return ($this->getNetoGravadoConDescuento() + $this->Nograv + $this->Iva + $this->IngBrutos + $this->Rg3337) * self::$aFactorMultip[$this->TipoComprobante];
	    //return ($this->TotalNeto * self::$aFactorMultip[$this->TipoComprobante]);
    }

    public function getTotalNetoConFactor(){
        //return ($this->TotalNeto - $this->PercepIB) * self::$aFactorMultip[$this->TipoComprobante];
        return ($this->getNetoGravadoConDescuento() + $this->Nograv + $this->Iva + $this->IngBrutos + $this->Rg3337) * self::$aFactorMultip[$this->TipoComprobante];
    }


    public function beforeDelete(){
        if(sizeof($this->aPago2FactProv) > 0){
            throw new CHttpException('500','No se puede borrar porque tiene pagos asociados');
        }

        return parent::beforeDelete();
    }

    public function afterDelete(){
	    $ajuste = $this->TotalNeto * self::$aFactorMultip[$this->TipoComprobante] ;
	    $oProveedor = $this->oProveedor;

	    $bDelete =  parent::afterDelete();
        Yii::log('$ajuste: '.$ajuste.' >'.$bDelete.'<','warning');

        $oProveedor->saldo -= $ajuste;
        if(!$oProveedor->save()){
            Yii::log('Error al actualizar el saldo: Previo: '.$oProveedor->saldo,'error');
            Yii::log('Error al actualizar el saldo: $this->TotalNeto: '.$ajuste,'error');
            throw new Exception(CHtml::errorSummary($oProveedor));
        }

        return $bDelete;
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProveedor');

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.Nro_Comprobante',$this->Nro_Comprobante);
		$criteria->compare('t.id_Proveedor',$this->id_Proveedor);
        if ($this->FechaFactura != null){
            $criteria->compare('FechaFactura',ComponentesComunes::fechaComparada($this->FechaFactura));
        }

		$criteria->compare('t.Clase',$this->Clase,true);
		$criteria->compare('t.Nro_Puesto_Vta',$this->Nro_Puesto_Vta,true);
		$criteria->compare('t.Nograv',$this->Nograv);
		$criteria->compare('t.TotalNeto',$this->TotalNeto);
		$criteria->compare('t.MesCarga',$this->MesCarga,true);
		$criteria->compare('t.Subtotal',$this->Subtotal);
		$criteria->compare('t.Descuento',$this->Descuento);
		$criteria->compare('t.Iva',$this->Iva);
		$criteria->compare('t.IngBrutos',$this->IngBrutos);
		$criteria->compare('t.Rg3337',$this->Rg3337);
		$criteria->compare('t.FechaCarga',$this->FechaCarga,true);
		$criteria->compare('t.id_Cuenta',$this->id_Cuenta);
		$criteria->compare('t.PercepIB',$this->PercepIB);
		$criteria->compare('t.ImpGanancias',$this->ImpGanancias);
		$criteria->compare('t.TipoComprobante',$this->TipoComprobante);
		$criteria->compare('t.Cai',$this->Cai,true);
		$criteria->compare('t.Fecha_Vencimiento',$this->Fecha_Vencimiento,true);
		$criteria->compare('t.Cant_alicuotas_iva',$this->Cant_alicuotas_iva);
        $criteria->compare('t.montoPagado',$this->montoPagado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    'id_Proveedor' => array(
                        'asc' => 'oProveedor.razonSocial asc,oProveedor.nbreFantasia asc',
                        'desc' => 'oProveedor.razonSocial desc,oProveedor.nbreFantasia desc',
                    ),
                    '*',
                ),
            )
		));
	}

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oProveedor');

        $criteria->compare('t.id',$this->id);
        $criteria->compare('Nro_Comprobante',$this->Nro_Comprobante);
        $criteria->compare('id_Proveedor',$this->id_Proveedor);
        if ($this->FechaFactura != null){
            $criteria->compare('FechaFactura',ComponentesComunes::fechaComparada($this->FechaFactura));
        }

        $criteria->compare('Clase',$this->Clase,true);
        $criteria->compare('Nro_Puesto_Vta',$this->Nro_Puesto_Vta,true);
        $criteria->compare('Nograv',$this->Nograv);
        $criteria->compare('TotalNeto',$this->TotalNeto);
        $criteria->compare('MesCarga',$this->MesCarga,true);
        $criteria->compare('Subtotal',$this->Subtotal);
        $criteria->compare('Descuento',$this->Descuento);
        $criteria->compare('Iva',$this->Iva);
        $criteria->compare('IngBrutos',$this->IngBrutos);
        $criteria->compare('Rg3337',$this->Rg3337);
        $criteria->compare('FechaCarga',$this->FechaCarga,true);
        $criteria->compare('id_Cuenta',$this->id_Cuenta);
        $criteria->compare('PercepIB',$this->PercepIB);
        $criteria->compare('ImpGanancias',$this->ImpGanancias);
        $criteria->compare('TipoComprobante',$this->TipoComprobante);
        $criteria->compare('Cai',$this->Cai,true);
        $criteria->compare('Fecha_Vencimiento',$this->Fecha_Vencimiento,true);
        $criteria->compare('Cant_alicuotas_iva',$this->Cant_alicuotas_iva);
        $criteria->compare('montoPagado',$this->montoPagado);

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    'id_Proveedor' => array(
                        'asc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia asc',
                        'desc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia desc',
                    ),
                    '*',
                ),

            )
        ));
    }
    
    public function searchPorCuentaWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.
        $criteria=new CDbCriteria;

        
        //$criteria->select = "sum(Subtotal * (1-(Descuento / 100))) as Subtotal,sum(Nograv) as Nograv,sum(TotalNeto) as TotalNeto,id_Cuenta,MesCarga,sum(Subtotal) as Subtotal,sum(Descuento) as Descuento,
        $criteria->select = "sum(TotalNeto - Iva - Nograv - IngBrutos - Rg3337 - ImpGanancias) as Subtotal,sum(Nograv) as Nograv,sum(TotalNeto) as TotalNeto,id_Cuenta,MesCarga,sum(Descuento) as Descuento,
        sum(Iva) as Iva,sum(IngBrutos) as IngBrutos,sum(Rg3337) as Rg3337,sum(PercepIB) as PercepIB,sum(ImpGanancias) as ImpGanancias,count(t.id) as id";
        $criteria->group = "id_Cuenta,MesCarga";

        $criteria->having = "MesCarga = '".$this->MesCarga."'";

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComprobanteProveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
