<?php

/**
 * This is the model class for table "detalleFacturaProv".
 *
 * The followings are the available columns in table 'detalleFacturaProv':
 * @property integer $id
 * @property integer $idArticulo
 * @property string $descripcion
 * @property string $fechaFactura
 * @property double $precioUnitario
 * @property integer $cantidadRecibida
 * @property integer $nroComprobante
 * @property string $nroPestoVta
 * @property string $clase
 * @property integer $nroItem
 * @property integer $alicuota
 * @property integer $idProveedor
 * @property integer $idMateriaPrima
 * @property integer $idFacturaProveedor
 *
 * The followings are the available model relations:
 * @property DetalleFacturaProv $oIdFacturaProveedor
 * @property DetalleFacturaProv[] $aDetalleFacturaProv
 */
class DetalleFacturaProv extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detalleFacturaProv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idArticulo, descripcion, fechaFactura, precioUnitario, cantidadRecibida, nroComprobante, nroPestoVta, clase, nroItem, alicuota, idProveedor, idMateriaPrima, idFacturaProveedor', 'required'),
			array('idArticulo, cantidadRecibida, nroComprobante, nroItem, alicuota, idProveedor, idMateriaPrima, idFacturaProveedor', 'numerical', 'integerOnly'=>true),
			array('precioUnitario', 'numerical'),
			array('descripcion', 'length', 'max'=>8),
			array('nroPestoVta', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idArticulo, descripcion, fechaFactura, precioUnitario, cantidadRecibida, nroComprobante, nroPestoVta, clase, nroItem, alicuota, idProveedor, idMateriaPrima, idFacturaProveedor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdFacturaProveedor' => array(self::BELONGS_TO, 'DetalleFacturaProv', 'idFacturaProveedor'),
			'aDetalleFacturaProv' => array(self::HAS_MANY, 'DetalleFacturaProv', 'idFacturaProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idArticulo' => 'Id Articulo',
			'descripcion' => 'Descripcion',
			'fechaFactura' => 'Fecha Factura',
			'precioUnitario' => 'Precio Unitario',
			'cantidadRecibida' => 'Cantidad Recibida',
			'nroComprobante' => 'Nro Comprobante',
			'nroPestoVta' => 'Nro Pesto Vta',
			'clase' => 'Clase',
			'nroItem' => 'Nro Item',
			'alicuota' => 'Alicuota',
			'idProveedor' => 'Id Proveedor',
			'idMateriaPrima' => 'Id Materia Prima',
			'idFacturaProveedor' => 'Id Factura Proveedor',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idArticulo',$this->idArticulo);
		$criteria->compare('descripcion',$this->descripcion,true);
        if ($this->fechaFactura != null){
            $criteria->compare('fechaFactura',ComponentesComunes::fechaComparada($this->fechaFactura));
        }

		$criteria->compare('precioUnitario',$this->precioUnitario);
		$criteria->compare('cantidadRecibida',$this->cantidadRecibida);
		$criteria->compare('nroComprobante',$this->nroComprobante);
		$criteria->compare('nroPestoVta',$this->nroPestoVta,true);
		$criteria->compare('clase',$this->clase,true);
		$criteria->compare('nroItem',$this->nroItem);
		$criteria->compare('alicuota',$this->alicuota);
		$criteria->compare('idProveedor',$this->idProveedor);
		$criteria->compare('idMateriaPrima',$this->idMateriaPrima);
		$criteria->compare('idFacturaProveedor',$this->idFacturaProveedor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleFacturaProv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
