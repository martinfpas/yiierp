<?php

/**
 * This is the model class for table "vCargaDocDePago".
 *
 * The followings are the available columns in table 'vCargaDocDePago':
 * @property integer $idEntregaDoc
 * @property integer $idCargaDoc
 * @property string $tipoDoc
 * @property integer $idFactura
 * @property integer $idNotaEntrega
 * @property integer $idPago
 * 
 * @property Cliente $oCliente
 * @property ChequeDeTerceros $oChequedeTercero
 * @property PagoCliente $oPagoCliente
 * @property CargaDocumentos $oCargaDoc
 * @property EntregaDoc $oEntregaDoc
 */
class VCargaDocDePago extends CActiveRecord
{

	const iInactivo = 0;
	const iActivo = 1; 
	
	const inactivo = 'Inactivo';
	const activo = 'Activo'; 
	
	public static $aEstado = array(
		self::iInactivo => self::inactivo,
		self::iActivo => self::activo,
	);
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vCargaDocDePago3';
	}

	public function primaryKey(){
        return 'idCargaDoc';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEntregaDoc, idCargaDoc, idFactura, idNotaEntrega, idPago', 'numerical', 'integerOnly'=>true),
			array('tipoDoc', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idEntregaDoc, idCargaDoc, idFactura, idNotaEntrega, idPago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// class name for the relations automatically generated below.
		return array(
            'oEntregaDoc' 		=> array(self::BELONGS_TO, 'EntregaDoc', 'idEntregaDoc'),
            'oCargaDoc' 		=> array(self::BELONGS_TO, 'CargaDocumentos', 'idCargaDoc'),
            'oPagoCliente' 		=> array(self::BELONGS_TO, 'PagoCliente', 'idPago'),
            'oCliente' 			=> array(self::BELONGS_TO, 'Cliente', 'idCliente'),
            'oChequedeTercero' 	=> array(self::BELONGS_TO, 'ChequeDeTerceros', 'idChequedeTercero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEntregaDoc' => 'Id Entrega Doc',
			'idCargaDoc' => 'Id Carga Doc',
			'idFactura' => 'Id Factura',
			'idNotaEntrega' => 'Id Nota Entrega',
			'idPago' => 'Id Pago',
		);
	}
	
	public function scopes()
    {
        return array(
            'activos'=>array(
                'condition'=>'activo='.self::iActivo,
            ),
            'inactivos'=>array(
                'condition'=>'activo='.self::iInactivo,
            ),
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEntregaDoc',$this->idEntregaDoc);
		$criteria->compare('idCargaDoc',$this->idCargaDoc);
		$criteria->compare('idFactura',$this->idFactura);
		$criteria->compare('idNotaEntrega',$this->idNotaEntrega);
		$criteria->compare('idPago',$this->idPago);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCargaDocDePago the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
