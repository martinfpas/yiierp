<?php

/**
 * This is the model class for table "vArtCotbyPc".
 *
 * The followings are the available columns in table 'vArtCotbyPc':
 * @property integer $id
 * @property integer $idCarga
 * @property integer $idArtVenta
 * @property integer $idPaquete
 * @property integer $cantidad
 * @property string $codCot
 * @property string $descripcion
 */
class VArtCotbyPc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vArtCotbyPc';
	}

    public function primaryKey(){

        return 'id';

    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCarga, idPaquete, cantidad', 'required'),
			array('id, idCarga, idArtVenta, idPaquete, cantidad', 'numerical', 'integerOnly'=>true),
			array('codCot', 'length', 'max'=>6),
			array('descripcion', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCarga, idArtVenta, idPaquete, cantidad, codCot, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCarga' => 'Id Carga',
			'idArtVenta' => 'Id Art Venta',
			'idPaquete' => 'Id Paquete',
			'cantidad' => 'Cantidad',
			'codCot' => 'Cod Cot',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idCarga',$this->idCarga);
		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('idPaquete',$this->idPaquete);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('codCot',$this->codCot,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VArtCotbyPc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
