<?php

/**
 * This is the model class for table "notaEntrega".
 *
 * The followings are the available columns in table 'notaEntrega':
 * @property integer $id
 * @property string $clase
 * @property integer $nroPuestoVenta
 * @property string $nroComprobante
 * @property integer $idNotaPedido
 * @property integer $idCliente
 * @property integer $idCamion
 * @property double $totalFinal
 * @property integer $enviada
 * @property integer $recibida
 * @property string $fecha
 * @property double $descuento1
 * @property double $descuento2
 * @property integer $nroViajante
 * @property string $observaciones
 * @property integer $impSinPrecio
 * @property integer $idPagoRelacionado
 * @property integer $estado
 * @property double $montoPagado
 *
 * The followings are the available model relations:
 * @property Cliente $oCliente
 * @property Vehiculo $oCamion
 * @property Viajante $oViajante
 * @property PuntoDeVenta $oPuestoVenta
 * @property NotaPedido $oNotaPedido
 * @property ProdNotaEntrega[] $aProdNotaEntrega
 * @property PagoCliente $oPagoCliente
 */
class NotaEntrega extends CActiveRecord
{
    const iNoEnviada = 0;
    const iEnviada = 1;
    public $alicuotaIva = 21;
    public $AlicuotaItem = 21;

    const iBorrador = 0;
    const iGuardada = 1;
    const iCerrada = 2;
    const iArchivada = 3;
    const iAnulada = 4;

    const borrador = 'Borrador';
    const guardada = 'Guardado';
    const cerrada = 'Cerrado';
    const archivada = 'Archivado';
    const anulada = 'Anulada';

    public static $aEstado = array(
        self::iBorrador => self::borrador,
        self::iGuardada => self::guardada,
        self::iCerrada => self::cerrada,
        self::iArchivada=> self::archivada,
        self::iAnulada=> self::anulada,
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notaEntrega';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clase, idCliente, idCamion, fecha, nroViajante, impSinPrecio', 'required'),
            array('idPagoRelacionado, nroPuestoVenta, idNotaPedido, idCliente, idCamion, nroViajante', 'numerical', 'integerOnly'=>true),
			array('totalFinal, descuento1, descuento2, montoPagado', 'numerical'),
			array('clase', 'length', 'max'=>1),
			array('nroComprobante', 'length', 'max'=>8),
            array('enviada, recibida','boolean'),
			array('observaciones, AlicuotaItem, estado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array(
			    'id, clase, nroPuestoVenta, nroComprobante, idNotaPedido, idCliente, idCamion, totalFinal, enviada, recibida, fecha, descuento1, 
			    descuento2, nroViajante, observaciones, impSinPrecio, idPagoRelacionado, estado', 'safe',
                'on'=>'search'
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCliente' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'oCamion' => array(self::BELONGS_TO, 'Vehiculo', 'idCamion'),
			'oViajante' => array(self::BELONGS_TO, 'Viajante', 'nroViajante'),
			'oPuestoVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'nroPuestoVenta'),
			'aProdNotaEntrega' => array(self::HAS_MANY, 'ProdNotaEntrega', 'idNotaEntrega'),
            'oNotaPedido' => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
            'oPagoCliente' => array(self::HAS_ONE, 'PagoCliente', 'idNotaEntrega'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Nota De Entrega',
            'models' => 'Notas De Entrega',
			'id' => 'ID Base',
            'clase' => Yii::t('application', 'Clase'),
            'nroPuestoVenta' => Yii::t('application', 'Nro Puesto Venta'),
            'nroComprobante' => Yii::t('application', 'Nro Comprobante'),
            'idNotaPedido' => Yii::t('application', 'Id Nota Pedido'),
            'idCliente' => Yii::t('application', 'Id Cliente'),
            'idCamion' => Yii::t('application', 'Id Camion'),
            'totalFinal' => Yii::t('application', 'Total Final'),
            'enviada' => Yii::t('application', 'Enviada'),
            'recibida' => Yii::t('application', 'Recibida'),
            'fecha' => Yii::t('application', 'Fecha'),
            'descuento1' => Yii::t('application', 'Descuento1'),
            'descuento2' => Yii::t('application', 'Descuento2'),
            'nroViajante' => Yii::t('application', 'Nro Viajante'),
            'observaciones' => Yii::t('application', 'Observaciones'),
            'impSinPrecio' => Yii::t('application', 'Imp Sin Precio'),
            'montoPagado' =>  Yii::t('application', 'Monto Pagado'),
		);
	}

    public function scopes(){
        return array(
            'noEnviadas' => array(
                'condition' => 'enviada = 0'
            ),
        );
    }
    
    public function esEditable(){
        if ($this->oNotaPedido != null){
            //return (($this->oNotaPedido->oCarga == null && $this->enviada == NotaEntrega::iNoEnviada ) || (($this->oNotaPedido->oCarga != null ) && ($this->oNotaPedido->oCarga->estado == Carga::iBorrador) && ($this->estado < NotaEntrega::iCerrada) ));
            return (($this->oNotaPedido->oCarga == null && $this->enviada == NotaEntrega::iNoEnviada ) || (($this->oNotaPedido->oCarga != null ) && ($this->estado < NotaEntrega::iCerrada) ));
        }else{
            //TODO REVISAR
            return true;
        }

    }

	/*
	 * GENERAR Y DEVOLVER UNA NOTA DE ENTREGA EN BASE A UNA NOTA DE PEDIDO
	 */
	public static function generarNe($idNotaPedido){
        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }

	    if ($idNotaPedido > 0){

	        $oNotaPedido = NotaPedido::model()->with('oNotaEntrega')->findByPk($idNotaPedido);
	        if($oNotaPedido <> null){

	            if($oNotaPedido->oNotaEntrega <> null){
                    if ($shouldCommit){
                        Yii::log('Commit 3 ','warning');
                        $transaction->commit();
                    }
	                return $oNotaPedido->oNotaEntrega;
                }
            }else{
                Yii::log('$oNotaPedido == null :: ','warning');
            }
	        if($oNotaPedido <> null){
	            $oNotaEntrega = new NotaEntrega();
	            $oNotaEntrega->fecha = date("Y-m-d H:i:s");
	            $oNotaEntrega->idCamion = $oNotaPedido->camion;
	            $oNotaEntrega->idCliente = $oNotaPedido->idCliente;
                $oNotaEntrega->nroViajante = $oNotaPedido->oCliente->codViajante;
                $oNotaEntrega->idNotaPedido = $idNotaPedido;
                $oNotaEntrega->impSinPrecio = $oNotaPedido->impSinPrecio;

                $idUser = Yii::app()->user->id;
                $oUser = User::model()->findByPk($idUser);
                if ($oUser != null){
                    if ($oUser->oPuntoDeVenta != null){
                        $oNotaEntrega->nroPuestoVenta = $oUser->puntoDeVenta;
                    }
                }else{ throw new Exception('El usuario no existe');}

                //TODO: VER QUE HACER CON LA CLASE
                $oNotaEntrega->clase = 'X';
                if ($oNotaEntrega->save()){
                    try{
                        $oNotaEntrega->clonarProductos();
                    }catch (Exception $e){
                        $transaction->rollback();
                        throw new CHttpException(500,$e->getMessage());
                    }
                    $oNotaEntrega->totalFinal = $oNotaEntrega->getTotalFinal();
                    if (!$oNotaEntrega->save()){
                        Yii::log('$oNotaEntrega->save() al guardar el TotalFinal:: '.CHtml::errorSummary($oNotaEntrega),'warning');
                    }
                    if ($shouldCommit){
                        Yii::log('Commit 2 ','warning');
                        $transaction->commit();
                    }
                    return $oNotaEntrega;
                }else{
                    //TODO: loguear errores
                    Yii::log('$oNotaEntrega->save() :: '.CHtml::errorSummary($oNotaEntrega),'error');
                    $transaction->rollback();
                    throw new CHttpException(500,CHtml::errorSummary($oNotaEntrega));
                }

            }else{
                $transaction->rollback();
                throw new CHttpException(500,'NotaPedido no encontrada');
                return null;
            }
        }else{
            $transaction->rollback();
            throw new CHttpException(500,'idNotaPedido no es mayor a 0');
            return null;
        }

        if ($shouldCommit){
            Yii::log('Commit 1 ','warning');
            $transaction->commit();
        }

    }

    /**
     * Toma la lista de productos de la nota de pedido asociada y clona los productos.
     *
     */
    public function clonarProductos(){
	    if ($this->oNotaPedido <> null){
	        $i=1;
            foreach ($this->oNotaPedido->aProdNotaPedido as $index => $oProdNotaPedido) {
               // Yii::log('$oProdNotaPedido->id :'.$oProdNotaPedido->id,'warning');
                $oProdNE = new ProdNotaEntrega();
                $oProdNE->cantidad = $oProdNotaPedido->cantidad;
                $oProdNE->idNotaEntrega = $this->id;
                $oProdNE->descripcion = $oProdNotaPedido->oArtVenta->descripcion;
                $oProdNE->nroItem = $i;
                $oProdNE->idArtVenta = $oProdNotaPedido->idArtVenta;
                $oProdNE->precioUnitario = $oProdNotaPedido->precioUnit;
                if(!$oProdNE->save()){
                    Yii::log('$oProdNE->save() :: '.CHtml::errorSummary($oProdNE),'warning');
                    throw new Exception(CHtml::errorSummary($oProdNE));
                };
                $i++;
	        }
        }
    }

    /**
     * Toma la lista de productos de la nota de pedido asociada y clona los productos.
     *
     */
    public function getSubTotal($reload = false){

        $fSubtotal=0;
        if($reload){
            Yii::log('reload :: ','warning');
            $aProdNotaEntrega = ProdNotaEntrega::model()->findAllByAttributes(array('idNotaEntrega' => $this->id));
            foreach ($aProdNotaEntrega as $index => $oProdNotaEntrega) {
                $importe = $oProdNotaEntrega->getImporte();
                $fSubtotal += number_format($importe,2, '.', '');
            }
        }else{
            foreach ($this->aProdNotaEntrega as $index => $oProdNotaEntrega) {
                $importe = $oProdNotaEntrega->getImporte();
                $fSubtotal += number_format($importe,2, '.', '');
            }
        }
        return number_format($fSubtotal,2, '.', '');
    }

    public function getDescuentoTotal(){
        return number_format(($this->totalFinal - $this->getSubTotal()),2, '.', '');
    }

    public function getTotalFinal($reload = false){

        $fSubTotal = $this->getSubTotal($reload);
        $fTotalFinal=($fSubTotal - ($fSubTotal * ($this->descuento1 / 100)) - ($fSubTotal * ($this->descuento2 / 100)));

        //return number_format($fTotalFinal,2, '.', '');
        $this->totalFinal = number_format($fTotalFinal,2, '.', '');
        return $this->totalFinal;
    }

    public function getDescuento1(){
        return number_format($this->descuento1,2,'.','');
    }

    public function getDescuento2(){
        return number_format($this->descuento2,2,'.','');
    }

    public function getAlicuotaItem(){
        $alicuota = 0;
        if ($this->oCliente->oRubro != null){
            if ($this->oCliente->oRubro->nombre == 'ESCUELA'){
                $alicuota = ($this->oCliente->oCategoriaIva->discIva)? 0 : $this->alicuotaIva;
                $alicuota += 10.5;
            }
        }else{
            //TODO: ALERTAR
            Yii::log('El Cliente :'.$this->oCliente->id.' NO TIENE RUBRO','error');
        }

        $this->AlicuotaItem = $alicuota;

        return $alicuota;
    }

    public function getFullTitle(){
        return ($this->nroComprobante != "")? $this->nroComprobante : '#'.$this->id;
    }

    public function getMontoASaldar(){
        return ($this->totalFinal > $this->montoPagado)? $this->totalFinal - $this->montoPagado : 0;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('clase',$this->clase,true);
        $criteria->compare('nroPuestoVenta',$this->nroPuestoVenta);
        $criteria->compare('nroComprobante',$this->nroComprobante,true);
        $criteria->compare('idNotaPedido',$this->idNotaPedido);
        $criteria->compare('idCliente',$this->idCliente);
        $criteria->compare('idCamion',$this->idCamion);
        $criteria->compare('totalFinal',$this->totalFinal);
        $criteria->compare('enviada',$this->enviada);
        $criteria->compare('recibida',$this->recibida);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::jpicker2db($this->fecha));
        }

        $criteria->compare('descuento1',$this->descuento1);
        $criteria->compare('descuento2',$this->descuento2);
        $criteria->compare('nroViajante',$this->nroViajante);
        $criteria->compare('observaciones',$this->observaciones,true);
        $criteria->compare('impSinPrecio',$this->impSinPrecio);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',

            ),
        ));
    }

    public function recalcular(){
        $this->getTotalFinal(true);
        if (!$this->save()){
            throw new Exception('Error '.CHtml::errorSummary($this));
        }
    }

    public function beforeDelete(){

        if($this->estado == 2){
            //return false;
            Yii::log('NOTA DE ENTREGA BORRADA CON ESTADO CERRADO.'.ComponentesComunes::print_array($this->attributes),'error');
        }

        $transaction = Yii::app()->db->getCurrentTransaction();
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
        }

        $oEstadoAnterior = NotaEntrega::model()->findByPk($this->id);

        $bReturn = parent::beforeDelete();
        if ($bReturn){
            $criteria = new CDbCriteria;
            $criteria->addCondition('idComprobante = '.$this->id);
            $criteria->addCondition("'tipoDoc' = 'NE'");
            $oCargaDoc = CargaDocumentos::model()->find($criteria);
            if($oCargaDoc != null){
                $oCargaDoc->delete();
            }
            // SOLO ACTUALLIZA EL SALDO DEL CLIENTE SI LO QUE SE DESCARTA "NO" ESTABA COMO BORRADOR
            if($oEstadoAnterior->estado != NotaEntrega::iBorrador){
                $oEstadoAnterior->oCliente->saldoActual = $oEstadoAnterior->oCliente->saldoActual - $oEstadoAnterior->totalFinal;
                if (!$oEstadoAnterior->oCliente->save()){
                    $transaction->rollback();
                    return false;
                }
            }
        }else{
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return $bReturn;
    }

    protected function beforeSave(){
        $bActualizarSaldo = false;
        $totalAnterior = 0;

        $oEstadoPrevio = self::model()->findByPk($this->id);
        $bBeforeSave =  parent::beforeSave();
        if($bBeforeSave){
            if ($oEstadoPrevio == null || $oEstadoPrevio->estado != NotaEntrega::iCerrada){
                if($this->nroComprobante == null){
                    $criteria=new CDbCriteria;
                    $criteria->order = 'CAST(t.nroComprobante AS unsigned) desc';
                    $row = NotaEntrega::model()->find($criteria);
                    if ($row != null){
                        $this->nroComprobante = (intval($row->nroComprobante)+1);
                    }else{
                        $this->nroComprobante = 1;
                    }
                }

                // SOLO ACTUALIZA SI EL ESTADO ACTUAL ES GUARDADO O CERRADO
                if ($this->estado != NotaEntrega::iBorrador && $this->estado != NotaEntrega::iArchivada && $this->estado != NotaEntrega::iAnulada){
                    if($oEstadoPrevio != null){
                        //SI EXISTE ESTADO PREVIO Y ESTABA GUARDADO O CERRADO, SOLO ACTUALIZA SI CAMBIO EL MONTO
                        if ($oEstadoPrevio->estado == NotaEntrega::iGuardada || $oEstadoPrevio->estado == NotaEntrega::iCerrada){
                            if ($oEstadoPrevio->totalFinal != $this->totalFinal ){
                                $bActualizarSaldo = true;
                                $totalAnterior = $oEstadoPrevio->totalFinal;
                            }
                        }else{
                            // SI EL ESTADO PREVIO ERA BORRADOR, CARGA EL SALDO COMPLETO
                            if ($oEstadoPrevio->estado == NotaEntrega::iBorrador){
                                $bActualizarSaldo = true;
                            }
                        }
                    }else{
                        // SI SE GUARDA SIN ESTADO ANTERIOR Y DIRECTAMENTE EN GUARDADO O CERRADO
                        $bActualizarSaldo = true;
                    }
                }

                if ($bActualizarSaldo){
                    Yii::log('Estado Nota entrega :: '.ComponentesComunes::print_array($this->attributes,true),'warning','custom');
                    $oCliente = $this->oCliente;
                    $oCliente->saldoActual = $oCliente->saldoActual + $this->totalFinal - $totalAnterior;
                    if (!$oCliente->save()){
                        //TODO: MANEJAR EL ERROR
                        throw new Exception(ComponentesComunes::print_array($oCliente->getErrors(),true));
                    }
                }

                Yii::log('Nota entrega '.$this->id.' totalFinal*....'.$this->totalFinal.' > ','warning');
            }else{
                Yii::log('Estaba Cerrada, solo se pueden actualizar ciertos campos','warning');
                $oEstadoNuevo = new NotaEntrega();
                $oEstadoNuevo->attributes = $this->attributes;
                $this->attributes = $oEstadoPrevio->attributes;
                $this->nroPuestoVenta = $oEstadoNuevo->nroPuestoVenta;
                $this->idCamion = $oEstadoNuevo->idCamion;
                $this->estado = $oEstadoNuevo->estado;
                $this->idPagoRelacionado = $oEstadoNuevo->idPagoRelacionado;
                $this->montoPagado = $oEstadoNuevo->montoPagado ;
                $this->recibida = $oEstadoNuevo->recibida;
                $this->enviada = $oEstadoNuevo->enviada;
            }
        }

        return $bBeforeSave;
    }

/*
    protected function afterSave()
    {
        if ($this->isNewRecord) {
            // TODO: AGREGAR AL SALDO DEL CLIENTE
            $oCliente = $this->oCliente;
            $oCliente->saldoActual = $oCliente->saldoActual + $this->totalFinal;
            if (!$oCliente->save()){
                //TODO: MANEJAR EL ERROR
            }
        }
        return parent::afterSave();
    }
*/

    /**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NotaEntrega the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
