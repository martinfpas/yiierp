<?php

/**
 * This is the model class for table "notaDebito".
 *
 * The followings are the available columns in table 'notaDebito':
 * @property integer $id
 * @property string $tipo
 * @property string $clase
 * @property integer $Nro_Puesto_Venta
 * @property string $Nro_Comprobante
 * @property integer $Iva_Responsable
 * @property integer $Nro_Viajante
 * @property integer $Id_Cliente
 * @property string $razonSocial
 * @property string $direccion
 * @property string $localidad
 * @property string $provincia
 * @property string $rubro
 * @property string $zona
 * @property string $tipoResponsable
 * @property string $cuit
 * @property integer $perIIBB
 * @property string $nroInscripIIBB
 * @property string $fecha
 * @property string $Moneda
 * @property string $Observaciones
 * @property integer $Id_Camion
 * @property double $Descuento1
 * @property double $Descuento2
 * @property integer $Enviada
 * @property integer $Recibida
 * @property double $Otro_Iva
 * @property string $CAE
 * @property string $Vencim_CAE
 * @property integer $idNotaPedido
 * @property integer $idVoucherAfip
 * @property double $alicuotaIva
 * @property double $netoGravado
 * @property double $subtotalIva
 * @property double $alicuotaIIBB
 * @property double $subtotal
 * @property double $Ing_Brutos
 * @property double $Total_Neto
 * @property double $Total_Final
 * @property integer $estado
 * @property integer $idNotaDeCredito
 * @property double $montoPagado
 * @property integer $idPagoRelacionado
 *
 * The followings are the available model relations:
 * @property PuntoDeVenta $nroPuestoVenta
 * @property Cliente $oCliente
 * @property Vehiculo $oCamion
 * @property AfipVoucher $oVoucherAfip
 * @property Viajante $oViajante
 * @property CategoriaIva $oIvaResponsable
 * @property ProdNotaDebito[] $aProdNotaDebito
 */
class NotaDebito extends Comprobante
{
    const borrador = 'Borrador';
    const guardada = 'Guardada';
    const cerrada = 'Cerrada';
    const archivada = 'Archivada';

    const iNoEnviada = 0;
    const iEnviada = 1;

    public static $aClase = array(
        'A' => 'A',
        'B' => 'B',
    );

    public static function alias(){
        return __CLASS__;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NotaDebito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobante';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clase, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion', 'required'),
            array('Nro_Puesto_Venta, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion, idNotaPedido, idVoucherAfip', 'numerical', 'integerOnly'=>true),
            array('Descuento1, Descuento2, Otro_Iva, alicuotaIva, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final', 'numerical'),
            array('tipo', 'length', 'max'=>2),
            array('clase', 'length', 'max'=>1),
            array('Nro_Comprobante, Moneda', 'length', 'max'=>8),
            array('razonSocial, direccion', 'length', 'max'=>50),
            array('localidad', 'length', 'max'=>65),
            array('provincia', 'length', 'max'=>40),
            array('rubro', 'length', 'max'=>35),
            array('zona', 'length', 'max'=>4),
            array('tipoResponsable, nroInscripIIBB', 'length', 'max'=>30),
            array('cuit', 'length', 'max'=>13),
            array('CAE', 'length', 'max'=>14),
            array('Enviada, Recibida','boolean'),
            array('fecha, Observaciones, Vencim_CAE, estado, idNotaDeCredito', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, tipo, clase, Nro_Puesto_Venta, Nro_Comprobante, Iva_Responsable, Nro_Viajante, Id_Cliente, razonSocial, direccion, localidad, provincia, rubro, zona, tipoResponsable, cuit, perIIBB, nroInscripIIBB, fecha, Moneda, Observaciones, Id_Camion, Descuento1, Descuento2, Enviada, Recibida, Otro_Iva, CAE, Vencim_CAE, idNotaPedido, idVoucherAfip, alicuotaIva, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final, estado', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'oViajante' => array(self::BELONGS_TO, 'Viajante', 'Nro_Viajante'),
            'oCamion' => array(self::BELONGS_TO, 'Vehiculo', 'Id_Camion'),
            'oNotaPedido' => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
            'oPuestoVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'Nro_Puesto_Venta'),
            'oCliente' => array(self::BELONGS_TO, 'Cliente', 'Id_Cliente'),
            'oIvaResponsable' => array(self::BELONGS_TO, 'CategoriaIva', 'Iva_Responsable'),
            'oVoucherAfip' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucherAfip'),
            'aProdNotaDebito' => array(self::HAS_MANY, 'ProdNotaCredito', 'idComprobante'),
            'aProdComprobante' => array(self::HAS_MANY, 'ProdNotaCredito', 'idComprobante'),
            'aProdComprobante' => array(self::HAS_MANY, 'ProdFactura', 'idComprobante'),
        );
    }



	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
            'model' => 'Nota De Débito',
            'models' => 'Notas De Débito',
            'id' => 'ID',
            'tipo' => 'Tipo',
            'clase' => 'Clase',
            'Nro_Puesto_Venta' => 'Nro Puesto Venta',
            'Nro_Comprobante' => 'Nro Comprobante',
            'Iva_Responsable' => 'Iva Responsable',
            'Nro_Viajante' => 'Nro Viajante',
            'Id_Cliente' => 'Id Cliente',
            'razonSocial' => 'Razon Social',
            'direccion' => 'Direccion',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
            'rubro' => 'Rubro',
            'zona' => 'Zona',
            'tipoResponsable' => 'Tipo Responsable',
            'cuit' => 'Cuit',
            'perIIBB' => 'Per Iibb',
            'nroInscripIIBB' => 'Nro Inscrip Iibb',
            'fecha' => 'Fecha',
            'Moneda' => 'Moneda',
            'Observaciones' => 'Observaciones',
            'Id_Camion' => 'Camion',
            'Descuento1' => 'Descuento1',
            'Descuento2' => 'Descuento2',
            'Enviada' => 'Enviada',
            'Recibida' => 'Recibida',
            'Otro_Iva' => 'Otro Iva',
            'CAE' => 'Cae',
            'Vencim_CAE' => 'Vencim Cae',
            'idNotaPedido' => 'Nota Pedido',
            'idVoucherAfip' => 'Voucher Afip',
            'alicuotaIva' => 'Alicuota Iva',
            'subtotalIva' => 'Subtotal Iva',
            'alicuotaIIBB' => 'Alicuota Iibb',
            'subtotal' => 'Subtotal',
            'Ing_Brutos' => 'Ing Brutos',
            'Total_Neto' => 'Total Neto',
            'Total_Final' => 'Total Final',
        );
	}



    //////////////////////////
    /**
     * Devuelve el subtotal sin descuentos
     *

    public function getSubTotalNeto(){

        $fSubtotal=0;
        foreach ($this->aProdNotaCredito as $index => $oProdComprobante) {
            $fSubtotal += number_format($oProdComprobante->getImporte(),2, '.', '');
        }
        return number_format($fSubtotal,2, '.', '');
    }
*/
    public function defaultScope()
    {
        return array(
            'alias' => self::alias(),
            'condition' => 'tipo = "ND"',
        );

    }

    public function scopes(){
        return array(
            'noEnviadas' => array(
                'condition' => 'Enviada = 0'
            ),
        );
    }

    /* @var $oNotaDebito NotaDebito */
    /* @var $oCliente Cliente */
    public static function generarND($idCliente){
        $oCliente = Cliente::model()->findByPk($idCliente);

        if($oCliente != null){
            $oNotaDebito = new NotaDebito();
            $oNotaDebito->Nro_Viajante = $oCliente->codViajante;
            $oNotaDebito->Iva_Responsable = $oCliente->categoriaIva;
            $oNotaDebito->Id_Cliente = $idCliente;
            $oNotaDebito->Id_Camion = 6;
            $oNotaDebito->Total_Final = 0;
            $oNotaDebito->clase = $oCliente->oCategoriaIva->tipoFact;
            $oNotaDebito->fecha = date('Y-m-d');


            $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $oCliente->cp, 'zp' => $oCliente->zp));
            if ($oLocalidad != null && $oLocalidad->id_provincia != 'E') {
                $oNotaDebito->Nro_Puesto_Venta = 9;
            } else {
                $oNotaDebito->Nro_Puesto_Venta = 10;
            }

            if ($oNotaDebito->save()) {
                return $oNotaDebito;
            } else {
                Yii::log('$oFactura->save() :: ' . CHtml::errorSummary($oNotaDebito), 'error');
                throw new Exception(CHtml::errorSummary($oNotaDebito));
            }


        }else{
            Yii::log($idCliente.'$idCliente  es invalido','error');
            throw new Exception('$idCliente  es invalido');
        }
    }

    public function cloneNotaCreditoItem($idNotaCredito){
        $aNotaCreditoProds = ProdNotaCredito::model()->findAllByAttributes(array('idComprobante' => $idNotaCredito));
        // TODO: RECORRER Y CREAR
        $i = 0;
        foreach ($aNotaCreditoProds as $key => $oNotaCreditoProd) {
            $oProdND = new ProdNotaDebito();
            $oProdND->idComprobante = $this->id;
            $oProdND->nroItem = $i;
            $oProdND->cantidad = $oNotaCreditoProd->cantidad;
            $oProdND->descripcion = $oNotaCreditoProd->descripcion;
            $oProdND->idArtVenta = $oNotaCreditoProd->idArtVenta;
            $oProdND->precioUnitario = $oNotaCreditoProd->precioUnitario;
            if (!$oProdND->save()){
                Yii::log('Error al guardar Prod NC'.CHtml::errorSummary($oProdND),'error');
            }

            $i++;
        }
    }

    public function beforeSave(){

        $this->tipo = 'ND';

        return parent::beforeSave();
    }

    public function beforeDelete(){
        if($this->CAE != '00000000000000' && $this->CAE != '' || ($this->oVoucherAfip != null && $this->oVoucherAfip->caea != '00000000000000' && $this->oVoucherAfip->caea != '')){
            Yii::log('No se puede borrar una factura que esta aprobada '.print_r($this,true),'error');
            return false;
        }

        $transaction = Yii::app()->db->getCurrentTransaction();
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
        }

        $oEstadoAnterior = self::model()->findByPk($this->id);

        $bReturn = parent::beforeDelete();
        if ($bReturn){
            $oEstadoAnterior->oCliente->saldoActual = $oEstadoAnterior->oCliente->saldoActual - $oEstadoAnterior->Total_Final;
            if (!$oEstadoAnterior->oCliente->save()){
                $transaction->rollback();
                return false;
            }
        }else{
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return $bReturn;
    }

}