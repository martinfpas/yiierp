<?php

/**
 * This is the model class for table "materiaPrima".
 *
 * The followings are the available columns in table 'materiaPrima':
 * @property integer $id
 * @property integer $id_materia_prima
 * @property integer $id_rubro
 * @property string $Descripcion
 * @property integer $id_articulo
 * @property integer $Id_Proveedor
 * @property string $Unidad_Medida
 * @property double $Precio
 * @property string $fecha_act_precio
 *
 * The followings are the available model relations:
 * @property Proveedor $oProveedor
 */
class MateriaPrima extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'materiaPrima';
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_materia_prima, id_rubro, Descripcion, id_articulo, Id_Proveedor, Unidad_Medida', 'required'),
			array('id_materia_prima, id_rubro, id_articulo, Id_Proveedor', 'numerical', 'integerOnly'=>true),
			array('Precio', 'numerical'),
			array('Descripcion', 'length', 'max'=>40),
			array('Unidad_Medida', 'length', 'max'=>4),
			array('fecha_act_precio', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_materia_prima, id_rubro, Descripcion, id_articulo, Id_Proveedor, Unidad_Medida, Precio, fecha_act_precio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'Id_Proveedor'),
		);
	}

    public function scopes()
    {
        return array(
            'todas'=>array(
                'order'=>'Descripcion asc',
            ),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Materia Prima',
            'models' => 'Materia Primas',
			'id' => Yii::t('application', 'ID'),
			'id_materia_prima' => Yii::t('application', 'Id Materia Prima'),
			'id_rubro' => Yii::t('application', 'Id Rubro'),
			'Descripcion' => Yii::t('application', 'Descripcion'),
			'id_articulo' => Yii::t('application', 'Id Articulo'),
			'Id_Proveedor' => Yii::t('application', 'Id Proveedor'),
			'Unidad_Medida' => Yii::t('application', 'Unidad Medida'),
			'Precio' => Yii::t('application', 'Precio'),
			'fecha_act_precio' => Yii::t('application', 'Fecha Act Precio'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_materia_prima',$this->id_materia_prima);
		$criteria->compare('id_rubro',$this->id_rubro);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('id_articulo',$this->id_articulo);
		$criteria->compare('Id_Proveedor',$this->Id_Proveedor);
		$criteria->compare('Unidad_Medida',$this->Unidad_Medida,true);
		$criteria->compare('Precio',$this->Precio);
		$criteria->compare('fecha_act_precio',$this->fecha_act_precio,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MateriaPrima the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
