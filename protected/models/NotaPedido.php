<?php

/**
 * This is the model class for table "notaPedido".
 *
 * The followings are the available columns in table 'notaPedido':
 * @property integer $id
 * @property string $nroCombrobante
 * @property integer $dig
 * @property integer $idCliente
 * @property integer $camion
 * @property string $fecha
 * @property integer $idCarga
 * @property integer $impSinPrecio
 *
 * * The followings are the available model relations:
 * @property NotaEntrega $oNotaEntrega
 * @property Factura $oFactura
 * @property Cliente $oCliente
 * @property Vehiculo $oCamion
 * @property Carga $oCarga
 * @property ProdNotaPedido[] $aProdNotaPedido
 */
class NotaPedido extends CActiveRecord
{
    public $sViajante = null;
    public $estadoCarga;

    const iSi = 1;
    const iNo = 0;

    public static $aSiNo = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'notaPedido';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nroCombrobante, dig, idCliente, fecha', 'required'),
            array('dig, Nro_Viajante, idCliente, camion, Orden_Carga, idCarga, impSinPrecio', 'numerical', 'integerOnly'=>true),
            array('clase', 'length', 'max'=>1),
            array('nroCombrobante', 'length', 'max'=>6),
            array('impSinPrecio' ,'safe'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, clase, nroCombrobante, dig, Nro_Viajante, idCliente, camion, Orden_Carga, fecha, idCarga, impSinPrecio, sViajante, estadoCarga', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oNotaEntrega'  => array(self::HAS_ONE, 'NotaEntrega', 'idNotaPedido'),
            'oFactura'      => array(self::HAS_ONE, 'Factura', 'idNotaPedido'),
			'oCliente'      => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
			'oCamion'       => array(self::BELONGS_TO, 'Vehiculo', 'camion'),
            'oCarga'        => array(self::BELONGS_TO, 'Carga', 'idCarga'),
			'aProdNotaPedido' => array(self::HAS_MANY, 'ProdNotaPedido', 'idNotaPedido'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
    public function attributeLabels()
    {
        return array(
            'model' => 'Nota De Pedido',
            'models' => 'Notas De Pedido',
            'id' => 'ID',
            'clase' => 'Clase',
            'nroCombrobante' => 'Nro Combrobante',
            'dig' => 'Dig',
            'Nro_Viajante' => 'Nro Viajante',
            'idCliente' => 'Id Cliente',
            'camion' => 'Camion',
            'Orden_Carga' => 'Orden Carga',
            'fecha' => 'Fecha',
            'idCarga' => 'Id Carga',
            'sViajante' => 'Viajante',
            'impSinPrecio' => 'Imp Sin Precio',
        );
    }

    public function getNroDig(){
        return str_pad(8,'0',$this->nroCombrobante).'-'.$this->dig;
    }

	public function getSubTotal(){
	    $fTotal = 0;
	    foreach ($this->aProdNotaPedido as $index => $oProdNotaPedido) {
            $fTotal += $oProdNotaPedido->getSubTotal();
	    }
	    return number_format($fTotal,2);
    }
    public function getCostoTotal(){
        $fTotal = 0;
        foreach ($this->aProdNotaPedido as $index => $oProdNotaPedido) {
            $fTotal += $oProdNotaPedido->getCostoTotal();
        }
        return number_format($fTotal,2);
    }

    /**
     * @param int $idCarga
     */
    public function saveIdCarga($idCarga)
    {
        //Yii::log('saveIdCarga :: $idCarga > '.$idCarga.' ------'.intval($idCarga),'warning');

        $id = intval($idCarga);

        if($idCarga =! null){
            $this->idCarga = $id ;
            if ($this->save()){
                // RECORRE LOS ITEMS
                //Yii::log('RECORRER LOS ITEMS =>>> '.sizeof($this->aProdNotaPedido),'warning');
                foreach ($this->aProdNotaPedido as $index => $oProdNotaPedido) {
                    //Yii::log('crearProdCarga :: $idCarga >>> '.$id,'warning');
                    $oProdNotaPedido->crearProdCarga();
                    //Yii::log('crearProdCarga !!!! '.$id,'warning');
                }
            }else{
                Yii::log('saveIdCarga :: $this->getErrors() :: >'.intval($idCarga).'< '.CHtml::errorSummary($this),'error');
                //return $this->getErrors();
                //echo CHtml::errorSummary($this);
            }
        }
    }

    /*
     *
     * */
    public function getPesoTotal(){
        $fPesoTotal = 0;
        foreach ($this->aProdNotaPedido as $index => $oProdNotaPedido) {
            //$unidades = $oProdNotaPedido->cantidad * $oProdNotaPedido->cantXbulto;
            $unidades = $oProdNotaPedido->cantidad;
            $oUnidadDeMedida = Udm::model()->findByPk($oProdNotaPedido->oArtVenta->uMedida);
            $unidadesXkilo = 1;
            if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
                $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
            }
            $kilosXunidad = $oProdNotaPedido->oArtVenta->contenido / $unidadesXkilo;

            $fPesoTotal += $kilosXunidad * $unidades;
        }
        return number_format($fPesoTotal,2,'.','') ;
    }

    public static function getSumaPeso($aIdNotas){
        $fSumaPeso = 0;
        foreach ($aIdNotas as $index ) {
            $oNota = NotaPedido::model()->findByPk($index);
            $fSumaPeso += $oNota->getPesoTotal();
        }
        return number_format($fSumaPeso,2,'.','');
    }

    public function getTitulo(){
        return str_pad($this->nroCombrobante,8,0,STR_PAD_LEFT).'-'.str_pad($this->dig,4,0,STR_PAD_LEFT);
    }

	/**
 * Retrieves a list of models based on the current search/filter conditions.
 *
 * Typical usecase:
 * - Initialize the model fields with values from filter form.
 * - Execute this method to get CActiveDataProvider instance which will filter
 * models according to data in model fields.
 * - Pass data provider to CGridView, CListView or any similar widget.
 *
 * @return CActiveDataProvider the data provider that can return the models
 * based on the search/filter conditions.
 */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oCarga');

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.clase',$this->clase,true);
        $criteria->compare('t.nroCombrobante',$this->nroCombrobante,true);
        $criteria->compare('t.dig',$this->dig);
        $criteria->compare('t.Nro_Viajante',$this->Nro_Viajante);
        $criteria->compare('t.idCliente',$this->idCliente);
        $criteria->compare('t.camion',$this->camion);
        $criteria->compare('t.Orden_Carga',$this->Orden_Carga);
        if ($this->fecha != null){
            $criteria->compare('t.fecha',ComponentesComunes::jpicker2db($this->fecha));
        }

        $criteria->compare('oCarga.estado',$this->estadoCarga);

        $criteria->compare('t.idCarga',$this->idCarga);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    '*',
                    'estadoCarga' => array(
                        'asc' => 'oCarga.estado asc',
                        'desc' => 'oCarga.estado desc',
                    ),
                ),
            ),
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWp()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oCliente');

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.clase',$this->clase,true);
        $criteria->compare('t.dig',$this->dig);
        $criteria->compare('t.Nro_Viajante',$this->Nro_Viajante);
        $criteria->compare('t.idCliente',$this->idCliente);
        $criteria->compare('t.camion',$this->camion);
        $criteria->compare('t.Orden_Carga',$this->Orden_Carga);
        if ($this->fecha != null ){
            $criteria->compare('fecha',ComponentesComunes::jpicker2db($this->fecha));
        }
        $criteria->compare('idCarga',$this->idCarga);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'attributes' => array(
                    'idCliente'=> array(
                        'asc' => 'oCliente.razonSocial asc',
                        'desc' => 'oCliente.razonSocial desc',
                    ),
                ),
            ),
        ));
    }

    public function searchSinCarga($idCamion = null)
    {
        $criteria=new CDbCriteria;
        $criteria->with = array('oCliente','oCliente.oViajante');

        if ($idCamion != null){
            $criteria->condition = 't.camion='.$this->camion.' and t.idCarga IS NULL ';

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => false,
                'pagination' => false,
            ));
        }else{
            $criteria->compare('t.id',$this->id);
            $criteria->compare('t.clase',$this->clase,true);
            $criteria->compare('t.nroCombrobante',$this->nroCombrobante,true);
            $criteria->compare('t.dig',$this->dig);
            $criteria->compare('t.Nro_Viajante',$this->Nro_Viajante);
            $criteria->compare('t.idCliente',$this->idCliente);
            $criteria->compare('t.camion',$this->camion);
            $criteria->compare('t.Orden_Carga',$this->Orden_Carga);

            if ($this->fecha != null ){
                $criteria->compare('t.fecha',ComponentesComunes::jpicker2db($this->fecha));
            }

            $criteria->addCondition('t.idCarga IS NULL ');

            if ($this->sViajante != null ){
                $criteria->addCondition('oViajante.numero = '.$this->sViajante);
            }

            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => array(
                    'defaultOrder' => 't.id desc',

                ),
                'pagination' => false,
            ));
        }



    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NotaPedido the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function beforeSave(){
	    $bForzarCambio = false;
        $oEstadoAnterior = NotaPedido::model()->findByPk($this->id);

        if ($this->idCarga > 0 && $this->oCarga != null ){

            //Yii::log(ComponentesComunes::print_array($oEstadoAnterior,false),'warning');
            if($oEstadoAnterior->camion != $this->camion || $oEstadoAnterior->idCarga != $this->idCarga){
                $bForzarCambio = true;
                if($this->oNotaEntrega != null){
                    $oNotaEntrega = $this->oNotaEntrega;
                    $oNotaEntrega->idCamion = $this->camion;
                    if(!$oNotaEntrega->save()){
                        Yii::log(ComponentesComunes::print_array($oNotaEntrega->getErrors(),false),'error');
                    }
                }elseif ($this->oFactura != null){
                    $oFactura = $this->oFactura;
                    $oFactura->Id_Camion = $this->camion;
                    if(!$oFactura->save()){
                        Yii::log(ComponentesComunes::print_array($oFactura->getErrors(),false),'error');
                    }
                }else{

                }
            }
            if ($this->oCarga->estado != Carga::iBorrador ){
                // TODO: CHEQUEAR
                if(!$bForzarCambio && $oEstadoAnterior->camion == $this->camion){
                    throw new Exception('La Nota de Pedido tiene una carga ya facturada. No se puede modificar');
                    return false;
                }
                $bForzarCambio = true;
            }
        }else{
            $bForzarCambio = true;
        }

        if (!$bForzarCambio && ($this->oFactura != null || $this->oNotaEntrega != null)){
            throw new Exception('La Nota ya fue facturada. No se puede modificar');
        }

        return parent::beforeSave();
    }

}
