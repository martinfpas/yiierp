<?php

/**
 * This is the model class for table "chequePropio".
 *
 * The followings are the available columns in table 'chequePropio':
 * @property integer $id
 * @property integer $nroCheque
 * @property string $fecha
 * @property integer $id_cuenta
 * @property double $importe
 * @property string $fechaDebito
 * @property string $destinatario
 *
 * The followings are the available model relations:
 * @property CuentasBancarias $oCuenta
 * @property ChequePropioMovBancario[] $aChequePropioMovBancario
 * @property PagoProveedorChequePpio[] $aPagoProveedorChequePpio
 */
class ChequePropio extends CActiveRecord
{
    public $fechaDesde = null;
    public $fechaHasta = null;
    public $sinPagosAsoc = true;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chequePropio';
	}

    public function scopes()
    {
        return array(
            'cuentasActivas'=>array(
                'with' => 'oCuenta',
                'condition'=>'oCuenta.fechaCierre IS NULL',
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nroCheque, fecha, id_cuenta', 'required'),
			array('nroCheque, id_cuenta', 'numerical', 'integerOnly'=>true),
            array('nroCheque', 'length', 'max'=>8),
			array('destinatario','length','max'=>70),
			array('importe', 'numerical'),
			array('fechaDebito', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nroCheque, fecha, id_cuenta, importe, fechaDebito,destinatario,fechaDesde,fechaHasta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCuenta' => array(self::BELONGS_TO, 'CuentasBancarias', 'id_cuenta'),
            'aChequePropioMovBancario' => array(self::HAS_MANY, 'ChequePropioMovBancario', 'idChequePropio'),
            'aPagoProveedorChequePpio' => array(self::HAS_MANY, 'PagoProveedorChequePpio', 'idChequePpio'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Cheque Propio',
            'models' => 'Cheques Propios',
			'id' => Yii::t('application', 'ID'),
			'nroCheque' => Yii::t('application', 'Nro Cheque'),
			'fecha' => Yii::t('application', 'Fecha'),
			'id_cuenta' => Yii::t('application', 'Id Cuenta'),
			'importe' => Yii::t('application', 'Importe'),
			'fechaDebito' => Yii::t('application', 'Fecha Debito'),
            'destinatario' => 'Destinatario',
		);
	}

	public function revisarMovimiento(){
		if(sizeof($this->aChequePropioMovBancario) == 0){
			echo '0 >'.$this->fechaDebito.'<';
			$criteria = new CDbCriteria();
			$criteria->addCondition('t.idCuenta ='.$this->id_cuenta);
			$criteria->addCondition('t.fechaMovimiento ="'.$this->fechaDebito.'"');
			$criteria->addCondition('t.Importe ='.$this->importe);
			// MEDIO => CHEQUE
			$criteria->addCondition('t.idMedio = 2');
			$aMovimentos = MovimientoCuentasBanc::model()->findAll($criteria);
			foreach($aMovimentos as $key => $oMovimento){
				if(sizeof($oMovimento->aChequePropioMovBancario) == 0){
					$oChequePropioMovBancario = new ChequePropioMovBancario();
					$oChequePropioMovBancario->idChequePropio = $this->id;
					$oChequePropioMovBancario->idMovimiento = $oMovimento->id;
					if(!$oChequePropioMovBancario->save()){
						throw new CHttpException('500',CHtml::errorSummary($oChequePropioMovBancario));
					}
				}
			}			
		}
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($paginar = true)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.nroCheque',$this->nroCheque);
        $criteria->compare('t.destinatario',$this->destinatario,true);
        if ($this->fecha != ''){
            $criteria->compare('t.fecha',ComponentesComunes::fechaComparada($this->fecha));
        }
		$criteria->compare('t.id_cuenta',$this->id_cuenta);
		$criteria->compare('t.importe',$this->importe);

        if ($this->fechaDebito != ''){
            $criteria->compare('t.fechaDebito',ComponentesComunes::fechaComparada($this->fechaDebito));
        }

        if ($this->fechaDesde != null) {
            $criteria->addCondition("t.fecha >= '".ComponentesComunes::jpicker2db($this->fechaDesde)." 00:00:00'");
        }

        if ($this->fechaHasta != null) {
            $criteria->addCondition("t.fecha <= '".ComponentesComunes::jpicker2db($this->fechaHasta,false)."  23:59:59'");
        }


        if(!$paginar){
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'sort' => array(
                    'defaultOrder' => 't.fecha desc',
                    'attributes' => array(
                        '*'
                    )
                ),
                'pagination' => false,
            ));
        }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.fecha desc',
                'attributes' => array(
                    '*'
                )
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ChequePropio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeDelete(){
		$oEstadoAnterior = self::model()->with('aChequePropioMovBancario')->findByPk($this->id);
		

		foreach($oEstadoAnterior->aChequePropioMovBancario as $key => $oChequePropioMovBancario){
			$oChequePropioMovBancario->oMovimiento->delete();
			$oChequePropioMovBancario->delete();
		}
		$result = parent::beforeDelete();

		return $result;
	}

	public function beforeSave(){
	    $oEstadoAnterior = self::model()->with('aChequePropioMovBancario')->findByPk($this->id);
		$oEstadoPreSave = $this;
        $bReturn = parent::beforeSave();
        if($bReturn && $oEstadoPreSave->id == null && $this->fechaDebito != null ){

            $oMovimiento = new MovimientoCuentasBanc();
            $oMovimiento->idCuenta = $this->id_cuenta;
            $oMovimiento->ingreso = 0;
            $oMovimiento->Importe = $this->importe;
            $oMovimiento->idMedio = 2;// cheque
            $oMovimiento->tipoMovimiento = 4; //credito
            $oMovimiento->fechaMovimiento = ComponentesComunes::jpicker2db($this->fechaDebito);
            $oMovimiento->Observacion = 'CHEQUE '.$this->nroCheque.' '.$this->destinatario;
            if ($oMovimiento->save()){
				$lastCheque = ChequePropio::model()->find(['order' => 'id desc']);
                $oChequePropioMovBancario = new ChequePropioMovBancario();
                //$oChequePropioMovBancario->idChequePropio = ($lastCheque)? $lastCheque->id : null;
				$oChequePropioMovBancario->idChequePropio = $this->id;
                $oChequePropioMovBancario->idMovimiento = $oMovimiento->id;
                if (!$oChequePropioMovBancario->save()){
                    //TODO: TIRAR ERROR
                }
            }
        }else{
			if(sizeof($oEstadoAnterior->aChequePropioMovBancario) == 1){
				$oMovimiento = $oEstadoAnterior->aChequePropioMovBancario[0]->oMovimiento;
				$oMovimiento->Importe = $this->importe;
				$oMovimiento->fechaMovimiento = ComponentesComunes::jpicker2db($this->fechaDebito);
				if(!$oMovimiento->save()){
					
				}
			}
		}

	    return $bReturn;
    }
}
