<?php

/**
 * This is the model class for table "clasifMovimientoCuentas".
 *
 * The followings are the available columns in table 'clasifMovimientoCuentas':
 * @property integer $codigo
 * @property string $Descripcion
 * @property integer $Ingreso
 */
class ClasifMovimientoCuentas extends CActiveRecord
{

    const iSi = 1;
    const iNo = 0;

    public static $aSiNo = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clasifMovimientoCuentas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Descripcion', 'required'),
			array('Ingreso', 'numerical', 'integerOnly'=>true),
			array('Descripcion', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigo, Descripcion, Ingreso', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Tipo Movimiento Cuentas',
            'models' => 'Tipos Movimiento Cuentas',
			'codigo' => Yii::t('application', 'Codigo'),
			'Descripcion' => Yii::t('application', 'Descripcion'),
			'Ingreso' => Yii::t('application', 'Ingreso'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('codigo',$this->codigo);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Ingreso',$this->Ingreso);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClasifMovimientoCuentas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
