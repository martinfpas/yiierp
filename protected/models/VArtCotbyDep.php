<?php

/**
 * This is the model class for table "vArtCotbyDep".
 *
 * The followings are the available columns in table 'vArtCotbyDep':
 * @property integer $idDepuracion
 * @property string $codCot
 * @property integer $idArtVenta
 * @property string $descripcion
 * @property integer $cantidad
 * @property double $contenido
 * @property string $id
 */
class VArtCotbyDep extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vArtCotbyDep';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idDepuracion, idArtVenta', 'required'),
			array('idDepuracion, idArtVenta, cantidad', 'numerical', 'integerOnly'=>true),
			array('contenido', 'numerical'),
			array('codCot', 'length', 'max'=>6),
			array('descripcion', 'length', 'max'=>50),
			array('id', 'length', 'max'=>23),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idDepuracion, codCot, idArtVenta, descripcion, cantidad, contenido, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vart Cotby Dep',
            'models' => 'Vart Cotby Deps',
			'idDepuracion' => Yii::t('application', 'Id Depuracion'),
			'codCot' => Yii::t('application', 'Cod Cot'),
			'idArtVenta' => Yii::t('application', 'Id Art Venta'),
			'descripcion' => Yii::t('application', 'Descripcion'),
			'cantidad' => Yii::t('application', 'Cantidad'),
			'contenido' => Yii::t('application', 'Contenido'),
			'id' => Yii::t('application', 'ID'),
		);
	}

    function getCodCot(){
        return ($this->codCot != "")? $this->codCot : "000000";
    }

    function getKgCarga(){

        $oDepuracion = Depuracion::model()->findByPk($this->idDepuracion);
        if ($oDepuracion != null){
            return $oDepuracion->getPesoTotal();
        }
        return 0;
    }

    public function getKilos(){
        $unidades = $this->cantidad;
        $oArtVenta = ArticuloVenta::model()->findByPk($this->idArtVenta);
        $oUnidadDeMedida = Udm::model()->findByPk($oArtVenta->uMedida);
        $unidadesXkilo = 1;
        if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
            $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
        }
        $kilosXunidad = number_format(($oArtVenta->contenido / $unidadesXkilo),2);

        // TODO: ASUMO QUE TIENEN EL MISMO PESO QUE EL AGUA
        return  number_format($kilosXunidad * $unidades,2); //$this->contenido / 1000 * $this->cantidad;
    }

    public function getContenido(){
        $oArtVenta = ArticuloVenta::model()->findByPk($this->idArtVenta);
        $oUnidadDeMedida = Udm::model()->findByPk($oArtVenta->uMedida);
        $unidadesXkilo = 1;
        if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
            $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
        }
        $kilosXunidad = $oArtVenta->contenido / $unidadesXkilo;

        // TODO: ASUMO QUE TIENEN EL MISMO PESO QUE EL AGUA
        return  $kilosXunidad; //$this->contenido / 1000 * $this->cantidad;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idDepuracion',$this->idDepuracion);
		$criteria->compare('codCot',$this->codCot,true);
		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('contenido',$this->contenido);
		$criteria->compare('id',$this->id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchNoGroupWp()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        //$criteria->compare('id',$this->id);
        $criteria->compare('idDepuracion',$this->idDepuracion);
        //$criteria->compare('idArtVenta',$this->idArtVenta);
        //$criteria->compare('idPaquete',$this->idPaquete);
        //$criteria->compare('cantidad',$this->cantidad);
        //$criteria->compare('codCot',$this->codCot,true);
        //$criteria->compare('descripcion',$this->descripcion,true);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.descripcion'
            ),
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWp()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('idDepuracion',$this->idDepuracion);
        $criteria->select = "codCot,descripcion";
        $criteria->group = "codCot,descripcion";

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.descripcion'
            ),
        ));
    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VArtCotbyDep the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
