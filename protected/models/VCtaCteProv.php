<?php

/**
 * This is the model class for table "vCtaCteProv".
 *
 * The followings are the available columns in table 'vCtaCteProv':
 * @property string $cp_id
 * @property string $rp_id
 * @property string $TipoComprobante
 * @property string $Nro_Comprobante
 * @property integer $id_Proveedor
 * @property string $fecha
 * @property double $TotalNeto
 */
class VCtaCteProv extends CActiveRecord
{

    public static $aClase = array(
        '001' => 'REMITO',
        '001' => 'FACTURAS A',
        '081' => 'TIQUE FACTURA A',
        '002' => 'NOTAS DE DEBITO A',
        '003' => 'NOTAS DE CREDITO A',
        '004' => 'RECIBOS A',
        '011' => 'FACTURAS C',
        '015' => 'RECIBOS C',
        '118' => 'TIQUE FACTURA M',
        '006' => 'FACTURAS B',
        '008' => 'NOTA DE CREDITO B',
        '007' => 'NOTA DE DEBITO B',
    );

    public static $aFactorMultip = array(
        '000' => 1,
        '001' => 1,
        '081' => 1,
        '002' => 1,
        '003' => -1,
        '004' => -1,
        //'004' => 0,
        '011' => 1,
        '015' => -1,
        //'015' => 0,
        '118' => 1,
        '006' => 1,
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vCtaCteProv';
	}

    public function primaryKey(){

        return 'cp_id';

    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha', 'required'),
			array('id_Proveedor', 'numerical', 'integerOnly'=>true),
			array('TotalNeto', 'numerical'),
			array('cp_id, rp_id', 'length', 'max'=>11),
			array('TipoComprobante', 'length', 'max'=>3),
			array('Nro_Comprobante', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cp_id, rp_id, TipoComprobante, Nro_Comprobante, id_Proveedor, fecha, TotalNeto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vcta Cte Prov',
            'models' => 'Vcta Cte Provs',
			'cp_id' => Yii::t('application', 'Cp'),
			'rp_id' => Yii::t('application', 'Rp'),
			'TipoComprobante' => Yii::t('application', 'Tipo Comprobante'),
			'Nro_Comprobante' => Yii::t('application', 'Nro Comprobante'),
			'id_Proveedor' => Yii::t('application', 'Id Proveedor'),
			'fecha' => Yii::t('application', 'Fecha'),
			'TotalNeto' => Yii::t('application', 'Total Neto'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cp_id',$this->cp_id,true);
		$criteria->compare('rp_id',$this->rp_id,true);
		$criteria->compare('TipoComprobante',$this->TipoComprobante,true);
		$criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
		$criteria->compare('id_Proveedor',$this->id_Proveedor);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('TotalNeto',$this->TotalNeto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCtaCteProv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
