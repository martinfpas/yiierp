<?php

/**
 * This is the model class for table "categoriaIva".
 *
 * The followings are the available columns in table 'categoriaIva':
 * @property integer $id_categoria
 * @property string $nombre
 * @property string $descrip
 * @property integer $discIva
 * @property string $tipoFact
 * @property string $fechaVigencia
 * @property integer $nCopias
 * @property string $catCorto
 * @property double $alicuota
 * @property integer $agregaIva
 *
 * The followings are the available model relations:
 * @property Cliente[] $aCliente
 * @property Proveedor[] $aProveedor
 */
class CategoriaIva extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CategoriaIva the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categoriaIva';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombre, alicuota', 'required'),
			array('agregaIva,discIva, nCopias', 'numerical', 'integerOnly'=>true),
			array('alicuota', 'numerical'),
			array('nombre, descrip', 'length', 'max'=>30),
			array('agregaIva,tipoFact', 'length', 'max'=>1),
			array('catCorto', 'length', 'max'=>5),
			array('fechaVigencia', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_categoria, nombre, descrip, discIva, tipoFact, fechaVigencia, nCopias, catCorto, alicuota, agregaIva', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aCliente' => array(self::HAS_MANY, 'Cliente', 'categoriaIva'),
			'aProveedor' => array(self::HAS_MANY, 'Proveedor', 'ivaResponsable'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id_categoria' => Yii::t('application', 'Id Categoria'),
		            'nombre' => Yii::t('application', 'Nombre'),
		            'descrip' => Yii::t('application', 'Descrip'),
		            'discIva' => Yii::t('application', 'Disc Iva'),
		            'tipoFact' => Yii::t('application', 'Tipo Fact'),
		            'fechaVigencia' => Yii::t('application', 'Fecha Vigencia'),
		            'nCopias' => Yii::t('application', 'N Copias'),
		            'catCorto' => Yii::t('application', 'Cat Corto'),
		            'alicuota' => Yii::t('application', 'Alicuota'),
		    );
	}

    public function scopes()
    {
        return array(
            'todas'=>array(
                'order'=>'nombre asc',
            ),
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_categoria',$this->id_categoria);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('descrip',$this->descrip,true);
		$criteria->compare('discIva',$this->discIva);

		$criteria->compare('tipoFact',$this->tipoFact,true);

        if ($this->fechaVigencia != null){
            $criteria->compare('fechaVigencia',ComponentesComunes::jpicker2db($this->fechaVigencia));
        }


		$criteria->compare('nCopias',$this->nCopias);
		$criteria->compare('catCorto',$this->catCorto,true);
		$criteria->compare('alicuota',$this->alicuota);
		$criteria->compare('discIva',$this->agregaIva);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}