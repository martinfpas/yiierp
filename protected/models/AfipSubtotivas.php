<?php

/**
 * This is the model class for table "afipSubtotivas".
 *
 * The followings are the available columns in table 'afipSubtotivas':
 * @property integer $id
 * @property integer $codigo // CODIGO RELACIONADO EN AfipVoucher::$aIvaAlicuotas
 * @property double $BaseImp
 * @property double $importe
 * @property integer $idVoucher
 *
 * The followings are the available model relations:
 * @property AfipVoucher $oIdVoucher
 */
class AfipSubtotivas extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'afipSubtotivas';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, BaseImp, importe, idVoucher', 'required'),
			array('codigo, idVoucher', 'numerical', 'integerOnly'=>true),
			array('BaseImp, importe', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo, BaseImp, importe, idVoucher', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdVoucher' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucher'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'BaseImp' => 'Base Imp',
			'importe' => 'Importe',
			'idVoucher' => 'Id Voucher',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo);
		$criteria->compare('BaseImp',$this->BaseImp);
		$criteria->compare('importe',$this->importe);
		$criteria->compare('idVoucher',$this->idVoucher);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AfipSubtotivas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
