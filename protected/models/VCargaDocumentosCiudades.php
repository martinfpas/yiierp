<?php

/**
 * This is the model class for table "vCargaDocumentosCiudades".
 *
 * The followings are the available columns in table 'vCargaDocumentosCiudades':

 * @property integer $idEntregaDoc
 * @property string $nombrelocalidad
 * @property string $codigoPostal
 * @property string $zp
 */
class VCargaDocumentosCiudades extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public $sortOrder;
	public function tableName()
	{
		return 'vCargaDocumentosCiudades2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEntregaDoc', 'required'),
			array('id, idEntregaDoc, idCliente', 'numerical', 'integerOnly'=>true),
			array('razonSocial', 'length', 'max'=>40),
			array('nombrelocalidad', 'length', 'max'=>65),
			array('codigoPostal, zp', 'length', 'max'=>4),
            array('sortOrder', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idEntregaDoc, idCliente, razonSocial, nombrelocalidad, codigoPostal, zp,orden', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function primaryKey(){

        return 'idEntregaDoc';

    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			//'id' => 'ID',
			'idEntregaDoc' => 'Id Entrega Doc',
			'idCliente' => 'Id Cliente',
			'razonSocial' => 'Razon Social',
			'nombrelocalidad' => 'Nombrelocalidad',
			'codigoPostal' => 'Codico Postal',
			'zp' => 'Zp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->compare('id',$this->id);
		$criteria->compare('idEntregaDoc',$this->idEntregaDoc);
		$criteria->compare('nombrelocalidad',$this->nombrelocalidad,true);
		$criteria->compare('codigoPostal',$this->codigoPostal,true);
		$criteria->compare('zp',$this->zp,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'nombrelocalidad',
            ),

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCargaDocumentosCiudades the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
