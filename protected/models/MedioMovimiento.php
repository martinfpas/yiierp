<?php

/**
 * This is the model class for table "medioMovimiento".
 *
 * The followings are the available columns in table 'medioMovimiento':
 * @property integer $CodMedio
 * @property string $Descripcion
 * @property integer $Demora
 * @property integer $activo
 */
class MedioMovimiento extends CActiveRecord
{
    const iInactivo = 0;
    const iActivo = 1;

    const inactivo = 'Inactivo';
    const activo = 'Activo';

    public static $aEstado = array(
        self::iInactivo => self::inactivo,
        self::iActivo => self::activo,
    );


    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'medioMovimiento';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('CodMedio, Descripcion', 'required'),
            array('CodMedio, Demora, activo', 'numerical', 'integerOnly'=>true),
            array('Descripcion', 'length', 'max'=>20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('CodMedio, Descripcion, Demora, activo', 'safe', 'on'=>'search'),
        );
    }

    public function scopes()
    {
        return array(
            'activos'=>array(
                'condition'=>'activo='.self::iActivo,
            ),
            'inactivos'=>array(
                'condition'=>'activo='.self::iInactivo,
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'aMovimientoCuentasBanc' => array(self::HAS_MANY, 'MovimientoCuentasBanc', 'idMedio'),
        );
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Medio Movimiento',
            'models' => 'Medio Movimientos',
			'CodMedio' => Yii::t('application', 'Cod Medio'),
			'Descripcion' => Yii::t('application', 'Descripcion'),
			'Demora' => Yii::t('application', 'Demora'),
            'activo' => Yii::t('application', 'Activo'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('CodMedio',$this->CodMedio);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('Demora',$this->Demora);
        $criteria->compare('activo',$this->activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MedioMovimiento the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // EN LUGAR DE BORRAR, LO PASA A INACTIVO PARA NO PERDER HISTORICO, Y NO GENERAR INCONSISTENCIAS
    public function beforeDelete(){
        $this->activo = self::iInactivo;
        $this->save();
        return false;
    }
}
