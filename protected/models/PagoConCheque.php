<?php

/**
 * This is the model class for table "pagoConCheque".
 *
 * The followings are the available columns in table 'pagoConCheque':
 * @property integer $id
 * @property double $monto
 * @property integer $idChequedeTercero
 * @property integer $idPagodeCliente
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros $oChequedeTercero
 * @property PagoCliente $oPagodeCliente
 */
class PagoConCheque extends CActiveRecord
{
	public $recalcOnDelete = true;
	public $table = 'pagoConCheque';
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return $this->table;
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monto, idChequedeTercero, idPagodeCliente', 'required'),
			array('idChequedeTercero, idPagodeCliente', 'numerical', 'integerOnly'=>true),
            array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monto, idChequedeTercero, idPagodeCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oChequedeTercero' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'idChequedeTercero'),
			'oPagodeCliente' => array(self::BELONGS_TO, 'PagoCliente', 'idPagodeCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'monto' => 'Monto',
			'idChequedeTercero' => 'Id Chequede Tercero',
			'idPagodeCliente' => 'Id Pagode Cliente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
        $criteria->compare('monto',$this->monto);
		$criteria->compare('idChequedeTercero',$this->idChequedeTercero);
		$criteria->compare('idPagodeCliente',$this->idPagodeCliente);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

		$criteria->with = array('oChequedeTercero','oChequedeTercero.oSucursal');
		
		//$criteria->select = array('"dddddddddddddddddddddd" as recalcOnDelete');

        
        $criteria->compare('t.monto',$this->monto);
        $criteria->compare('t.idChequedeTercero',$this->idChequedeTercero);
        $criteria->compare('t.idPagodeCliente',$this->idPagodeCliente);

		if($this->id == -1){
			$criteria->limit = 1;
		}else{
			$criteria->compare('t.id',$this->id);
		}

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
	}
	
	public function searchWPUnion()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

		
        $criteria=new CDbCriteria;

		$criteria->select = 't.id as idddd';

        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.monto',$this->monto);
        $criteria->compare('t.idChequedeTercero',$this->idChequedeTercero);
        $criteria->compare('t.idPagodeCliente',$this->idPagodeCliente);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    protected function beforeSave()
    {
        //$model->importe = number_format($model->importe,2,'.','');
        Yii::log('$this->monto pre'.$this->monto,'warning');

        return parent::beforeSave();
    }

    protected function afterSave()
    {
        //$model->importe = number_format($model->importe,2,'.','');
        Yii::log('$this->monto pos'.$this->monto,'warning');

        $this->oPagodeCliente->recalcular();
        return parent::afterSave();
    }

    public function afterDelete(){

	    Yii::log(':: idChequedeTercero '.$this->idChequedeTercero,'warning');
	    if (sizeof($this->oChequedeTercero->aPagoConCheque) == 0){
            Yii::log(':: $this->oChequedeTercero->aPagoConCheque) == 0 ','warning');
            try{
                $this->oChequedeTercero->delete();
            }catch (Exception $e){
                    
            }
        }

	    $oPagoCliente = $this->oPagodeCliente;
        //$monto = $this->monto;
        $bool = parent::afterDelete();
        if ($this->recalcOnDelete){
            Yii::log(':: recalcOnDelete '.$this->id,'warning');
            $oPagoCliente->recalcular();
        }
        return $bool;
    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoConCheque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
