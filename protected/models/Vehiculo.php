<?php

/**
 * This is the model class for table "vehiculo".
 *
 * The followings are the available columns in table 'vehiculo':
 * @property integer $id_vehiculo
 * @property string $nroDocumento
 * @property string $marca
 * @property string $modelo
 * @property string $patente
 * @property double $tara
 * @property double $capMaxima
 */
class Vehiculo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vehiculo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_vehiculo', 'required'),
			array('id_vehiculo', 'numerical', 'integerOnly'=>true),
			array('tara, capMaxima', 'numerical'),
			array('nroDocumento', 'length', 'max'=>11),
			array('marca, modelo', 'length', 'max'=>40),
			array('patente', 'length', 'max'=>8),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_vehiculo, nroDocumento, marca, modelo, patente, tara, capMaxima', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_vehiculo' => 'Id Vehiculo',
			'nroDocumento' => 'Nro Documento',
			'marca' => 'Marca',
			'modelo' => 'Modelo',
			'patente' => 'Patente',
			'tara' => 'Tara',
			'capMaxima' => 'Cap Maxima',
		);
	}

	public function getIdNombre() {
		return $this->id_vehiculo.' - '.$this->patente;
	}


	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_vehiculo',$this->id_vehiculo);
		$criteria->compare('nroDocumento',$this->nroDocumento,true);
		$criteria->compare('marca',$this->marca,true);
		$criteria->compare('modelo',$this->modelo,true);
		$criteria->compare('patente',$this->patente,true);
		$criteria->compare('tara',$this->tara);
		$criteria->compare('capMaxima',$this->capMaxima);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vehiculo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
