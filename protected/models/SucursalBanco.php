<?php

/**
 * This is the model class for table "sucursalBanco".
 *
 * The followings are the available columns in table 'sucursalBanco':
 * @property integer $id
 * @property string $nombre
 * @property integer $id_banco
 * @property string $localidad
 * @property string $provincia
 *
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros[] $aChequeDeTerceros
 * @property Banco $oBanco
 * @property Provincia $oProvincia
 */
class SucursalBanco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SucursalBanco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sucursalBanco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_banco, provincia', 'required'),
			array('id_banco', 'numerical', 'integerOnly'=>true),
			array('nombre, localidad, provincia', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nombre, id_banco, localidad, provincia', 'safe', 'on'=>'search'),
		);
	}

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aChequeDeTerceros' => array(self::HAS_MANY, 'ChequeDeTerceros', 'id_sucursal'),
            'oBanco' => array(self::BELONGS_TO, 'Banco', 'id_banco'),
            'oProvincia' => array(self::BELONGS_TO, 'Provincia', 'provincia'),
		);
	}

    /**
     * @return array customized attribute labels (name=>label)
     */

    public function scopes()
    {
        return array(
            'todas'=>array(
                'order' => 'nombre asc',
            ),
        );
    }

    public function getTitle(){
	    if($this->nombre <> '') {
            return $this->nombre;

        }elseif ($this->localidad <> ''){
	        return $this->localidad;
        }else {
	        return $this->id;
        }
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'nombre' => Yii::t('application', 'Nombre'),
		            'id_banco' => Yii::t('application', 'Id Banco'),
		            'localidad' => Yii::t('application', 'Localidad'),
		            'provincia' => Yii::t('application', 'Provincia'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('localidad',$this->localidad,true);
		$criteria->compare('provincia',$this->provincia,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}