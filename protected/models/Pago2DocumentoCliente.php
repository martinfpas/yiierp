<?php

/**
 * This is the model class for table "pago2DocumentoCliente".
 *
 * The followings are the available columns in table 'pago2DocumentoCliente':
 * @property integer $id
 * @property integer $idPago
 * @property integer $idComprobante
 * @property integer $idNotaEntrega
 * @property string $montoSaldado
 *
 * The followings are the available model relations:
 * @property PagoCliente $oPago
 * @property Comprobante $oComprobante
 * @property NotaEntrega $oNotaEntrega
 */
class Pago2DocumentoCliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pago2DocumentoCliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPago', 'required'),
			array('idPago, idComprobante, idNotaEntrega', 'numerical', 'integerOnly'=>true),
			array('montoSaldado', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idPago, idComprobante, idNotaEntrega, montoSaldado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oPago' => array(self::BELONGS_TO, 'PagoCliente', 'idPago'),
			'oComprobante' => array(self::BELONGS_TO, 'Comprobante', 'idComprobante'),
			'oNotaEntrega' => array(self::BELONGS_TO, 'NotaEntrega', 'idNotaEntrega'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago2 Documento Cliente',
            'models' => 'Pago2 Documento Clientes',
			'id' => Yii::t('application', 'ID'),
			'idPago' => Yii::t('application', 'Id Pago'),
			'idComprobante' => Yii::t('application', 'Id Comprobante'),
			'idNotaEntrega' => Yii::t('application', 'Id Nota Entrega'),
			'montoSaldado' => Yii::t('application', 'Monto Saldado'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idPago',$this->idPago);
		$criteria->compare('idComprobante',$this->idComprobante);
		$criteria->compare('idNotaEntrega',$this->idNotaEntrega);
		$criteria->compare('montoSaldado',$this->montoSaldado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function beforeSave(){
	    $oEstadoAnterior = self::model()->findByPk($this->id);
        $bReturn = parent::beforeSave();
        if($bReturn && $oEstadoAnterior != null && $oEstadoAnterior->id > 0 && $oEstadoAnterior->montoSaldado <> $this->montoSaldado){
			
			if($this->oNotaEntrega != null){
				$oComprobante = $this->oNotaEntrega;
			}else{
				$oComprobante = $this->oComprobante;
			}
			$oComprobante->montoPagado = $oComprobante->montoPagado + $this->montoSaldado - $oEstadoAnterior->montoSaldado;
			if(!$oComprobante->save()){
				//HANDLE ERROR
				Yii::log('Error al guardar modelo en Form de PagoCliente'.CHtml::errorSummary($oComprobante),'error');
			}
		}
		return $bReturn;
	}

	public function beforeDelete(){
	    if($this->oComprobante != null){
			$this->oComprobante->montoPagado -= $this->montoSaldado;
			
			if(!$this->oComprobante->save()){
				//TODO: HANDLE ERROR
			}
		}
		if($this->oNotaEntrega != null){
			$this->oNotaEntrega->montoPagado -= $this->montoSaldado;
			if(!$this->oNotaEntrega->save()){
				//TODO: HANDLE ERROR
			}
		}
	    return parent::beforeDelete();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pago2DocumentoCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
