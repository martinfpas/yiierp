<?php

/**
 * This is the model class for table "vDocumentosDeCarga".
 *
 * The followings are the available columns in table 'vDocumentosDeCarga':
 * @property string $id
 * @property integer $idFa
 * @property integer $idNe
 * @property integer $idNotaPedido
 * @property string $fecha
 * @property integer $idNp
 * @property integer $idCarga
 *
 * @property Carga $oCarga
 */
class VDocumentosDeCarga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vDocumentosDeCarga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idFa, idNe, idNotaPedido, idNp, idCarga', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>36),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idFa, idNe, idNotaPedido, fecha, idNp, idCarga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oCarga'        => array(self::BELONGS_TO, 'Carga', 'idCarga'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idFa' => 'Id Fa',
			'idNe' => 'Id Ne',
			'idNotaPedido' => 'Id Nota Pedido',
			'fecha' => 'Fecha',
			'idNp' => 'Id Np',
			'idCarga' => 'Id Carga',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('idFa',$this->idFa);
		$criteria->compare('idNe',$this->idNe);
		$criteria->compare('idNotaPedido',$this->idNotaPedido);
        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }
		$criteria->compare('idNp',$this->idNp);
		$criteria->compare('idCarga',$this->idCarga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VDocumentosDeCarga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
