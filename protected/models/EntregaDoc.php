<?php

/**
 * This is the model class for table "entregaDoc".
 *
 * The followings are the available columns in table 'entregaDoc':
 * @property integer $id
 * @property string $tipoPersona
 * @property integer $nroCamViaj
 * @property string $observacion
 * @property double $montoInicial
 * @property double $kmRecorridos
 * @property double $litrosCargados
 * @property double $otrosPagos
 * @property integer $cerrada
 * @property double $otrasDevoluciones
 * @property integer $idCarga
 *
 * The followings are the available model relations:
 * @property CargaDocumentos[] $aCargaDocumentos
 * @property EntregaCheque[] $aEntregaCheque
 * @property Carga $oCarga
 * @property EntregaGtos[] $aEntregaGtos
 */
class EntregaDoc extends CActiveRecord
{
    public $aComisiones = array();
    public $fechaCarga;
    public $estadoCarga;
    const camion = 'CAM';
    const viajante = 'VIA';

    public static $aTipoPersona = array(
        self::camion => 'Camion',
        self::viajante => 'Viajante',
    );
    public static $aCerrada = array(
        '0' => 'No',
        '1' => 'Si',
    );

    public static $aViajantes = array(
        '1' => 'ERCA SRL (Santa Fe)',
        '6' => 'ERCA (Entre Rios)',
        '9' => 'FARYNIARZ',
        '11' => 'Bessi',
    );
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EntregaDoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'entregaDoc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('nroCamViaj', 'required'),
			array('kmRecorridos, litrosCargados, otrosPagos, cerrada, otrasDevoluciones', 'required','on'=> 'update'),
			array('nroCamViaj, cerrada, idCarga', 'numerical', 'integerOnly'=>true),
			array('montoInicial, kmRecorridos, litrosCargados, otrosPagos, otrasDevoluciones', 'numerical'),
			array('tipoPersona', 'length', 'max'=>4),
			array('observacion', 'length', 'max'=>400),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tipoPersona, nroCamViaj, observacion, montoInicial, kmRecorridos, litrosCargados, otrosPagos, cerrada, otrasDevoluciones,fechaCarga,estadoCarga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aCargaDocumentos' => array(self::HAS_MANY, 'CargaDocumentos', 'idEntregaDoc'),
			'aEntregaCheque' => array(self::HAS_MANY, 'EntregaCheque', 'id_carga_doc'),
            'oCarga' => array(self::BELONGS_TO, 'Carga', 'idCarga'),
            'aEntregaGtos' => array(self::HAS_MANY, 'EntregaGtos', 'idEntregaDoc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'Id Referencia'),
		            'tipoPersona' => Yii::t('application', 'Tipo Persona'),
		            'nroCamViaj' => Yii::t('application', 'Nro Cam Viaj'),
		            'observacion' => Yii::t('application', 'Observacion'),
		            'montoInicial' => Yii::t('application', 'Monto Inicial'),
		            'kmRecorridos' => Yii::t('application', 'Km Recorridos'),
		            'litrosCargados' => Yii::t('application', 'Litros Cargados'),
		            'totalEfectivo' => Yii::t('application', 'Total Efectivo'),
		            'totalCheques' => Yii::t('application', 'Total Cheques'),
		            'otrosPagos' => Yii::t('application', 'Otros Pagos'),
		            'cerrada' => Yii::t('application', 'Cerrada'),
		            'otrasDevoluciones' => Yii::t('application', 'Otras Devoluciones'),
                    'idCarga' => 'Id Carga',
		    );
	}

    /* @var $oCargaDocumentos CargaDocumentos */
    public function getTotal(){
        $fTotal = 0;

        foreach ($this->aCargaDocumentos as $index => $oCargaDocumentos) {
            $fTotal += $oCargaDocumentos->totalFinal;
        }
        return number_format($fTotal,2,'.','');
    }

	public function cargarDocumentos(){
        //TODO CAMBIAR POR UNA VISTA
	    if($this->tipoPersona == 'CAM'){
            $aFacturas = Factura::model()->findAllByAttributes(array('Enviada'=>false,'Id_Camion'=>$this->nroCamViaj));
            $aNotasEntrega = NotaEntrega::model()->findAllByAttributes(array('enviada'=>false,'idCamion'=>$this->nroCamViaj));
        }else{
            $aFacturas = Factura::model()->findAllByAttributes(array('Enviada'=>false,'Nro_Viajante'=>$this->nroCamViaj));
            $aNotasEntrega = NotaEntrega::model()->findAllByAttributes(array('enviada'=>false,'nroViajante'=>$this->nroCamViaj));
        }

        $i = 0;
        $idCarga = 0;

        foreach ($aFacturas as $index => $oFactura) {

            //TODO: CONTROLAR QUE NO ESTE EN OTRA EntregaDoc
            $oCargaDoc = new CargaDocumentos();
            $oCargaDoc->idEntregaDoc = $this->id;
            $oCargaDoc->idComprobante = $oFactura->id;
            $oCargaDoc->tipoDoc = CargaDocumentos::FAC;
            $oCargaDoc->totalFinal = $oFactura->Total_Final;
            $oCargaDoc->fechaCarga = date('Y-m-d');
            $oCargaDoc->orden = $i;
            $oCargaDoc->comision = 1;
            if (!$oCargaDoc->save()){
                // TODO: MANEJAR EL ERROR
                Yii::log('CHtml::errorSummary($this) '.CHtml::errorSummary($oCargaDoc),'warning');
            }

            if($oFactura->oNotaPedido != null && $oFactura->oNotaPedido->idCarga > 0 && $idCarga == 0){
                $idCarga = $oFactura->oNotaPedido->idCarga;
            }
            $i++;
            //$oFactura->
        }

        foreach ($aNotasEntrega as $index => $oNotaEntrega) {
            //TODO: CONTROLAR QUE NO ESTE EN OTRA EntregaDoc
            $oCargaDoc = new CargaDocumentos();
            $oCargaDoc->idEntregaDoc = $this->id;
            $oCargaDoc->idComprobante = $oNotaEntrega->id;
            $oCargaDoc->tipoDoc = CargaDocumentos::NE;
            $oCargaDoc->totalFinal = $oNotaEntrega->totalFinal;
            $oCargaDoc->fechaCarga = date('Y-m-d');
            $oCargaDoc->orden = $i;
            $oCargaDoc->comision = 1;
            if (!$oCargaDoc->save()){
                // TODO: MANEJAR EL ERROR
                Yii::log('CHtml::errorSummary($this) '.CHtml::errorSummary($oCargaDoc),'warning');
            }else{

            }
            if($oNotaEntrega->oNotaPedido != null && $oNotaEntrega->oNotaPedido->idCarga > 0 && $idCarga == 0){
                $idCarga = $oFactura->oNotaPedido->idCarga;
            }

            $i++;
            //Yii::log('$i :: '.$i,'warning');
        }

        return $idCarga;
    }

    public function esEditable(){
	    return (($this->oCarga == null && !$this->cerrada) || ($this->oCarga != null && $this->oCarga->estado < Carga::iCerrada) );
    }

    public function getTotalMercaderia(){
        $total = 0;

        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            $total += $aCargaDocumento->totalFinal;
        }
        return number_format($total,2,'.','');
    }

    public function getTotalNotaDeCredito(){
        $total = 0;

        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            if($aCargaDocumento->tipoDoc == CargaDocumentos::NC){
                $total += $aCargaDocumento->totalFinal;
            }
        }
        return number_format($total,2,'.','');
    }

    public function getTotalDevoluciones(){
        $total = 0;

        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            $total += $aCargaDocumento->devolucion;
        }
        return number_format($total,2,'.','');
    }

    public function getTotalNoEntregadas(){
        $total = 0;

        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            $entregada = ($aCargaDocumento->tipoDoc != CargaDocumentos::NE)? CargaDocumentos::$aSiNo[$aCargaDocumento->oComprobante->Recibida] : CargaDocumentos::$aSiNo[$aCargaDocumento->oNotaEntrega->recibida];
            $docs = [];
            
            if($entregada == "No"){
                $total += $aCargaDocumento->totalFinal;
            }            
        }
        
        return number_format($total,2,'.','');
    }

    public function getTotalEfectivo(){
	    $total = 0;
        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            //$entregada = ($aCargaDocumento->tipoDoc != CargaDocumentos::NE)? CargaDocumentos::$aSiNo[$aCargaDocumento->oComprobante->Recibida] : CargaDocumentos::$aSiNo[$aCargaDocumento->oNotaEntrega->recibida];
            //if($entregada == "Si"){
                $total += $aCargaDocumento->efectivo;
            //}
	    }
        return number_format($total,2,'.','');
    }

    public function getTotalCheques(){
        $total = 0;
        foreach ($this->aCargaDocumentos as $index => $aCargaDocumento) {
            $total += $aCargaDocumento->cheque;
            if($aCargaDocumento->cheque <> 0){
                //echo '<br>'.$aCargaDocumento->cheque;
            }
        }
        //echo '<br>';
        return number_format($total,2,'.','');
    }
    public function getTotalGastos(){
        $total  = 0;
        foreach ($this->aEntregaGtos as $index => $aEntregaGto) {
            $total += $aEntregaGto->montoGasto;
        }
        return number_format($total,2,'.','');
    }

    public function getTotalARendir(){
        $total = $this->getTotalEfectivo() + $this->getTotalCheques() + $this->montoInicial - $this->getTotalGastos();
        
        if($this->oCarga->estado == Carga::iCerrada){
            $total -= $this->getTotalNoEntregadas();//$this->getTotalDevoluciones();
        }
        
        return number_format($total,2,'.','');
    }

    public function getContadoPendiente(){
        return number_format(($this->getTotalMercaderia() - $this->getTotalDevoluciones() - $this->getTotalCheques() - $this->getTotalEfectivo()) , 2 , '.','' );
    }


    public function getComisionesGeneradas(){
        /* @var $oCargaDocumentos CargaDocumentos */
        $fMontoComisionable = 0;

        foreach ($this->aCargaDocumentos as $index => $oCargaDocumentos) {
            $fMontoPasibleComision = $oCargaDocumentos->MontoPasibleComision;
            //echo  '<br>'.$oCargaDocumentos->id.' :: '.$fMontoPasibleComision.' : V '.$oCargaDocumentos->Cliente->codViajante.' : R '.$oCargaDocumentos->Cliente->id_rubro;
            $esInstitucion = $oCargaDocumentos->Cliente->esInstitucion();

            // ES COMERCIO
            if(!$esInstitucion){
                // ES BESSI
                if($oCargaDocumentos->Cliente->codViajante == 11 ){
                    if (!isset($this->aComisiones[11])){
                        $this->aComisiones[11] = $fMontoPasibleComision;
                    }else{
                        $this->aComisiones[11] += $fMontoPasibleComision;
                    }
                }else{
                    if($oCargaDocumentos->Cliente->Provincia == 'ENTRE RIOS'){
                        if (!isset($this->aComisiones[6])){
                            $this->aComisiones[6] = $fMontoPasibleComision;
                        }else{
                            $this->aComisiones[6] += $fMontoPasibleComision;
                        }
                    }else{ // $oCargaDocumentos->Cliente->codViajante
                        if (!isset($this->aComisiones[$oCargaDocumentos->Cliente->codViajante])){
                            $this->aComisiones[$oCargaDocumentos->Cliente->codViajante] = $fMontoPasibleComision;
                        }else{
                            $this->aComisiones[$oCargaDocumentos->Cliente->codViajante] += $fMontoPasibleComision;
                        }
                        /*
                        if (!isset($this->aComisiones[1])){
                            $this->aComisiones[1] = $fMontoPasibleComision;
                        }else{
                            $this->aComisiones[1] += $fMontoPasibleComision;
                        }
                        */
                    }
                }
            }else{ // ES ESCUELA
                if (!isset($this->aComisiones[1])){
                    $this->aComisiones[1] = $fMontoPasibleComision;
                }else{
                    $this->aComisiones[1] += $fMontoPasibleComision;
                }
            }
            $fMontoComisionable += $fMontoPasibleComision;
        }
        //echo  '<br> $fMontoComisionable:: '.$fMontoComisionable;
        return number_format($fMontoComisionable,2,'.','');
    }

    //public function

    /*
     *  * @property double $totalEfectivo
 * @property double $totalCheques*/

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oCarga');

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.tipoPersona',$this->tipoPersona,true);
		$criteria->compare('t.nroCamViaj',$this->nroCamViaj);
		$criteria->compare('t.observacion',$this->observacion,true);
		$criteria->compare('t.montoInicial',$this->montoInicial);
		$criteria->compare('t.kmRecorridos',$this->kmRecorridos);
		$criteria->compare('t.litrosCargados',$this->litrosCargados);
		$criteria->compare('t.otrosPagos',$this->otrosPagos);
		$criteria->compare('t.cerrada',$this->cerrada);
        $criteria->compare('t.idCarga',$this->idCarga);

        if ($this->fechaCarga != null){
            $criteria->compare('oCarga.fecha',ComponentesComunes::fechaComparada($this->fechaCarga));
        }

        $criteria->compare('oCarga.estado',$this->estadoCarga);



		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    '*',
                    'fechaCarga' => array(
                        'asc' => 'oCarga.fecha asc',
                        'desc' => 'oCarga.fecha desc',
                    ),
                    'estadoCarga' => array(
                        'asc' => 'oCarga.estado asc',
                        'desc' => 'oCarga.estado desc',
                    ),
                )
            ),
		));
	}
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchDespachadasOCerradas()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oCarga');

        $criteria->compare('id',$this->id);
        $criteria->compare('tipoPersona',$this->tipoPersona,true);
        $criteria->compare('nroCamViaj',$this->nroCamViaj);
        $criteria->compare('observacion',$this->observacion,true);
        $criteria->compare('montoInicial',$this->montoInicial);
        $criteria->compare('kmRecorridos',$this->kmRecorridos);
        $criteria->compare('litrosCargados',$this->litrosCargados);
        $criteria->compare('otrosPagos',$this->otrosPagos);
        $criteria->compare('cerrada',$this->cerrada);
        $criteria->compare('otrasDevoluciones',$this->otrasDevoluciones);
        $criteria->compare('idCarga',$this->idCarga);

        if ($this->fechaCarga != null){
            $criteria->compare('oCarga.fecha',ComponentesComunes::fechaComparada($this->fechaCarga));
        }

        $criteria->addCondition('oCarga.estado = '.Carga::iDespachada.' or oCarga.estado = '.Carga::iCerrada);
        $criteria->compare('oCarga.estado',$this->estadoCarga);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    '*',
                    'fechaCarga' => array(
                        'asc' => 'oCarga.fecha asc',
                        'desc' => 'oCarga.fecha desc',
                    ),
                    'estadoCarga' => array(
                        'asc' => 'oCarga.estado asc',
                        'desc' => 'oCarga.estado desc',
                    ),
                )
            ),
        ));
    }

	public function beforeSave(){
	    if ($this->oCarga->estado < Carga::iFacturada){
	        throw new Exception('La carga asociada debe estar al menos en estado facturada');
        }
        return parent::beforeSave();
    }
}