<?php

/**
 * This is the model class for table "puntoDeVenta".
 *
 * The followings are the available columns in table 'puntoDeVenta':
 * @property integer $num
 * @property string $descripcion
 * @property integer $activo
 *
 * The followings are the available model relations:
 * @property NotaEntrega[] $aNotaEntrega
 * @property User[] $aUser
 */
class PuntoDeVenta extends CActiveRecord
{

	const iInactivo = 0;
	const iActivo = 1; 
	
	const inactivo = 'Inactivo';
	const activo = 'Activo'; 
	
	public static $aEstado = array(
		self::iInactivo => self::inactivo,
		self::iActivo => self::activo,
	);
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'puntoDeVenta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('num, activo', 'required'),
			array('num, activo', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('num, descripcion, activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aNotaEntrega' => array(self::HAS_MANY, 'NotaEntrega', 'nroPuestoVenta'),
			'aUser' => array(self::HAS_MANY, 'User', 'puntoDeVenta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'num' => 'Num',
			'descripcion' => 'Descripcion',
			'activo' => 'Activo',
		);
	}

    public function esElectronico(){
        return ($this->num == 9 || $this->num == 10);
    }

	public function getTitle(){
	    return $this->num.' -'.$this->descripcion;
    }
	
	public function scopes()
    {

        return array(
            'activos'=>array(
                'condition'=>'activo='.self::iActivo,
            ),
            'inactivos'=>array(
                'condition'=>'activo='.self::iInactivo,
            ),
            'manual' => array(
                'condition' => 'num <> 9 AND num <> 10 '
            ),
            'electronico' => array(
                'condition' => '(num = 9 OR num = 10)'
            )
        );
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('num',$this->num);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('activo',$this->activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PuntoDeVenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// EN LUGAR DE BORRAR, LO PASA A INACTIVO PARA NO PERDER HISTORICO, Y NO GENERAR INCONSISTENCIAS
	public function beforeDelete(){
		$this->activo = self::iInactivo;
		$this->save();
		return false;
	}
}
