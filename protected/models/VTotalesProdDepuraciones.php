<?php

/**
 * This is the model class for table "vTotalesProdDepuraciones".
 *
 * The followings are the available columns in table 'vTotalesProdDepuraciones':
 * @property integer $idArtVenta
 * @property integer $idPaquete
 * @property integer $idCarga
 * @property string $cantidad
 */
class VTotalesProdDepuraciones extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vTotalesProdDepuraciones';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idArtVenta, idPaquete', 'required'),
			array('idArtVenta, idPaquete, idCarga', 'numerical', 'integerOnly'=>true),
			array('cantidad', 'length', 'max'=>32),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idArtVenta, idPaquete, idCarga, cantidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vtotales Prod Depuraciones',
            'models' => 'Vtotales Prod Depuraciones',
			'idArtVenta' => Yii::t('application', 'Id Art Venta'),
			'idPaquete' => Yii::t('application', 'Id Paquete'),
			'idCarga' => Yii::t('application', 'Id Carga'),
			'cantidad' => Yii::t('application', 'Cantidad'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('idPaquete',$this->idPaquete);
		$criteria->compare('idCarga',$this->idCarga);
		$criteria->compare('cantidad',$this->cantidad,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VTotalesProdDepuraciones the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
