<?php

/**
 * This is the model class for table "ordenDePago".
 *
 * The followings are the available columns in table 'ordenDePago':
 * @property integer $id
 * @property string $observacion
 * @property integer $idProveedor
 * @property string $fecha
 * @property integer $idPagoProveedor
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property FacturaOrdenPago[] $aFacturaOrdenPago
 * @property PagoProveedor $oPagoProveedor
 * @property User $user
 * @property Proveedor $oProveedor
 */
class OrdenDePago extends CActiveRecord
{

    public $idProveedor;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ordenDePago';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idProveedor, fecha', 'required'),
			array('idProveedor, idPagoProveedor, user_id', 'numerical', 'integerOnly'=>true),
			array('observacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idProveedor, observacion, fecha, idPagoProveedor, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aFacturaOrdenPago' => array(self::HAS_MANY, 'FacturaOrdenPago', 'idOrdeDePago'),
			'oPagoProveedor' => array(self::BELONGS_TO, 'PagoProveedor', 'idPagoProveedor'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'idProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Orden De Pago',
            'models' => 'Orden De Pagos',
			'id' => Yii::t('application', 'ID'),
			'observacion' => Yii::t('application', 'Observacion'),
			'fecha' => Yii::t('application', 'Fecha'),
			'idPagoProveedor' => Yii::t('application', 'Id Pago Proveedor'),
			'user_id' => Yii::t('application', 'User'),
            'idProveedor' => Yii::t('application', 'Proveedor'),
		);
	}

	public function beforeSave(){
	    $this->user_id = Yii::app()->user->id;
	    return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProveedor');

		$criteria->compare('id',$this->id);
        $criteria->compare('idProveedor',$this->idProveedor);
		$criteria->compare('observacion',$this->observacion,true);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

		$criteria->compare('idPagoProveedor',$this->idPagoProveedor);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc' ,
                'attributes' => array(
                    'idProveedor' => array(
                        'asc' => 'oProveedor.razonSocial asc',
                        'desc' => 'oProveedor.razonSocial desc'
                    ),
                    '*',
                ),
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OrdenDePago the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
