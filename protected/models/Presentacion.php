<?php

/**
 * This is the model class for table "presentacion".
 *
 * The followings are the available columns in table 'presentacion':
 * @property integer $id_presentacion
 * @property integer $id_articulo
 * @property integer $id_rubro
 * @property string $descripcion
 * @property integer $contenido
 * @property double $precio
 * @property double $Costo_base
 * @property string $Unidad_Medida
 *
 * The followings are the available model relations:
 * @property Paquete[] $aPaquete
 */
class Presentacion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'presentacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('precio', 'required'),
			array('id_presentacion, id_articulo, id_rubro, contenido', 'numerical', 'integerOnly'=>true),
			array('precio, Costo_base', 'numerical'),
			array('descripcion', 'length', 'max'=>35),
			array('Unidad_Medida', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_presentacion, id_articulo, id_rubro, descripcion, contenido, precio, Costo_base, Unidad_Medida', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aPaquete' => array(self::HAS_MANY, 'Paquete', 'idPresentacion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_presentacion' => 'Id Presentacion',
			'id_articulo' => 'Id Articulo',
			'id_rubro' => 'Id Rubro',
			'descripcion' => 'Descripcion',
			'contenido' => 'Contenido',
			'precio' => 'Precio',
			'Costo_base' => 'Costo Base',
			'Unidad_Medida' => 'Unidad Medida',
		);
	}
	
	public function getIdTitulo() {
		return $this->id_presentacion
		.' - '.$this->descripcion;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_presentacion',$this->id_presentacion);
		$criteria->compare('id_articulo',$this->id_articulo);
		$criteria->compare('id_rubro',$this->id_rubro);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('contenido',$this->contenido);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('Costo_base',$this->Costo_base);
		$criteria->compare('Unidad_Medida',$this->Unidad_Medida,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function scopes()
	{
		return array(
				'todas'=>array(
						'order'=>'descripcion asc',
				),
		);
	}	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Presentacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
