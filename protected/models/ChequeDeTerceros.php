<?php

/**
 * This is the model class for table "chequeDeTerceros".
 *
 * The followings are the available columns in table 'chequeDeTerceros':
 * @property integer $nroCheque
 * @property string $fecha
 * @property integer $id_banco
 * @property integer $id_sucursal
 * @property double $importe
 * @property integer $id_cliente
 * @property integer $propio
 * @property string $fechaDeposito
 * @property integer $estado
 * @property integer $idProveedor
 * @property string $observacion
 * @property integer $id
 *
 * The followings are the available model relations:
 * @property Banco $oBanco
 * @property Proveedor $oIdProveedor
 * @property SucursalBanco $oSucursal
 * @property Cliente oCliente
 * @property MigraChequeDe3Ro $migraChequeDe3Ro
 * @property PagoConCheque[] $aPagoConCheque
 */
class ChequeDeTerceros extends CActiveRecord
{
    public $id_banco = null;
    public $id_pagoCliente = null;
    const iSi = 1;
    const iNo = 0;

    public static $aSiNo = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

    const iEnCartera = 0;
    const iDepositado = 1;
    const iPagoTercero = 2;

    const enCartera = 'En Cartera';
    const depositado = 'Depositado';
    const pagoTercero = 'Pago Tercero';

    public static $aEstado = array(
        self::iEnCartera => self::enCartera,
        self::iDepositado => self::depositado,
        self::iPagoTercero => self::pagoTercero,
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ChequeDeTerceros the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chequeDeTerceros';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nroCheque, id_cliente', 'required'),
            array('nroCheque, id_banco, id_sucursal, id_cliente, propio, estado, idProveedor', 'numerical', 'integerOnly'=>true),
            array('importe', 'numerical'),
            array('nroCheque','length','max' => '12'),
            array('id_banco, fecha, fechaDeposito, observacion', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('nroCheque, fecha, id_banco, id_sucursal, importe, id_cliente, propio, fechaDeposito, estado, idProveedor, observacion, id', 'safe', 'on'=>'search'),
        );
    }

    public function scopes()
    {
        return array(
            'enCartera'=>array(
                'condition'=>'estado='.self::iEnCartera,
            ),
            'pagoTercero'=>array(
                'condition'=>'estado='.self::iPagoTercero,
            ),
            'depositados'=>array(
                'condition'=>'estado='.self::iDepositado,
            ),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oSucursal' => array(self::BELONGS_TO, 'SucursalBanco', 'id_sucursal'),
            'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'idProveedor'),
            'oBanco' => array(self::BELONGS_TO, 'Banco', 'id_banco'),
            'oCliente' => array(self::BELONGS_TO, 'Cliente', 'id_cliente'),
			'migraChequeDe3Ro' => array(self::HAS_ONE, 'MigraChequeDe3Ro', 'id'),
            'aPagoConCheque' => array(self::HAS_MANY, 'PagoConCheque', 'idChequedeTercero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
            'model' => 'Cheque De Terceros',
            'models' => 'Cheques De Terceros',
            'nroCheque' => Yii::t('application', 'Nro Cheque'),
            'fecha' => Yii::t('application', 'Fecha de pago'),
            'id_sucursal' => Yii::t('application', 'Banco'),
            'id_sucursal' => Yii::t('application', 'Sucursal'),
            'importe' => Yii::t('application', 'Importe'),
            'id_cliente' => Yii::t('application', 'Cliente'),
            'propio' => Yii::t('application', 'Propio'),
            'fechaDeposito' => Yii::t('application', 'Fecha Deposito'),
            'estado' => Yii::t('application', 'Estado'),
            'idProveedor' => Yii::t('application', 'Id Proveedor'),
            'observacion' => Yii::t('application', 'Observacion'),
            'id' => Yii::t('application', 'ID'),
        );
	}

	public function depositar(){
	    $this->estado = self::iDepositado;
	    if ($this->fechaDeposito <> ''){
            $this->fechaDeposito = date('Y-m-d');
        }
        if ($this->save()){
	        Yii::log('$this:'.CHtml::errorSummary($this),'error');
        }

    }

	/**
	    Devuelve el monto que queda disponible para aplicar a un pago, descontando los pagos realizados
     */
	public function importeDisponible(){
	    $montosAplicados = 0;
        foreach ($this->aPagoConCheque as $index => $oPagoConCheque) {
            $montosAplicados += $oPagoConCheque->monto;
	    }
	    return $this->importe - $montosAplicados;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nroCheque',$this->nroCheque);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }


        $criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('id_sucursal',$this->id_sucursal);
		$criteria->compare('importe',$this->importe);
		$criteria->compare('id_cliente',$this->id_cliente);
		$criteria->compare('propio',$this->propio);

        if ($this->fechaDeposito != null){
            $criteria->compare('fechaDeposito',ComponentesComunes::fechaComparada($this->fechaDeposito));
        }
        $criteria->compare('estado',$this->estado);
        $criteria->compare('idProveedor',$this->idProveedor);
        $criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.fecha',
            ),
		));
	}
}