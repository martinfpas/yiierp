<?php



/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property integer $puntoDeVenta
 * @property string $password
 * @property string $status
 * @property string $created_date
 * @property string $last_login
 * @property string $user_type
 * @property string $telephone
 * @property string $mobile
 * @property string $address
 * @property integer $county
 * @property integer $country
 * @property string $salt
 * @property integer $adress_number
 * @property string $postcode
 * @property string $city
 * @property string $salutation
 * @property string $birthdate
 * 
 * The followings are the available model relations:
 * @property PuntoDeVenta $oPuntoDeVenta
 * 
 */
class User extends CActiveRecord
{
	public $password_1=NULL;
	public $password_confirm=NULL;
	public $terms=NULL;
	
	public $verifyCode;
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('county, country, adress_number', 'numerical', 'integerOnly'=>true),
            array('puntoDeVenta, county, country, adress_number', 'numerical', 'integerOnly'=>true),
			array('username, first_name, last_name, telephone, city', 'length', 'max'=>100),
			array('username','unique','on'=>'insert'),
			array('first_name, last_name, email', 'required'),
			array('email, password, salt', 'length', 'max'=>250),
			array('email','email'),
			array('status', 'length', 'max'=>9),
			array('password_1, password, password_confirm', 'required','on'=>'insert'),
			array('password_1', 'compare', 'compareAttribute'=>'password_confirm','on'=>'insert'),
			array('password_1', 'compare', 'compareAttribute'=>'password_confirm','on'=>'update'),
			//array('terms', 'required','on'=>'insert','message' => 'Read and accept Terms & Conditions'),
			array('user_type, postcode, salutation', 'length', 'max'=>20),
			array('mobile', 'length', 'max'=>30),
			array('address, birthdate, password_confirm, password_1', 'safe'),
			
			// HABILITAR
			//array('verifyCode', 'captcha', 'on'=>'captchaRequired', 'allowEmpty'=>!CCaptcha::checkRequirements() ),
			//array('verifyCode', 'captcha', 'on'=>'insert', 'allowEmpty'=>!CCaptcha::checkRequirements() ),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, first_name, last_name, email, password, status, created_date, last_login, user_type, telephone, mobile, address, county,country, salt, adress_number, postcode, city, salutation, birthdate, driver_license,terms', 'safe', 'on'=>'search'),
			//last_login
			array('last_login','safe','on'=>'login'),
		);
	}

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'oPuntoDeVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'puntoDeVenta'),
        );
    }

    /**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'first_name' => 'Nombre',
			'last_name' => 'Apellido',
			'email' => 'Email',
            'puntoDeVenta' => 'Punto De Venta',
			'password' => 'Password',
			'password_1' => 'Nueva Password',
			'password_confirm' => 'Confirmar Password',
			'status' => 'Estado',
			'created_date' => 'Fecha Creacion',
			'last_login' => 'Ultimo Login',
			'user_type' => 'Tipo de usuario',
			'telephone' => 'Telefono',
			'mobile' => 'Movil',
			'address' => 'Direccion',
			'county' => 'County',
			'country' => 'Pais',
			'salt' => 'Salt',
			'adress_number' => 'Numero',
			'postcode' => 'Postcode',
			'city' => 'Ciudad',
			'salutation' => 'Trato',
			'birthdate' => 'Fecha Nacimiento',
			'terms' => 'Terminos y condiciones',	
		);
	}
	protected function afterSave()
	{
		if ($this->isNewRecord) {
			$asign = new AuthAssignment;
			$asign->itemname = $this->user_type;
			//$asign->userid = $this->username;
			$asign->userid = $this->id;
			$asign->save();			 
		}

	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
        $criteria->compare('puntoDeVenta',$this->puntoDeVenta);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('county',$this->county);
		$criteria->compare('country',$this->country);
		$criteria->compare('salt',$this->salt,true);
		$criteria->compare('adress_number',$this->adress_number);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('salutation',$this->salutation,true);
		$criteria->compare('birthdate',$this->birthdate,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password,$this->salt)===$this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password,$salt)
	{
		return md5($salt.$password);
	}

	/**
	 * Generates a salt that can be used to generate a password hash.
	 * @return string the salt
	 */
	public function generateSalt()
	{
		return uniqid('',true);
	}
	
	public function beforeSave() {
		//TODO: VALIDATE
		
		
		//die($this->password_1);
		
		if($this->password_1 != NULL){
			$this->password = $this->hashPassword($this->password_1,$this->salt);
			//die('$this->password '.$this->password);
		}
		/*
		 else {
			if($this->password != NULL){
				$this->password = $this->hashPassword($this->password,$this->salt);
			}
		} 
		*/
		
		if(parent::beforeSave()){
		    return true;
	    }else{
	        return false;
	    }
	}
	
	public function time_elapsed_string()
	{
	    $ptime = strtotime($this->created_date);
		$etime = time() - $ptime;
	
	    if ($etime < 1)
	    {
	        return '0 seconds';
	    }
	
	    $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
	                30 * 24 * 60 * 60       =>  'month',
	                24 * 60 * 60            =>  'day',
	                60 * 60                 =>  'hour',
	                60                      =>  'minute',
	                1                       =>  'second'
	                );
	
	    foreach ($a as $secs => $str)
	    {
	        $d = $etime / $secs;
	        if ($d >= 1)
	        {
	            $r = round($d);
	            return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
	        }
	    }
	}
	
}