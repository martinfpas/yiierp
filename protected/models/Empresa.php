<?php

/**
 * This is the model class for table "empresa".
 *
 * The followings are the available columns in table 'empresa':
 * @property integer $id
 * @property string $demominacion
 * @property string $nombreFantasia
 * @property string $actividad
 * @property integer $publica
 * @property string $email
 * @property string $email2
 * @property string $calle
 * @property integer $numero
 * @property integer $codigoPostal
 * @property integer $zonaPostal
 * @property string $paginaWeb
 */
class Empresa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Empresa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'empresa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('demominacion, publica, codigoPostal, zonaPostal', 'required'),
			array('publica, numero, codigoPostal, zonaPostal', 'numerical', 'integerOnly'=>true),
			array('demominacion', 'length', 'max'=>20),
			array('nombreFantasia, actividad, email, email2', 'length', 'max'=>50),
			array('calle', 'length', 'max'=>30),
			array('paginaWeb', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, demominacion, nombreFantasia, actividad, publica, email, email2, calle, numero, codigoPostal, zonaPostal, paginaWeb', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'demominacion' => Yii::t('application', 'Demominacion'),
		            'nombreFantasia' => Yii::t('application', 'Nombre Fantasia'),
		            'actividad' => Yii::t('application', 'Actividad'),
		            'publica' => Yii::t('application', 'Publica'),
		            'email' => Yii::t('application', 'Email'),
		            'email2' => Yii::t('application', 'Email2'),
		            'calle' => Yii::t('application', 'Calle'),
		            'numero' => Yii::t('application', 'Numero'),
		            'codigoPostal' => Yii::t('application', 'Codigo Postal'),
		            'zonaPostal' => Yii::t('application', 'Zona Postal'),
		            'paginaWeb' => Yii::t('application', 'Pagina Web'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('demominacion',$this->demominacion,true);
		$criteria->compare('nombreFantasia',$this->nombreFantasia,true);
		$criteria->compare('actividad',$this->actividad,true);
		$criteria->compare('publica',$this->publica);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('email2',$this->email2,true);
		$criteria->compare('calle',$this->calle,true);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('codigoPostal',$this->codigoPostal);
		$criteria->compare('zonaPostal',$this->zonaPostal);
		$criteria->compare('paginaWeb',$this->paginaWeb,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}