<?php

/**
 * This is the model class for table "vPagosDeEntregaDoc".
 *
 * The followings are the available columns in table 'vPagosDeEntregaDoc':
 * @property integer $idEntregaDoc
 * @property integer $idCargaDoc
 * @property integer $idFactura
 * @property integer $idNotaEntrega
 * @property integer $idPago
 * @property integer $idCliente
 * @property double $monto
 * @property integer $idChequedeTercero
 * @property string $tipoDoc
 * @property double $totalFinal
 *
 *
 * @property Cliente $oCliente
 * @property ChequeDeTerceros $oChequedeTercero
 * @property PagoCliente $oPagoCliente
 * @property CargaDocumentos $oCargaDoc
 * @property EntregaDoc $oEntregaDoc
 *
 */
class VPagosDeEntregaDoc extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vPagosDeEntregaDoc4';
	}

    public function primaryKey(){
        return 'idCargaDoc';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEntregaDoc, idCargaDoc, idFactura, idNotaEntrega, idPago, idCliente, idChequedeTercero', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idEntregaDoc, idCargaDoc, idFactura, idNotaEntrega, idPago, idCliente, monto, idChequedeTercero', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oEntregaDoc' => array(self::BELONGS_TO, 'EntregaDoc', 'idEntregaDoc'),
            'oCargaDoc' => array(self::BELONGS_TO, 'CargaDocumentos', 'idCargaDoc'),
            'oPagoCliente' => array(self::BELONGS_TO, 'PagoCliente', 'idPago'),
            'oCliente' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
            'oChequedeTercero' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'idChequedeTercero'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idEntregaDoc' => 'Id Entrega Doc',
			'idCargaDoc' => 'Id Carga Doc',
			'idFactura' => 'Id Factura',
			'idNotaEntrega' => 'Id Nota Entrega',
			'idPago' => 'Id Pago',
			'idCliente' => 'Id Cliente',
			'monto' => 'Monto',
			'idChequedeTercero' => 'Id Chequede Tercero',
		);
	}

	public function deletePago(){

	    if($this->oCargaDoc != null && $this->oCargaDoc->getOPagoCliente() != null && $this->oCargaDoc->getOPagoCliente()->id != $this->idPago){
			$oPago = PagoCliente::model()->findByPk($this->oCargaDoc->getOPagoCliente()->id);
			Yii::log('$this->oCargaDoc->getOPagoCliente()->id:'.$this->oCargaDoc->getOPagoCliente()->id,'warning');
			if($oPago != null){
				$this->oCargaDoc->getOPagoCliente()->delete();
			}
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idEntregaDoc',$this->idEntregaDoc);
		$criteria->compare('idCargaDoc',$this->idCargaDoc);
		$criteria->compare('idFactura',$this->idFactura);
		$criteria->compare('idNotaEntrega',$this->idNotaEntrega);
		$criteria->compare('idPago',$this->idPago);
		$criteria->compare('idCliente',$this->idCliente);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('idChequedeTercero',$this->idChequedeTercero);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWpWs()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('idEntregaDoc',$this->idEntregaDoc);
        $criteria->compare('idCargaDoc',$this->idCargaDoc);
        $criteria->compare('idFactura',$this->idFactura);
        $criteria->compare('idNotaEntrega',$this->idNotaEntrega);
        $criteria->compare('idPago',$this->idPago);
        $criteria->compare('idCliente',$this->idCliente);
        $criteria->compare('monto',$this->monto);
        $criteria->compare('idChequedeTercero',$this->idChequedeTercero);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,

        ));
    }

    public function searchChequesWpWs()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->select = 'distinct(t.idChequedeTercero)';

        $criteria->compare('idCargaDoc',$this->idCargaDoc);
        $criteria->compare('idFactura',$this->idFactura);
        $criteria->compare('idNotaEntrega',$this->idNotaEntrega);
        $criteria->compare('idPago',$this->idPago);
        $criteria->compare('idCliente',$this->idCliente);
        $criteria->compare('monto',$this->monto);
        $criteria->compare('idChequedeTercero',$this->idChequedeTercero);
        $criteria->distinct = true;

        $criteria->condition = 'idChequedeTercero  IS NOT NULL and idEntregaDoc = '.$this->idEntregaDoc;

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => false,
            'pagination' => false,

        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VPagosDeEntregaDoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
