<?php

/**
 * This is the model class for table "pagoCliente".
 *
 * The followings are the available columns in table 'pagoCliente':
 * @property integer $id
 * @property double $monto
 * @property integer $id_factura
 * @property integer $idNotaEntrega
 * @property string $fecha
 * @property integer $idCliente
 * @property integer $multipago
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property PagoAlContado[] $aPagoAlContado
 * @property Factura $oFactura
 * @property NotaDebito $oNotaDebito
 * @property NotaCredito $oNotaCredito
 * @property Cliente $oCliente
 * @property NotaEntrega $oNotaEntrega
 * @property PagoConCheque[] $aPagoConCheque
 */
class PagoCliente extends CActiveRecord
{
    public $clienteNombre;
    public $nroComprobante;
    public $Fact_Nro_Comprobante;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PagoCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoCliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monto, fecha, idCliente', 'required'),
            array('id_factura, idNotaEntrega, idCliente, multipago', 'numerical', 'integerOnly'=>true),
            //array('', 'numerical'),
			array('observacion,monto','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
            array('id, monto, id_factura, idNotaEntrega, fecha, idCliente, multipago, clienteNombre, nroComprobante, Fact_Nro_Comprobante', 'safe', 'on'=>'search'),
        );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'aPagoAlContado' => array(self::HAS_MANY, 'PagoAlContado', 'idPagodeCliente'),
			'oFactura' => array(self::BELONGS_TO, 'Factura', 'id_factura'),
            'oNotaDebito' => array(self::BELONGS_TO, 'NotaDebito', 'id_factura'),
            'oNotaCredito' => array(self::BELONGS_TO, 'NotaCredito', 'id_factura'),
			'oCliente' => array(self::BELONGS_TO, 'Cliente', 'idCliente'),
            'oNotaEntrega' => array(self::BELONGS_TO, 'NotaEntrega', 'idNotaEntrega'),
            'aPagoConCheque' => array(self::HAS_MANY, 'PagoConCheque', 'idPagodeCliente'),
            'aComprobantesConciliados' => array(self::HAS_MANY, 'Pago2DocumentoCliente', 'idPago'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
            'model' => 'Pago de Cliente',
            'models' => 'Pagos de Cliente',
            'id' => 'ID',
            'monto' => 'Monto',
            'id_factura' => 'Id Factura',
            'idNotaEntrega' => 'Id Nota Entrega',
            'fecha' => 'Fecha',
            'idCliente' => 'Id Cliente',
            'multipago' => Yii::t('application', 'Multipago'),
            'observacion' => Yii::t('application', 'Observacion'),
        );
	}

    public function recalcular(){

        $this->monto = 0;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.idPagodeCliente='.$this->id);

        $aPagoAlContado = PagoAlContado::model()->findAll($criteria);
        foreach ($aPagoAlContado as $index => $oPagoAlContado) {
            $this->monto += $oPagoAlContado->monto;
        }
        $aPagoConCheque = PagoConCheque::model()->findAll($criteria);
        foreach ($aPagoConCheque as $index => $oPagoConCheque) {
            Yii::log('$oPagoConCheque->monto '.$oPagoConCheque->monto,'warning');
            $this->monto += $oPagoConCheque->monto;
	    }

	    if ($this->fecha == '0000-00-00'){
            $this->fecha = date('Y-m-d');
        }

        if (!$this->save()){
            Yii::log('Error al guardar recalcular Pago '.CHtml::errorSummary($this),'error');
            throw new Exception(CHtml::errorSummary($this));
        }

    }

    public static function newBlank($idCliente){
        $oPagoCliente = new PagoCliente();
        $oPagoCliente->idCliente = $idCliente;
        $oPagoCliente->monto = 0;
        $oPagoCliente->fecha = date('Y-m-d');
        if (!$oPagoCliente->save()){
            throw new Exception(CHtml::errorSummary($oPagoCliente));
        }else{
            return $oPagoCliente;
        }
    }

    public static function sumPagoRelacionado($idPagoRelacionado){

        $sum = 0;
        $oPagoRelacionado = PagoCliente::model()->findByPk($idPagoRelacionado);
        $oPagoRelacionado->recalcular();
        $sum = $oPagoRelacionado->monto;
        /*
        $aComprobantes = Comprobante::model()->findAllByAttributes(array('idPagoRelacionado' => $idPagoRelacionado ));
        foreach ($aComprobantes as $index => $aComprobante) {
            if ($aComprobante->oPagoCliente != null){
                $monto = number_format($aComprobante->oPagoCliente->monto,2,'.','');
                $sum += number_format($aComprobante->oPagoCliente->monto,2,'.','');

            }
        }

        $aComprobantes = NotaEntrega::model()->findAllByAttributes(array('idPagoRelacionado' => $idPagoRelacionado ));
        foreach ($aComprobantes as $index => $aComprobante) {
            if ($aComprobante->oPagoCliente){
                $monto = number_format($aComprobante->oPagoCliente->monto,2,'.','');
                $sum += number_format($aComprobante->oPagoCliente->monto,2,'.','');

            }
        }
        */
        return $sum;
    }

    public function getTotalComprobante(){
        if ($this->id_factura > 0){
            if($this->oFactura){
                return number_format($this->oFactura->Total_Final,2,'.','');
            }else if($this->oNotaDebito){
                return number_format($this->oNotaDebito->Total_Final,2,'.','');
            }else{
                return number_format($this->oNotaCredito->Total_Final,2,'.','');
            }
            
        }else{
            return number_format($this->oNotaEntrega->totalFinal,2,'.','');
        }
    }

    public function getMontoEfectivo(){
        $monto = 0;

        foreach ($this->aPagoAlContado as $index => $oPagoAlContado) {
            $monto += $oPagoAlContado->monto;
        }

        return number_format($monto,2,'.','');
    }

    /** @return Devuelve la suma del monto de cheque aplicado al pago */
    public function getMontoCheque(){
        $monto = 0;

        foreach ($this->aPagoConCheque as $index => $oPagoConCheque) {
            $monto += $oPagoConCheque->monto;
        }

        return number_format($monto,2,'.','');
    }

    /**  @return Devuelve la suma del importe neto de los cheques  */
    public function getImporteCheque(){
        $monto = 0;

        foreach ($this->aPagoConCheque as $index => $oPagoConCheque) {
            $monto += $oPagoConCheque->oChequedeTercero->importe;
        }

        return number_format($monto,2,'.','');
    }

    public function getMontoOtros(){
        // todo:

    }

    public static function devolucionMercaderia($idCliente,$monto,$idNE=null,$idFact=null){
        Yii::log('devolucionMercaderia : ','warning','custom');
        $oPago = new PagoCliente();
        $oPago->monto = 0;
        $oPago->idCliente = $idCliente;
        $oPago->idNotaEntrega = $idNE;
        $oPago->id_factura = $idFact;
        $oPago->observacion = 'Devolución de mercaderia ';
        $oPago->fecha = date('Y-m-d');
        if (!$oPago->save()){
           throw new Exception(ComponentesComunes::print_array($oPago,false));
        }else{
            $oPagoContado = new PagoAlContado();
            $oPagoContado->idPagodeCliente = $oPago->id;
            $oPagoContado->monto = $monto;
            if (!$oPagoContado->save()){

                throw new Exception(ComponentesComunes::print_array($oPagoContado,false));
            }else{
                //$oPago->recalcular();
                Yii::log('$oPago->attributes devolucionMercaderia:: '.ComponentesComunes::print_array($oPago->attributes,true),'warning','custom');
            }
        }

    }



    protected function beforeSave(){
        //Yii::log('1','warning');
        if ($this->isNewRecord){
            $oPago = null;
        }else{
            $oPago = PagoCliente::model()->findByPk($this->id);
        }

        $oCliente = Cliente::model()->findByPk($this->idCliente);

        //Yii::log('pre parent','warning');
        $bReturn = parent::beforeSave();
        $montoAnterior = 0;
        $montoNuevo = $this->monto;

        if($bReturn){
            if ($oPago != null){
                $montoAnterior = $oPago->monto;
            }

            //TODO: descomentar las dos lineas
            Yii::log('$id pago:'.$this->id.' $monto:'.$this->monto,'warning');
            Yii::log('$montoAnterior:'.$montoAnterior,'warning');
            Yii::log('$montoNuevo:'.$montoNuevo,'warning');

            if($oCliente != null){
                //Yii::log($oCliente->id.' => $oCliente->saldoActual :'.$oCliente->saldoActual,'warning');
                $oCliente->saldoActual = $oCliente->saldoActual + $montoAnterior - $montoNuevo;

                if (!$oCliente->save()){
                    Yii::log('$oCliente::'.CHtml::errorSummary($oCliente) ,'error');
                }
            }
        }
        return $bReturn;
    }

    public function afterSave(){
        // SE DISPARA UN RECALCULO DE LA CARGADOCUMENTO, SI ES QUE HAY ASOCIADO
        if ($this->id_factura > 0){
            $oCagaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante'=>$this->id_factura,'tipoDoc' => CargaDocumentos::FAC));
            if(!$oCagaDoc){
                $oCagaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante'=>$this->id_factura,'tipoDoc' => CargaDocumentos::ND));
            }            
        }else{
            $oCagaDoc = CargaDocumentos::model()->findByAttributes(array('idComprobante'=>$this->idNotaEntrega,'tipoDoc' => CargaDocumentos::NE));
        }

        if ($oCagaDoc != null){
            //TODO: descomentar la linea
            // Yii::log('Recalculando CargaDoc N:'.$oCagaDoc->id,'warning');
            $oCagaDoc->recalcular();
        }else{
            //TODO: descomentar la linea
            //Yii::log('No se encontro carga asociada','warning');
        }


        return parent::afterSave();
    }

    protected function beforeDelete(){
        /* @var $oPago PagoCliente */
        /* @var $oPagoConCheque PagoConCheque */
        /* @var $oPagoContado PagoAlContado */

        $transaction = Yii::app()->db->getCurrentTransaction();
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
        }

        $deletedId = $this->id;
        $oPago = PagoCliente::model()->findByPk($this->id);
        
        

        $aPagosDeEntregaDoc = VPagosDeEntregaDoc::model()->findAll('idPago='.$deletedId);

        foreach ($oPago->aPagoConCheque as $index => $oPagoConCheque) {
            $oPagoConCheque->recalcOnDelete = false;
            if(!$oPagoConCheque->delete()){
                $transaction->rollback();
                return false;
            }
        }

        foreach ($oPago->aPagoAlContado as $index => $oPagoContado) {
            $oPagoContado->recalcOnDelete = false;
            if(!$oPagoContado->delete()){
                $transaction->rollback();
                return false;
            }
        }

        $bReturn = parent::beforeDelete();
        
        // RECALCULA EL SALDO DEL CLIENTE CUANDO BORRA EL PAGO
        if($bReturn && $oPago != null){
            /*  @var $oPagosDeEntregaDoc VPagosDeEntregaDoc */
            /* @var  $oCargaDoc CargaDocumentos */

            $montoAnterior = $oPago->monto;

            $oCliente = Cliente::model()->findByPk($oPago->idCliente);
            if($oCliente != null){

                $montoNuevo = $oCliente->saldoActual + $montoAnterior;
                //TODO: descomentar las dos lineas
                Yii::log('Borrando $id pago:'.$deletedId.' $monto:'.$oPago->monto,'warning');
                Yii::log('$montoAnterior:'.$montoAnterior,'warning');
                Yii::log('$montoNuevo:'.$montoNuevo,'warning');

                $oCliente->saldoActual = $montoNuevo;
                if (!$oCliente->save()){
                    Yii::log('$oCliente::'.CHtml::errorSummary($oCliente) ,'error');
                }
            }


            foreach ($aPagosDeEntregaDoc as $index => $oPagosDeEntregaDoc) {
                Yii::log('Encontre $oPagosDeEntregaDoc idCargaDoc: '.$oPagosDeEntregaDoc->idCargaDoc.' monto:'.$oPagosDeEntregaDoc->monto,'warning');

                $oPago = $oPagosDeEntregaDoc->oCargaDoc->getOPagoCliente();


                if ($oPago != null){
                    Yii::log('Encontre $oPagosDeEntregaDoc idPago: '.$oPago->id,'warning');
                //  $oPago->delete();
                }

                $oCargaDoc = $oPagosDeEntregaDoc->oCargaDoc;
                $oCargaDoc->efectivo = 0;
                $oCargaDoc->cheque = 0;
                $oCargaDoc->pagado = 0;
                $oCargaDoc->notaCredito = 0;
                $oCargaDoc->Recibida = 0;
                if (!$oCargaDoc->save()){
                    Yii::log('Error $oCargaDoc:'.CHtml::errorSummary($oCargaDoc),'error');
                }

                $oPagosDeEntregaDoc->deletePago();
            }
        }else{
            $transaction->rollback();
            return $bReturn;
        }

        $aNotasEntregas = NotaEntrega::model()->findAllByAttributes(array('idPagoRelacionado'=>$deletedId));
        foreach($aNotasEntregas as $key => $oNotaEntrega){
            $oNotaEntrega->idPagoRelacionado = null;
            $oNotaEntrega->save();
        }
        $aFacturas = Factura::model()->findAllByAttributes(array('idPagoRelacionado'=>$deletedId));
        foreach($aFacturas as $key => $oFactura){
            $oFactura->idPagoRelacionado = null;
            $oFactura->save();
        }

        $aNotaDebitos = NotaDebito::model()->findAllByAttributes(array('idPagoRelacionado'=>$deletedId));
        foreach($aNotaDebitos as $key => $oNotaDebito){
            $oNotaDebito->idPagoRelacionado = null;
            $oNotaDebito->save();
        }

        $aNotaCreditos = NotaCredito::model()->findAllByAttributes(array('idPagoRelacionado'=>$deletedId));
        foreach($aNotaCreditos as $key => $oNotaCredito){
            $oNotaCredito->idPagoRelacionado = null;
            $oNotaCredito->save();
        }
        
        try{
            $transaction = Yii::app()->db->getCurrentTransaction();
            if($transaction == null){
                $transaction=Yii::app()->db->beginTransaction();
            }
            $transaction->commit();
        }catch (CDbException $e){

        }

        return $bReturn;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oCliente','oNotaEntrega','oFactura');

		$criteria->compare('t.id',$this->id);
        $criteria->compare('t.monto',$this->monto);
        $criteria->compare('t.id_factura',$this->id_factura);
        $criteria->compare('multipago',$this->multipago);
        if ($this->fecha != null){
            $criteria->compare('t.fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

		$criteria->compare('t.idCliente',$this->idCliente);

        if($this->clienteNombre != ''){
            $criteria->addCondition("(oCliente.razonSocial like '%".$this->clienteNombre."%' or oCliente.nombreFantasia like '%".$this->clienteNombre."%')");
        }

        if($this->nroComprobante != null){
            $criteria->addCondition('oNotaEntrega.nroComprobante = '.$this->nroComprobante);
        }

        if ($this->Fact_Nro_Comprobante != null){
            $criteria->addCondition('Factura.Nro_Comprobante ='.$this->Fact_Nro_Comprobante);
        }


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    '*',
                    'clienteNombre' => array(
                        'asc' =>  'oCliente.razonSocial,oCliente.nombreFantasia asc',
                        'desc' =>  'oCliente.razonSocial,oCliente.nombreFantasia desc',
                    ),
                    'nroComprobante' => array(
                        'asc' =>  'oNotaEntrega.nroComprobante asc',
                        'desc' =>  'oNotaEntrega.nroComprobante desc',
                    ),
                    'Fact_Nro_Comprobante'=>array(
                        'asc' =>  'Factura.Nro_Comprobante asc',
                        'desc' =>  'Factura.Nro_Comprobante desc',
                    ),
                )
            )
		));
	}
}