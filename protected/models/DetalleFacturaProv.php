<?php

/**
 * This is the model class for table "detalleFacturaProv".
 *
 * The followings are the available columns in table 'detalleFacturaProv':
 * @property integer $id
 * @property integer $idArticulo
 * @property string $descripcion
 * @property double $precioUnitario
 * @property integer $cantidadRecibida
 * @property integer $nroItem
 * @property float $alicuota
 * @property integer $idMateriaPrima
 * @property integer $idFacturaProveedor
 *
 * The followings are the available model relations:
 * @property FacturaProveedor $oFacturaProveedor
 * @property Articulo $oArticulo
 */
class DetalleFacturaProv extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'detalleFacturaProv';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, precioUnitario, cantidadRecibida, nroItem, alicuota, idFacturaProveedor', 'required'),
			array('idArticulo, cantidadRecibida, nroItem, idMateriaPrima, idFacturaProveedor', 'numerical', 'integerOnly'=>true),
			array('alicuota, precioUnitario', 'numerical'),
			array('descripcion', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idArticulo, descripcion, precioUnitario, cantidadRecibida, nroItem, alicuota, idMateriaPrima, idFacturaProveedor', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oFacturaProveedor' => array(self::BELONGS_TO, 'FacturaProveedor', 'idFacturaProveedor'),
            'oArticulo' => array(self::BELONGS_TO, 'Articulo', 'idArticulo'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Detalle Factura Prov',
            'models' => 'Detalle Factura Provs',
			'id' => Yii::t('application', 'ID'),
			'idArticulo' => Yii::t('application', 'Id Articulo'),
			'descripcion' => Yii::t('application', 'Descripcion'),
			'precioUnitario' => Yii::t('application', 'Precio Unitario'),
			'cantidadRecibida' => Yii::t('application', 'Cantidad'),
			'nroItem' => Yii::t('application', 'Nro Item'),
			'alicuota' => Yii::t('application', 'IVa'),
			'idMateriaPrima' => Yii::t('application', 'Id Materia Prima'),
			'idFacturaProveedor' => Yii::t('application', 'Id Factura Proveedor'),
		);
	}

	public function getSubotal(){
	    return number_format($this->cantidadRecibida * $this->precioUnitario,2);
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idArticulo',$this->idArticulo);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('precioUnitario',$this->precioUnitario);
		$criteria->compare('cantidadRecibida',$this->cantidadRecibida);
		$criteria->compare('nroItem',$this->nroItem);
		$criteria->compare('alicuota',$this->alicuota);
		$criteria->compare('idMateriaPrima',$this->idMateriaPrima);
		$criteria->compare('idFacturaProveedor',$this->idFacturaProveedor);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function afterSave(){
	    $this->oFacturaProveedor->recalcular();
	    return parent::afterSave();
    }

    public function afterDelete(){
        $this->oFacturaProveedor->recalcular();
        return parent::afterDelete();
    }

	public function beforeSave(){
        $oDetalle = DetalleFacturaProv::model()->find('idFacturaProveedor = '.$this->idFacturaProveedor,array('order'=>'nroItem desc'));
        if($oDetalle != null){
            $this->nroItem = $oDetalle->nroItem + 1;
        }else{
            $this->nroItem = 1;
        }
	    return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DetalleFacturaProv the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
