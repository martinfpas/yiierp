<?php

/**
 * This is the model class for table "cliente".
 *
 * The followings are the available columns in table 'cliente':
 * @property integer $id
 * @property string $razonSocial
 * @property string $direccion
 * @property string $nombreFantasia
 * @property string $contacto
 * @property string $cuit
 * @property string $telefonos
 * @property string $PDpto
 * @property string $email
 * @property string $fax
 * @property string $observaciones
 * @property integer $id_rubro
 * @property string $direccionDeposito
 * @property integer $codViajante
 * @property string $cp
 * @property string $zp
 * @property integer $categoriaIva
 * @property string $tipoCliente
 * @property integer $exeptuaIIB
 * @property string $pieFactura
 * @property double $saldoActual
 * @property string $saldoConciliado
 * @property string $fechaConciliacion
 *
 * The followings are the available model relations:
 * @property CodigoPostal $oCp
 * @property RubroClientes $oRubro
 * @property CategoriaIva $oCategoriaIva
 * @property Viajante $oViajante
 * @property Comprobante[] $aComprobante
 * @property NotaEntrega[] $aNotaEntrega
 */

class Cliente extends CActiveRecord
{
    public $atencionSobre = '';
    public $sLocalidad = '';
    public $sProvincia = '';

	public static $tipoPersona = array(
		'E' => 'Empresa',
		'P' => 'Persona',
	);
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		 return array(
            array('codViajante, cp, zp, categoriaIva, direccion, razonSocial', 'required'),
            array('id, id_rubro, codViajante, categoriaIva,exeptuaIIB', 'numerical', 'integerOnly'=>true),
            array('saldoActual', 'numerical'),
            array('razonSocial, telefonos', 'length', 'max'=>40),
            array('fax, direccion, PDpto', 'length', 'max'=>30),
            array('nombreFantasia, direccionDeposito', 'length', 'max'=>60),
            array('contacto, email', 'length', 'max'=>50),
            array('cuit', 'length', 'max'=>13),
            array('cp, zp', 'length', 'max'=>4),
            array('tipoCliente', 'length', 'max'=>2),
            array('id,observaciones,pieFactura, fechaConciliacion', 'safe'),
            array('cuit','validarCuit'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, razonSocial, direccion, nombreFantasia, contacto, cuit, telefonos, PDpto, email, fax, observaciones, id_rubro, direccionDeposito, codViajante, cp, zp, categoriaIva, tipoCliente, exeptuaIIB, pieFactura, saldoActual, saldoConciliado, fechaConciliacion', 'safe', 'on'=>'search'),
        );
	}

    public function validarCuit($attribute){
        //$this->$attribute
        if($this->isNewRecord){
            if($this->categoriaIva != 5 && $this->categoriaIva != 6 && $this->categoriaIva != 7 && $this->categoriaIva != 8){
                if($this->cuit == ''){

                    $this->addError($attribute, 'El cuit no puede ser vacio');
                }else{
                    if (!self::isValidCuit($this->cuit)){
                        $this->addError($attribute, 'El cuit no es válido');
                    }
                }
            }
        }
    }

    public static function isValidCuit($cuit) {
        $digits = array();
        if (strlen($cuit) != 11) return false;
        for ($i = 0; $i < strlen($cuit); $i++) {
            if (!ctype_digit($cuit[$i])) return false;
            if ($i < 11) {
                $digits[] = $cuit[$i];
            }
        }
        $acum = 0;
        foreach (array(5, 4, 3, 2, 7, 6, 5, 4, 3, 2) as $i => $multiplicador) {
            $acum += $digits[$i] * $multiplicador;
        }
        $cmp = 11 - ($acum % 11);
        if ($cmp == 11) $cmp = 0;
        if ($cmp == 10) $cmp = 9;
        return ($cuit[10] == $cmp);
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCp' => array(self::BELONGS_TO, 'CodigoPostal', 'cp'),
            'oRubro' => array(self::BELONGS_TO, 'RubroClientes', 'id_rubro'),
            'oCategoriaIva' => array(self::BELONGS_TO, 'CategoriaIva', 'categoriaIva'),
            'oViajante' => array(self::BELONGS_TO, 'Viajante', 'codViajante'),
            'aComprobante' => array(self::HAS_MANY, 'Comprobante', 'Id_Cliente'),
            'aNotaEntrega' => array(self::HAS_MANY, 'NotaEntrega', 'idCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'razonSocial' => 'Razon Social',
            'direccion' => 'Direccion',
            'nombreFantasia' => 'Nombre Fantasia',
            'contacto' => 'Contacto',
            'cuit' => 'Cuit',
            'telefonos' => 'Telefonos',
            'PDpto' => 'Pdpto',
            'email' => 'Email',
            'fax' => 'Fax',
            'observaciones' => 'Observaciones',
            'id_rubro' => 'Id Rubro',
            'direccionDeposito' => 'Direccion Deposito',
            'codViajante' => 'Cod Viajante',
            'cp' => 'Cp',
            'zp' => 'Zp',
            'categoriaIva' => 'Categoria Iva',
            'tipoCliente' => 'Tipo Cliente',
            'exeptuaIIB' => 'Exeptua IIB',
            'saldoActual' => 'Saldo Actual',
            'atencionSobre' => 'Atencion Sr.//a.',
            'saldoConciliado' => Yii::t('application', 'Saldo Conciliado'),
            'fechaConciliacion' => Yii::t('application', 'Fecha Conciliacion'),
		);
	}

	public function getTitle(){
	    if ($this->razonSocial == ''){
	        return $this->nombreFantasia;
        }else{
            return $this->razonSocial;
        }
    }

	public function getLocalidad() {
		$oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal'=>$this->cp,'zp' => $this->zp));
		$sLocalidad = '';
		if($oLocalidad != NULL){
			$sLocalidad = $oLocalidad->nombrelocalidad;
		}
		return $sLocalidad;
	}

	public function getProvincia() {
		$oCodigoPostal = CodigoPostal::model()->findByAttributes(array('codigoPostal'=>$this->cp,'zp' => $this->zp));
		$sProvincia = '';
		if($oCodigoPostal != NULL){
			$sProvincia = $oCodigoPostal->oProvincia->nombre;
		}
		return $sProvincia;
	}

    public function getNro_Puesto_Venta(){
        $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $this->cp, 'zp' => $this->zp));
        if ($oLocalidad != null && $oLocalidad->id_provincia != 'E') {
            return 9;
        } else {
            return 10;
        }
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		 $criteria->compare('id',$this->id);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('nombreFantasia',$this->nombreFantasia,true);
        $criteria->compare('contacto',$this->contacto,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('telefonos',$this->telefonos,true);
        $criteria->compare('PDpto',$this->PDpto,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('observaciones',$this->observaciones,true);
        $criteria->compare('id_rubro',$this->id_rubro);
        $criteria->compare('direccionDeposito',$this->direccionDeposito,true);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('cp',$this->cp,true);
        $criteria->compare('zp',$this->zp,true);
        $criteria->compare('categoriaIva',$this->categoriaIva);
        $criteria->compare('tipoCliente',$this->tipoCliente,true);
        $criteria->compare('saldoActual',$this->saldoActual);
        $criteria->compare('saldoConciliado',$this->saldoConciliado,true);
        $criteria->compare('fechaConciliacion',$this->fechaConciliacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('nombreFantasia',$this->nombreFantasia,true);
        $criteria->compare('contacto',$this->contacto,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('telefonos',$this->telefonos,true);
        $criteria->compare('PDpto',$this->PDpto,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('observaciones',$this->observaciones,true);
        $criteria->compare('id_rubro',$this->id_rubro);
        $criteria->compare('direccionDeposito',$this->direccionDeposito,true);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('cp',$this->cp,true);
        $criteria->compare('zp',$this->zp,true);
        $criteria->compare('categoriaIva',$this->categoriaIva);
        $criteria->compare('tipoCliente',$this->tipoCliente,true);
        $criteria->compare('saldoActual',$this->saldoActual);
        $criteria->compare('saldoConciliado',$this->saldoConciliado,true);
        $criteria->compare('fechaConciliacion',$this->fechaConciliacion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.razonSocial,t.nombreFantasia'
            ),

        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchListado()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
        $criteria->compare('id',$this->id);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('nombreFantasia',$this->nombreFantasia,true);
        $criteria->compare('contacto',$this->contacto,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('telefonos',$this->telefonos,true);
        $criteria->compare('PDpto',$this->PDpto,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('observaciones',$this->observaciones,true);
        $criteria->compare('id_rubro',$this->id_rubro);
        $criteria->compare('direccionDeposito',$this->direccionDeposito,true);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('cp',$this->cp,true);
        $criteria->compare('zp',$this->zp,true);
        $criteria->compare('categoriaIva',$this->categoriaIva);
        $criteria->compare('tipoCliente',$this->tipoCliente,true);
        $criteria->compare('saldoActual',$this->saldoActual);
        $criteria->compare('saldoConciliado',$this->saldoConciliado,true);
        $criteria->compare('fechaConciliacion',$this->fechaConciliacion,true);

        $criteria->select = array('t.id', 't.razonSocial', 't.direccion', 't.nombreFantasia', 't.telefonos', 't.id_rubro', 't.cp', 't.zp', 't.categoriaIva');


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.razonSocial,t.nombreFantasia'
            ),

        ));
    }

    public function searchWP2()
	    {
	        // @todo Please modify the following code to remove attributes that should not be searched.

	        $criteria=new CDbCriteria;

	        $criteria->compare('id',$this->id);
	        $criteria->compare('razonSocial',$this->razonSocial,true);
	        $criteria->compare('direccion',$this->direccion,true);
	        $criteria->compare('nombreFantasia',$this->nombreFantasia,true);
	        $criteria->compare('contacto',$this->contacto,true);
	        $criteria->compare('cuit',$this->cuit,true);
	        $criteria->compare('telefonos',$this->telefonos,true);
	        $criteria->compare('PDpto',$this->PDpto,true);
	        $criteria->compare('email',$this->email,true);
	        $criteria->compare('fax',$this->fax,true);
	        $criteria->compare('observaciones',$this->observaciones,true);
	        $criteria->compare('id_rubro',$this->id_rubro);
	        $criteria->compare('direccionDeposito',$this->direccionDeposito,true);
	        $criteria->compare('codViajante',$this->codViajante);
	        $criteria->compare('cp',$this->cp,true);
	        $criteria->compare('zp',$this->zp,true);
	        $criteria->compare('categoriaIva',$this->categoriaIva);
	        $criteria->compare('tipoCliente',$this->tipoCliente,true);
	        $criteria->compare('saldoActual',$this->saldoActual);
            $criteria->compare('saldoConciliado',$this->saldoConciliado,true);
            $criteria->compare('fechaConciliacion',$this->fechaConciliacion,true);

	        return new CActiveDataProvider($this, array(
	            'criteria'=>$criteria,
	            'pagination' => false,
	            'sort' => array(
	            	'defaultOrder' => 't.razonSocial,t.nombreFantasia'
	            ),
	        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchDeudores()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('nombreFantasia',$this->nombreFantasia,true);
        $criteria->compare('contacto',$this->contacto,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('telefonos',$this->telefonos,true);
        $criteria->compare('PDpto',$this->PDpto,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('observaciones',$this->observaciones,true);
        $criteria->compare('id_rubro',$this->id_rubro);
        $criteria->compare('direccionDeposito',$this->direccionDeposito,true);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('cp',$this->cp,true);
        $criteria->compare('zp',$this->zp,true);
        $criteria->compare('categoriaIva',$this->categoriaIva);
        $criteria->compare('tipoCliente',$this->tipoCliente,true);
        $criteria->condition = 'saldoActual < 0';


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'razonSocial,nombreFantasia',
            )
        ));
    }

	/**
	 * Retorna una lista de modelos basados enla busqueda del nombre.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchMultiple($cadena)
	{

		$cadena = addcslashes($cadena, '%_'); // escape LIKE's special characters
		$criteria = new CDbCriteria( array(
				'condition' => "LOWER(razonSocial) LIKE LOWER(:cadena) or LOWER(nombreFantasia) LIKE LOWER(:cadena) or LOWER(id) LIKE LOWER(:cadena)",     	// no quotes around :match
				'params'	=> array(':cadena' => "%$cadena%")  // Aha! Wildcards go here
		) );

		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

    public function searchShort(){
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('razonSocial',$this->razonSocial,'true');
        if ($this->nombreFantasia != '') {
            $criteria->condition = "nombreFantasia LIKE :nf";
            $criteria->params = array(':nf' => trim($this->nombreFantasia) . '%');
        }
        $criteria->compare('cuit',$this->cuit);


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.razonSocial',
            )
        ));
    }

    public function scopes()
    {
        return array(
            'todos'=>array(
                'order'=>'razonSocial asc',
            ),
            'last' => array(
                'order'=>'id desc',
            ),
        );
    }

    public function percibeIIBB(){
        if($this->oCp->id_provincia != 'E'){
            return false;
        }else{
            return (!$this->exeptuaIIB);

        }
    }

    public function esEscuela(){
        return ($this->id_rubro == 10 || $this->id_rubro == 19 || $this->id_rubro == 22);
    }

    public function esInstitucion(){
        // CATEGORIA EXCENTO
        return ($this->categoriaIva == 4);
    }

    public function getTotalFacturasConSaldo(){
        $aVDocumentosConSaldo = VDocumentosConSaldo::model()->findAllByAttributes(array('tipo' => 'F','Id_Cliente'=> $this->id));
        $total = 0;
        foreach($aVDocumentosConSaldo as $key => $value){
            $total += $value->saldo;
        }
        return $total;
    }

    public  function esFacturable()
    {
        if($this->cuit != '' && $this->categoriaIva != 7 && $this->categoriaIva != 5){
            return true;
        }
        return false;
    }

    public function getSaldoRecalculado(){
        $oCtaCte = new VCtaCteCliente('search');
        $oCtaCte->unsetAttributes();
        if (isset($_GET['VCtaCteCliente'])){
            $oCtaCte->attributes = $_GET['VCtaCteCliente'];
        }

        $fecha =  new DateTime($this->fechaConciliacion);
        $fecha->modify('+1 day');
        
        $oCtaCte->desde = $fecha->format('Y-m-d');
        $oCtaCte->clId = $this->id;

        $saldoRecalculado = $this->saldoConciliado;
        foreach ($oCtaCte->searchWP()->getData() as $key => $data) {
            $saldoRecalculado += $data->debe;
            $saldoRecalculado -= $data->haber;
        }

        return $saldoRecalculado;

    }

    public function beforeSave(){
        $estadoAnterior = Cliente::model()->findByPk($this->id);
        if($estadoAnterior != null){
            Yii::log('Estado Previo:: '.ComponentesComunes::print_array($estadoAnterior->attributes,true),'warning','custom');
        }

        Yii::log('Nuevo Estado :: '.ComponentesComunes::print_array($this->attributes,true),'warning','custom');
        return parent::beforeSave();

    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
