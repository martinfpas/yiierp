<?php

/**
 * This is the model class for table "conciliacionPagoCliente".
 *
 * The followings are the available columns in table 'conciliacionPagoCliente':
 * @property integer $id
 * @property integer $idComprobante
 * @property integer $idNotaEntrega
 * @property string $monto
 * @property integer $idPago
 *
 * The followings are the available model relations:
 * @property Comprobante $oIdComprobante
 * @property NotaEntrega $oIdNotaEntrega
 * @property PagoCliente $oIdPago
 */
class ConciliacionPagoCliente extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'conciliacionPagoCliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPago', 'required'),
			array('idComprobante, idNotaEntrega, idPago', 'numerical', 'integerOnly'=>true),
			array('monto', 'length', 'max'=>9),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idComprobante, idNotaEntrega, monto, idPago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdComprobante' => array(self::BELONGS_TO, 'Comprobante', 'idComprobante'),
			'oIdNotaEntrega' => array(self::BELONGS_TO, 'NotaEntrega', 'idNotaEntrega'),
			'oIdPago' => array(self::BELONGS_TO, 'PagoCliente', 'idPago'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Conciliacion Pago Cliente',
            'models' => 'Conciliacion Pago Clientes',
			'id' => Yii::t('application', 'ID'),
			'idComprobante' => Yii::t('application', 'Id Comprobante'),
			'idNotaEntrega' => Yii::t('application', 'Id Nota Entrega'),
			'monto' => Yii::t('application', 'Monto'),
			'idPago' => Yii::t('application', 'Id Pago'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idComprobante',$this->idComprobante);
		$criteria->compare('idNotaEntrega',$this->idNotaEntrega);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('idPago',$this->idPago);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConciliacionPagoCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
