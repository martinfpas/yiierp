<?php

/**
 * This is the model class for table "afipVoucher".
 *
 * The followings are the available columns in table 'afipVoucher':
 * @property integer $id
 * @property integer $numeroPuntoVenta
 * @property integer $codigoTipoComprobante
 * @property integer $codigoTipoDocumento
 * @property integer $codigoConcepto
 * @property string $numeroDocumento
 * @property string $caea
 * @property string $fechaVencimientoCAE
 * @property integer $numeroComprobante
 * @property string $fechaComprobante
 * @property double $importeOtrosTributos
 * @property string $fechaDesde
 * @property string $fechaHasta
 * @property string $fechaVtoPago
 * @property string $codigoMoneda
 * @property double $cotizacionMoneda
 * @property double $importeIVA
 * @property double $importeNoGravado
 * @property double $importeExento
 * @property double $importeGravado
 * @property double $importeTotal
 * @property string $result
 * @property array $CbtesAsoc
 * @property array $PeriodoAsoc
 *
 * The followings are the available model relations:
 * @property AfipSubtotivas[] $subtotivas
 * @property AfipTributo[] $Tributos
 * @property AfipVoucherItem[] $aAfipVoucherItems
 */
class AfipVoucher extends CActiveRecord
{

    // RELACION CODIGO => MONTO DE ALICUOTA EN AFIP
    public static $aIvaAlicuotas =  array
    (
        '3' => '0',
        '4' => '10.5',
        '5' => '21',
        '6' => '27',
        '8' => '5',
        '9' => '2.5'
    );

    public static $aCbteTipo2 = array(
        '1' => 'Factura A',
        '2' => 'Nota de Débito A',
        '3' => 'Nota de Crédito A',
        '6' => 'Factura B',
        '7' => 'Nota de Débito B',
        '8' => 'Nota de Crédito B',
    );

    public static $aCbteTipo = array(
        'F' => array(
            'A' => 1,
            'B' => 6,
        ),
        'ND' => array(
            'A' => 2,
            'B' => 7,
        ),
        'NC' => array(
            'A' => 3,
            'B' => 8,
        ),

    );

    public static $aCbteTipoStr = array(
        'F' => array(
            'A' => 'Factura A',
            'B' => 'Factura B',
        ),
        'NC' => array(
            'A' => 'Nota de Credito A',
            'B' => 'Nota de Credito B',
        ),
        'ND' => array(
            'A' => 'Nota de Débito A',
            'B' => 'Nota de Débito B',
        ),
    );
    
    public static $aTributos = Array
    (
            '1' => 'Impuestos nacionales',
            '2' => 'Impuestos provinciales',
            '3' => 'Impuestos municipales',
            '4' => 'Impuestos Internos',
            '5' => 'IIBB',
            '6' => 'Percepcion de IVA',
            '7' => 'Percepcion de IIBB',
            '8' => 'Percepciones por Impuestos Municipales',
            '9' => 'Otras Percepciones',
            '13' => 'Percepcion de IVA a no Categorizado',
            '99' => 'Otro',
        );

    public static $aDocTipo = array(
        'CUIT' => 80,
        'DNI' => 99,
    );

    public $aFactTipo = array(
        'A' => 5,
        'B' => 6,
    );


    public static $IIBB = 0;
	
	public $CbtesAsoc = array();
	public $items = array();
	public $PeriodoAsoc = array();

	public $subtotivas = array();
    public $Tributos = array();
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'afipVoucher';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numeroPuntoVenta, fechaComprobante, importeIVA, importeNoGravado, importeGravado, importeTotal', 'required'),
			array('numeroPuntoVenta, codigoTipoComprobante, codigoTipoDocumento, codigoConcepto, numeroComprobante', 'numerical', 'integerOnly'=>true),
			array('importeOtrosTributos, cotizacionMoneda, importeIVA, importeNoGravado, importeExento, importeGravado, importeTotal', 'numerical'),
			array('numeroDocumento', 'length', 'max'=>11),
			array('caea', 'length', 'max'=>14),
			array('fechaComprobante, fechaDesde, fechaHasta, fechaVtoPago', 'length', 'max'=>8),
			array('codigoMoneda', 'length', 'max'=>3),
            array('fechaVencimientoCAE, result', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, numeroPuntoVenta, codigoTipoComprobante, codigoTipoDocumento, codigoConcepto, numeroDocumento, caea, fechaVencimientoCAE, numeroComprobante, fechaComprobante, importeOtrosTributos, fechaDesde, fechaHasta, fechaVtoPago, codigoMoneda, cotizacionMoneda, importeIVA, importeNoGravado, importeExento, importeGravado, importeTotal,items, result', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'subtotivas' => array(self::HAS_MANY, 'AfipSubtotivas', 'idVoucher'),
			'Tributos' => array(self::HAS_MANY, 'AfipTributo', 'idVoucher'),
            'aAfipVoucherItems' => array(self::HAS_MANY, 'AfipVoucherItem', 'idVoucher'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'numeroPuntoVenta' => 'Numero Punto Venta',
			'codigoTipoComprobante' => 'Codigo Tipo Comprobante',
			'codigoTipoDocumento' => 'Codigo Tipo Documento',
			'codigoConcepto' => 'Codigo Concepto',
			'numeroDocumento' => 'Numero Documento',
			'caea' => 'Caea',
			'numeroComprobante' => 'Numero Comprobante',
			'fechaComprobante' => 'Fecha Comprobante',
			'importeOtrosTributos' => 'Importe Otros Tributos',
			'fechaDesde' => 'Fecha Desde',
			'fechaHasta' => 'Fecha Hasta',
			'fechaVtoPago' => 'Fecha Vto Pago',
			'codigoMoneda' => 'Codigo Moneda',
			'cotizacionMoneda' => 'Cotizacion Moneda',
			'importeIVA' => 'Importe Iva',
			'importeNoGravado' => 'Importe No Gravado',
			'importeExento' => 'Importe Exento',
			'importeGravado' => 'Importe Gravado',
			'importeTotal' => 'Importe Total',
            'result' => 'application', 'Resultado',
		);
	}

    public function setCbtesAsocData(){
	    if($this->codigoTipoComprobante <> 6 && $this->codigoTipoComprobante <> 1){
            $this->CbtesAsoc = array();
            $oNotaCredito = NotaCredito::model()->findByAttributes(array('idVoucherAfip'=>$this->id));
            $oNotaDebito = NotaDebito::model()->findByAttributes(array('idVoucherAfip'=>$this->id));
            if($oNotaCredito != null && $oNotaCredito->oFactura != null) {
                $oFactura = $oNotaCredito->oFactura;
                $this->CbtesAsoc[] = array(
                    'Tipo' => self::$aCbteTipo[$oFactura->tipo][$oFactura->clase],
                    'PtoVta' => $oFactura->Nro_Puesto_Venta,
                    'Nro' => $oFactura->Nro_Comprobante,
                    'Cuit' => $oFactura->cuit,
                    'CbteFch' => ComponentesComunes::fechaFormateada($oFactura->fecha,'Ymd'),
                );
                return;
            }elseif ($oNotaDebito != null){
                $oComprobante = Comprobante::model()->findByPk($oNotaDebito->idNotaDeCredito);
                if($oComprobante !=  null){
                    $this->CbtesAsoc[] = array(
                        'Tipo' => self::$aCbteTipo[$oComprobante->tipo][$oComprobante->clase],
                        'PtoVta' => $oComprobante->Nro_Puesto_Venta,
                        'Nro' => $oComprobante->Nro_Comprobante,
                        'Cuit' => $oComprobante->cuit,
                        'CbteFch' => ComponentesComunes::fechaFormateada($oComprobante->fecha,'Ymd'),
                    );
                }else {
                    throw new Exception('ND . Comprobante null :: idNotaDeCredito =>'.$oNotaDebito->idNotaDeCredito);
                }
            }else {
                //throw new Exception('ND y NC null');
                //PeriodoAsoc->
                $this->PeriodoAsoc = array(
                    'FchDesde' => date('Ymd') ,
                    'FchHasta' => date('Ymd')
                );
            }
        }

    }

	public function emitir(){
	    if ($this->caea != null){
            $oComprobante = Comprobante::model()->findByAttributes(array('idVoucherAfip'=>$this->id));
            if($oComprobante != null){
                $oComprobante->Nro_Comprobante = $this->numeroComprobante;
                $oComprobante->CAE = $this->caea;
                $oComprobante->Vencim_CAE = ($this->fechaVencimientoCAE == '0000-00-00')? null : $this->fechaVencimientoCAE;
                $oComprobante->estado = Comprobante::iCerrada;
                if (!$oComprobante->save()){
                    Yii::log('No se guardo la factura que le corresponde al voucher'.CHtml::errorSumary($oComprobante),'error');
                    throw new Exception('Voucher ya emitido y No se guardaron los datos del Comprobante que le corresponde al voucher '.$this->id,'1000');
                }
            }else{
                Yii::log('No se pudo encontrar la factura que le corresponde al voucher','error');
                throw new Exception('Voucher ya emitido y no hay comprobante para este voucher'.$this->id,'1001');
            }
	        throw new Exception('Voucher ya emitido ::'.$this->id,'1002');
        }

        $cuit = Yii::app()->params['cuit'];
        Yii::log('$cuit:: '.$cuit,'error');
        Yii::log('$afipVoucher:: '.$this->id,'error');
        require_once 'wsfev1.php';

        $aComprobante = self::getUltimoComprobanteAutorizado($this->numeroPuntoVenta, $this->codigoTipoComprobante);
        if(isset($aComprobante['number'])){
            $this->numeroComprobante = intval($aComprobante['number'])+1;
            $oVoucher = AfipVoucher::findByAttributes(array('numeroPuntoVenta' => $this->numeroPuntoVenta, 'codigoTipoComprobante' => $this->codigoTipoComprobante, 'numeroComprobante' =>$aComprobante['number'] ),array('order' => 't.id desc'));

            if($oVoucher != null){
                if($oVoucher->caea == ''){
                    throw new Exception('El ultimo comprobante no esta aprobado:: '.$aComprobante['number'].' id_base:'.$oVoucher->id,'1004');
                }

            }else{ // CHEQUEAR SI
                if(AfipVoucher::findByPk(array('numeroPuntoVenta' => $this->numeroPuntoVenta, 'codigoTipoComprobante' => $this->codigoTipoComprobante)) != null){
                    throw new Exception('No se encuentra el ultimo comprobante ::'.$this->id,'1003');
                };
            }

        }else{
            Yii::log('isset($aComprobante[\'number\']) nulo!!! ','error');
            $this->numeroComprobante = 1;
        }
        $this->setCbtesAsocData();

        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);

        $result = $afip->init(); //Crea el cliente SOAP
        if ($result["code"] === Wsfev1::RESULT_OK) {
            $oComprobante = Comprobante::model()->findByAttributes(array('idVoucherAfip'=>$this->id));
            if($oComprobante == null){
                throw new Exception('ESTO PREVIENE SALTEARSE NUMERACION ::  No se encontró el comprobante que le corresponde al voucher  ::'.$this->id,'1007');
            }

            $result = $afip->emitirComprobante($this); //$this debe tener la estructura esperada (ver a continuaci�n de la wiki)
            // PROCESA EL PEDIDO
            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo correctamente

                Yii::log('Wsfev1::RESULT_OK :: $result["cae"] => '.$result["cae"],'warning');
                $this->caea = $result["cae"];
                $this->fechaVencimientoCAE = $result['fechaVencimientoCAE'];
                $this->result = 'RESULT_OK';

                if($this->save()){
                    //$oComprobante = Comprobante::model()->findByPk($id);
                    $oComprobante = Comprobante::model()->findByAttributes(array('idVoucherAfip'=>$this->id));
                    if($oComprobante != null){
                        $oComprobante->Nro_Comprobante = $this->numeroComprobante;
                        $oComprobante->CAE = $this->caea;
                        $oComprobante->Vencim_CAE = ($this->fechaVencimientoCAE == '0000-00-00')? null : $this->fechaVencimientoCAE;
                        $oComprobante->estado = Comprobante::iCerrada;
                        if (!$oComprobante->save()){
                            throw new Exception('Comprobante emitido en afip :: No se guardo el comprobante que le corresponde al voucher ::'.$this->id,'1005');
                        }
                    }else{
                        throw new Exception('Comprobante emitido en afip :: No se encontró el comprobante que le corresponde al voucher ::'.$this->id,'1006');
                    }
                }else{
                    Yii::log('Error al guardar el voucher:'.CHtml::errorSummary($this),'error');
                    throw new Exception('Comprobante emitido en afip :: Error al guardar el voucher ::'.$this->id,'1006');
                };
                echo json_encode($this->attributes);
                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar
            } else {
                //Yii::log(print_r($this,true),'warning');
                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo emitirse la factura :: '.$result["msg"];
                $this->result = 'No pudo emitirse la factura :: '.$result["msg"];
                if ($result["code"] == '10016' ){
                    $this->numeroComprobante = NULL;
                }
                $this->save();
                throw new Exception('<span class="errorSpan">Error:'.$this->result.'</span>','1007');
            }
        } else {
            //echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo
            $this->result = 'No pudo emitirse la factura :: '.$result["msg"];
            $this->save();
            throw new Exception('<span class="errorSpan">Error al crear el cliente soap :'.$this->result.'</span>',1008);
        }
    }



	public static function getUltimoComprobanteAutorizado($PV, $TC) {

        require_once 'wsfev1.php';

        $cuit = Yii::app()->params['cuit'];
        $afip = new Wsfev1($cuit, Yii::app()->params['modoAfip'] /*Wsaa::MODO_HOMOLOGACION*/);



        $result = $afip->init(); //Crea el cliente SOAP



        if ($result["code"] === Wsfev1::RESULT_OK) {
            $result = $afip->consultarUltimoComprobanteAutorizado($PV, $TC); //$voucher debe tener la estructura esperada (ver a continuaci�n de la wiki)
            if ($result["code"] === Wsfev1::RESULT_OK) {
                //La facturacion electronica finalizo

                return ($result);

                //$result["cae"] y $result["fechaVencimientoCAE"] son datos que deber�as almacenar
            } else {
                //No pudo emitirse la factura y $result["msg"] contendr� el motivo
                echo '<br>No pudo consultarUltimoComprobanteAutorizado :: '.$result["msg"];
            }
        } else {
            echo '<br>No pudo crearse el cliente de conexi�n con AFIP  :: '.$result["msg"];
            //No pudo crearse el cliente de conexi�n con AFIP  y $result["msg"] contendr� el motivo
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numeroPuntoVenta',$this->numeroPuntoVenta);
		$criteria->compare('codigoTipoComprobante',$this->codigoTipoComprobante);
		$criteria->compare('codigoTipoDocumento',$this->codigoTipoDocumento);
		$criteria->compare('codigoConcepto',$this->codigoConcepto);
		$criteria->compare('numeroDocumento',$this->numeroDocumento,true);
		$criteria->compare('caea',$this->caea,true);
        if ($this->fechaVencimientoCAE != null){
            $criteria->compare('fechaVencimientoCAE',ComponentesComunes::jpicker2db($this->fechaVencimientoCAE));
        }


		$criteria->compare('numeroComprobante',$this->numeroComprobante);
		$criteria->compare('fechaComprobante',$this->fechaComprobante,true);
		$criteria->compare('importeOtrosTributos',$this->importeOtrosTributos);
		$criteria->compare('fechaDesde',$this->fechaDesde,true);
		$criteria->compare('fechaHasta',$this->fechaHasta,true);
		$criteria->compare('fechaVtoPago',$this->fechaVtoPago,true);
		$criteria->compare('codigoMoneda',$this->codigoMoneda,true);
		$criteria->compare('cotizacionMoneda',$this->cotizacionMoneda);
		$criteria->compare('importeIVA',$this->importeIVA);
		$criteria->compare('importeNoGravado',$this->importeNoGravado);
		$criteria->compare('importeExento',$this->importeExento);
		$criteria->compare('importeGravado',$this->importeGravado);
		$criteria->compare('importeTotal',$this->importeTotal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function afterFind()
    {

        $fSubTotIvas = 0;
        $fBase = 0;

        $aTributos = AfipTributo::model()->findAllByAttributes(array('idVoucher'=>$this->id));

        if(sizeof($aTributos) > 0){
            foreach ($aTributos as $index => $aTributo) {

                $this->Tributos[] = $aTributo->attributes;
            }
            //Yii::log('array 3 TRIBUTOS:'.print_r($aTributos,true),'warning');
        }

        $aSubtotIvas = AfipSubtotivas::model()->findAllByAttributes(array('idVoucher'=>$this->id));

        if (sizeof($aSubtotIvas) > 0){
            foreach ($aSubtotIvas as $index => $aSubtotIva) {
                $this->subtotivas[] = $aSubtotIva->attributes;
            }
        }

        /*
        if ($this->codigoTipoComprobante == 1){ // FACTURA A
            foreach ($this->aAfipVoucherItems as $index => $oAfipVoucherItem) {
                $this->items[] = $oAfipVoucherItem->attributes;

                $neto = ($oAfipVoucherItem->importeItem / 1.21);
                $iva = $oAfipVoucherItem->importeItem - $neto;
                $fSubTotIvas += $iva; //TODO: VER SI ES FACTURA A
                $fBase += $neto ;
            }

            $fSubTotIvas =  number_format($fSubTotIvas,2,'.','');
            $fBase =  number_format($fBase,2,'.','');


            if($this->importeIVA != $fSubTotIvas && $this->importeIVA == 0 ){
                $this->importeIVA = $fSubTotIvas;
                $this->save();
            }

            $this->subtotivas[] = array('codigo'=>5,'BaseImp'=>number_format($fBase,2,'.',''),'importe'=>number_format($fSubTotIvas,2,'.',''));
        }else{

            $fBase = $this->importeTotal / 1.21;
            $fBase = number_format($fBase,2,'.','');
            $fSubTotIvas = $this->importeTotal - $fBase;

            $this->subtotivas[] = array('codigo'=>5,'BaseImp'=>number_format($fBase,2,'.',''),'importe'=>number_format($fSubTotIvas,2,'.',''));
        }
        */
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AfipVoucher the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
