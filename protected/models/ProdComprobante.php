<?php

/**
 * This is the model class for table "prodComprobante".
 *
 * The followings are the available columns in table 'prodComprobante':
 * @property integer $id
 * @property integer $idComprobante
 * @property integer $nroItem
 * @property integer $idArtVenta
 * @property double $cantidad
 * @property string $fechaEntrega
 * @property double $precioUnitario
 * @property double $alicuotaIva
 * @property integer $cantidadEntregada
 * @property integer $nroPuestoVenta
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property Comprobante $oComprobante
 * @property ArticuloVenta $oArtVenta
 * @property PuntoDeVenta $oNroPuestoVenta
 */
class ProdComprobante extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'prodComprobante';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idComprobante, idArtVenta, descripcion', 'required'),
            array('idComprobante, nroItem, idArtVenta, cantidadEntregada, nroPuestoVenta', 'numerical', 'integerOnly'=>true),
            array('cantidad, precioUnitario, alicuotaIva', 'numerical'),
            //array('descripcion', 'length', 'max'=>40),
            array('fechaEntrega', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, idComprobante, nroItem, idArtVenta, cantidad, fechaEntrega, precioUnitario, alicuotaIva, cantidadEntregada, nroPuestoVenta, descripcion', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'oComprobante' => array(self::BELONGS_TO, 'Comprobante', 'idComprobante'),
            'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
            'oNroPuestoVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'nroPuestoVenta'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'idComprobante' => 'Id Comprobante',
            'nroItem' => 'Nro Item',
            'idArtVenta' => 'Id Art Venta',
            'cantidad' => 'Cantidad',
            'fechaEntrega' => 'Fecha Entrega',
            'precioUnitario' => 'Precio Unitario',
            'alicuotaIva' => 'Alicuota Iva',
            'cantidadEntregada' => 'Cantidad Entregada',
            'nroPuestoVenta' => 'Nro Puesto Venta',
            'descripcion' => 'Descripcion',
        );
    }

    public function getIva(){
        return number_format(((floatval(str_replace(',','',$this->precioUnitario)) * $this->cantidad))*($this->alicuotaIva/100),2, '.', '');
    }

    public function getImporte(){
        return number_format((floatval(str_replace(',','',$this->precioUnitario)) * $this->cantidad),2, '.', '');
    }

    public function getPrecioUnitario(){
        return number_format(floatval(str_replace(',','',$this->precioUnitario)),2, '.', '');
    }

    public static function prods2VoucherItems($idComprobante,$clase,$idVoucherAfip){
        $aProds = self::model()->findAllByAttributes(array('idComprobante' => $idComprobante));
        $oProdComprobante = new ProdComprobante();
		Yii::log('inside prods2VoucherItems','warning');
        if($clase == "A"){
            foreach ($aProds as $index => $oProdComprobante) {
                $oAfipVoucherItem = new AfipVoucherItem();
                $oAfipVoucherItem->descripcion = $oProdComprobante->descripcion;
                $oAfipVoucherItem->idVoucher = $idVoucherAfip;
                $oAfipVoucherItem->codigo = $oProdComprobante->oArtVenta->getCodigo();
                $oAfipVoucherItem->cantidad = $oProdComprobante->cantidad;
                //$oAfipVoucherItem->codigoUnidadMedida =
                $oAfipVoucherItem->precioUnitario = number_format($oProdComprobante->precioUnitario,2,'.','');
                $oAfipVoucherItem->importeItem = number_format($oProdComprobante->getImporte(),2,'.','');
                if(!$oAfipVoucherItem->save()){
                	Yii::log('Error en oAfipVoucherItem::'.CHtml::errorSummary($oAfipVoucherItem),'error');
                	throw new Exception(CHtml::errorSummary($oAfipVoucherItem));
                };

            }
        }else{
            foreach ($aProds as $index => $oProdComprobante) {
                $oAfipVoucherItem = new AfipVoucherItem();
                $x=$oProdComprobante->descripcion;
                $oAfipVoucherItem->descripcion = $oProdComprobante->descripcion;
                $oAfipVoucherItem->idVoucher = $idVoucherAfip;
                $oAfipVoucherItem->codigo = $oProdComprobante->oArtVenta->getCodigo();
                $oAfipVoucherItem->cantidad = $oProdComprobante->cantidad;
                //$oAfipVoucherItem->codigoUnidadMedida =
                $oAfipVoucherItem->precioUnitario = number_format($oProdComprobante->precioUnitario * (1-($oProdComprobante->oComprobante->alicuotaIva / 100)),2,'.','');
                $oAfipVoucherItem->importeItem = number_format($oProdComprobante->getImporte() * (1-($oProdComprobante->alicuotaIva / 100)),2,'.','');
                if(!$oAfipVoucherItem->save()){
					Yii::log('Error en oAfipVoucherItem::'.CHtml::errorSummary($oAfipVoucherItem),'error');
					throw new Exception(CHtml::errorSummary($oAfipVoucherItem));
                };

            }
        }

    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('idComprobante',$this->idComprobante);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('cantidad',$this->cantidad);
        if ($this->fechaEntrega != null){
            $criteria->compare('fechaEntrega',ComponentesComunes::fechaComparada($this->fechaEntrega));
        }

        $criteria->compare('precioUnitario',$this->precioUnitario);
        $criteria->compare('alicuotaIva',$this->alicuotaIva);
        $criteria->compare('cantidadEntregada',$this->cantidadEntregada);
        $criteria->compare('nroPuestoVenta',$this->nroPuestoVenta);
        $criteria->compare('descripcion',$this->descripcion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,

        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchWP()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('idComprobante',$this->idComprobante);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('cantidad',$this->cantidad);
        if ($this->fechaEntrega != null){
            $criteria->compare('fechaEntrega',ComponentesComunes::jpicker2db($this->fechaEntrega));
        }
        $criteria->compare('fechaEntrega',$this->fechaEntrega,true);
        $criteria->compare('precioUnitario',$this->precioUnitario);
        $criteria->compare('cantidadEntregada',$this->cantidadEntregada);
        $criteria->compare('nroPuestoVenta',$this->nroPuestoVenta);
        $criteria->compare('descripcion',$this->descripcion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProdComprobante the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}