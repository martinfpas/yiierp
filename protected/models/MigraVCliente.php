<?php

/**
 * This is the model class for table "migra_vCliente".
 *
 * The followings are the available columns in table 'migra_vCliente':
 * @property integer $id
 * @property string $razonSocial
 * @property string $direccion
 * @property string $nombreFantasia
 * @property string $contacto
 * @property string $cuit
 * @property string $telefonos
 * @property string $PDpto
 * @property string $email
 * @property string $fax
 * @property string $observaciones
 * @property string $id_rubro
 * @property string $direccionDeposito
 * @property integer $codViajante
 * @property integer $cp
 * @property integer $zp
 * @property string $categoriaIva
 * @property string $tipoCliente
 * @property double $saldoActual
 */
class MigraVCliente extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MigraVCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'migra_vCliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, codViajante, cp, zp', 'numerical', 'integerOnly'=>true),
			array('saldoActual', 'numerical'),
			array('razonSocial, nombreFantasia, telefonos, email', 'length', 'max'=>40),
			array('direccion, contacto, PDpto, fax, direccionDeposito', 'length', 'max'=>30),
			array('cuit', 'length', 'max'=>13),
			array('id_rubro', 'length', 'max'=>2),
			array('categoriaIva, tipoCliente', 'length', 'max'=>1),
			array('observaciones', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, razonSocial, direccion, nombreFantasia, contacto, cuit, telefonos, PDpto, email, fax, observaciones, id_rubro, direccionDeposito, codViajante, cp, zp, categoriaIva, tipoCliente, saldoActual', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'razonSocial' => Yii::t('application', 'Razon Social'),
		            'direccion' => Yii::t('application', 'Direccion'),
		            'nombreFantasia' => Yii::t('application', 'Nombre Fantasia'),
		            'contacto' => Yii::t('application', 'Contacto'),
		            'cuit' => Yii::t('application', 'Cuit'),
		            'telefonos' => Yii::t('application', 'Telefonos'),
		            'PDpto' => Yii::t('application', 'Pdpto'),
		            'email' => Yii::t('application', 'Email'),
		            'fax' => Yii::t('application', 'Fax'),
		            'observaciones' => Yii::t('application', 'Observaciones'),
		            'id_rubro' => Yii::t('application', 'Id Rubro'),
		            'direccionDeposito' => Yii::t('application', 'Direccion Deposito'),
		            'codViajante' => Yii::t('application', 'Cod Viajante'),
		            'cp' => Yii::t('application', 'Cp'),
		            'zp' => Yii::t('application', 'Zp'),
		            'categoriaIva' => Yii::t('application', 'Categoria Iva'),
		            'tipoCliente' => Yii::t('application', 'Tipo Cliente'),
		            'saldoActual' => Yii::t('application', 'Saldo Actual'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('nombreFantasia',$this->nombreFantasia,true);
		$criteria->compare('contacto',$this->contacto,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('telefonos',$this->telefonos,true);
		$criteria->compare('PDpto',$this->PDpto,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('observaciones',$this->observaciones,true);
		$criteria->compare('id_rubro',$this->id_rubro,true);
		$criteria->compare('direccionDeposito',$this->direccionDeposito,true);
		$criteria->compare('codViajante',$this->codViajante);
		$criteria->compare('cp',$this->cp);
		$criteria->compare('zp',$this->zp);
		$criteria->compare('categoriaIva',$this->categoriaIva,true);
		$criteria->compare('tipoCliente',$this->tipoCliente,true);
		$criteria->compare('saldoActual',$this->saldoActual);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}