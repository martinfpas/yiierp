<?php

/**
 * This is the model class for table "prodNotaPedido".
 *
 * The followings are the available columns in table 'prodNotaPedido':
 * @property integer $id
 * @property integer $idNotaPedido
 * @property integer $idArtVenta
 * @property double $cantidad
 * @property double $precioUnit
 * @property double $costo
 * @property double $subTotal
 * @property integer $idPaquete
 *
 * The followings are the available model relations:
 * @property NotaPedido $oNotaPedido
 * @property ArticuloVenta $oArtVenta
 * @property Paquete $oPaquete
 */
class ProdNotaPedido extends CActiveRecord
{
	public $id_rubro;
	public $id_articulo;
	public $id_presentacion;
    public $id_carga;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prodNotaPedido';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idNotaPedido, idArtVenta, cantidad, precioUnit, idPaquete', 'required'),
			array('idNotaPedido, idArtVenta, idPaquete', 'numerical', 'integerOnly'=>true),
			array('cantidad, precioUnit,costo ,subTotal', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idNotaPedido, idArtVenta, cantidad, precioUnit, costo ,subTotal, idPaquete,id_carga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oNotaPedido' => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
			'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
            'oPaquete' => array(self::BELONGS_TO, 'Paquete', 'idPaquete'),
		);
	}



    /**
     * @return float
     */
    public function getSubTotal()
    {
        if($this->subTotal <> ''){
            return number_format($this->subTotal,2, '.', '');
        }
        return number_format(($this->cantidad * $this->precioUnit),2, '.', '');

    }

    /**
     * @return float
     */
    public function getCostoTotal()
    {
        return number_format(($this->cantidad * $this->costo),2, '.', '');
    }

    /**
     * @return int
     */
    public function getCantXbulto()
    {
        return $this->oPaquete->cantidad;
    }

    /**
     * @return ProdCarga
     */
    public function crearProdCarga()
    {
        if ($this->oNotaPedido->idCarga != null ){
            Yii::log('$this->idArtVenta >> '.$this->idArtVenta.'>> idPaquete'.$this->idPaquete,'warning');
            $oProdCargaX = ProdCarga::model()->findByAttributes(array('idArtVenta' => $this->idArtVenta,'idPaquete'  => $this->idPaquete, 'idCarga' => $this->oNotaPedido->idCarga));
            if ($oProdCargaX !== null){
                $oProdCargaX->cantidad += $this->cantidad;
                if ($oProdCargaX->save()){
                    Yii::log('updateProdCarga >>>>>>>>>>>> '.$this->oNotaPedido->idCarga,'warning');
                    return $oProdCargaX;
                }else{
                    Yii::log('$oProdCargaX->save() :: $this->getErrors() ::  '.CHtml::errorSummary($oProdCargaX),'error');
                    return null;
                }

            }

            $oProdCarga = new ProdCarga();
            $oProdCarga->idCarga = $this->oNotaPedido->idCarga;
            $oProdCarga->cantidad = $this->cantidad;
            $oProdCarga->idArtVenta = $this->idArtVenta;
            $oProdCarga->idPaquete = $this->idPaquete;
            // TODO: VER SI ES NECESARIO MANEJAR ESTE ERROR
            if ($oProdCarga->save()){
                Yii::log('crearProdCarga >>>>>>>>>>>> '.$this->oNotaPedido->idCarga,'warning');
                return $oProdCarga;
            }else{
                Yii::log('$oProdCarga->save() :: $this->getErrors() ::  '.CHtml::errorSummary($oProdCarga),'error');
                return null;
            }
        }else{
            Yii::log('($this->oNotaPedido->idCarga == null )','warning');
            return null;
        }

    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idNotaPedido' => 'Id Nota Pedido',
			'idArtVenta' => 'Id Art Venta',
			'cantidad' => 'Cantidad',
			'precioUnit' => 'Precio Unit',
			'subTotal' => 'Sub Total',
            'costo' => 'Costo',
			'idPaquete' => 'Id Paquete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with = array('oNotaPedido');

		$criteria->compare('id',$this->id);
		$criteria->compare('idNotaPedido',$this->idNotaPedido);
		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('precioUnit',$this->precioUnit);
		$criteria->compare('subTotal',$this->subTotal);
		$criteria->compare('idPaquete',$this->idPaquete);

        $criteria->compare('oNotaPedido.idCarga',$this->id_carga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArtVenta');

        $criteria->compare('id',$this->id);
        $criteria->compare('idNotaPedido',$this->idNotaPedido);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('precioUnit',$this->precioUnit);
        $criteria->compare('subTotal',$this->subTotal);
        $criteria->compare('idPaquete',$this->idPaquete);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            )
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProdNotaPedido the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function beforeDelete(){
        if ($this->oNotaPedido->oCarga != null ){
            if($this->oNotaPedido->oCarga->estado >= Carga::iFacturada){
                throw new Exception('No se puede borrar un producto de un pedido cuya carga fue Facturada o Despachada');
            }
        }

        return parent::beforeDelete();
    }


	/*
	 * EL 
	 * */
    public function beforeSave() {
        //TODO: SACAR LOS LOGUEOS
        $estadoPrevio = null;
        if(!$this->isNewRecord){
            $estadoPrevio = self::model()->findByPk($this->id);
        }
        $resto = $this->cantidad % $this->oPaquete->cantidad;
        $oProdResto = null;

        if($this->oPaquete->id_artVenta != $this->idArtVenta){
            throw new Exception('Error en el paquete');
        }

        if(($this->oPaquete->cantidad != 1) && ($resto > 0)){

            if($this->cantidad > $resto){
                $this->cantidad = $this->cantidad - $resto;
            }
            $oPaquete = Paquete::model()->desc()->findByAttributes(array('id_artVenta' => $this->oPaquete->id_artVenta), 'abs(cantidad) <= '.$resto);


            Yii::log('$resto >>'.$resto,'warning');
            if ($oPaquete != null){
                $oProdResto = new ProdNotaPedido;
                $oProdResto->attributes = $this->attributes;
                $oProdResto->idPaquete = $oPaquete->id;
                $oProdResto->cantidad = $resto;
                Yii::log('El paguete del $oProdResto es '.$oPaquete->id.' >> '.$oPaquete->cantidad,'warning');
            }else{
                Yii::log('El paguete del $oProdResto es null *!!'.$this->oPaquete->id_envase.' >> '.$this->oPaquete->id_artVenta,'warning');
            }

        }else{
            Yii::log('$this->oPaquete->cantidad >>'.$this->oPaquete->cantidad,'warning');
        }

        if(parent::beforeSave()){
            if (($this->oNotaPedido->idCarga > 0) && ($estadoPrevio != null) && ($estadoPrevio->cantidad != $this->cantidad)){
                $this->oNotaPedido->oCarga->descontarProd($this->idArtVenta,intval($estadoPrevio->cantidad - $this->cantidad));
            }

            if ($oProdResto != null){
                Yii::log('Guardando $oProdResto','warning');
                if (!$oProdResto->save()){
                    Yii::log('error al guardar $oProdResto','warning');
                }else{
                    Yii::log('error al guardar '.CHtml::errorSummary($oProdResto),'error');
                };

            }else{
                Yii::log('$oProdResto es null !','warning');
            }

            return true;
        }else{
            return false;
        }
    }

    public function afterSave(){

        if ($this->oNotaPedido->oCarga != null ){
            if($this->oNotaPedido->oCarga->estado >= Carga::iFacturada){
                throw new Exception('No se puede modificar un producto de un pedido cuya carga fue Facturada o Despachada');
            }
        }

        if ($this->oNotaPedido->oFactura != null){
            // SI CAMBIA UN PRODUCTO DE LA NOTA DE PEDIDO, Y HAY UNA FACTURA ASOCIADA A LA NOTA DE PEDIDO, LA INTENTO BORRAR
            if (!$this->oNotaPedido->oFactura->delete()){
                Yii::log('Error al borrar Factura'.CHtml::errorSummary($this->oNotaPedido->oFactura),'error');
            }
        }

        return parent::afterSave();
    }



}
