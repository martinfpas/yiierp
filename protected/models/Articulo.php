<?php

/**
 * This is the model class for table "articulo".
 *
 * The followings are the available columns in table 'articulo':
 * @property integer $id
 * @property integer $id_articulo
 * @property string $descripcion
 * @property integer $id_rubro
 *
 * The followings are the available model relations:
 * @property Rubro $oRubro
 */
class Articulo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articulo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
      return array( 
      		
            array('id_articulo, descripcion, id_rubro', 'required'),
            array('id_articulo, id_rubro', 'numerical', 'integerOnly'=>true),
            array('descripcion', 'length', 'max'=>30),
      		array('id_articulo, id_rubro', 'ECompositeUniqueValidator',
      				'attributesToAddError'=>'id_articulo',
      				'message'=>'El id_articulo {value_id_articulo} ya existe para este rubro.'),
            // The following rule is used by search(). 
            // @todo Please remove those attributes that should not be searched. 
            array('id, id_articulo, descripcion, id_rubro', 'safe', 'on'=>'search'), 
        );  
	}
	

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oRubro' => array(self::BELONGS_TO, 'Rubro', 'id_rubro'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_articulo' => 'Id Articulo',
			'descripcion' => 'Descripcion',
			'id_rubro' => 'Id Rubro',
		);
	}
	
	public function getIdTitulo() {
		return $this->id_articulo.' - '.$this->descripcion;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array('oRubro');
		$criteria->compare('id',$this->id);
		$criteria->compare('t.id_articulo',$this->id_articulo);
		$criteria->compare('t.descripcion',$this->descripcion,true);
		$criteria->compare('t.id_rubro',$this->id_rubro);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
				'defaultOrder' => 't.id desc',
				'attributes' => array(
					'id_rubro' => array(
						'asc' => 'oRubro.descripcion asc',
						'desc' => 'oRubro.descripcion desc',
					),
					'*',
				),		
			),
		));
	}
	
	
	public function scopes()
	{
		return array(
				'todos'=>array(
						'order'=>'descripcion asc',
				),
				'todosId'=>array(
						'order'=>'id_articulo asc',
				),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Articulo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
