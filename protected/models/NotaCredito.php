<?php

/**
 * This is the model class for table "notaCredito".
 *
 * The followings are the available columns in table 'notaCredito':
 * @property integer $id
 * @property string $tipo
 * @property string $clase
 * @property integer $Nro_Puesto_Venta
 * @property string $Nro_Comprobante
 * @property integer $Iva_Responsable
 * @property integer $Nro_Viajante
 * @property integer $Id_Cliente
 * @property string $razonSocial
 * @property string $direccion
 * @property string $localidad
 * @property string $provincia
 * @property string $rubro
 * @property string $zona
 * @property string $tipoResponsable
 * @property string $cuit
 * @property integer $perIIBB
 * @property string $nroInscripIIBB
 * @property string $fecha
 * @property string $Moneda
 * @property string $Observaciones
 * @property integer $Id_Camion
 * @property double $Descuento1
 * @property double $Descuento2
 * @property integer $Enviada
 * @property integer $Recibida
 * @property double $Otro_Iva
 * @property string $CAE
 * @property string $Vencim_CAE
 * @property integer $idNotaPedido
 * @property integer $idVoucherAfip
 * @property double $alicuotaIva
 * @property double $netoGravado
 * @property double $subtotalIva
 * @property double $alicuotaIIBB
 * @property double $subtotal
 * @property double $Ing_Brutos
 * @property double $Total_Neto
 * @property double $Total_Final
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Viajante $oViajante
 * @property Vehiculo $oCamion
 * @property PuntoDeVenta $oPuestoVenta
 * @property AfipVoucher $oVoucherAfip
 * @property Cliente $oCliente
 * @property CategoriaIva $oIvaResponsable
 * @property ProdNotaCredito[] $aProdNotaCredito
 * @property Factura $oFactura
 * @property NotaDebito $oNotaDebito
 */

/* @var $oFacturaProd ProdFactura */
class NotaCredito extends Comprobante
{
    const borrador = 'Borrador';
    const guardada = 'Guardada';
    const cerrada = 'Cerrada';
    const archivada = 'Archivada';

    const iNoEnviada = 0;
    const iEnviada = 1;

    public $idFactura = null;

    public static $aClase = array(
        'A' => 'A',
        'B' => 'B',
    );

    public static function alias(){
        return __CLASS__;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return NotaCredito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobante';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clase, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion', 'required'),
            array('Nro_Puesto_Venta, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion, idNotaPedido, idVoucherAfip', 'numerical', 'integerOnly'=>true),
            array('Descuento1, Descuento2, Otro_Iva, alicuotaIva, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final', 'numerical'),
            array('tipo', 'length', 'max'=>2),
            array('clase', 'length', 'max'=>1),
            array('Nro_Comprobante, Moneda', 'length', 'max'=>8),
            array('razonSocial, direccion', 'length', 'max'=>50),
            array('localidad', 'length', 'max'=>65),
            array('provincia', 'length', 'max'=>40),
            array('rubro', 'length', 'max'=>35),
            array('zona', 'length', 'max'=>4),
            array('tipoResponsable, nroInscripIIBB', 'length', 'max'=>30),
            array('cuit', 'length', 'max'=>13),
            array('CAE', 'length', 'max'=>14),
            array('Enviada, Recibida','boolean'),
            array('fecha, Observaciones, Vencim_CAE, estado', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, tipo, clase, Nro_Puesto_Venta, Nro_Comprobante, Iva_Responsable, Nro_Viajante, Id_Cliente, razonSocial, direccion, localidad, provincia, rubro, zona, tipoResponsable, cuit, perIIBB, nroInscripIIBB, fecha, Moneda, Observaciones, Id_Camion, Descuento1, Descuento2, Enviada, Recibida, Otro_Iva, CAE, Vencim_CAE, idNotaPedido, idVoucherAfip, alicuotaIva, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final, estado', 'safe', 'on'=>'search'),
        );
    }

    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oViajante'         => array(self::BELONGS_TO, 'Viajante', 'Nro_Viajante'),
			'oCamion'           => array(self::BELONGS_TO, 'Vehiculo', 'Id_Camion'),
            'oNotaPedido'       => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
            'oPuestoVenta'      => array(self::BELONGS_TO, 'PuntoDeVenta', 'Nro_Puesto_Venta'),
            'oVoucherAfip'      => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucherAfip'),
            'oCliente'          => array(self::BELONGS_TO, 'Cliente', 'Id_Cliente'),
            'oIvaResponsable'   => array(self::BELONGS_TO, 'CategoriaIva', 'Iva_Responsable'),
			'aProdNotaCredito'  => array(self::HAS_MANY, 'ProdNotaCredito', 'idComprobante'),
            'aProdComprobante'  => array(self::HAS_MANY, 'ProdNotaCredito', 'idComprobante'),
            'oFactura'          => array(self::HAS_ONE, 'Factura', 'idNotaDeCredito'),
            'oNotaDebito'       => array(self::HAS_ONE, 'NotaDebito', 'idNotaDeCredito'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
        return array(
            'model' => 'Nota De Crédito',
            'models' => 'Notas De Crédito',
            'id' => 'ID',
            'tipo' => 'Tipo',
            'clase' => 'Clase',
            'Nro_Puesto_Venta' => 'Nro Puesto Venta',
            'Nro_Comprobante' => 'Nro Comprobante',
            'Iva_Responsable' => 'Iva Responsable',
            'Nro_Viajante' => 'Nro Viajante',
            'Id_Cliente' => 'Id Cliente',
            'razonSocial' => 'Razon Social',
            'direccion' => 'Direccion',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
            'rubro' => 'Rubro',
            'zona' => 'Zona',
            'tipoResponsable' => 'Tipo Responsable',
            'cuit' => 'Cuit',
            'perIIBB' => 'Per Iibb',
            'nroInscripIIBB' => 'Nro Inscrip Iibb',
            'fecha' => 'Fecha',
            'Moneda' => 'Moneda',
            'Observaciones' => 'Observaciones',
            'Id_Camion' => 'Camion',
            'Descuento1' => 'Descuento1',
            'Descuento2' => 'Descuento2',
            'Enviada' => 'Enviada',
            'Recibida' => 'Recibida',
            'Otro_Iva' => 'Otro Iva',
            'CAE' => 'Cae',
            'Vencim_CAE' => 'Vencim Cae',
            'idNotaPedido' => 'Nota Pedido',
            'idVoucherAfip' => 'Voucher Afip',
            'alicuotaIva' => 'Alicuota Iva',
            'subtotalIva' => 'Subtotal Iva',
            'alicuotaIIBB' => 'Alicuota Iibb',
            'subtotal' => 'Subtotal',
            'Ing_Brutos' => 'Ing Brutos',
            'Total_Neto' => 'Total Neto',
            'Total_Final' => 'Total Final',
        );
	}



    //////////////////////////
    /**
     * Devuelve el subtotal sin descuentos
     *

    public function getSubTotalNeto(){

        $fSubtotal=0;
        foreach ($this->aProdNotaCredito as $index => $oProdComprobante) {
            $fSubtotal += number_format($oProdComprobante->getImporte(),2, '.', '');
        }
        return number_format($fSubtotal,2, '.', '');
    }
*/
    public function defaultScope()
    {
        return array(
            'alias' => self::alias(),
            'condition' => 'tipo = "NC"',
        );

    }

    public function scopes(){
        return array(
            'noEnviadas' => array(
                'condition' => 'Enviada = 0'
            ),
        );
    }

    public function crearNotaDebito(){
	    // TODO: BEGNIG TRANSACTION
        $transaction=Yii::app()->db->beginTransaction();

    	$oNotaDebito = new NotaDebito();
    	$oNotaDebito->clase = $this->clase;
    	$oNotaDebito->Nro_Puesto_Venta = $this->Nro_Puesto_Venta;
    	$oNotaDebito->Iva_Responsable = $this->Iva_Responsable;
        $oNotaDebito->Nro_Viajante = $this->Nro_Viajante;
        $oNotaDebito->Id_Cliente = $this->Id_Cliente;
        $oNotaDebito->fecha = date("Y-m-d H:i:s");
        $oNotaDebito->Id_Camion = $this->Id_Camion;
        $oNotaDebito->Total_Final = $this->Total_Final;
        $oNotaDebito->Enviada = 0;
        $oNotaDebito->Recibida = 0;
        $oNotaDebito->Observaciones = $this->Observaciones;
        $oNotaDebito->Otro_Iva = $this->Otro_Iva;
        $oNotaDebito->Descuento1 = $this->Descuento1;
        $oNotaDebito->Descuento2 = $this->Descuento2;
        $oNotaDebito->Total_Neto = $this->Total_Neto;
        $oNotaDebito->CAE = 0;
        $oNotaDebito->Vencim_CAE = null;
        $oNotaDebito->Ing_Brutos = $this->Ing_Brutos;
        $oNotaDebito->idNotaDeCredito = $this->id;
        $oNotaDebito->Nro_Comprobante = $oNotaDebito->getNextNumber();

        if ($oNotaDebito->save()){
            $oNotaDebito->cloneNotaCreditoItem($this->id);

            $this->idNotaDeCredito = $oNotaDebito->id;
            if (!$this->save($this->save())){
                Yii::log('Error al guardar el idNotaDeCredito asociado:'.CHtml::errorSummary($this),'error');
            }
            $transaction->commit();

        }else{
            Yii::log('Error al guardar NC'.CHtml::errorSummary($oNotaDebito),'error');
            $transaction->rollback();
        }

    	return $oNotaDebito;
    }

    /* @var $oNotaCredito NotaCredito */
    /* @var $oCliente Cliente */
    public static function generarNC($idCliente){
        $oCliente = Cliente::model()->findByPk($idCliente);

        if($oCliente != null){
            $oNotaCredito = new NotaCredito();
            $oNotaCredito->Nro_Viajante = $oCliente->codViajante;
            $oNotaCredito->Iva_Responsable = $oCliente->categoriaIva;
            $oNotaCredito->Id_Cliente = $idCliente;
            $oNotaCredito->Id_Camion = 6;
            $oNotaCredito->Total_Final = 0;
            $oNotaCredito->clase = $oCliente->oCategoriaIva->tipoFact;
            $oNotaCredito->fecha = date('Y-m-d');


            $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $oCliente->cp, 'zp' => $oCliente->zp));
            if ($oLocalidad != null && $oLocalidad->id_provincia != 'E') {
                $oNotaCredito->Nro_Puesto_Venta = 9;
            } else {
                $oNotaCredito->Nro_Puesto_Venta = 10;
            }

            if ($oNotaCredito->save()) {
                return $oNotaCredito;
            } else {
                Yii::log('$oFactura->save() :: ' . CHtml::errorSummary($oNotaCredito), 'error');
                throw new Exception(CHtml::errorSummary($oNotaCredito));
            }


        }else{
            Yii::log($idCliente.'$idCliente  es invalido','error');
            throw new Exception('$idCliente  es invalido');
        }
    }

    public function cloneFacturaItem($idFactura){
        $aFacturaProds = ProdFactura::model()->findAllByAttributes(array('idComprobante' => $idFactura));
        // TODO: RECORRER Y CREAR
        $i = 0;
        foreach ($aFacturaProds as $key => $oFacturaProd) {
            $oProdNC = new ProdNotaCredito();
            $oProdNC->idComprobante = $this->id;
            $oProdNC->nroItem = $i;
            $oProdNC->cantidad = $oFacturaProd->cantidad;
            $oProdNC->descripcion = $oFacturaProd->descripcion;
            $oProdNC->idArtVenta = $oFacturaProd->idArtVenta;
            $oProdNC->precioUnitario = $oFacturaProd->precioUnitario;
            if (!$oProdNC->save()){
                Yii::log('Error al guardar Prod NC'.CHtml::errorSummary($oProdNC),'error');
            }

            $i++;
        }
    }



	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	/*
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

        //TODO: AGREGAR LA CONDICION PARA TIPO NC
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('clase',$this->clase,true);
		$criteria->compare('Nro_Puesto_Venta',$this->Nro_Puesto_Venta);
		$criteria->compare('Nro_Comprobante',$this->Nro_Comprobante);
		$criteria->compare('Iva_Responsable',$this->Iva_Responsable);
		$criteria->compare('Nro_Viajante',$this->Nro_Viajante);
		$criteria->compare('Id_Cliente',$this->Id_Cliente);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::jpicker2db($this->fecha));
        }

        if ($this->Vencim_CAE != null){
            $criteria->compare('Vencim_CAE',ComponentesComunes::jpicker2db($this->Vencim_CAE));
        }

		$criteria->compare('Id_Camion',$this->Id_Camion);
		$criteria->compare('Total_Final',$this->Total_Final);
		$criteria->compare('Enviada',$this->Enviada);
		$criteria->compare('Recibida',$this->Recibida);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('Otro_Iva',$this->Otro_Iva);
		$criteria->compare('Descuento1',$this->Descuento1);
		$criteria->compare('Descuento2',$this->Descuento2);
		$criteria->compare('Total_Neto',$this->Total_Neto);
		$criteria->compare('CAE',$this->CAE);



		$criteria->compare('Ing_Brutos',$this->Ing_Brutos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
		));
	}
	*/

	public function beforeSave(){
        /** @var  $oEstadoAnterior NotaCredito */
        $this->tipo = 'NC';

        $oEstadoAnterior = self::model()->findByPk($this->id);

        $bReturn = parent::beforeSave();
/*
        if($bReturn && $this->estado == NotaCredito::iCerrada && $oEstadoAnterior->estado < NotaCredito::iCerrada){
            Yii::log('SE GRABO, Y PASA DE UN ESTADO INFERIOR A CERRADA','warning');
            if($this->oFactura != null){
                Yii::log('HAY FACTURA '.$this->oFactura->id,'warning');
                $this->oFactura->montoPagado += $this->Total_Final;
                Yii::log($this->oFactura->montoPagado.' -- '.$this->Total_Final,'warning');
                if (!$this->oFactura->save()){
                    // TODO: DEBERIA TIRAR EXCEPCION
                    Yii::log(''.CHtml::errorSummary($this->oFactura),'warning');
                }
            }else{
                Yii::log('$this->oFactura == null','warning');
            }
        }else{
            //Yii::log('-- '.ComponentesComunes::print_array($this->attributes),'warning');
        }
*/

        return $bReturn;
    }

    public function beforeDelete(){
        /** @var  $oEstadoAnterior NotaCredito */

	    if($this->CAE != '00000000000000' && $this->CAE != '' || ($this->oVoucherAfip != null && $this->oVoucherAfip->caea != '00000000000000' && $this->oVoucherAfip->caea != '')){
            Yii::log('No se puede borrar una factura que esta aprobada '.print_r($this,true),'error');
            return false;
        }

        $transaction = Yii::app()->db->getCurrentTransaction();
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
        }

        $oEstadoAnterior = self::model()->findByPk($this->id);

        $bReturn = parent::beforeDelete();
        if ($bReturn){
            if($oEstadoAnterior->oFactura != null){
                $oFactura = Factura::model()->findByPk($oEstadoAnterior->oFactura->id);
                if($oFactura != null){
                    $oFactura->idNotaDeCredito = null;
                    $oFactura->save();
                }
            }
        }else{
            $transaction->rollback();
            return false;
        }

        $transaction->commit();
        return $bReturn;
    }

}