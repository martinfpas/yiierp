<?php

/**
 * This is the model class for table "carga".
 *
 * The followings are the available columns in table 'carga':
 * @property integer $id
 * @property integer $nro
 * @property string $fecha
 * @property integer $idVehiculo
 * @property integer $idCamionero
 * @property string $observacion
 * @property integer $estado
 * @property bool $recalcularDepuracion
 *
 * The followings are the available model relations:
 * @property Vehiculo $oCamion
 * @property Camionero $oCamionero
 * @property Depuracion[] $aDepuracion
 * @property EntregaDoc $oEntregaDoc
 * @property NotaPedido[] $aNotaPedido
 * @property ProdCarga[] $aProdCarga *
 */
class Carga extends CActiveRecord
{
    const iBorrador = 0;
    const iFacturada = 1;
    const iDespachada = 2;
    const iCerrada = 3;
    const iDescartada = 4;
    const iArchivada = 5;

    const borrador = 'Borrador';
    const facturada = 'Facturada';
    const despachada = 'Despachada';
    const archivada = 'Archivada';
    const descartada = 'Descartada';
    const cerrada = 'Cerrada';

    public static $aEstado = array(
        self::iBorrador => self::borrador,
        self::iFacturada => self::facturada,
        self::iDespachada => self::despachada,
        self::iCerrada => self::cerrada,
        self::iArchivada=> self::archivada,
        self::iDescartada => self::descartada,
    );

    public static $aEstadoNoBorrador = array(
        self::iFacturada => self::facturada,
        self::iDespachada => self::despachada,
        self::iCerrada => self::cerrada,
        self::iArchivada=> self::archivada,
        self::iDescartada => self::descartada,
    );

    public static $aEstadoNoDesp = array(
        self::iBorrador => self::borrador,
        self::iFacturada => self::facturada,
    );

    public static $aEstadoDesp = array(
        self::iDespachada => self::despachada,
        self::iCerrada => self::cerrada,
    );


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'carga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, idVehiculo', 'required'),
			array('idVehiculo,idCamionero, estado', 'numerical', 'integerOnly'=>true),
            array('recalcularDepuracion','boolean', 'allowEmpty' => false),
			array('observacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, fecha, idVehiculo, idCamionero, observacion, estado, recalcularDepuracion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oCamion' => array(self::BELONGS_TO, 'Vehiculo', 'idVehiculo'),
            'oCamionero' => array(self::BELONGS_TO, 'Camionero', 'idCamionero'),
            'aDepuracion' => array(self::HAS_MANY, 'Depuracion', 'idCarga'),
			'aProdCarga' => array(self::HAS_MANY, 'ProdCarga', 'idCarga'),
            'aNotaPedido' => array(self::HAS_MANY, 'NotaPedido', 'idCarga'),
            'oEntregaDoc' => array(self::HAS_ONE, 'EntregaDoc', 'idCarga'),

            'aCargaDocs'         => array(
                self::HAS_MANY,'CargaDocumentos',  array('id'=>'idEntregaDoc'),
                'through' => 'oEntregaDoc'),
            //'aCargaDocumentos' => array(self::HAS_MANY, 'CargaDocumentos', 'idEntregaDoc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Nro',
			'fecha' => 'Fecha',
			'idVehiculo' => 'Id Vehiculo',
			'observacion' => 'Observacion',
            'idCamionero' => 'Conductor',
            'estado' => Yii::t('application', 'Estado'),
            'recalcularDepuracion' => Yii::t('application', 'Recalcular Depuracion'),
		);
	}

	public function getPesoTotal(){
	    $fPesoTotal = 0;
        foreach ($this->aProdCarga as $index => $oProdCarga) {
            //$unidades = $oProdCarga->cantidad * $oProdCarga->cantXbulto;
            $unidades = $oProdCarga->cantidad;
            $oUnidadDeMedida = Udm::model()->findByPk($oProdCarga->oArtVenta->uMedida);
            $unidadesXkilo = 1;
            if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
                $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
            }
            $kilosXunidad = $oProdCarga->oArtVenta->contenido / $unidadesXkilo;

            $fPesoTotal += $kilosXunidad * $unidades;
	    }
	    return $fPesoTotal;
    }

    /* @var $oNotaPedido NotaPedido */

    public function getMontoTotal(){
        $fMontoTotal = 0;

        foreach ($this->aNotaPedido as $index => $oNotaPedido) {
            $fMontoTotal += $oNotaPedido->SubTotal;
        }
        return $fMontoTotal;
    }

    /* @var $oNotaPedido NotaPedido */

    public function getCostoMercaderia(){
        $fMontoTotal = 0;

        foreach ($this->aNotaPedido as $index => $oNotaPedido) {
            $fMontoTotal += $oNotaPedido->getCostoTotal();
        }
        return $fMontoTotal;
    }

    /* @var $oProdCarga ProdCarga*/
    public function descontarProd($idArtVenta,$cantidad,$idPaquete){
        foreach ($this->aProdCarga as $index => $oProdCarga) {
            if ($oProdCarga->idArtVenta == $idArtVenta && $oProdCarga->idPaquete == $idPaquete){
                if ($oProdCarga->cantidad == $cantidad){
                    Yii::log('Delete $oProdCarga->cantidad:'.$oProdCarga->cantidad.' > $cantidad '.$cantidad,'error');
                    $oProdCarga->delete();
                    return true;
                }else{
                    $oProdCarga->cantidad -= $cantidad;
                }

                if(!$oProdCarga->save()){
                    Yii::log(CHtml::errorSummary($oProdCarga),'error');
                    return false;
                }else{
                    return true;
                }

            }
        }
        return false;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

		$criteria->compare('idVehiculo',$this->idVehiculo);
        $criteria->compare('idCamionero',$this->idCamionero);
		$criteria->compare('observacion',$this->observacion,true);
        $criteria->compare('estado',$this->estado);
        $criteria->compare('recalcularDepuracion',$this->recalcularDepuracion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',

            ),
		));
	}

    public function searchNoDespachadas()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }


        $criteria->compare('idVehiculo',$this->idVehiculo);
        $criteria->compare('observacion',$this->observacion,true);

        if ($this->estado == null){
            $criteria->addCondition('(estado = '.Carga::iBorrador.' or estado = '.Carga::iFacturada.')');
        }else{
            if ($this->estado == Carga::iBorrador || $this->estado == Carga::iFacturada){

                $criteria->addCondition('estado = '.$this->estado);
            }
        }




        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',

            ),
        ));
    }

    public function beforeDelete(){
	    if ($this->estado <> Carga::iBorrador){
	        throw new Exception('Sólo se pueden borrar cargas en estado '.Carga::borrador);
        }

	    return parent::beforeDelete();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Carga the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
