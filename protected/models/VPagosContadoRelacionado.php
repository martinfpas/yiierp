<?php

/**
 * This is the model class for table "vPagosContadoRelacionado".
 *
 * The followings are the available columns in table 'vPagosContadoRelacionado':
 * @property integer $id
 * @property double $monto
 * @property integer $idPagodeCliente
 * @property integer $cid
 * @property integer $neid
 * @property string $idPagoRelacionado
 *
 * The followings are the available model relations:
 * @property PagoCliente $oPagodeCliente
 * @property NotaEntrega $oNotaEntrega
 * @property Factura $oFactura
 */
class VPagosContadoRelacionado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vPagosContadoRelacionado2';
	}

    public function primaryKey(){
        return 'id';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monto, idPagodeCliente', 'required'),
			array('id, idPagodeCliente, cid, neid', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('idPagoRelacionado', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monto, idPagodeCliente, cid, neid, idPagoRelacionado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oFactura' => array(self::BELONGS_TO, 'Factura', 'cid'),
            'oNotaEntrega' => array(self::BELONGS_TO, 'NotaEntrega', 'neid'),
            'oPagodeCliente' => array(self::BELONGS_TO, 'PagoCliente', 'idPagodeCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vpagos Contado Relacionado',
            'models' => 'Vpagos Contado Relacionados',
			'id' => Yii::t('application', 'ID'),
			'monto' => Yii::t('application', 'Monto'),
			'idPagodeCliente' => Yii::t('application', 'Id Pagode Cliente'),
			'cid' => Yii::t('application', 'Cid'),
			'neid' => Yii::t('application', 'Neid'),
			'idPagoRelacionado' => Yii::t('application', 'Id Pago Relacionado'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->with = array('oPagodeCliente');

		$criteria->compare('id',$this->id);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('idPagodeCliente',$this->idPagodeCliente);
		$criteria->compare('cid',$this->cid);
		$criteria->compare('neid',$this->neid);
		$criteria->compare('idPagoRelacionado',$this->idPagoRelacionado,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VPagosContadoRelacionado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
