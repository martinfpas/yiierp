<?php

/**
 * This is the model class for table "depuracion".
 *
 * The followings are the available columns in table 'depuracion':
 * @property integer $id
 * @property string $fecha
 * @property integer $idCarga
 *
 * The followings are the available model relations:
 * @property Carga $oCarga
 * @property ProdDepuracion[] $aProdDepuracion
 */
class Depuracion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Depuracion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'depuracion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, idCarga', 'required'),
			array('idCarga', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha, idCarga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCarga' => array(self::BELONGS_TO, 'Carga', 'idCarga'),
			'aProdDepuracion' => array(self::HAS_MANY, 'ProdDepuracion', 'idDepuracion'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
            'model' => Yii::t('application', 'Depuracion'),
            'models' => 'Depuraciones',
            'id' => Yii::t('application', 'ID'),
            'fecha' => Yii::t('application', 'Fecha'),
            'idCarga' => Yii::t('application', 'Id Carga'),
        );
	}

    public function getPesoTotal(){
        $fPesoTotal = 0;
        foreach ($this->aProdDepuracion as $index => $oProdDepuracion) {
            //$unidades = $oProdDepuracion->cantidad * $oProdDepuracion->cantXbulto;
            $unidades = $oProdDepuracion->cantidad;
            $oUnidadDeMedida = Udm::model()->findByPk($oProdDepuracion->oArtVenta->uMedida);
            $unidadesXkilo = 1;
            if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
                $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
            }
            $kilosXunidad = $oProdDepuracion->oArtVenta->contenido / $unidadesXkilo;

            $fPesoTotal += $kilosXunidad * $unidades;
        }
        return $fPesoTotal;
    }


    public function clonarProductos(){
        /* @var $oProdCarga ProdCarga*/

        $aProdCargaIDs = array();
        foreach ($this->oCarga->aProdCarga as $index => $oProdCarga) {
            $aProdCargaIDs[] = $oProdCarga->idPaquete;
            if($oProdCarga->cantidad > $oProdCarga->cantDepurada){
                $oProdDepuracion = new ProdDepuracion();
                $oProdDepuracion->idDepuracion = $this->id;
                $oProdDepuracion->idArtVenta = $oProdCarga->idArtVenta;
                $oProdDepuracion->idPaquete = $oProdCarga->idPaquete;
                $oProdDepuracion->idProdCarga = $oProdCarga->id;
                $oProdDepuracion->cantidad = ($oProdCarga->cantidad - $oProdCarga->cantDepurada);
                if(!$oProdDepuracion->save()){
                    Yii::log(CHtml::errorSummary($oProdDepuracion),'error');
                    throw new Exception(CHtml::errorSummary($oProdDepuracion));
                }
                $oProdCarga->cantDepurada += $oProdDepuracion->cantidad;

                if(!$oProdCarga->save()){
                    throw new Exception(CHtml::errorSummary($oProdCarga));
                }
            }else{ // RECALCULAR
                if($oProdCarga->cantidad < $oProdCarga->cantDepurada){
                    $oProdDepuracion = new ProdDepuracion();
                    $oProdDepuracion->idDepuracion = $this->id;
                    $oProdDepuracion->idArtVenta = $oProdCarga->idArtVenta;
                    $oProdDepuracion->idPaquete = $oProdCarga->idPaquete;
                    $oProdDepuracion->idProdCarga = $oProdCarga->id;
                    $oProdDepuracion->cantidad = ($oProdCarga->cantidad - $oProdCarga->cantDepurada);

                    if(!$oProdDepuracion->save()){
                        Yii::log(CHtml::errorSummary($oProdDepuracion),'error');
                        throw new Exception(CHtml::errorSummary($oProdDepuracion));
                    }
                    $oProdCarga->cantDepurada = $oProdCarga->cantidad;

                    if(!$oProdCarga->save()){
                        throw new Exception(CHtml::errorSummary($oProdCarga));
                    }
                }
            }
        }
        if($this->oCarga->recalcularDepuracion){
            $criteria = new CDbCriteria();
            $criteria->addNotInCondition('idPaquete',$aProdCargaIDs);
            $criteria->addCondition('idCarga='.$this->oCarga->id);
            $aProdTotalDepuraciones = VTotalesProdDepuraciones::model()->findAll($criteria);
            foreach ($aProdTotalDepuraciones as $index => $oProdTotalDepuraciones) {
                $oProdDepuracion = new ProdDepuracion();
                $oProdDepuracion->idDepuracion = $this->id;
                $oProdDepuracion->idArtVenta = $oProdTotalDepuraciones->idArtVenta;
                $oProdDepuracion->idPaquete = $oProdTotalDepuraciones->idPaquete;
                $oProdDepuracion->idProdCarga = null;
                $oProdDepuracion->cantidad = (-1) * abs($oProdTotalDepuraciones->cantidad);

                if(!$oProdDepuracion->save()){
                    Yii::log(CHtml::errorSummary($oProdDepuracion),'error');
                    throw new Exception(CHtml::errorSummary($oProdDepuracion));
                }
            }
            $this->oCarga->recalcularDepuracion = false;
            if (!$this->oCarga->save()){
                Yii::log(CHtml::errorSummary($this->oCarga),'error');
                throw new Exception(CHtml::errorSummary($this->oCarga));
            }
        }
    }

    public function afterSave(){
        $transaction=Yii::app()->db->beginTransaction();
	    $bReturn = parent::beforeSave();
	    // SOLO CLONA SI ES NUEVO
        if($bReturn ){

            try{
                Yii::log('$this id :'.$this->id,'warning');
                $this->clonarProductos();

            }catch (Exception $e){
                $transaction->rollback();
                return false;
            }
            $transaction->commit();
        }else{
            $transaction->rollback();
        }
        return $bReturn;
    }

    public  function afterDelete(){
        $oDepuracion = $this;

        Yii::log(ComponentesComunes::print_array($oDepuracion->attributes,true),'warning');
        if(parent::afterDelete()){
            foreach ($oDepuracion->oCarga->aProdCarga as $index => $oProdCarga) {
                $oProdDepurado = VTotalesProdDepuraciones::model()->findByAttributes(array('idPaquete' => $oProdCarga->idPaquete,'idCarga'=> $oProdCarga->idCarga));
                $cantDepurada = 0;
                if ($oProdDepurado != null){
                    $cantDepurada = $oProdDepurado->cantidad;
                }
                $oProdCarga->cantidad = $cantDepurada;
                if (!$oProdCarga->save()){
                    Yii::log('Error: '.CHtml::errorSummary($oProdCarga),'error');
                }
            }
        }
    }

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		//$criteria->with = array('oArtVenta');

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('idCarga',$this->idCarga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=> array(
                //'defaultOrder' => 'oArtVenta.descripcion asc',
                'attributes' => array(
                    'idArtVenta' => array(
                        'asc' => 'oArtVenta.descripcion asc',
                        'desc' => 'oArtVenta.descripcion desc',
                    )
                )
            ),
		));
	}
}