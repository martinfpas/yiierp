<?php

/**
 * This is the model class for table "pagoProvCheque".
 *
 * The followings are the available columns in table 'pagoProvCheque':
 * @property integer $id
 * @property integer $idCheque3ro
 * @property double $monto
 * @property integer $idPagoProv
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros $oCheque3ro
 * @property PagoProveedor $oPagoProv
 */
class PagoProvCheque extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoProvCheque';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCheque3ro, idPagoProv', 'required'),
			array('idCheque3ro, idPagoProv', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCheque3ro, monto, idPagoProv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCheque3ro' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'idCheque3ro'),
			'oPagoProv' => array(self::BELONGS_TO, 'PagoProveedor', 'idPagoProv'),
		);
	}

	public function beforeSave(){
	    /** @var $oEstadoAnterior PagoProvCheque */
        $oEstadoAnterior = null;
	    if ($this->id > 0){
            $oEstadoAnterior = self::model()->findByPk($this->id);
        }
	    $bReturn = parent::beforeSave();

	    /*
	    if ($bReturn && $oEstadoAnterior == null){
	        try{
                $this->oCheque3ro->estado = ChequeDeTerceros::iPagoTercero;
                if (!$this->oCheque3ro->save()){
                    throw new Exception(CHtml::errorSummary($this->oCheque3ro));
                }
            }catch (Exception $exception){

            }
        }
	    */
	    return $bReturn;
    }

    public function beforeDelete(){
        /** @var $oEstadoAnterior PagoProvCheque */

        $oEstadoAnterior = self::model()->findByPk($this->id);
        $bReturn = parent::beforeDelete();
        if ($bReturn && $oEstadoAnterior != null){
            $oEstadoAnterior->oCheque3ro->estado = ChequeDeTerceros::iEnCartera;
            if (!$oEstadoAnterior->oCheque3ro->save()){
                throw new Exception(CHtml::errorSummary($oEstadoAnterior->oCheque3ro));
            }
        }

        return $bReturn;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago Prov Cheque',
            'models' => 'Pago Prov Cheques',
			'id' => Yii::t('application', 'ID'),
			'idCheque3ro' => Yii::t('application', 'Id Cheque3ro'),
			'monto' => Yii::t('application', 'Monto'),
			'idPagoProv' => Yii::t('application', 'Id Pago Prov'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idCheque3ro',$this->idCheque3ro);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('idPagoProv',$this->idPagoProv);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoProvCheque the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
