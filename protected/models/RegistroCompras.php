<?php

/**
 * This is the model class for table "registroCompras".
 *
 * The followings are the available columns in table 'registroCompras':
 * @property integer $id
 * @property string $FECHA
 * @property string $NUMERO
 * @property string $DOC
 * @property string $Proveedor
 * @property string $CUIT
 * @property string $NETOGRAV
 * @property string $NOGRAV
 * @property string $IVA
 * @property string $IIBB
 * @property string $PREC
 * @property string $TOTAL
 * @property integer $id_mes_compra
 *
 * The followings are the available model relations:
 * @property MesesCargaCompra $oMesCompra
 */
class RegistroCompras extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'registroCompras';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('FECHA, NUMERO, DOC, Proveedor, IVA, IIBB, PREC, TOTAL, id_mes_compra', 'required'),
			array('id_mes_compra', 'numerical', 'integerOnly'=>true),
			array('FECHA, DOC, NETOGRAV, NOGRAV, IVA, IIBB, PREC, TOTAL', 'length', 'max'=>10),
			array('NUMERO', 'length', 'max'=>40),
			array('Proveedor', 'length', 'max'=>25),
			array('CUIT', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, FECHA, NUMERO, DOC, Proveedor, CUIT, NETOGRAV, NOGRAV, IVA, IIBB, PREC, TOTAL, id_mes_compra', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oMesCompra' => array(self::BELONGS_TO, 'MesesCargaCompra', 'id_mes_compra'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Registro Compras',
            'models' => 'Registro Comprases',
			'id' => Yii::t('application', 'ID'),
			'FECHA' => Yii::t('application', 'Fecha'),
			'NUMERO' => Yii::t('application', 'Numero'),
			'DOC' => Yii::t('application', 'Doc'),
			'Proveedor' => Yii::t('application', 'Proveedor'),
			'CUIT' => Yii::t('application', 'Cuit'),
			'NETOGRAV' => Yii::t('application', 'Netograv'),
			'NOGRAV' => Yii::t('application', 'Nograv'),
			'IVA' => Yii::t('application', 'Iva'),
			'IIBB' => Yii::t('application', 'Iibb'),
			'PREC' => Yii::t('application', 'Prec'),
			'TOTAL' => Yii::t('application', 'Total'),
			'id_mes_compra' => Yii::t('application', 'Id Mes Compra'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('FECHA',$this->FECHA,true);
		$criteria->compare('NUMERO',$this->NUMERO,true);
		$criteria->compare('DOC',$this->DOC,true);
		$criteria->compare('Proveedor',$this->Proveedor,true);
		$criteria->compare('CUIT',$this->CUIT,true);
		$criteria->compare('NETOGRAV',$this->NETOGRAV,true);
		$criteria->compare('NOGRAV',$this->NOGRAV,true);
		$criteria->compare('IVA',$this->IVA,true);
		$criteria->compare('IIBB',$this->IIBB,true);
		$criteria->compare('PREC',$this->PREC,true);
		$criteria->compare('TOTAL',$this->TOTAL,true);
		$criteria->compare('id_mes_compra',$this->id_mes_compra);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RegistroCompras the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
