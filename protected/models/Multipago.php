<?php

/**
 * This is the model class for table "pagoCliente".
 *
 * The followings are the available columns in table 'pagoCliente':
 * @property integer $id
 * @property double $monto
 * @property integer $id_factura
 * @property integer $idNotaEntrega
 * @property string $fecha
 * @property integer $idCliente
 * @property integer $multipago
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property PagoAlContado[] $aPagoAlContado
 * @property Comprobante[] $aComprobantes
 * @property NotaEntrega[] $aNotasEntrega
 * @property PagoConCheque[] $PagoConCheque
 * @property VPagosDeEntregaDoc[] $aPagosDeEntregaDoc
 */

class Multipago extends PagoCliente{

    public $total = 0;
    public $fTotalCheques = 0;
    public $fTotalEfectivo = 0;
    public $fTotalNc = 0;
    public $idPagoRel = 0;
    public $aNcSinPR = array();
    public $aIdCargaDocumentos = array();
    public $devolucion = 0;
    public $saldo = 0;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Factura the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            //'aPagoAlContado' => array(self::HAS_MANY, 'PagoAlContado', 'idPagodeCliente'),
            'aPagoAlContado' => array(self::HAS_MANY, 'VPagosContadoRelacionado', 'idPagodeCliente'),
            'aComprobantes'  => array(self::HAS_MANY, 'Factura', 'idPagoRelacionado'),
            'aNotasEntrega'  => array(self::HAS_MANY, 'NotaEntrega', 'idPagoRelacionado'),
            'aPagoConCheque' => array(self::HAS_MANY, 'PagoConCheque', 'idPagodeCliente'),
            'aPagosDeEntregaDoc'  => array(self::HAS_MANY, 'VPagosDeEntregaDoc', 'idPago'),
        );
    }

    public static function create($aCargaDocumentos){
        $oMultipago = new Multipago();

        $oMultipago->total = 0;
        $oMultipago->fTotalCheques = 0;
        $oMultipago->fTotalEfectivo = 0;
        $oMultipago->fTotalNc = 0;
        $idPagoRel = 0;
        $oMultipago->aNcSinPR = array();
        $i = 0;
        foreach ($aCargaDocumentos as $index => $oCargaDocumento) {
            $oDocumento = $oCargaDocumento->getDocumento();

            if ($idPagoRel != 0) { // YA HAY UN PAGO RELACIONADO
                //Yii::log('$i:::'.$idPagoRel,'warning');
                if ($oDocumento->idPagoRelacionado == null){
                    $oDocumento->idPagoRelacionado = $idPagoRel;
                    if (!$oDocumento->save()){
                        Yii::log('$oDocumento->idPagoRelacionado ::'.CHtml::errorSummary($oDocumento),'warning');
                        throw new CHttpException(CHtml::errorSummary($oDocumento));
                    }else{
                        //TODO: VER SI HACEMOS ALGO ACA
                    }
                }
            }else{ // SE VA A CREAR UN PAGO RELACIONADO
                if ($oCargaDocumento->tipoDoc == CargaDocumentos::NC) { // (3)
                    if ($oDocumento->idPagoRelacionado != null) { // (5)
                        $idPagoRel = $oDocumento->idPagoRelacionado;
                    }else{ // (6)
                        $oMultipago->aNcSinPR[] = $oCargaDocumento;
                    }
                }else{ //(4)
                    if ($oDocumento->idPagoRelacionado != null) { // (7)
                        if ($idPagoRel == 0){
                            $idPagoRel = $oDocumento->idPagoRelacionado;
                        }else{
                            // TODO: VER QUE HACER, PORQUE NO DEBERIA PASAR
                        }
                    }else{ // (8)
                        $oPago = $oCargaDocumento->getOPagoCliente();
                        if ($oPago == null){ // SI NO TIENE PAGO ASIGNADO
                            $oPago = PagoCliente::newBlank($oCargaDocumento->getCliente()->id);
                            if ($oCargaDocumento->tipoDoc == CargaDocumentos::NE) {
                                $oPago->idNotaEntrega = $oDocumento->id;
                            }else{
                                if ($oCargaDocumento->tipoDoc == CargaDocumentos::FAC) {
                                    $oPago->id_factura = $oDocumento->id;
                                }else if ($oCargaDocumento->tipoDoc == CargaDocumentos::ND) {
                                    $oPago->id_factura = $oDocumento->id;
                                }
                            }
                            if (!$oPago->save()){
                                throw new CHttpException(CHtml::errorSummary($oPago));
                            }
                        }
                        if ($idPagoRel == 0){
                            $idPagoRel = $oPago->id;
                            $oDocumento->idPagoRelacionado = $oPago->id;
                        }else{
                            $oDocumento->idPagoRelacionado = $idPagoRel;
                        }
                        if (!$oDocumento->save()){
                            throw new CHttpException(CHtml::errorSummary($oDocumento));
                        }
                    }
                }
            }

            // SE CALCULAN LOS TOTALES PARA PASAR A LA VISTA
            if ($oCargaDocumento->tipoDoc != CargaDocumentos::NC){
                $oMultipago->total += $oCargaDocumento->totalFinal;
                $oMultipago->fTotalCheques += $oCargaDocumento->cheque;
                $oMultipago->fTotalEfectivo += $oCargaDocumento->efectivo;
                //$TotalEfectivo
            }else{
                $oMultipago->total = $oMultipago->total + $oCargaDocumento->totalFinal;
                $oMultipago->fTotalNc += $oCargaDocumento->totalFinal;
            }
            $i++;
        }

        $oMultipago->id = $idPagoRel;

        return $oMultipago;
    }

    public function sumPago(){
        $sum = 0;
        foreach ($this->aComprobantes as $index => $aComprobante) {
            if ($aComprobante->oPagoCliente != null){
                $monto = number_format($aComprobante->oPagoCliente->monto,2,'.','');
                //Yii::log('Monto1: '.$monto.' id '.$aComprobante->id,'warning');
                $sum += number_format($aComprobante->oPagoCliente->monto,2,'.','');
            }
        }

        foreach ($this->aNotasEntrega as $index => $aComprobante) {
            if ($aComprobante->oPagoCliente != null){
                //ComponentesComunes::print_array($aComprobante->attributes);
                $monto = number_format($aComprobante->oPagoCliente->monto,2,'.','');
                //Yii::log('Monto 2:: '.$monto.' id '.$aComprobante->id,'warning');
                $sum += number_format($aComprobante->oPagoCliente->monto,2,'.','');
            }
        }
        return $sum;
    }
    
    public function getAttributes($names = true)
    {
        $arrAttributes = parent::getAttributes();

        $arrAttributes['total'] = $this->total ;
        $arrAttributes['fTotalCheques'] = $this->fTotalCheques ;
        $arrAttributes['fTotalEfectivo'] = $this->fTotalEfectivo ;
        $arrAttributes['fTotalNc'] = $this->fTotalNc ;
        $arrAttributes['idPagoRel'] = $this->idPagoRel ;
        $arrAttributes['aNcSinPR'] = $this->aNcSinPR ;
        $arrAttributes['aIdCargaDocumentos'] = $this->aIdCargaDocumentos ;
        $arrAttributes['devolucion'] = $this->devolucion ;
        $arrAttributes['saldo'] = $this->saldo ;

        return $arrAttributes;
    }

    public function afterFind()

    {

        foreach ($this->aPagosDeEntregaDoc as $index => $oPagosDeEntregaDoc) {

            //Yii::log('Encontre $oPagosDeEntregaDoc idCargaDoc: '.$oPagosDeEntregaDoc->idCargaDoc.' monto:'.$oPagosDeEntregaDoc->monto,'warning');
            array_push($this->aIdCargaDocumentos,$oPagosDeEntregaDoc->idCargaDoc);

            $oCargaDocumento = $oPagosDeEntregaDoc->oCargaDoc;
            // SE CALCULAN LOS TOTALES PARA PASAR A LA VISTA
            if ($oCargaDocumento->tipoDoc != CargaDocumentos::NC){
                $this->total += $oCargaDocumento->totalFinal;
                $this->fTotalCheques += $oCargaDocumento->cheque;
                $this->fTotalEfectivo += $oCargaDocumento->efectivo;
                //$TotalEfectivo
            }else{
                $this->total = $this->total + $oCargaDocumento->totalFinal;
                $this->fTotalNc += $oCargaDocumento->totalFinal;
            }
        }



        foreach ($this->aIdCargaDocumentos as $index => $idCargaDocumentos) {
            $oCargaDocumento = CargaDocumentos::model()->findByPk($idCargaDocumentos);
            $this->devolucion += $oCargaDocumento->devolucion;
        }

        $sumPago = $this->sumPago();
        $this->saldo = number_format(($this->total - $sumPago - $this->devolucion),2,',','.');


        parent::afterFind();

    }

    //public function $total


}