<?php

/**
 * This is the model class for table "movimientoCuentasBanc".
 *
 * The followings are the available columns in table 'movimientoCuentasBanc':
 * @property integer $id
 * @property integer $idCuenta
 * @property double $Importe
 * @property integer $tipoMovimiento
 * @property integer $codigoMovimiento
 * @property integer $idMedio
 * @property string $fechaMovimiento
 * @property string $Nro_operacion
 * @property double $saldo_acumulado
 * @property string $Observacion
 *
 * The followings are the available model relations:
 * @property Cheque3RoMovBancario[] $aCheque3RoMovBancario
 * @property ChequePropioMovBancario[] $aChequePropioMovBancario
 * @property CuentasBancarias $oCuenta
 * @property MedioMovimiento $oMedio
 * @property ClasifMovimientoCuentas $oTipoMovimiento
 */
class MovimientoCuentasBanc extends CActiveRecord
{
    public $idsCheques3Ro ;
    public $idsChequesPpios ;
    public $ingreso;
    public $fechaDesde = null;
    public $fechaHasta = null;
    public $bRecalcular = false;
    public $fechaRecalcular = null;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movimientoCuentasBanc';
	}

    public function scopes(){
        return array(
            'activas' => array(
                'with' => 'oCuenta',
                'condition' => 'oCuenta.fechaCierre IS NULL ',
            ),
        );
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCuenta, Importe, tipoMovimiento, idMedio, fechaMovimiento', 'required'),
			array('idCuenta, tipoMovimiento, codigoMovimiento, idMedio', 'numerical', 'integerOnly'=>true),
			array('saldo_acumulado,Importe', 'numerical'),
			array('Nro_operacion', 'length', 'max'=>9),
			array('saldo_acumulado,Observacion,idsCheques3Ro,idsChequesPpios,bRecalcular', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCuenta, Importe, tipoMovimiento, codigoMovimiento, idMedio, fechaMovimiento, Nro_operacion, saldo_acumulado, Observacion, ingreso,fechaDesde,fechaHasta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'aCheque3RoMovBancario' => array(self::HAS_MANY, 'Cheque3RoMovBancario', 'idMovimiento'),
            'aChequePropioMovBancario' => array(self::HAS_MANY, 'ChequePropioMovBancario', 'idMovimiento'),
			'oCuenta' => array(self::BELONGS_TO, 'CuentasBancarias', 'idCuenta'),
			'oMedio' => array(self::BELONGS_TO, 'MedioMovimiento', 'idMedio'),
			'oTipoMovimiento' => array(self::BELONGS_TO, 'ClasifMovimientoCuentas', 'tipoMovimiento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' =>  'Movimiento Cta.Banc.',
            'models' => 'Movimientos Cta.Banc.',
			'id' => Yii::t('application', 'ID'),
			'idCuenta' => Yii::t('application', 'Cuenta'),
			'Importe' => Yii::t('application', 'Importe'),
			'tipoMovimiento' => Yii::t('application', 'Tipo Movimiento'),
			'codigoMovimiento' => Yii::t('application', 'Codigo Movimiento'),
			'idMedio' => Yii::t('application', 'Medio'),
			'fechaMovimiento' => Yii::t('application', 'Fecha Movimiento'),
			'Nro_operacion' => Yii::t('application', 'Nro Operacion'),
			'Observacion' => Yii::t('application', 'Observacion'),
            'idsCheques3Ro' => Yii::t('application','Cheques De Terceros'),
            'idsChequesPpios' => Yii::t('application','Cheques Propios'),
            'ingreso' => Yii::t('application','Ingreso'),
		);
	}

	public function recalcular($save = true){
	    /* @var $oChequePropioMovBancario ChequePropioMovBancario */
        /* @var $oChequePropioMovBancario Cheque3RoMovBancario */
	    $importe = 0;
        foreach ($this->aChequePropioMovBancario as $index => $oChequePropioMovBancario) {
            $importe += $oChequePropioMovBancario->oChequePropio->importe;
	    }
        foreach ($this->aCheque3RoMovBancario as $index => $oCheque3RoMovBancario) {
            $importe += $oCheque3RoMovBancario->oCheque3ro->importe;
        }

        $this->Importe = $importe;
        if ($save){
            if(!$this->save()){
                throw new Exception(CHtml::errorSummary($this));
            }
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oCuenta','oTipoMovimiento');

		$criteria->compare('id',$this->id);
		$criteria->compare('idCuenta',$this->idCuenta);
		$criteria->compare('Importe',$this->Importe);
		$criteria->compare('tipoMovimiento',$this->tipoMovimiento);
		$criteria->compare('codigoMovimiento',$this->codigoMovimiento);
		$criteria->compare('idMedio',$this->idMedio);
		if ($this->fechaMovimiento != ''){
            $criteria->compare('fechaMovimiento',ComponentesComunes::fechaComparada($this->fechaMovimiento));
        }

        if($this->ingreso != null){
            $criteria->compare('oTipoMovimiento.Ingreso',$this->ingreso);
        }

		$criteria->compare('Nro_operacion',$this->Nro_operacion,true);
		$criteria->compare('Observacion',$this->Observacion,true);

		$criteria->addCondition('oCuenta.fechaCierre IS NULL');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'fechaMovimiento desc',
                'attributes' => array(
                    '*',
                    't.ingreso' => array(
                        'asc' => 'oTipoMovimiento.Ingreso asc',
                        'desc' => 'oTipoMovimiento.Ingreso desc',
                    ),

                ),
            )
		));
	}
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oCuenta','oTipoMovimiento');

        $criteria->compare('id',$this->id);
        $criteria->compare('idCuenta',$this->idCuenta);
        $criteria->compare('Importe',$this->Importe);
        $criteria->compare('tipoMovimiento',$this->tipoMovimiento);
        $criteria->compare('codigoMovimiento',$this->codigoMovimiento);
        $criteria->compare('idMedio',$this->idMedio);
        if ($this->fechaMovimiento != ''){
            $criteria->compare('fechaMovimiento',ComponentesComunes::fechaComparada($this->fechaMovimiento));
        }

        if ($this->fechaDesde != null) {
            $criteria->addCondition("t.fechaMovimiento >= '".ComponentesComunes::jpicker2db($this->fechaDesde)." 00:00:00'");
        }

        if ($this->fechaHasta != null) {
            $criteria->addCondition("t.fechaMovimiento <= '".ComponentesComunes::jpicker2db($this->fechaHasta,false)."  23:59:59'");
        }

        if($this->ingreso != null){
            $criteria->compare('oTipoMovimiento.Ingreso',$this->ingreso);
        }

        $criteria->compare('Nro_operacion',$this->Nro_operacion,true);
        $criteria->compare('Observacion',$this->Observacion,true);

        $criteria->addCondition('oCuenta.fechaCierre IS NULL');

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'fechaMovimiento desc,t.id desc',
                'attributes' => array(
                    '*',
                    't.ingreso' => array(
                        'asc' => 'oTipoMovimiento.Ingreso asc',
                        'desc' => 'oTipoMovimiento.Ingreso desc',
                    ),

                ),
            )
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWPReport()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oCuenta','oTipoMovimiento');

        $criteria->compare('id',$this->id);
        $criteria->compare('idCuenta',$this->idCuenta);
        $criteria->compare('Importe',$this->Importe);
        $criteria->compare('tipoMovimiento',$this->tipoMovimiento);
        $criteria->compare('codigoMovimiento',$this->codigoMovimiento);
        $criteria->compare('idMedio',$this->idMedio);
        if ($this->fechaMovimiento != ''){
            $criteria->compare('fechaMovimiento',ComponentesComunes::fechaComparada($this->fechaMovimiento));
        }

        if ($this->fechaDesde != null) {
            $criteria->addCondition("t.fechaMovimiento >= '".ComponentesComunes::jpicker2db($this->fechaDesde)." 00:00:00'");
        }

        if ($this->fechaHasta != null) {
            $criteria->addCondition("t.fechaMovimiento <= '".ComponentesComunes::jpicker2db($this->fechaHasta,false)."  23:59:59'");
        }

        if($this->ingreso != null){
            $criteria->compare('oTipoMovimiento.Ingreso',$this->ingreso);
        }

        $criteria->compare('Nro_operacion',$this->Nro_operacion,true);
        $criteria->compare('Observacion',$this->Observacion,true);

        $criteria->addCondition('oCuenta.fechaCierre IS NULL');

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'fechaMovimiento asc,t.id asc',
                'attributes' => array(
                    '*',
                    't.ingreso' => array(
                        'asc' => 'oTipoMovimiento.Ingreso asc',
                        'desc' => 'oTipoMovimiento.Ingreso desc',
                    ),

                ),
            )
        ));
    }

	public function afterDelete(){
        $oCuenta = $this->oCuenta;
        $importe = $this->Importe;
        $oTipoMov = $this->oTipoMovimiento;
        $transaction = Yii::app()->db->beginTransaction();
        $bparentAfterDelete= parent::afterDelete();
        //if ($bparentAfterDelete){
        if (true){
            if ($oTipoMov->Ingreso){
                $oCuenta->saldo += $importe;
            }else{
                $oCuenta->saldo -= $importe;
            }
            $this->oCuenta->fechaSaldo = date('Y-m-d');
            if(!$oCuenta->save()){
                Yii::log('$this->oCuenta::'.CHtml::errorSummary($oCuenta),'error');
                $transaction->rollback();
                throw new CHttpException('500',CHtml::errorSummary($this->oCuenta));
            }else{
                Yii::log('$oCuenta->saldo ::'.$oCuenta->saldo,'error');
            }
        }else{
            Yii::log('elseee ::','error');
        }

        $transaction->commit();
        $old = self::model()->findByPk($this->id);
        $this->fechaRecalcular = ($old == null || $this->fechaMovimiento < $old->fechaMovimiento)?  $this->fechaMovimiento : $old->fechaMovimiento;
        $this->oCuenta->recalcularSaldo($this->fechaRecalcular);

	    return $bparentAfterDelete;
    }

    public function afterSave(){
        //Yii::log('id: '.$this->id.' bRecalcular::'.$this->bRecalcular.'<'.$this->saldo_acumulado.'<','error');
        if ($this->bRecalcular){
            $this->oCuenta->recalcularSaldo($this->fechaRecalcular);
        }
        return parent::afterSave();
    }
	public function beforeSave(){

	    $old = null;
        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null){ // SOLO EMPIEZO UNA TRANSACCION SI NO HAY UNA
            $transaction=Yii::app()->db->beginTransaction();
        }else{
            $shouldCommit = false;
        }
        $montoViejo = 0;
        //$this->getScenario() == 'update'
        if($this->id > 0){
            $old = self::model()->findByPk($this->id);
            if ($old != null){
                $montoViejo = $old->Importe;
            }else{
                if ($shouldCommit){ // SOLO HAGO COMMIT SI SE INICIO LA TRANSACCION ACA
                    $transaction->commit();
                }
                return parent::beforeSave();
                //$transaction->rollback();
                //throw new Exception('No se encuentra el movimiento anterior');
            }
        }

        $cambioImporte = $montoViejo <> $this->Importe;
        $recalcular = false;
        $bRecalcular = (
            ($old != null && $old->saldo_acumulado == $this->saldo_acumulado && ($cambioImporte || $this->fechaMovimiento != $old->fechaMovimiento))
            ||
            ($old == null && $this->fechaMovimiento != date('Y-m-d'))
        );

        if($cambioImporte && !$bRecalcular){
            //Yii::log('valor de recalcular:: '.$this->id.' >'.$this->saldo_acumulado.'<',CLogger::LEVEL_ERROR);
            if($this->oTipoMovimiento->Ingreso){
                $this->saldo_acumulado = $this->oCuenta->saldo + $this->Importe - $montoViejo;
            }else{
                $this->saldo_acumulado = $this->oCuenta->saldo - $this->Importe + $montoViejo;
            }
        }

        $bParentSave = parent::beforeSave();
        if ($bParentSave){
            $this->oCuenta->saldo = $this->saldo_acumulado;
            $this->oCuenta->fechaSaldo = $this->fechaMovimiento;
            if (!$this->oCuenta->save()) {
                Yii::log('$this->oCuenta::' . CHtml::errorSummary($this->oCuenta), 'error');
                $transaction->rollback();
                throw new CHttpException('500', CHtml::errorSummary($this->oCuenta));
            }
        }
        if ($shouldCommit){ // SOLO HAGO COMMIT SI SE INICIO LA TRANSACCION ACA
            $transaction->commit();
        }

        if ($bParentSave){
            //Yii::log('id: '.$this->id.' old::'.(($old != null)? $old->saldo_acumulado : 'no old ').' :: '.$this->saldo_acumulado.'< $this->recalcular::'.$bRecalcular.'< >'.$this->saldo_acumulado,'error');
            $this->bRecalcular = $bRecalcular;
            $this->fechaRecalcular = ($old == null || $this->fechaMovimiento < $old->fechaMovimiento)?  $this->fechaMovimiento : $old->fechaMovimiento;
        }
	    return $bParentSave;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MovimientoCuentasBanc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
