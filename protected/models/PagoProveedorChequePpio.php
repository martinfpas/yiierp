<?php

/**
 * This is the model class for table "pagoProveedorChequePpio".
 *
 * The followings are the available columns in table 'pagoProveedorChequePpio':
 * @property integer $id
 * @property integer $idChequePpio
 * @property double $monto
 * @property integer $idPagoProv
 *
 * The followings are the available model relations:
 * @property ChequePropio $oChequePpio
 * @property PagoProveedor $oPagoProv
 */
class PagoProveedorChequePpio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoProveedorChequePpio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idChequePpio, idPagoProv', 'required'),
			array('idChequePpio, idPagoProv', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idChequePpio, monto, idPagoProv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oChequePpio' => array(self::BELONGS_TO, 'ChequePropio', 'idChequePpio'),
			'oPagoProv' => array(self::BELONGS_TO, 'PagoProveedor', 'idPagoProv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago Proveedor Cheque Ppio',
            'models' => 'Pago Proveedor Cheque Ppios',
			'id' => Yii::t('application', 'ID'),
			'idChequePpio' => Yii::t('application', 'Id Cheque Ppio'),
			'monto' => Yii::t('application', 'Monto'),
			'idPagoProv' => Yii::t('application', 'Id Pago Prov'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idChequePpio',$this->idChequePpio);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('idPagoProv',$this->idPagoProv);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoProveedorChequePpio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
