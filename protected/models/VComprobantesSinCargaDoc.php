<?php

/**
 * This is the model class for table "vComprobantesSinCargaDoc".
 *
 * The followings are the available columns in table 'vComprobantesSinCargaDoc':
 * @property integer $id
 * @property string  $tipo
 * @property integer $idCamion
 * @property integer $idComprobante
 * @property integer $idCliente
 */
class VComprobantesSinCargaDoc extends CActiveRecord
{
    public $Cliente;
    public $Fecha;
    public $Nro;

    const NE = 'NE';
    const FAC = 'F';
    const NC = 'NC';
    const ND = 'ND';

    public static $aTipoDoc = array(
        self::NE => self::NE,
        self::FAC => self::FAC,
        self::NC => self::NC,
        self::ND => self::ND,
    );

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return VComprobantesSinCargaDoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function primaryKey(){

        return 'id';

    }

    public function defaultScope()
    {
        return array(
            'condition' => 'idComprobante IS NULL ',
        );

    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vComprobantesSinCargaDoc';
	}

	public static function ALIAS(){
        return 'vComprobantesSinCargaDoc';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idCamion', 'numerical', 'integerOnly'=>true),
			array('tipo', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tipo, idCamion, idCliente, Fecha, Nro', 'safe', 'on'=>'search'),
		);
	}



	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oNotaEntrega'  => array(self::HAS_ONE,'NotaEntrega','id'),
            'oComprobante'  => array(self::HAS_ONE,'Comprobante','id'),
		    'oCamion'  => array(self::BELONGS_TO,'Vehiculo','idCamion'),
            'oCliente'      => array(self::BELONGS_TO,'Cliente','idCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'tipo' => Yii::t('application', 'Tipo'),
		            'idCamion' => Yii::t('application', 'Id Camion'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		//$criteria->with = array('oNotaEntrega','oComprobante','oNotaEntrega.oCliente','oComprobante.oCliente');
        $criteria->with = array('oNotaEntrega','oComprobante');


		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.tipo',$this->tipo);
		$criteria->compare('t.idCamion',$this->idCamion);

        if ($this->Fecha != null){
            $criteria->addCondition('oNotaEntrega.fecha like "%'.ComponentesComunes::fechaComparada($this->Fecha).'%" or '.'oComprobante.fecha like "%'.ComponentesComunes::fechaComparada($this->Fecha).'%"');
        }

        $criteria->compare('t.idCliente',$this->idCliente);

		//Cliente, Fecha, Nro
        if ($this->Nro != ''){
            $criteria->addCondition('oNotaEntrega.nroComprobante like "%'.$this->Nro.'%" or '.'oComprobante.Nro_Comprobante like "%'.$this->Nro.'%"');
        }




		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
				'pageSize'=>20,
			),
		));
	}
}