<?php

/**
 * This is the model class for table "pagoProvEfectivo".
 *
 * The followings are the available columns in table 'pagoProvEfectivo':
 * @property integer $id
 * @property integer $idPagoProveedor
 * @property double $monto
 *
 * The followings are the available model relations:
 * @property PagoProveedor $oPagoProveedor
 *
 */
class PagoProvEfectivo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoProvEfectivo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idPagoProveedor', 'required'),
			array('idPagoProveedor', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idPagoProveedor, monto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
        return array(
            'oPagoProveedor' => array(self::BELONGS_TO, 'PagoProveedor', 'idPagoProveedor'),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago Prov Efectivo',
            'models' => 'Pago Prov Efectivos',
			'id' => Yii::t('application', 'ID'),
			'idPagoProveedor' => Yii::t('application', 'Id Pago Proveedor'),
			'monto' => Yii::t('application', 'Monto'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idPagoProveedor',$this->idPagoProveedor);
		$criteria->compare('monto',$this->monto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoProvEfectivo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
