<?php

/**
 * This is the model class for table "vClientesLista".
 *
 * The followings are the available columns in table 'vClientesLista':
 * @property integer $id
 * @property string $razonSocial
 * @property string $direccion
 * @property string $nombreFantasia
 * @property string $telefonos
 * @property integer $id_rubro
 * @property string $cp
 * @property string $zp
 * @property integer $codViajante
 * @property double $saldoActual
 * @property string $rubro
 * @property string $nombrelocalidad
 * @property string $id_provincia
 * @property string $idIva
 * @property integer $categoriaIva
 *
 * @property Provincia $oProvincia
 */
class VClientesLista extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vClientesLista';
	}

    public function primaryKey(){
        return 'id';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cp, zp', 'required'),
			array('id, id_rubro, codViajante, categoriaIva', 'numerical', 'integerOnly'=>true),
			array('saldoActual', 'numerical'),
			array('razonSocial, telefonos', 'length', 'max'=>40),
			array('direccion', 'length', 'max'=>30),
			array('nombreFantasia', 'length', 'max'=>60),
			array('cp, zp', 'length', 'max'=>4),
			array('rubro', 'length', 'max'=>35),
			array('nombrelocalidad', 'length', 'max'=>65),
			array('id_provincia', 'length', 'max'=>1),
			array('idIva', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, razonSocial, direccion, nombreFantasia, telefonos, id_rubro, cp, zp, codViajante, saldoActual, rubro, nombrelocalidad, id_provincia, idIva, categoriaIva', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oProvincia' => array(self::BELONGS_TO, 'Provincia', 'id_provincia'),
			'oViajante' => array(self::BELONGS_TO, 'Viajante', 'codViajante'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vclientes Lista',
            'models' => 'Vclientes Listas',
			'id' => Yii::t('application', 'ID'),
			'razonSocial' => Yii::t('application', 'Razon Social'),
			'direccion' => Yii::t('application', 'Direccion'),
			'nombreFantasia' => Yii::t('application', 'Nombre Fantasia'),
			'telefonos' => Yii::t('application', 'Telefonos'),
			'id_rubro' => Yii::t('application', 'Id Rubro'),
			'cp' => Yii::t('application', 'Cp'),
			'zp' => Yii::t('application', 'Zp'),
			'codViajante' => Yii::t('application', 'Cod Viajante'),
			'saldoActual' => Yii::t('application', 'Saldo Actual'),
			'rubro' => Yii::t('application', 'Rubro'),
			'nombrelocalidad' => Yii::t('application', 'Nombrelocalidad'),
			'id_provincia' => Yii::t('application', 'Id Provincia'),
			'idIva' => Yii::t('application', 'Id Iva'),
			'categoriaIva' => Yii::t('application', 'Categoria Iva'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProvincia');

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.razonSocial',$this->razonSocial,true);
		$criteria->compare('t.direccion',$this->direccion,true);
		$criteria->compare('t.nombreFantasia',$this->nombreFantasia,true);
		$criteria->compare('t.telefonos',$this->telefonos,true);
		$criteria->compare('t.id_rubro',$this->id_rubro);
		$criteria->compare('t.cp',$this->cp,true);
		$criteria->compare('t.zp',$this->zp,true);
		$criteria->compare('t.codViajante',$this->codViajante);
		$criteria->compare('t.saldoActual',$this->saldoActual);
		$criteria->compare('t.rubro',$this->rubro,true);
		$criteria->compare('t.nombrelocalidad',$this->nombrelocalidad,true);
		$criteria->compare('t.id_provincia',$this->id_provincia,true);
		$criteria->compare('t.idIva',$this->idIva,true);
		$criteria->compare('t.categoriaIva',$this->categoriaIva);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'oProvincia.nombre , nombrelocalidad , razonSocial ',
                'attributes' => array(
                    't.id_provincia' => array(
                        'desc' => 'oProvincia.nombre desc, t.nombrelocalidad desc',
                        'asc' => 'oProvincia.nombre asc, t.nombrelocalidad asc'
                    ),
                    '*',
                )
            )

		));
	}

		/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchConSaldo()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProvincia');

		$criteria->compare('t.id',$this->id);
		$criteria->compare('t.razonSocial',$this->razonSocial,true);
		$criteria->compare('t.direccion',$this->direccion,true);
		$criteria->compare('t.nombreFantasia',$this->nombreFantasia,true);
		$criteria->compare('t.telefonos',$this->telefonos,true);
		$criteria->compare('t.id_rubro',$this->id_rubro);
		$criteria->compare('t.cp',$this->cp,true);
		$criteria->compare('t.zp',$this->zp,true);
		$criteria->compare('t.codViajante',$this->codViajante);
		$criteria->compare('t.saldoActual',$this->saldoActual);
		$criteria->compare('t.rubro',$this->rubro,true);
		$criteria->compare('t.nombrelocalidad',$this->nombrelocalidad,true);
		$criteria->compare('t.id_provincia',$this->id_provincia,true);
		$criteria->compare('t.idIva',$this->idIva,true);
		$criteria->compare('t.categoriaIva',$this->categoriaIva);

		$criteria->addCondition('t.saldoActual > 0');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'oProvincia.nombre , nombrelocalidad , razonSocial ',
                'attributes' => array(
                    't.id_provincia' => array(
                        'desc' => 'oProvincia.nombre desc, t.nombrelocalidad desc',
                        'asc' => 'oProvincia.nombre asc, t.nombrelocalidad asc'
                    ),
                    '*',
                )
            )

		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VClientesLista the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
