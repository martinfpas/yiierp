<?php

/**
 * This is the model class for table "comprobanteProveedor".
 *
 * The followings are the available columns in table 'comprobanteProveedor':
 * @property integer $id
 * @property string $Nro_Comprobante
 * @property integer $id_Proveedor
 * @property string $FechaFactura
 * @property string $Clase
 * @property string $Nro_Puesto_Vta
 * @property double $Nograv
 * @property double $TotalNeto
 * @property string $MesCarga
 * @property double $Subtotal
 * @property double $Descuento
 * @property double $Iva
 * @property double $IngBrutos
 * @property double $Rg3337
 * @property string $FechaCarga
 * @property integer $id_Cuenta
 * @property double $PercepIB
 * @property double $ImpGanancias
 * @property string $TipoComprobante
 * @property string $Cai
 * @property string $Fecha_Vencimiento
 * @property integer $Cant_alicuotas_iva
 * @property double $montoPagado
 *
 * The followings are the available model relations:
 * @property AlicuotasCompra[] $aAlicuotasCompra
 * @property Proveedor $oProveedor
 * @property DetalleComprobanteProv[] $aDetalleComprobanteProv
 * @property FacturaOrdenPago[] $aFacturaOrdenPago
 * @property Pago2FactProv[] $aPago2FactProv
 */
class ComprobanteProveedor2 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobanteProveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, MesCarga, FechaCarga, id_Cuenta, TipoComprobante, Cant_alicuotas_iva', 'required'),
			array('id_Proveedor, id_Cuenta, Cant_alicuotas_iva', 'numerical', 'integerOnly'=>true),
			array('Nograv, TotalNeto, Subtotal, Descuento, Iva, IngBrutos, Rg3337, PercepIB, ImpGanancias, montoPagado', 'numerical'),
			array('Nro_Comprobante', 'length', 'max'=>8),
			array('Clase', 'length', 'max'=>1),
			array('Nro_Puesto_Vta', 'length', 'max'=>4),
			array('MesCarga', 'length', 'max'=>7),
			array('TipoComprobante', 'length', 'max'=>3),
			array('Cai', 'length', 'max'=>15),
			array('Fecha_Vencimiento', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, Nograv, TotalNeto, MesCarga, Subtotal, Descuento, Iva, IngBrutos, Rg3337, FechaCarga, id_Cuenta, PercepIB, ImpGanancias, TipoComprobante, Cai, Fecha_Vencimiento, Cant_alicuotas_iva, montoPagado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aAlicuotasCompra' => array(self::HAS_MANY, 'AlicuotasCompra', 'idComprobanteCompra'),
			'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'id_Proveedor'),
			'aDetalleComprobanteProv' => array(self::HAS_MANY, 'DetalleComprobanteProv', 'idComprobanteProveedor'),
			'aFacturaOrdenPago' => array(self::HAS_MANY, 'FacturaOrdenPago', 'idComprobanteProveedor'),
			'aPago2FactProv' => array(self::HAS_MANY, 'Pago2FactProv', 'idComprobanteProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Comprobante Proveedor2',
            'models' => 'Comprobante Proveedor2s',
			'id' => Yii::t('application', 'ID'),
			'Nro_Comprobante' => Yii::t('application', 'Nro Comprobante'),
			'id_Proveedor' => Yii::t('application', 'Id Proveedor'),
			'FechaFactura' => Yii::t('application', 'Fecha Factura'),
			'Clase' => Yii::t('application', 'Clase'),
			'Nro_Puesto_Vta' => Yii::t('application', 'Nro Puesto Vta'),
			'Nograv' => Yii::t('application', 'Nograv'),
			'TotalNeto' => Yii::t('application', 'Total Neto'),
			'MesCarga' => Yii::t('application', 'Mes Carga'),
			'Subtotal' => Yii::t('application', 'Subtotal'),
			'Descuento' => Yii::t('application', 'Descuento'),
			'Iva' => Yii::t('application', 'Iva'),
			'IngBrutos' => Yii::t('application', 'Ing Brutos'),
			'Rg3337' => Yii::t('application', 'Rg3337'),
			'FechaCarga' => Yii::t('application', 'Fecha Carga'),
			'id_Cuenta' => Yii::t('application', 'Id Cuenta'),
			'PercepIB' => Yii::t('application', 'Percep Ib'),
			'ImpGanancias' => Yii::t('application', 'Imp Ganancias'),
			'TipoComprobante' => Yii::t('application', 'Tipo Comprobante'),
			'Cai' => Yii::t('application', 'Cai'),
			'Fecha_Vencimiento' => Yii::t('application', 'Fecha Vencimiento'),
			'Cant_alicuotas_iva' => Yii::t('application', 'Cant Alicuotas Iva'),
			'montoPagado' => Yii::t('application', 'Monto Pagado'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
		$criteria->compare('id_Proveedor',$this->id_Proveedor);
		$criteria->compare('FechaFactura',$this->FechaFactura,true);
		$criteria->compare('Clase',$this->Clase,true);
		$criteria->compare('Nro_Puesto_Vta',$this->Nro_Puesto_Vta,true);
		$criteria->compare('Nograv',$this->Nograv);
		$criteria->compare('TotalNeto',$this->TotalNeto);
		$criteria->compare('MesCarga',$this->MesCarga,true);
		$criteria->compare('Subtotal',$this->Subtotal);
		$criteria->compare('Descuento',$this->Descuento);
		$criteria->compare('Iva',$this->Iva);
		$criteria->compare('IngBrutos',$this->IngBrutos);
		$criteria->compare('Rg3337',$this->Rg3337);
		$criteria->compare('FechaCarga',$this->FechaCarga,true);
		$criteria->compare('id_Cuenta',$this->id_Cuenta);
		$criteria->compare('PercepIB',$this->PercepIB);
		$criteria->compare('ImpGanancias',$this->ImpGanancias);
		$criteria->compare('TipoComprobante',$this->TipoComprobante,true);
		$criteria->compare('Cai',$this->Cai,true);
		$criteria->compare('Fecha_Vencimiento',$this->Fecha_Vencimiento,true);
		$criteria->compare('Cant_alicuotas_iva',$this->Cant_alicuotas_iva);
		$criteria->compare('montoPagado',$this->montoPagado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ComprobanteProveedor2 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
