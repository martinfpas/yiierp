<?php

/**
 * This is the model class for table "vDocumentosConSaldo".
 *
 * The followings are the available columns in table 'vDocumentosConSaldo':
 * @property string $uid
 * @property string $tipo
 * @property string $nroComprobante
 * @property integer $estado
 * @property integer $id
 * @property string $fecha
 * @property integer $Id_Cliente
 * @property integer $codViajante
 * @property double $total
 * @property double $saldo
 * @property string $localidad
 */
class VDocumentosConSaldo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vDocumentosConSaldo6';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, Id_Cliente', 'numerical', 'integerOnly'=>true),
			array('total, saldo', 'numerical'),
			array('uid', 'length', 'max'=>36),
			array('tipo', 'length', 'max'=>2),
			array('nroComprobante', 'length', 'max'=>8),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, tipo, nroComprobante,estado, id, fecha, Id_Cliente,codViajante total, saldo, localidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCliente'     => array(self::BELONGS_TO, 'Cliente', 'Id_Cliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vdocumentos Con Saldo',
            'models' => 'Vdocumentos Con Saldos',
			'uid' => Yii::t('application', 'Uid'),
			'tipo' => Yii::t('application', 'Tipo'),
			'nroComprobante' => Yii::t('application', 'Nro Comprobante'),
			'id' => Yii::t('application', 'ID'),
			'fecha' => Yii::t('application', 'Fecha'),
			'Id_Cliente' => Yii::t('application', 'Id Cliente'),
			'total' => Yii::t('application', 'Total'),
			'saldo' => Yii::t('application', 'Saldo'),
			'localidad' => Yii::t('application', 'Localidad'),
		);
	}

    public function primaryKey(){

        return 'uid';

	}
	
	public function getFullTitleDocumento(){
        if($this->tipo == CargaDocumentos::NE){
			$oNotaEntrega = NotaEntrega::model()->findByPk($this->id);
			return $oNotaEntrega->getFullTitle();
		}else if($this->tipo == 'SC'){
			return '';
        }else{
			$oComprobante = Comprobante::model()->findByPk($this->id);
			return $oComprobante->getFullTitle();
        }
	}
	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('nroComprobante',$this->nroComprobante,true);
		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('Id_Cliente',$this->Id_Cliente);
        $criteria->compare('codViajante',$this->codViajante);
		$criteria->compare('total',$this->total,true);
		$criteria->compare('saldo',$this->saldo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->addCondition('t.fecha >= 2020-06-01');

        $criteria->with = array('oCliente');

        $criteria->order = 'localidad, oCliente.razonSocial,oCliente.nombreFantasia,t.fecha asc,nroComprobante asc';

        $criteria->compare('t.uid',$this->uid,true);
        $criteria->compare('t.tipo',$this->tipo,true);
        $criteria->compare('t.nroComprobante',$this->nroComprobante,true);
        $criteria->compare('t.id',$this->id);
        $criteria->compare('t.fecha',$this->fecha,true);
        $criteria->compare('t.Id_Cliente',$this->Id_Cliente);
        $criteria->compare('t.codViajante',$this->codViajante);
        $criteria->compare('t.total',$this->total);
		$criteria->compare('t.saldo',$this->saldo);
			

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VDocumentosConSaldo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
