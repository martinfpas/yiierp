<?php

/**
 * This is the model class for table "pagoProveedor".
 *
 * The followings are the available columns in table 'pagoProveedor':
 * @property integer $id
 * @property double $monto
 * @property string $fecha
 * @property integer $idProveedor
 * @property integer $idPago2Factura
 * @property integer $estado
 *
 * The followings are the available model relations:
 * @property Pago2FactProv[] $aPago2FactProv
 * @property PagoProvCheque[] $aPagoProvCheque
 * @property PagoProvEfectivo[] $aPagoProvEfectivo
 * @property PagoProvMovimientoBanc[] $aPagoProvMovimientoBanc
 * @property Proveedor $oProveedor
 * @property Pago2FactProv $oPago2Factura
 * @property PagoProveedorChequePpio[] $aPagoProveedorChequePpio
 *
 */
class PagoProveedor extends CActiveRecord
{
    const iBorrador = 0;
    const iGuardada = 1;
    const iCerrada = 2;
    const iArchivada = 3;

    const borrador = 'Borrador';
    const guardada = 'Guardado';
    const cerrada = 'Cerrado';
    const archivada = 'Archivado';

    public static $aEstado = array(
        self::iBorrador => self::borrador,
        self::iGuardada => self::guardada,
        self::iCerrada => self::cerrada,
        self::iArchivada=> self::archivada,
    );
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoProveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, idProveedor', 'required'),
			array('idProveedor, idPago2Factura, estado', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monto, fecha, idProveedor, idPago2Factura, estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aPago2FactProv' => array(self::HAS_MANY, 'Pago2FactProv', 'idPagoProveedor'),
			'aPagoProvCheque' => array(self::HAS_MANY, 'PagoProvCheque', 'idPagoProv'),
            'aPagoProvEfectivo' => array(self::HAS_MANY, 'PagoProvEfectivo', 'idPagoProveedor'),
			'aPagoProvMovimientoBanc' => array(self::HAS_MANY, 'PagoProvMovimientoBanc', 'idPagoProveedor'),
            'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'idProveedor'),
			'oPago2Factura' => array(self::BELONGS_TO, 'Pago2FactProv', 'idPago2Factura'),
            'aPagoProveedorChequePpio' => array(self::HAS_MANY, 'PagoProveedorChequePpio', 'idPagoProv'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago a Proveedor',
            'models' => 'Pagos a Proveedores',
			'id' => Yii::t('application', 'ID'),
			'monto' => Yii::t('application', 'Monto'),
			'fecha' => Yii::t('application', 'Fecha'),
			'idProveedor' => Yii::t('application', 'Id Proveedor'),
			'idPago2Factura' => Yii::t('application', 'Id Pago2 Factura'),
            'estado' => Yii::t('application', 'Estado'),
		);
	}

	public function getTotalCH3ros(){
	    $total = 0;
        foreach ($this->aPagoProvCheque as $index => $item) {
            $total += $item->oCheque3ro->importe;
	    }
	    return $total;
    }

    public function getTotalEfectivo(){
        $total = 0;
        foreach ($this->aPagoProvEfectivo as $index => $item) {
            $total += $item->monto;
        }
        return $total;
    }

    public function getTotalChequesPpios(){
        $total = 0;
        foreach ($this->aPagoProveedorChequePpio as $index => $item) {
            $total += $item->oChequePpio->importe;
        }
        return $total;
    }

    public function getTotalMovBanc(){
        $total = 0;
        foreach ($this->aPagoProvMovimientoBanc as $index => $item) {
            $total += $item->oMovBanc->Importe;
        }
        return $total;
    }

	public function recalcular(){
        $monto = 0;

        foreach ($this->aPagoProvCheque as $index => $item) {
            $monto += $item->monto;
        }

        foreach ($this->aPagoProvEfectivo as $index => $item) {
            $monto += $item->monto;
        }

        foreach ($this->aPagoProvMovimientoBanc as $index => $item) {
            $monto += $item->monto;
        }

        foreach ($this->aPagoProveedorChequePpio as $index => $item) {
            $monto += $item->monto;
        }
        $this->monto = $monto;
	    if (!$this->save()){
            throw new Exception(CHtml::errorSummary($this));
        }
    }	

	public function beforeSave(){
	    // TODO: IMPLEMENTAR LA TRANSACCION
	    $oEstadoPrevio = PagoProveedor::model()->with('oPago2Factura','aPago2FactProv')->findByPk($this->id);
	    $bReturn = parent::beforeSave();
	    if ($bReturn){
	        $oProveedor = $this->oProveedor;
	        // SI PASA DE BORRADOR A OTRA COSA
	        if ($this->estado == PagoProveedor::iCerrada){
	            $montoACancelar = $this->monto;
                $oProveedor->saldo = $oProveedor->saldo - $this->monto;
                if (!$oProveedor->save()){
                    //TODO: CONTROLAR EL ERROR
                }
                foreach ($this->aPago2FactProv as $index => $oPago2FactProv) {
                    if ($montoACancelar > 0){
                        $dif  = $oPago2FactProv->oComprobanteProveedor->TotalNeto - $oPago2FactProv->oComprobanteProveedor->montoPagado;
                        if($montoACancelar < $dif){
                            $oPago2FactProv->oComprobanteProveedor->montoPagado += $montoACancelar;
                            $montoACancelar = 0;
                        }else{
                            $montoACancelar -= $dif;
                            $oPago2FactProv->oComprobanteProveedor->montoPagado += $dif;
                        }

                        if (!$oPago2FactProv->oComprobanteProveedor->save()){
                            // TODO: MANEJAR EL  ERROR

                        }
                    }

                }

            }
        }
        return $bReturn;
    }

    public function beforeDelete(){
	    /** @var $oEstadoPrevio PagoProveedor */
        $transaction = Yii::app()->db->getCurrentTransaction();
        $shouldCommit = true;
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
            //$shouldCommit = false;
        }

        $oEstadoPrevio = PagoProveedor::model()->with('oPago2Factura','aPago2FactProv')->findByPk($this->id);

        if ($oEstadoPrevio->oPago2Factura != null){
            Yii::log('$oEstadoPrevio->oPago2Factura '.$oEstadoPrevio->oPago2Factura->id,'error');
            $oComprobanteProveedor = $oEstadoPrevio->oPago2Factura->oComprobanteProveedor;
            $oComprobanteProveedor->montoPagado -= $oEstadoPrevio->oPago2Factura->oPagoProveedor->monto;
            if(!$oEstadoPrevio->oPago2Factura->delete()){
                Yii::log('!$oEstadoPrevio->oPago2Factura->delete()','error');
                $transaction->rollback();
                return false;
            }
            if(!$oComprobanteProveedor->save()){
                Yii::log('$oComprobanteProveedor-> '.CHtml::errorSummary($oComprobanteProveedor),'error');
                $transaction->rollback();
                return false;
            }
        }

        foreach ($oEstadoPrevio->aPago2FactProv as $index => $oPago2Factura) {
            Yii::log('$oPago2Factura '.$oPago2Factura->id,'error');
            $oComprobanteProveedor = $oPago2Factura->oComprobanteProveedor;
            //$oComprobanteProveedor->montoPagado -= $oPago2Factura->oPagoProveedor->monto;
            $oComprobanteProveedor->montoPagado = 0;
            if(!$oPago2Factura->delete()){
                Yii::log('!$oPago2Factura->delete()','error');
                $transaction->rollback();
                return false;
            }
            if(!$oComprobanteProveedor->save()){
                Yii::log('$oComprobanteProveedor-> '.CHtml::errorSummary($oComprobanteProveedor),'error');
                $transaction->rollback();
                return false;
            }
        }


        foreach ($oEstadoPrevio->aPagoProvCheque as $index => $item) {
            if(!$item->delete()){
                Yii::log('!$item->delete()','error');
                $transaction->rollback();
                return false;
            }
        }

        foreach ($oEstadoPrevio->aPagoProvEfectivo as $index => $item) {
            if(!$item->delete()){
                Yii::log('!$item->->->delete()','error');
                $transaction->rollback();
                return false;
            }
        }

        foreach ($oEstadoPrevio->aPagoProvMovimientoBanc as $index => $item) {
            if(!$item->delete()){
                Yii::log('!$item->->->delete()','error');
                $transaction->rollback();
                return false;
            }
        }

        foreach ($oEstadoPrevio->aPagoProveedorChequePpio as $index => $item) {
            if(!$item->delete()){
                Yii::log('!$item->->->->delete()','error');
                $transaction->rollback();
                return false;
            }
        }


        $bReturn = parent::beforeDelete();
        if ($bReturn){
            $oProveedor = $oEstadoPrevio->oProveedor;
            // SI ESTABA EN estado DISTITNO A BORRADOR
            if ($oEstadoPrevio->estado > PagoProveedor::iBorrador){
                $montoAnterior = 0;
                if($oEstadoPrevio != null){
                    $montoAnterior = $oEstadoPrevio->monto;
                }
                $oProveedor->saldo = $oProveedor->saldo + $montoAnterior ;
                if (!$oProveedor->save()){
                    Yii::log('$oProveedor->'.CHtml::errorSummary($oProveedor),'error');
                    $transaction->rollback();
                    return false;
                }
            }
        }

        Yii::log('$oProveedor OK '.$oProveedor->id.' :: $shouldCommit:'.$shouldCommit,'warning');

        if ($shouldCommit){
            $transaction->commit();
        }

        return $bReturn;
    }

	public function getTextMedios(){
	    $numPagosEfectivo = sizeof($this->aPagoProvEfectivo);
        $numPagosCheque = sizeof($this->aPagoProvCheque);
        $numPagosMB = sizeof($this->aPagoProvMovimientoBanc);
	    if(($numPagosCheque+$numPagosEfectivo+$numPagosMB) > 1){
	        return 'Multiple';
        }else{
	        if ($numPagosEfectivo ==1){
	            return 'Efectivo';
            }
            if ($numPagosCheque ==1){
                return 'Cheque';
            }
            if ($numPagosMB ==1){
                return 'Movimiento Bancario';
            }
            return '-';
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProveedor');

		$criteria->compare('id',$this->id);
		$criteria->compare('monto',$this->monto);

		if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

		$criteria->compare('idProveedor',$this->idProveedor);

		$criteria->compare('estado',$this->estado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'attributes' => array(
                    'idProveedor' => array(
                        'asc' => 'oProveedor.razonSocial asc',
                        'desc' => 'oProveedor.razonSocial desc',
                    ),
                    '*',
                ),
                'defaultOrder' => 't.id desc',
            )
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoProveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
