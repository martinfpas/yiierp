<?php

/**
 * This is the model class for table "afipVoucherItem".
 *
 * The followings are the available columns in table 'afipVoucherItem':
 * @property integer $id
 * @property integer $idVoucher
 * @property string $codigo
 * @property string $descripcion
 * @property double $cantidad
 * @property integer $codigoUnidadMedida
 * @property double $precioUnitario
 * @property double $impBonif
 * @property double $importeItem
 *
 * The followings are the available model relations:
 * @property AfipVoucher $oIdVoucher
 */
class AfipVoucherItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AfipVoucherItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'afipVoucherItem';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idVoucher, descripcion, precioUnitario, importeItem', 'required'),
			array('idVoucher, codigoUnidadMedida', 'numerical', 'integerOnly'=>true),
			array('cantidad, precioUnitario, impBonif, importeItem', 'numerical'),
			array('codigo', 'length', 'max'=>14),
			//array('descripcion', 'length', 'max'=>40),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idVoucher, codigo, descripcion, cantidad, codigoUnidadMedida, precioUnitario, impBonif, importeItem', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdVoucher' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucher'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'idVoucher' => Yii::t('application', 'Id Voucher'),
		            'codigo' => Yii::t('application', 'Codigo'),
		            'descripcion' => Yii::t('application', 'Descripcion'),
		            'cantidad' => Yii::t('application', 'Cantidad'),
		            'codigoUnidadMedida' => Yii::t('application', 'Codigo Unidad Medida'),
		            'precioUnitario' => Yii::t('application', 'Precio Unitario'),
		            'impBonif' => Yii::t('application', 'Imp Bonif'),
		            'importeItem' => Yii::t('application', 'Importe Item'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idVoucher',$this->idVoucher);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('codigoUnidadMedida',$this->codigoUnidadMedida);
		$criteria->compare('precioUnitario',$this->precioUnitario);
		$criteria->compare('impBonif',$this->impBonif);
		$criteria->compare('importeItem',$this->importeItem);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}