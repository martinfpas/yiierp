<?php

/**
 * This is the model class for table "afipTributo".
 *
 * The followings are the available columns in table 'afipTributo':
 * @property integer $idTributo
 * @property integer $Id
 * @property string $Desc
 * @property double $BaseImp
 * @property double $Importe
 * @property double $Alic
 * @property integer $idVoucher
 *
 * The followings are the available model relations:
 * @property AfipVoucher $oIdVoucher
 */
class AfipTributo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'afipTributo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Id, BaseImp, Importe, Alic, idVoucher', 'required'),
			array('Id, idVoucher', 'numerical', 'integerOnly'=>true),
			array('BaseImp, Importe, Alic', 'numerical'),
			array('Desc', 'length', 'max'=>80),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idTributo, Id, Desc, BaseImp, Importe, Alic, idVoucher', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdVoucher' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucher'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTributo' => 'Id Tributo',
			'Id' => 'ID',
			'Desc' => 'Desc',
			'BaseImp' => 'Base Imp',
			'Importe' => 'Importe',
			'Alic' => 'Alic',
			'idVoucher' => 'Id Voucher',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTributo',$this->idTributo);
		$criteria->compare('Id',$this->Id);
		$criteria->compare('Desc',$this->Desc,true);
		$criteria->compare('BaseImp',$this->BaseImp);
		$criteria->compare('Importe',$this->Importe);
		$criteria->compare('Alic',$this->Alic);
		$criteria->compare('idVoucher',$this->idVoucher);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AfipTributo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
