<?php

/**
 * This is the model class for table "codigoPostal".
 *
 * The followings are the available columns in table 'codigoPostal':
 * @property string $codigoPostal
 * @property string $nombrelocalidad
 * @property string $id_provincia
 * @property string $zp
 *
 * The followings are the available model relations:
 * @property Cliente[] $aCliente 
 * @property Provincia $oProvincia
 */
class CodigoPostal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'codigoPostal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigoPostal, id_provincia', 'required'),
			array('codigoPostal, zp', 'length', 'max'=>4),
			array('nombrelocalidad', 'length', 'max'=>65),
			array('id_provincia', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('codigoPostal, nombrelocalidad, id_provincia, zp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'aCliente' => array(self::HAS_MANY, 'Cliente', 'cp'),
			'oProvincia' => array(self::BELONGS_TO, 'Provincia', 'id_provincia'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'codigoPostal' => 'Codigo Postal',
			'nombrelocalidad' => 'Nombre localidad',
			'id_provincia' => 'Id Provincia',
			'zp' => 'Zp',
		);
	}
	
	public function getIdTitulo() {
		return $this->codigoPostal.' - '.$this->nombrelocalidad;
	}

    public function getIdTituloYProv() {
        return $this->codigoPostal.' - '.$this->nombrelocalidad.'('.$this->oProvincia->nombre.')';
    }
	
	public function getIdZp() {
		return $this->zp.' - '.$this->nombrelocalidad;
	}	
	
	public function getCodigosUnicos($cp){
		
		//$cp = addcslashes($cp, '%_'); // escape LIKE's special characters
		$criteria = new CDbCriteria;
		
		$criteria->compare('codigoPostal',$cp,true);
		
		$criteria->select = 'Distinct(codigoPostal) as codigoPostal';
		
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => false,
		));
	}

    public function sarchCodigos($string){

        //$string = addcslashes($string, '%_'); // escape LIKE's special characters
        $criteria = new CDbCriteria;
        $criteria->order = 'nombrelocalidad';

        $criteria->addSearchCondition('nombrelocalidad', $string,true,'or' );
        $criteria->addSearchCondition('codigoPostal', $string,true,'or' );
/*
 *        echo '<pre>';
        print_r($criteria);
        echo '</pre>';
 *
 * */

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('oProvincia');

		$criteria->compare('codigoPostal',$this->codigoPostal,true);
		$criteria->compare('nombrelocalidad',$this->nombrelocalidad,true);
		$criteria->compare('t.id_provincia',$this->id_provincia);
		$criteria->compare('zp',$this->zp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'attributes' => array(
                    'id',
                    'zp',
                    'nombrelocalidad',
                    'codigoPostal',
                    'id_provincia' => array(
                        'asc' => 'oProvincia.nombre asc',
                        'desc' => 'oProvincia.nombre desc',
                    )
                )
            )
		));
	}
	public function scopes()
	{
		return array(
				'todos'=>array(
						'order'=>'codigoPostal,nombrelocalidad asc',
				),
				'todosXnombre' =>array(
						'order'=>'nombrelocalidad asc',
				),
		);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CodigoPostal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
