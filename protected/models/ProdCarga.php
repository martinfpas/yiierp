<?php

/**
 * This is the model class for table "prodCarga".
 *
 * The followings are the available columns in table 'prodCarga':
 * @property integer $id
 * @property integer $idCarga
 * @property integer $idArtVenta
 * @property integer $idPaquete
 * @property integer $cantidad
 * @property integer $cantDepurada
 *
 *
 * The followings are the available model relations:
 * @property Carga $oCarga
 * @property ArticuloVenta $oArtVenta
 * @property Paquete $oPaquete
 */
class ProdCarga extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prodCarga';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCarga, idArtVenta, cantidad', 'required'),
			array('idCarga, idArtVenta, cantidad', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCarga, idArtVenta, cantidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCarga' => array(self::BELONGS_TO, 'Carga', 'idCarga'),
            'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
            'oPaquete' => array(self::BELONGS_TO, 'Paquete', 'idPaquete'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idCarga' => 'Id Carga',
			'idArtVenta' => 'Id Art Venta',
			'cantXbulto' => 'Cant x bulto',
			'cantidad' => 'Cantidad',
            'idPaquete' => 'Id Paquete',
            'cantDepurada' => 'Cant. Depurada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idCarga',$this->idCarga);
		$criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('idPaquete',$this->idPaquete);
		$criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('cantDepurada',$this->cantDepurada);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter without pagination
     *
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;


        $criteria->with = array('oArtVenta');

        $criteria->compare('id',$this->id);
        $criteria->compare('idCarga',$this->idCarga);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('idPaquete',$this->idPaquete);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('cantDepurada',$this->cantDepurada);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort'=> array(
                'defaultOrder' => 'oArtVenta.nombre asc ,oArtVenta.contenido asc',
                'attributes' => array(
                    'idArtVenta' => array(
                        'asc'  => 'oArtVenta.nombre asc ,oArtVenta.contenido asc',
                        'desc' => 'oArtVenta.nombre desc,oArtVenta.contenido desc',
                    )
                )
            ),
        ));
    }



    /**
     * @return int
     */
    public function getCantXbulto()
    {
	if($this->oPaquete != null){
		return $this->oPaquete->cantidad;
	}else{
		print_r($this->oArtVenta->attributes);

		print_r($this->attributes);
		die();

	}
    }

    public function getCantbultoPres()
    {
        return $this->oPaquete->cantidad.'   x '.$this->oArtVenta->contenido. ' '.$this->oArtVenta->uMedida;
    }

    public function getEnvase()
    {
        return $this->oPaquete->oEnvase->descripcion;
    }

    public function getBultos(){
        return $this->cantidad / $this->oPaquete->cantidad;
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProdCarga the static model class
	 */

	public function getKilos(){
        //$unidades = $this->cantidad * $this->cantXbulto;
        $unidades = $this->cantidad;
        $oUnidadDeMedida = Udm::model()->findByPk($this->oArtVenta->uMedida);
        $unidadesXkilo = 1;
        if($oUnidadDeMedida != null && $oUnidadDeMedida->kilosXudm <> 0 ){
            $unidadesXkilo = $oUnidadDeMedida->kilosXudm;
        }
        $kilosXunidad = $this->oArtVenta->contenido / $unidadesXkilo;

        return $kilosXunidad * $unidades;
	}
	
	public function getBultosRecalculados(){
		$criteria = new CDbCriteria;
		$criteria->with = 'oNotaPedido';
		$criteria->addCondition('oNotaPedido.idCarga='.$this->idCarga);
		$criteria->addCondition('t.idArtVenta='.$this->idArtVenta); 
		$criteria->addCondition('t.idPaquete='.$this->idPaquete); 

		$cantidad = 0;
		$aProdNotas = ProdNotaPedido::model()->findAll($criteria);
		foreach($aProdNotas as $key => $oProdNota){
			$cantidad += $oProdNota->cantidad;
		}
		return $cantidad / $this->oPaquete->cantidad;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
