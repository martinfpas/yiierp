<?php

/**
 * This is the model class for table "entregaGtos".
 *
 * The followings are the available columns in table 'entregaGtos':
 * @property integer $id
 * @property integer $idEntregaDoc
 * @property string $gasto
 * @property double $montoGasto
 *
 * The followings are the available model relations:
 * @property EntregaDoc $oEntregaDoc
 */
class EntregaGtos extends CActiveRecord
{

    public static $aTipos = array(
        'Combustible' => 'Combustible',
        'Peaje' => 'Peaje',
        'Viaticos' => 'Viaticos',
        'Otros' => 'Otros',
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'entregaGtos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idEntregaDoc, gasto, montoGasto', 'required'),
			array('idEntregaDoc', 'numerical', 'integerOnly'=>true),
			array('montoGasto', 'numerical'),
			array('gasto', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idEntregaDoc, gasto, montoGasto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oEntregaDoc' => array(self::BELONGS_TO, 'EntregaDoc', 'idEntregaDoc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idEntregaDoc' => 'Id Carga Doc',
			'gasto' => 'Gasto',
			'montoGasto' => 'Monto Gasto',
		);
	}

    public function esEditable(){
        return ($this->oEntregaDoc != null && $this->oEntregaDoc->esEditable());
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idEntregaDoc',$this->idEntregaDoc);
		$criteria->compare('gasto',$this->gasto,true);
		$criteria->compare('montoGasto',$this->montoGasto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     *
     * Do not have Pagination or sort
     */
    public function searchWpWs()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('idEntregaDoc',$this->idEntregaDoc);
        $criteria->compare('gasto',$this->gasto,true);
        $criteria->compare('montoGasto',$this->montoGasto);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => false,
            'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EntregaGtos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
