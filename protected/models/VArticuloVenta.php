<?php

/**
 * This is the model class for table "vArticuloVenta".
 *
 * The followings are the available columns in table 'vArticuloVenta':
 * @property integer $id_articulo_vta
 * @property string $descripcion
 * @property integer $id_presentacion
 * @property integer $id_articulo
 * @property double $contenido
 * @property string $uMedida
 * @property string $codBarra
 * @property double $precio
 * @property double $costo
 * @property integer $enLista
 * @property double $stockMinimo
 * @property integer $id_rubro
 */
class VArticuloVenta extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vArticuloVenta';
	}

    public function primaryKey(){

        return 'id_articulo_vta';

    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, id_presentacion, id_articulo, contenido, uMedida, precio', 'required'),
			array('id_articulo_vta, id_presentacion, id_articulo, enLista, id_rubro', 'numerical', 'integerOnly'=>true),
			array('contenido, precio, costo, stockMinimo', 'numerical'),
			array('descripcion', 'length', 'max'=>70),
			array('uMedida', 'length', 'max'=>6),
			array('codBarra', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_articulo_vta, descripcion, id_presentacion, id_articulo, contenido, uMedida, codBarra, precio, costo, enLista, stockMinimo, id_rubro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_articulo_vta' => 'Id Articulo Vta',
			'descripcion' => 'Descripcion',
			'id_presentacion' => 'Id Presentacion',
			'id_articulo' => 'Id Articulo',
			'contenido' => 'Contenido',
			'uMedida' => 'U Medida',
			'codBarra' => 'Cod Barra',
			'precio' => 'Precio',
			'costo' => 'Costo',
			'enLista' => 'En Lista',
			'stockMinimo' => 'Stock Minimo',
			'id_rubro' => 'Id Rubro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_articulo_vta',$this->id_articulo_vta);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('id_presentacion',$this->id_presentacion);
		$criteria->compare('id_articulo',$this->id_articulo);
		$criteria->compare('contenido',$this->contenido);
		$criteria->compare('uMedida',$this->uMedida,true);
		$criteria->compare('codBarra',$this->codBarra,true);
		$criteria->compare('precio',$this->precio);
		$criteria->compare('costo',$this->costo);
		$criteria->compare('enLista',$this->enLista);
		$criteria->compare('stockMinimo',$this->stockMinimo);
		$criteria->compare('id_rubro',$this->id_rubro);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VArticuloVenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
