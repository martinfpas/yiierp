<?php

/**
 * This is the model class for table "facturaProveedor".
 *
 * The followings are the available columns in table 'facturaProveedor':
 * @property integer $id
 * @property string $Nro_Comprobante
 * @property integer $id_Proveedor
 * @property string $FechaFactura
 * @property string $Clase
 * @property string $Nro_Puesto_Vta
 * @property double $Nograv
 * @property double $TotalNeto
 * @property string $MesCarga
 * @property double $Subtotal
 * @property double $Descuento
 * @property double $Iva
 * @property double $IngBrutos
 * @property double $Rg3337
 * @property string $FechaCarga
 * @property integer $id_Cuenta
 * @property double $PercepIB
 * @property double $ImpGanancias
 * @property integer $TipoComprobante
 * @property string $Cai
 * @property string $Fecha_Vencimiento
 * @property integer $Cant_alicuotas_iva
 * @property double $montoPagado
 *
 *
 * The followings are the available model relations:
 * @property Proveedor $oProveedor
 * @property CuentaGastos $oCuentaGastos
 * @property DetalleFacturaProv[] $aDetalleFactura
 * @property Pago2FactProv[] $aPago2FactProv
 */
class FacturaProveedor extends CActiveRecord
{
    public $bPagoInmediado = true;
    public $bIsNew = false;
    public $condImpaga;
    // RELACION CODIGO => MONTO DE ALICUOTA EN AFIP
    public static $aIvaAlicuotasMontos =  array
    (
        '0' => '0',
        '10.5' => '10.5',
        '21' => '21',
        '27' => '27',
        '5' => '5',
        '2.5' => '2.5'
    );

    public static $aClase = array(
        '001' => 'FACTURAS A',
        '081' => 'TIQUE FACTURA A',
        '002' => 'NOTAS DE DEBITO A',
        '003' => 'NOTAS DE CREDITO A',
        '004' => 'RECIBOS A',
        '011' => 'FACTURAS C',
        '015' => 'RECIBOS C',
        '118' => 'TIQUE FACTURA M',
        '006' => 'FACTURAS B',
    );

    public static $aFactorMultip = array(
        '001' => 1,
        '081' => 1,
        '002' => 1,
        '003' => -1,
        '004' => -1,
        '011' => 1,
        '015' => -1,
        '118' => 1,
        '006' => 1,
    );

    public static $aClaseTipo = array(
        '001' => 'A',
        '081' => 'A',
        '002' => 'A',
        '003' => 'A',
        '004' => 'A',
        '011' => 'C',
        '015' => 'C',
        '118' => 'M',
        '006' => 'B',
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'facturaProveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, MesCarga, FechaCarga, id_Cuenta, TipoComprobante, Cant_alicuotas_iva', 'required'),
			array('id_Proveedor, id_Cuenta, Cant_alicuotas_iva', 'numerical', 'integerOnly'=>true),
			array('Nograv, TotalNeto, Subtotal, Descuento, Iva, IngBrutos, Rg3337, PercepIB, ImpGanancias, montoPagado', 'numerical'),
			array('Nro_Comprobante', 'length', 'max'=>8),
            array('MesCarga', 'length', 'max'=>7),
			array('Nro_Puesto_Vta', 'length', 'max'=>1),
            array('TipoComprobante', 'length', 'max'=>3),
			array('Cai', 'length', 'max'=>15),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, Nro_Comprobante, id_Proveedor, FechaFactura, Clase, Nro_Puesto_Vta, Nograv, TotalNeto, MesCarga, Subtotal, Descuento, Iva, IngBrutos, Rg3337, 
			FechaCarga, id_Cuenta, PercepIB, ImpGanancias, TipoComprobante, Cai, Fecha_Vencimiento, Cant_alicuotas_iva, montoPagado, bPagoInmediado,condImpaga', 'safe', 'on'=>'search'),
		);
	}

    public function recalcular(){
	    /* @var $oDetalleFactura DetalleFacturaProv */
	    $subtotal = 0;
	    $totalPrevio = $this->TotalNeto;
        foreach ($this->aDetalleFactura as $index => $oDetalleFactura) {
            $subtotal += $oDetalleFactura->cantidadRecibida * $oDetalleFactura->precioUnitario * (1+($oDetalleFactura->alicuota / 100));
        }
        $this->Subtotal = $subtotal;
        $this->TotalNeto = $subtotal - $this->Descuento + $this->IngBrutos + $this->Rg3337 + $this->PercepIB  + $this->ImpGanancias;
        if (!$this->save()){
            throw new Exception(CHtml::errorSummary($this));
        }else{
            $this->oProveedor->saldo =  $this->oProveedor->saldo + ($this->TotalNeto - $totalPrevio) * self::$aFactorMultip[$this->TipoComprobante];
            if (!$this->oProveedor->save()){
                Yii::log('Error al actualizar el saldo: Previo: '.$this->oProveedor->saldo,'error');
                Yii::log('Error al actualizar el saldo: $totalPrevio: '.$totalPrevio,'error');
                Yii::log('Error al actualizar el saldo: $this->TotalNeto: '.$this->TotalNeto,'error');
                throw new Exception('Error al actualizar el saldo: '.CHtml::errorSummary($this->oProveedor));
            }
        }
    }


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oProveedor' => array(self::BELONGS_TO, 'Proveedor', 'id_Proveedor'),
            'aDetalleFactura' => array(self::HAS_MANY,'DetalleFacturaProv','idFacturaProveedor'),
            'oCuentaGastos' => array(self::BELONGS_TO, 'CuentaGastos', 'id_Cuenta'),
            'aPago2FactProv' => array(self::HAS_MANY, 'Pago2FactProv', 'idFacturaProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'model' => 'Factura Recibida',
            'models' => 'Facturas Recibidas',
            'id' => Yii::t('application', 'ID'),
            'Nro_Comprobante' => Yii::t('application', 'Nro Doc Recib'),
            'id_Proveedor' => Yii::t('application', 'Id Proveedor'),
            'FechaFactura' => Yii::t('application', 'Fecha Factura'),
            'Clase' => Yii::t('application', 'Clase'),
            'Nro_Puesto_Vta' => Yii::t('application', 'Nro Puesto Vta'),
            'Nograv' => Yii::t('application', 'Nograv'),
            'TotalNeto' => Yii::t('application', 'Total Neto'),
            'MesCarga' => Yii::t('application', 'Mes Carga'),
            'Subtotal' => Yii::t('application', 'Subtotal'),
            'Descuento' => Yii::t('application', 'Descuento'),
            'Iva' => Yii::t('application', 'Iva'),
            'IngBrutos' => Yii::t('application', 'Ing Brutos'),
            'Rg3337' => Yii::t('application', 'Rg3337'),
            'FechaCarga' => Yii::t('application', 'Fecha Carga'),
            'id_Cuenta' => Yii::t('application', 'Id Cuenta'),
            'PercepIB' => Yii::t('application', 'Percep Ib'),
            'ImpGanancias' => Yii::t('application', 'Imp Ganancias'),
            'TipoComprobante' => Yii::t('application', 'Tipo Comprobante'),
            'Cai' => Yii::t('application', 'Cai'),
            'Fecha_Vencimiento' => Yii::t('application', 'Fecha Vencimiento'),
            'Cant_alicuotas_iva' => Yii::t('application', 'Cant Alicuotas Iva'),
            'montoPagado' => Yii::t('application', 'Monto Pagado'),
            'bPagoInmediado'  => Yii::t('application', 'Pago Inmediato'),
		);
	}

	public function afterDelete(){
	    $ajuste = $this->TotalNeto * self::$aFactorMultip[$this->TipoComprobante] ;
	    $oProveedor = $this->oProveedor;

	    $bDelete =  parent::afterDelete();
        Yii::log('$ajuste: '.$ajuste.' >'.$bDelete.'<','warning');

        $oProveedor->saldo -= $ajuste;
        if(!$oProveedor->save()){
            Yii::log('Error al actualizar el saldo: Previo: '.$oProveedor->saldo,'error');
            Yii::log('Error al actualizar el saldo: $this->TotalNeto: '.$ajuste,'error');
            throw new Exception(CHtml::errorSummary($oProveedor));
        }

        return $bDelete;
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oProveedor');

		$criteria->compare('id',$this->id);
		$criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
		$criteria->compare('id_Proveedor',$this->id_Proveedor);
        if ($this->FechaFactura != null){
            $criteria->compare('FechaFactura',ComponentesComunes::fechaComparada($this->FechaFactura));
        }

		$criteria->compare('Clase',$this->Clase,true);
		$criteria->compare('Nro_Puesto_Vta',$this->Nro_Puesto_Vta,true);
		$criteria->compare('Nograv',$this->Nograv);
		$criteria->compare('TotalNeto',$this->TotalNeto);
		$criteria->compare('MesCarga',$this->MesCarga,true);
		$criteria->compare('Subtotal',$this->Subtotal);
		$criteria->compare('Descuento',$this->Descuento);
		$criteria->compare('Iva',$this->Iva);
		$criteria->compare('IngBrutos',$this->IngBrutos);
		$criteria->compare('Rg3337',$this->Rg3337);
		$criteria->compare('FechaCarga',$this->FechaCarga,true);
		$criteria->compare('id_Cuenta',$this->id_Cuenta);
		$criteria->compare('PercepIB',$this->PercepIB);
		$criteria->compare('ImpGanancias',$this->ImpGanancias);
		$criteria->compare('TipoComprobante',$this->TipoComprobante);
		$criteria->compare('Cai',$this->Cai,true);
		$criteria->compare('Fecha_Vencimiento',$this->Fecha_Vencimiento,true);
		$criteria->compare('Cant_alicuotas_iva',$this->Cant_alicuotas_iva);
        $criteria->compare('montoPagado',$this->montoPagado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    'id_Proveedor' => array(
                        'asc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia asc',
                        'desc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia desc',
                    ),
                    '*',
                ),

            )
		));
	}

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oProveedor');

        $criteria->compare('id',$this->id);
        $criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
        $criteria->compare('id_Proveedor',$this->id_Proveedor);
        if ($this->FechaFactura != null){
            $criteria->compare('FechaFactura',ComponentesComunes::fechaComparada($this->FechaFactura));
        }

        $criteria->compare('Clase',$this->Clase,true);
        $criteria->compare('Nro_Puesto_Vta',$this->Nro_Puesto_Vta,true);
        $criteria->compare('Nograv',$this->Nograv);
        $criteria->compare('TotalNeto',$this->TotalNeto);
        $criteria->compare('MesCarga',$this->MesCarga,true);
        $criteria->compare('Subtotal',$this->Subtotal);
        $criteria->compare('Descuento',$this->Descuento);
        $criteria->compare('Iva',$this->Iva);
        $criteria->compare('IngBrutos',$this->IngBrutos);
        $criteria->compare('Rg3337',$this->Rg3337);
        $criteria->compare('FechaCarga',$this->FechaCarga,true);
        $criteria->compare('id_Cuenta',$this->id_Cuenta);
        $criteria->compare('PercepIB',$this->PercepIB);
        $criteria->compare('ImpGanancias',$this->ImpGanancias);
        $criteria->compare('TipoComprobante',$this->TipoComprobante);
        $criteria->compare('Cai',$this->Cai,true);
        $criteria->compare('Fecha_Vencimiento',$this->Fecha_Vencimiento,true);
        $criteria->compare('Cant_alicuotas_iva',$this->Cant_alicuotas_iva);
        $criteria->compare('montoPagado',$this->montoPagado);

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        if($this->condImpaga == true || $this->condImpaga == 1){
            $criteria->addCondition('t.TotalNeto > t.montoPagado');
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => array(
                'defaultOrder' => 't.id desc',
                'attributes' => array(
                    'id_Proveedor' => array(
                        'asc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia asc',
                        'desc' => 'oProveedor.razonSocial,oProveedor.nbreFantasia desc',
                    ),
                    '*',
                ),

            )
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FacturaProveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
