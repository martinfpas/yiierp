<?php

/**
 * This is the model class for table "vDocumentosYPagos".
 *
 * The followings are the available columns in table 'vDocumentosYPagos':
 * @property string $id
 * @property string $tipo
 * @property integer $clId
 * @property string $razonSocial
 * @property double $saldoActual
 * @property integer $codViajante
 * @property string $fecha
 * @property double $debe
 * @property double $haber
 * @property integer $neId
 * @property integer $faId
 * @property integer $pcId
 * @property string $ncId
 * @property string $ndId
 */
class VDocumentosYPagos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vDocumentosYPagos2';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clId, codViajante, neId, faId, pcId', 'numerical', 'integerOnly'=>true),
			array('saldoActual, debe, haber', 'numerical'),
			array('id', 'length', 'max'=>36),
			array('tipo', 'length', 'max'=>2),
			array('razonSocial', 'length', 'max'=>40),
			array('fecha, ncId, ndId', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo, clId, razonSocial, saldoActual, codViajante, fecha, debe, haber, neId, faId, pcId, ncId, ndId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vdocumentos Ypagos',
            'models' => 'Vdocumentos Ypagoses',
			'id' => Yii::t('application', 'ID'),
			'tipo' => Yii::t('application', 'Tipo'),
			'clId' => Yii::t('application', 'Cl'),
			'razonSocial' => Yii::t('application', 'Razon Social'),
			'saldoActual' => Yii::t('application', 'Saldo Actual'),
			'codViajante' => Yii::t('application', 'Cod Viajante'),
			'fecha' => Yii::t('application', 'Fecha'),
			'debe' => Yii::t('application', 'Debe'),
			'haber' => Yii::t('application', 'Haber'),
			'neId' => Yii::t('application', 'Ne'),
			'faId' => Yii::t('application', 'Fa'),
			'pcId' => Yii::t('application', 'Pc'),
			'ncId' => Yii::t('application', 'Nc'),
			'ndId' => Yii::t('application', 'Nd'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('clId',$this->clId);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('saldoActual',$this->saldoActual);
		$criteria->compare('codViajante',$this->codViajante);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('debe',$this->debe);
		$criteria->compare('haber',$this->haber);
		$criteria->compare('neId',$this->neId);
		$criteria->compare('faId',$this->faId);
		$criteria->compare('pcId',$this->pcId);
		$criteria->compare('ncId',$this->ncId,true);
		$criteria->compare('ndId',$this->ndId,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
        $criteria->compare('tipo',$this->tipo,true);
        $criteria->compare('clId',$this->clId);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('saldoActual',$this->saldoActual);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('fecha',$this->fecha,true);
        $criteria->compare('debe',$this->debe);
        $criteria->compare('haber',$this->haber);
        $criteria->compare('neId',$this->neId);
        $criteria->compare('faId',$this->faId);
        $criteria->compare('pcId',$this->pcId);
        $criteria->compare('ncId',$this->ncId,true);
        $criteria->compare('ndId',$this->ndId,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    public function searchDeudores()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->addCondition('debe > haber ');
        $criteria->addCondition('fecha > 2020-05-01');

        $criteria->compare('id',$this->id,true);
        $criteria->compare('tipo',$this->tipo,true);
        $criteria->compare('clId',$this->clId);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('saldoActual',$this->saldoActual);
        $criteria->compare('codViajante',$this->codViajante);
        $criteria->compare('fecha',$this->fecha,true);
        $criteria->compare('debe',$this->debe);
        $criteria->compare('haber',$this->haber);
        $criteria->compare('neId',$this->neId);
        $criteria->compare('faId',$this->faId);
        $criteria->compare('pcId',$this->pcId);
        $criteria->compare('ncId',$this->ncId,true);
        $criteria->compare('ndId',$this->ndId,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VDocumentosYPagos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
