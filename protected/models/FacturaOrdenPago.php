<?php

/**
 * This is the model class for table "facturaOrdenPago".
 *
 * The followings are the available columns in table 'facturaOrdenPago':
 * @property integer $id
 * @property integer $idComprobanteProveedor
 * @property integer $idOrdeDePago
 *
 * The followings are the available model relations:
 * @property OrdenDePago $oOrdeDePago
 * @property ComprobanteProveedor $oComprobanteProveedor
 */
class FacturaOrdenPago extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'facturaOrdenPago';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idComprobanteProveedor, idOrdeDePago', 'required'),
			array('idComprobanteProveedor, idOrdeDePago', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idComprobanteProveedor, idOrdeDePago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oOrdeDePago' => array(self::BELONGS_TO, 'OrdenDePago', 'idOrdeDePago'),
			'oComprobanteProveedor' => array(self::BELONGS_TO, 'ComprobanteProveedor', 'idComprobanteProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Factura Orden Pago',
            'models' => 'Factura Orden Pagos',
			'id' => Yii::t('application', 'ID'),
			'idComprobanteProveedor' => Yii::t('application', 'Id Factura Proveedor'),
			'idOrdeDePago' => Yii::t('application', 'Id Orde De Pago'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idComprobanteProveedor',$this->idComprobanteProveedor);
		$criteria->compare('idOrdeDePago',$this->idOrdeDePago);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FacturaOrdenPago the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
