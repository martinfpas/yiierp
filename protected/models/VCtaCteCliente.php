<?php

/**
 * This is the model class for table "vCtaCteCliente".
 *
 * The followings are the available columns in table 'vCtaCteCliente':
 * @property string $id
 * @property string $tipo
 * @property integer $clId
 * @property string $razonSocial
 * @property string $fecha
 * @property double $debe
 * @property double $haber
 * @property integer $neId
 * @property integer $faId
 * @property integer $pcId
 * @property integer $ncId
 * @property integer $ndId
 */
class VCtaCteCliente extends CActiveRecord
{
    public $desde;
    public $hasta;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vCtaCteCliente';
	}

    public function primaryKey(){

        return 'id';

    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clId, neId, faId, pcId, ncId, ndId', 'numerical', 'integerOnly'=>true),
			array('debe, haber', 'numerical'),
            array('tipo', 'length', 'max'=>2),
			array('id', 'length', 'max'=>36),
			array('razonSocial', 'length', 'max'=>40),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo, clId, razonSocial, fecha, debe, haber, neId, faId, pcId, ncId, ndId,desde,hasta', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{

		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oNotaEntrega'  => array(self::BELONGS_TO, 'NotaEntrega', 'neId'),
            'oPagoCliente'  => array(self::BELONGS_TO, 'PagoCliente', 'pcId'),
            'oNotaCredito'  => array(self::BELONGS_TO, 'NotaCredito', 'ncId'),
            'oNotaDebito'   => array(self::BELONGS_TO, 'NotaDebito' , 'ndId'),
            'oFactura'      => array(self::BELONGS_TO, 'Factura'    , 'faId'),
		);

	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
            'tipo' => 'Tipo',
            'clId' => 'Cl',
			'razonSocial' => 'Razon Social',
			'fecha' => 'Fecha',
			'debe' => 'Debe',
			'haber' => 'Haber',
			'neId' => 'Ne',
			'faId' => 'Fa',
			'pcId' => 'Pc',
			'ncId' => 'Nc',
            'ndId' => 'Nd',
		);
	}


	public function getComprobante(){
	    if($this->faId > 0){
           return Comprobante::model()->findByPk($this->faId);
        }
        if($this->ncId > 0){
            return Comprobante::model()->findByPk($this->ncId);
        }
        if($this->ndId > 0){
            return Comprobante::model()->findByPk($this->ndId);
        }
        return NotaEntrega::model()->findByPk($this->neId);
    }

    public function getTitle(){
	    if ($this->tipo == 'PC'){
	        return $this->pcId;
        }else{
	        if ($this->getComprobante() != null){
                return $this->getComprobante()->FullTitle;
            }else{
	            echo 'Error de referencia';
            }
        }
    }

    public function getTipoFull(){
        switch ($this->tipo){
            case 'PC':
                return 'Pago';
                break;
            case 'NE':
                return 'Nota Entrega';
                break;
            case 'NC':
                return 'Nota Crédito';
                break;
            case 'ND':
                return 'Nota Débito';
                break;
              default:
                return $this->tipo;
        }
        if ($this->tipo == 'PC'){
            return $this->pcId;
        }else{
            if ($this->getComprobante() != null){
                return $this->getComprobante()->FullTitle;
            }else{
                echo 'Error de referencia';
            }
        }
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('clId',$this->clId);
		$criteria->compare('razonSocial',$this->razonSocial,true);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

        if ($this->desde != null) {
            $criteria->addCondition("t.fecha >= '".ComponentesComunes::jpicker2db($this->desde)." 00:00:00'");
        }

        if ($this->hasta != null) {
            $criteria->addCondition("t.fecha <= '".ComponentesComunes::jpicker2db($this->hasta,false)."  23:59:59'");
        }

		$criteria->compare('debe',$this->debe);
		$criteria->compare('haber',$this->haber);
		$criteria->compare('neId',$this->neId);
		$criteria->compare('faId',$this->faId);
		$criteria->compare('pcId',$this->pcId);
		$criteria->compare('ncId',$this->ncId);
        $criteria->compare('ndId',$this->ndId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'fecha desc,neId desc,faId desc,pcId desc',
            ),
		));
	}

		/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchWP()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('clId',$this->clId);
		$criteria->compare('razonSocial',$this->razonSocial,true);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

        if ($this->desde != null) {
            $criteria->addCondition("t.fecha >= '".ComponentesComunes::jpicker2db($this->desde)." 00:00:00'");
        }

        if ($this->hasta != null) {
            $criteria->addCondition("t.fecha <= '".ComponentesComunes::jpicker2db($this->hasta,false)."  23:59:59'");
        }

		$criteria->compare('debe',$this->debe);
		$criteria->compare('haber',$this->haber);
		$criteria->compare('neId',$this->neId);
		$criteria->compare('faId',$this->faId);
		$criteria->compare('pcId',$this->pcId);
		$criteria->compare('ncId',$this->ncId);
        $criteria->compare('ndId',$this->ndId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
            'sort' => array(
                'defaultOrder' => 'fecha desc',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VCtaCteCliente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
