<?php

/**
 * This is the model class for table "VPagosConChequeNulo".
 *
 * The followings are the available columns in table 'VPagosConChequeNulo':
 * @property string $id
 * @property string $monto
 * @property string $idChequedeTercero
 * @property string $idPagodeCliente
 */
class VPagosConChequeNulo extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vPagosConChequeNulo';
	}

	public function primaryKey(){

        return 'id';

    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, idChequedeTercero, idPagodeCliente', 'length', 'max'=>11),
			array('monto', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, monto, idChequedeTercero, idPagodeCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oChequedeTercero' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'idChequedeTercero'),
			'oPagodeCliente' => array(self::BELONGS_TO, 'PagoCliente', 'idPagodeCliente'),
		);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Vpagos Con Cheque',
            'models' => 'Vpagos Con Cheques',
			'id' => Yii::t('application', 'ID'),
			'monto' => Yii::t('application', 'Monto'),
			'idChequedeTercero' => Yii::t('application', 'Id Chequede Tercero'),
			'idPagodeCliente' => Yii::t('application', 'Id Pagode Cliente'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oChequedeTercero','oChequedeTercero.oSucursal');

		$criteria->compare('id',$this->id,true);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('idChequedeTercero',$this->idChequedeTercero,true);
		$criteria->compare('idPagodeCliente',$this->idPagodeCliente,true);

		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
	}

	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function searchWP()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oChequedeTercero','oChequedeTercero.oSucursal');

		$criteria->compare('t.id',$this->id,true);
		$criteria->compare('t.monto',$this->monto,true);
		$criteria->compare('t.idChequedeTercero',$this->idChequedeTercero,true);
		$criteria->compare('t.idPagodeCliente',$this->idPagodeCliente,true);
		
		return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
	}


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return VPagosConChequeNulo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
