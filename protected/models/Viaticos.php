<?php

/**
 * This is the model class for table "viaticos".
 *
 * The followings are the available columns in table 'viaticos':
 * @property integer $id
 * @property double $efectivo
 * @property string $fecha
 * @property integer $idNumeroViatico
 * @property double $bonos
 * @property integer $numeroViajante
 * @property double $tickets
 * @property double $cheques
 */
class Viaticos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Viaticos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viaticos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numeroViajante', 'required'),
			array('idNumeroViatico, numeroViajante', 'numerical', 'integerOnly'=>true),
			array('efectivo, bonos, tickets, cheques', 'numerical'),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, efectivo, fecha, idNumeroViatico, bonos, numeroViajante, tickets, cheques', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'efectivo' => Yii::t('application', 'Efectivo'),
		            'fecha' => Yii::t('application', 'Fecha'),
		            'idNumeroViatico' => Yii::t('application', 'Id Numero Viatico'),
		            'bonos' => Yii::t('application', 'Bonos'),
		            'numeroViajante' => Yii::t('application', 'Numero Viajante'),
		            'tickets' => Yii::t('application', 'Tickets'),
		            'cheques' => Yii::t('application', 'Cheques'),
		    );
	}

	public function getIdNumeroViatico(){
	    return $this->id;
    }

    public function setIdNumeroViatico($id){
        //return $this->id;

    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('efectivo',$this->efectivo);
        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }

		$criteria->compare('idNumeroViatico',$this->idNumeroViatico);
		$criteria->compare('bonos',$this->bonos);
		$criteria->compare('numeroViajante',$this->numeroViajante);
		$criteria->compare('tickets',$this->tickets);
		$criteria->compare('cheques',$this->cheques);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}