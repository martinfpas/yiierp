<?php

/* @variable $oLocalidad CodigoPostal*/

/**
 * This is the model class for table "factura".
 *
 * The followings are the available columns in table 'factura':
 * @property integer $id
 * @property string $tipo
 * @property string $clase
 * @property integer $Nro_Puesto_Venta
 * @property string $Nro_Comprobante
 * @property integer $Iva_Responsable
 * @property integer $Nro_Viajante
 * @property integer $Id_Cliente
 * @property string $razonSocial
 * @property string $direccion
 * @property string $localidad
 * @property string $provincia
 * @property string $rubro
 * @property string $zona
 * @property string $tipoResponsable
 * @property string $cuit
 * @property integer $perIIBB
 * @property string $nroInscripIIBB
 * @property string $fecha
 * @property string $Moneda
 * @property string $Observaciones
 * @property integer $Id_Camion
 * @property double $Descuento1
 * @property double $Descuento2
 * @property integer $Enviada
 * @property integer $Recibida
 * @property double $Otro_Iva
 * @property string $CAE
 * @property string $Vencim_CAE
 * @property integer $idNotaPedido
 * @property integer $idVoucherAfip
 * @property double $alicuotaIva
 * @property double $netoGravado
 * @property double $subtotalIva
 * @property double $alicuotaIIBB
 * @property double $subtotal
 * @property double $Ing_Brutos
 * @property double $Total_Neto
 * @property double $Total_Final
 * @property integer $estado
 * @property integer $idNotaDeCredito
 * @property double $montoPagado
 * @property integer $idPagoRelacionado
 * 
 * The followings are the available model relations:
 * @property Cliente $oCliente
 * @property Vehiculo $oCamion
 * @property PuntoDeVenta $oPuestoVenta
 * @property NotaPedido $oNotaPedido
 * @property AfipVoucher $oVoucherAfip
 * @property CategoriaIva $oIvaResponsable
 * @property NotaDebito[] $aNotaDebito
 * @property PagoCliente[] $aPagoCliente
 * @property ProdFactura[] $aProdFactura
 * @property PagoCliente $oPagoCliente
 */
class Factura extends Comprobante
{

    const borrador = 'Borrador';
    const guardada = 'Guardada';
    const cerrada = 'Cerrada';
    const archivada = 'Archivada';

    const iNoEnviada = 0;
    const iEnviada = 1;

    public static $aClase = array(
        'A' => 'A',
        'B' => 'B',
    );

    public static function alias(){
        return __CLASS__;
    }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Factura the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobante';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('clase, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion', 'required'),
            array('Nro_Puesto_Venta, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion, idNotaPedido, idVoucherAfip', 'numerical', 'integerOnly'=>true),
            array('Descuento1, Descuento2, Otro_Iva, alicuotaIva, netoGravado, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final', 'numerical'),
            array('tipo', 'length', 'max'=>2),
            array('clase', 'length', 'max'=>1),
            array('Nro_Comprobante, Moneda', 'length', 'max'=>8),
            array('razonSocial, direccion', 'length', 'max'=>50),
            array('localidad', 'length', 'max'=>65),
            array('provincia', 'length', 'max'=>40),
            array('rubro', 'length', 'max'=>35),
            array('zona', 'length', 'max'=>4),
            array('tipoResponsable, nroInscripIIBB', 'length', 'max'=>30),
            array('cuit', 'length', 'max'=>13),
            array('CAE', 'length', 'max'=>14),
            array('Enviada, Recibida','boolean'),
            array('id,fecha, Observaciones, Vencim_CAE, condicion, estado, idNotaDeCredito,montoPagado,idPagoRelacionado', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, tipo, clase, Nro_Puesto_Venta, Nro_Comprobante, Iva_Responsable, Nro_Viajante, Id_Cliente, razonSocial, direccion, localidad, provincia, rubro, zona, tipoResponsable, cuit, perIIBB, nroInscripIIBB, fecha, Moneda, Observaciones, Id_Camion, Descuento1, Descuento2, Enviada, Recibida, Otro_Iva, CAE, Vencim_CAE, idNotaPedido, idVoucherAfip, alicuotaIva, netoGravado, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final, idNotaDeCredito, estado', 'safe', 'on'=>'search'),
        );
    }




    /**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'oViajante' => array(self::BELONGS_TO, 'Viajante', 'Nro_Viajante'),
		    'oCamion' => array(self::BELONGS_TO, 'Vehiculo', 'Id_Camion'),
            'oNotaPedido' => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
            'oPuestoVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'Nro_Puesto_Venta'),
            'oCliente' => array(self::BELONGS_TO, 'Cliente', 'Id_Cliente'),
            'oIvaResponsable' => array(self::BELONGS_TO, 'CategoriaIva', 'Iva_Responsable'),
            'oVoucherAfip' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucherAfip'),
            'aNotaDebito' => array(self::HAS_MANY, 'NotaDebito', 'id_factura'),
            'aProdFactura' => array(self::HAS_MANY, 'ProdFactura', 'idComprobante'),
            'oPagoCliente' => array(self::HAS_ONE, 'PagoCliente', 'id_factura'),
            'aProdComprobante' => array(self::HAS_MANY, 'ProdFactura', 'idComprobante'),
            'oNotaDeCredito' => array(self::BELONGS_TO,'NotaDeCredito','idNotaDeCredito'),
		);
	}



    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'model' => 'Factura',
            'models' => 'Facturas',
            'id' => 'ID',
            'tipo' => 'Tipo',
            'clase' => 'Clase',
            'Nro_Puesto_Venta' => 'Nro Puesto Venta',
            'Nro_Comprobante' => 'Nro Comprobante',
            'Iva_Responsable' => 'Iva Responsable',
            'Nro_Viajante' => 'Nro Viajante',
            'Id_Cliente' => 'Id Cliente',
            'razonSocial' => 'Razon Social',
            'direccion' => 'Direccion',
            'localidad' => 'Localidad',
            'provincia' => 'Provincia',
            'rubro' => 'Rubro',
            'zona' => 'Zona',
            'tipoResponsable' => 'Tipo Responsable',
            'cuit' => 'Cuit',
            'perIIBB' => 'Per Iibb',
            'nroInscripIIBB' => 'Nro Inscrip Iibb',
            'fecha' => 'Fecha',
            'Moneda' => 'Moneda',
            'Observaciones' => 'Observaciones',
            'Id_Camion' => 'Camion',
            'Descuento1' => 'Descuento1',
            'Descuento2' => 'Descuento2',
            'Enviada' => 'Enviada',
            'Recibida' => 'Recibida',
            'Otro_Iva' => 'Otro Iva',
            'CAE' => 'Cae',
            'Vencim_CAE' => 'Vencim Cae',
            'idNotaPedido' => 'Nota Pedido',
            'idVoucherAfip' => 'Voucher Afip',
            'alicuotaIva' => 'Alicuota Iva',
            'netoGravado' => 'Neto Gravado',
            'subtotalIva' => 'Subtotal Iva',
            'alicuotaIIBB' => 'Alicuota Iibb',
            'subtotal' => 'Subtotal',
            'Ing_Brutos' => 'Ing Brutos',
            'Total_Neto' => 'Total Neto',
            'Total_Final' => 'Total Final',
            'condicion' => 'Condición',
        );
    }

    public function defaultScope()
    {
        return array(
            'alias' => self::alias(),
            'condition' => 'tipo = "F"',
        );

    }

	public function scopes(){
	    return array(
	        'noEnviadas' => array(
	            'condition' => 'Enviada = 0'
            ),
        );
    }


    public function crearNotaCredito(){
	    // TODO: BEGNIG TRANSACTION
        $transaction=Yii::app()->db->beginTransaction();

    	$oNotaCredito = new NotaCredito();
    	$oNotaCredito->clase = $this->clase;
    	$oNotaCredito->Nro_Puesto_Venta = $this->Nro_Puesto_Venta;
    	$oNotaCredito->Iva_Responsable = $this->Iva_Responsable;
        $oNotaCredito->Nro_Viajante = $this->Nro_Viajante;
        $oNotaCredito->Id_Cliente = $this->Id_Cliente;
        $oNotaCredito->fecha = date("Y-m-d H:i:s");
        $oNotaCredito->Id_Camion = $this->Id_Camion;
        $oNotaCredito->Total_Final = $this->Total_Final;
        $oNotaCredito->Enviada = 0;
        $oNotaCredito->Recibida = 0;
        $oNotaCredito->Observaciones = $this->Observaciones;
        $oNotaCredito->Otro_Iva = $this->Otro_Iva;
        $oNotaCredito->Descuento1 = $this->Descuento1;
        $oNotaCredito->Descuento2 = $this->Descuento2;
        $oNotaCredito->Total_Neto = $this->Total_Neto;
        $oNotaCredito->CAE = 0;
        $oNotaCredito->Vencim_CAE = null;
        $oNotaCredito->Ing_Brutos = $this->Ing_Brutos;
        $oNotaCredito->idFactura = $this->id;
        $oNotaCredito->Nro_Comprobante = $oNotaCredito->getNextNumber();

        if ($oNotaCredito->save()){
            $oNotaCredito->cloneFacturaItem($this->id);

            $this->idNotaDeCredito = $oNotaCredito->id;
            if (!$this->save($this->save())){
                Yii::log('Error al guardar el idNotaDeCredito asociado:'.CHtml::errorSummary($this),'error');
            }
            $transaction->commit();

        }else{
            Yii::log('Error al guardar NC'.CHtml::errorSummary($oNotaCredito),'error');
            $transaction->rollback();
        }

    	return $oNotaCredito;
    }




    /*
 * GENERAR Y DEVOLVER UNA Factura EN BASE A UNA NOTA DE PEDIDO
 */
    public static function generarFa($idNotaPedido = null,$idCliente = null){
        if ($idNotaPedido > 0) {

            $oNotaPedido = NotaPedido::model()->with('oFactura')->findByPk($idNotaPedido);
            if ($oNotaPedido <> null) {

                if ($oNotaPedido->oFactura <> null) {

                    return $oNotaPedido->oFactura;
                }
            } else {
                Yii::log('$oNotaPedido == null :: ', 'warning');
            }
            if ($oNotaPedido <> null) {
                $oFactura = new Factura();
                $oFactura->clase = $oNotaPedido->oCliente->oCategoriaIva->tipoFact;
                $oFactura->Nro_Viajante = $oNotaPedido->oCliente->codViajante;
                $oFactura->Nro_Comprobante = '';
                $oFactura->Iva_Responsable = $oNotaPedido->oCliente->oCategoriaIva->id_categoria;
                $oFactura->Nro_Viajante = $oNotaPedido->oCliente->codViajante;
                $oFactura->Id_Cliente = $oNotaPedido->oCliente->id;
                $oFactura->fecha = date("Y-m-d H:i:s");
                $oFactura->Total_Neto = 0;
                $oFactura->Id_Camion = $oNotaPedido->camion;
                $oFactura->Total_Final = 0;
                $oFactura->Id_Cliente = $oNotaPedido->idCliente;
                $oFactura->idNotaPedido = $idNotaPedido;
                $oFactura->alicuotaIva = $oNotaPedido->oCliente->oCategoriaIva->alicuota;
                //netoGravado

                $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $oNotaPedido->oCliente->cp, 'zp' => $oNotaPedido->oCliente->zp));
                if ($oLocalidad != null && $oLocalidad->id_provincia != 'E') {
                    $oFactura->Nro_Puesto_Venta = 9;
                } else {
                    $oFactura->Nro_Puesto_Venta = 10;
                }

                if ($oFactura->save()) {
                    $oFactura->clonarProductos();
                    $oFactura->Total_Final = $oFactura->Total_Final;
                    if (!$oFactura->save()) {
                        Yii::log('$oFactura->save() al guardar el TotalFinal:: ' . CHtml::errorSummary($oFactura), 'warning');
                    } else {
                        Yii::log('$oFactura->save() ??????????????? ', 'warning');
                    }
                    return $oFactura;
                } else {
                    Yii::log('$oFactura->save() :: ' . CHtml::errorSummary($oFactura), 'error');
                    return null;
                }

            } else {
                Yii::log('$oNotaPedido no encontrada', 'error');
                //TODO: TIRAR EXCEPCION
                return null;
            }
        }elseif ($idCliente > 0){

            $oCliente = Cliente::model()->findByPk($idCliente);

            if($oCliente != null){
                $oFactura = new Factura();
                $oFactura->clase = $oCliente->oCategoriaIva->tipoFact;
                $oFactura->Nro_Viajante = $oCliente->codViajante;
                $oFactura->Nro_Comprobante = '';
                $oFactura->Iva_Responsable = $oCliente->oCategoriaIva->id_categoria;
                $oFactura->Nro_Viajante = $oCliente->codViajante;
                $oFactura->Id_Cliente = $oCliente->id;
                $oFactura->fecha = date("Y-m-d H:i:s");
                $oFactura->Total_Neto = 0;
                $oFactura->Id_Camion = 6;
                $oFactura->Total_Final = 0;
                $oFactura->Id_Cliente = $idCliente;
                $oFactura->alicuotaIva = $oCliente->oCategoriaIva->alicuota;

                Yii::log('$oCliente->oCategoriaIva->alicuota :: ' . $oCliente->oCategoriaIva->alicuota, 'warning');

                $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal' => $oCliente->cp, 'zp' => $oCliente->zp));
                if ($oLocalidad != null && $oLocalidad->id_provincia != 'E') {
                    $oFactura->Nro_Puesto_Venta = 9;
                } else {
                    $oFactura->Nro_Puesto_Venta = 10;
                }

                if ($oFactura->save()) {
                    return $oFactura;
                } else {
                    Yii::log('$oFactura->save() :: ' . CHtml::errorSummary($oFactura), 'error');
                    return null;
                }

            }else{
                Yii::log('$idCliente no es mayor a 0 o es invalido','error');
            }
        }else{
            Yii::log('$idNotaPedido no es mayor a 0','error');
            //TODO: TIRAR EXCEPCION
            return null;
        }

    }

    /**
     * Toma la lista de productos de la nota de pedido asociada y clona los productos.
     *
     */
    public function clonarProductos(){
        if ($this->oNotaPedido <> null){
            $i=1;
            foreach ($this->oNotaPedido->aProdNotaPedido as $index => $oProdNotaPedido) {
                Yii::log('$oProdNotaPedido->id :'.$oProdNotaPedido->id,'warning');
                $oProdFa = new ProdFactura();
                $oProdFa->idComprobante = $this->id;
                $oProdFa->idArtVenta = $oProdNotaPedido->idArtVenta;
                $oProdFa->cantidad = $oProdNotaPedido->cantidad;
                                                                                                             // AL TENER UN IVA POR FACTURA, SE TOMA LA ALICUOTA DE AHI.
                $oProdFa->precioUnitario = ($this->oIvaResponsable->agregaIva)? ($oProdNotaPedido->precioUnit * (1+$this->alicuotaIva/100) ) :$oProdNotaPedido->precioUnit;
                $oProdFa->cantidadEntregada = 0;
                $oProdFa->nroItem = $i;
                $oProdFa->nroPuestoVenta = $this->Nro_Puesto_Venta;
                $oProdFa->descripcion = $oProdNotaPedido->oArtVenta->descripcion;
                if(!$oProdFa->save()){
                    Yii::log('$oFactura->save() :: '.CHtml::errorSummary($oProdFa),'warning');
                    throw new Exception(CHtml::errorSummary($oProdFa));
                };
                $i++;
            }
        }
    }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        //TODO: AGREGAR LA CONDICION PARA TIPO F

        $criteria->compare('id',$this->id);
        $criteria->compare('tipo',$this->tipo,true);
        $criteria->compare('clase',$this->clase,true);
        $criteria->compare('Nro_Puesto_Venta',$this->Nro_Puesto_Venta);
        $criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
        $criteria->compare('Iva_Responsable',$this->Iva_Responsable);
        $criteria->compare('Nro_Viajante',$this->Nro_Viajante);
        $criteria->compare('Id_Cliente',$this->Id_Cliente);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('localidad',$this->localidad,true);
        $criteria->compare('provincia',$this->provincia,true);
        $criteria->compare('rubro',$this->rubro,true);
        $criteria->compare('zona',$this->zona,true);
        $criteria->compare('tipoResponsable',$this->tipoResponsable,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('perIIBB',$this->perIIBB);
        $criteria->compare('nroInscripIIBB',$this->nroInscripIIBB,true);
        $criteria->compare('Moneda',$this->Moneda,true);
        $criteria->compare('Observaciones',$this->Observaciones,true);
        $criteria->compare('Id_Camion',$this->Id_Camion);
        $criteria->compare('Descuento1',$this->Descuento1);
        $criteria->compare('Descuento2',$this->Descuento2);
        $criteria->compare('Enviada',$this->Enviada);
        $criteria->compare('Recibida',$this->Recibida);
        $criteria->compare('Otro_Iva',$this->Otro_Iva);
        $criteria->compare('CAE',$this->CAE,true);
        $criteria->compare('idNotaPedido',$this->idNotaPedido);
        $criteria->compare('idVoucherAfip',$this->idVoucherAfip);
        $criteria->compare('alicuotaIva',$this->alicuotaIva);
        $criteria->compare('subtotalIva',$this->subtotalIva);
        $criteria->compare('alicuotaIIBB',$this->alicuotaIIBB);
        $criteria->compare('subtotal',$this->subtotal);
        $criteria->compare('Ing_Brutos',$this->Ing_Brutos);
        $criteria->compare('Total_Neto',$this->Total_Neto);
        $criteria->compare('Total_Final',$this->Total_Final);

        if ($this->fecha != null){
            $criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }
        if ($this->Vencim_CAE != null){
            $criteria->compare('Vencim_CAE',ComponentesComunes::fechaComparada($this->Vencim_CAE));
        }


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => self::alias().'.id desc',

            ),
		));
	}

	public function beforeDelete(){
        if($this->CAE != '00000000000000' && $this->CAE != '' || ($this->oVoucherAfip != null && $this->oVoucherAfip->caea != '00000000000000' && $this->oVoucherAfip->caea != '')){
            Yii::log('No se puede borrar una factura que esta aprobada '.print_r($this,true),'error');
            return false;
        }

        $transaction = Yii::app()->db->getCurrentTransaction();
        if($transaction == null){
            $transaction=Yii::app()->db->beginTransaction();
        }

        $oEstadoAnterior = self::model()->findByPk($this->id);

        $bReturn = parent::beforeDelete();
        if ($bReturn && !$oEstadoAnterior->esElectronica()){
            $oEstadoAnterior->oCliente->saldoActual = $oEstadoAnterior->oCliente->saldoActual - $oEstadoAnterior->Total_Final;
            if (!$oEstadoAnterior->oCliente->save()){
                Yii::log('$oEstadoAnterior->oCliente '.CHtml::errorSummary($oEstadoAnterior->oCliente),'error');
                $transaction->rollback();
                return false;
            }
        }else{
            if(!$bReturn){
                Yii::log('retornó falso :'.$bReturn,'error');
                $transaction->rollback();
                return false;
            }/* else{
                Yii::log('Es electronica:'.$oEstadoAnterior->esElectronica(),'error');
            }*/
        }

        $transaction->commit();
        return $bReturn;
    }

    public function beforeSave()
    {
        /* @var $model Comprobante */
        $this->tipo = 'F';
        if ($this->estado >= Comprobante::iCerrada){
            $model = Comprobante::model()->findByPk($this->id);
            if($model != null){
                if($model->netoGravado != $this->netoGravado || $model->Id_Camion != $this->Id_Camion || $model->estado != $this->estado || $model->Recibida != $this->Recibida || $model->idNotaDeCredito != $this->idNotaDeCredito || $model->montoPagado != $this->montoPagado || $model->idNotaPedido != $this->idNotaPedido || $model->idPagoRelacionado != $this->idPagoRelacionado){
                    return parent::beforeSave();
                }
            }
            //TODO: VER SI NO DEVOLVEMOS UNA EXCEPCION...
            $this->addError('estado', 'La factura #'.$this->id.' Recibida -'.$this->Recibida.'- ya esta cerrada, no se puede modificar ');
            return false;
        }
        return parent::beforeSave();
    }

}