<?php

/**
 * This is the model class for table "pagoProvMovimientoBanc".
 *
 * The followings are the available columns in table 'pagoProvMovimientoBanc':
 * @property integer $id
 * @property integer $idMovBanc
 * @property integer $idPagoProveedor
 * @property double $monto
 *
 * The followings are the available model relations:
 * @property MovimientoCuentasBanc $oMovBanc
 * @property PagoProveedor $oPagoProveedor
 */
class PagoProvMovimientoBanc extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoProvMovimientoBanc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idMovBanc, idPagoProveedor', 'required'),
			array('idMovBanc, idPagoProveedor', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idMovBanc, idPagoProveedor, monto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oMovBanc' => array(self::BELONGS_TO, 'MovimientoCuentasBanc', 'idMovBanc'),
			'oPagoProveedor' => array(self::BELONGS_TO, 'PagoProveedor', 'idPagoProveedor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Pago Prov Movimiento Banc',
            'models' => 'Pago Prov Movimiento Bancs',
			'id' => Yii::t('application', 'ID'),
			'idMovBanc' => Yii::t('application', 'Id Mov Banc'),
			'idPagoProveedor' => Yii::t('application', 'Id Pago Proveedor'),
			'monto' => Yii::t('application', 'Monto'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idMovBanc',$this->idMovBanc);
		$criteria->compare('idPagoProveedor',$this->idPagoProveedor);
		$criteria->compare('monto',$this->monto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagoProvMovimientoBanc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
