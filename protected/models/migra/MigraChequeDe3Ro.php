<?php

/**
 * This is the model class for table "migra_chequeDe3Ro".
 *
 * The followings are the available columns in table 'migra_chequeDe3Ro':
 * @property integer $id
 * @property integer $nroCheque
 * @property integer $id_banco
 * @property integer $id_sucursal
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros $oId
 */
class MigraChequeDe3Ro extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MigraChequeDe3Ro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'migra_chequeDe3Ro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, nroCheque, id_banco, id_sucursal', 'required'),
			array('id, nroCheque, id_banco, id_sucursal', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, nroCheque, id_banco, id_sucursal', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oId' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'nroCheque' => Yii::t('application', 'Nro Cheque'),
		            'id_banco' => Yii::t('application', 'Id Banco'),
		            'id_sucursal' => Yii::t('application', 'Id Sucursal'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nroCheque',$this->nroCheque);
		$criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('id_sucursal',$this->id_sucursal);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}