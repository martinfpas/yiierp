<?php

/**
 * This is the model class for table "migra_sucursal".
 *
 * The followings are the available columns in table 'migra_sucursal':
 * @property integer $id
 * @property integer $id_sucursal
 * @property integer $id_banco
 *
 * The followings are the available model relations:
 * @property SucursalBanco $oId
 */
class MigraSucursal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MigraSucursal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'migra_sucursal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, id_sucursal, id_banco', 'required'),
			array('id, id_sucursal, id_banco', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_sucursal, id_banco', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oId' => array(self::BELONGS_TO, 'SucursalBanco', 'id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'id_sucursal' => Yii::t('application', 'Id Sucursal'),
		            'id_banco' => Yii::t('application', 'Id Banco'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_sucursal',$this->id_sucursal);
		$criteria->compare('id_banco',$this->id_banco);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}