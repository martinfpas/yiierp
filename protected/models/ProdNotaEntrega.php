<?php

/**
 * This is the model class for table "prodNotaEntrega".
 *
 * The followings are the available columns in table 'prodNotaEntrega':
 * @property integer $id
 * @property integer $nroItem
 * @property integer $idNotaEntrega
 * @property integer $idArtVenta
 * @property double $precioUnitario
 * @property double $cantidad
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property NotaEntrega $oNotaEntrega
 * @property ArticuloVenta $oArtVenta
 */
class ProdNotaEntrega extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prodNotaEntrega';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idNotaEntrega, idArtVenta, precioUnitario, cantidad, descripcion', 'required'),
			array('nroItem, idNotaEntrega, idArtVenta', 'numerical', 'integerOnly'=>true),
			array('precioUnitario, cantidad', 'numerical'),
			array('descripcion', 'length', 'max'=>70),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nroItem, idNotaEntrega, idArtVenta, precioUnitario, cantidad, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oNotaEntrega' => array(self::BELONGS_TO, 'NotaEntrega', 'idNotaEntrega'),
			'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'models' => 'Productos De Nota de Entrega',
            'model' => 'Producto De Nota de Entrega',
			'id' => 'ID',
			'nroItem' => 'Nro Item',
			'idNotaEntrega' => 'Id Nota Entrega',
			'idArtVenta' => 'Id Art Venta',
			'precioUnitario' => 'Precio Unitario',
			'cantidad' => 'Cantidad',
			'descripcion' => 'Descripcion',
		);
	}

	public function getImporte(){
	    return number_format((floatval(str_replace(',','',$this->precioUnitario)) * $this->cantidad),2, '.', '');
    }

    public function getPrecioUnitario(){
        return number_format(floatval(str_replace(',','',$this->precioUnitario)),2, '.', '');
    }

    public function getidComprobante(){
        return $this->idNotaEntrega;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nroItem',$this->nroItem);
		$criteria->compare('idNotaEntrega',$this->idNotaEntrega);
		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('precioUnitario',$this->precioUnitario);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => false,
		));
	}
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('nroItem',$this->nroItem);
        $criteria->compare('idNotaEntrega',$this->idNotaEntrega);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('precioUnitario',$this->precioUnitario);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('descripcion',$this->descripcion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort' => false,
        ));
    }

    public function afterSave(){
        try{
            $this->oNotaEntrega->recalcular();
        }catch (Exception $e){
            //TODO: VER QUE HACEMOS
            Yii::log('Message: '.$e->getMessage(),'error');
        }
        return parent::afterSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProdNotaEntrega the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
