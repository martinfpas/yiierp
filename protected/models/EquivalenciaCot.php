<?php

/**
 * This is the model class for table "equivalenciaCot".
 *
 * The followings are the available columns in table 'equivalenciaCot':
 * @property integer $id
 * @property integer $idArticuloVenta
 * @property string $codCot
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property ArticuloVenta $oArticuloVenta
 */
class EquivalenciaCot extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'equivalenciaCot';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idArticuloVenta, codCot, descripcion', 'required'),
			array('idArticuloVenta', 'numerical', 'integerOnly'=>true),
			array('codCot', 'length', 'max'=>6),
			array('descripcion', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idArticuloVenta, codCot, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oArticuloVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArticuloVenta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'idArticuloVenta' => 'Id Articulo Venta',
			'codCot' => 'Cod Cot',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idArticuloVenta',$this->idArticuloVenta);
		$criteria->compare('codCot',$this->codCot,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return EquivalenciaCot the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
