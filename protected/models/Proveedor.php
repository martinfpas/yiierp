<?php

/**
 * This is the model class for table "proveedor".
 *
 * The followings are the available columns in table 'proveedor':
 * @property integer $id
 * @property string $nbreFantasia
 * @property string $contacto
 * @property string $razonSocial
 * @property string $cuit
 * @property integer $ivaResponsable
 * @property string $direccion
 * @property string $pisoDto
 * @property string $email
 * @property string $cp
 * @property string $zp
 * @property string $tel
 * @property string $fax
 * @property string $obs
 * @property double $saldo
 * @property integer $idTipo
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros[] $aChequeDeTerceros
 * @property ComprobanteProveedor[] $aComprobanteProveedor
 * @property MateriaPrima[] $aMateriaPrima
 * @property CategoriaIva $oIvaResponsable
 */
class Proveedor extends CActiveRecord
{
    public $atencionSobre = '';
    public $sLocalidad = '';
    public $sProvincia = '';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'proveedor';
    }

    /**
     * @return array validation rules for model attributes.

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, nbreFantasia, razonSocial, ivaResponsable', 'required'),
            array('id, ivaResponsable, idTipo', 'numerical', 'integerOnly' => true),
            array('saldo', 'numerical'),
            array('nbreFantasia', 'length', 'max' => 50),
            array('razonSocial, direccion', 'length', 'max' => 60),
            array('contacto,email', 'length', 'max'=>80),
            array('cuit', 'length', 'max' => 11),
            array('pisoDto', 'length', 'max' => 8),
            array('cp, zp', 'length', 'max' => 4),
            array('tel,fax', 'length', 'max' => 35),
            array('Fecha_Vencimiento' => 'save'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nbreFantasia, contacto, razonSocial, cuit, ivaResponsable, direccion, pisoDto, email, cp, zp, tel, fax, obs, saldo, idTipo', 'safe', 'on' => 'search'),
        );
    }
    */
    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('id, nbreFantasia, razonSocial, ivaResponsable', 'required'),
            array('id, ivaResponsable, idTipo', 'numerical', 'integerOnly'=>true),
            array('saldo', 'numerical'),
            array('nbreFantasia', 'length', 'max'=>50),
            array('contacto, email', 'length', 'max'=>80),
            array('razonSocial, direccion', 'length', 'max'=>60),
            array('cuit', 'length', 'max'=>11),
            array('pisoDto', 'length', 'max'=>8),
            array('cp, zp', 'length', 'max'=>4),
            array('tel, fax', 'length', 'max'=>35),
            array('obs', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nbreFantasia, contacto, razonSocial, cuit, ivaResponsable, direccion, pisoDto, email, cp, zp, tel, fax, obs, saldo, idTipo', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'aChequeDeTerceros' => array(self::HAS_MANY, 'ChequeDeTerceros', 'idProveedor'),
            'aComprobanteProveedor' => array(self::HAS_MANY, 'ComprobanteProveedor', 'id_Proveedor'),
            'aMateriaPrima' => array(self::HAS_MANY, 'MateriaPrima', 'Id_Proveedor'),
            'oIvaResponsable' => array(self::BELONGS_TO, 'CategoriaIva', 'ivaResponsable'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'nbreFantasia' => 'Nbre Fantasia',
            'contacto' => 'Contacto',
            'razonSocial' => 'Razon Social',
            'cuit' => 'Cuit',
            'ivaResponsable' => 'Iva Responsable',
            'direccion' => 'Direccion',
            'pisoDto' => 'Piso Dto',
            'email' => 'Email',
            'cp' => 'Cp',
            'zp' => 'Zp',
            'tel' => 'Tel',
            'fax' => 'Fax',
            'obs' => 'Obs',
            'saldo' => 'Saldo',
            'idTipo' => 'Id Tipo',
            'Title' => 'Proveedor',
            'atencionSobre' => 'Atencion Sr.//a.',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('nbreFantasia', $this->nbreFantasia, true);
        $criteria->compare('razonSocial', $this->razonSocial, true);
        $criteria->compare('cuit', $this->cuit, true);
        $criteria->compare('ivaResponsable', $this->ivaResponsable);
        $criteria->compare('direccion', $this->direccion, true);
        $criteria->compare('pisoDto', $this->pisoDto, true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('cp', $this->cp, true);
        $criteria->compare('zp', $this->zp, true);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('fax', $this->fax, true);
        $criteria->compare('obs',$this->obs,true);
        $criteria->compare('saldo',$this->saldo);
        $criteria->compare('idTipo',$this->idTipo);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getTitle(){
        if ($this->razonSocial == ''){
            return $this->nombreFantasia;
        }else{
            return $this->razonSocial;
        }
    }

    public function getLocalidad() {
        $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal'=>$this->cp,'zp' => $this->zp));
        $sLocalidad = '';
        if($oLocalidad != NULL){
            $sLocalidad = $oLocalidad->nombrelocalidad;
        }
        return $sLocalidad;
    }

    public function getProvincia() {
        $oCodigoPostal = CodigoPostal::model()->findByAttributes(array('codigoPostal'=>$this->cp,'zp' => $this->zp));
        $sProvincia = '';
        if($oCodigoPostal != NULL){
            $sProvincia = $oCodigoPostal->oProvincia->nombre;
        }
        return $sProvincia;
    }

    public function getLocalidadYProv() {
        /* @var $oLocalidad CodigoPostal*/
        $oLocalidad = CodigoPostal::model()->findByAttributes(array('codigoPostal'=>$this->cp,'zp' => $this->zp));
        $sLocalidad = '';
        if($oLocalidad != NULL){
            $sLocalidad = $oLocalidad->getIdTituloYProv();
        }
        return $sLocalidad;
    }

    public function searchWP()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('nbreFantasia',$this->nbreFantasia,true);
        $criteria->compare('razonSocial',$this->razonSocial,true);
        $criteria->compare('cuit',$this->cuit,true);
        $criteria->compare('ivaResponsable',$this->ivaResponsable);
        $criteria->compare('direccion',$this->direccion,true);
        $criteria->compare('pisoDto',$this->pisoDto,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('cp',$this->cp,true);
        $criteria->compare('zp',$this->zp,true);
        $criteria->compare('tel',$this->tel,true);
        $criteria->compare('fax',$this->fax,true);
        $criteria->compare('obs',$this->obs,true);
        $criteria->compare('saldo',$this->saldo);
        $criteria->compare('idTipo',$this->idTipo);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    public function scopes()
    {
        return array(
            'todos'=>array(
                'order'=>'razonSocial asc',
            ),
        );
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Proveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
