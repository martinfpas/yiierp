<?php

/**
 * This is the model class for table "alicuotasCompra".
 *
 * The followings are the available columns in table 'alicuotasCompra':
 * @property integer $id
 * @property integer $idComprobanteCompra
 * @property double $impNetoGravado
 * @property string $alicuota
 * @property double $impuestoLiquidado
 *
 * The followings are the available model relations:
 * @property ComprobanteProveedor $oIdComprobanteCompra
 */
class AlicuotasCompra extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alicuotasCompra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idComprobanteCompra, impNetoGravado, alicuota, impuestoLiquidado', 'required'),
			array('idComprobanteCompra', 'numerical', 'integerOnly'=>true),
			array('impNetoGravado, impuestoLiquidado', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idComprobanteCompra, impNetoGravado, alicuota, impuestoLiquidado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oIdComprobanteCompra' => array(self::BELONGS_TO, 'ComprobanteProveedor', 'idComprobanteCompra'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Alicuotas Compra',
            'models' => 'Alicuotas Compras',
			'id' => Yii::t('application', 'ID'),
			'idComprobanteCompra' => Yii::t('application', 'Id Comprobante Compra'),
			'impNetoGravado' => Yii::t('application', 'Importe Neto Gravado'),
			'alicuota' => Yii::t('application', 'Alicuota'),
			'impuestoLiquidado' => Yii::t('application', 'Impuesto Liquidado'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idComprobanteCompra',$this->idComprobanteCompra);
		$criteria->compare('impNetoGravado',$this->impNetoGravado);
		$criteria->compare('alicuota',$this->alicuota,true);
		$criteria->compare('impuestoLiquidado',$this->impuestoLiquidado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AlicuotasCompra the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
