<?php

/**
 * This is the model class for table "pagoAlContado".
 *
 * The followings are the available columns in table 'pagoAlContado':
 * @property integer $id
 * @property double $monto
 * @property integer $idPagodeCliente
 *
 * The followings are the available model relations:
 * @property PagoCliente $oPagodeCliente
 */
class PagoAlContado extends CActiveRecord
{
    public $recalcOnDelete = true;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PagoAlContado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pagoAlContado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('monto, idPagodeCliente', 'required'),
			array('idPagodeCliente', 'numerical', 'integerOnly'=>true),
            array('monto', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, monto, idPagodeCliente', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oPagodeCliente' => array(self::BELONGS_TO, 'PagoCliente', 'idPagodeCliente'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
                'id' => Yii::t('application', 'ID'),
                'monto' => 'Monto',
                'idPagodeCliente' => Yii::t('application', 'Id Pagode Clliente'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->with = array('oPagodeCliente');

		$criteria->compare('id',$this->id);
        $criteria->compare('monto',$this->monto);
		$criteria->compare('idPagodeCliente',$this->idPagodeCliente);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function afterSave()
    {
		$this->oPagodeCliente->recalcular();
		Yii::log('$this->$this->$this-> '.ComponentesComunes::print_array($this->attributes,true),'warning');
        return parent::afterSave();
    }

    public function afterDelete(){
        $oPagoCliente = $this->oPagodeCliente;
        $monto = $this->monto;
        $bool = parent::afterDelete();

        if ($this->recalcOnDelete){
            $oPagoCliente->recalcular();
        }

        return $bool;
    }



}