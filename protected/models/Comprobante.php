<?php

/**
 * This is the model class for table "comprobante".
 *
 * The followings are the available columns in table 'comprobante':
 * @property integer $id
 * @property string $tipo
 * @property string $clase
 * @property integer $Nro_Puesto_Venta
 * @property string $Nro_Comprobante
 * @property integer $Iva_Responsable
 * @property integer $Nro_Viajante
 * @property integer $Id_Cliente
 * @property string $razonSocial
 * @property string $direccion
 * @property string $localidad
 * @property string $provincia
 * @property string $rubro
 * @property string $zona
 * @property string $tipoResponsable
 * @property string $cuit
 * @property double $perIIBB
 * @property string $nroInscripIIBB
 * @property string $fecha
 * @property string $Moneda
 * @property string $Observaciones
 * @property integer $Id_Camion
 * @property double $Descuento1
 * @property double $Descuento2
 * @property integer $Enviada
 * @property integer $Recibida
 * @property double $Otro_Iva
 * @property string $CAE
 * @property string $Vencim_CAE
 * @property integer $idNotaPedido
 * @property integer $idVoucherAfip
 * @property double $alicuotaIva
 * @property double $netoGravado
 * @property double $subtotalIva
 * @property double $alicuotaIIBB
 * @property double $subtotal
 * @property double $Ing_Brutos
 * @property double $Total_Neto
 * @property double $Total_Final
 * @property integer $condicion
 * @property integer $idPagoRelacionado
 * @property integer $estado
 * @property integer $idNotaDeCredito
 * @property double $montoPagado
 *
 * The followings are the available model relations:
 * @property Vehiculo $oCamion
 * @property NotaPedido $oNotaPedido
 * @property AfipVoucher $oVoucherAfip
 * @property PuntoDeVenta $oPuestoVenta
 * @property CategoriaIva $oIvaResponsable
 * @property Cliente $oCliente
 * @property NotaDebito[] $aNotaDebito
 * @property PagoCliente[] $aPagoCliente
 * @property ProdComprobante[] $aProdComprobante
 */
class Comprobante extends CActiveRecord
{
    const iBorrador = 0;
    const iGuardada = 1;
    const iCerrada = 2;
    const iArchivada = 3;

    const borrador = 'Borrador';
    const guardada = 'Guardado';
    const cerrada = 'Cerrado';
    const archivada = 'Archivado';

    public static $aEstado = array(
        self::iBorrador => self::borrador,
        self::iGuardada => self::guardada,
        self::iCerrada => self::cerrada,
        self::iArchivada=> self::archivada,
    );

    const iContado = 1;
    const iCtaCte = 2;

    const contado = 'Contado';
    const ctaCte = 'Cuenta Corriente';

    public static $aCondicion = array(
        self::iContado => self::contado,
        self::iCtaCte => self::ctaCte,
    );

    public static $aClase = array(
	        'A' => 'A',
	        'B' => 'B',
    );

    // RELACION CODIGO => MONTO DE ALICUOTA EN AFIP
    public static $aIvaAlicuotasMontos =  array
    (
        '0' => '0',
        '10.5' => '10.5',
        '21' => '21',
        '27' => '27',
        '5' => '5',
        '2.5' => '2.5'
    );

    public $AlicuotaItem = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comprobante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('clase, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion', 'required'),
			array(
			    'idPagoRelacionado, Nro_Puesto_Venta, Iva_Responsable, Nro_Viajante, Id_Cliente, Id_Camion, idNotaPedido, idVoucherAfip',
                'numerical', 'integerOnly'=>true
            ),
			array('Descuento1, Descuento2, Otro_Iva, alicuotaIva, netoGravado, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, Total_Neto, Total_Final, montoPagado', 'numerical'),
			array('tipo', 'length', 'max'=>2),
			array('clase', 'length', 'max'=>1),
			array('Nro_Comprobante, Moneda', 'length', 'max'=>8),
			array('razonSocial, direccion', 'length', 'max'=>50),
			array('localidad', 'length', 'max'=>65),
			array('provincia', 'length', 'max'=>40),
			array('rubro', 'length', 'max'=>35),
			array('zona', 'length', 'max'=>4),
			array('tipoResponsable, nroInscripIIBB', 'length', 'max'=>30),
			array('cuit', 'length', 'max'=>13),
			array('CAE', 'length', 'max'=>14),
			array('Enviada, Recibida','boolean'),
			array('id, fecha, Observaciones, Vencim_CAE, perIIBB, condicion, idNotaDeCredito, estado', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array(
			    'id, tipo, clase, Nro_Puesto_Venta, Nro_Comprobante, Iva_Responsable, Nro_Viajante, Id_Cliente, razonSocial, direccion, localidad, 
			    provincia, rubro, zona, tipoResponsable, cuit, perIIBB, nroInscripIIBB, fecha, Moneda, Observaciones, Id_Camion, Descuento1, Descuento2, 
			    Enviada, Recibida, Otro_Iva, CAE, Vencim_CAE, idNotaPedido, idVoucherAfip, alicuotaIva, netoGravado, subtotalIva, alicuotaIIBB, subtotal, Ing_Brutos, 
			    Total_Neto, Total_Final, condicion,idPagoRelacionado, idNotaDeCredito, estado', 'safe',
                'on'=>'search'
            ),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCamion' => array(self::BELONGS_TO, 'Vehiculo', 'Id_Camion'),
			'oNotaPedido' => array(self::BELONGS_TO, 'NotaPedido', 'idNotaPedido'),
			'oVoucherAfip' => array(self::BELONGS_TO, 'AfipVoucher', 'idVoucherAfip'),
			'oPuestoVenta' => array(self::BELONGS_TO, 'PuntoDeVenta', 'Nro_Puesto_Venta'),
			'oIvaResponsable' => array(self::BELONGS_TO, 'CategoriaIva', 'Iva_Responsable'),
			'oCliente' => array(self::BELONGS_TO, 'Cliente', 'Id_Cliente'),
			'aNotaDebito' => array(self::HAS_MANY, 'NotaDebito', 'id_factura'),
			'oPagoCliente' => array(self::HAS_ONE, 'PagoCliente', 'id_factura'),
			'aProdComprobante' => array(self::HAS_MANY, 'ProdComprobante', 'idComprobante'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo' => 'Tipo',
			'clase' => 'Clase',
			'Nro_Puesto_Venta' => 'Nro Puesto Venta',
			'Nro_Comprobante' => 'Nro Comprobante',
			'Iva_Responsable' => 'Iva Responsable',
			'Nro_Viajante' => 'Nro Viajante',
			'Id_Cliente' => 'Id Cliente',
			'razonSocial' => 'Razon Social',
			'direccion' => 'Direccion',
			'localidad' => 'Localidad',
			'provincia' => 'Provincia',
			'rubro' => 'Rubro',
			'zona' => 'Zona',
			'tipoResponsable' => 'Tipo Responsable',
			'cuit' => 'Cuit',
			'perIIBB' => 'Per Iibb',
			'nroInscripIIBB' => 'Nro Inscrip Iibb',
			'fecha' => 'Fecha',
			'Moneda' => 'Moneda',
			'Observaciones' => 'Observaciones',
			'Id_Camion' => 'Id Camion',
			'Descuento1' => 'Descuento1',
			'Descuento2' => 'Descuento2',
			'Enviada' => 'Enviada',
			'Recibida' => 'Recibida',
			'Otro_Iva' => 'Otro Iva',
			'CAE' => 'Cae',
			'Vencim_CAE' => 'Vencim Cae',
			'idNotaPedido' => 'Id Nota Pedido',
			'idVoucherAfip' => 'Id Voucher Afip',
			'alicuotaIva' => 'Alicuota Iva',
            'netoGravado' => 'Neto Gravado',
			'subtotalIva' => 'Subtotal Iva',
			'alicuotaIIBB' => 'Alicuota Iibb',
			'subtotal' => 'Subtotal',
			'Ing_Brutos' => 'Ing Brutos',
			'Total_Neto' => 'Total Neto',
			'Total_Final' => 'Total Final',
            'condicion' => 'Condición',
            'montoPagado' =>  Yii::t('application', 'Monto Pagado'),
		);
	}

    public function getAfipJson(){
        $detail = [
            "ver"       => 1,
            "fecha"     => ComponentesComunes::fechaFormateada($this->fecha,"Y-m-d"),
            "cuit"      => intVal(Yii::app()->params['cuit']),
            "ptoVta"    => intVal($this->Nro_Puesto_Venta),
            "tipoCmp"   => intVal(AfipVoucher::$aCbteTipo[$this->tipo][$this->clase]),
            "nroCmp"    => intVal($this->Nro_Comprobante),
            "importe"   => floatval(number_format($this->Total_Final,2,'.','')),
            "moneda"    => "PES",
            "ctz"       => 1,
            "tipoDocRec"=> intVal(AfipVoucher::$aDocTipo['CUIT']),
            "nroDocRec" => intVal($this->cuit),
            "tipoCodAut"=> "A",
            "codAut"    => intVal($this->CAE)
        ];

        return CJSON::encode($detail);
    }

    public function getQrUrl(){
        return "https://www.afip.gob.ar/fe/qr/?p=".base64_encode($this->getAfipJson());
    }

    public function getNetoGravado($save = false){
        if($this->netoGravado == 0 && $this->tipoResponsable != 'Exento'){
            if($this->clase == 'A'){
                $this->netoGravado = number_format($this->subtotal,2,'.','');
            }else{
                $this->netoGravado = number_format($this->getSubTotalGravado(),2,'.','');
            }
            if($save){
                //TODO: HANDLE ERROR
                if(!$this->save()){
                    Yii::log(CHtml::errorSummary($this,'error'));
                }
            }
            
        }
        Yii::log('$this->netoGravado :'.$this->netoGravado.' ','warning');
        return $this->netoGravado;
    }

	public function getFullTitle(){
	    return $this->tipo.'-'.$this->clase.'-'.str_pad($this->Nro_Puesto_Venta,2,0,STR_PAD_LEFT).'-'.str_pad($this->Nro_Comprobante,7,0,STR_PAD_LEFT);
    }

	public function getAlicuotaItem(){
	    $alicuota = ($this->oIvaResponsable->discIva)? 0 : $this->alicuotaIva;
        if ($this->oCliente->oRubro != null){
            if ($this->oCliente->esInstitucion()){
                $alicuota += 10.5;
            }
        }else{
            Yii::log('El Cliente :'.$this->oCliente->id.' NO TIENE RUBRO','error');
        }

        $this->AlicuotaItem = $alicuota;

        return $alicuota;
    }

    public function getIdAfipVoucher(){

	    if($this->estado == self::iBorrador){
	        $this->estado = self::iGuardada;

	        if (!$this->save()){
	            //TODO: DEVOLVER ERROR
	         	Yii::log('Error::'.CHtml::errorSummary($this),'error');
            }
        }
        if($this->fecha == null){
			$this->fecha = date('Ymd');
			//Yii::log('Error::'.number_format($this->perIIBB),'warning');
			if (!$this->save()){
				//TODO: DEVOLVER ERROR
				Yii::log('Error::'.CHtml::errorSummary($this),'error');
			}
		}


	    if ($this->idVoucherAfip > 0){
	        //return $this->idVoucherAfip;
        }

        return $this->generarVoucher();

    }

    public function getCaePath(){
        return Yii::app()->baseUrl."/upload/cae/$this->id.png";
    }

    public function getAliasCaePath(){
        return Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png";
    }

    public function createBarcodeImg(){
        if(!file_exists(Yii::getPathOfAlias("webroot")."/upload/")){
            mkdir(Yii::getPathOfAlias("webroot")."/upload/",0755);
        }
        if(!file_exists(Yii::getPathOfAlias("webroot")."/upload/cae/")){
            mkdir(Yii::getPathOfAlias("webroot")."/upload/cae/",0755);
        }

        if(!file_exists(Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png") || filesize(Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png") == 0 ){
            //TODO: DA ERROR SI EXISTE EL ARCHIVO
            $file = fopen(Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png", "x+");
            fclose($file);
            chmod(Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png",0755);

            $imgBarcode = I2of5::picture(str_replace('-','',$this->codigoDeBarras()),true);
            imagepng($imgBarcode,Yii::getPathOfAlias("webroot")."/upload/cae/$this->id.png");
            imagedestroy($imgBarcode);
        }
    }

    public function getHeader(){
        if ($this->Nro_Puesto_Venta == 10) {
            return '
            <div class="span7" style="font-size:11pt">
                Avda. Ramirez 3039 - P.B.<br>
                (3100) Paraná - Prov. de Entre Ríos<br>
                e-mail: daubysrl@gmail.com<br>
                web: www.dauby.com.ar<br>
                <b>IVA RESPONSABLE INSCRIPTO</b><br>

            </div>
            <div class="span5">
                <span style="font-size: 11pt"></span><b>Nro: '.str_pad($this->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT).'-'.str_pad($this->Nro_Comprobante,8,'0',STR_PAD_LEFT).'</b><br>
                <b>Fecha Factura:  '.ComponentesComunes::fechaFormateada($this->fecha).'</b><br>
                <br>
                CUIT: 33-63444580-9<br>
                IB.CONV. MULT N° 921-741221-8<br>
                Fecha Inicio Actividad: 15/11/2012<br>

            </div>
            ';
        }else{
            return '
            <div class="span7" style="font-size: 11pt">
                RUTA Pcial N°2 - Alt. 2350<br>
                (3014) Monte Vera - Prov. Sta Fe<br>
                Tel/Fax : 54 (0342) 4904299<br> 
                Tel: 54 (0342) 4904164<br>                
                e-mail: daubysrl@gmail.com<br>
                web: www.dauby.com.ar<br>
                <b>IVA RESPONSABLE INSCRIPTO</b><br>

            </div>
            <div class="span5">
                <span style="font-size: 11pt"></span><b>Nro: '.str_pad($this->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT).'-'.str_pad($this->Nro_Comprobante,8,'0',STR_PAD_LEFT).'</b><br>
                <b>Fecha Factura:  '.ComponentesComunes::fechaFormateada($this->fecha).'</b><br>
                <br>
                CUIT: 33-63444580-9<br>
                IB.CONV. MULT N° 921-741221-8<br>
                Fecha Inicio Actividad: 01/09/1989<br>

            </div>
            ';
        }
    }

    public function getItemsFooter(){

        // SI ES DE ENTRE RIOS Y NO EXCEPTUA INGRESOS BRUTOS
        if ($this->oCliente->oCategoriaIva->tipoFact == 'A' && $this->Nro_Puesto_Venta == 10 && $this->oCliente->exeptuaIIB == 0) {
            $strFooter = '
                <div class="span6">
                    .
                </div>
                <div class="span3">
                    Subtotal:<br>
                    Descuento: <br>
                    Neto Gravado:<br>
                    IVA ' . $this->alicuotaIva . '%:<br>
                    Perc. IIBB E.Rios:<br>
                    <b>Total:</b>
                </div>
                <div class="span2" style="text-align: right">
                    ' . number_format($this->Total_Neto, 2, ',', '.') . '<br>
                    ' . number_format($this->getDescuentoTotal(), 2, ',', '.') . '<br>
                    ' . number_format($this->subtotal, 2, ',', '.') . '<br>
                    ' . number_format($this->subtotalIva, 2, ',', '.') . '<br>
                    ' . number_format($this->getIIBB(), 2, ',', '.') . '<br>
                    <b>' . number_format($this->Total_Final, 2, ',', '.') . '</b>
    
                </div>
                <div class="span1" style="text-align: right">
                </div>
            ';
        }else if($this->oCliente->oCategoriaIva->tipoFact == 'B'){
            $strFooter = '
            <div class="span7">
                .
            </div>
            <div class="span2">
                Subtotal:<br>
                Descuento: <br>
                Neto Gravado:<br>   
                IVA '.$this->alicuotaIva.'%:<br>
                <b>Total:</b>
            </div>
            <div class="span2" style="text-align: right">
                '.number_format($this->Total_Neto,2,',','.').'<br>
                '.number_format($this->getDescuentoTotal(),2,',','.').'<br>
                '.number_format($this->subtotal,2,',','.').'<br>
                0,00 <br>             
                <b>'.number_format($this->Total_Final,2,',','.').'</b>

            </div>
            <div class="span1" style="text-align: right">
            </div>
        ';
        }else{
            $strFooter = '
            <div class="span7">
                .
            </div>
            <div class="span2">
                Subtotal:<br>
                Descuento: <br>
                Neto Gravado:<br>
                IVA '.$this->alicuotaIva.'%:<br>
                <b>Total:</b>
            </div>
            <div class="span2" style="text-align: right">
                '.number_format($this->Total_Neto,2,',','.').'<br>
                '.number_format($this->getDescuentoTotal(),2,',','.').'<br>
                '.number_format($this->subtotal,2,',','.').'<br>
                '.number_format($this->subtotalIva,2,',','.').'<br>
                <b>'.number_format($this->Total_Final,2,',','.').'</b>

            </div>
            <div class="span1" style="text-align: right">
            </div>
        ';
        }


        return $strFooter;
    }

    // CREA EL VOUCHER DE AFIP PARA TRANSMITIR LA FACTURA ELECTRONICA

    public function generarVoucher(){
        $transaction=Yii::app()->db->beginTransaction();

        $voucher = new AfipVoucher();
        $voucher->numeroPuntoVenta = $this->Nro_Puesto_Venta;
        $voucher->codigoConcepto = Yii::app()->params['ConceptoAfip'];  //1; //PRODUCTOS

        if ($this->cuit == 0){
            $voucher->codigoTipoDocumento = AfipVoucher::$aDocTipo['DNI'];
        }else{
            $voucher->codigoTipoDocumento = AfipVoucher::$aDocTipo['CUIT'];
        }

        $voucher->numeroDocumento = $this->cuit;
        $voucher->fechaComprobante = ComponentesComunes::fechaFormateada($this->fecha,"Ymd");
        $voucher->codigoMoneda = 'PES';
        $voucher->cotizacionMoneda = 1;
        $voucher->importeExento = 0;
        $voucher->importeNoGravado = 0;  // CHEQUEAR
        $voucher->codigoTipoComprobante = AfipVoucher::$aCbteTipo[$this->tipo][$this->clase];

        //if($this->clase == 'A'){
        if($this->oIvaResponsable->discIva){
            $voucher->importeGravado = $this->getSubTotal2(); // $this->getSubTotalGravado();  // CHEQUEAR
            Yii::log('$voucher->importeGravado...'.$voucher->importeGravado,'warning');
            $voucher->importeTotal =  number_format($this->Total_Final,2,'.','');
            $voucher->importeIVA = number_format($this->subtotalIva,2,'.','');
            $voucher->importeOtrosTributos = number_format($this->Ing_Brutos,2,'.','');
        }else{
            $voucher->importeTotal = number_format($this->Total_Final,2,'.','');
            $voucher->importeGravado = number_format(($this->Total_Final - $this->subtotalIva ),2,'.','');  // CHEQUEAR
            $voucher->importeIVA = number_format($this->subtotalIva,2,'.','');
            $voucher->importeOtrosTributos = 0;
        }

        if($voucher->save()){
            $this->idVoucherAfip = $voucher->id;

            $oAfipSusbtotIva = new AfipSubtotivas();
            $oAfipSusbtotIva->idVoucher = $this->idVoucherAfip;
            $oAfipSusbtotIva->importe = number_format($this->subtotalIva,2,'.','');
            $oAfipSusbtotIva->codigo = 5;
            if($this->oIvaResponsable->discIva) {
                $oAfipSusbtotIva->BaseImp = number_format($this->subtotal, 2, '.', '');
            }else{
                $oAfipSusbtotIva->BaseImp = number_format($voucher->importeGravado, 2, '.', '');
            }
            foreach (AfipVoucher::$aIvaAlicuotas as $codigo => $fAlicuota) {
                if (number_format($fAlicuota,2) == number_format($this->alicuotaIva,2) ){
                    $oAfipSusbtotIva->codigo = $codigo;
                }
            }

            if (!$oAfipSusbtotIva->save()){
                $transaction->rollback();
                Yii::log('CHtml::errorSummary($this) '.CHtml::errorSummary($oAfipSusbtotIva),'error');
                throw new Exception(CHtml::errorSummary($oAfipSusbtotIva));
            }

            //TODO: APLICAR LA TABLA DE iibb
            if ($this->Nro_Puesto_Venta == 10 && $this->oCliente->exeptuaIIB == 0 && $this->clase == 'A'){ //ENTRE RIOS
                Yii::log('Tributos ENTRE RIOS...','warning');

                $vTributo = new AfipTributo();
                $vTributo->idVoucher = $voucher->id;
                $vTributo->Desc = AfipVoucher::$aTributos[7];
                $vTributo->Id = 7; // CODIGO PARA PERCEPCION INGRESOS BRUTOS
                // TODO: FALTAN CAMPOS Y GUARDAR
                $vTributo->Alic = ''.number_format($this->alicuotaIIBB,2,'.','');
                $vTributo->BaseImp = $this->getSubTotalGravado();
                $vTributo->Importe = number_format($voucher->importeOtrosTributos,2,'.','');
                if (!$vTributo->save()){
                    $transaction->rollback();

                    Yii::log('CHtml::errorSummary($this) '.ComponentesComunes::print_array($voucher->attributes,true),'error');
                    throw new Exception(CHtml::errorSummary($vTributo));
                }

            }else{
                //CUANDO NO TIENE OTROS TRIBUTOS
                $voucher->importeOtrosTributos = 0;
            }

            if ($this->save()){
            	Yii::log('pre prods2VoucherItems','warning');
                ProdComprobante::prods2VoucherItems($this->id,$this->clase,$voucher->id);
                $transaction->commit();
                return $voucher->id;
            }else{
                Yii::log('CHtml::errorSummary($this) sin VoucherItems '.ComponentesComunes::print_array($this->attributes,true),'warning');
                Yii::log('CHtml::errorSummary($this) sin VoucherItems '.CHtml::errorSummary($this),'warning');
                $transaction->rollback();
                return false;
            }
        }else{
            Yii::log('CHtml::errorSummary($this) '.CHtml::errorSummary($voucher),'warning');
            $transaction->rollback();
            return false;
        }
        $transaction->rollback();

    }

    public function esElectronica(){
        $bEsElectronica = $this->oPuestoVenta->esElectronico();
        if($bEsElectronica){
            try{
                if(!($this->idVoucherAfip > 0)){
                    //$this->generarVoucher();
                }else{

                }
            }catch (Exception $exception){
                Yii::log('EXCEPTION ! idVoucherAfip '.$exception->getMessage(),'warning');
            }

        }
        return ($bEsElectronica);
    }

    public static function devolucion($idCliente,$monto){
        $oCliente = Cliente::model()->findByPk($idCliente);
        if ($oCliente == null){
            throw new Exception('Cliente inexistente');
        }
        $oNotaCredito = new NotaCredito();
        $oNotaCredito->tipo = 'NE';
        $oNotaCredito->Id_Cliente = $idCliente;

        if (!$oNotaCredito->save()){
            throw new Exception(CHtml::errorSummary($oNotaCredito));
        }

        $oArtVenta = ArticuloVenta::getArticuloDevolucion();
        $oProdNotaCredito = new ProdNotaCredito();
        $oProdNotaCredito->idComprobante = $oNotaCredito->id;
        $oProdNotaCredito->precioUnitario = $monto;
        $oProdNotaCredito->idArtVenta = $oArtVenta->id_articulo_vta;

        $oProdNotaCredito->save();

        $idVoucher = $oNotaCredito->generarVoucher();
        $oVoucher = AfipVoucher::model()->findByPk($idVoucher);
        if ($oVoucher == null){
            throw new Exception('Error al generar el voucher');
        }

        try {
            $oVoucher->emitir();
        }catch (Exception $e){
            throw new CHttpException('500','<span class="errorSpan">'.$e->getMessage().' =>'.$e->getCode().'</span>');
        }
    }


    public function codigoDeBarras(){

        $date = DateTime::createFromFormat('Y-m-d', $this->Vencim_CAE);
        if (!$date){
            return '';
        }

		// TODO: VER SI 33634445809 ES EL CUIT DE ERCA
        $Numero = '33634445809'.'0'.str_pad($this->Nro_Puesto_Venta,5,0).$this->CAE.$date->format('dmY');
        $j=strlen($Numero);
        $par=0;$impar=0;
        for ($i=0; $i < $j ; $i++) {
            if ($i%2==0){
                $par=$par+$Numero[$i];
            }else{
                $impar=$impar+$Numero[$i];
            }

        }
        $par=$par*3;
        $suma=$par+$impar;
        for ($i=0; $i < 9; $i++) {
            if ( fmod(($suma +$i),10) == 0) {
                $verificador=$i;
            }
        }
        $digito = 10 - ($suma - (intval($suma / 10) * 10));
        if ($digito == 10){
            $digito = 0;
        }
        return str_replace('-','',$Numero.$digito);
    }


    public function getDescuento1(){
        return number_format($this->Descuento1,2,'.','');
    }

    public function getDescuento2(){
        return number_format($this->Descuento2,2,'.','');
    }
    ////////////////////////

    public function calcularNumero(){
        if($this->Nro_Comprobante == '' && ($this->CAE != '' || $this->CAE != '00000000000000')){
            $afipResult = AfipVoucher::getUltimoComprobanteAutorizado($this->Nro_Puesto_Venta, AfipVoucher::$aCbteTipo[$this->tipo][$this->clase]);
            if(isset($afipResult['number'])){
                $this->Nro_Comprobante = ($afipResult['number']) + 1;
            }else{
                $this->Nro_Comprobante = 1;
            }
        }

        return $this->Nro_Comprobante;
    }

    public function getMontoASaldar(){
        return ($this->Total_Final > $this->montoPagado)? $this->Total_Final - $this->montoPagado : 0;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('clase',$this->clase,true);
		$criteria->compare('Nro_Puesto_Venta',$this->Nro_Puesto_Venta);
		$criteria->compare('Nro_Comprobante',$this->Nro_Comprobante,true);
		$criteria->compare('Iva_Responsable',$this->Iva_Responsable);
		$criteria->compare('Nro_Viajante',$this->Nro_Viajante);
		$criteria->compare('Id_Cliente',$this->Id_Cliente);
		$criteria->compare('razonSocial',$this->razonSocial,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('localidad',$this->localidad,true);
		$criteria->compare('provincia',$this->provincia,true);
		$criteria->compare('rubro',$this->rubro,true);
		$criteria->compare('zona',$this->zona,true);
		$criteria->compare('tipoResponsable',$this->tipoResponsable,true);
		$criteria->compare('cuit',$this->cuit,true);
		$criteria->compare('perIIBB',$this->perIIBB);
		$criteria->compare('nroInscripIIBB',$this->nroInscripIIBB,true);
		if ($this->fecha != null){
			$criteria->compare('fecha',ComponentesComunes::fechaComparada($this->fecha));
        }
		$criteria->compare('Moneda',$this->Moneda,true);
		$criteria->compare('Observaciones',$this->Observaciones,true);
		$criteria->compare('Id_Camion',$this->Id_Camion);
		$criteria->compare('Descuento1',$this->Descuento1);
		$criteria->compare('Descuento2',$this->Descuento2);
		$criteria->compare('Enviada',$this->Enviada);
		$criteria->compare('Recibida',$this->Recibida);
		$criteria->compare('Otro_Iva',$this->Otro_Iva);
		$criteria->compare('CAE',$this->CAE,true);
		if ($this->Vencim_CAE != null){
			$criteria->compare('Vencim_CAE',ComponentesComunes::fechaComparada($this->Vencim_CAE));
        }
		$criteria->compare('idNotaPedido',$this->idNotaPedido);
		$criteria->compare('idVoucherAfip',$this->idVoucherAfip);
		$criteria->compare('alicuotaIva',$this->alicuotaIva);
		$criteria->compare('subtotalIva',$this->subtotalIva);
		$criteria->compare('alicuotaIIBB',$this->alicuotaIIBB);
		$criteria->compare('subtotal',$this->subtotal);
		$criteria->compare('Ing_Brutos',$this->Ing_Brutos);
		$criteria->compare('Total_Neto',$this->Total_Neto);
		$criteria->compare('Total_Final',$this->Total_Final);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'id desc',
            ),
		));
	}




    /**
     *
     */
    public function getSubTotalGravado(){


        $fSubtotal = $this->getSubTotal2() / (1+($this->alicuotaIva/100)) ;

        return number_format($fSubtotal,2, '.', '');
    }

    public function getDescuentoTotal(){
        $subTotalNeto = $this->getSubTotalNeto();
        $desc1 = $subTotalNeto * ($this->Descuento1/100) ;
        $desc2 = ($subTotalNeto - $desc1) * ($this->Descuento2/100);
        return number_format( ($desc1 + $desc2 ),2, '.', '');
    }



    public function getIIBB(){

        if ($this->oCliente->oCategoriaIva->tipoFact == 'A' && $this->Nro_Puesto_Venta == 10 && $this->oCliente->exeptuaIIB == 0){
            Yii::log('Ing_Brutos $'.$this->subtotal,'warning');
            // POR AHORA SOLO INGRESOS BRUTOS DE ENTRE RIOS
            return number_format(($this->getSubTotal2() * ($this->alicuotaIIBB / 100)),2,'.','');
        } else {
            return 0;
        }
    }

    public function getIva2(){

        $fIva = 0;

        foreach ($this->aProdComprobante as $index => $oProdComprobante) {
            $fIva += number_format($oProdComprobante->getIva(),2, '.', '');
        }

        return number_format($fIva,2, '.', '');
    }


    /**
     * Devuelve el subtotal sin descuentos
     *
     */
    public function getSubTotalNeto(){

        $fSubtotal=0;
        foreach ($this->aProdComprobante as $index => $oProdComprobante) {
            $fSubtotal += number_format($oProdComprobante->getImporte(),2, '.', '');
        }
        return number_format($fSubtotal,2, '.', '');
    }

    public function getSubTotal(){
        return $this->subtotal;
    }

	/**
     * Devuelve el subtotal calculado sin descuentos
     *
     */
    public function getSubTotal2(){

        $fSubtotal= $this->getSubTotalNeto();


        $fSubtotal2 = $fSubtotal * (1-($this->Descuento1 / 100 )) * (1-($this->Descuento2 / 100 )) ;

        //Yii::log('$fSubtotal2 :'.$fSubtotal2.' ->'.$this->Descuento1.' ->'.$this->Descuento2,'warning');

        return number_format($fSubtotal2,2, '.', '');
    }

	public function recalcular(){

        $subtotal = $this->getSubTotalNeto();
        $this->Total_Neto = number_format($subtotal,2,'.','');
        $this->subtotal = number_format($this->getSubTotal2(),2, '.', '');

        if ($this->oIvaResponsable->discIva){
            $this->subtotalIva = number_format(($this->subtotal * ($this->alicuotaIva / 100)),2, '.', '');
            $this->Total_Final = number_format(($this->subtotal + $this->subtotalIva + $this->Ing_Brutos),2,'.','');
        }else{
            $this->subtotalIva = number_format(($this->subtotal - ($this->subtotal / (1 + ($this->alicuotaIva / 100)))),2, '.', '');
            $this->Total_Final = number_format($this->subtotal ,2,'.','');
        }

        if($this->alicuotaIva != 0){
            $this->netoGravado = number_format(($this->subtotalIva / ($this->alicuotaIva / 100)),2, '.', '');
        }else{
            $this->netoGravado = 0;
        }
            

        /*
        Yii::log('$this->subtotal: Total_Neto :'.$this->Total_Neto,'warning');
        Yii::log('$this->subtotal:'.$this->subtotal,'warning');
        Yii::log('$this->subtotalIva:'.$this->subtotalIva,'warning');
        Yii::log('Recalcular : $this->Total_Final:'.$this->Total_Final,'warning');
        */
    }

    public function getNextNumber(){
        /** @var $oComprobante Comprobante */
        $oComprobante = Comprobante::model()->findByAttributes(array('Nro_Puesto_Venta'=>$this->Nro_Puesto_Venta,'clase' => $this->clase),array('order' => 't.Nro_Comprobante desc'));
        return ($oComprobante != null)? ($oComprobante->Nro_Comprobante +1 ) : 1;
    }

	public function beforeSave(){
        /* @var $oComprobante Comprobante */
	    $oComprobante = Comprobante::model()->findByPk($this->id);
		//Yii::log('beforeSave :'.get_class($this),'warning');

		//ACTUALIZO LOS DATOS DEL CLIENTE si ES NUEVO o SI NO ESTA CERRADA + HAY CAMBIO DE CLIENTE
        if ($oComprobante == null || ($oComprobante->estado < Comprobante::iCerrada && $oComprobante->Id_Cliente != $this->Id_Cliente)){
            $this->razonSocial = $this->oCliente->razonSocial;
            $this->direccion = $this->oCliente->direccion;
            $this->localidad = $this->oCliente->localidad;
            $this->provincia = $this->oCliente->provincia;
            $this->zona = $this->oCliente->zp;
            $this->rubro = ($this->oCliente->oRubro != null)? $this->oCliente->oRubro->nombre : 'SIN RUBRO';
            $this->tipoResponsable = $this->oCliente->oCategoriaIva->nombre;
            $this->cuit = $this->oCliente->cuit;
            $this->perIIBB = number_format($this->perIIBB,2);
            if ($this->Nro_Puesto_Venta == 10 && $this->oCliente->exeptuaIIB == 0){
            	$this->alicuotaIIBB = 3;
            }
            /* TODO: REVISAR LOS SIGUIENTES CAMPOS*/
            $this->clase = ($this->clase != null)? $this->clase : $this->oCliente->oCategoriaIva->tipoFact;
            $this->Nro_Puesto_Venta = ($this->Nro_Puesto_Venta != null)? $this->Nro_Puesto_Venta : $this->oCliente->Nro_Puesto_Venta;
            $this->Nro_Viajante = $this->oCliente->codViajante;
		}

		// SI ES NUEVO O SI NO ETA CERRADA
		if ($oComprobante == null || $oComprobante->estado < Comprobante::iCerrada){
            $this->Ing_Brutos = number_format($this->getIIBB(),2, '.', '');
            $this->recalcular();

            if($oComprobante != null){
                if ($this->CAE == "00000000000000" || $this->CAE == "" )
                // SI CAMBIO algun importe
                if(($oComprobante->idVoucherAfip > 0)&& ($oComprobante->Id_Cliente != $this->Id_Cliente || $oComprobante->fecha != $this->fecha ||$oComprobante->subtotal != $this->subtotal || $oComprobante->alicuotaIva != $this->alicuotaIva || $oComprobante->Descuento1 != $this->Descuento1 || $oComprobante->Descuento2 != $this->Descuento2)){
                    $this->idVoucherAfip = null;
                    $this->CAE = "";
                    $this->Vencim_CAE = null;
                }
            }

            // SI CAMBIA EL ESTADO A CERRADO SE ACTUALIZA EL SALDO
            //Yii::log('SI CAMBIA EL ESTADO A CERRADO SE ACTUALIZA EL SALDO :','warning');
            if ($oComprobante != null && $this->estado == Factura::iCerrada){
                $oCliente = $this->oCliente;
                //Yii::log('ACTUALIZA EL SALDO :::::::::: '.$oCliente->saldoActual,'warning');
                if ($this->tipo == 'NC' ){ // ES NOTA DE CREDITO
                    $oCliente->saldoActual = $oCliente->saldoActual - $this->Total_Final;
                    $oFactura = Factura::model()->findByAttributes(array('idNotaDeCredito' => $this->id));
                    if($oFactura != null){
                        Yii::log('HAY FACTURA '.$oFactura->id,'warning');
                        $oFactura->montoPagado += $this->Total_Final;
                        Yii::log($oFactura->montoPagado.' -- '.$this->Total_Final,'warning');
                        if (!$oFactura->save()){
                            // TODO: DEBERIA TIRAR EXCEPCION
                            Yii::log(''.CHtml::errorSummary($oFactura),'warning');
                        }
                    }else{
                        Yii::log('$this->oFactura == null','warning');
                    }

                }else{ // ES FACTURA O NOTA DE DEBITO
                    $oCliente->saldoActual = $oCliente->saldoActual + $this->Total_Final;
                }
                if(!$oCliente->save()){
                    Yii::log(ComponentesComunes::print_array($oCliente->getErrors(),true),'error');
                }
            }

            //$this->perIIBB = $this->oCliente->percibeIIBB();
            //TODO: implementar lo siguiente...
            //$this->nroInscripIIBB
        }else{
            //SI EL COMPROBANTE ESTA CERRADO SOLO SE PERMITE CAMBIO DE ESTADO Y CAMION. ES PARA PERMITIR PONER ESTADO ARCHIVADO y ENVIADO Y RECIBIDO
            $newStatus = $this->estado;
            $newAttributes = new Comprobante();
            $newAttributes->attributes = $this->attributes;
            $this->attributes = $oComprobante->attributes;
            $this->estado = $newStatus;
            $this->Recibida = $newAttributes->Recibida;
            $this->Enviada = $newAttributes->Enviada;
            $this->Id_Camion = $newAttributes->Id_Camion;
            $this->idPagoRelacionado = $newAttributes->idPagoRelacionado;
            $this->idNotaDeCredito = $newAttributes->idNotaDeCredito;
            $this->montoPagado = $newAttributes->montoPagado ;
            $this->idNotaPedido = $newAttributes->idNotaPedido ;
            $this->netoGravado = $newAttributes->netoGravado;
            $this->subtotalIva = $newAttributes->subtotalIva;
            $this->alicuotaIva = $newAttributes->alicuotaIva;
		}
        $this->getNetoGravado(false);

        //TODO:CONTROLAR LO SIGUIENTE
        $this->perIIBB = 0;
	    return parent::beforeSave();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Comprobante the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


}
