<?php

/**
 * This is the model class for table "articuloVenta".
 *
 * The followings are the available columns in table 'articuloVenta':
 * @property integer $id_articulo_vta
 * @property string $nombre
 * @property string $descripcion
 * @property integer $id_presentacion
 * @property integer $id_articulo
 * @property double $contenido
 * @property string $uMedida
 * @property string $codBarra
 * @property double $precio
 * @property double $costo
 * @property integer $enLista
 * @property double $stockMinimo
 *
 * The followings are the available model relations:
 * @property Articulo $oArticulo
 * @property Paquete[] $aPaquete
 */
class ArticuloVenta extends CActiveRecord
{
	public $id_rubro;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articuloVenta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, id_presentacion, id_articulo, contenido, uMedida, precio', 'required'),
			array('id_presentacion, id_articulo, enLista', 'numerical', 'integerOnly'=>true),
			array('contenido, precio, costo, stockMinimo', 'numerical'),
			array('descripcion', 'length', 'max'=>70),
			array('uMedida', 'length', 'max'=>6),
			array('codBarra', 'length', 'max'=>60),
			array('nombre','safe'),
			array('id_presentacion','checkUnique'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id_articulo_vta, nombre, descripcion, id_presentacion, id_articulo, contenido, uMedida, codBarra, precio, costo, enLista, stockMinimo, id_rubro', 'safe', 'on'=>'search'),
		);
	}

	public function checkUnique($attribute,$params){
        if($this->id_presentacion !== null)
        {
            $model = ArticuloVenta::model()->find('id_presentacion = ? AND id_articulo = ?', array($this->id_presentacion, $this->id_articulo));
            if($model != null && $model->id_articulo_vta != $this->id_articulo_vta)
                $this->addError('id_presentacion','Esta presentación ya existe para este rubro/artículo ');
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oArticulo' => array(self::BELONGS_TO, 'Articulo', 'id_articulo'),
			'aPaquete' => array(self::HAS_MANY, 'Paquete', 'id_artVenta'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
		    'model' => 'Articulo Vta',
            'models' => 'Articulos Vta',
			'id_articulo_vta' => 'Articulo Vta',
            'nombre' => 'nombre',
			'descripcion' => 'Descripcion',
			'id_presentacion' => 'Presentacion',
			'id_articulo' => 'Articulo',
			'contenido' => 'Contenido',
			'uMedida' => 'U Medida',
			'codBarra' => 'Cod Barra',
			'precio' => 'Precio',
			'costo' => 'Costo',
			'enLista' => 'En Lista',
			'stockMinimo' => 'Stock Minimo',
            'id_rubro' => 'Rubro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->with = array('oArticulo');//,'oArticulo.oRubro'

		$criteria->compare('t.id_articulo_vta',$this->id_articulo_vta);
		$criteria->compare('t.descripcion',$this->descripcion,true);
        $criteria->compare('t.nombre',$this->nombre,true);
		$criteria->compare('t.id_presentacion',$this->id_presentacion);
        $criteria->compare('t.nombre',$this->nombre,true);
		$criteria->compare('t.contenido',$this->contenido);
		$criteria->compare('t.uMedida',$this->uMedida,true);
		$criteria->compare('t.codBarra',$this->codBarra,true);
		$criteria->compare('t.precio',$this->precio);
		$criteria->compare('t.costo',$this->costo);
		$criteria->compare('t.enLista',$this->enLista);
		$criteria->compare('t.stockMinimo',$this->stockMinimo);

        //if ($this->id_articulo > 0 && $this->id_rubro > 0) {
        if ($this->id_rubro > 0) {
			$criteria->compare('oArticulo.id_rubro',$this->id_rubro);
		}
		if ($this->id_articulo > 0) {
			$criteria->compare('oArticulo.id_articulo',$this->id_articulo);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
				'attributes' =>array(
					'*',
					't.id_articulo' => array(
							'asc' => 'oArticulo.id_articulo asc',
							'desc' => 'oArticulo.id_articulo desc',
					),
					't.id_rubro' => array(
							'asc' => 'oArticulo.id_rubro asc',
							'desc' => 'oArticulo.id_rubro desc',
					),
                    't.descripcion' => array(
                        'asc' => 't.nombre,t.contenido asc',
                        'desc' => 't.nombre,t.contenido desc',
                    ),
				),
			),	
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchLong()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArticulo');//,'oArticulo.oRubro'

        $criteria->compare('t.id_articulo_vta',$this->id_articulo_vta);
        $criteria->compare('t.descripcion',$this->descripcion,true);
        $criteria->compare('t.id_presentacion',$this->id_presentacion);
        $criteria->compare('t.nombre',$this->nombre,true);
        $criteria->compare('t.contenido',$this->contenido);
        $criteria->compare('t.uMedida',$this->uMedida,true);
        $criteria->compare('t.codBarra',$this->codBarra,true);
        $criteria->compare('t.precio',$this->precio);
        $criteria->compare('t.costo',$this->costo);
        $criteria->compare('t.enLista',$this->enLista);
        $criteria->compare('t.stockMinimo',$this->stockMinimo);

        //if ($this->id_articulo > 0 && $this->id_rubro > 0) {
        if ($this->id_rubro > 0) {
            $criteria->compare('oArticulo.id_rubro',$this->id_rubro);
        }
        if ($this->id_articulo > 0) {
            $criteria->compare('oArticulo.id_articulo',$this->id_articulo);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array(
                'pageSize' => 35,
            ),
            'sort' => array(
                'attributes' =>array(
                    '*',
                    't.id_articulo' => array(
                        'asc' => 'oArticulo.id_articulo asc',
                        'desc' => 'oArticulo.id_articulo desc',
                    ),
                    't.id_rubro' => array(
                        'asc' => 'oArticulo.id_rubro asc',
                        'desc' => 'oArticulo.id_rubro desc',
                    ),
                    't.descripcion' => array(
                        'asc' => 't.nombre,t.contenido asc',
                        'desc' => 't.nombre,t.contenido desc',
                    ),
                ),
            ),
        ));
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchShort()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArticulo','oArticulo.oRubro');//,'oArticulo.oRubro'

        $criteria->compare('t.id_articulo_vta',$this->id_articulo_vta);
        $criteria->compare('t.descripcion',$this->descripcion,true);
        $criteria->compare('t.id_presentacion',$this->id_presentacion);
        $criteria->compare('t.nombre',$this->nombre,true);
        $criteria->compare('t.contenido',$this->contenido);
        $criteria->compare('t.uMedida',$this->uMedida,true);
        $criteria->compare('t.codBarra',$this->codBarra,true);
        $criteria->compare('t.precio',$this->precio);
        $criteria->compare('t.costo',$this->costo);
        $criteria->compare('t.enLista',$this->enLista);
        $criteria->compare('t.stockMinimo',$this->stockMinimo);

        if ($this->id_rubro <> "") {
            echo '$this->id_rubro '.$this->id_rubro;
            $criteria->compare('oRubro.descripcion',$this->id_rubro,true);

        }
        if ($this->id_articulo <> "") {
            echo '$this->id_articulo '.$this->id_articulo;
            $criteria->compare('oArticulo.descripcion',$this->id_articulo,true);
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array(
                'attributes' =>array(
                    '*',
                    't.id_articulo' => array(
                        'asc' => 'oArticulo.descripcion asc',
                        'desc' => 'oArticulo.descripcion desc',
                    ),
                    't.id_rubro' => array(
                        'asc' => 'oRubro.descripcion asc',
                        'desc' => 'oRubro.descripcion desc',
                    ),
                    't.descripcion' => array(
                        'asc' => 't.nombre,t.contenido asc',
                        'desc' => 't.nombre,t.contenido desc',
                    ),
                ),
            ),
        ));
    }
	
	public function getCodigo(){
		//return $this->oArticulo->id_rubro.' '.$this->oArticulo->id_articulo.' '.$this->id_presentacion;
        return $this->oArticulo->id_rubro.'-'.$this->oArticulo->id_articulo.'-'.$this->id_presentacion;
	}
	
	public function getPresentacion(){
		return $this->id_presentacion.'::'.$this->descripcion;
	}
	
	public function buscarMultiplesCampos($idPresenacion,$idArticulo=NULL) {
		$idPresenacion = addcslashes($idPresenacion, '%_'); // escape LIKE's special characters
		$criteria = new CDbCriteria( array(
				'condition' => "(id_presentacion LIKE :param)",     	// no quotes around :match
				'params'	=> array(':param' => "$idPresenacion")  // Aha! Wildcards go here
		) );
	
		$criteria->addCondition('id_articulo = :idArticulo');
		$criteria->params[':idArticulo'] = $idArticulo;
		/*
		 echo '<pre>';
			print_r($criteria->params);
			echo '</pre>';
	
			die();
		 */
	
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination' => false,
		));
	}

	public static function getArticuloDevolucion(){
	    return ArticuloVenta::findByPk(2767);
    }

	/**
	 * Returns the   model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ArticuloVenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
