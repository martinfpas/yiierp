<?php

/**
 * This is the model class for table "cheque3RoMovBancario".
 *
 * The followings are the available columns in table 'cheque3RoMovBancario':
 * @property integer $id
 * @property integer $idCheque3ro
 * @property integer $idMovimiento
 *
 * The followings are the available model relations:
 * @property ChequeDeTerceros $oCheque3ro
 * @property MovimientoCuentasBanc $oMovimiento
 */
class Cheque3RoMovBancario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cheque3RoMovBancario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idCheque3ro, idMovimiento', 'required'),
			array('idCheque3ro, idMovimiento', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, idCheque3ro, idMovimiento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oCheque3ro' => array(self::BELONGS_TO, 'ChequeDeTerceros', 'idCheque3ro'),
			'oMovimiento' => array(self::BELONGS_TO, 'MovimientoCuentasBanc', 'idMovimiento'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'model' => 'Cheque3 Ro Mov Bancario',
            'models' => 'Cheque3 Ro Mov Bancarios',
			'id' => Yii::t('application', 'ID'),
			'idCheque3ro' => Yii::t('application', 'Id Cheque3ro'),
			'idMovimiento' => Yii::t('application', 'Id Movimiento'),
		);
	}

	public function beforeSave(){
	    $this->oCheque3ro->depositar();
	    return parent::beforeSave();
    }

    public function afterDelete(){
	    $this->oMovimiento->recalcular(true);
	    $this->oCheque3ro->estado = ChequeDeTerceros::iEnCartera;
        $this->oCheque3ro->fechaDeposito = null;
        //TODO: MANEJAR ESTO
        if ($this->oCheque3ro->save()){
            Yii::log('$this->oCheque3ro:'.CHtml::errorSummary($this->oCheque3ro),'error');
        }


	    return parent::afterDelete();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idCheque3ro',$this->idCheque3ro);
		$criteria->compare('idMovimiento',$this->idMovimiento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cheque3RoMovBancario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
