<?php

/**
 * This is the model class for table "comisionViajante".
 *
 * The followings are the available columns in table 'comisionViajante':
 * @property integer $numeroViajante
 * @property string $fechaInicio
 * @property string $fechaFinal
 * @property double $porcentaje
 * @property double $importeTotal
 * @property string $fechaPago
 * @property double $importePago
 * @property string $modoPago
 *
 * The followings are the available model relations:
 * @property Viajante $oViajante
 */
class ComisionViajante extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ComisionViajante the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comisionViajante';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numeroViajante, fechaInicio, fechaFinal, porcentaje, importeTotal, fechaPago, importePago', 'required'),
			array('numeroViajante', 'numerical', 'integerOnly'=>true),
			array('porcentaje, importeTotal, importePago', 'numerical'),
			array('modoPago', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('numeroViajante, fechaInicio, fechaFinal, porcentaje, importeTotal, fechaPago, importePago, modoPago', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'oViajante' => array(self::BELONGS_TO,'Viajante','numeroViajante'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'numeroViajante' => Yii::t('application', 'Numero Viajante'),
		            'fechaInicio' => Yii::t('application', 'Fecha Inicio'),
		            'fechaFinal' => Yii::t('application', 'Fecha Final'),
		            'porcentaje' => Yii::t('application', 'Porcentaje'),
		            'importeTotal' => Yii::t('application', 'Importe Total'),
		            'fechaPago' => Yii::t('application', 'Fecha Pago'),
		            'importePago' => Yii::t('application', 'Importe Pago'),
		            'modoPago' => Yii::t('application', 'Modo Pago'),
		    );
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('numeroViajante',$this->numeroViajante);

        if ($this->fechaInicio != null){
            $criteria->compare('fechaInicio',ComponentesComunes::fechaComparada($this->fechaInicio));
        }



        if ($this->fechaFinal != null){
            $criteria->compare('fechaFinal',ComponentesComunes::fechaComparada($this->fechaFinal));
        }


		$criteria->compare('porcentaje',$this->porcentaje);
		$criteria->compare('importeTotal',$this->importeTotal);

        if ($this->fechaPago != null){
            $criteria->compare('fechaPago',ComponentesComunes::fechaComparada($this->fechaPago));
        }


		$criteria->compare('importePago',$this->importePago);
		$criteria->compare('modoPago',$this->modoPago,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 't.id desc',
            ),
		));
	}
}