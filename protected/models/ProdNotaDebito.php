<?php

/**
 * This is the model class for table "prodNotaCredito".
 *
 * The followings are the available columns in table 'prodNotaCredito':
 * @property integer $id
 * @property integer $idComprobante
 * @property integer $nroItem
 * @property integer $idArtVenta
 * @property double $cantidad
 * @property string $fechaEntrega
 * @property double $precioUnitario
 * @property double $alicuotaIva
 * @property integer $cantidadEntregada
 * @property integer $nroPuestoVenta
 * @property string $descripcion
 *
 *
 * The followings are the available model relations:
 * @property NotaCredito $oidComprobante
 * @property ArticuloVenta $oArticuloVenta
 */
class ProdNotaDebito extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProdNotaDebito the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prodComprobante';
	}

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idComprobante, idArtVenta, descripcion', 'required'),
            array('idComprobante, nroItem, idArtVenta, cantidadEntregada, nroPuestoVenta', 'numerical', 'integerOnly'=>true),
            array('cantidad, precioUnitario, alicuotaIva', 'numerical'),
            array('descripcion', 'length', 'max'=>40),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, idComprobante, idArtVenta, cantidad, fechaEntrega, precioUnitario, alicuotaIva, cantidadEntregada, nroPuestoVenta, descripcion', 'safe', 'on'=>'search'),
        );
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oNotaDebito' => array(self::BELONGS_TO, 'NotaDebito', 'idComprobante'),
			'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
		);
	}

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return number_format(($this->cantidad * $this->precioUnitario),2, '.', '');

    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('application', 'ID'),
            'idComprobante' => Yii::t('application', 'Id NC'),
            'nroItem' => 'Nro Item',
            'idArtVenta' => Yii::t('application', 'Id Art Venta'),
            'cantidad' => Yii::t('application', 'Cantidad'),
            'fechaEntrega' => Yii::t('application', 'Fecha Entrega'),
            'precioUnitario' => Yii::t('application', 'Precio Unitario'),
            'alicuotaIva' => Yii::t('application', 'Alicuota Iva'),
            'cantidadEntregada' => Yii::t('application', 'Cantidad Entregada'),
            'nroPuestoVenta' => Yii::t('application', 'Nro Puesto Venta'),
            'descripcion' => Yii::t('application', 'Descripcion'),
        );
    }

    public function getIva(){
        return number_format(((floatval(str_replace(',','',$this->precioUnitario)) * $this->cantidad))*($this->alicuotaIva/100),2, '.', '');
    }

    public function getImporte(){
        return number_format((floatval(str_replace(',','',$this->precioUnitario)) * $this->cantidad),2, '.', '');
    }

    public function getPrecioUnitario(){
        return number_format(floatval(str_replace(',','',$this->precioUnitario)),2, '.', '');
    }

    public static function prods2VoucherItems($idComprobante,$tipoFactura){
        $aProds = self::model()->findAllByAttributes(array('idComprobante' => $idComprobante));
        $oProdFactura = new ProdFactura();

        if($tipoFactura == "A"){
            foreach ($aProds as $index => $oProdFactura) {
                $oAfipVoucherItem = new AfipVoucherItem();
                $oAfipVoucherItem->descripcion = $oProdFactura->descripcion;
                $oAfipVoucherItem->idVoucher = $oProdFactura->oFactura->idVoucherAfip;
                $oAfipVoucherItem->codigo = $oProdFactura->oArtVenta->getCodigo();
                $oAfipVoucherItem->cantidad = $oProdFactura->cantidad;
                //$oAfipVoucherItem->codigoUnidadMedida =
                $oAfipVoucherItem->precioUnitario = $oProdFactura->precioUnitario ;
                $oAfipVoucherItem->importeItem = $oProdFactura->getImporte();
                $oAfipVoucherItem->save();

            }
        }else{
            foreach ($aProds as $index => $oProdFactura) {
                $oAfipVoucherItem = new AfipVoucherItem();
                $oAfipVoucherItem->descripcion = $oProdFactura->descripcion;
                $oAfipVoucherItem->idVoucher = $oProdFactura->oFactura->idVoucherAfip;
                $oAfipVoucherItem->codigo = $oProdFactura->oArtVenta->getCodigo();
                $oAfipVoucherItem->cantidad = $oProdFactura->cantidad;
                //$oAfipVoucherItem->codigoUnidadMedida =
                $oAfipVoucherItem->precioUnitario = $oProdFactura->precioUnitario * (1-($oProdFactura->alicuotaIva / 100));
                $oAfipVoucherItem->importeItem = $oProdFactura->getImporte() * (1-($oProdFactura->alicuotaIva / 100));
                $oAfipVoucherItem->save();

            }
        }

    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('idComprobante',$this->idComprobante);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('cantidad',$this->cantidad);
        if ($this->fechaEntrega != null){
            $criteria->compare('fechaEntrega',ComponentesComunes::jpicker2db($this->fechaEntrega));
        }

        $criteria->compare('precioUnitario',$this->precioUnitario);
        $criteria->compare('alicuotaIva',$this->alicuotaIva);
        $criteria->compare('cantidadEntregada',$this->cantidadEntregada);
        $criteria->compare('nroPuestoVenta',$this->nroPuestoVenta);
        $criteria->compare('descripcion',$this->descripcion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,

        ));
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchWP()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('idComprobante',$this->idComprobante);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('cantidad',$this->cantidad);
        if ($this->fechaEntrega != null){
            $criteria->compare('fechaEntrega',ComponentesComunes::fechaComparada($this->fechaEntrega));
        }

        $criteria->compare('precioUnitario',$this->precioUnitario);
        $criteria->compare('cantidadEntregada',$this->cantidadEntregada);
        $criteria->compare('nroPuestoVenta',$this->nroPuestoVenta);
        $criteria->compare('descripcion',$this->descripcion,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    public function afterSave(){
        $oND = $this->oNotaDebito;
        $oND->recalcular();
        $oND->save();

        return parent::afterSave();
    }

}