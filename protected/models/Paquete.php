<?php

/**
 * This is the model class for table "paquete".
 *
 * The followings are the available columns in table 'paquete':
 * @property integer $id
 * @property integer $id_artVenta
 * @property integer $id_envase
 * @property double $cantidad
 * @property integer $listaComercial
 * @property integer $listaInstitucional
 * @property integer $activo
 *
 *
 * The followings are the available model relations:
 * @property ArticuloVenta $oArtVenta
 * @property Envase $oEnvase
 */

class Paquete extends CActiveRecord
{

    const iInactivo = 0;
    const iActivo = 1;

    const inactivo = 'Inactivo';
    const activo = 'Activo';

    public static $aEstado = array(
        self::iInactivo => self::inactivo,
        self::iActivo => self::activo,
    );

    const iSi = 1;
    const iNo = 0;

    public static $aSiNo = array(
        self::iSi => 'Si',
        self::iNo => 'No',
    );

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'paquete';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_artVenta, id_envase, cantidad', 'required'),
			array('id_artVenta, id_envase, listaComercial, listaInstitucional, activo', 'numerical', 'integerOnly'=>true),
			array('cantidad', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, id_artVenta, id_envase, cantidad, listaComercial, listaInstitucional, activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'id_artVenta'),
			'oEnvase' => array(self::BELONGS_TO, 'Envase', 'id_envase'),
            'aProdCarga' => array(self::HAS_MANY, 'ProdCarga', 'idPaquete'),
            'aProdDepuracion' => array(self::HAS_MANY, 'ProdDepuracion', 'idPaquete'),
            'aProdNotaPedido' => array(self::HAS_MANY, 'ProdNotaPedido', 'idPaquete'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_artVenta' => 'Id Art Venta',
			'id_envase' => 'Id Envase',
			'cantidad' => 'Cantidad',
            'listaComercial' => Yii::t('application', 'Lista Comercial'),
            'listaInstitucional' => Yii::t('application', 'Lista Institucional'),
            'activo' => 'Activo',
		);
	}

    public function getCantEnvase(){
        return $this->cantidad.' :: '.$this->oEnvase->descripcion;
    }

    public function getIdEnvase(){
        return $this->id_envase.'::'.$this->oEnvase->descripcion;
    }

	public function getEnvase(){
		return $this->oEnvase->descripcion.' X '.$this->cantidad;
	}


	public function defaultScope(){
	    return array(
            'condition'=>'activo='.self::iActivo,
        );
    }

    public function scopes()
    {
        return array(
            'todos'=>array(
                'order'=>'abs(cantidad) asc',
            ),
            'desc'=>array(
                'order'=>'abs(cantidad) desc',
            ),
            'activos'=>array(
                'condition'=>'activo='.self::iActivo,
            ),
            'inactivos'=>array(
                'condition'=>'activo='.self::iInactivo,
            ),
            'activosEinactivos'=>array(
                'condition'=>'',
            ),
        );
    }

    public function masChicos($idEnvase,$idArtVenta,$cantidad){
        $this->getDbCriteria()->mergeWith(array(
            'order'=>'cantidad DESC',
            'condition'=>'id_artVenta=:idArtVenta and id_envase=:idEnvase and cantidad < :cantidad ',
            'params'=>array(':idEnvase' => idEnvase, ':idArtVenta' => $idArtVenta, ':cantidad' => $cantidad ),
        ));
        return $this;
    }

    public function masChico($idEnvase,$idArtVenta){
        $this->getDbCriteria()->mergeWith(array(
            'order'=>'cantidad DESC',
            'condition'=>'id_artVenta=:idArtVenta and id_envase=:idEnvase and cantidad = 1',
            'params'=>array(':idEnvase' => $idEnvase, ':idArtVenta' => $idArtVenta),
        ));
        return $this;
    }

	/**
 * Retrieves a list of models based on the current search/filter conditions.
 *
 * Typical usecase:
 * - Initialize the model fields with values from filter form.
 * - Execute this method to get CActiveDataProvider instance which will filter
 * models according to data in model fields.
 * - Pass data provider to CGridView, CListView or any similar widget.
 *
 * @return CActiveDataProvider the data provider that can return the models
 * based on the search/filter conditions.
 */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('id_artVenta',$this->id_artVenta);
        $criteria->compare('id_envase',$this->id_envase);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('listaComercial',$this->listaComercial);
        $criteria->compare('listaInstitucional',$this->listaInstitucional);
        $criteria->compare('activo',$this->activo);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchComercial()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArtVenta');

        $criteria->compare('id',$this->id);
        $criteria->compare('id_artVenta',$this->id_artVenta);
        $criteria->compare('id_envase',$this->id_envase);
        $criteria->compare('cantidad',$this->cantidad);

        $criteria->addCondition('listaComercial = 1');
        $criteria->addCondition('activo = 1');

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function searchInstitucional()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArtVenta');

        $criteria->compare('id',$this->id);
        $criteria->compare('id_artVenta',$this->id_artVenta);
        $criteria->compare('id_envase',$this->id_envase);
        $criteria->compare('cantidad',$this->cantidad);

        $criteria->addCondition('listaInstitucional = 1');


        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paquete the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    // EN LUGAR DE BORRAR, LO PASA A INACTIVO PARA NO PERDER HISTORICO, Y NO GENERAR INCONSISTENCIAS
    public function beforeDelete(){
        $this->activo = self::iInactivo;
        $this->save();
        return false;
    }
}
