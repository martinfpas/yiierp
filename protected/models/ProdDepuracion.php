<?php

/**
 * This is the model class for table "prodDepuracion".
 *
 * The followings are the available columns in table 'prodDepuracion':
 * @property integer $id
 * @property integer $idDepuracion
 * @property integer $idArtVenta
 * @property integer $idPaquete
 * @property integer $cantidad
 * @property integer $idProdCarga
 *
 * The followings are the available model relations:
 * @property ArticuloVenta $oIdArtVenta
 * @property Depuracion $oIdDepuracion
 * @property Paquete $oIdPaquete
 * @property ProdCarga $oIdProdCarga
 */
class ProdDepuracion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProdDepuracion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'prodDepuracion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idDepuracion, idArtVenta, idPaquete', 'required'),
			array('idDepuracion, idArtVenta, idPaquete, cantidad, idProdCarga', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, idDepuracion, idArtVenta, idPaquete, cantidad, idProdCarga', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'oArtVenta' => array(self::BELONGS_TO, 'ArticuloVenta', 'idArtVenta'),
			'oDepuracion' => array(self::BELONGS_TO, 'Depuracion', 'idDepuracion'),
			'oPaquete' => array(self::BELONGS_TO, 'Paquete', 'idPaquete'),
			'oProdCarga' => array(self::BELONGS_TO, 'ProdCarga', 'idProdCarga'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
	    return array(
		            'id' => Yii::t('application', 'ID'),
		            'idDepuracion' => Yii::t('application', 'Id Depuracion'),
		            'idArtVenta' => Yii::t('application', 'Id Art Venta'),
		            'idPaquete' => Yii::t('application', 'Id Paquete'),
		            'cantidad' => Yii::t('application', 'Cantidad'),
		            'idProdCarga' => Yii::t('application', 'Id Prod Carga'),
		    );
	}

    /**
     * @return int
     */
    public function getCantXbulto()
    {
        return $this->oPaquete->cantidad;
    }

    public function getCantbultoPres()
    {
        return $this->oPaquete->cantidad.'   x '.$this->oArtVenta->contenido. ' '.$this->oArtVenta->uMedida;
    }

    public function getEnvase()
    {
        return $this->oPaquete->oEnvase->descripcion;
    }

    public function getBultos(){
        return $this->cantidad / $this->oPaquete->cantidad;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('idDepuracion',$this->idDepuracion);
		$criteria->compare('idArtVenta',$this->idArtVenta);
		$criteria->compare('idPaquete',$this->idPaquete);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('idProdCarga',$this->idProdCarga);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchWP()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->with = array('oArtVenta');

        $criteria->compare('id',$this->id);
        $criteria->compare('idDepuracion',$this->idDepuracion);
        $criteria->compare('idArtVenta',$this->idArtVenta);
        $criteria->compare('idPaquete',$this->idPaquete);
        $criteria->compare('cantidad',$this->cantidad);
        $criteria->compare('idProdCarga',$this->idProdCarga);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => false,
            'sort'=> array(
                'defaultOrder' => 'oArtVenta.descripcion asc',
                'attributes' => array(
                    'idArtVenta' => array(
                        'asc' => 'oArtVenta.descripcion asc',
                        'desc' => 'oArtVenta.descripcion desc',
                    )
                )
            ),
        ));
    }
}