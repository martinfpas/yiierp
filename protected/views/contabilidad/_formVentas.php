<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 6/12/2019
 * Time: 00:36
 */

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'comprobante-form',
    'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('contabilidad/ListadoCiti'),
));

?>
    <h3>Listado Citi compras y ventas</h3>


    <div class="content-fluid">
        <div class="row-fluid">

            <div class="span1">
                <?php

                echo 'Periodo';

                $this->widget('CMaskedTextField', array(
                    //'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'Citi[periodo]', // Cambiar 'NotaPedido por el modelo que corresponda
                    ///'model' => $model,
                    //'attribute' => 'periodo',
                    'mask' => '99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;width:75px;',
                        'class'=>'span12',
                        'title'=>'Hacer click en el campo y seleccionar la fecha',
                        'tabindex' => 2
                    ),
                ));

                ?>
            </div>
            <div class="span3">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=> 'Guardar',
                    'htmlOptions' => array(
                        'style'=> 'margin-top:25px;'
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>