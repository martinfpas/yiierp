<?php
/* @var $this ContabilidadController */

$this->breadcrumbs=array(
	'Contabilidad'=>array('/contabilidad'),
	'Citi',
);
?>
<h3><?php echo $this->id . '/' . $this->action->id; ?></h3>

<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;



if(isset($_POST['Citi']['periodo'])){
    $ventas = new Citi('V', 'txt', '','S',$_POST['Citi']['periodo']);
    $alicuotasVenta = new Citi('AV', 'txt', '','S',$_POST['Citi']['periodo']);

    $compras = new Citi('C', 'txt', '','S',$_POST['Citi']['periodo']);
    $alicuotasCompras = new Citi('AC', 'txt', '','S',$_POST['Citi']['periodo']);

    // la tabla datos_cabecera debe existir en la base y ser carga po otro formulario. ya que se deben ingfresar datos de cargas
    // sociales, aportes patronales, llevar ecuencas y llevar otros valores respecto al CF de IVa.
    //
    // $cabecera = new CITI('Cab', 'txt', '','S','0919');


    $host= Yii::app()->getBaseUrl();

    $urlFinal = '/upload/';


    echo '<h3>Ventas</h3>';
    echo '<a href="' . $host . $urlFinal. $ventas->GetArchivoV().'">*'.$ventas->GetArchivoV().'</a>';
    echo '<br />';
    echo '<a href="' . $host . $urlFinal. $alicuotasVenta->GetArchivoAV().'">*'.$alicuotasVenta->GetArchivoAV().'</a>';
    echo '<br />';
    echo '<h3>Compras</h3>';
    echo '<a href="' . $host . $urlFinal. $compras->GetArchivoC().'">*'.$compras->GetArchivoC().'</a>';
    echo '<br />';
    echo '<a href="' . $host . $urlFinal. $alicuotasCompras->GetArchivoAC().'">*'.$alicuotasCompras->GetArchivoAC().'</a>';

}



?>