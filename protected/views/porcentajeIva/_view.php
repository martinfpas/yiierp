<?php
/* @var $this PorcentajeIvaController */
/* @var $data PorcentajeIva */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idImpuesto')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idImpuesto),array('view','id'=>$data->idImpuesto)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->Porcentaje); ?>
	<br />


</div>