<?php
/* @var $this PorcentajeIvaController */
/* @var $model PorcentajeIva */

$this->breadcrumbs=array(
	'Porcentaje Ivas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de PorcentajeIva', 'url'=>array('admin')),
);
?>

<h1>Creando PorcentajeIva</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>