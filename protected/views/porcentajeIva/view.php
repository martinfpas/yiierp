<?php
$this->breadcrumbs=array(
	'Porcentaje Ivas'=>array('admin'),
	$model->idImpuesto,
);

$this->menu=array(
	array('label'=>'Nuevo PorcentajeIva', 'url'=>array('create')),
	array('label'=>'Modificar PorcentajeIva', 'url'=>array('update', 'id'=>$model->idImpuesto)),
	array('label'=>'Borrar PorcentajeIva', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idImpuesto),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM PorcentajeIva', 'url'=>array('admin')),
);
?>

<h1>Ver PorcentajeIva #<?php echo $model->idImpuesto; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'idImpuesto',
		'Porcentaje',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
