<?php
$this->breadcrumbs=array(
	'Porcentaje Ivas'=>array('index'),
	$model->idImpuesto=>array('view','id'=>$model->idImpuesto),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo PorcentajeIva', 'url'=>array('create')),
	array('label'=>'Ver PorcentajeIva', 'url'=>array('view', 'id'=>$model->idImpuesto)),
	array('label'=>'ABM PorcentajeIva', 'url'=>array('admin')),
);
?>

	<h1>Modificando PorcentajeIva <?php echo $model->idImpuesto; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>