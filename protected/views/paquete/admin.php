<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Paquete', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#paquete-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Paquetes</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'paquete-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'id_artVenta',
		'id_envase',
		'cantidad',
        array(
            'name' => 'listaComercial',
            'value' => 'Paquete::$aEstado[$data->listaComercial]',
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
        array(
            'name' => 'listaInstitucional',
            'value' => 'Paquete::$aEstado[$data->listaInstitucional]',
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
        array(
            'name' => 'activo',
            'value' => 'Paquete::$aEstado[$data->activo]',
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
