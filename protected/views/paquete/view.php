<?php
$this->breadcrumbs=array(
	'Paquetes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Paquete', 'url'=>array('create')),
	array('label'=>'Modificar Paquete', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Paquete', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Paquete', 'url'=>array('admin')),
);
?>

<h1>Ver Paquete #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_artVenta',
		'id_envase',
		'cantidad',
        array(
            'name' => 'listaComercial',
            'value' => Paquete::$aEstado[$model->listaComercial],
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
        array(
            'name' => 'listaInstitucional',
            'value' => Paquete::$aEstado[$model->listaInstitucional],
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
        array(
            'name' => 'activo',
            'value' => Paquete::$aEstado[$model->activo],
            'headerHtmlOptions' => array('style' => 'width: 100px'),
            'filter' => Paquete::$aEstado,
        ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
