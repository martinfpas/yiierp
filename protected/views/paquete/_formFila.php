
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoPaquete" id="masPaquete" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoPaquete" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'paquete-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('Paquete/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'paquete-grid',
			'idMas' => 'masPaquete',
			'idDivNuevo' => 'nuevoPaquete',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorPaquete"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okPaquete">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		<?php echo $form->hiddenField($model,'id_artVenta'); ?>
		<div class="span3">
			<?php 
			echo $form->labelEx($model,'id_envase');
			echo $form->dropDownList($model,'id_envase',CHtml::listData(Envase::model()->todos()->findAll(),'id_envase','IdTitulo'),array('emtpy'=>'Seleccionar uno','tabindex'=> 1)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12','tabindex'=> 2)); ?>
		</div>
        <div class="span3"><?php
            if(!$model->isNewRecord){
                echo $form->labelEx($model,'activo');
                echo $form->dropDownList($model,'activo', Paquete::$aSiNo, array( 'style'=>''));
            }
            ?>
        </div>
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'paquete-grid',
'dataProvider'=>$aPaquete->search(),
//'filter'=>$apaquete,
'columns'=>array(
		array(
				'name' => 'id_envase',
				'htmlOptions' => array('style' => ''),
		),
		array(
				'name' => 'id_envase',
				'header' => 'Descripcion',
				'value' => '$data->oEnvase->descripcion',
				'htmlOptions' => array('style' => ''),
		),
		'cantidad',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("Paquete/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Paquete/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>