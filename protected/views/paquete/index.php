
/* @var $this PaqueteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Paquetes',
);

$this->menu=array(
	array('label'=>'Nuevo Paquete', 'url'=>array('create')),
	array('label'=>'Gestion de Paquete', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>