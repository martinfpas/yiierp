<?php
$this->breadcrumbs=array(
	'Paquetes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Paquete', 'url'=>array('create')),
	array('label'=>'Ver Paquete', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Paquete', 'url'=>array('admin')),
);
?>

	<h1>Modificando Paquete <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>