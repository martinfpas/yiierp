<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Paquete', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#paquete-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'paquete-grid',
'dataProvider'=>$model->searchInstitucional(),
'template' => '{items}',
'columns'=>array(
        array(
            'name' => 'Codigo',
            'value' => '$data->oArtVenta->Codigo',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'Descripcion',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'header' => 'Bolsa',
            'value' => '$data->cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'Estuche',
            'header' => 'Unidades por bulto (caja,Bolsa,Bidon)',
            'value' => '$data->oEnvase->descripcion',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'Precio',
            'value' => 'number_format(($data->oArtVenta->precio * 1.315),2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:90px'),
        ),
        array(
            'name' => 'Total',
            'header' => 'Total por bulto',
            'value' => 'number_format(($data->oArtVenta->precio *  1.315 * $data->cantidad),2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:90px'),
        ),
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
	),
)); ?>
