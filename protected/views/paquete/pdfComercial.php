<?php

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'paquete-grid',
'dataProvider'=>$model->searchComercial(),
'template' => '{items}',
'columns'=>array(
        array(
            'name' => 'Codigo',
			'value' => '$data->oArtVenta->Codigo',
			'htmlOptions' => array('style' => ''),
		),
        array(
            'name' => 'Descripcion',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'Bolsa',
			'value' => '$data->cantidad',
			'htmlOptions' => array('style' => 'text-align:right;'),
		),

        array(
            'name' => 'Estuche',
            'header' => 'Unidades por bulto (caja,Bolsa,Bidon)',
			'value' => '$data->oEnvase->descripcion',
			'htmlOptions' => array('style' => ''),
		),
        array(
            'name' => 'Precio',
            'value' => 'number_format(($data->oArtVenta->precio),2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:90px'),
        ),
        array(
            'name' => 'Total',
            'header' => 'Total por bulto',
            'value' => 'number_format(($data->oArtVenta->precio * $data->cantidad),2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:90px'),
        ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

	),
)); ?>
