<?php
/* @var $this PaqueteController */
/* @var $model Paquete */

$this->breadcrumbs=array(
	'Paquetes'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Paquete', 'url'=>array('admin')),
);
?>

<h1>Creando Paquete</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>