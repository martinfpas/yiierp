<?php
/* @var $this PaqueteController */
/* @var $data Paquete */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_artVenta')); ?>:</b>
	<?php echo CHtml::encode($data->id_artVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_envase')); ?>:</b>
	<?php echo CHtml::encode($data->id_envase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />


</div>