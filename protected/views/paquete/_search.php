<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'id_artVenta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'id_envase',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>

        <?php echo $form->labelEx($model,'activo'); ?>
        <?php echo $form->dropDownList($model,'activo', array('0' => Paquete::inactivo,'1' => Paquete::activo),    array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
