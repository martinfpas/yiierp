<?php
/* @var $this CategoriaIvaController */
/* @var $model CategoriaIva */

$this->breadcrumbs=array(
	'Categoria Ivas'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo CategoriaIva', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#categoria-iva-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Categoria Ivas</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'categoria-iva-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		
		array(
				'name' => 'id_categoria',
				'header' => 'ID',
				'headerHtmlOptions' => array('style' => 'width:50px'),
				'htmlOptions' => array('style' => 'text-align:right;'),
		),
		'nombre',
		'descrip',
		array(
			'name' => 'discIva',
			'value' => '($data->discIva == 0)? "No" : "Si" ',
			'htmlOptions' => array('style' => ''),
			'filter' => array('1'=>'Si','0'=>'No'),
		),
		
		array(
			'name' => 'tipoFact',
			'headerHtmlOptions' => array('style' => 'width:65px'),
			'htmlOptions' => array('style' => 'text-align:center;'),
		),
		
		array(
				'name' => 'alicuota',
				'header' => 'Fecha Vig.',
				'headerHtmlOptions' => array('style' => 'width:75px'),
				'htmlOptions' => array('style' => ''),
		),
		array(
				'name' => 'fechaVigencia',
				'header' => 'Fecha Vig.',
				'headerHtmlOptions' => array('style' => 'width:75px'),
				'htmlOptions' => array('style' => ''),
		),
        array(
            'name' => 'agregaIva',
            'value' => '($data->agregaIva == 0)? "No" : "Si" ',
            'htmlOptions' => array('style' => ''),
            'filter' => array('1'=>'Si','0'=>'No'),
        ),
		/*
		'nCopias',
		'catCorto',
		
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
