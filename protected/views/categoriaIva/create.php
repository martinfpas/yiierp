<?php
/* @var $this CategoriaIvaController */
/* @var $model CategoriaIva */

$this->breadcrumbs=array(
	'Categoria Ivas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de CategoriaIva', 'url'=>array('admin')),
);
?>

<h1>Creando CategoriaIva</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>