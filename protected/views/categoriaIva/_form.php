<?php 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'categoria-iva-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCategoriaIva"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCategoriaIva">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

        <div class="span3">
            <?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>30,'tabindex'=>'1')); ?>
        </div>
        <div class="span3">
            <?php echo $form->textFieldRow($model,'descrip',array('class'=>'span12','maxlength'=>30,'tabindex'=>'2')); ?>
        </div>
        <div class="span3">
            <?php 
            	echo $form->labelEx($model,'discIva');
            	echo $form->dropDownList($model,'discIva',array('1'=>'Si','0'=>'No'),array('class'=>'span12','tabindex'=>'3'));
            ?>
        </div>

        <div class="span3">
            <?php echo $form->textFieldRow($model,'tipoFact',array('class'=>'span12','maxlength'=>1,'tabindex'=>'4')); ?>
        </div>
    </div>        
	<div class="row-fluid">
        <div class="span2">
            <?php echo $form->textFieldRow($model,'fechaVigencia',array('class'=>'span12','tabindex'=>'5')); ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'nCopias',array('class'=>'span12','tabindex'=>'6')); ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'alicuota',array('class'=>'span12','tabindex'=>'8')); ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'catCorto',array('class'=>'span12','maxlength'=>5,'tabindex'=>'7')); ?>
        </div>
        <div class="span2">
            <?php
            echo $form->labelEx($model,'agregaIva');
            echo $form->dropDownList($model,'agregaIva',array('1'=>'Si','0'=>'No'),array('class'=>'span12','tabindex'=>'9'));
            ?>
        </div>
        <div class="span2">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;'
                )
            )); ?>
        </div>
    </div>
</div>



<?php $this->endWidget(); ?>
