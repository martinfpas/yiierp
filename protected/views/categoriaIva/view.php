<?php
$this->breadcrumbs=array(
	'Categoria Ivas'=>array('admin'),
	$model->id_categoria,
);

$this->menu=array(
	array('label'=>'Nuevo CategoriaIva', 'url'=>array('create')),
	array('label'=>'Modificar CategoriaIva', 'url'=>array('update', 'id'=>$model->id_categoria)),
	array('label'=>'Borrar CategoriaIva', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_categoria),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM CategoriaIva', 'url'=>array('admin')),
);
?>

<h1>Ver CategoriaIva #<?php echo $model->id_categoria; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_categoria',
		'nombre',
		'bool',
		'tipoFact',
		'catCorto',
		'alicuota',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
