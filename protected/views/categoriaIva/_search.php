<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id_categoria',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>30)); ?>

		<?php echo $form->textFieldRow($model,'descrip',array('class'=>'span12','maxlength'=>30)); ?>

		<?php echo $form->textFieldRow($model,'discIva',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'tipoFact',array('class'=>'span12','maxlength'=>1)); ?>

		<?php echo $form->textFieldRow($model,'fechaVigencia',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nCopias',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'catCorto',array('class'=>'span12','maxlength'=>5)); ?>

		<?php echo $form->textFieldRow($model,'alicuota',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
