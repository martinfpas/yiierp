<?php
/* @var $this CategoriaIvaController */
/* @var $data CategoriaIva */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_categoria')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_categoria),array('view','id'=>$data->id_categoria)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descrip')); ?>:</b>
	<?php echo CHtml::encode($data->descrip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discIva')); ?>:</b>
	<?php echo CHtml::encode($data->discIva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoFact')); ?>:</b>
	<?php echo CHtml::encode($data->tipoFact); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaVigencia')); ?>:</b>
	<?php echo CHtml::encode($data->fechaVigencia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nCopias')); ?>:</b>
	<?php echo CHtml::encode($data->nCopias); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('catCorto')); ?>:</b>
	<?php echo CHtml::encode($data->catCorto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alicuota')); ?>:</b>
	<?php echo CHtml::encode($data->alicuota); ?>
	<br />

	*/ ?>

</div>