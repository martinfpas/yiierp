<?php
$this->breadcrumbs=array(
	'Categoria Ivas'=>array('index'),
	$model->id_categoria=>array('view','id'=>$model->id_categoria),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo CategoriaIva', 'url'=>array('create')),
	array('label'=>'Ver CategoriaIva', 'url'=>array('view', 'id'=>$model->id_categoria)),
	array('label'=>'Gestion de CategoriaIva', 'url'=>array('admin')),
);
?>

	<h1>Modificando CategoriaIva <?php echo $model->id_categoria; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>