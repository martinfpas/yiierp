
/* @var $this CategoriaIvaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Categoria Ivas',
);

$this->menu=array(
	array('label'=>'Nuevo CategoriaIva', 'url'=>array('create')),
	array('label'=>'ABM CategoriaIva', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>