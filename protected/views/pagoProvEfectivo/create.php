<?php
/* @var $this PagoProvEfectivoController */
/* @var $model PagoProvEfectivo */

$this->breadcrumbs=array(
	PagoProvEfectivo::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.PagoProvEfectivo::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=PagoProvEfectivo::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>