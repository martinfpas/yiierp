
/* @var $this PagoProvEfectivoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'PagoProvEfectivo::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo PagoProvEfectivo', 'url'=>array('create')),
	array('label'=>'Gestion de PagoProvEfectivo', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>