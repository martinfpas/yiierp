<?php
$this->breadcrumbs=array(
	PagoProvEfectivo::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProvEfectivo::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.PagoProvEfectivo::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.PagoProvEfectivo::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.PagoProvEfectivo::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=PagoProvEfectivo::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idPagoProveedor',
		'monto',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
