
/* @var $this TipoCuentaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Tipo Cuentas',
);

$this->menu=array(
	array('label'=>'Nuevo TipoCuenta', 'url'=>array('create')),
	array('label'=>'ABM TipoCuenta', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>