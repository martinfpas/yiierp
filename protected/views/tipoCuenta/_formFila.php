
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoTipoCuenta" id="masTipoCuenta" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoTipoCuenta" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'tipo-cuenta-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('TipoCuenta/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'tipo-cuenta-grid',
			'idMas' => 'masTipoCuenta',
			'idDivNuevo' => 'nuevoTipoCuenta',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorTipoCuenta"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okTipoCuenta">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'tipo-cuenta-grid',
'dataProvider'=>$aTipoCuenta->search(),
'columns'=>array(
		'id',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("TipoCuenta/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("TipoCuenta/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>