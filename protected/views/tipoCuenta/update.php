<?php
$this->breadcrumbs=array(
	'Tipo Cuentas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo TipoCuenta', 'url'=>array('create')),
	array('label'=>'Ver TipoCuenta', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de TipoCuenta', 'url'=>array('admin')),
);
?>

	<h1>Modificando TipoCuenta <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>