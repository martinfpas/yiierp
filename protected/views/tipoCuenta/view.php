<?php
$this->breadcrumbs=array(
	'Tipo Cuentas'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo TipoCuenta', 'url'=>array('create')),
	array('label'=>'Modificar TipoCuenta', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar TipoCuenta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de TipoCuenta', 'url'=>array('admin')),
);
?>

<h1>Ver TipoCuenta #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
