<?php
/* @var $this TipoCuentaController */
/* @var $model TipoCuenta */

$this->breadcrumbs=array(
	'Tipo Cuentas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de TipoCuenta', 'url'=>array('admin')),
);
?>

<h1>Creando TipoCuenta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>