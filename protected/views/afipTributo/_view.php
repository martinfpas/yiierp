<?php
/* @var $this AfipTributoController */
/* @var $data AfipTributo */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('idTributo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idTributo),array('view','id'=>$data->idTributo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id')); ?>:</b>
	<?php echo CHtml::encode($data->Id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Desc')); ?>:</b>
	<?php echo CHtml::encode($data->Desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('BaseImp')); ?>:</b>
	<?php echo CHtml::encode($data->BaseImp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Importe')); ?>:</b>
	<?php echo CHtml::encode($data->Importe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Alic')); ?>:</b>
	<?php echo CHtml::encode($data->Alic); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idVoucher')); ?>:</b>
	<?php echo CHtml::encode($data->idVoucher); ?>
	<br />


</div>