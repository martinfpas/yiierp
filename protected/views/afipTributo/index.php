
/* @var $this AfipTributoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Afip Tributos',
);

$this->menu=array(
	array('label'=>'Nuevo AfipTributo', 'url'=>array('create')),
	array('label'=>'Gestion de AfipTributo', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>