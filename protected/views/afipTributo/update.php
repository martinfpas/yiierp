<?php
$this->breadcrumbs=array(
	'Afip Tributos'=>array('index'),
	$model->idTributo=>array('view','id'=>$model->idTributo),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo AfipTributo', 'url'=>array('create')),
	array('label'=>'Ver AfipTributo', 'url'=>array('view', 'id'=>$model->idTributo)),
	array('label'=>'Gestion de AfipTributo', 'url'=>array('admin')),
);
?>

	<h1>Modificando AfipTributo <?php echo $model->idTributo; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>