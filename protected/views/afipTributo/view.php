<?php
$this->breadcrumbs=array(
	'Afip Tributos'=>array('admin'),
	$model->idTributo,
);

$this->menu=array(
	array('label'=>'Nuevo AfipTributo', 'url'=>array('create')),
	array('label'=>'Modificar AfipTributo', 'url'=>array('update', 'id'=>$model->idTributo)),
	array('label'=>'Borrar AfipTributo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idTributo),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de AfipTributo', 'url'=>array('admin')),
);
?>

<h1>Ver AfipTributo #<?php echo $model->idTributo; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'idTributo',
		'Id',
		'Desc',
		'BaseImp',
		'Importe',
		'Alic',
		'idVoucher',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
