<?php
/* @var $this AfipTributoController */
/* @var $model AfipTributo */

$this->breadcrumbs=array(
	'Afip Tributos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de AfipTributo', 'url'=>array('admin')),
);
?>

<h1>Creando AfipTributo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>