<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'idTributo',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Desc',array('class'=>'span12','maxlength'=>80)); ?>

		<?php echo $form->textFieldRow($model,'BaseImp',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Importe',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Alic',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idVoucher',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
