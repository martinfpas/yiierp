
/* @var $this PagoAlContadoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Pago Al Contados',
);

$this->menu=array(
	array('label'=>'Nuevo PagoAlContado', 'url'=>array('create')),
	array('label'=>'ABM PagoAlContado', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>