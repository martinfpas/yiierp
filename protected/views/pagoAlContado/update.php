<?php
$this->breadcrumbs=array(
	'Pago Al Contados'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo PagoAlContado', 'url'=>array('create')),
	array('label'=>'Ver PagoAlContado', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de PagoAlContado', 'url'=>array('admin')),
);
?>

	<h1>Modificando PagoAlContado <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>