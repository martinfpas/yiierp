<?php
/* @var $this PagoAlContadoController */
/* @var $model PagoAlContado */

$this->breadcrumbs=array(
	'Pago Al Contados'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de PagoAlContado', 'url'=>array('admin')),
);
?>

<h1>Creando PagoAlContado</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>