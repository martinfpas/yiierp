<?php
$this->breadcrumbs=array(
	'Pago Al Contados'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo PagoAlContado', 'url'=>array('create')),
	array('label'=>'Modificar PagoAlContado', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar PagoAlContado', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de PagoAlContado', 'url'=>array('admin')),
);
?>

<h1>Ver PagoAlContado #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idPagodeCliente',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
