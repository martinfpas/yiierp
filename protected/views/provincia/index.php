
/* @var $this ProvinciaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Provincias',
);

$this->menu=array(
	array('label'=>'Nuevo Provincia', 'url'=>array('create')),
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>