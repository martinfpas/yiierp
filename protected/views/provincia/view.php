<?php
$this->breadcrumbs=array(
	'Provincias'=>array('admin'),
	$model->id_provincia,
);

$this->menu=array(
	array('label'=>'Nueva', 'url'=>array('create')),
	array('label'=>'Modificar ', 'url'=>array('update', 'id'=>$model->id_provincia)),
	array('label'=>'Borrar ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_provincia),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>

<h1>Ver Provincia #<?php echo $model->nombre; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_provincia',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
