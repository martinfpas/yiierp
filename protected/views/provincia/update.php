<?php
$this->breadcrumbs=array(
	'Provincias'=>array('index'),
	$model->id_provincia=>array('view','id'=>$model->id_provincia),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nueva', 'url'=>array('create')),
	array('label'=>'Ver ', 'url'=>array('view', 'id'=>$model->id_provincia)),
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>

	<h1>Modificando Provincia <?php echo $model->nombre; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>