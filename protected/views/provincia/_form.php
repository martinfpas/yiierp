<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/tabindex.js");
	
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'provincia-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorProvincia"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okProvincia">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'id_provincia',array('class'=>'span12','maxlength'=>1,'tabindex'=>'1')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>40,'tabindex'=>'2')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
