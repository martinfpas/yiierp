<?php
/* @var $this ProvinciaController */
/* @var $model Provincia */

$this->breadcrumbs=array(
	'Provincias'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Provincia', 'url'=>array('admin')),
);
?>

<h1>Creando Provincia</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>