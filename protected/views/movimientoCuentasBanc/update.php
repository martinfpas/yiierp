<?php
$this->breadcrumbs=array(
	MovimientoCuentasBanc::model()->getAttributeLabel('models') => array('admin'),
	MovimientoCuentasBanc::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM '.MovimientoCuentasBanc::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=MovimientoCuentasBanc::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>