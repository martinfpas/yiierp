<?php
/* @var $model MovimientoCuentasBanc */
$this->breadcrumbs=array(
	MovimientoCuentasBanc::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM '.MovimientoCuentasBanc::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=MovimientoCuentasBanc::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'name' => 'idCuenta',
                        'value' => $model->oCuenta->Title,
                        'header' => 'Cuenta',
                        //'htmlOptions' => array('style' => ''),
                        //'headerHtmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'tipoMovimiento',
                        'value' => $model->oTipoMovimiento->Descripcion,
                    ),
                    'codigoMovimiento',
                    array(
                        'name' => 'fechaMovimiento',
                        'value' => ComponentesComunes::fechaFormateada($model->fechaMovimiento),
                    ),
                ),
            ));
            ?>
        </div>
        <div class="span6">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'name' => 'idMedio',
                        'value' => $model->oMedio->Descripcion,
                    ),
                    'Nro_operacion',
                    'Observacion',
                    array(
                        'name' => 'Importe',
                        'value' => "$" . number_format($model->Importe, 2),
                    ),
                    /*
                        array(
                            'name' => '',
                            'value' => '',
                            'header' => '',
                            'htmlOptions' => array('style' => ''),
                            'headerHtmlOptions' => array('style' => ''),
                        ),
                    */
                ),
            ));
            ?>
        </div>
    </div>
</div>

<?php

if (sizeof($model->aCheque3RoMovBancario) > 0){
    $dataProvider=new CArrayDataProvider($model->aCheque3RoMovBancario,array(
            'pagination'=> false,
        )
    );
    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'cheque3-ro-mov-bancario-grid',
        'dataProvider'=>$dataProvider,
        'columns'=>array(
            array(
                'name' => 'nroCheque',
                'value' =>'$data->oCheque3ro->nroCheque',
                'htmlOptions' => array('style' => 'width:85px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:85px;text-align:right;'),
            ),
            array(
                'name' => 'fecha',
                'value' => 'ComponentesComunes::fechaFormateada($data->oCheque3ro->fecha)',
                'htmlOptions' => array('style' => 'width:75px;text-align:right;'), // ESTILO VALORES
                'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),  // ESTILO CABECERA
            ),
            array(
                'name' => 'banco',
                'value' =>'($data->oCheque3ro->oBanco)? $data->oCheque3ro->oBanco->nombre :""',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => 'width:180px;'),
            ),
            array(
                'name' => 'sucursal',
                'value' =>'($data->oCheque3ro->oSucursal)? $data->oCheque3ro->oSucursal->nombre :""',
                'htmlOptions' => array('style' => ''),
            ),
            array(
                'name' => 'importe',
                'value' => '"$".number_format($data->oCheque3ro->importe,2)',
                'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            ),
        ),
    ));
}


if (sizeof($model->aChequePropioMovBancario) > 0){


    $dataProviderPropio = new CArrayDataProvider($model->aChequePropioMovBancario,array(
            'pagination' => false,
        )
    );

    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'cheque-propio-grid',
        'dataProvider'=>$dataProviderPropio,
        'columns'=>array(
            array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'width:60px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:60px;text-align:center;'),
            ),
            array(
                'name' => 'nroCheque',
                'value' => '($data->oChequePropio)? $data->oChequePropio->nroCheque :"" ',
                'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
            ),
            array(
                'name' => 'fecha',
                'value' => '($data->oChequePropio)? ComponentesComunes::fechaFormateada($data->oChequePropio->fecha) : ""',
                'htmlOptions' => array('style' => 'width:80px;'),
                'headerHtmlOptions' => array('style' => 'width:80px;'),
            ),
            array(
                'name' => 'id_cuenta',
                'value' => '$data->oMovimiento->oCuenta->title',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
            array(
                'name' => 'importe',
                'value' => '($data->oChequePropio)? "$".number_format($data->oChequePropio->importe,2) : ""',
                'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
            ),
        ),
    ));
}
?>
