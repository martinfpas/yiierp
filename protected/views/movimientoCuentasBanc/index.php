
/* @var $this MovimientoCuentasBancController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"MovimientoCuentasBanc::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo MovimientoCuentasBanc', 'url'=>array('create')),
	array('label'=>'ABM MovimientoCuentasBanc', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>