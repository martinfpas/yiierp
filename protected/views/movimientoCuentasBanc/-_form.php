<?php
/* @var $model MovimientoCuentasBanc */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/jquery.yii.js");
$cs->registerScriptFile($baseUrl."/js/bootstrap-tagsinput.min.js");
$cs->registerCssFile($baseUrl.'/css/bootstrap-tagsinput.css');

$scriptOrigen = '';
if (sizeof($model->aCheque3RoMovBancario) > 0){
    $scriptOrigen = "$('#origenCheque').val('terceros');$('#origenCheque').trigger('change');";
}

if (sizeof($model->aChequePropioMovBancario) > 0){
    $scriptOrigen = "$('#origenCheque').val('propios');$('#origenCheque').trigger('change');";
}

$cs->registerScript('movimCBForm',"
    $(document).ready(function(){
        toggleOrigen($('#MovimientoCuentasBanc_idMedio').val());        
        
        ele = $('#MovimientoCuentasBanc_idsCheques3Ro');
        ele.tagsinput({
              allowDuplicates: false,
              itemValue: 'value',
              itemText: 'text',
              itemImporte: 'importe',
        });
        
        ele2 = $('#MovimientoCuentasBanc_idsChequesPpios');
        ele2.tagsinput({
              allowDuplicates: false,
              itemValue: 'value',
              itemText: 'text',
              itemImporte: 'importe',
        });
        
        
        
        $scriptOrigen
                     
    });
    
    function updateImporte(){
        var datos = 'id='+'".$model->id."';
        $.ajax({
            url: '".$this->createUrl('MovimientoCuentasBanc/Json')."',
            data : datos,
            type: 'GET',
            success: function(data){
                var Movimiento = JSON.parse(data);
                $('#MovimientoCuentasBanc_Importe').val(Movimiento.Importe);    
            },
            error: function(x,y,z){
                
            },
        });
    }
    
    // SE BORRA UN CHEQUE DE TERCERO
    $('#MovimientoCuentasBanc_idsCheques3Ro').on('beforeItemRemove', function(event) {
       var tag = event.item;
        // Do some processing here        
        console.log(tag);   
        let importe = parseFloat($('#MovimientoCuentasBanc_Importe').val());
                
        if(isNaN(importe)){
            importe = 0.00;
        }        
        console.log(importe);      
        
        let ch = parseFloat($('#ChequeDeTerceros_nroCheque').select2('data').importe);
        console.log(ch);        
        
        console.log(importe);
        importe -= parseFloat(tag.importe);
        console.log(importe); 
        
        $('#MovimientoCuentasBanc_Importe').val(importe.toFixed(2));
    });
    
    
    // SE AGREGA UN CHEQUE DE TERCEROS
    $('#agregarCh3ro').click(function(e){
        e.preventDefault();
        ele = $('#MovimientoCuentasBanc_idsCheques3Ro');
        ele.tagsinput('add',{ value:$('#ChequeDeTerceros_nroCheque').select2('data').id,text:$('#ChequeDeTerceros_nroCheque').select2('data').sBanco+' : '+$('#ChequeDeTerceros_nroCheque').select2('data').text,importe:$('#ChequeDeTerceros_nroCheque').select2('data').importe});
        let importe = parseFloat($('#MovimientoCuentasBanc_Importe').val());
                
        if(isNaN(importe)){
            importe = 0.00;
        }        
        console.log(importe);      
        
        let ch = parseFloat($('#ChequeDeTerceros_nroCheque').select2('data').importe);
        console.log(ch);
        importe = (ch + importe);        
        $('#MovimientoCuentasBanc_Importe').val(importe.toFixed(2));
        // PARA QUE FUNCIONE, EL SELECT2 TIENE QUE TENER EL METODO initSelection
        $('#ChequeDeTerceros_id_banco').select2('val','');
        $('#ChequeDeTerceros_nroCheque').select2('val','');
        return false;
        //alert();
    });
    
    $('#agregarChPpio').click(function(e){
        e.preventDefault();
        let ele =  $('#MovimientoCuentasBanc_idsChequesPpios');
        console.log($('#ChequePropio_nroCheque').select2('data'));
        ele.tagsinput('add',{ value:$('#ChequePropio_nroCheque').select2('data').id,text:$('#ChequePropio_nroCheque').select2('data').text});
        let importe = parseFloat($('#MovimientoCuentasBanc_Importe').val());
        
        if(isNaN(importe)){
            importe = 0.00;
        }        
        console.log(importe);      
        
        let ch = parseFloat($('#ChequePropio_nroCheque').select2('data').importe);
        console.log(ch);
        importe = (ch + importe);        
        $('#MovimientoCuentasBanc_Importe').val(importe.toFixed(2));
        // PARA QUE FUNCIONE, EL SELECT2 TIENE QUE TENER EL METODO initSelection        
        $('#ChequePropio_nroCheque').select2('val','');
        return false;
    });
    
    $('#MovimientoCuentasBanc_idMedio').change(function(){
        toggleOrigen($(this).val());
    });
    
    
    $('#origenCheque').change(function(){
        let origen = $(this).val();
        if(origen == 'terceros'){
            $('.deTerceros').show('slow');
            $('#propios').hide('slow');
            $('.propios').hide('slow');        
        }else if(origen == 'propios'){
            $('.deTerceros').hide('slow');
            $('#propios').show('slow');
            $('.propios').show('slow');            
        }else{
            $('.deTerceros').hide('slow');
            $('#propios').hide('slow');
            $('.propios').hide('slow');
        }
    });
    
    function toggleOrigen(medio){
        // SI EL MEDIO ES UN CHEQUE
        if(medio == 2 || medio == 3){
            $('.cheques').show('slow');
            $('#cheques').show('slow');            
            // ESTA LINEA ES IMPORTANTE PARA QUE PUEDAN SEGUIR PASANDO DE UN CAMPO A OTRO CON ENTER
            $('#origenCheque').attr('tabindex',8);
            $('#MovimientoCuentasBanc_Importe').attr('readonly','readonly');
        }else{
            $('.cheques').hide('slow');
            $('#cheques').hide('slow');
            // ESTA LINEA ES IMPORTANTE PARA QUE PUEDAN SEGUIR PASANDO DE UN CAMPO A OTRO CON ENTER
            $('#origenCheque').attr('tabindex',9);
            $('#MovimientoCuentasBanc_Importe').removeAttr('readonly');
        }
    }
");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'movimiento-cuentas-banc-form',
	'enableAjaxValidation'=>false,
));


?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorMovimientoCuentasBanc"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okMovimientoCuentasBanc">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span4">
			<?php
                echo $form->labelEx($model,'idCuenta');
                echo $form->dropDownList($model,'idCuenta',CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),array('class'=>'span12','empty'=>'Seleccionar','tabindex' => 1));
            ?>
		</div>
        <div class="span2">
            <?php
            echo $form->labelEx($model,'idMedio');
            echo $form->dropDownList($model,'idMedio',CHtml::listData(MedioMovimiento::model()->activos()->findAll(),'CodMedio','Descripcion'),array('class'=>'span12','empty'=>'Seleccionar','tabindex' => 2));
            ?>
        </div>
        <div class="span2">
            <?php
            echo $form->labelEx($model,'tipoMovimiento');
            echo $form->dropDownList($model,'tipoMovimiento',CHtml::listData(ClasifMovimientoCuentas::model()->findAll(),'codigo','Descripcion'),array('empty'=>'Seleccionar','class'=>'span12','tabindex' => 3));
            ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'Nro_operacion',array('class'=>'span12','maxlength'=>9,'tabindex' => 4)); ?>
        </div>
        <div class="span2">
            <?php echo $form->hiddenField($model,'codigoMovimiento'); ?>
            <?php
            echo $form->labelEx($model,'fechaMovimiento',array('style'=>'display:inline;'));
            $this->widget('CMaskedTextField', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fechaMovimiento),
                'name'=>'MovimientoCuentasBanc[fechaMovimiento]', // Cambiar 'NotaPedido por el modelo que corresponda
                //'model' => $model,
                //'attribute' => 'FechaFactura',
                'mask' => '99-99-9999',
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Ingresar los numeros sin los guiones  ',
                    'tabindex' => 5
                ),
            ));
            echo $form->error($model,'fechaMovimiento');
            ?>
        </div>
    </div>

    <div class="row-fluid">
		<div class="span6">
            <div class="deTerceros" style="display: none;" >
                <?php echo $form->labelEx($model,'idsCheques3Ro',array()) ?>
                <input type="text" name="MovimientoCuentasBanc[idsCheques3Ro]" id="MovimientoCuentasBanc_idsCheques3Ro" value="<?=$model->idsCheques3Ro?>" style="" >
            </div>
            <div class="propios" style="display: none;" >
                <?php echo $form->labelEx($model,'idsChequesPpios',array()) ?>
                <input type="text" name="MovimientoCuentasBanc[idsChequesPpios]" id="MovimientoCuentasBanc_idsChequesPpios" value="<?=$model->idsChequesPpios?>" style="" >
            </div>
            <?php echo $form->textAreaRow($model,'Observacion',array('rows'=>2, 'cols'=>50, 'class'=>'span12','tabindex' => 6)); ?>

		</div>
        <div class="span2 cheques" style="display: none;">
            <?php
                echo CHtml::label('Origen Del Cheque','origenCheque');
                echo CHtml::dropDownList('origenCheque','',array('terceros' =>'Terceros','propios'=>'Propios'),array('class'=>'span12','tabindex' => 9,'empty' => 'Seleccionar'))
            ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'Importe',array('class'=>'span12','tabindex' => 7)); ?>
        </div>
        <div class="span2">

            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                )
            )); ?>

        </div>
    </div>

    <div class="row-fluid">
        <div class="span10">
            <div class="row-fluid" id="cheques" class="cheques" style="display: none;">
                <div class="span8 bordeRedondo deTerceros" id="deTerceros" style="display: none">
                    <?php 
                    $modelCheque3ro = new ChequeDeTerceros();
                    $formCheque3ro=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        'id'=>'cheque-de-terceros-form',
                        'enableAjaxValidation'=>false,
                    )); ?>
                    <div class="span5">
                        <?php
                        echo $formCheque3ro->textFieldRow($modelCheque3ro,'id_banco',array('class'=>'span12'));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#ChequeDeTerceros_id_banco',
                                'options'=>array(
                                    'elementTabIndex' => 1,
                                    'openOnEnter' => false,
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('banco/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                        'results'=>"js:function(data,page) { return {results: data}; }",
                                    ),
                                    'initSelection' => 'function (element, callback) {
                                                        var data = {id: element.val(), text: ChequeDeTerceros_id_banco};
                                                        console.log(data);
                                                        callback(data);
                                     }',
                                    'class' => 'span12',
                                    'tabindex' => 1,
                                ),
                            )
                        );
                        ?>
                    </div>
                    <div class="span5">
                        <?php
                        echo $formCheque3ro->textFieldRow($modelCheque3ro,'nroCheque',array('class'=>'span12'));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#ChequeDeTerceros_nroCheque',
                                'options'=>array(
                                    'elementTabIndex' => 2,
                                    'openOnEnter' => false,
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('chequeDeTerceros/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true,id_banco: $("#ChequeDeTerceros_id_banco").val()}; }',
                                        'results'=>'js:function(data,page) { return {results: data}; }',
                                    ),
                                    'initSelection' => 'function (element, callback) {
                                                        var data = {id: element.val(), text: ChequeDeTerceros_nroCheque};
                                                        console.log(data);
                                                        callback(data);
                                     }',
                                    'class' => 'span12',
                                    'tabindex' => 2,
                                ),
                            )
                        );
                        ?>
                    </div>
                    <div class="span2">
                        <input class="btn btn-primary" id="agregarCh3ro" type="submit" value="Agregar" style="margin-top:25px">
                    </div>

                    <?php $this->endWidget(); ?>

                </div>
                <div class="span8 bordeRedondo " id="propios" style="display: none">

                    <?php
                        $modelChequePropio = new ChequePropio();
                        $formChequePropio = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                            'id' => 'cheque-propio-form',
                            'enableAjaxValidation'=>false,
                        ));
                        echo $formChequePropio->hiddenField($modelChequePropio,'id_cuenta');
                    ?>
                    <div class="span8 " style="">
                        <?php
                        echo $formChequePropio->textFieldRow($modelChequePropio,'nroCheque',array('class'=>'span12'));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#ChequePropio_nroCheque',
                                'options'=>array(
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('ChequePropio/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true,id_cuenta: $("#MovimientoCuentasBanc_idCuenta").val()}; }',
                                        'results'=>"js:function(data,page) { return {results: data}; }",
                                    ),
                                    'initSelection' => 'function (element, callback) {
                                                            var data = {id: element.val(), text: ChequePropio_nroCheque,partial: true,id_cuenta: $("#MovimientoCuentasBanc_idCuenta").val()};
                                                            console.log(data);
                                                            callback(data);
                                         }',
                                    'class' => 'span12',
                                    'tabindex' => 1,
                                ),
                            )
                        );

                        ?>
                    </div>
                    <div class="span2">
                        <input class="btn btn-primary" id="agregarChPpio" type="submit" value="Agregar" style="margin-top:25px">
                    </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>

    </div>
</div>


<?php $this->endWidget(); ?>

<?php
if (sizeof($model->aCheque3RoMovBancario) > 0){
    $dataProvider=new CArrayDataProvider($model->aCheque3RoMovBancario,array(
            'pagination'=> false,
        )
    );
    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'cheque3-ro-mov-bancario-grid',
        'dataProvider'=>$dataProvider,
        'columns'=>array(
            array(
                'name' => 'nroCheque',
                'value' =>'$data->oCheque3ro->nroCheque',
                'htmlOptions' => array('style' => 'width:85px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:85px;text-align:right;'),
            ),
            array(
                'name' => 'fecha',
                'value' => 'ComponentesComunes::fechaFormateada($data->oCheque3ro->fecha)',
                'htmlOptions' => array('style' => 'width:75px;text-align:right;'), // ESTILO VALORES
                'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),  // ESTILO CABECERA
            ),
            array(
                'name' => 'banco',
                'value' =>'($data->oCheque3ro->oBanco)? $data->oCheque3ro->oBanco->nombre :""',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => 'width:180px;'),
            ),
            array(
                'name' => 'sucursal',
                'value' =>'($data->oCheque3ro->oSucursal)? $data->oCheque3ro->oSucursal->nombre :""',
                'htmlOptions' => array('style' => ''),
            ),
            array(
                'name' => 'importe',
                'value' => '"$".number_format($data->oCheque3ro->importe,2)',
                'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}',
                'buttons'=>array(
                    'delete' => array(
                        'label' => 'Desvincular Cheque',
                        'url'=>'Yii::app()->controller->createUrl("Cheque3RoMovBancario/delete", array("id"=>$data->id,"ajax"=>""))',
                        'click'=>'function(e){ 
                                if (confirm("Desea desvincular El cheque de terceros?")){
                                        $.post($(this).attr("href"), function(data) {
                                            updateImporte();  
                                            $.fn.yiiGridView.update("cheque3-ro-mov-bancario-grid");                                                                                           
                                        }).fail(function(x) {
                                            console.error( "error" );
                                        });                                                                                
                                        return false;
                                }else{
                                    return false;                                
                                }
                             }',
                    ),
                ),
            ),
        ),
    ));
}

if (sizeof($model->aChequePropioMovBancario) > 0){
    $dataProviderPropio = new CArrayDataProvider($model->aChequePropioMovBancario,array(
            'pagination' => false,
        )
    );

    $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'cheque-propio-grid',
        'dataProvider'=>$dataProviderPropio,
        'columns'=>array(
            array(
                'name' => 'id',
                'htmlOptions' => array('style' => 'width:60px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:60px;text-align:center;'),
            ),
            array(
                'name' => 'nroCheque',
                'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
            ),
            array(
                'name' => 'fecha',
                'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
                'htmlOptions' => array('style' => 'width:80px;'),
                'headerHtmlOptions' => array('style' => 'width:80px;'),
            ),
            array(
                'name' => 'id_cuenta',
                'value' => '$data->oCuenta->title',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
            array(
                'name' => 'importe',
                'value' => '"$".number_format($data->importe,2)',
                'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}',
                'buttons'=>array(
                    'delete' => array(
                        'label' => 'Borrar Item',
                        'url'=>'Yii::app()->controller->createUrl("ChequePropioMovBancario/delete", array("id"=>$data->id,"ajax"=>""))',
                        'click'=>'function(e){ 
                                if (confirm("Desea desvincular El cheque de terceros?")){
                                        $.post($(this).attr("href"), function(data) {
                                            updateImporte();  
                                            $.fn.yiiGridView.update("cheque-propio-grid");                                                                                           
                                        }).fail(function(x) {
                                            console.error( "error" );
                                        });                                                                                
                                        return false;
                                }else{
                                    return false;                                
                                }
                             }',
                    ),
                ),
            ),
        ),
    ));
}
?>