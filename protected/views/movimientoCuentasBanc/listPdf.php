<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 1/12/2019
 * Time: 14:43
 */

/** @var $model MovimientoCuentasBanc */
?>
<style>
    .table th{
        font-weight: normal;
        border-bottom: 0.05em dotted black;
    }
</style>
<hr>
<h3><?=MovimientoCuentasBanc::model()->getAttributeLabel('model').' '.$model->oCuenta->numero?> <?=($model->fechaDesde != null)? ' Desde '.ComponentesComunes::fechaFormateada($model->fechaDesde) : '' ?> <?=($model->fechaHasta != null)? ' Hasta '.ComponentesComunes::fechaFormateada($model->fechaHasta) : '' ?></h3>
<?php
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'movimiento-cuentas-banc-grid',
    'dataProvider'=>$model->searchWPReport(),
    'enableSorting' => false,
    'htmlOptions' => array(
        'style' => 'padding-top:0px;',
    ),
    'columns'=>array(
        array(
            'name' => 'fechaMovimiento',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaMovimiento)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'text-align:right;width:90px; text-align:center;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'Nro_operacion',
            'htmlOptions' => array('style' => 'text-align:center;width:110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),

        array(
            'name' => 'Observacion',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:200px;'),
        ),

        array(
            'name' => 'Importe',
            'value' => '($data->oTipoMovimiento->Ingreso)? number_format($data->Importe,2,",",".") : ""',
            'header' => 'Debe',
            'htmlOptions' => array('style' => 'text-align:right;width:110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'Importe',
            'value' => '(!$data->oTipoMovimiento->Ingreso)? number_format($data->Importe,2,",",".") : ""',
            'header' => 'Haber',
            'htmlOptions' => array('style' => 'text-align:right;width:110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header' => '$ Saldo',
            'name' => 'saldo_acumulado',
            'value' => 'number_format($data->saldo_acumulado,2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:130px;'),
        ),

    ),
)); ?>