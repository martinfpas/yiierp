

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
    <div class="content-fluid">
        <div class="row-fluid">
            <div class='span4'><?php echo $form->textFieldRow($model, 'id', array('class' => 'span12')); ?>
            </div>
            <div class='span4'>
                <?php
                echo $form->labelEx($model,'idCuenta');
                echo $form->dropDownList($model,'idCuenta',CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),array('class'=>'span12','empty'=>'Seleccionar','tabindex' => 1));
                ?>
            </div>
            <div class='span4'><?php echo $form->textFieldRow($model, 'Importe', array('class' => 'span12')); ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class='span4'>
                <?php
                echo $form->labelEx($model,'tipoMovimiento');
                echo $form->dropDownList($model,'tipoMovimiento',CHtml::listData(ClasifMovimientoCuentas::model()->findAll(),'codigo','Descripcion'),array('empty'=>'Seleccionar','class'=>'span12','tabindex' => 3));
                ?>
            </div>
            <div class='span4'><?php echo $form->textFieldRow($model, 'codigoMovimiento', array('class' => 'span12')); ?>
            </div>
            <div class='span4'>
                <?php
                echo $form->labelEx($model,'idMedio');
                echo $form->dropDownList($model,'idMedio',CHtml::listData(MedioMovimiento::model()->activos()->findAll(),'CodMedio','Descripcion'),array('class'=>'span12','empty'=>'Seleccionar','tabindex' => 2));
                ?>
            </div>
        </div>
        <div class="row-fluid">
            <div class='span4'>
                <?php
                    echo $form->labelEx($model, 'fechaMovimiento');
                    $this->widget('CMaskedTextField', array(
                        'value'=> ComponentesComunes::fechaFormateada($model->fechaMovimiento),
                        'name'=>'MovimientoCuentasBanc[fechaMovimiento]',
                        'mask' => '99-99-9999',
                        'htmlOptions'=>array(
                            'style'=>'height:25px;',
                            'class'=>'span12',
                            'title'=>'Ingresar los numeros sin los guiones  ',
                            'tabindex' => 4
                        ),
                    ));
                ?>

                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type'=>'primary',
                    'label'=>Yii::t('application','Buscar'),
                    'htmlOptions'=>array(
                        'style' => 'margin-top:5px;',
                    ),
                )); ?>
            </div>
            <div class='span4'><?php echo $form->textFieldRow($model, 'Nro_operacion', array('class' => 'span12', 'maxlength' => 9)); ?>
            </div>
            <div class='span4'><?php echo $form->textAreaRow($model, 'Observacion', array('rows' => 3, 'cols' => 50, 'class' => 'span12')); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

