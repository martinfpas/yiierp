<?php
/* @var $this MovimientoCuentasBancController */
/* @var $model MovimientoCuentasBanc */

$this->breadcrumbs=array(
	MovimientoCuentasBanc::model()->getAttributeLabel('models')=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo '.MovimientoCuentasBanc::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#movimiento-cuentas-banc-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>ABM <?=MovimientoCuentasBanc::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'movimiento-cuentas-banc-grid',
'dataProvider'=>$model->activas()->search(),
'filter'=>$model,
'columns'=>array(
        array(

            'name' => 'idCuenta',
            'value' => '($data->oCuenta != null)? substr($data->oCuenta->Title,0,30) : ""',
            'header' => 'Cuenta',
            'filter' => CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),
            //'headerHtmlOptions' => array('style' => ''),
        ),

        array(
            'header' => '$ Ingreso',
            'name' => 'Importe',
            'value' => '($data->oTipoMovimiento->Ingreso == 1)? "$".number_format($data->Importe,2) :"" ',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header' => '$ Egreso',
            'name' => 'Importe',
            'value' => '($data->oTipoMovimiento->Ingreso == 0)? "$".number_format($data->Importe,2) :"" ',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'filter' => CHtml::listData(ClasifMovimientoCuentas::model()->findAll(),'codigo','Descripcion'),
            'name' => 'tipoMovimiento',
            'value' =>  '$data->oTipoMovimiento->Descripcion',
        ),
        array(
            'name' => 'fechaMovimiento',
            'header' => 'Fecha Mov',
            'value' =>  'ComponentesComunes::fechaFormateada($data->fechaMovimiento)',
            'htmlOptions' => array('style' => 'text-align:center;width:80px'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:80px'),
        ),
        array(
            'filter' => CHtml::listData(MedioMovimiento::model()->activos()->findAll(),'CodMedio','Descripcion'),
            'name' => 'idMedio',
            'value' =>  '$data->oMedio->Descripcion',
        ),
        'Nro_operacion',
        array(
            'name' => 'ingreso',
            'value' => '($data->oTipoMovimiento->Ingreso == 0)? "No" :"Si"',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            'filter' => ClasifMovimientoCuentas::$aSiNo,
        ),
        array(
            'header' => '$ Saldo',
            'name' => 'saldo_acumulado',
            'value' => '"$".number_format($data->saldo_acumulado,2)',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
		/*
		'fechaMovimiento',
		'Nro_operacion',
		'Observacion',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
