<?php
/* @var $this MovimientoCuentasBancController */
/* @var $data MovimientoCuentasBanc */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCuenta')); ?>:</b>
	<?php echo CHtml::encode($data->idCuenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Importe')); ?>:</b>
	<?php echo CHtml::encode($data->Importe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoMovimiento')); ?>:</b>
	<?php echo CHtml::encode($data->tipoMovimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoMovimiento')); ?>:</b>
	<?php echo CHtml::encode($data->codigoMovimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMedio')); ?>:</b>
	<?php echo CHtml::encode($data->idMedio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaMovimiento')); ?>:</b>
	<?php echo CHtml::encode($data->fechaMovimiento); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_operacion')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_operacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observacion')); ?>:</b>
	<?php echo CHtml::encode($data->Observacion); ?>
	<br />

	*/ ?>

</div>