<?php
/* @var $this MovimientoCuentasBancController */
/* @var $model MovimientoCuentasBanc */

$this->breadcrumbs=array(
	MovimientoCuentasBanc::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.MovimientoCuentasBanc::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=MovimientoCuentasBanc::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>