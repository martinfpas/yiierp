
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoMovimientoCuentasBanc" id="masMovimientoCuentasBanc" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoMovimientoCuentasBanc" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'movimiento-cuentas-banc-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('MovimientoCuentasBanc/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'movimiento-cuentas-banc-grid',
			'idMas' => 'masMovimientoCuentasBanc',
			'idDivNuevo' => 'nuevoMovimientoCuentasBanc',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorMovimientoCuentasBanc"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okMovimientoCuentasBanc">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idCuenta',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Importe',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'tipoMovimiento',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoMovimiento',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idMedio',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaMovimiento',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_operacion',array('class'=>'span12','maxlength'=>9)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textAreaRow($model,'Observacion',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'movimiento-cuentas-banc-grid',
'dataProvider'=>$aMovimientoCuentasBanc->search(),
'columns'=>array(
		'id',
		'idCuenta',
		'Importe',
		'tipoMovimiento',
		'codigoMovimiento',
		'idMedio',
		/*
		'fechaMovimiento',
		'Nro_operacion',
		'Observacion',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("MovimientoCuentasBanc/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("MovimientoCuentasBanc/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>