

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>40)); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Id_Proveedor',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Unidad_Medida',array('class'=>'span12','maxlength'=>4)); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Precio',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'fecha_act_precio',array('class'=>'span12')); ?>
</div>
                    <div class='span4'>
                    <div class="form-actions">
                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>Yii::t('application','Buscar'),
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>

