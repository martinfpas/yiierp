<?php
$this->breadcrumbs=array(
	MateriaPrima::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.MateriaPrima::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.MateriaPrima::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.MateriaPrima::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM '.MateriaPrima::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=MateriaPrima::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_rubro',
		'Descripcion',
		'id_articulo',
		'Id_Proveedor',
		'Unidad_Medida',
		'Precio',
		'fecha_act_precio',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
