<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'materia-prima-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorMateriaPrima"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okMateriaPrima">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Proveedor',array('class'=>'span12')); ?>
		</div>

                    </div>
                    <div class="row-fluid">		<div class="span3">
			<?php echo $form->textFieldRow($model,'Unidad_Medida',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Precio',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha_act_precio',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
