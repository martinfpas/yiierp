
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoMateriaPrima" id="masMateriaPrima" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoMateriaPrima" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'materia-prima-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('MateriaPrima/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'materia-prima-grid',
			'idMas' => 'masMateriaPrima',
			'idDivNuevo' => 'nuevoMateriaPrima',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorMateriaPrima"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okMateriaPrima">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Proveedor',array('class'=>'span12')); ?>
		</div>
	
                    </div>
                    <div class="row-fluid">		<div class="span3">
			<?php echo $form->textFieldRow($model,'Unidad_Medida',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Precio',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha_act_precio',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'materia-prima-grid',
'dataProvider'=>$aMateriaPrima->search(),
'columns'=>array(
		'id',
		'id_rubro',
		'Descripcion',
		'id_articulo',
		'Id_Proveedor',
		'Unidad_Medida',
		/*
		'Precio',
		'fecha_act_precio',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("MateriaPrima/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("MateriaPrima/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>