<?php
/* @var $this MateriaPrimaController */
/* @var $model MateriaPrima */

$this->breadcrumbs=array(
	MateriaPrima::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.MateriaPrima::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=MateriaPrima::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>