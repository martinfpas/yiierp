
/* @var $this MateriaPrimaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"MateriaPrima::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo MateriaPrima', 'url'=>array('create')),
	array('label'=>'ABM MateriaPrima', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>