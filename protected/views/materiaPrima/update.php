<?php
$this->breadcrumbs=array(
	MateriaPrima::model()->getAttributeLabel('models') => array('admin'),
	MateriaPrima::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.MateriaPrima::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.MateriaPrima::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.MateriaPrima::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=MateriaPrima::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>