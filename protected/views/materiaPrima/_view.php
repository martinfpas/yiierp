<?php
/* @var $this MateriaPrimaController */
/* @var $data MateriaPrima */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_rubro')); ?>:</b>
	<?php echo CHtml::encode($data->id_rubro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo')); ?>:</b>
	<?php echo CHtml::encode($data->id_articulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id_Proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->Id_Proveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Unidad_Medida')); ?>:</b>
	<?php echo CHtml::encode($data->Unidad_Medida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Precio')); ?>:</b>
	<?php echo CHtml::encode($data->Precio); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act_precio')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act_precio); ?>
	<br />

	*/ ?>

</div>