<?php
$this->breadcrumbs=array(
	'Vcta Cte Clientes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo VCtaCteCliente', 'url'=>array('create')),
	array('label'=>'Modificar VCtaCteCliente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar VCtaCteCliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de VCtaCteCliente', 'url'=>array('admin')),
);
?>

<h1>Ver VCtaCteCliente #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'clId',
		'razonSocial',
		'fecha',
		'debe',
		'haber',
		'neId',
		'faId',
		'pcId',
		'ncId',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
