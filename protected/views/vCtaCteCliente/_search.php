<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12','maxlength'=>36)); ?>

		<?php echo $form->textFieldRow($model,'clId',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>40)); ?>

		<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'debe',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'haber',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'neId',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'faId',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'pcId',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'ncId',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
