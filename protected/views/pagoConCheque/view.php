<?php
$this->breadcrumbs=array(
	'Pago Con Cheques'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo PagoConCheque', 'url'=>array('create')),
	array('label'=>'Modificar PagoConCheque', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar PagoConCheque', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de PagoConCheque', 'url'=>array('admin')),
);
?>

<h1>Ver PagoConCheque #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idChequedeTercero',
		'idPagodeCliente',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
