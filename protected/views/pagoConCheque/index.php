
/* @var $this PagoConChequeController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Pago Con Cheques',
);

$this->menu=array(
	array('label'=>'Nuevo PagoConCheque', 'url'=>array('create')),
	array('label'=>'ABM PagoConCheque', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>