
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoPagoConCheque" id="masPagoConCheque" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoPagoConCheque" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'pago-con-cheque-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('PagoConCheque/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'pago-con-cheque-grid',
			'idMas' => 'masPagoConCheque',
			'idDivNuevo' => 'nuevoPagoConCheque',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorPagoConCheque"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okPagoConCheque">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
    <div class="content-fluid">
        <div class="row-fluid">
                <div class="span3">
                <?php echo $form->textFieldRow($model,'idChequedeTercero',array('class'=>'span12')); ?>
            </div>
                <div class="span3">
                <?php echo $form->textFieldRow($model,'idPagodeCliente',array('class'=>'span12')); ?>
            </div>

        </div>
    </div>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago-con-cheque-grid',
'dataProvider'=>$aPagoConCheque->search(),
'columns'=>array(
		'id',
		'idChequedeTercero',
		'idPagodeCliente',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("PagoConCheque/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("PagoConCheque/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>