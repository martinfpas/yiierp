<?php
$this->breadcrumbs=array(
	'Pago Con Cheques'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo PagoConCheque', 'url'=>array('create')),
	array('label'=>'Ver PagoConCheque', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de PagoConCheque', 'url'=>array('admin')),
);
?>

	<h1>Modificando PagoConCheque <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>