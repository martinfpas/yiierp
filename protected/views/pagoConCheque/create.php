<?php
/* @var $this PagoConChequeController */
/* @var $model PagoConCheque */

$this->breadcrumbs=array(
	'Pago Con Cheques'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de PagoConCheque', 'url'=>array('admin')),
);
?>

<h1>Creando PagoConCheque</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>