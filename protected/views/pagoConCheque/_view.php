<?php
/* @var $this PagoConChequeController */
/* @var $data PagoConCheque */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idChequedeTercero')); ?>:</b>
	<?php echo CHtml::encode($data->idChequedeTercero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPagodeCliente')); ?>:</b>
	<?php echo CHtml::encode($data->idPagodeCliente); ?>
	<br />


</div>