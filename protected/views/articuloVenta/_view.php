<?php
/* @var $this ArticuloVentaController */
/* @var $data ArticuloVenta */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo_vta')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_articulo_vta),array('view','id'=>$data->id_articulo_vta)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_presentacion')); ?>:</b>
	<?php echo CHtml::encode($data->id_presentacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_articulo')); ?>:</b>
	<?php echo CHtml::encode($data->id_articulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contenido')); ?>:</b>
	<?php echo CHtml::encode($data->contenido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uMedida')); ?>:</b>
	<?php echo CHtml::encode($data->uMedida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codBarra')); ?>:</b>
	<?php echo CHtml::encode($data->codBarra); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('costo')); ?>:</b>
	<?php echo CHtml::encode($data->costo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enLista')); ?>:</b>
	<?php echo CHtml::encode($data->enLista); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stockMinimo')); ?>:</b>
	<?php echo CHtml::encode($data->stockMinimo); ?>
	<br />

	*/ ?>

</div>