
<?php 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
$cs->registerCssFile($baseUrl."/css/customFineUploader.css");


Yii::app()->clientScript->registerScript('search', "
	
	//$('#ArticuloVenta_id_presentacion').change(function(){
	//	autoDescripcion();
	//});

	function buscarPresentacAsync(request, response){
		console.log(request);
		$.ajax({
           	url: '".$this->createUrl('ArticuloVenta/buscarPresentacAsync')."',
			dataType: 'json',
			data: {
				articulo: $('#ArticuloVenta_id_articulo').val(),
				presentacion: request.term			        
			},
			success: function (data) {
                response(data);
			}
		})
	}	
		
	// TRAE LA GRILLA DE ARTICULOS Y ENVASES	
	function getFilaGrilla(){
		var idArt = $('#ArticuloVenta_id_articulo_vta').val();
		var data = '&id_artVenta='+idArt;
		var url = '".Yii::app()->createUrl('Paquete/ViewFilaGrillaAsync')."';

		$.ajax({
			url: url,
            type: 'Get',
            data: data,
            success: function (html) {
               	
               	//PONER VARIABLES CON EL ID CREADO
				$('#presentaciones').html(html).promise().done(function(){
			    	// HACE EL BIND DE LAS FUNCIONES CON EL HTML QUE LLEGA
					bindSaveAsync();
				});
		

				
			},
			error: function (x,y,z){
				// TODO: MANEJAR LOS ERRORES
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				console.log($(divError));
				console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$(divError).html(htmlError);
				$(divOk).hide('slow');
				$(divError).show('slow');
			}
		});
		
	}		
		
	$(document).ready(function(){
		var idArt = $('#ArticuloVenta_id_articulo_vta').val();
		console.log(idArt);
		if(idArt > 0){
			getFilaGrilla();
		}
	});	

	//_form	
	$('#articulo-venta-form').submit(function(e){
		e.preventDefault();
		var datos = $(this).serialize();
		//console.log(datos);
		var respuesta ;
		var url = $(this).attr('action');
		//var divError = $(this).find('.SaveAsyncError');
		var divError = $('#errorArticuloVenta');
		var divOk = $(this).find('.SaveAsyncOk');
		var idGrid =  $(this).attr('idGrilla');
		var idMas = $(this).attr('idMas');
		var idNuevo = $(this).attr('idNuevo');
		
		var form = $(this);
		
		$(divError).hide('slow');
		$(divOk).hide('slow');
		
		
		$.ajax({
			url: url,
            type: 'Post',
            data: datos,
            success: function (html) {
               	$(divOk).show('slow');
               	//PONER VARIABLES CON EL ID CREADO
				$('#ArticuloVenta_id_articulo_vta').val(html);
				console.log($('#'+idNuevo));
				$('#btnGuardar').val('Guardar');
				// TRAE LA GRILLA DE ARTICULOS Y ENVASES
				getFilaGrilla();
               	
			},
			error: function (x,y,z){
				// TODO: MANEJAR LOS ERRORES
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				//console.log($(divError));
				//console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$(divError).html(htmlError);
				$(divOk).hide('slow');
				$(divError).show('slow');
			}
		});
		return false;
	});
		
	// AUTOCOMPLETA EL CAMPO DESCRIPCION BASADO EN LAS OPCIONES DE LOS MENU	
	function autoDescripcion(){
		var id_rubro = 'id_rubro='+$('#ArticuloVenta_id_rubro').val();
		var id_articulo = '&id_articulo='+$('#ArticuloVenta_id_articulo').val();
		var id_presentacion = '&id_presentacion='+$('#ArticuloVenta_id_presentacion').val();
		
		console.log(id_rubro+id_articulo+id_presentacion);
		
		$.ajax({
				url: '".$this->createUrl('ArticuloVenta/autoDescripcion')."',
	            type: 'Post',
	            data: id_rubro+id_articulo+id_presentacion,
	            success: function (html) {
	               	console.log(html);
	               	$('#ArticuloVenta_nombre').val(html);
	               	$('#ArticuloVenta_descripcion').val(html+' X');
				},
				error: function (x,y,z){
					// TODO: MANEJAR LOS ERRORES
					respuesta =  $.parseHTML(x.responseText);
					errorDiv = $(respuesta).find('.errorSpan');
					console.log($(errorDiv).parent().next());
					var htmlError = $(errorDiv).parent().next().html();
					$('#errorAsignacion').html(htmlError);
					$('#errorAsignacion').show('slow');
				}
		});
		
	};

		

	$('#ArticuloVenta_id_rubro').change(function(){
		//TODO: CAMBIAR EL COMBO DE ARTICULO
		
	});	
");
?>

<?php 

	$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'articulo-venta-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ArticuloVenta/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm2',
			'idGrilla'=>'articulo-venta-grid',
			'idMas' => 'masArticuloVenta',
			'idDivNuevo' => 'nuevoArticuloVenta',
		),
	)); ?>

<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorArticuloVenta"></div>
<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okArticuloVenta">Datos Guardados Correctamente !</div>
	
	
<style>
.bordeRedondo{
	border: #dddddd 1px solid;
	padding: 10px; 
	border-radius: 0.7em;
	min-height: 55px;
}
</style>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorArticuloVenta"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okArticuloVenta">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
		<div class="span3">
			<?php 
				echo $form->hiddenField($model,'id_articulo_vta');
				echo $form->labelEx($model,'id_rubro'); 
				echo $form->dropDownList(
							$model,'id_rubro',CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),

							array(
								'tabindex'=>'1','empty' => '--Seleccionar--', 
								'ajax' => array(
									'type'=>'POST', //request type
									'url'=>CController::createUrl('ListaArticulos'), //url to call.
									'update'=>'#ArticuloVenta_id_articulo', //selector to update
								),
								//'disabled' => ($model->isNewRecord)? '' : 'disabled',

								   		
							)
						);
			?>
			
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_articulo');
				echo $form->dropDownList($model,'id_articulo',CHtml::listData(Articulo::model()->todosId()->findAll(),'id','IdTitulo'),array('tabindex'=>'2','empty' => '--Seleccionar--'));
			?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_presentacion');
				//echo $form->textFieldRow($model,'id_presentacion',array('tabindex'=>'3','empty' => '--Seleccionar--'));
				
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
			    'name'=>'ArticuloVenta[id_presentacion]',
				'id'=>'ArticuloVenta_id_presentacion',
				'value' => $model->id_presentacion,
				'model' => $model,
				'source'=>'js: function(request, response) { 
					
					buscarPresentacAsync(request, response)
				
				}',
				// additional javascript options for the autocomplete plugin
				'options'=>array(
								
			                    'minLength'=>'1',
			                    'showAnim'=>'fold',
								'dataType'=>'json',  
								'data'=>array(
								),
			                    'select' => 'js:function(event, ui)
			                        {
										//AVISO AL USUARIO  
										alert("La presentacion ya existe");
										// LLENO EL FORMULARIO
										$("#ArticuloVenta_descripcion").val(ui.item["descripcion"]);
										$("#ArticuloVenta_id_articulo_vta").val(ui.item["id"]);
										$("#ArticuloVenta_codBarra").val(ui.item["codBarra"]);
										$("#ArticuloVenta_enLista").val(ui.item["enLista"]);
										$("#ArticuloVenta_stockMinimo").val(ui.item["stockMinimo"]);
										$("#ArticuloVenta_contenido").val(ui.item["contenido"]);
										$("#ArticuloVenta_uMedida").val(ui.item["uMedida"]);
										$("#ArticuloVenta_precio").val(ui.item["precio"]);
										$("#ArticuloVenta_costo").val(ui.item["costo"]);
   										$("#btnGuardar").attr("disabled","disabled");
										getFilaGrilla(); 
			                        }',
								'response'=> 'js:function( event, ui ) {
								   	//CUENTA SI TRAJO RESULTADOS
									console.log("ui.content.length:"+ui.content.length);
									if (ui.content.length === 0) {
								   		autoDescripcion();
										//$("#ArticuloVenta_descripcion").focus();
										$("#btnGuardar").removeAttr("disabled");
								   	}else{
								   			
								   	}
								}',
				),
			    'htmlOptions'=>array(
			        'style'=>'height:20px;width: 80%;',
			    	'tabindex'=>'3'
				),
			));
			
				
				
				
				?>
			
			
		</div>
	</div>
    <div class="row-fluid">
        <div class="span9">
            <?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>70,'tabindex'=>'4')); ?>
        </div>

    </div>
	<div class="row-fluid">	
		<div class="span9">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70,'tabindex'=>'5')); ?>
		</div>

	</div>

	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'codBarra',array('class'=>'span12','maxlength'=>60,'tabindex'=>'6')); ?>
		</div>		
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'enLista');
				echo $form->dropDownList($model,'enLista',array('0' => 'No', '1' => 'Si' ),array('tabindex'=>'7'));
			?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'stockMinimo',array('class'=>'span12','tabindex'=>'8')); ?>
		</div>
		
	</div>
	<div class="row-fluid" style="width: 49%;float: left;">
		<div class="bordeRedondo">
			<div class="span6">
				<?php echo $form->textFieldRow($model,'contenido',array('class'=>'span12','tabindex'=>'9')); ?>
			</div>
			<div class="span6">
				<?php echo $form->labelEx($model,'uMedida'); ?>
                <?php echo $form->dropDownList($model,'uMedida',CHtml::listData(Udm::model()->findAll(),'id','unidad'),array('empty'=>'-Seleccionar-','class'=>'span12','tabindex'=>'10')); ?>

			</div>
		</div>
	</div>
	<div class="row-fluid" style="width: 49%;float: left;margin-left: 17px;margin-bottom: 10px;">
		<div class="bordeRedondo">
			<div class="span6">
				<?php echo $form->textFieldRow($model,'precio',array('class'=>'span12','tabindex'=>'11')); ?>
			</div>
			<div class="span6">
				<?php echo $form->textFieldRow($model,'costo',array('class'=>'span12','tabindex'=>'12')); ?>
			</div>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12 form-actions">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'submit',
					'type'=>'primary',
					'label'=>$model->isNewRecord ? 'Guardar y cargar presentaciones' : 'Guardar',
					'htmlOptions' => array(
						'id' => 'btnGuardar',								
					),
				)); ?>
		</div>
	</div>
</div>

<?php $this->endWidget(); ?>

<div id='presentaciones'></div>