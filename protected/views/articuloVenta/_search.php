
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
    'htmlOptions' => array(
        'id' => 'artVtaSearch',
    )
)); ?>
<div class="content-fuid">
	<div class="row-fuid"> 
		<div class="span3"><?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12','tabindex' => '1')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12','tabindex' => '2')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'id_presentacion',array('class'=>'span12','tabindex' => '3')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70,'tabindex' => '4')); ?> </div>
	</div>
	<div class="row-fuid avanzado"> 
		<div class="span3"><?php echo $form->textFieldRow($model,'id_articulo_vta',array('class'=>'span12','tabindex' => '6')); ?> </div>
				
		<div class="span3"><?php echo $form->textFieldRow($model,'contenido',array('class'=>'span12','tabindex' => '7')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'uMedida',array('class'=>'span12','maxlength'=>6,'tabindex' => '8')); ?> </div>

		<div class="span3"><?php echo $form->textFieldRow($model,'codBarra',array('class'=>'span12','maxlength'=>60,'tabindex' => '9')); ?> </div>
	</div>
	<div class="row-fuid avanzado"> 
		<div class="span3"><?php echo $form->textFieldRow($model,'precio',array('class'=>'span12','tabindex' => '10')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'costo',array('class'=>'span12','tabindex' => '11')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'enLista',array('class'=>'span12','tabindex' => '12')); ?> </div>
		
		<div class="span3"><?php echo $form->textFieldRow($model,'stockMinimo',array('class'=>'span12','tabindex' => '13')); ?> </div>
			
	</div>

</div>


	<div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Buscar',
        )); ?>
        <?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
	</div>

<?php $this->endWidget(); ?>
