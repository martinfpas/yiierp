<?php
$this->breadcrumbs=array(
	'Articulo Ventas'=>array('admin'),
	$model->id_articulo_vta,
);

$this->menu=array(
	array('label'=>'Modificar ', 'url'=>array('update', 'id'=>$model->id_articulo_vta)),
	array('label'=>'Borrar ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_articulo_vta),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>

<h3>Ver Articulo para la Venta #<?php echo $model->codigo; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'descripcion',
                    array(
                        'name' => 'oArticulo.id_rubro',
                        'value' => $model->oArticulo->oRubro->IdTitulo,
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'id_articulo',
                        'value' => $model->oArticulo->IdTitulo,
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'id_presentacion',
                        'htmlOptions' => array('style' => 'width:60px;'),
                    ),
                    'contenido',
                    'uMedida',
                ),
            )); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'codBarra',
                    array(
                        'name' => 'precio',
                        'value' => '$'.number_format($model->precio,2),
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'precio',
                        'value' => '$'.number_format($model->costo,2),
                        'htmlOptions' => array('style' => ''),
                    ),
                    'enLista',
                    'stockMinimo',
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>
</div>


<h4> Paquetes:</h4>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'paquete-grid',
'dataProvider'=>$aPaquete->activosEinactivos()->search(),
'columns'=>array(
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'activo',
            'value' => 'Paquete::$aSiNo[$data->activo]',
            'editable' => array(
                //'apply'      => '$data->pagado != 1 && $data->esEditable()',
                'type'      => 'select',
                'mode'      => 'inline',
                'url'       => $this->createUrl('Paquete/SaveFieldAsync'),
                'source'    => Editable::source(Paquete::$aSiNo),
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                        try{
                                            $.fn.yiiGridView.update("carga-documentos-grid");
                                        }catch(e){
                                            console.log(e);
                                        };
                                     }',
            ),
            'htmlOptions' => array('style' => 'width:50px;'),

        ),
		array(
				'name' => 'id_envase',
				'value' => '$data->oArtVenta->descripcion',
				'htmlOptions' => array('style' => ''),
		),
		array(
				'name' => 'id_envase',
				'header' => 'Descripcion',
				'value' => '$data->oEnvase->descripcion',
				'htmlOptions' => array('style' => ''),
		),
		'cantidad',
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'listaComercial',
            'value' => 'Paquete::$aSiNo[$data->listaComercial]',
            'editable' => array(
                //'apply'      => '$data->pagado != 1 && $data->esEditable()',
                'type'      => 'select',
                'mode'      => 'inline',
                'url'        => $this->createUrl('Paquete/SaveFieldAsync'),
                'source'    => Editable::source(Paquete::$aSiNo),
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                    try{
                                        $.fn.yiiGridView.update("paquete-grid");
                                    }catch(e){
                                        console.log(e);
                                    };
                                 }',
            ),
           'htmlOptions' => array('style' => 'width:105px;'),

        ),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'listaInstitucional',
            'value' => 'Paquete::$aSiNo[$data->listaInstitucional]',
            'editable' => array(
                //'apply'      => '$data->pagado != 1 && $data->esEditable()',
                'type'      => 'select',
                'mode'      => 'inline',
                'url'       => $this->createUrl('Paquete/SaveFieldAsync'),
                'source'    => Editable::source(Paquete::$aSiNo),
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                    try{
                                        $.fn.yiiGridView.update("carga-documentos-grid");
                                    }catch(e){
                                        console.log(e);
                                    };
                                 }',
            ),
            'htmlOptions' => array('style' => 'width:105px;'),

        ),

	),
)); ?>

