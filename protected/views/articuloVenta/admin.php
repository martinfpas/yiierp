<?php
/* @var $this ArticuloVentaController */
/* @var $model ArticuloVenta */

$this->breadcrumbs=array(
	'Articulo Ventas'=>array('admin'),
	'ABM',
);

$this->menu=array(
    array('label'=>'Nuevo '.ArticuloVenta::model()->getAttributeLabel('model'), 'url'=>array('create')),
    array('label'=>'ABM precios ', 'url'=>array('AdminModificarPrecios')),
    array('label'=>'Lista Comercial', 'url'=>array('Paquete/ListaComercio')),
    array('label'=>'Lista Instituciones', 'url'=>array('Paquete/ListaInstituciones')),
);

$cs = Yii::app()->getClientScript();
$baseUrl = Yii::app()->baseUrl;
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.avanzado').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#articulo-venta-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
window.prevFocus = $();
$('input[type=\"text\"]').focus(function(){
    let selected = false;
    $(this).on('mouseup.a keyup.a', function(e){
        //$(this).off('mouseup.a keyup.a').select();
        if($(prevFocus).attr('id') != $(this).attr('id') && !selected){            
            $(this).off('mouseup.a keyup.a').select();
            selected = true;
        };
    });
    $(this).on('keypress.a', function(e){
        selected = true;
    });
});

");
?>
<style>
.avanzado{
	display:none;
}
</style>
<h3> ABM <?=ArticuloVenta::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>


<div class="search-form" style="">




<?php 

$this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'articulo-venta-grid',
'dataProvider'=>$model->search(),
'columns'=>array(
		array(
			'name' => 'id_presentacion',
			'header' => 'idPresent',	
			'headerHtmlOptions' => array('style' => 'width:50px'),
		),
		array(
			'name' => 'id_articulo',
			'header' => 'idArtic',
			'htmlOptions' => array('style' => 'width:50px'),
		),
		array(
				'name' => 'oArticulo.id_rubro',
				'value' => '$data->oArticulo->oRubro->descripcion',
				'htmlOptions' => array('style' => ''),
				'filter' => CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),
		),
		'descripcion',

		array(
				'name' => 'precio',
				'value' => '"$".number_format($data->precio,2)',
				'htmlOptions' => array('style' => 'text-align:right;'),
				'headerHtmlOptions' => array('style' => 'width:120px'),
		),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}',
		),
	),
)); ?>
