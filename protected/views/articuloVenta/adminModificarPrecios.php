<?php
/* @var $this ArticuloVentaController */
/* @var $model ArticuloVenta */

$this->breadcrumbs=array(
	'Articulo Ventas'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo Art Venta', 'url'=>array('create')),
);

$cs = Yii::app()->getClientScript();
$baseUrl = Yii::app()->baseUrl;
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.avanzado').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#articulo-venta-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
window.prevFocus = $();
$('input[type=\"text\"]').focus(function(){
    let selected = false;
    $(this).on('mouseup.a keyup.a', function(e){
        //$(this).off('mouseup.a keyup.a').select();
        if($(prevFocus).attr('id') != $(this).attr('id') && !selected){            
            $(this).off('mouseup.a keyup.a').select();
            selected = true;
        };
    });
    $(this).on('keypress.a', function(e){
        selected = true;
    });
});
");
?>
<style>
.avanzado{
	display:none;
}
</style>
<h3>ABM Precios </h3>


<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>


<div class="search-form" style="">

<?php 

$this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'articulo-venta-grid',
'dataProvider'=>$model->searchLong(),
'columns'=>array(
        'Codigo',
        array(
            'name' => 'oArticulo.id_rubro',
            'value' => '$data->oArticulo->oRubro->descripcion',
            'htmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),
        ),
		array(
			'name' => 'id_articulo',
			'header' => 'idArtic',
            'value' => '$data->oArticulo->descripcion',
			'htmlOptions' => array('style' => ''),
		),
        array(
            'name' => 'id_presentacion',
            'header' => 'idPresent',
            'headerHtmlOptions' => array('style' => ''),
        ),


		'descripcion',
		/*
        array(
				'name' => 'precio',
				'value' => '"$".number_format($data->precio,2)',
				'htmlOptions' => array('style' => 'text-align:right;'),
				'headerHtmlOptions' => array('style' => 'width:120px'),
		),
		*/
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'precio',
            'value' => '"$".number_format($data->precio,"2",".","")',
            'editable' => array(    //editable section
                'url'        => $this->createUrl('ArticuloVenta/SaveFieldAsync'),
                'placement'  => 'right',
                'display' => 'js: function(value, sourceData) {
                                        value = value.replace("$",""); //
                                        if(!isNaN(value) > 0){
                                            $(this).html("$ "+parseFloat(value).toFixed(2));
                                        }else{
                                            console.log("Value is NaN:"+value);
                                            $(this).html("$ 0.00");
                                        }        
                                    }',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {
                                         setTimeout(function() {
                                            editable.input.$input.select();
                                          },0);
                                    }',
                /*
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                        try{
                                            $.fn.yiiGridView.update("carga-documentos-grid");
                                        }catch(e){
                                            console.log(e);
                                        };
                                     }',
                */
            ),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}',
		),
	),
)); ?>
