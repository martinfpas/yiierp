<?php
$this->breadcrumbs=array(
	'Articulo Ventas'=>array('index'),
	$model->id_articulo_vta=>array('view','id'=>$model->id_articulo_vta),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Gestion de ArticuloVenta', 'url'=>array('admin')),
);
?>

	<h1>Modificando ArticuloVenta <?php echo $model->id_articulo_vta; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>