
/* @var $this ArticuloVentaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Articulo Ventas',
);

$this->menu=array(
	array('label'=>'Nuevo ArticuloVenta', 'url'=>array('create')),
	array('label'=>'Gestion de ArticuloVenta', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>