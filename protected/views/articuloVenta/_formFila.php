<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoArticuloVenta" id="masArticuloVenta" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoArticuloVenta" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'articulo-venta-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ArticuloVenta/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'articulo-venta-grid',
			'idMas' => 'masArticuloVenta',
			'idDivNuevo' => 'nuevoArticuloVenta',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error" style="display:none;" id="errorArticuloVenta"></div>
		<div class="alert alert-block alert-success" style="display:none;" id="okArticuloVenta">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_presentacion',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'contenido',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'uMedida',array('class'=>'span12','maxlength'=>6)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'codBarra',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'precio',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'costo',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'enLista',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'stockMinimo',array('class'=>'span12')); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'articulo-venta-grid',
'dataProvider'=>$aArticuloVenta->search(),
'filter'=>$aarticulo-venta,
'columns'=>array(
		'id_articulo_vta',
		'descripcion',
		'id_rubro',
		'id_articulo',
		'id_presentacion',
		'contenido',
		/*
		'uMedida',
		'codBarra',
		'precio',
		'costo',
		'enLista',
		'stockMinimo',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ArticuloVenta/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ArticuloVenta/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>