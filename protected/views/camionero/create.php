<?php
/* @var $this CamioneroController */
/* @var $model Camionero */

$this->breadcrumbs=array(
	Camionero::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.Camionero::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=Camionero::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>