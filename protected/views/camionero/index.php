
/* @var $this CamioneroController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Camionero::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo Camionero', 'url'=>array('create')),
	array('label'=>'Gestion de Camionero', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>