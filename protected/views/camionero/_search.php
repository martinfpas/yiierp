

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Nro_documento',array('class'=>'span12','maxlength'=>11)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Apellido',array('class'=>'span12','maxlength'=>30)); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Nombres',array('class'=>'span12','maxlength'=>40)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Domicilio',array('class'=>'span12','maxlength'=>50)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Telefono',array('class'=>'span12','maxlength'=>15)); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Codigo_Postal',array('class'=>'span12','maxlength'=>4)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Zona_Postal',array('class'=>'span12','maxlength'=>4)); ?>
</div>
                    <div class='span4'>
                    <div class="form-actions">
                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>Yii::t('application','Buscar'),
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>

