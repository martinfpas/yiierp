<?php
$this->breadcrumbs=array(
	Camionero::model()->getAttributeLabel('models') => array('admin'),
	Camionero::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.Camionero::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.Camionero::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.Camionero::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=Camionero::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>