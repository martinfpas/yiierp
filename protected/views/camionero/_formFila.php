
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoCamionero" id="masCamionero" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoCamionero" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'camionero-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('Camionero/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'camionero-grid',
			'idMas' => 'masCamionero',
			'idDivNuevo' => 'nuevoCamionero',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorCamionero"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okCamionero">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_documento',array('class'=>'span12','maxlength'=>11)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Apellido',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Nombres',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Domicilio',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Telefono',array('class'=>'span12','maxlength'=>15)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Codigo_Postal',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Zona_Postal',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'camionero-grid',
'dataProvider'=>$aCamionero->search(),
'columns'=>array(
		'id',
		'Nro_documento',
		'Apellido',
		'Nombres',
		'Domicilio',
		'Telefono',
		/*
		'Codigo_Postal',
		'Zona_Postal',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("Camionero/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Camionero/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>