<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'camionero-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCamionero"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCamionero">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_documento',array('class'=>'span12','maxlength'=>11)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Apellido',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nombres',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Domicilio',array('class'=>'span12','maxlength'=>50)); ?>
		</div>

                    </div>
                    <div class="row-fluid">		<div class="span3">
			<?php echo $form->textFieldRow($model,'Telefono',array('class'=>'span12','maxlength'=>15)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Codigo_Postal',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Zona_Postal',array('class'=>'span12','maxlength'=>4)); ?>
		</div>
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
