<?php
$this->breadcrumbs=array(
	Camionero::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.Camionero::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.Camionero::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.Camionero::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.Camionero::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=Camionero::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'Nro_documento',
		'Apellido',
		'Nombres',
		'Domicilio',
		'Telefono',
		'Codigo_Postal',
		'Zona_Postal',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
