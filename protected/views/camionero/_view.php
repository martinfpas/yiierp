<?php
/* @var $this CamioneroController */
/* @var $data Camionero */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_documento')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_documento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Apellido')); ?>:</b>
	<?php echo CHtml::encode($data->Apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nombres')); ?>:</b>
	<?php echo CHtml::encode($data->Nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Domicilio')); ?>:</b>
	<?php echo CHtml::encode($data->Domicilio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Telefono')); ?>:</b>
	<?php echo CHtml::encode($data->Telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Codigo_Postal')); ?>:</b>
	<?php echo CHtml::encode($data->Codigo_Postal); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('Zona_Postal')); ?>:</b>
	<?php echo CHtml::encode($data->Zona_Postal); ?>
	<br />

	*/ ?>

</div>