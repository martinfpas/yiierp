<style>

    #prod-carga-grid tr.odd td,#prod-carga-grid tr.even td{
        padding-top:5px;
        padding-bottom: 5px;
        padding-left: 3px;
        padding-right: 3px;
        border-bottom: 0.05em dotted black;
        /*border-right: 1px dashed black;*/
    }

    tr.odd td,tr.even td{
        padding-top:10px;
        border-bottom: 0.05em dotted black;
    }
    th
    {
        font-weight:normal;
        border-bottom: 0.05em dotted black;
    }

</style>
<div style="width: 100%; text-align: right">
    <span>Fecha: <?php echo ComponentesComunes::fechaFormateada($model->fecha) ; ?> </span>
</div>

<span>Numero de Carga : <?php echo $model->id; ?></span>
<div style="width: 100%; text-align: center; margin-bottom: 25px;">
    <u>Listado de Articulos a Cargar</u>
    <br>
    <br>
    <?php $this->widget('bootstrap.widgets.TbDetailView',array(
        'data'=>$model,
        'htmlOptions' => array(
            'style' => 'font-size:larger;margin-left:10px;padding-left:5px;',
        ),
        'attributes'=>array(
            'observacion',
        ),
    ));
    ?>
</div>
<div style="padding-left: 0px;width: 100% ">

<?php
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-carga-grid',
    'dataProvider'=>$aProds->searchWP(),
    'enableSorting' => false,
    'template' => '{items}',
    'htmlOptions' => array(
        'style' => 'margin-left:100px;',
    ),
    'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => '72x4',
            'value' => '',
            'htmlOptions' => array('style' => 'width:15px;text-align:center;'),
        ),


        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */

    ),
));
echo '</div>'

?>
<div class="container-fluid" style="margin-top: 50px;">
    <div class="row-fluid">
        <div class="span7">
            RECIBI CONFORME EL TOTAL DE LA CARGA<br><br><br><br>
            <br>
            <span style="border-top: 0.05em dotted black;">FIRMA Y ACLARACION DEL EMPLEADO</span>
        </div>
        <div class="span5" style="text-align: right;">
            VERIFICO CARGA DEL CAMION<br><br><br><br>
            <br>
            <span style="border-top: 0.05em dotted black;">FIRMA Y ACLARACION ENCARGADO</span>
        </div>
    </div>
</div>


