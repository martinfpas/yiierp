<?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'nota-pedido-grid2',
            'dataProvider'=>$aNotasPedido->searchWp(),
            'afterAjaxUpdate'=>'function(id, data){console.log("afterupdate 2")}',
            'columns'=>array(
                array(
                    'name' => 'nroCombrobante',
                    'header' => 'nro comp.',
                    'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
                ),
                array(
                    'name' => 'dig',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'headerHtmlOptions' => array('style' => 'width:30px;text-align:center;'),
                ),
                array(
                    'name' => 'idCliente',
                    'value' => '($data->oCliente)? $data->oCliente->title : "RETIRAR ESTA NOTA DE PEDIDO Y VERIFICAR CLIENTE"',
                    'htmlOptions' => array('style' => ''),
                    'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','Title'),
                ),
                array(
                    'name' => 'Importe',
                    'value' => '$data->SubTotal',
                    'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                ),

                array(
                    'name' => 'fecha',
                    'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
                    'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
                ),

                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{notaEntrega} {linkNotaEntrega} | {factura} {linkFactura}{linkFacturaSinCae}',
                    'headerHtmlOptions' => array(
                        'style' => 'width:160px',
                    ),
                    'buttons' => array(
                        'notaEntrega' => array
                        (
                            'label'=>'Crear N.E.',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                idRow = $(this).attr('href');
                                selectedRow = $(this).parent().parent();
                                notaEntrega($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small ne',
                            ),
                            'visible' => '$data->oFactura == null && $data->oNotaEntrega == null'
                        ),
                        'linkNotaEntrega' => array
                        (
                            'label'=>'Ver NE.',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                notaEntrega($(this).attr('href'));
                                idRow = $(this).attr('href');
                                selectedRow = $(this).parent().parent();
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liNe',
                            ),
                            'visible' => '$data->oNotaEntrega != null '
                        ),
                        'factura' => array
                        (
                            'label'=>'Facturar.',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                idRow = $(this).attr('href');
                                selectedRow = $(this).parent().parent();
                                factura($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small fa',
                            ),
                            'visible' => '$data->oFactura == null && $data->oNotaEntrega == null && ($data->oCliente)? $data->oCliente->esFacturable() : false '
                        ),
                        'linkFactura' => array
                        (
                            'label'=>'Ver Factura',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                idRow = $(this).attr('href');
                                selectedRow = $(this).parent().parent();
                                factura($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liFa',
                            ),
                            'visible' => '$data->oFactura != null && $data->oFactura->estado >= Comprobante::iCerrada'
                        ),
                        'linkFacturaSinCae' => array
                        (
                            'label'=>'Factura Sin Cae',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                idRow = $(this).attr('href');
                                selectedRow = $(this).parent().parent();
                                factura($(this).attr('href'));
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liFa',
                            ),
                            'visible' => '$data->oFactura != null && ($data->oFactura->CAE == "" || $data->oFactura->CAE == "00000000000000")'
                        ),
                    )
                ),
                array(
                    'header' => 'Monto Facturado',
                    'value' => '($data->oFactura != null)? $data->oFactura->Total_Final : (($data->oNotaEntrega != null)? $data->oNotaEntrega->totalFinal : "" )',
                    'htmlOptions' => array('style' => 'width:115px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:115px;text-align:right;'),
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{view} | {delete}',
                    'buttons'=>array(
                        'view' => array(
                            'label' => 'Ver ',
                            'url'=>'Yii::app()->controller->createUrl("NotaPedido/view", array("id"=>$data->id))',
                            'options'=>array(),
                        ),
                        'delete' => array(
                            'label' => 'Desvincular Nota.',
                            'icon' => 'resize-full',
                            //'deleteConfirmation' => "js: 'Desea desvincular la Nota De Pedido?'",

                            'visible' => ($model->estado <= Carga::iFacturada)? 'true' : 'false',
                            'url'=>'Yii::app()->controller->createUrl("NotaPedido/DesvincularCarga", array("id"=>$data->id))',
                            'click'=>'function(){
                                if (confirm("Desea desvincular la Nota De Pedido?")){
                                    try{
                                        console.log("Pre submit:...");
                                        jQuery.yii.submitForm(document.body, $(this).attr("href"), {});
                                        console.log("success:...");
                                        $.fn.yiiGridView.update("prod-carga-grid");
                                        return false;
                                    }catch(e){
                                        console.warn(e);
                                    }

                                }else{
                                    return false;
                                }
                             }',
                        ),
                    ),
                ),
            ),
        )); ?>