<?php
/* @var $this CargaController */
/* @var $model Carga */

$this->breadcrumbs=array(
	'Cargas'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Carga', 'url'=>array('admin')),
);
?>

<h2>Nueva Carga</h2>

<?php echo $this->renderPartial('_formNueva', array('model'=>$model,'idCamion' => $idCamion,)); ?>
<div class="modalLoading" ><!-- Place at bottom of page --></div>
