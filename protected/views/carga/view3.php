<?php
/* @var $model Carga */

?>
<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    .modal-dialog {
        margin: 2.5vh auto;
    }

    /*Sets the maximum height of the entire modal to 95% of the screen height*/
    .modal-content {
        max-height: 95vh;
        overflow: scroll;
    }

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    .modal-body {
        max-height: 90vh;
    }
    /*Sets the maximum height of the modal image to 69% of the screen height*/
    .modal-body img {
        max-height: 69vh;
    }

    .modal{
        width: 94%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);

    }

    .modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    .modalLoading {
        display:    none;
        position:   fixed;
        z-index:    1500;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 )
        url('<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif')
        50% 50%
        no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading .modalLoading {
        overflow: hidden;
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modalLoading {
        display: block;
    }

</style>
<script language="JavaScript">
    console.log('carga...');

    $(document).on('hidden', '#modalNotaEntrega', function() {
        console.log('modalNotaEntrega L '+$('#nota-pedido-grid2').length);

        if($('#nota-pedido-grid2').length > 0){
            try {
                $('#modalNotaEntrega .modal-body').html('');
                //FALLA SI NO VIENE EL FORMULARIO
                $.fn.yiiGridView.update('nota-pedido-grid2');
            } catch (e){
                console.log(e);
            }
        }
    });

    $(document).on('hidden', '#modalFactura', function() {
        console.log('modalFactura'+$('#nota-pedido-grid2').length);
        if($('#nota-pedido-grid2').length > 0) {
            try{
                $('#modalFactura .modal-body').html('');
                //FALLA SI NO VIENE EL FORMULARIO
                $.fn.yiiGridView.update('nota-pedido-grid2');
            } catch (e){
                console.log(e);
            }
        }
    });
</script>
<?php

Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
    
    /*
    $('.print-button-row').click(function(e){        
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = href+'&print=true';
        console.log();
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
        return false;   
    });
    */
",CClientScript::POS_READY);

$this->breadcrumbs=array(
	'Cargas'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva Carga', 'url'=>array('create')),
	array('label'=>'Modificar Carga', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Carga', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Carga', 'url'=>array('admin')),
);
Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
");

Yii::app()->clientScript->registerScript('viewCarga', "
    $('#depurar').click(function(e){
            var datos = 'Depuracion%5BidCarga%5D='+$(this).attr('idCarga')+'&ajax=';
            e.stopPropagation();
            e.preventDefault();            
            
            //var datos = '';
            var url = $(this).attr('href');
            console.log(url);
                        
            $.ajax({
                url:url,
                type:'POST',
                data: datos,
                beforeSend: function() { $('.modalLoading').show('slow'); },
                complete: function() { 
                  $('.modalLoading').hide('slow'); 
                  $('#idCae').closest('.modal.fade.in').modal('hide');
                  $('.modalLoading').hide('slow');
                },
                success:function(html) {
                  //TODO: SE HACE ALGO CON EL html?
                  console.log(html);
                  $.fn.yiiGridView.update('depuracion-grid');
                  return false;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  respuesta =  $.parseHTML(x.responseText);
                  errorDiv = $(respuesta).find('.errorSpan');
                  console.log(errorDiv);
                  var htmlError = $(errorDiv).html();
                  
                  console.log(htmlError);
                  if (typeof value === 'undefined') {
                        alert($(respuesta).text());
                  }else{
                        alert(htmlError);
                  }
                  $('.modalLoading').hide('slow');
                  return false;
                  
                }
            });
        });

    function factura(idNotaPedido){
        var datos = 'is_modal=1&idNotaPedido='+idNotaPedido;
        $.ajax({
            url: '".$this->createUrl('factura/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                $('#modalFactura .modal-body').html(html);
                $('#modalFactura .modal-body').find('#fecha').datepicker();
                try{
                    // DEBE VENIR EL FORMULARIO, SINO VA POR EL CATCH
                    bindFacturaHandlers();
                }catch(e){
                    console.log('Todo normal, viene el view sin el formulario');
                }
                
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorNotaPedido').html(htmlError);
                //$('#errorNotaPedido').show('slow');
                
                
            }
        });
        
        $('#modalFactura').modal();
    }
    
    function notaEntrega(idNotaPedido){
        var datos = 'is_modal=1&idNotaPedido='+idNotaPedido;
        $.ajax({
            url:'".$this->createUrl('notaEntrega/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                $('#modalNotaEntrega .modal-body').html(html);
                $('#modalNotaEntrega .modal-body').find('#fecha').datepicker();
                
                try{
                    // DEBE VENIR EL FORMULARIO, SINO VA POR EL CATCH
                    bindNotaEntregaHandlers();
                }catch(e){
                    console.log('Todo normal, viene el view sin el formulario');
                }
                
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorNotaPedido').html(htmlError);
                //$('#errorNotaPedido').show('slow');
                
                
            }
        });
        
        $('#modalNotaEntrega').modal();
    }
    
");

if ($model->estado < Carga::iDespachada){
    Yii::app()->clientScript->registerScript('sinCarga', "
        $('a[href=\"#sinCarga\"]').click(function(){
            var datos = 'agregar=1&idCamion='+".$model->idVehiculo."+'&idCarga='+".$model->id.";
            $.ajax({
                url:'".$this->createUrl('NotaPedido/SinNcarga')."',
                type: 'Get',
                data: datos,
                success: function (html) {
                    $('#sinCarga').html(html);
                    
                },
                error: function (x,y,z){
                    //TODO: MANEJAR EL ERROR
                    
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    console.log($(errorDiv).parent().next());
                    
                }
            });
        });
    ");
}


?>

<h2>Ver Carga #<?php echo $model->id; ?></h2>


<div class="content-fluid">
    <div class="row-fluid">
        <div class="span4">
            <?php
            $this->widget('editable.EditableDetailView', array(
                'data'       => $model,

                //you can define any default params for child EditableFields
                'url'        => $this->createUrl('Carga/SaveFieldAsync'), //common submit url for all fields
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                //'apply' => false, //you can turn off applying editable to all attributes

                'attributes' => array(

                    array( //select loaded from database
                        'name' => 'idVehiculo',
                        'editable' => array(
                            'type'   => 'select',
                            'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre'),
                            'mode'      => 'popup',
                            'model'     => $model,
                            'apply'     => ($model->estado < Carga::iDespachada), //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),
                    array(
                        'name' => 'estado',
                        'value' => Carga::$aEstado[$model->estado],
                        'editable' => array(
                            'apply'      => false,
                        ),
                        'htmlOptions' => array('style' => ''),
                    ),

                )
            ));
            ?>

        </div>
        <div class="span8">
            <?php
            $this->widget('editable.EditableDetailView', array(
                'data'       => $model,

                //you can define any default params for child EditableFields
                'url'        => $this->createUrl('Carga/SaveFieldAsync'), //common submit url for all fields
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                //'apply' => false, //you can turn off applying editable to all attributes

                'attributes' => array(

                    array( //select loaded from database
                        'name' => 'observacion',
                        'editable' => array(
                            'type'      => 'textarea',
                            'mode'      => 'popup',
                            'model'     => $model,
                            'attribute' => 'observacion',
                            'apply'     => ($model->estado < Carga::iDespachada), //NO SE PUEDEN MODIFICAR CARGAS CERRADAS
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),
                    array(
                        'name' => 'fecha',
                        'editable' => array(
                            'type'       => 'date',
                            'format'      => 'yyyy-mm-dd', //format in which date is expected from model and submitted to server
                            'apply'     => ($model->estado < Carga::iDespachada), //NO SE PUEDEN MODIFICAR CARGAS CERRADAS
                            'viewformat' => 'dd/mm/yyyy',
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),

                )
            ));
            ?>

        </div>
    </div>
</div>

<?php
if ($model->estado == Carga::iBorrador){
    echo CHtml::link('Marcar Carga como Facturada', Yii::app()->createUrl('Carga/Facturada',array('id'=>$model->id)), array('class' => 'bulk-button btn', 'style' => 'float:left;margin-right: 20px;'));
}
if ($model->oEntregaDoc != null) {
    if ($model->estado == Carga::iFacturada){
        echo CHtml::link('Ver la Hoja de ruta asociada <i class="icon-white icon-road"></i>', Yii::app()->createUrl('EntregaDoc/ViewPrevia',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary', 'style' => 'float:left;margin-right: 20px;'));
    }else{
        echo CHtml::link('Ver la Hoja de ruta asociada <i class="icon-white icon-road"></i>', Yii::app()->createUrl('EntregaDoc/ViewPrevia',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary ', 'style' => 'float:left;margin-right: 20px;'));
        echo CHtml::link('Ver la planilla de rendición <i class="icon-white icon-list-alt"></i>', Yii::app()->createUrl('EntregaDoc/rendir',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary ', 'style' => 'float:left;margin-right: 20px;'));
    }

}else{
    if ($model->estado != Carga::iDescartada && $model->estado != Carga::iBorrador && $model->estado != Carga::iDespachada){
        echo CHtml::link('Crear Hoja de ruta', Yii::app()->createUrl('EntregaDoc/CrearFromCarga',array('idCarga'=>$model->id)), array('class' => 'bulk-button btn btn-primary', 'style' => 'float:left;margin-right: 20px;'));
    }
}

?>

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#productos">Productos</a></li>
    <li><a data-toggle="tab" href="#notas">Notas De Pedido Asociadas</a></li>
    <?php
    if ($model->estado != Carga::iDespachada && $model->estado != Carga::iDescartada){
        ?>
        <li><a data-toggle="tab" href="#sinCarga">Agregar Notas </a></li>
        <?php
    }
    ?>
    <li><a data-toggle="tab" href="#depuraciones"><?=Depuracion::model()->getAttributeLabel('models')?></a></li>
</ul>
<div class="tab-content">
    <div id="productos" class="tab-pane fade in active">
        <?php
            //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
            echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf2', array('id'=>$model->id)),
                array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'print' => Yii::app()->createUrl('carga/pdf2', array('id'=>$model->id,'print'=>true)) ));
        ?>

        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'prod-carga-grid',
            'afterAjaxUpdate'=>'function(id, data){console.log("afterupdate 1")}',
            'dataProvider'=>$aProds->searchWP(),
            'columns'=>array(
                array(
                    'name' => 'idArtVenta',
                    'header' => 'Codigo',
                    'value' => '$data->oArtVenta->codigo',
                    'htmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'idArtVenta',
                    'header' => 'Articulo',
                    'value' => '$data->oArtVenta->descripcion',
                    'htmlOptions' => array('style' => ''),
                    'footer' => 'La carga actual es de '.$model->getPesoTotal().' kilogramos',
                ),

                array(
                    'name' => 'cantXbulto',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:85px;'),
                ),

                array(
                    'name' => 'Bultos',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
                ),

                array(
                    'name' => 'envase',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
                ),

            ),
        )); ?>
    </div>

    <div id="notas" class="tab-pane fade">
        <?php
            if ($model->estado >= Carga::iFacturada){
                echo CHtml::link('Imprimir Notas de Entrega <i class="icon-white icon-print"></i>',
                    Yii::app()->createUrl('NotaEntrega/PdfxCarga',array('idCarga'=>$model->id)),
                    array('class'=>'btn btn-primary print-button','id' => 'imprimirNE','style' => 'float:left;', 'target' => '_blank;',
                    'print' => Yii::app()->createUrl('NotaEntrega/PdfxCarga', array('idCarga'=>$model->id,'print'=>true))
                ));
                echo CHtml::link('Imprimir Facturas <i class="icon-print"></i>',Yii::app()->createUrl('Factura/PdfxCarga', array('idCarga'=>$model->id)),
                    array('class'=>'btn  print-button','id' => 'imprimirFA','style' => 'float:left;margin-left:5px;', 'target' => '_blank;',
                        'print' => Yii::app()->createUrl('Factura/PdfxCarga', array('id'=>$model->id,'print'=>true))
                    ));
            }

        ?>
        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'nota-pedido-grid2',
            'dataProvider'=>$aNotasPedido->searchWp(),
            'afterAjaxUpdate'=>'function(id, data){console.log("afterupdate 2")}',
            'columns'=>array(
                array(
                    'name' => 'nroCombrobante',
                    'header' => 'nro comp.',
                    'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
                ),
                array(
                    'name' => 'dig',
                    'htmlOptions' => array('style' => 'text-align:center;'),
                    'headerHtmlOptions' => array('style' => 'width:30px;text-align:center;'),
                ),
                array(
                    'name' => 'idCliente',
                    'value' => '$data->oCliente->Title',
                    'htmlOptions' => array('style' => ''),
                    'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','Title'),
                ),
                array(
                    'name' => 'Importe',
                    'value' => '$data->SubTotal',
                    'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                ),

                array(
                    'name' => 'fecha',
                    'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
                    'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
                ),

                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{notaEntrega} {linkNotaEntrega} | {factura} {linkFactura}{linkFacturaSinCae}',
                    'headerHtmlOptions' => array(
                        'style' => 'width:160px',
                    ),
                    'buttons' => array(
                        'notaEntrega' => array
                        (
                            'label'=>'Crear N.E.',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopPropagation();
                                e.preventDefault();
                                notaEntrega($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small ne',
                            ),
                            'visible' => '$data->oFactura == null && $data->oNotaEntrega == null'
                        ),
                        'linkNotaEntrega' => array
                        (
                            'label'=>'Ver NE.',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                notaEntrega($(this).attr('href'));
                                console.log('Ver NE.');
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liNe',
                            ),
                            'visible' => '$data->oNotaEntrega != null '
                        ),
                        'factura' => array
                        (
                            'label'=>'Facturar',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                factura($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small fa',
                            ),
                            'visible' => '$data->oFactura == null && $data->oNotaEntrega == null'
                        ),
                        'linkFactura' => array
                        (
                            'label'=>'Ver Factura',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();
                                factura($(this).attr('href'));
                                return false;
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liFa',
                            ),
                            'visible' => '$data->oFactura != null && $data->oFactura->estado >= Comprobante::iCerrada'
                        ),
                        'linkFacturaSinCae' => array
                        (
                            'label'=>'Factura Sin Cae',
                            'url'=>'$data->id',
                            'click' => "js:function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation();
                                e.preventDefault();                                
                                factura($(this).attr('href'));                                
                            }",
                            // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                            'options'=>array(
                                'class'=>'btn btn-small liFa',
                            ),
                            'visible' => '$data->oFactura != null && ($data->oFactura->CAE == "" || $data->oFactura->CAE == "00000000000000")'
                        ),
                    )
                ),
                array(
                    'header' => 'Monto Facturado',
                    'value' => '($data->oFactura != null)? $data->oFactura->Total_Final : (($data->oNotaEntrega != null)? $data->oNotaEntrega->totalFinal : "" )',
                    'htmlOptions' => array('style' => 'width:115px;text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'width:115px;text-align:right;'),
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{view} | {delete}',
                    'buttons'=>array(
                        'view' => array(
                            'label' => 'Ver',
                            'url'=>'Yii::app()->controller->createUrl("NotaPedido/view", array("id"=>$data->id))',
                            'options'=>array(),
                        ),
                        'delete' => array(
                            'label' => 'Desvincular Nota.',
                            'icon' => 'resize-full',
                            //'deleteConfirmation' => "js: 'Desea desvincular la Nota De Pedido?'",

                            'visible' => ($model->estado == Carga::iBorrador)? 'true' : 'false',
                            'url'=>'Yii::app()->controller->createUrl("NotaPedido/DesvincularCarga", array("id"=>$data->id))',
                            'click'=>'function(){ 
                                if (confirm("Desea desvincular la Nota De Pedido?")){                                    
                                    try{        
                                        console.log("Pre submit:...");                            
                                        jQuery.yii.submitForm(document.body, $(this).attr("href"), {});
                                            console.log("success:...");
                                            $.fn.yiiGridView.update("prod-carga-grid");
                                            return false;
                                        return false;
                                    }catch(e){
                                        console.warn(e);
                                    }

                                }else{
                                    return false;                                
                                }
                             }',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
    <?php
        if ($model->estado < Carga::iDespachada){
            // SOLO TRAE LAS NOTAS SIN CARGA CUANDO SE PRESIONA LA SOLAPA
            ?>
            <div id="sinCarga" class="tab-pane fade">

            </div>
            <?php
        }
    ?>
    <div id="depuraciones" class="tab-pane fade">
        <h4><?=Depuracion::model()->getAttributeLabel('models')?></h4>
        <?php
            if($model->estado < Carga::iDespachada){
                echo CHtml::link('Nueva Depuración',Yii::app()->createUrl('depuracion/saveAsync'),array( 'idCarga'=> $model->id,'class'=>'bulk-button btn','id' => 'depurar','style' => 'float:left;', 'target' => '_blank;' ));
            }
        ?>
        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'depuracion-grid',
            'dataProvider'=>$aDepuraciones->search(),
            'columns'=>array(
                'id',
                array(
                    'name' => 'fecha',
                    'value' => 'ComponentesComunes::fechaFormateada($data->fecha,"d-m-Y HH:mm")',
                    'htmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'items',
                    'value' => 'sizeof($data->aProdDepuracion)',
                    'htmlOptions' => array('style' => ''),
                ),

                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{view} | {delete}',
                    'buttons'=>array(
                        'view' => array(
                            'label' => 'Imprimir',
                            'icon' => 'print',
                            'url'=>'Yii::app()->controller->createUrl("Depuracion/viewPdf", array("id"=>$data->id))',
                            //'url'=>'$data->id',

                            'click' => "js:function(e){
                                e.stopPropagation();
                                e.preventDefault();
                                let href = $(this).attr('href');
                                let print = href+'&print=true';
                                console.log();
                                $('#download-form').attr('action',href);
                                $('#print-form').attr('action',print);        
                                $('#modalPrint').modal();
                                return false;
                            }",

                            'options'=>array(
                                //'target'=>'_blank',
                                'class' => 'print-button-row',
                                'print' => Yii::app()->createUrl('Depuracion/viewPdf', array('id'=>$model->id,'print'=>true)),

                            ),
                        ),
                        'delete' => array(
                            'label' => 'Borrar Depuración',
                            'url'=>'Yii::app()->controller->createUrl("Depuracion/delete", array("id"=>$data->id))',
                            'visible' => '$data->oCarga->estado < Carga::iDespachada',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>

</div>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalFactura" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Facturar </h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif"></img>
            </div>
        </div>

    </div>
</div>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalNotaEntrega" class="modal fade" tabindex="-1">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nueva Nota de Entrega </h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif"></img>
            </div>

        </div>

    </div>
</div>
<div class="modalLoading" ><!-- Place at bottom of page --></div>
<?php
$this->widget('BuscaArtVenta', array('js_comp'=>'#ProdFactura_idArtVenta','is_modal' => true,'ajaxMode' => false));
?>
<?php $this->widget('PrintWidget') ?>