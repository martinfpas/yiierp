<?php
$this->breadcrumbs=array(
	'Cargas'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nueva Carga', 'url'=>array('create')),
	array('label'=>'Ver Carga', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Carga', 'url'=>array('admin')),
);
?>

	<h2>Modificando Carga <?php echo $model->id; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-carga-grid',
    'dataProvider'=>$aProds->search(),
    'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => 'Codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'idArtVenta',
            'header' => 'Articulo',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => ''),
            'footer' => 'La carga actual es de '.$model->getPesoTotal().' kilogramos',
        ),

        array(
            'name' => 'cantXbulto',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
        ),
        array(
            'name' => 'cantidad',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),

        ),

        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("ProdCarga/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>

