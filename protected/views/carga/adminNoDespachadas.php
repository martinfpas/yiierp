<?php
/* @var $this CargaController */
/* @var $model Carga */

$this->breadcrumbs=array(
	'Cargas'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva Carga', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#carga-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Cargas No Despachadas </h2>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'carga-grid',
'dataProvider'=>$model->searchNoDespachadas(),
'filter'=>$model,
'columns'=>array(
		array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
		),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
        ),

        array(
            'name' => 'idVehiculo',
            'value' => '$data->oCamion->IdNombre',
            'htmlOptions' => array('style' => 'width:100px;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),
            'filter' =>  CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),
        ),
        array(
            'name' => 'estado',
            'value' => 'Carga::$aEstadoNoDesp[$data->estado]',
            'htmlOptions' => array('style' => ''),
            'filter' => Carga::$aEstadoNoDesp,
        ),
        array(
            'name' => 'observacion',
            'value' => 'substr($data->observacion,0,50)',
            //'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'PesoTotal',
            'value' => 'number_format($data->PesoTotal,2,",","")." kg"',
            'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
        ),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} | {delete} ',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Borrar Carga ',
                    'visible' => '$data->estado == Carga::iBorrador',
                ),
            ),
        ),
	),
)); ?>
