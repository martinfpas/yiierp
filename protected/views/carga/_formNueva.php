<?php


if ($idCamion > 0){
    Yii::app()->clientScript->registerScript('idCamion', "
        $(document).ready(function(){
            console.log('docuement ready');
            changeVehiculo();
        });    
    ");
}

Yii::app()->clientScript->registerScript('tooltip', "
    

    $('#Carga_idVehiculo').change(function(){
            changeVehiculo();
    });
    
    function changeVehiculo(){
        if($('#Carga_id').val() != ''){
                traerNotasDP();
            }else{
                var datos = $('#carga-form').serialize();
                $.ajax({
                    url: '".Yii::app()->createUrl('carga/saveAsync')."',
                    data: datos,
                    type: 'POST',
                    success:function(html) {
                        $('#Carga_id').val(html);
                        $('#nCarga').html('Nº de carga: '+html);
                        traerNotasDP();
                                      
                    },
                    error:function(x,y,z) {
                      console.warn(x);
                    },
                });        
            }
    }
    

    function traerNotasDP(){
        $('.modalLoading').show('slow');
        var datos = $('#carga-form').serialize();
        $.ajax({
            url: '".Yii::app()->createUrl('NotaPedido/SinNcarga')."',
            data: datos,
            type: 'POST',
            success:function(html) {                
                $('#NotasDePedido').html(html);
                $('.modalLoading').hide('slow');              
            },
            error:function(x,y,z) {
                $('.modalLoading').hide('slow');
                console.warn(x.responseText);
                alert(x.responseText);
            },
        });
    }
");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'carga-form',
	'enableAjaxValidation'=>false,
));


?>


	<div class="alert alert-block alert-error" style="display:none;" id="errorCarga"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCarga">Datos Guardados Correctamente !</div>
	
<?php echo $form->errorSummary($model); ?>
<?php echo $form->hiddenField($model,'id'); ?>
<?php echo $form->hiddenField($model,'fecha'); ?>

<div class="content-fluid">
	<div class="row-fluid">
		<div class="span3">
            <?php echo $form->labelEx($model,'idVehiculo'); ?>
            <?php echo $form->dropDownList($model,'idVehiculo',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array('empty' => '--Seleccione uno ---' )); ?>
        </div>
        <div class="span3">
        </div>
        <div class="span3">
        </div>
        <div class="span3" id="nCarga">

        </div>
    </div>
</div>



<?php $this->endWidget(); ?>

<div id="NotasDePedido"></div>