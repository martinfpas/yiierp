<?php
/* @var $this CargaController */
/* @var $model Carga */

$this->breadcrumbs=array(
	'Cargas'=>array('adminHistoricas'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva Carga.', 'url'=>array('create')),
);

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/shortcut.js");

Yii::app()->clientScript->registerScript('search2', "
$('.search-button').click(function(){
    console.log('search');
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#carga-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

function moveDown() {
    var rows = $('#carga-grid table tr');
    var currentRow = $(\"tr.selected\").get(0);

    if (rows.length > 2) {
        if (currentRow === undefined) {
            rows.eq(1).addClass('selected');
        } else if ($(currentRow).next('tr').get(0) === undefined) {
            //do nothing
        } else {
            $(currentRow).next('tr').addClass('selected');
            $(currentRow).removeClass('selected');
        }
    }
}

function moveUp() {
    console.log('moveUp');
    var rows = $('#carga-grid table tr');
    var currentRow = $(\"tr.selected\").get(0);

    if (rows.length > 2) {
        if (currentRow === undefined) {
            rows.eq(1).addClass('selected');
        } else if ($(currentRow).prev('tr').get(0) === undefined) {
            //do nothing
        } else {
            $(currentRow).prev('tr').addClass('selected');
            $(currentRow).removeClass('selected');
        }
    }
}

$(document).ready(function(){    
    shortcut.add('Down', function() {
        moveDown();
    });
    
    shortcut.add('Up', function() {
        moveUp();
    });
});

",CClientScript::POS_READY);
?>

<h2>Gestion de Cargas</h2>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'carga-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
		),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
        ),

        array(
            'name' => 'idVehiculo',
            'value' => '$data->oCamion->IdNombre',
            'htmlOptions' => array('style' => 'width:100px;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),
            'filter' =>  CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),
        ),
        array(
            'name' => 'estado',
            'value' => 'Carga::$aEstado[$data->estado]',
            'htmlOptions' => array('style' => ''),
            'filter' => Carga::$aEstado,
        ),



		array(
			//'name' => 'observacion',
			'header' => 'Nº items',
			'value' => 'sizeof($data->aProdCarga)',
			//'htmlOptions' => array('style' => ''),
		),


        array(
            'name' => 'PesoTotal',
            'value' => 'number_format($data->PesoTotal,2,",","")." kg"',
            'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
        ),

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} | {delete} ',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Borrar Carga ',
                    'visible' => '$data->estado == Carga::iBorrador',
                ),
            ),
		),
	),
)); ?>
