<?php
/**
 * @var $model Carga
 * @var $aProds ProdCarga
 */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/shortcut.js");

?>
<style>
    /*When the modal fills the screen it has an even 2.5% on top and bottom*/
    /*Centers the modal*/
    .modal-dialog {
        margin: 2.5vh auto;
    }

    /*Sets the maximum height of the entire modal to 95% of the screen height*/
    .modal-content {
        max-height: 95vh;
        overflow: scroll;
    }

    /*Sets the maximum height of the modal body to 90% of the screen height*/
    .modal-body {
        max-height: 90vh;
    }
    /*Sets the maximum height of the modal image to 69% of the screen height*/
    .modal-body img {
        max-height: 69vh;
    }

    .modal{
        width: 94%;
        margin: auto;
        left: inherit;
        top: 10px;

        left: 50%;
        /*top: 38%;*/
        top: 1%;
        /*transform: translate(-50%, -50%);*/
        transform: translate( -50%);

    }

    .modal.fade.in {
        /*top: 38%;*/
        top: 1%;
    }

    h4.modal-title {
        margin: 0px;
    }
    .modal-header{
        padding: 1px 15px;
    }

    .modalLoading {
        display:    none;
        position:   fixed;
        z-index:    1500;
        top:        0;
        left:       0;
        height:     100%;
        width:      100%;
        background: rgba( 255, 255, 255, .8 )
        url('<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif')
        50% 50%
        no-repeat;
    }

    /* When the body has the loading class, we turn
       the scrollbar off with overflow:hidden */
    body.loading .modalLoading {
        overflow: hidden;
    }

    /* Anytime the body has the loading class, our
       modal element will be visible */
    body.loading .modalLoading {
        display: block;
    }
    .alerta{
        background-color: #ff7b7b;
    }

</style>
<script language="JavaScript">
    //console.log('carga...');

    $(document).on('hidden', '#modalNotaEntrega', function() {
        //console.log('modalNotaEntrega L '+$('#nota-pedido-grid2').length);
        if($('#nota-pedido-grid2').length > 0){
            try {
                $('#modalNotaEntrega .modal-body').html('');
                //FALLA SI NO VIENE EL FORMULARIO
                //LA FALLA SE DA SI NO SE EXCLUYEN LOS JQUERY DE LOS HTML QUE VIENEN POR AJAX
                $.fn.yiiGridView.update('nota-pedido-grid2');
            } catch (e){
                alert('La planilla no se actualizó. Presione F5');
                console.log(e);
            }
        }
    });

    $(document).on('hidden', '#modalFactura', function() {
        //console.log('modalFactura'+$('#nota-pedido-grid2').length);
        if($('#nota-pedido-grid2').length > 0) {
            try{
                $('#modalFactura .modal-body').html('');
                //FALLA SI NO VIENE EL FORMULARIO
                $.fn.yiiGridView.update('nota-pedido-grid2');
            } catch (e){
                console.log(e);
                alert('La planilla no se actualizó. Presione F5');
            }
        }
    });
    /****** CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
    function moveDown() {
        var rows = $('#prod-carga-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).next('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).next('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }

    function moveUp() {
        var rows = $('#prod-carga-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).prev('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).prev('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }

    $(document).ready(function(){
        shortcut.add('Down', function() {
            moveDown();
        });

        shortcut.add('Up', function() {
            moveUp();
        });
        shortcut.add('F5', function() {
            console.log('F5');
            $.fn.yiiGridView.update('nota-pedido-grid2');
        });

    });

    /****** fin CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
</script>
<?php

Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
    
    /*
    $('.print-button-row').click(function(e){        
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = href+'&print=true';
        //console.log();
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
        return false;   
    });
    */
",CClientScript::POS_READY);

$this->breadcrumbs=array(
	'Cargas'=>array('admin'),
	$model->id,
);

$this->menu=array(	
	array('label'=>'Volver a la Carga', 'url'=>array('view', 'id'=>$model->id)),	
);


?>

<h2>Revision de Carga #<?php echo $model->id; ?></h2>

<?php
//print_r(Camionero::model()->findAll());

?>

<div class="content-fluid">
    <div class="row-fluid">
        <div class="span4">
            <?php
            $this->widget('editable.EditableDetailView', array(
                'data'       => $model,

                //you can define any default params for child EditableFields
                'url'        => $this->createUrl('Carga/SaveFieldAsync'), //common submit url for all fields
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                //'apply' => false, //you can turn off applying editable to all attributes

                'attributes' => array(

                    array( //select loaded from database
                        'name' => 'idVehiculo',
                        'editable' => array(
                            'type'   => 'select',
                            'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre'),
                            'mode'      => 'popup',
                            'model'     => $model,
                            'apply'     => false,
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),
                    array(
                        'name' => 'estado',
                        'value' => Carga::$aEstado[$model->estado],
                        'editable' => array(
                            'apply'      => false,
                        ),
                        'htmlOptions' => array('style' => ''),
                    ),
                    array( //select loaded from database
                        'name' => 'idCamionero',
                        'editable' => array(
                            'type'   => 'select',
                            'source' => Editable::source(array_merge(array(''=>new Camionero()),Camionero::model()->findAll()), 'id', 'titulo'),
                            'mode'      => 'popup',
                            'model'     => $model,
                            'apply'     => false,
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),

                )
            ));
            ?>

        </div>
        <div class="span8">
            <?php
            $this->widget('editable.EditableDetailView', array(
                'data'       => $model,

                //you can define any default params for child EditableFields
                'url'        => $this->createUrl('Carga/SaveFieldAsync'), //common submit url for all fields
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                //'apply' => false, //you can turn off applying editable to all attributes

                'attributes' => array(

                    array( //select loaded from database
                        'name' => 'observacion',
                        'editable' => array(
                            'type'      => 'textarea',
                            'mode'      => 'popup',
                            'model'     => $model,
                            'attribute' => 'observacion',
                            'apply'     => false,
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),
                    array(
                        'name' => 'fecha',
                        'editable' => array(
                            'type'       => 'date',
                            'format'      => 'yyyy-mm-dd', //format in which date is expected from model and submitted to server
                            'apply'     => false,
                            'viewformat' => 'dd/mm/yyyy',
                            'url'       => $this->createUrl('Carga/SaveFieldAsync'),
                        )
                    ),

                )
            ));
            ?>

        </div>
    </div>
</div>

<?php
/*
if ($model->estado == Carga::iBorrador){
    echo CHtml::link('Marcar Carga como Facturada', Yii::app()->createUrl('Carga/Facturada',array('id'=>$model->id)), array('class' => 'bulk-button btn', 'style' => 'float:left;margin-right: 20px;'));
}
if ($model->oEntregaDoc != null) {
    if ($model->estado == Carga::iFacturada){
        echo CHtml::link('Ver la Hoja de ruta asociada <i class="icon-white icon-road"></i>', Yii::app()->createUrl('EntregaDoc/ViewPrevia',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary', 'style' => 'float:left;margin-right: 20px;'));
    }else{
        echo CHtml::link('Ver la Hoja de ruta asociada <i class="icon-white icon-road"></i>', Yii::app()->createUrl('EntregaDoc/ViewPrevia',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary ', 'style' => 'float:left;margin-right: 20px;'));
        echo CHtml::link('Ver la planilla de rendición <i class="icon-white icon-list-alt"></i>', Yii::app()->createUrl('EntregaDoc/rendir',array('id'=>$model->oEntregaDoc->id)), array('class' => 'bulk-button btn btn-primary ', 'style' => 'float:left;margin-right: 20px;'));
    }

}else{
    if ($model->estado != Carga::iDescartada && $model->estado != Carga::iBorrador && $model->estado != Carga::iDespachada){
        echo CHtml::link('Crear Hoja de ruta', Yii::app()->createUrl('EntregaDoc/CrearFromCarga',array('idCarga'=>$model->id)), array('class' => 'bulk-button btn btn-primary', 'style' => 'float:left;margin-right: 20px;'));
    }
}
*/
?>


<div class="content-fluid">
        <?php
            /*
            //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
            echo CHtml::link('Imprimir',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),
                array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'print' => Yii::app()->createUrl('carga/pdf', array('id'=>$model->id,'print'=>true)) ));
            */
            $dataProvider=new CArrayDataProvider($model->aProdCarga,array(
                'pagination'=> false,
            )
        );
        

        $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'prod-carga-grid',
            //'afterAjaxUpdate'=>'function(id, data){console.log("afterupdate 1")}',
            'dataProvider'=>$dataProvider,
            'template' => '{items}',
            'rowCssClassExpression'=>'($data->BultosRecalculados != $data->Bultos)?"alerta":"normal"',
            'columns'=>array(
                array(
                    'name' => 'idArtVenta',
                    'header' => 'Codigo',
                    'value' => '$data->oArtVenta->codigo',
                    'htmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'idArtVenta',
                    'header' => 'Articulo',
                    'value' => '$data->oArtVenta->descripcion',
                    'htmlOptions' => array('style' => ''),
                    'footer' => 'La carga actual es de '.$model->getPesoTotal().' kilogramos',
                ),

                array(
                    'name' => 'cantXbulto',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:85px;'),
                ),

                array(
                    'name' => 'Bultos',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
                ),

                array(
                    'name' => 'BultosRecalculados',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
                ),

                array(
                    'name' => 'envase',
                    'htmlOptions' => array('style' => 'text-align:right'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{view}',
                        'buttons'=>array(
                            'view' => array(
                                'label' => 'Ver',
                                'url'=>'Yii::app()->controller->createUrl("prodCarga/view", array("id"=>$data->id))',
                                'options'=>array('target'=>'_blank'),
                            ),
                        ),
                ),
            ),
        )); ?>

</div>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalFactura" class="modal fade" tabindex="-1" >
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Facturar </h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif"></img>
            </div>
        </div>

    </div>
</div>

<!-- EL  >> tabindex="-1" << SIRVE PARA CERRAR EL MODAL CON ESCAPE -->
<div id="modalNotaEntrega" class="modal fade" tabindex="-1">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Nueva Nota de Entrega </h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ajax-loader.gif"></img>
            </div>

        </div>

    </div>
</div>
<div class="modalLoading" ><!-- Place at bottom of page --></div>
<?php
$this->widget('BuscaArtVenta', array('js_comp'=>'#ProdFactura_idArtVenta','is_modal' => true,'ajaxMode' => false));
?>
<?php $this->widget('PrintWidget');

//ComponentesComunes::print_array(array_merge(array(''=>new Camionero()),Camionero::model()->findAll()));
?>


