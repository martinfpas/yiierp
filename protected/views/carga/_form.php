<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'carga-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCarga"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCarga">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

			<div class="span3">
                <?php echo $form->labelEx($model,'idVehiculo'); ?>
                <?php echo $form->dropDownList($model,'idVehiculo',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array('empty' => '--Seleccione uno ---' )); ?>
            </div>
			<div class="span9">
                <?php echo $form->textAreaRow($model,'observacion',array('rows'=>3, 'cols'=>50, 'class'=>'span12')); ?>
            </div>
		</div>
</div>

<div class="form-actions">
	<?php

    if (!$model->isNewRecord){
        echo CHtml::link('Imprimir',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn','id' => 'imprimir','style' => 'float:left; margin-right:5px;', 'target' => '_blank;' ));
    }
    $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
