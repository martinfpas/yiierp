<?php
/* @var $this DetalleComprobanteProvController */
/* @var $model DetalleComprobanteProv */

$this->breadcrumbs=array(
	DetalleComprobanteProv::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.DetalleComprobanteProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=DetalleComprobanteProv::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>