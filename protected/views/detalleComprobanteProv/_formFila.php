
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoDetalleComprobanteProv" id="masDetalleComprobanteProv" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoDetalleComprobanteProv" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'detalle-factura-prov-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('DetalleComprobanteProv/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'detalle-factura-prov-grid',
			'idMas' => 'masDetalleComprobanteProv',
			'idDivNuevo' => 'nuevoDetalleComprobanteProv',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorDetalleComprobanteProv"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okDetalleComprobanteProv">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idArticulo',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'cantidadRecibida',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nroItem',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'alicuota',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idMateriaPrima',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idComprobanteProveedor',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'detalle-factura-prov-grid',
'dataProvider'=>$aDetalleComprobanteProv->search(),
'columns'=>array(
		'id',
		'idArticulo',
		'descripcion',
		'precioUnitario',
		'cantidadRecibida',
		'nroItem',
		/*
		'alicuota',
		'idMateriaPrima',
		'idComprobanteProveedor',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("DetalleComprobanteProv/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("DetalleComprobanteProv/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>