<?php
$this->breadcrumbs=array(
	DetalleComprobanteProv::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.DetalleComprobanteProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.DetalleComprobanteProv::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.DetalleComprobanteProv::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.DetalleComprobanteProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=DetalleComprobanteProv::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idArticulo',
		'descripcion',
		'precioUnitario',
		'cantidadRecibida',
		'nroItem',
		'alicuota',
		'idMateriaPrima',
		'idComprobanteProveedor',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
