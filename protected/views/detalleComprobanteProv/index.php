
/* @var $this DetalleComprobanteProvController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'DetalleComprobanteProv::model()->getAttributeLabel("models")',
);

$this->menu=array(
	array('label'=>'Nuevo DetalleComprobanteProv', 'url'=>array('create')),
	array('label'=>'Gestion de DetalleComprobanteProv', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>