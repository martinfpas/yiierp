<?php
$this->breadcrumbs=array(
	DetalleComprobanteProv::model()->getAttributeLabel('models') => array('admin'),
	DetalleComprobanteProv::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.DetalleComprobanteProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.DetalleComprobanteProv::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.DetalleComprobanteProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=DetalleComprobanteProv::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

