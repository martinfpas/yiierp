<?php
/* @var $this PuntoDeVentaController */
/* @var $model PuntoDeVenta */

$this->breadcrumbs=array(
	'Punto De Ventas'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo PuntoDeVenta', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#punto-de-venta-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Punto De Ventas</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'punto-de-venta-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'num',
		'descripcion',

			array(
				'name' => 'activo',
				'value' => 'PuntoDeVenta::$aEstado[$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => PuntoDeVenta::$aEstado,
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
