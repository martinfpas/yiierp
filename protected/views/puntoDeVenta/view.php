<?php
$this->breadcrumbs=array(
	'Punto De Ventas'=>array('admin'),
	$model->num,
);

$this->menu=array(
	array('label'=>'Nuevo PuntoDeVenta', 'url'=>array('create')),
	array('label'=>'Modificar PuntoDeVenta', 'url'=>array('update', 'id'=>$model->num)),
	array('label'=>'Borrar PuntoDeVenta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->num),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de PuntoDeVenta', 'url'=>array('admin')),
);
?>

<h1>Ver PuntoDeVenta #<?php echo $model->num; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'num',
		'descripcion',

			array(
				'name' => 'activo',
				'header' => 'Estado', 
				'value' => PuntoDeVenta::$aEstado[$model->activo],
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
