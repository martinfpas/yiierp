<?php
$this->breadcrumbs=array(
	'Punto De Ventas'=>array('admin'),
	$model->num=>array('view','id'=>$model->num),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo PuntoDeVenta', 'url'=>array('create')),
	array('label'=>'Ver PuntoDeVenta', 'url'=>array('view', 'id'=>$model->num)),
	array('label'=>'Gestion de PuntoDeVenta', 'url'=>array('admin')),
);
?>

	<h1>Modificando PuntoDeVenta <?php echo $model->num; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>