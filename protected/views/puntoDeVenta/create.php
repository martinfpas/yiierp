<?php
/* @var $this PuntoDeVentaController */
/* @var $model PuntoDeVenta */

$this->breadcrumbs=array(
	'Punto De Ventas'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM PuntoDeVenta', 'url'=>array('admin')),
);
?>

<h1>Nuevo PuntoDeVenta</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>