
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoPuntoDeVenta" id="masPuntoDeVenta" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoPuntoDeVenta" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'punto-de-venta-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('PuntoDeVenta/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'punto-de-venta-grid',
			'idMas' => 'masPuntoDeVenta',
			'idDivNuevo' => 'nuevoPuntoDeVenta',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorPuntoDeVenta"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okPuntoDeVenta">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'num',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
			<div class="span3">
			<?php 
				if(!$model->isNewRecord){
					echo $form->labelEx($model,'activo'); 
	echo $form->dropDownList($model,'activo', array('0' => PuntoDeVenta::inactivo,'1' => PuntoDeVenta::activo),	array( 'class'=>'span3')); 
				}
				?> 		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'punto-de-venta-grid',
'dataProvider'=>$aPuntoDeVenta->search(),
'filter'=>$apunto-de-venta,
'columns'=>array(
		'num',
		'descripcion',

			array(
				'name' => 'activo',
				'value' => 'PuntoDeVenta::$aEstado[$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => PuntoDeVenta::$aEstado,
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("PuntoDeVenta/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("PuntoDeVenta/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>