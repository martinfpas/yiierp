
/* @var $this PuntoDeVentaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Punto De Ventas',
);

$this->menu=array(
	array('label'=>'Nuevo PuntoDeVenta', 'url'=>array('create')),
	array('label'=>'ABM PuntoDeVenta', 'url'=>array('admin')),
);
?>

<h1>Punto De Ventas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
