<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'num',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>40)); ?>

		<?php echo $form->labelEx($model,'activo'); ?>
<?php echo $form->dropDownList($model,'activo', array('0' => PuntoDeVenta::inactivo,'1' => PuntoDeVenta::activo),	array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ?> 
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
