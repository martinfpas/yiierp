<?php
/* @var $this VehiculoController */
/* @var $data Vehiculo */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_vehiculo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_vehiculo),array('view','id'=>$data->id_vehiculo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroDocumento')); ?>:</b>
	<?php echo CHtml::encode($data->nroDocumento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marca')); ?>:</b>
	<?php echo CHtml::encode($data->marca); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modelo')); ?>:</b>
	<?php echo CHtml::encode($data->modelo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('patente')); ?>:</b>
	<?php echo CHtml::encode($data->patente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tara')); ?>:</b>
	<?php echo CHtml::encode($data->tara); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('capMaxima')); ?>:</b>
	<?php echo CHtml::encode($data->capMaxima); ?>
	<br />


</div>