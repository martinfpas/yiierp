<?php
/* @var $this VehiculoController */
/* @var $model Vehiculo */

$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Vehiculo', 'url'=>array('admin')),
);
?>

<h1>Creando Vehiculo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>