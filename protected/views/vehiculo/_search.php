<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id_vehiculo',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'nroDocumento',array('class'=>'span12','maxlength'=>11)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'marca',array('class'=>'span12','maxlength'=>40)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'modelo',array('class'=>'span12','maxlength'=>40)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'patente',array('class'=>'span12','maxlength'=>8)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'tara',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'capMaxima',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
