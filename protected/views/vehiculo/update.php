<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('index'),
	$model->id_vehiculo=>array('view','id'=>$model->id_vehiculo),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Vehiculo', 'url'=>array('create')),
	array('label'=>'Ver Vehiculo', 'url'=>array('view', 'id'=>$model->id_vehiculo)),
	array('label'=>'Gestion de Vehiculo', 'url'=>array('admin')),
);
?>

	<h1>Modificando Vehiculo <?php echo $model->id_vehiculo; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>