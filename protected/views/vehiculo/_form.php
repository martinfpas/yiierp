<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'vehiculo-form',
	'enableAjaxValidation'=>false,
)); 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorVehiculo"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okVehiculo">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'id_vehiculo',array('class'=>'span12','tabindex'=>'1')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroDocumento',array('class'=>'span12','maxlength'=>11,'tabindex'=>'2')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'marca',array('class'=>'span12','maxlength'=>40,'tabindex'=>'3')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'modelo',array('class'=>'span12','maxlength'=>40,'tabindex'=>'4')); ?>
		</div>
	</div>	
	<div class="row-fluid">	
		<div class="span3">
			<?php echo $form->textFieldRow($model,'patente',array('class'=>'span12','maxlength'=>8,'tabindex'=>'5')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'tara',array('class'=>'span12','tabindex'=>'6')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'capMaxima',array('class'=>'span12','tabindex'=>'7')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
