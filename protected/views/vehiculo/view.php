<?php
$this->breadcrumbs=array(
	'Vehiculos'=>array('admin'),
	$model->id_vehiculo,
);

$this->menu=array(
	array('label'=>'Nuevo Vehiculo', 'url'=>array('create')),
	array('label'=>'Modificar Vehiculo', 'url'=>array('update', 'id'=>$model->id_vehiculo)),
	array('label'=>'Borrar Vehiculo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_vehiculo),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Vehiculo', 'url'=>array('admin')),
);
?>

<h1>Ver Vehiculo #<?php echo $model->id_vehiculo; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_vehiculo',
		'nroDocumento',
		'marca',
		'modelo',
		'patente',
		'tara',
		'capMaxima',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
