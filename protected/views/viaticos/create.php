<?php
/* @var $this ViaticosController */
/* @var $model Viaticos */

$this->breadcrumbs=array(
	'Viaticoses'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Viaticos', 'url'=>array('admin')),
);
?>

<h1>Creando Viaticos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>