<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'viaticos-form',
	'enableAjaxValidation'=>false,
));

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorViaticos"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okViaticos">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'efectivo',array('class'=>'span12','tabindex'=>1)); ?>
		</div>
		<div class="span2">
            <?php

            echo $form->labelEx($model,'fecha',array('style'=>'display:inline;'));
            $this->widget('CMaskedTextField', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'Viaticos[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'mask' => '99-99-9999',
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Ingresar los numeros sin los guiones  ',
                    'tabindex' => 2
                ),
            ));
            echo $form->error($model,'fecha');
            ?>
		</div>

		<div class="span3">
			<?php echo $form->textFieldRow($model,'bonos',array('class'=>'span12','tabindex'=>3)); ?>
		</div>
		<div class="span4">
            <?php
                echo $form->labelEx($model,'numeroViajante',array());
                echo $form->dropDownList($model,'numeroViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','tabindex'=>4));
            ?>
		</div>
    </div>
    <div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'tickets',array('class'=>'span12','tabindex'=>5)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cheques',array('class'=>'span12','tabindex'=>6)); ?>
		</div>
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                ),
            )); ?>
        </div>
		
	</div>
</div>

<?php $this->endWidget(); ?>
