<?php
/* @var $this ViaticosController */
/* @var $data Viaticos */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('efectivo')); ?>:</b>
	<?php echo CHtml::encode($data->efectivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNumeroViatico')); ?>:</b>
	<?php echo CHtml::encode($data->idNumeroViatico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bonos')); ?>:</b>
	<?php echo CHtml::encode($data->bonos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroViajante')); ?>:</b>
	<?php echo CHtml::encode($data->numeroViajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tickets')); ?>:</b>
	<?php echo CHtml::encode($data->tickets); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cheques')); ?>:</b>
	<?php echo CHtml::encode($data->cheques); ?>
	<br />

	*/ ?>

</div>