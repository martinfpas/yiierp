<?php
/** @var $model Viaticos */
$this->breadcrumbs=array(
	'Viaticoses'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Viaticos', 'url'=>array('create')),
	array('label'=>'Modificar Viaticos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Viaticos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Viaticos', 'url'=>array('admin')),
);
?>

<h1>Ver Viaticos #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
        array(
            'name' => 'efectivo',
			'value' => "$".number_format($model->efectivo,2),
			'htmlOptions' => array('style' => ''),
		),
        array(
            'name' => 'fecha',
			'value' => ComponentesComunes::fechaFormateada($model->fecha),
			'htmlOptions' => array('style' => ''),
		),

		//'idNumeroViatico',
		'bonos',
		'numeroViajante',
		'tickets',
		'cheques',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
