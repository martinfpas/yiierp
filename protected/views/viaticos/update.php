<?php
$this->breadcrumbs=array(
	'Viaticoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Viaticos', 'url'=>array('create')),
	array('label'=>'Ver Viaticos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Viaticos', 'url'=>array('admin')),
);
?>

	<h1>Modificando Viaticos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>