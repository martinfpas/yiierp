
/* @var $this ViaticosController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Viaticoses',
);

$this->menu=array(
	array('label'=>'Nuevo Viaticos', 'url'=>array('create')),
	array('label'=>'Gestion de Viaticos', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>