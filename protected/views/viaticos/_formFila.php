
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoViaticos" id="masViaticos" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoViaticos" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'viaticos-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('Viaticos/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'viaticos-grid',
			'idMas' => 'masViaticos',
			'idDivNuevo' => 'nuevoViaticos',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorViaticos"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okViaticos">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'efectivo',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idNumeroViatico',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'bonos',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'numeroViajante',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'tickets',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'cheques',array('class'=>'span12')); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'viaticos-grid',
'dataProvider'=>$aViaticos->search(),
'columns'=>array(
		'id',
		'efectivo',
		'fecha',
		'idNumeroViatico',
		'bonos',
		'numeroViajante',
		/*
		'tickets',
		'cheques',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("Viaticos/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Viaticos/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>