<?php
$this->breadcrumbs=array(
	'Carga Documentoses'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo CargaDocumentos', 'url'=>array('create')),
	array('label'=>'Modificar CargaDocumentos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar CargaDocumentos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM CargaDocumentos', 'url'=>array('admin')),
);
?>

<h1>Ver CargaDocumentos #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idEntregaDoc',
		'fechaCarga',
		'tipoDoc',
		'idComprobante',
        'razonSocial',
		'totalFinal',
		'pagado',
		'efectivo',
		'cheque',
		'devolucion',
		'orden',
		'notaCredito',
		'origen',
		'comision',
		/*
		 * Nro_Comprobante
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
