<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="row-fluid">
        <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
		<?php echo $form->textFieldRow($model,'idEntregaDoc',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
		<?php echo $form->textFieldRow($model,'fechaCarga',array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->textFieldRow($model,'tipoDoc',array('class'=>'span12','maxlength'=>4)); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'idComprobante',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'totalFinal',array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->textFieldRow($model,'pagado',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'efectivo',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'cheque',array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
                <?php echo $form->textFieldRow($model,'devolucion',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'orden',array('class'=>'span12')); ?>
        </div>
        <div class="span4">
                <?php echo $form->textFieldRow($model,'notaCredito',array('class'=>'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
		<?php echo $form->textFieldRow($model,'origen',array('class'=>'span12','maxlength'=>8)); ?>
        </div>
        <div class="span4">
         <?php echo $form->textFieldRow($model,'comision',array('class'=>'span12')); ?>
        </div>
        <div class="span4" style="padding-top: 24px">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	    </div>
    </div>
<?php $this->endWidget(); ?>
