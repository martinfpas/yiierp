<?php
/* @var $this CargaDocumentosController */
/* @var $model CargaDocumentos */

$this->breadcrumbs=array(
	'Carga Documentos'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo CargaDocumentos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#carga-documentos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Carga Documentos</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'carga-documentos-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'idEntregaDoc',
		'fechaCarga',
		array(
		    'name' => 'razonSocial',
            'value' => '$data->getRazonSocial()',
        ),

		'tipoDoc',
		'idComprobante',
		'totalFinal',
		/*
		'pagado',
		'efectivo',
		'cheque',
		'devolucion',
		'orden',
		'notaCredito',
		'origen',
		'comision',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
