<?php
$this->breadcrumbs=array(
	'Carga Documentoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo CargaDocumentos', 'url'=>array('create')),
	array('label'=>'Ver CargaDocumentos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM CargaDocumentos', 'url'=>array('admin')),
);
?>

	<h1>Modificando CargaDocumentos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>