<?php
/* @var $this CargaDocumentosController */
/* @var $data CargaDocumentos */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idEntregaDoc')); ?>:</b>
	<?php echo CHtml::encode($data->idEntregaDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaCarga')); ?>:</b>
	<?php echo CHtml::encode($data->fechaCarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoDoc')); ?>:</b>
	<?php echo CHtml::encode($data->tipoDoc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalFinal')); ?>:</b>
	<?php echo CHtml::encode($data->totalFinal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pagado')); ?>:</b>
	<?php echo CHtml::encode($data->pagado); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('efectivo')); ?>:</b>
	<?php echo CHtml::encode($data->efectivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cheque')); ?>:</b>
	<?php echo CHtml::encode($data->cheque); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('devolucion')); ?>:</b>
	<?php echo CHtml::encode($data->devolucion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('orden')); ?>:</b>
	<?php echo CHtml::encode($data->orden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notaCredito')); ?>:</b>
	<?php echo CHtml::encode($data->notaCredito); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen')); ?>:</b>
	<?php echo CHtml::encode($data->origen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comision')); ?>:</b>
	<?php echo CHtml::encode($data->comision); ?>
	<br />

	*/ ?>

</div>