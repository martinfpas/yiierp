<?php
/* @var $this CargaDocumentosController */
/* @var $model CargaDocumentos */

$this->breadcrumbs=array(
	'Carga Documentoses'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM CargaDocumentos', 'url'=>array('admin')),
);
?>

<h1>Creando CargaDocumentos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>