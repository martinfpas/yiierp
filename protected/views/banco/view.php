<?php
$this->breadcrumbs=array(
	'Bancos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Banco', 'url'=>array('create')),
	array('label'=>'Modificar Banco', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Banco', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Banco', 'url'=>array('admin')),
);
?>

<h1>Ver Banco #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
