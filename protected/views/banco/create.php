<?php
/* @var $this BancoController */
/* @var $model Banco */

$this->breadcrumbs=array(
	'Bancos'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Banco', 'url'=>array('admin')),
);
?>

<h1>Nuevo Banco</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>