<?php
$this->breadcrumbs=array(
	'Udms'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Udm', 'url'=>array('create')),
	array('label'=>'Ver Udm', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Udm', 'url'=>array('admin')),
);
?>

	<h1>Modificando Udm <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>