<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'udm-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorUdm"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okUdm">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'id',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'unidad',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'kilosXudm',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
