
/* @var $this UdmController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Udms',
);

$this->menu=array(
	array('label'=>'Nuevo Udm', 'url'=>array('create')),
	array('label'=>'Gestion de Udm', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>