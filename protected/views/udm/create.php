<?php
/* @var $this UdmController */
/* @var $model Udm */

$this->breadcrumbs=array(
	'Udms'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Udm', 'url'=>array('admin')),
);
?>

<h1>Creando Udm</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>