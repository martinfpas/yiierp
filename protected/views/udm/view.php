<?php
$this->breadcrumbs=array(
	'Udms'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Udm', 'url'=>array('create')),
	array('label'=>'Modificar Udm', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Udm', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Udm', 'url'=>array('admin')),
);
?>

<h1>Ver Udm #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'unidad',
		'kilosXudm',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
