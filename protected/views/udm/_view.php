<?php
/* @var $this UdmController */
/* @var $data Udm */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unidad')); ?>:</b>
	<?php echo CHtml::encode($data->unidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kilosXudm')); ?>:</b>
	<?php echo CHtml::encode($data->kilosXudm); ?>
	<br />


</div>