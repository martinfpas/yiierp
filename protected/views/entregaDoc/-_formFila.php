<?php
    /* @var $aCargaDocumentos CargaDocumentos*/
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
    $cs->registerScriptFile($baseUrl."/js/print-button.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");

?>

<?php echo CHtml::link('Imprimir Lista de Documentos',Yii::app()->createUrl('entregaDoc/viewPdf', array('id'=>$entregaDoc->id)),
    array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;margin-right:5px;', 'target' => '_blank;',
    'print' => Yii::app()->createUrl('entregaDoc/viewPdf', array('id'=>$entregaDoc->id,'print'=>true)),
    ));
?>

<?php
    if ($entregaDoc->oCarga != null && $entregaDoc->oCarga->estado == Carga::iFacturada){
        echo CHtml::link('Carga Despachada', Yii::app()->createUrl('Carga/Despachada',array('id'=>$entregaDoc->oCarga->id)), array('class' => 'bulk-button btn', 'style' => 'float:left;margin-right: 20px;'));
    }else{
        echo CHtml::link('Rendir',Yii::app()->createUrl('entregaDoc/rendir', array('id'=>$entregaDoc->id)),array('class'=>'btn btn-primary','id' => 'rendir','style' => 'float:left;margin-right:5px;' ));
    }
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'carga-documentos-grid',
'dataProvider'=>$aCargaDocumentos->searchWP(),
'enableSorting' => false,
'columns'=>array(
        array(
            'name' => 'idComprobante',
            'header' => 'idComp',
            'headerHtmlOptions' => array('style' => 'font-size: x-small;'),
			'htmlOptions' => array('style' => 'font-size: x-small;'),
		),
        'tipoDoc',
        /*
         array(
            'name' => 'fechaCarga',
			'value' => 'ComponentesComunes::fechaFormateada($data->fechaCarga)',
			'htmlOptions' => array('style' => ''),
		),
        */
        array(
            'name' => 'Nro',
            //'value' => '($data->oComprobante != null)? $data->oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante',
            'value' => '($data->tipoDoc != CargaDocumentos::NE)? $data->oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante', //(($data->oNotaEntrega != null)? "$data->oNotaEntrega->nroComprobante" : $data->idComprobante)',
            //'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'Clase',
			'value' => '($data->oComprobante != null)? $data->oComprobante->clase : $data->oNotaEntrega->clase',
			//'htmlOptions' => array('style' => ''),
		),


        array(
            'name' => 'Pto.Vta',
            'value' => '($data->oComprobante != null)? $data->oComprobante->Nro_Puesto_Venta : $data->oNotaEntrega->nroPuestoVenta',
            //'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'totalFinal',
			'value' => '"$".number_format($data->totalFinal,"2",",","")',
			'htmlOptions' => array('style' => 'text-align:right;'),
            'footer' => "<h4>Total $".number_format($entregaDoc->total,2,',','.')."</h4>",
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
		),
        'orden',

        array(
            'name' => 'razonSocial',
			'value' => '$data->Cliente->razonSocial',
			'htmlOptions' => array('style' => ''),
		),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
				'buttons'=>array(
					'delete' => array(
	                	'icon' => 'resize-full',
						'url'=>'Yii::app()->controller->createUrl("CargaDocumentos/delete", array("id"=>$data->id))',
                        'label' => 'Desvincular Documento',
                        'visible' => '$data->oEntregaDoc->oCarga != null && ($data->oEntregaDoc->oCarga->estado < Carga::iDespachada)'
					),
				),
		),
	),
)); ?>
<?php $this->widget('PrintWidget') ?>
