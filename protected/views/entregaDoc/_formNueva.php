<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'entrega-doc-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorEntregaDoc"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okEntregaDoc">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php
                echo $form->labelEx($model,'tipoPersona');
                echo $form->dropDownList($model,'tipoPersona',EntregaDoc::$aTipoPersona,array(
                        'empty' => 'Seleccionar',
                        'ajax' => array(
                            'type'=>'POST', //request type
                            'url'=>CController::createUrl('ListarCamionesViajantes'), //url to call.
                            'update'=>'#EntregaDoc_nroCamViaj', //selector to update
                        ),
                ));

                //ListarCamionesViajantes
            ?>
		</div>
		<div class="span3">
			<?php
                echo $form->dropDownList($model,'nroCamViaj',CHtml::listData(array(),'id','nombre'),array('empty' => 'Seleccionar','style'=>'margin-top:25px;'));
            ?>

		</div>

	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
