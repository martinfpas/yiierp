<?php
/* @var $this EntregaDocController */
/* @var $model EntregaDoc */

$this->breadcrumbs=array(
    'Documentos a Entregar'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM EntregaDoc', 'url'=>array('admin')),
);
?>

<h1>Creando Documentos a Entregar</h1>

<?php echo $this->renderPartial('_formNueva', array('model'=>$model)); ?>