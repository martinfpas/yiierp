<style>
    div#documentos {
        font-size: smaller;
    }
    a.btn.btn-small.verPago {
        color: #4747ad;
    }
    a.btn.btn-small.nuevoPago {
        color: #bb4d4d;
    }
    a.btn.btn-small.verMultipago {
        color: #00BB00;
    }

    table.items{
        margin-bottom: 0px;
    }
    .disableCheck{
        pointer-events: none;
        background: rgba(40,40,40,0.2);
        color:black;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        border: none;
        position: relative;
    }
</style>
<?php
	$baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl."/js/entregaDocRendir.js");
    $cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );

?>
<script>
    var rowsCargaDocs;
    var urlRendirPartial = '<?=Yii::app()->createUrl('EntregaDoc/RendirPartial')?>';
    var entregaDocId = '<?=$oEntregaDoc->id?>';
    $(document).ready(function(){
/*
        try{
            shortcut.add('ctrl+m', function() {
                console.log('se presionó el mas....');
                let seleccionados = $('#carga-documentos-grid').selGridView('getAllSelection');
                if(seleccionados.length <= 1){
                    console.log(seleccionados.length);
                    alert('Debe seleccionar dos o mas comprobantes');
                    return false;
                }else {
                    $('#modalPagoClientes').modal();
                }
            });
        }catch(e){
            console.warn(e);
        }
*/
    });
    function multipago(idPagoRel){
        console.log('function multipago()');
        console.log(idPagoRel);
        if(!isNaN(idPagoRel) && (idPagoRel > 0)){
            try{
                let datos = {idPagoRel:idPagoRel,hola:'mundo2'};
                $.ajax({
                    url: '<?=Yii::app()->createUrl('pagoCliente/multipago')?>',
                    data : datos,
                    //TODO: EN SUCCESS ACTUALIZAR LA GRILLA
                    success: function(html){
                    
                    $('#modalPagoClientes').find('.modal-body').html(html);
                    console.log($(html).find('[rel="CargaDocumentos_Recibida"]'));
                    rowsCargaDocs = [];
                    $(html).find('[rel="CargaDocumentos_Recibida"]').each(function(key,element){
                        let pk = $(element).attr('data-pk');
                        let row = $('[name=\"carga-documentos-grid_c1[]\"]').filter('[data-pk="'+pk+'"]')
                        rowsCargaDocs.push($(this).parent().parent());
                    });
                },
                error: function(x,y,z){
                    // TODO: MANEJAR EL ERROR

                },

            });
            }catch(error){
                console.error(error);
            }
        }else{
            try{
                //let seleccionados = $('#carga-documentos-grid').selGridView('getAllSelection');
                let seleccionados = $.fn.yiiGridView.getSelection('carga-documentos-grid');
                if(seleccionados.length <= 1){
                    console.log(seleccionados.length);
                    return false;
                }
                let datos = {CargaDocumentos:seleccionados,hola:'mundo'};
                $.ajax({
                    url: '<?=Yii::app()->createUrl('pagoCliente/multipago')?>',
                    data : datos,
                    //TODO: EN SUCCESS ACTUALIZAR LA GRILLA
                    success: function(html){
                    //Sean Callery
                    $('#modalPagoClientes').find('.modal-body').html(html);
                    //$.fn.modal.Constructor.prototype.enforceFocus = function() {};
                    //$('#modalPagoClientes').modal();
                    //bindHandlers();
                },
                error: function(x,y,z){
                    // TODO: MANEJAR EL ERROR

                },

            });
            }catch(error){
                console.error(error);
            }
        }
    }

	function verPago(idPago) {
	    //console.log('verPago');
        $('#idPagoWid').val(idPago);
        $('#modalPagoClientes').modal();
    }
    function nuevoPago(idCargaDoc) {
        //console.log('nuevoPago');
	    $('#idCargaDocWid').val(idCargaDoc);
        $('#idPagoWid').val('');
        $('#modalPagoClientes').modal();
    }


    function verMultipago(idPago) {
        //console.log('verPago');
        multipago(idPago);
        $('#modalPagoClientes').modal();
        return false;
    }

    /****** CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
    function moveDown() {
        var rows = $('#prod-carga-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).next('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).next('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }

    function moveUp() {
        var rows = $('#prod-carga-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).prev('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).prev('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }


</script>

<input type="hidden" id="PagoCLienteIdModal" />

<?php
    /* @var CargaDocumentos $aCargaDocumentos*/
    /* @var EntregaDoc $oEntregaDoc */

Yii::app()->clientScript->registerScript('PagoCLienteIdModalUpdate2',"
        $(document).ready(function(){
            
            $('#multipago').click(function(){
                console.log('#....multipago');
                //let seleccionados = $('#carga-documentos-grid').selGridView('getAllSelection');
                let seleccionados = $.fn.yiiGridView.getSelection('carga-documentos-grid');
                if(seleccionados.length <= 1){
                    console.log(seleccionados.length);
                    alert('Debe seleccionar dos o mas comprobantes');
                    return false;
                }
                $('#modalPagoClientes').modal();                
            });
            $('#PagoCLienteIdModal').change(function(){            
                try{
                    $(rowsCargaDocs).each(function(key,rowCargaDoc){

                        let idCargaDoc = $(rowCargaDoc).find('[name=\"carga-documentos-grid_c1[]\"]').val();
                        console.log(idCargaDoc);
                        updateRow(rowCargaDoc,'".Yii::app()->createUrl('EntregaDoc/RendirPartial')."');

                    });
                    

                    //$.fn.yiiGridView.update('carga-documentos-grid');
                    $('#modalPagoClientes .modal-body   ').html('');
                }catch(e){
                    location.reload(true);
                    $('#carga-documentos-grid').removeClass('grid-view-loading');
                    console.log(e);
                };
            });
            try{
                /*
                shortcut.add('Down', function() {
                    moveDown();
                });
        
                shortcut.add('Up', function() {
                    moveUp();
                });
                */
            }catch(e){
                console.warn(e);
            }
            
        
        checkHandlers();    
    });
    
    /****** CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
    function moveDown() {
        try{
            var rows = $('#carga-documentos-grid table tr');
            var currentRow = $(\"tr.selected\").get(0);
    
            if (rows.length > 2) {
                if (currentRow === undefined) {
                    rows.eq(1).addClass('selected');
                } else if ($(currentRow).next('tr').get(0) === undefined) {
                    //do nothing
                } else {
                    $(currentRow).next('tr').addClass('selected');
                    $(currentRow).removeClass('selected');
                }
            }
        }catch(e){
            console.log(e);
        }        
    }

    function moveUp() {
        try{
            var rows = $('#carga-documentos-grid table tr');
            var currentRow = $(\"tr.selected\").get(0);
    
            if (rows.length > 2) {
                if (currentRow === undefined) {
                    rows.eq(1).addClass('selected');
                } else if ($(currentRow).prev('tr').get(0) === undefined) {
                    //do nothing
                } else {
                    $(currentRow).prev('tr').addClass('selected');
                    $(currentRow).removeClass('selected');
                }
            }   
        }catch(e){
            console.log(e);
        }
    }    

    /****** fin CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
    
    function checkHandlers(){
        $('.disableCheck').click(function(){
            return false;
        });

    
        /*
        // SI SE MARCÓ O DESMARCÓ TODOS LOS CHECKS
        $('#carga-documentos-grid_c0').change(function(){
            console.log('toggleBotonMultiPago()');
            toggleBotonMultiPago();
            return true;
        });
        
        // SI CAMBIÓ AL MENOS UN CHECK
        console.log('CHECK handler');
        $('[name=\"carga-documentos-grid_c1[]\"]').change(function(){
            console.log('carga-documentos-grid_c1[]');
            //SI TIENE PAGO RELACIONADO BUSCA LOS DOCUMENTOS DEL MISMO PAGO 
            if($(this).parent().is(\"[class*='idPagoRel_']\")){
                let checkCss = '.'+$(this).parent().attr('class').replace(' ','.');
                var checked = $(this).is(':checked');                
                $(checkCss).each(function(e){
                    // MARCA O DESMARCA EL RESTO DE LOS DOCUMENTOS DEL MISMO PAGO RELACIONADO
                    $(this).find('input').attr('checked',checked);
                    // PINTA O DESPINTA LA FILA, NO SACAR
                    if(checked){
                        $(this).closest('tr').addClass('selected');
                    }else{
                        $(this).closest('tr').removeClass('selected');
                    }
                });
            }
            
            toggleBotonMultiPago();
            return true;        
        });    
        */
    }
    
",CClientScript::POS_READY);



echo CHtml::button('Pago Multiple',array('class' => 'btn btn-primary','id' => 'multipago','style' => 'margin-bottom:5px;'));
//$this->widget('bootstrap.widgets.TbGridView',array(
$this->widget('ext.selgridview.BootSelGridView',array(

    'id'=>'carga-documentos-grid',
    'dataProvider'=>$aCargaDocumentos->searchWP(),
    'template' => '{items}',
    'selectableRows' => 2,
    'enableSorting' => false,
    'afterAjaxUpdate'=>'js:function(id, data){console.log("updateTotatlARendir");updateTotatlARendir();checkHandlers();return true;}',
    'htmlOptions' => array(
        'style' => 'padding-top: 0px;'
    ),
'columns'=>array(
        'orden',
        array(
            'class' => 'CCheckBoxColumn',
            'cssClassExpression' => '(($data->oPagoCliente != null)? "disableCheck" : "")',
            'visible' => ($oEntregaDoc->oCarga->estado <> Carga::iCerrada),
        ),
        array(
			'name' => 'tipoDoc',
			'header' => 'Tipo',
		),
        array(
            'name' => 'Nro',
            //'value' => '($data->$oComprobante != null)? $data->$oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante',
            'value' => '($data->tipoDoc != CargaDocumentos::NE)? $data->oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante',
            //'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'Clase',
			'value' => '($data->tipoDoc != CargaDocumentos::NE)? $data->oComprobante->clase : ""',
			//'htmlOptions' => array('style' => ''),
		),

        array(
            'name' => 'razonSocial',
            'value' => '$data->getRazonSocial()',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'Recibida',
            'header' => 'Ent.',
            'value' => '($data->tipoDoc != CargaDocumentos::NE)? CargaDocumentos::$aSiNo[$data->oComprobante->Recibida] : CargaDocumentos::$aSiNo[$data->oNotaEntrega->recibida]',
            //'htmlOptions' => array('style' => ''),
            'editable' => array(    //editable section
                //'apply'      => '($data->tipoDoc == CargaDocumentos::NC || $data->tipoDoc == CargaDocumentos::ND || $data->totalFinal == 0) && $data->esEditable() ',
                'apply'      => '$data->esEditable()',
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'type'   => 'select',
                'source' => Editable::source(CargaDocumentos::$aSiNo),
                'onInit' => 'js: function(ev,editable) {CargaDocumentos_Recibida_init(ev,editable)}',
            )
        ),

        array(
            'name' => 'pagado',
            'value' => 'CargaDocumentos::$aSiNo[$data->pagado]',
            //'htmlOptions' => array('style' => ''),
        ),

        array(

            'name' => 'totalFinal',
			'value' => '"$".number_format($data->totalFinal,"2",".","")',
			'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
            'footer' => 'Total Cobrado: <span id="totalCobrado">$'.number_format(($oEntregaDoc->getTotalEfectivo() + $oEntregaDoc->TotalCheques),2,'.','').'</span>',
            'footerHtmlOptions' => array(
                'colspan' => 2,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )
		),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'devolucion',
            'value' => '"$".number_format($data->devolucion,"2",".","")',
            'editable' => array(    //editable section
                'apply'      => '$data->pagado != 1 && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC',
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'placement'  => 'right',
                'display' => 'js: function(value, editable){CargaDocumentos_devolucion_display(value, editable)}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {CargaDocumentos_devolucion_shown(ev,editable)}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {CargaDocumentos_devolucion_success(response, newValue)}',
                // INICIALIZA LAS OPCIONES 
                'onInit' => 'js: function(ev,editable) {CargaDocumentos_devolucion_init(ev,editable)}',
            ),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),

        array(
            'name' => 'Monto pagado',
            'value' => '($data->oPagoCliente != null)? "$".number_format($data->oPagoCliente->monto,"2",",","") : "$0.00"',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
        ),


        array(
            'name' => 'MontoPasibleComision',
            'value' => '"$".number_format($data->MontoPasibleComision,"2",".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),

        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{verPago}{nuevoPago}{multipago}',
			'headerHtmlOptions' => array(
				'style' => 'width:90px;text-align:right;',
			),
			'buttons' => array(
			    'multipago' => array(
                    'label'=>'Multipago',
                    'url'=>'$data->Documento->idPagoRelacionado',
                    'click' => "js:function(e){
                           e.stopImmediatePropagation();
						   e.stopPropagation();						   
						   e.preventDefault();						   						   
						   verMultipago($(this).attr('href'));
						   console.log('multipago($(this).attr(href)) '+$(this).attr('href'));
						   //multipago($(this).attr('href'));
                           //$('#modalPagoClientes').modal();
						   return false;
					   }",
                    'options'=>array(
                        'class'=>'btn btn-small verMultipago',
                    ),
                    'visible' => '$data->documento->idPagoRelacionado > 0'
                ),
				'verPago' => array
				(
					'label'=>'Ver Pago',
					'url'=>'($data->oPagoCliente != null)? $data->oPagoCliente->id : "+"',
					'click' => "js:function(e){
						   e.stopPropagation();
						   e.preventDefault();
						   rowsCargaDocs = [$(this).parent().parent()];
						   verPago($(this).attr('href'));
						   return false;
					   }",
					// LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
					'options'=>array(
						'class'=>'btn btn-small verPago',
					),
                    'visible' => '($data->oPagoCliente != null && $data->tipoDoc != CargaDocumentos::NC && $data->tipoDoc != CargaDocumentos::ND && $data->Documento->idPagoRelacionado == "") '
				),
                'nuevoPago' => array
                (
                    'label'=>'Nuevo Pago',
                    'url'=>'$data->id',
                    'click' => "js:function(e){
						   e.stopPropagation();
						   e.preventDefault();
						   rowsCargaDocs = [$(this).parent().parent()];
						   console.log(rowsCargaDocs);
						   nuevoPago($(this).attr('href'));
						   return false;
					   }",
                    // LA CLASE SE PONE PORQUE SINO, DISPARA TODOS LAS FUNCIONES SIN DISTINGUIR EL BOTON
                    'options'=>array(
                        'class'=>'btn btn-small nuevoPago',
                    ),
                    'visible' => '($data->oPagoCliente == null && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC && $data->tipoDoc != CargaDocumentos::ND  && $data->Documento->idPagoRelacionado == "") ',
                ),

			),
            'footer' => 'Total Efectivo: <span id="totalEfectivo">$'.number_format($oEntregaDoc->getTotalEfectivo(),2,'.','').'</span>',
            'footerHtmlOptions' => array(
                'colspan' => 2,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )
		),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'efectivo',
            'value' => '"$".number_format($data->efectivo,"2",".","")',
            'editable' => array(    //editable section
                'apply'      => '$data->pagado != 1 && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC',
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'placement'  => 'right',
                'display' => 'js: function(value, editable){CargaDocumentos_efectivo_display(value, editable)}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {CargaDocumentos_efectivo_shown(ev,editable)}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {CargaDocumentos_efectivo_success(response, newValue)}',
                //'onInit'  => 'js: funtion(ev,editable){CargaDocumentos_efectivo_init(value, editable)}',
                'onInit' => 'js: function(ev,editable) {CargaDocumentos_efectivo_init(ev,editable)}',
            ),
            'htmlOptions' => array('style' => 'width: 70px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footer' => 'Total Cheques: <span id="totalCheques">$'.number_format($oEntregaDoc->TotalCheques,2,'.','').'</span>',
            'footerHtmlOptions' => array(
                'colspan' => 3,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )

        ),
		array(
			//'class' => 'editable.EditableColumn',
			'name' => 'cheque',
			'value' => '"$".number_format($data->cheque,"2",",","")',
			'htmlOptions' => array('style' => 'width: 70px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
		),


        array(
            'class' => 'editable.EditableColumn',
            'name' => 'comision',
            'value' => 'CargaDocumentos::$aSiNo[$data->comision]',
            'editable' => array(
                //'apply'      => '$data->pagado != 1 && $data->esEditable() ',
                'type'      => 'select',
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'source'    => Editable::source(CargaDocumentos::$aComision),
                'onInit' => 'js: function(ev,editable) {CargaDocumentos_comision_init(ev,editable)}',
            ),
            'header' => 'com',
            'htmlOptions' => array('style' => 'width:30px;'),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

	),
)); ?>