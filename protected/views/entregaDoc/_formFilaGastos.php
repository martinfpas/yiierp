<?php
/* @var $model EntregaGtos */
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>

<div class="content-fluid bordeRedondo" >
    <div class="row-fluid">
        <div class="span7">
            <h4>Registro de Gastos</h4>
            <?php
            if ($model->esEditable()){
            ?>
                <div id="nuevoEntregaGtos" style="">

                    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                        'id'=>'entrega-gtos-form',
                        'enableAjaxValidation'=>false,
                        'action' => Yii::app()->createUrl('EntregaGtos/SaveAsync'),
                        'htmlOptions' => array(
                            'class' => 'SaveAsyncForm',
                            'idGrilla'=>'entrega-gtos-grid',
                            'idMas' => 'masEntregaGtos',
                            'idDivNuevo' => 'nuevoEntregaGtos',
                        ),
                    )); ?>

                    <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorEntregaGtos"></div>
                    <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okEntregaGtos">Datos Guardados Correctamente !</div>

                    <?php echo $form->errorSummary($model); ?>
                    <div class="content-fluid">
                        <div class="row-fluid">

                            <?php echo $form->hiddenField($model,'idEntregaDoc',array()); ?>

                            <div class="span5">
                                <?php
                                echo $form->labelEx($model,'gasto');
                                echo $form->dropDownList($model,'gasto',EntregaGtos::$aTipos,array('tabindex' => 1,'empty' => 'Seleccionar'));
                                ?>
                            </div>
                            <div class="span3">
                                <?php echo $form->textFieldRow($model,'montoGasto',array('class'=>'span12','tabindex' => 2)); ?>
                            </div>
                            <div class="span2">
                                <?php $this->widget('bootstrap.widgets.TbButton', array(
                                    'buttonType'=>'submit',
                                    'type'=>'primary',
                                    'label'=> 'Guardar',
                                    'htmlOptions' => array(
                                        'style' => 'margin-top:25px;',
                                    )
                                )); ?>
                            </div>

                        </div>
                    </div>


                    <?php $this->endWidget(); ?>
                </div>
            <?php } ?>
        </div>
        <div class="span5">
            <?php

                $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'entrega-gtos-grid',
                    'template' => '{items}',
                    'afterAjaxUpdate'=>'js:function(id, data){console.log("updateTotatlARendir 2");updateTotatlARendir()}',
                    'dataProvider'=>$aEntregaGastos->search(),
                    'columns'=>array(
                        'gasto',
                        array(
                            'class' => 'editable.EditableColumn',
                            'name' => 'montoGasto',
                            'value' => '"$".number_format($data->montoGasto,"2",".","")',
                            'editable' => array(    //editable section

                                'apply'      => '$data->esEditable()',
                                'url'        => $this->createUrl('EntregaGtos/SaveFieldAsync'),
                                'placement'  => 'right',

                                'display' => 'js: function(value, sourceData) {
                                    value = value.replace("$",""); //
                                    if(!isNaN(value) > 0){
                                        $(this).html("$ "+parseFloat(value).toFixed(2));
                                    }else{
                                        console.log("Value is NaN:"+value);
                                        $(this).html("$ 0.00");
                                    }

							}',

                                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',

                                // ACTUALIZO LA GRILLA
                                'success' => 'js: function(response, newValue) {
                                updateTotatlARendir();
                                $("#entrega-gtos-grid").yiiGridView("update", {
                                    
                                });
                             }',
                            ),
                            'headerHtmlOptions' => array('style' => 'text-align:right;'),
                            'htmlOptions' => array('style' => 'text-align:right;'),
                            'footer' => "$".number_format($oEntregaDoc->getTotalGastos(),2,'.',''),
                            'footerHtmlOptions' => array(
                                'style' => 'font-weight: bold;text-align:right;',
                            )
                        ),


                        array(
                            'class'=>'bootstrap.widgets.TbButtonColumn',
                            'template' => '{delete}',

                            'buttons'=>array(

                                'delete' => array(
                                    'label' => 'Borrar Item',
                                    'url'=>'Yii::app()->controller->createUrl("EntregaGtos/delete", array("id"=>$data->id))',
                                    'visible'=>'$data->esEditable()',
                                ),
                            ),
                        ),
                    ),
                ));

            ?>
        </div>
    </div>
</div>



