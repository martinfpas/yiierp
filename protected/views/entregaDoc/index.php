
/* @var $this EntregaDocController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Entrega Docs',
);

$this->menu=array(
	array('label'=>'Nueva EntregaDoc', 'url'=>array('create')),
	array('label'=>'ABM EntregaDoc', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>