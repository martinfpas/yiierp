<script>
    function updateTotatlARendir(){
        var datos = 'id=<?=$model->id?>&param=TotalARendir';
        $.ajax({
            url: '<?=Yii::app()->createUrl('entregaDoc/GetParamVal')?>',
            data:datos,
            type: 'GET',
            //TODO:TERMINAR
            success:function(html){
                $('#TotalARendir').html(html);
            }
        });
    }
</script>
<style>
.detail-view th {
     width: 100px;
 }
</style>
<?php
/* @var $model EntregaDoc */
$this->breadcrumbs=array(
	'Documentos a Entregar'=>array('PlanillasRendicion'),
	$model->id,
);

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/shortcut.js");


Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
");

Yii::app()->clientScript->registerScript('search', "

     console.log('updateTotatlARendir');
    $('a[href=\"#gastos\"]').click(function() {
        console.log('#gastos a');
        $('#EntregaGtos_gasto').focus();
    });
    
    $(document).ready(function(){
        var code = '';
        updateTotatlARendir();
    });
");


$this->menu=array(
    array('label'=>'Gestion de Doc a Entregar', 'url'=>array('admin')),
);
?>

<h3>Vista Rendicion de Documentos a entregar Carga #<?php echo $model->idCarga; ?></h3>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model->oCarga,
                'attributes'=>array(
                    'observacion',

                ),
            ));
            ?>
        </div>
        <div class="span2">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(

                    'nroCamViaj',

                ),
            ));
            ?>
        </div>
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(


                    array(
                        'name' => 'idCarga',
                        'header' =>  'Nº carga',
                        'type'=>'raw',
                        'value' => ($model->idCarga != null)? CHtml::link('Ir a carga Nº'.$model->idCarga,Yii::app()->createUrl('carga/view',array('id' => $model->idCarga))) : 'No hay cargas asociadas',
                        'htmlOptions' => array('style' => ''),
                    ),
                ),
            ));
            ?>
        </div>
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'oCarga.estado',
                        'value' => Carga::$aEstado[$model->oCarga->estado],
                    )
                ),
            ));
            ?>
        </div>
    </div>
</div>



<?php
    echo CHtml::link('Imprimir Planilla <i class="icon-print"></i>',Yii::app()->createUrl('entregaDoc/PdfRendicion', array('id'=>$model->id)),
            array('class'=>'btn print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;', 'target' => 'blank',
                'print' => Yii::app()->createUrl('entregaDoc/PdfRendicion', array('id'=>$model->id,'print'=>true)))
    );
?>
<?php
    if (($model->idCarga != null) && $model->oCarga->estado == Carga::iDespachada){
        echo CHtml::link('Cerrar Planilla de la carga',Yii::app()->createUrl('carga/Cerrar', array('id'=>$model->idCarga)),array('class'=>'btn btn-primary','id' => 'cerrar','style' => 'float:left;margin-right:5px;' ));
    }
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#documentos">Documentos</a></li>
    <li><a data-toggle="tab" href="#gastos">Gastos Efectuados</a></li>

</ul>




<div class="tab-content">
    <div id="documentos" class="tab-pane fade in active">
        <?php
        $this->renderPartial('_formFilaRendir',array(
            'model'=> $oCargaDocumento,
            'aCargaDocumentos' => $aCargaDocumentos,
            'oEntregaDoc' => $model,
        ));
        ?>
    </div>
    <div id="gastos" class="tab-pane fade">
        <div class="content-fluid">
            <div class="row-fluid">
                <div class="span4">
                    <span>Monto Inicial</span>
                    <?php
                    $this->widget('editable.EditableField', array(
                        'type'      => 'text',
                        'model'     => $model,
                        'attribute' => 'montoInicial',
                        'apply' => $model->esEditable(),
                        'url'       => $this->createUrl('EntregaDoc/SaveFieldAsync'),
                        'display' => 'js: function(value, sourceData) {
                                
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                        // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                        'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                        'onSave' => 'js: function(e, params) {
                            console.log("Saved value: " + params.newValue);
                            updateTotatlARendir();
                        }',
                        'placement' => 'right',
                    ));
                    ?>
                </div>
                <div class="span4">
                    <span>Km Recorridos</span>
                    <?php
                    $this->widget('editable.EditableField', array(
                        'type'      => 'text',
                        'model'     => $model,
                        'attribute' => 'kmRecorridos',
                        'apply' => $model->esEditable(),
                        'url'       => $this->createUrl('EntregaDoc/SaveFieldAsync'),
                        'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(1)+" km");								
								}else{
									$(this).html("0.0 km");							
								}

							}',
                        // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                        'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                        'placement' => 'right',
                    ));
                    ?>
                </div>
                <div class="span4">
                    <span>Listros Cargados</span>
                    <?php
                    $this->widget('editable.EditableField', array(
                        'type'      => 'text',
                        'model'     => $model,
                        'attribute' => 'litrosCargados',
                        'apply' => $model->esEditable(),
                        'url'       => $this->createUrl('EntregaDoc/SaveFieldAsync'),
                        'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(1)+" lts");								
								}else{
									$(this).html("0.0 lts");							
								}

							}',
                        // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                        'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                        'placement' => 'right', 
                    ));
                    ?>
                </div>
            </div>
            <div class="row-fluid">
                <?php
                    $this->renderPartial('_formFilaGastos',array(
                        'model'  => $oEntregaGastos,
                        'aEntregaGastos' => $aEntregaGastos,
                        'oEntregaDoc' => $model,
                    ));
                ?>
            </div>
        </div>



    </div>
</div>
<h3>Total a rendir:$<span id="TotalARendir"><?=number_format($model->TotalARendir,2,'.','');?></span></h3>
<?php
$this->widget('PagoClienteWid', array('js_comp'=>'#PagoCLienteIdModal','is_modal' => true));

?>
<?php $this->widget('PrintWidget') ?>