     <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span3">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span3">
		<?php echo $form->textFieldRow($model,'nroCamViaj',array('class'=>'span12')); ?>
    </div>
    <div class="span3">
		<?php echo $form->textFieldRow($model,'observacion',array('class'=>'span12','maxlength'=>400)); ?>
    </div>
    <div class="span3">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Buscar',
            'htmlOptions' => array('style' => 'margin-top:25px;'),
        )); ?>
    </div>
</div>


<?php $this->endWidget(); ?>
