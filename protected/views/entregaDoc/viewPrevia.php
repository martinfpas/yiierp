<?php
/* @var $aCargaDocumentosCiudades VCargaDocumentosCiudades */
/* @var $aComprobantesLibres VComprobantesSinCargaDoc */
/* @var $model EntregaDoc */
$this->breadcrumbs=array(
    'Documentos a Entregar'=>array('admin'),
	$model->id,
);

$this->menu=array(
    array('label'=>'Ir a la Carga asociada', 'url'=>array('carga/view','id' => $model->oCarga->id)),
    array('label'=>'Gestion de Doc a Entregar', 'url'=>array('admin')),
);


Yii::app()->getClientScript()->registerScript('vincularx',"

    $('#addMultipleDocs').click(function(){
        var seleccionados = $('#docs-libres-grid').selGridView('getAllSelection');

        if(seleccionados.length < 1){
            alert('Seleccione al menos un documento');
            return false;
        };

        $.when.apply($, seleccionados.map((doc) => {
            // REALIZAR LA LLAMADA PARA ASIGNAR LA CARGA
            
            let url = $('#docs-libres-grid .select-on-check[value=\"'+doc+'\"]').parent().parent().find('.vincular').attr('href')
            console.log(url);
                        
            return $.ajax({
                url:url,
                type: 'Get',
                success: function (html) {
                    console.log('doc ok:'+doc);
                },
                error: function (x,y,z){
                    //TODO: MANEJAR EL ERROR
                    
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    console.log($(errorDiv).parent().next());
                    
                }
            });
            
        })).then(function() {
            // RECARGA SOLO CUANDO TERMINA DE ARMAR LA CARGA
            $.fn.yiiGridView.update('carga-documentos-grid');
            $.fn.yiiGridView.update('docs-libres-grid');
            $.fn.yiiGridView.update('ciudades-grid');
        });
        




    });

    function vincular(url){
    
        $.ajax({
            url:url,
            type: 'Get',
            success: function (html) {
                $.fn.yiiGridView.update('carga-documentos-grid');
                $.fn.yiiGridView.update('docs-libres-grid');
                $.fn.yiiGridView.update('ciudades-grid');
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                
            }
        });
    }
",CClientScript::POS_READY);
?>
<style>
    table.detail-view th {
        width: 60px;
    }
    #addMultipleDocs{
        cursor: pointer;
    }
</style>
<h3>Hoja de Ruta :: Carga #<?php echo $model->idCarga; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span3">
            <?php
            $this->widget('editable.EditableDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' =>'nroCamViaj',
                        'label' => 'camion',
                        'editable' => array(
                            'type'   => 'select',
                            'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre'),
                            'mode'      => 'popup',
                            'model'     => $model,
                            'apply'     => ($model->oCarga->estado < Carga::iDespachada), //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                            'url'       => $this->createUrl('EntregaDoc/SaveFieldAsync'),

                        )
                    )
                ),
            ));
            ?>
        </div>
        <div class="span4">
            <table class="detail-view table table-striped table-condensed" id="yw1">
                <tbody>
                <tr class="odd"><th>Camionero:</th><td><?=($model->oCarga->oCamionero != null)? $model->oCarga->oCamionero->Titulo : '-' ?></td></tr>
                </tbody>
            </table>
        </div>
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(


                    array(
                        'name' => 'idCarga',
                        'header' =>  'Nº carga',
                        'type'=>'raw',
                        'value' => ($model->idCarga != null)? CHtml::link('Ir a carga Nº'.$model->idCarga,Yii::app()->createUrl('carga/view',array('id' => $model->idCarga))) : 'No hay cargas asociadas',
                        'htmlOptions' => array('style' => ''),
                    ),
                ),
            ));
            ?>
        </div>
        <div class="span2">
            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'oCarga.estado',
                        'value' => Carga::$aEstado[$model->oCarga->estado],
                    )
                ),
            ));
            ?>
        </div>
    </div>
</div>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#documentos">Documentos</a></li>
    <li style="<?=($model->oCarga != null && $model->oCarga->estado >= Carga::iDespachada )? 'display:none' : '' ?>"><a data-toggle="tab" href="#localidades">Localidades</a></li>
    <li style="<?=($model->oCarga != null && $model->oCarga->estado >= Carga::iDespachada )? 'display:none' : '' ?>e"><a data-toggle="tab" href="#agregarDocs">Agregar Documentos</a></li>
</ul>


<div class="tab-content">
<div id="documentos" class="tab-pane fade in active">
    <?php
    $this->renderPartial('_formFila',array(
        'model'=> $oCargaDocumento,
        'entregaDoc'=> $model,
        'aCargaDocumentos' => $aCargaDocumentos,

    ));

    ?>
</div>
    <div id="localidades" class="tab-pane fade">
        <?php

        $str_js = "
                var fixHelper = function(e, ui) {
                    ui.children().each(function() {
                        $(this).width($(this).width());
                    });
                    return ui;
                };
                        
                $('#ciudades-grid table.items tbody').sortable({
                    forcePlaceholderSize: true,
                    forceHelperSize: true,
                    items: 'tr',
                    update : function () {
                        
                    },
                    helper: fixHelper
                }).disableSelection();
                
                $('#ordenarComprobantes').click(function(){
                    var serial = $('#ciudades-grid table.items tbody').sortable('serialize', {key: '', attribute: 'ciudad'})+'&idEntregaDoc=".$model->id."';
                    $('.modalLoading').show('slow');
                    $.ajax({
                        'url': '" . $this->createUrl('/CargaDocumentos/Sort') . "',
                        'type': 'post',
                        'data': serial,
                        'success': function(data){                        
                            $.fn.yiiGridView.update('carga-documentos-grid');
                            $.fn.yiiGridView.update('ciudades-grid');
                            $('.modalLoading').hide('slow');
                        },
                        'error': function(request, status, error){
                            $('.modalLoading').hide('slow');
                            alert('Error al ordenar');
                            
                        }
                    });
                });
            ";

        Yii::app()->clientScript->registerScript('sortable-project', $str_js);
        ?>
        <a class="btn btn-primary" id="ordenarComprobantes" href="#">Ordenar Comprobantes</a>
        <?php $this->widget('bootstrap.widgets.TbGridView', array(
            'id'=>'ciudades-grid',
            'dataProvider'=>$aCargaDocumentosCiudades->search(),
            //'rowCssClassExpression'=>'"ciudades[{$data->codigoPostal}][{$data->zp}] "',
            'enableSorting' => false,
            'htmlOptions' => array(
                'style' => 'padding-top: 0px;'
            ),
            'rowHtmlOptionsExpression' => '[ "ciudad" => "ciudad_".$data->codigoPostal.".".$data->zp.""]',
            'columns'=>array(
                'id',
                'codigoPostal',
                'zp',
                'nombrelocalidad',

            ),
        )); ?>
    </div>
    <div id="agregarDocs" class="tab-pane fade">
        <span class="btn btn-primary" id="addMultipleDocs">Agregar Documentos Seleccionados</span>
        <?php    
        $this->widget('ext.selgridview.BootSelGridView', array(
            'id'=>'docs-libres-grid',
            'selectableRows' => 2,
            'dataProvider'=>$aComprobantesLibres->search(),
            'afterAjaxUpdate' => 'js:function(id,data){$.fn.yiiGridView.update("carga-documentos-grid");}',
            'enableSorting' => false,
            'filter' => $aComprobantesLibres,
            'htmlOptions' => array(
                'style' => 'padding-top:0px',
            ),
            'columns'=>array(
                array(
                    'class' => 'CCheckBoxColumn',
                    'header' => '',
                ),
                array(
                    'name' => 'tipo',
                    'header' => 'tipo',
                    'filter' => VComprobantesSinCargaDoc::$aTipoDoc,
                ),
                array(
                    'name' => 'idCliente',
                    'header' => 'Cliente',
                    'value' => '$data->oCliente->Title',
                    'filter' => CHtml::listData(VComprobantesSinCargaDoc::model()->findAll($aComprobantesLibres->search()->criteria),'idCliente','oCliente.Title'),
                ),
                array(
                    'name' => 'Fecha',
                    'header' => 'Fecha',
                    'value' => '($data->tipo == "NE")? ComponentesComunes::fechaFormateada($data->oNotaEntrega->fecha) : ComponentesComunes::fechaFormateada($data->oComprobante->fecha)'
                ),
                array(
                    'name' => 'Nro',
                    'header' => 'Nro',
                    'value' => '($data->tipo == "NE")? $data->oNotaEntrega->nroComprobante : $data->oComprobante->FullTitle'
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{vincular}',
                    'buttons'=>array(
                        'vincular' => array(
                            'url'=>'Yii::app()->controller->createUrl("EntregaDoc/vincular", array("id"=>$data->id,"tipo"=>$data->tipo,"idEntregaDoc"=>"'.$model->id.'"))',
                            'click' => 'js:function(e){
                                vincular($(this).attr("href"));
                                return false;
                            }',
                            'label' => 'Vincular Documento',
                            'icon' => 'plus-sign',
                            'htmlOptions' => array(
                                'x' => 'y',
                                'tipo' => '$data->tipo',
                            )
                        ),
                    ),
                ),

            ),
        )); ?>
    </div>
</div>
<div class="modalLoading" ><!-- Place at bottom of page --></div>