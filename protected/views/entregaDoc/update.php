<?php
$this->breadcrumbs=array(
	'Entrega Docs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nueva EntregaDoc', 'url'=>array('create')),
	array('label'=>'Ver EntregaDoc', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM EntregaDoc', 'url'=>array('admin')),
);
?>

	<h1>Modificando EntregaDoc <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>