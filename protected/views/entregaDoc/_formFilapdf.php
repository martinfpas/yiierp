<style>
    tr.odd td,tr.even td{
        padding-bottom:25px;
        border-bottom: 0.05em dotted black;
    }
</style>
<?php
/* @var $data CargaDocumentos */
$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'carga-documentos-grid',
'template' => '{items}',
'dataProvider'=>$aCargaDocumentos->searchWP(),
'enableSorting' => false,
'columns'=>array(
        array(
            'header' => 'NºCliente',
            'value' => '$data->Cliente->id',
            'headerHtmlOptions' => array('style' => 'text-align:left; width:70px;'),
        ),

        array(
            'name' => 'razonSocial',
            'value' => '$data->Cliente->Title',
            'headerHtmlOptions' => array('style' => 'text-align:left; width:350px;'),
        ),
        array(
            'name' => 'Localidad',
            'value' => '$data->Cliente->Localidad',
            'headerHtmlOptions' => array('style' => 'text-align:left; width:120px;'),
        ),
        /*
        array(
            'name' => 'tipoDoc',
            'headerHtmlOptions' => array('style' => 'text-align:left;width:70px;'),
        ),
        array(
            'name' => 'Numero',
            'value' => '($data->tipoDoc == CargaDocumentos::NE)? $data->oNotaEntrega->nroComprobante : $data->oFactura->Nro_Comprobante ',
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        */
        array(
            'name' => 'totalFinal',
			'header' => 'Importe',
			'value' => '"$".number_format($data->totalFinal,"2",",","")',
			'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
		),
        array(
            'name' => 'Observacion',
			'value' => '',
			'htmlOptions' => array('style' => 'width:100px;'),
		),

	),
)); ?>