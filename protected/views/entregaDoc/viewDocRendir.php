<style>
    .container-fluid{padding-right:20px;padding-left:20px;*zoom:1;}.container-fluid:before,.container-fluid:after{display:table;content:"";line-height:0;}
    .container-fluid:after{clear:both;}
    .container-fluid{padding:0;}
    .row-fluid{width:100%;*zoom:1;}.row-fluid:before,.row-fluid:after{display:table;content:"";line-height:0;}
    .row-fluid:after{clear:both;}
    .row-fluid [class*="span"]{display:block;width:100%;min-height:30px;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;float:left;margin-left:2.127659574468085%;*margin-left:2.074468085106383%;}
    .row-fluid [class*="span"]:first-child{margin-left:0;}
    .row-fluid .controls-row [class*="span"]+[class*="span"]{margin-left:2.127659574468085%;}
    .row-fluid .span12{width:100%;*width:99.94680851063829%;}
    .row-fluid .span11{width:91.48936170212765%;*width:91.43617021276594%;}
    .row-fluid .span10{width:82.97872340425532%;*width:82.92553191489361%;}
    .row-fluid .span9{width:74.46808510638297%;*width:74.41489361702126%;}
    .row-fluid .span8{width:65.95744680851064%;*width:65.90425531914893%;}
    .row-fluid .span7{width:57.44680851063829%;*width:57.39361702127659%;}
    .row-fluid .span6{width:48.93617021276595%;*width:48.88297872340425%;}
    .row-fluid .span5{width:40.42553191489362%;*width:40.37234042553192%;}
    .row-fluid .span4{width:31.914893617021278%;*width:31.861702127659576%;}
    .row-fluid .span3{width:23.404255319148934%;*width:23.351063829787233%;}
    .row-fluid .span2{width:14.893617021276595%;*width:14.840425531914894%;}
    .row-fluid .span1{width:6.382978723404255%;*width:6.329787234042553%;}
    .row-fluid .offset12{margin-left:104.25531914893617%;*margin-left:104.14893617021275%;}
    .row-fluid .offset12:first-child{margin-left:102.12765957446808%;*margin-left:102.02127659574467%;}
    .row-fluid .offset11{margin-left:95.74468085106382%;*margin-left:95.6382978723404%;}
    .row-fluid .offset11:first-child{margin-left:93.61702127659574%;*margin-left:93.51063829787232%;}
    .row-fluid .offset10{margin-left:87.23404255319149%;*margin-left:87.12765957446807%;}
    .row-fluid .offset10:first-child{margin-left:85.1063829787234%;*margin-left:84.99999999999999%;}
    .row-fluid .offset9{margin-left:78.72340425531914%;*margin-left:78.61702127659572%;}
    .row-fluid .offset9:first-child{margin-left:76.59574468085106%;*margin-left:76.48936170212764%;}
    .row-fluid .offset8{margin-left:70.2127659574468%;*margin-left:70.10638297872339%;}
    .row-fluid .offset8:first-child{margin-left:68.08510638297872%;*margin-left:67.9787234042553%;}
    .row-fluid .offset7{margin-left:61.70212765957446%;*margin-left:61.59574468085106%;}
    .row-fluid .offset7:first-child{margin-left:59.574468085106375%;*margin-left:59.46808510638297%;}
    .row-fluid .offset6{margin-left:53.191489361702125%;*margin-left:53.085106382978715%;}
    .row-fluid .offset6:first-child{margin-left:51.063829787234035%;*margin-left:50.95744680851063%;}
    .row-fluid .offset5{margin-left:44.68085106382979%;*margin-left:44.57446808510638%;}
    .row-fluid .offset5:first-child{margin-left:42.5531914893617%;*margin-left:42.4468085106383%;}
    .row-fluid .offset4{margin-left:36.170212765957444%;*margin-left:36.06382978723405%;}
    .row-fluid .offset4:first-child{margin-left:34.04255319148936%;*margin-left:33.93617021276596%;}
    .row-fluid .offset3{margin-left:27.659574468085104%;*margin-left:27.5531914893617%;}
    .row-fluid .offset3:first-child{margin-left:25.53191489361702%;*margin-left:25.425531914893618%;}
    .row-fluid .offset2{margin-left:19.148936170212764%;*margin-left:19.04255319148936%;}
    .row-fluid .offset2:first-child{margin-left:17.02127659574468%;*margin-left:16.914893617021278%;}
    .row-fluid .offset1{margin-left:10.638297872340425%;*margin-left:10.53191489361702%;}
    .row-fluid .offset1:first-child{margin-left:8.51063829787234%;*margin-left:8.404255319148938%;}
    [class*="span"].hide,.row-fluid [class*="span"].hide{display:none;}
    [class*="span"].pull-right,.row-fluid [class*="span"].pull-right{float:right;}
    .container{margin-right:auto;margin-left:auto;*zoom:1;}.container:before,.container:after{display:table;content:"";line-height:0;}
    .container:after{clear:both;}
    .container-fluid{padding-right:20px;padding-left:20px;*zoom:1;}.container-fluid:before,.container-fluid:after{display:table;content:"";line-height:0;}
    .container-fluid:after{clear:both;}


    .row-fluid table td[class*="span"],.row-fluid table th[class*="span"]{display:table-cell;float:none;margin-left:0;}
    .table td.span1,.table th.span1{float:none;width:44px;margin-left:0;}
    .table td.span2,.table th.span2{float:none;width:124px;margin-left:0;}
    .table td.span3,.table th.span3{float:none;width:204px;margin-left:0;}
    .table td.span4,.table th.span4{float:none;width:284px;margin-left:0;}
    .table td.span5,.table th.span5{float:none;width:364px;margin-left:0;}
    .table td.span6,.table th.span6{float:none;width:444px;margin-left:0;}
    .table td.span7,.table th.span7{float:none;width:524px;margin-left:0;}
    .table td.span8,.table th.span8{float:none;width:604px;margin-left:0;}
    .table td.span9,.table th.span9{float:none;width:684px;margin-left:0;}
    .table td.span10,.table th.span10{float:none;width:764px;margin-left:0;}
    .table td.span11,.table th.span11{float:none;width:844px;margin-left:0;}
    .table td.span12,.table th.span12{float:none;width:924px;margin-left:0;}
    .table tbody tr.success>td{background-color:#dff0d8;}
    .table tbody tr.error>td{background-color:#f2dede;}
    .table tbody tr.warning>td{background-color:#fcf8e3;}
    .table tbody tr.info>td{background-color:#d9edf7;}
    .table-hover tbody tr.success:hover>td{background-color:#d0e9c6;}
    .table-hover tbody tr.error:hover>td{background-color:#ebcccc;}
    .table-hover tbody tr.warning:hover>td{background-color:#faf2cc;}
    .table-hover tbody tr.info:hover>td{background-color:#c4e3f3;}

    .table td.span1,.table th.span1{float:none;width:44px;margin-left:0;}
    .table td.span2,.table th.span2{float:none;width:124px;margin-left:0;}
    .table td.span3,.table th.span3{float:none;width:204px;margin-left:0;}
    .table td.span4,.table th.span4{float:none;width:284px;margin-left:0;}
    .table td.span5,.table th.span5{float:none;width:364px;margin-left:0;}
    .table td.span6,.table th.span6{float:none;width:444px;margin-left:0;}
    .table td.span7,.table th.span7{float:none;width:524px;margin-left:0;}
    .table td.span8,.table th.span8{float:none;width:604px;margin-left:0;}
    .table td.span9,.table th.span9{float:none;width:684px;margin-left:0;}
    .table td.span10,.table th.span10{float:none;width:764px;margin-left:0;}
    .table td.span11,.table th.span11{float:none;width:844px;margin-left:0;}
    .table td.span12,.table th.span12{float:none;width:924px;margin-left:0;}
    .table tbody tr.success>td{background-color:#dff0d8;}
    .table tbody tr.error>td{background-color:#f2dede;}
    .table tbody tr.warning>td{background-color:#fcf8e3;}
    .table tbody tr.info>td{background-color:#d9edf7;}
    .table-hover tbody tr.success:hover>td{background-color:#d0e9c6;}
    .table-hover tbody tr.error:hover>td{background-color:#ebcccc;}
    .table-hover tbody tr.warning:hover>td{background-color:#faf2cc;}
    .table-hover tbody tr.info:hover>td{background-color:#c4e3f3;}

    .row-fluid [class*="span"]:first-child{margin-left:0;}
    .row-fluid .controls-row [class*="span"]+[class*="span"]{margin-left:2.127659574468085%;}
    .row-fluid .span12{width:100%;*width:99.94680851063829%;}
    .row-fluid .span11{width:91.48936170212765%;*width:91.43617021276594%;}
    .row-fluid .span10{width:82.97872340425532%;*width:82.92553191489361%;}
    .row-fluid .span9{width:74.46808510638297%;*width:74.41489361702126%;}
    .row-fluid .span8{width:65.95744680851064%;*width:65.90425531914893%;}
    .row-fluid .span7{width:57.44680851063829%;*width:57.39361702127659%;}
    .row-fluid .span6{width:48.93617021276595%;*width:48.88297872340425%;}
    .row-fluid .span5{width:40.42553191489362%;*width:40.37234042553192%;}
    .row-fluid .span4{width:31.914893617021278%;*width:31.861702127659576%;}
    .row-fluid .span3{width:23.404255319148934%;*width:23.351063829787233%;}
    .row-fluid .span2{width:14.893617021276595%;*width:14.840425531914894%;}
    .row-fluid .span1{width:6.382978723404255%;*width:6.329787234042553%;}
    /* de BOOTSTRAP */

    .grid-view {
        padding-top: 0px;
        font-size: smaller;
        width: 850px;
    }

    .bordeRedondo{
        border: #dddddd 1px solid;
        padding: 10px;
        border-radius: 0.7em;
        min-height: 55px;
    }

    .table th, .table td{
        padding: 0px;
    }

    .detail-view th {
        width: 80px;
        font-size: 60%;
    }
    .detail-view td {
        font-size: 60%;
    }
    .span1{
        float: left;
    }
    .span2{
        float: left;
    }
    .span3{
        float: left;
    }
    .span4{
        float: left;
        width:31%;
        background-color: #2d6987;
    }
    .span5{
        float: left;
    }
    .span6{
        float: left;
    }
    .span7{
        float: left;
    }
    .span8{
        float: left;
    }
    .span9{
        float: left;
    }
    .span10{
        float: left;
    }
    .span11{
        float: left;
    }
    .span12{
        float: left;
    }
    hr{
        margin-bottom: 5px;
        margin-top: 5px;
    }
    tr.nuevo td{
        padding-bottom:25px;
        padding-top:5px;
        border-top: 0.05em dotted black;
        border-right: 0.05em dotted black;
    }
    tr.agregado td{
        padding-bottom:15px;
        border-right: 0.05em dotted black;
    }
</style>
<?php

/* @var $model EntregaDoc */
/* @var $aEntregaGtos EntregaGtos */
/* @var $aPagosCheque VPagosDeEntregaDoc*/
?>
<h5> Listado Entrega Camion / Viajante #<?=$model->oCarga->id?></h5>

<div class="container-fluid" style="width: 600px;">
    <div class="row-fluid" style="border-bottom: 1px dotted">
        <div class="span6">
            <?php
                $tituloViajanteOCamion = '';

                if($model->tipoPersona == EntregaDoc::camion){
                    $oCamion = Vehiculo::model()->findByPk($model->nroCamViaj);
                    if($oCamion != null){
                        $tituloViajanteOCamion = $oCamion->IdNombre;
                    }

                }else{
                    $oViajante = Viajante::model()->findByPk($model->nroCamViaj);
                    if($oViajante != null){
                        $tituloViajanteOCamion = $oViajante->IdTitulo;
                    }
                }

            ?>
            <span>Viajante o camion <?=$tituloViajanteOCamion?></span>
        </div>
        <div class="span6">
            <span>Fecha de carga <?=($model->oCarga != null)? ComponentesComunes::fechaFormateada($model->oCarga->fecha) : "" ?></span>
        </div>
    </div>
    <div class="row-fluid" style="border-bottom: 1px dotted">
        <div class="span9">
            <?php

            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'entrega-gtos-grid',
                'dataProvider'=>$aEntregaGtos->searchWpWs(),
                'template' => '{items}',
                'columns'=>array(

                    array(
                        'name' => 'gasto',
                        'headerHtmlOptions' => array(
                            'style' => 'text-align:left;width:100px;',
                        ),
                        'htmlOptions' => array(
                            'style' => 'text-align:left;width:100px;',
                        )
                    ),
                    array(
                        'name' => 'montoGasto',
                        'value' => '"$".number_format($data->montoGasto ,2,".","")',
                        'htmlOptions' => array(
                            'style' => 'text-align:right;',
                        )
                    ),

                ),
            ));

            ?>
        </div>
    </div>
</div>


<br/>

<span>Observaciones : </span>
<p style=""><?=$model->observacion?></p>

<br />

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4" style="width:100px">
            <span>Monto Inicial Entregado :</span>
        </div>
        <div class="span2" style="text-align: right;padding-right: 5px;">
            $ <?=number_format($model->c,2)?>
        </div>
        <div class="span4">
            <span> - Kilometros Recorridos :</span>
        </div>
        <div class="span2" style="text-align: right">
            <span> <?=number_format($model->kmRecorridos,2)?>km</span>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Litros Cargados :</span>
        </div>
        <div class="span2" style="text-align: right;padding-right: 5px;">
            <?=number_format($model->litrosCargados,2)?>lts
        </div>
        <div class="span4">
            <span> - CONSUMO : </span>
        </div>
        <div class="span2" style="text-align: right">
            <?=($model->litrosCargados!=0 )?number_format(($model->kmRecorridos / $model->litrosCargados),2) : 0;?>
        </div>
    </div>

</div>

<br />

<div class="container-fluid" >
    <div class="row-fluid">
        <br>
        <div class="span4">
            <span>Costo Merdaderia Entregada :</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->oCarga->CostoMercaderia,2,".","") ?>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Carga</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalMercaderia,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Devoluciones</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalDevoluciones,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Notas De Credito</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalNotaDeCredito,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Contado Pendiente</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->ContadoPendiente,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Efectivo</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalEfectivo,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Cheques</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalCheques,2,'.','') ?>
        </div>
    </div>
    <br>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>Total gastos</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalGastos,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Otras devoluciones</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalDevoluciones,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Otros Cobros</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->otrosPagos,2,'.','')?>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>TOTAL A RENDIR</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalARendir,2,'.','')?>
        </div>
    </div>
</div>
<br />
<span> RESPONSABLE REVISOR DE ESTA ENTREGA...............................................</span>

<br />
<br />
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6" style="">
            COMISIONES GENERADAS CON LA ENTREGA PARA
        </div>
        <div class="span4" style="text-align: center;">
           <?=$tituloViajanteOCamion?>
        </div>
        <div class="span2" style="text-align: right;">

            $ <?=number_format($model->ComisionesGeneradas,2,'.',',')?>
        </div>

    </div>

    <?php
    foreach ($model->aComisiones as $index => $comision) {
    ?>
        <div class="row-fluid">
            <br>
            <div class="span6" style="">
                COMISIONES GENERADAS CON LA ENTREGA PARA
            </div>
            <div class="span4" style="text-align: center">
                <span><?=$index.' - '.EntregaDoc::$aViajantes[$index]?></span>
            </div>
            <div class="span2" style="text-align: right;">
                $ <?=number_format($comision,2,'.',',')?>
            </div>
        </div>
    <?php
    }
    ?>


</div>
<br>
<hr>
<span>CHEQUES RECIBIDOS EN ESTA ENTREGA</span>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'entrega-gtos-grid',
    'dataProvider'=>$aPagosCheque->searchChequesWpWs(),
    'template' => '{items}',
    'htmlOptions' => array(

    ),
    'columns'=>array(
        array(
            'name' => 'nroCheque',
            'value' => '$data->oChequedeTercero->nroCheque',
            'headerHtmlOptions' => array(
                'style' => 'width:100px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:100px;text-align:left;',
            )
        ),

        array(
            'name' => 'Banco',
            'value' => '$data->oChequedeTercero->oBanco->nombre',
            'headerHtmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            )
        ),
        array(
            'name' =>'Sucursal',
            'value' =>'($data->oChequedeTercero->oSucursal != null)? $data->oChequedeTercero->oSucursal->nombre : ""',
            'headerHtmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            )
        ),

        array(
            'name' => 'Cliente',
            //'value' => '$data->oCliente->Title',
            'value' => '$data->oChequedeTercero->oCliente->Title',
            'headerHtmlOptions' => array(
                'style' => 'width:300px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:300px;text-align:left;',
            )
        ),

        array(
          'name' => 'monto',
          'header' => 'Importe',
          'value' => '"$".number_format($data->oChequedeTercero->importe,2)',
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;width:100px;',
            ),
            'htmlOptions' => array(
                'style' => 'text-align:right;width:100px;',
            )
        ),

    ),
)); ?>