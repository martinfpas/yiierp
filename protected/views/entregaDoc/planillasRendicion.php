<?php
/* @var $this EntregaDocController */
/* @var $model EntregaDoc */

$this->breadcrumbs=array(
    'Documentos a Entregar'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva Doc a Entregar', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#entrega-doc-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});


");
?>

<h1>Gestion de Planillas De Rendicion</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'entrega-doc-grid',
'dataProvider'=>$model->searchDespachadasOCerradas(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'idCarga',
            'htmlOptions' => array('style' => 'width:60px'),
		),

        array(
            'name' => 'fechaCarga',
            'header' => 'Fecha',
            'value' => '($data->oCarga != null)? ComponentesComunes::fechaFormateada($data->oCarga->fecha) : "Sin carga vinculada"',
            'htmlOptions' => array('style' => 'width:80px'),
        ),

        array(
            'filter' => EntregaDoc::$aTipoPersona,
            'name' => 'tipoPersona',
            'htmlOptions' => array('style' => 'width:90px;text-align:center;'),
		),

		'nroCamViaj',
        array(
            'name' => 'totalEfectivo',
            'value' => '"$".number_format($data->totalEfectivo,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'totalCheques',
            'value' => '"$".number_format($data->totalCheques,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'estadoCarga',
            'header' => 'Estado Carga',
			'value' => '($data->oCarga != null)? Carga::$aEstado[$data->oCarga->estado]  : "Sin carga vinculada"',
			'htmlOptions' => array('style' => ''),
            'filter' => Carga::$aEstadoDesp,
		),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}',
            'buttons' => array(
                'view' => array(
                    'url'=>'Yii::app()->controller->createUrl("EntregaDoc/rendir", array("id"=>$data->id))',
                )
            ),
            'headerHtmlOptions' => array('style' => 'width:50px'),
        ),
	),
)); ?>
