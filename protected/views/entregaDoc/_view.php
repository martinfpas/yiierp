<?php
/* @var $this EntregaDocController */
/* @var $data EntregaDoc */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoPersona')); ?>:</b>
	<?php echo CHtml::encode($data->tipoPersona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroCamViaj')); ?>:</b>
	<?php echo CHtml::encode($data->nroCamViaj); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacion')); ?>:</b>
	<?php echo CHtml::encode($data->observacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('montoInicial')); ?>:</b>
	<?php echo CHtml::encode($data->montoInicial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kmRecorridos')); ?>:</b>
	<?php echo CHtml::encode($data->kmRecorridos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('litrosCargados')); ?>:</b>
	<?php echo CHtml::encode($data->litrosCargados); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalEfectivo')); ?>:</b>
	<?php echo CHtml::encode($data->totalEfectivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalCheques')); ?>:</b>
	<?php echo CHtml::encode($data->totalCheques); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otrosPagos')); ?>:</b>
	<?php echo CHtml::encode($data->otrosPagos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cerrada')); ?>:</b>
	<?php echo CHtml::encode($data->cerrada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('otrasDevoluciones')); ?>:</b>
	<?php echo CHtml::encode($data->otrasDevoluciones); ?>
	<br />

	*/ ?>

</div>