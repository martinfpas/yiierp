<?php

/* @var $model EntregaDoc */
/* @var $aEntregaGtos EntregaGtos */
/* @var $aPagosCheque VPagosDeEntregaDoc*/
?>
<span> Listado Entrega Camion / Viajante #<?=$model->oCarga->id?></span><br><br>
<div class="container-fluid">
    <div class="row-fluid" style="border-bottom: 1px dotted">
        <div class="span12">
            <?php
                $tituloViajanteOCamion = '';

                if($model->tipoPersona == EntregaDoc::camion){
                    $oCamion = Vehiculo::model()->findByPk($model->nroCamViaj);
                    if($oCamion != null){
                        $tituloViajanteOCamion = $oCamion->IdNombre;
                    }
                    $tituloViajanteOCamion .= ' - Conductor: ';
                    if($model->oCarga->oCamionero != null){
                        $tituloViajanteOCamion .= $model->oCarga->oCamionero->getTitulo();
                    }


                }else{
                    $oViajante = Viajante::model()->findByPk($model->nroCamViaj);
                    if($oViajante != null){
                        $tituloViajanteOCamion = $oViajante->IdTitulo;
                    }
                }

            ?>
            <span>Camion <?=$tituloViajanteOCamion?></span>
        </div>
    </div>
    <div class="row-fluid" style="border-bottom: 1px dotted">
        <div class="span9">
            <?php

            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'entrega-gtos-grid',
                'dataProvider'=>$aEntregaGtos->searchWpWs(),
                'template' => '{items}',
                'columns'=>array(

                    array(
                        'name' => 'gasto',
                        'headerHtmlOptions' => array(
                            'style' => 'text-align:left;width:100px;',
                        ),
                        'htmlOptions' => array(
                            'style' => 'text-align:left;width:100px;',
                        )
                    ),
                    array(
                        'name' => 'montoGasto',
                        'value' => '"$".number_format($data->montoGasto ,2,".","")',
                        'htmlOptions' => array(
                            'style' => 'text-align:right;',
                        )
                    ),

                ),
            ));

            ?>
        </div>
    </div>
</div>


<br/>
<p style=""><span>Observaciones : </span> <?=$model->oCarga->observacion?></p>
<br>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4">
            <span>Monto Inicial Entregado :</span>
        </div>
        <div class="span2" style="text-align: right;padding-right: 5px;">
            $ <?=number_format($model->montoInicial,2)?>
        </div>
        <div class="span4">
            <span> - Kilometros Recorridos :</span>
        </div>
        <div class="span2" style="text-align: right">
            <span> <?=number_format($model->kmRecorridos,2)?>km</span>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Litros Cargados :</span>
        </div>
        <div class="span2" style="text-align: right;padding-right: 5px;">
            <?=number_format($model->litrosCargados,2)?>lts
        </div>
        <div class="span4">
            <span> - CONSUMO : </span>
        </div>
        <div class="span2" style="text-align: right">
            <?=($model->litrosCargados!=0 )?number_format(($model->kmRecorridos / $model->litrosCargados),2) : 0;?>
        </div>
    </div>

</div>

<br>

<div class="container-fluid" >
    <div class="row-fluid">
        <br>
        <div class="span4">
            <span>Costo Merdaderia Entregada :</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->oCarga->CostoMercaderia,2,".","") ?>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Carga</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalMercaderia,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Devoluciones</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalDevoluciones,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total En Notas De Credito</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalNotaDeCredito,2,".","") ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Contado Pendiente</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->ContadoPendiente,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Efectivo</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalEfectivo,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Cheques</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalCheques,2,'.','') ?>
        </div>
    </div>
    <br>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>Total gastos</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalGastos,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Otras devoluciones</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalDevoluciones,2,'.','') ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <span>Total Otros Cobros</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->otrosPagos,2,'.','')?>
        </div>
    </div>
    <br>
    <div class="row-fluid">
        <div class="span4">
            <span>TOTAL A RENDIR</span>
        </div>
        <div class="span3" style="text-align: right">
            $ <?=number_format($model->TotalARendir,2,'.','')?>
        </div>
    </div>
</div>
<br/>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6" style="">
            <span> RESPONSABLE REVISOR DE ESTA ENTREGA</span>
        </div>
        <div class="span6" style="border-bottom: dotted;">
            .
        </div>
    </div>
</div>


<br />
    <br />
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span5" style="">
                COMISIONES GENERADAS PARA
            </div>
            <div class="span4" style="text-align: center;">
               <?=$tituloViajanteOCamion?>
            </div>
            <div class="span3" style="text-align: right;">
                $ <?=number_format($model->ComisionesGeneradas,2,'.',',')?>
            </div>

        </div>

        <?php
        $aViajantes = [];
        foreach ($model->aComisiones as $index => $comision) {                        
            if(!isset($aViajantes[$index])){
                $aViajante = Viajante::model()->findByPk($index);
                $aViajantes[$index] = ($aViajante)? $aViajante->apellido.' '.$aViajante->nombres : '--';
            }
        ?>
            <div class="row-fluid">
                <br>
                <div class="span5" style="">
                    COMISIONES GENERADAS PARA
                </div>
                <div class="span4" style="text-align: left;">
                    <span><?=$index.' - '.$aViajantes[$index]?></span>
                </div>
                <div class="span3" style="text-align: right;">
                    $ <?=number_format($comision,2,'.',',')?>
                </div>
            </div>
        <?php
        }
        ?>


    </div>
<br>
<hr>
<span>CHEQUES RECIBIDOS EN ESTA ENTREGA</span>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'entrega-gtos-grid',
    'dataProvider'=>$aPagosCheque->searchChequesWpWs(),
    'enablePagination' => false,
    'template' => '{items}',
    'htmlOptions' => array(

    ),
    'columns'=>array(
        array(
            'name' => 'nroCheque',
            'value' => '$data->oChequedeTercero->nroCheque',
            'headerHtmlOptions' => array(
                'style' => 'width:100px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:100px;text-align:left;',
            )
        ),

        array(
            'name' => 'Banco',
            'value' => '$data->oChequedeTercero->oBanco->nombre',
            'headerHtmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            )
        ),
        /*
        array(
            'name' =>'Sucursal',
            'value' =>'($data->oChequedeTercero->oSucursal != null)? $data->oChequedeTercero->oSucursal->nombre : ""',
            'headerHtmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:150px;text-align:left;',
            )
        ),
        */
        array(
            'name' => 'Cliente',
            //'value' => '$data->oCliente->Title',
            'value' => 'substr($data->oChequedeTercero->oCliente->Title,0,35)',
            'headerHtmlOptions' => array(
                'style' => 'width:310px;text-align:left;',
            ),
            'htmlOptions' => array(
                'style' => 'width:310px;text-align:left;',
            )
        ),

        array(
          'name' => 'monto',
          'header' => 'Importe',
          'value' => '"$".number_format($data->oChequedeTercero->importe,2)',
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;width:100px;',
            ),
            'htmlOptions' => array(
                'style' => 'text-align:right;width:100px;',
            )
        ),

    ),
)); ?>