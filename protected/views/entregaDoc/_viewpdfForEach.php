<?php
/* @var $model EntregaDoc*/
/* @var $aCargaDocumentos CargaDocumentos*/
/* @var $oCargaDocumento CargaDocumentos*/
?>
<style>
    tr.nuevo td{
        padding-bottom:25px;
        padding-top:5px;
        border-top: 0.05em dotted black;
        border-right: 0.01em dashed  black;
    }
    tr.agregado td{
        padding-bottom:15px;
        border-right: 1pt dashed  black;
        border-width: thin;
    }

</style>
<h3>Entrega de Documentos #<?=$model->oCarga->id;?></h3>


<div id="carga-documentos-grid" class="grid-view">
    <table class="items table">
        <thead>
        <tr>
            <th style="text-align:center;width:55px;" id="carga-documentos-grid_c0">Nº Cli</th><th style="text-align:left; width:300px;" id="carga-documentos-grid_c1">Razon Social</th><th style="text-align:left; width:170px;" id="carga-documentos-grid_c2">Localidad</th><th style="text-align:right;" id="carga-documentos-grid_c3">Importe</th><th id="carga-documentos-grid_c4" style="width:170px;border-right: none;">Observacion</th></tr>
        </thead>

        <tbody>
        <?php
            $cliAnt = '';
            foreach ($aCargaDocumentos as $index => $oCargaDocumento) {
                if($oCargaDocumento->Cliente->id != $cliAnt){
                    echo '<tr class="nuevo">';
                        echo '<td style="text-align:center;">'.$oCargaDocumento->Cliente->id.'</td>';
                        echo '<td>'.$oCargaDocumento->razonSocial.'</td>';
                        echo '<td>'.$oCargaDocumento->Cliente->Localidad.'</td>';
                        $cliAnt = $oCargaDocumento->Cliente->id;
                }else{
                    echo '<tr class="agregado">';
                        echo '<td></td>';
                        echo '<td></td>';
                        echo '<td></td>';
                }
                if($oCargaDocumento->tipoDoc == CargaDocumentos::FAC || $oCargaDocumento->tipoDoc == CargaDocumentos::NE || $oCargaDocumento->tipoDoc == CargaDocumentos::ND){
                    //echo '<td style="text-align:right;padding:5px; font-family:courier new;font-size: 13pt;">$'.number_format(($oCargaDocumento->totalFinal * 100),"2",",",".").'</td>';
                    echo '<td style="text-align:right;padding:5px;">'.number_format($oCargaDocumento->totalFinal ,"2",",",".").'</td>';
                }else{
                    echo '<td style="text-align:right;">-'.number_format($oCargaDocumento->totalFinal,"2",",",".").'</td>';
                }

                echo '<td style="width:80px;border-right: none;"></td>';

                echo '</tr>';
            }
        ?>
        </tbody>
    </table>
    <div style="text-align: right">
        <h4 style="margin-right: 25px;">Total: <?='$'.$model->getTotal()?></h4>
    </div>


</div>