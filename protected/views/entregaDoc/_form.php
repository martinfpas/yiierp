<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'entrega-doc-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorEntregaDoc"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okEntregaDoc">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">

	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'tipoPersona',array('class'=>'span12','disable'=>'disable')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroCamViaj',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'observacion',array('class'=>'span12','maxlength'=>400)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'montoInicial',array('class'=>'span12')); ?>
		</div>
    </div>
    <div class="row-fluid">

		<div class="span3">
            <?php
                echo $form->labelEx($model,'cerrada');
                echo $form->dropDownList($model,'cerrada',EntregaDoc::$aCerrada,array());
            ?>
        </div>
        <div class="span3">
            <?php echo $form->textFieldRow($model,'otrasDevoluciones',array('class'=>'span12')); ?>
        </div>
        <div class="span3">
            <?php echo $form->hiddenField($model,'litrosCargados',array()); ?>

            <?php echo $form->hiddenField($model,'totalEfectivo',array()); ?>

            <?php echo $form->hiddenField($model,'totalCheques',array()); ?>

            <?php echo $form->hiddenField($model,'otrosPagos',array()); ?>

            <?php echo $form->hiddenField($model,'kmRecorridos',array()); ?>

            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;'
                )
            )); ?>
        </div>

    </div>
</div>

<?php $this->endWidget(); ?>
