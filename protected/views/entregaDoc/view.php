<?php
/* @var $model EntregaDoc */
$this->breadcrumbs=array(
    'Documentos a Entregar'=>array('admin'),
	$model->id,
);

$this->menu=array(
    array('label'=>'ABM Doc a Entregar', 'url'=>array('admin')),
);
?>

<h3>Ver Documentos a Entregar de la carga #<?php echo $model->idCarga; ?></h3>

<div class="content-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'tipoPersona',
                    'nroCamViaj',
                    array(
                        'name' => 'montoInicial',
                        'value' => '$'.number_format($model->montoInicial,2,'.',''),
                    ),
                    'kmRecorridos',
                    'litrosCargados',
                ),
            ));
            ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'totalEfectivo',
                        'value' => '$'.number_format($model->totalEfectivo,2,'.',''),
                        //'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'totalCheques',
                        'value' => '$'.number_format($model->totalCheques,2,'.',''),
                        //'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'otrosPagos',
                        'value' => '$'.number_format($model->otrosPagos,2,'.',''),
                        //'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'otrasDevoluciones',
                        'value' => '$'.number_format($model->otrosPagos,2,'.',''),
                        //'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'cerrada',
                        'value' => ($model->cerrada == 0)? 'No' : 'Si',
                        'htmlOptions' => array('style' => ''),
                    ),
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            ));
            ?>
        </div>
    </div>
</div>
<?php

$this->renderPartial('_formFila',array(
    'entregaDoc' => $model,
    'model'=> $oCargaDocumento,
    'aCargaDocumentos' => $aCargaDocumentos
));
?>