<?php
/* @var $this NotaEntregaController */
/* @var $data NotaEntrega */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clase')); ?>:</b>
	<?php echo CHtml::encode($data->clase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroPuestoVenta')); ?>:</b>
	<?php echo CHtml::encode($data->nroPuestoVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->nroComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaPedido')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaPedido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCliente')); ?>:</b>
	<?php echo CHtml::encode($data->idCliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCamion')); ?>:</b>
	<?php echo CHtml::encode($data->idCamion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalFinal')); ?>:</b>
	<?php echo CHtml::encode($data->totalFinal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('enviada')); ?>:</b>
	<?php echo CHtml::encode($data->enviada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recibida')); ?>:</b>
	<?php echo CHtml::encode($data->recibida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento1')); ?>:</b>
	<?php echo CHtml::encode($data->descuento1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descuento2')); ?>:</b>
	<?php echo CHtml::encode($data->descuento2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroViajante')); ?>:</b>
	<?php echo CHtml::encode($data->nroViajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	*/ ?>

</div>