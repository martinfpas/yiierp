<style>
    table.detail-view th{
        width: 105px;
    }
</style>
<?php
/* @var $model NotaEntrega */

$this->breadcrumbs=array(
	'Nota Entregas'=>array('admin'),
	$model->id,
);

Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
    
    $('#cerrar').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let datos = 'name=estado&value=".NotaEntrega::iCerrada."&pk=".$model->id."';
        $.ajax({
            url: href,
            data: datos,
            type: 'POST',
            success:function(html){
                location.reload(true);
            },
            error:function(x,y,z){                
                console.log(x);
                console.log(y);
                console.log(z);
                alert(y);
            },
        });
    });
");

$this->menu=array(
	array('label'=>'Nueva '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.NotaEntrega::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>


<h1><?php echo NotaEntrega::model()->getAttributeLabel('model').' #'.$model->nroComprobante; ?></h1>

<div class="content-fluid">
    <div class="row-fluid headerNota">
        <div class="span6 bordeRedondo">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'nroPuestoVenta',
                    'nroComprobante',
                    'idNotaPedido',
                    array(
                        'name' => 'idCliente',
                        'value' => $model->oCliente->Title,
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'estado',
                        'value' => NotaEntrega::$aEstado[$model->estado],
                        'htmlOptions' => array('style' => ''),
                    ),
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>

        </div>
        <div class="span3 bordeRedondo">
            <?php $this->widget('editable.EditableDetailView',array(
                'data'=>$model,
                //you can define any default params for child EditableFields
                'url'        => $this->createUrl('NotaEntrega/SaveFieldAsync'), //common submit url for all fields
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                'attributes'=>array(
                    array( //select loaded from database
                        'name' => 'idCamion',
                        'editable' => array(
                            'type'   => 'select',
                            //'mode'      => ($is_modal)? 'inline' : 'popup',
                            'mode'   => 'inline',
                            'apply'  => true,
                            'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre'),
                        )
                    ),
                ),
            )); ?>
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'enviada',
                    'recibida',
                    array(
                        'name' => 'fecha',
                        'value' => ComponentesComunes::fechaFormateada($model->fecha),
                    ),
                    'nroViajante',
                    'observaciones',
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
        <div class="span3 bordeRedondo">
            <div id="montoPagadoPatchBlock" class="row-fluid" style="width: 95%;display:block">
                <div class="span12">
                    <?php $this->widget('editable.EditableDetailView',array(
                        'data'=>$model,
                        //you can define any default params for child EditableFields
                        'url'        => $this->createUrl('NotaEntrega/SaveFieldAsync'), //common submit url for all fields
                        'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                        'attributes'=>array(
                            array( //select loaded from database
                                'name' => 'montoPagado',
                                'editable' => array(
                                    'mode'   => 'inline',
                                    'apply'  => true,
                                )
                            ),
                            array(
                                'name' => 'idPagoRelacionado',
                                'editable' => array(
                                    'mode'   => 'inline',
                                    'apply'  => $model->idPagoRelacionado == null,
                                )
                            ),
                        ),
                    )); 
                    ?>
                </div>
            </div>
            
            <div class="row-fluid" style="margin-top: 10px;">
                <div class="span6">
                    <?php
                    if ($model->impSinPrecio){
                        echo CHtml::link('Imprimir <i class="icon-white icon-print"></i>',Yii::app()->createUrl('NotaEntrega/PdfSinPrecio', array('id'=>$model->id)),
                            array('class'=>'btn btn-primary print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;', 'target' => 'blank',
                                'print' => Yii::app()->createUrl('NotaEntrega/PdfSinPrecio', array('id'=>$model->id,'print'=>true)))
                        );
                    }else{
                        echo CHtml::link('Imprimir <i class="icon-white icon-print"></i>',Yii::app()->createUrl('NotaEntrega/pdf', array('id'=>$model->id)),
                            array('class'=>'btn btn-primary print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;', 'target' => 'blank',
                                'print' => Yii::app()->createUrl('NotaEntrega/pdf', array('id'=>$model->id,'print'=>true)))
                        );
                    }
                    ?>
                </div>
                <div class="span3">
                    <?php
                        if ($model->estado != NotaEntrega::iCerrada){
                            echo CHtml::link('Guardar y Cerrar <i class="icon-white"></i>',Yii::app()->createUrl('NotaEntrega/saveFieldAsync', array('id'=>$model->id)),
                                array('class'=>'btn btn-success','id' => 'cerrar','style' => 'float:left;margin-right:5px;')
                            );
                        }

                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php

    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'prod-nota-entrega-grid',
        'dataProvider' => $aProdNotaEntrega->search(),
        //'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");refreshComprobanteDetails();}',
        'template' => '{items}',
        'columns' => array(
            array(
                'name' => 'codigo',
                'value' => '$data->oArtVenta->codigo',
                'htmlOptions' => array('style' => ''),
            ),
            'descripcion',

            array(
                'name' => 'precioUnitario',
                'value' => '"$".$data->precioUnitario',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'headerHtmlOptions' => array('style' => 'text-align:right;'),
            ),
            'cantidad',
            array(
                'name' => 'importe',
                'value' => '"$".$data->importe',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'headerHtmlOptions' => array('style' => 'text-align:right;'),
            ),
        ),
    ));

    ?>
    <div class="content-fluid bordeRedondo">
        <div class="row-fluid" style="text-align: right;">
            <div class="span1" style="">
                <b>Subtotal: </b>
            </div>

            <div class="span2"> Descuento 1 </div>

            <div class="span2">Descuento2</div>
            <div class="span1" style="">
                <b>Subtotal: </b>
            </div>
            <div class="span2">

            </div>
            <div class="span2" style="text-align: right;">

                &nbsp;<span style="float: right"></span>&nbsp;
            </div>
            <div class="span2" ><b>Total $</b></div>

        </div>
        <div class="row-fluid" style="text-align: right;">

            <div class="span1">
                <b>$<span id="spanSubTotalNeto"><?=$model->SubTotal?></span></b>
            </div>
            <div class="span2">
                <?=number_format($model->descuento1,2,'.','')?></div>
            <div class="span2">
                <?=number_format($model->descuento2,2,'.','')?>
            </div>
            <div class="span1">
                <b>$<span id="spanSubTotal"><?=number_format($model->subtotal ,2,'.','') ?></span></b>
                <?php
                /*
                echo $form2->hiddenField($model,'id');
                echo $form2->hiddenField($model,'clase');
                echo $form2->hiddenField($model,'fecha');
                echo $form2->hiddenField($model,'Nro_Puesto_Venta');
                */
                ?>
            </div>
            <div class="span2" id="Ing_Brutos">

            </div>
            <div class="span2" id="subtotalIva">

            </div>
            <div class="span2" style=""><b>$<span id="spanTotal"><?=number_format($model->totalFinal,2, '.', '')?></span></b></div>
        </div>
    </div>
</div>
<?php $this->widget('PrintWidget') ?>