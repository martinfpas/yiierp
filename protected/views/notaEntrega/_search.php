<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'clase',array('class'=>'span12','maxlength'=>1)); ?>

		<?php echo $form->textFieldRow($model,'nroPuestoVenta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nroComprobante',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'idNotaPedido',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idCliente',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idCamion',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'totalFinal',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'enviada',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'recibida',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descuento1',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descuento2',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nroViajante',array('class'=>'span12')); ?>

		<?php echo $form->textAreaRow($model,'observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
