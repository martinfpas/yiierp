
/* @var $this NotaEntregaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Nota Entregas',
);

$this->menu=array(
	array('label'=>'Nueva '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Gestion de '.NotaEntrega::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h2><?=NotaEntrega::model()->getAttributeLabel('models')?></h2>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
