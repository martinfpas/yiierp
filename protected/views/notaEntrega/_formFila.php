<style>
    .grid-view {
        padding-top: 0px;
    }
    .detail-view th {
        width: 80px;
        font-size: 85%;
    }
    .detail-view td {
        font-size: 85%;
    }
    #fichaCliente th{
        width:90px;
    }
</style>
<?php

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
/* @var $model NotaEntrega */

$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
$cs->registerScriptFile($baseUrl."/js/music.js");

if ($is_modal){
    //Yii::log('es modal','warning');
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );

    Yii::app()->clientScript->registerScript('notaEntregaGuardarNE', "
    $('#guardarNE').click(function(){
        window.location = '".(($model->id > 0)? Yii::app()->createUrl('notaEntrega/view',array('id'=> $model->id )) : Yii::app()->createUrl('notaEntrega/admin'))."';
        });
        
        
    ");
}else{
    //Yii::log('NO ES modal','warning');
}

Yii::app()->clientScript->registerScript('print-button', "
    var isModal = ('$is_modal'=='1');
    
    function afterAjaxUpdateNE(){
        if(isModal){
            
        }else{
            //TODO: BUSCAR UN MEJOR FIX PARA ESTO. TAMBIEN ESTA AFECTANDO A np
            //SI EL PRIMER RENGLON DE PRODUCTOS NO ESTA EDITABLE, RECARGO LA PAGINA
            if(!$('[rel=\"ProdNotaEntrega_precioUnitario\"]').first().hasClass('editable editable-click')){
                window.location = '".Yii::app()->createUrl('NotaEntrega/update',array('id'=>$model->id))."';
            }
        }
    }
    
    function gridItems(){
	    return $('[rel=\"ProdNotaEntrega_precioUnitario\"]').length;
	}
    
    $('#cerrar').click(function(e){        
        e.stopPropagation();
        e.preventDefault();
        $('.modalLoading').show('slow'); 
        let href = $(this).attr('href');
        let datos = 'name=estado&value=".NotaEntrega::iCerrada."&pk=".$model->id."';
        $.ajax({
            url: href,
            data: datos,
            type: 'POST',
            success:function(html){
                $('.modalLoading').hide('slow'); 
                if(!isModal){                        
                    window.location = '".Yii::app()->createUrl('NotaEntrega/view',array('id'=>$model->id))."';
                }
            },
            error:function(x,y,z){                
                console.log(x);
                console.log(y);
                console.log(z);
                $('.modalLoading').hide('slow'); 
                alert(y);
            },
        });
    });

    $('#idPdf').click(function(e){
        e.stopImmediatePropagation();
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
        $('#nota-entrega-form2').submit();
        return false;
    });
    /*
    $('#idPdf').click(function(e){                
        $('#nota-entrega-form2').submit();
        return true;
    });
    */

",CClientScript::POS_READY);




Yii::app()->clientScript->registerScript('notaEntregaHandlers', "
        $(document).ready(function(){
            
            bindNotaEntregaHandlers();
        });
        
        
        
        function bindNotaEntregaHandlers(){      
            
            console.log('bindNotaEntregaHandlers() 4');

            showView(".$model->idCliente.");            
            $('#clase').change(function(){
                $('#NotaEntrega_clase').val($(this).val());
            });            
            $('#nroPuestoVenta').change(function(){
                $('#NotaEntrega_nroPuestoVenta').val($(this).val());
            });            
            $('#fecha').change(function(){
                $('#NotaEntrega_fecha').val($(this).val());
            });
            $('#NotaEntrega_descuento1').keypress(function(e) {
                if(e.keyCode == 13){
                    e.preventDefault();
                    $('#NotaEntrega_descuento2').focus();
                }   
            });
            
            $('#NotaEntrega_descuento2').keypress(function(e) {
                if(e.keyCode == 13){
                    e.preventDefault();
                    $('#guardarNE').focus();
                }
            });                                   
            
            
            $('#nota-entrega-form2').submit(function(e){
                e.stopImmediatePropagation();
                e.stopPropagation();
                e.preventDefault();
                var datos = $(this).serialize()+'&cerrar=1';                
                var respuesta ;
                var url = $(this).attr('action');
                var divError = $(this).find('.SaveAsyncError');
                var divOk = $(this).find('.SaveAsyncOk');                
                var form = $(this);                
                $(divError).hide('slow');
                $(divOk).hide('slow');                
                
                $.ajax({
                    url: url,
                    type: 'Post',
                    data: datos,
                    success: function (html) {
                        //RECARGA LA PAGINA SI SE VENCIO LA SESION
                        if(html == 'YII_LOGIN_REQUIRED'){
                            window.location.reload(true);
                        }
                        $(divOk).show('slow');
                        try{
                            //$('#descartarNE').closest('.modal.fade.in').modal('hide');
                        }catch(e){                            
                            $('#modalNotaEntrega button[data-dismiss=\"modal\"]').first().click();
                            alert('Error al cerrar. Presione F5 para recargar');                            
                        }
                    },
                    error: function (x,y,z){
                        // TODO: MANEJAR LOS ERRORES
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        console.log($(errorDiv).parent().next());
                        var htmlError = $(errorDiv).parent().next().html();
                        $(divError).html(htmlError);
                        $(divOk).hide('slow');
                        $(divError).show('slow');
                    }
                });
                return false;
            });
            
            $('#descartarNE').click(function(e){                
                e.stopPropagation();
                e.preventDefault();
                
                var datos = 'id='+$('#idCarga').val()+'&ajax=';
                var url = $(this).attr('url')+'&id='+$('#NotaEntrega_id').val()+'&ajax=';
                
                $.ajax({
                    url:url,
                    type:'POST',
                    data: datos,
                    success:function(html) {
                      //TODO: SE HACE ALGO CON EL html?
                        console.log(html);
                        if(isModal){                                                        
                            try{
                                //$('#descartarNE').closest('.modal.fade.in').modal('hide');
                            }catch(e){
                                $('#modalNotaEntrega button[data-dismiss=\"modal\"]').first().click();
                                alert('Error al cerrar. Presione F5 para recargar');                        
                            }        
                        }else{
                            window.location = '".Yii::app()->createUrl('notaEntrega/admin')."';
                        }
                                              
                    },
                    error:function(x,y,z) {
                      // TODO: MOSTRAR LOS ERRORES
                      console.log(x);
                      alert(x.responseText);
                      return false;
                    }
                });
                return true;
            });
        }
        
        function recalcularTotal(){
            let st = (isNaN($('#spanSubTotal').html()))?           0 : $('#spanSubTotal').html();
            let d1 = (isNaN($('#NotaEntrega_descuento1').val()))? 0 : $('#NotaEntrega_descuento1').val();
            let d2 = (isNaN($('#NotaEntrega_descuento2').val()))? 0 : $('#NotaEntrega_descuento2').val();
            //let spanTotal = (parseFloat(st,2) * (parseFloat(d1,2) / 100)) * (parseFloat(st,2) * (parseFloat(d2,2) / 100));
            let spanTotal = parseFloat(st,2) - (parseFloat(st,2) * (parseFloat(d1,2) / 100));
            spanTotal = spanTotal - (parseFloat(spanTotal,2) * (parseFloat(d2,2) / 100));
            console.log(spanTotal);
            $('#spanTotal').html(parseFloat(spanTotal,2).toFixed(2));
            
        }
        
        function showView(param){
            $('#errorNotaPedido').hide('slow');
            $('#okNotaPedido').hide('slow');
    
            console.log('clSelected '+param);
            var datos = 'id='+param;
            $.ajax({
                url:'".$this->createUrl('Cliente/ViewFicha')."',
                type: 'Get',
                data: datos,
                success: function (html) {
                    $('#fichaCliente').html(html);
                },
                error: function (x,y,z){
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    console.log($(errorDiv).parent().next());
                    var htmlError = $(errorDiv).parent().next().html();
                    $('#errorNotaPedido').html(htmlError);
                    $('#errorNotaPedido').show('slow');
                }
            });
        }        
        
    ",CClientScript::POS_READY);
?>
<div class="content-fluid">
    <div class="row-fluid bordeRedondo" style="font-style: italic;margin-bottom: 5px;" >
        <div class="span3">
            Id Cliente :<span ><?=$model->idCliente?></span>
        </div>
        <div class="span3">
            Nro Pedio: <span ><?=($model->oNotaPedido)? $model->oNotaPedido->nroCombrobante.'-'.$model->oNotaPedido->dig : '' ?></span>
        </div>
        <div class="span3">
            Camion <span ><?=$model->idCamion?></span>
        </div>
        <div class="span3">
            Nota de Entrega Nº <span ><?=$model->id?></span>
        </div>
    </div>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span8" id="fichaCliente">

        </div>
        <div class="span4">
            <div id="nuevoNotaEntrega" style="">

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    'id'=>'nota-entrega-form',
                    'enableAjaxValidation'=>false,
                )); ?>

                <?php echo $form->hiddenField($model,'id'); ?>
                <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorNotaEntrega"></div>
                <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okNotaEntrega">Datos Guardados Correctamente !</div>

                <?php echo $form->errorSummary($model); ?>
                <div class="content-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                            <?php
                            $this->widget('editable.EditableDetailView', array(
                                'data'       => $model,

                                //you can define any default params for child EditableFields
                                'url'        => $this->createUrl('NotaEntrega/SaveFieldAsync'), //common submit url for all fields
                                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                                //'apply' => false, //you can turn off applying editable to all attributes

                                'attributes' => array(
                                    array( //select loaded from database
                                        'name' => 'nroPuestoVenta',
                                        'editable' => array(
                                            'type'   => 'select',
                                            'mode'      => ($is_modal)? 'inline' : 'popup',
                                            'apply' => $model->esEditable(),
                                            'source' => Editable::source(PuntoDeVenta::model()->activos()->findAll(), 'num', 'title'),
                                        )
                                    ),
                                    array( //select loaded from database
                                        'name' => 'idCamion',
                                        'editable' => array(
                                            'type'   => 'select',
                                            'mode'      => ($is_modal)? 'inline' : 'popup',
                                            'apply' => $model->esEditable(),
                                            'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre'),
                                        )
                                    ),
                                    array(
                                        'name' => 'fecha',
                                        'editable' => array(
                                            'type'       => 'date',
                                            'mode'      => ($is_modal)? 'inline' : 'popup',
                                            'format'      => ($model->esEditable())? 'yyyy-mm-dd' : 'dd/mm/yyyy', //format in which date is expected from model and submitted to server
                                            'viewformat' => 'dd/mm/yyyy',
                                            'apply' => $model->esEditable(),
                                        )
                                    ),

                                )
                            ));

                            echo ' <a class="btn btn-primary print-button" target="_blank" style="margin-top: 15px;" id="idPdf" print="'.Yii::app()->createUrl('notaEntrega/Pdf', array('id'=>$model->id,'print'=>true)).'"  href="'.Yii::app()->createUrl('notaEntrega/Pdf',array('id'=>$model->id)).'">Imprimir</a>';
                            if ($model->idNotaPedido == null && $model->estado != NotaEntrega::iCerrada){
                                echo CHtml::link('Guardar y Cerrar <i class="icon-white"></i>',Yii::app()->createUrl('NotaEntrega/saveFieldAsync', array('id'=>$model->id)),
                                    array('class'=>'btn btn-success','id' => 'cerrar','style' => 'float:left;margin-right:5px;margin-top:15px;')
                                );
                            }
                            ?>

                        </div>
                    </div>

                </div>

                <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>

</div>

<?php
    if ($model->esEditable()) {
        $this->widget('ArticuloVentaCSearch',
            array(
                'js_comp' => 'ProdNotaEntrega_idArtVenta',
                'is_modal' => $is_modal,
                'model' => $modelProd,
                'modelParent' => $model,
                'idDivNuevo' => "nuevoProdNotaEntrega",
                'idMasProd' => "masProdNotaEntrega",
                'cantidad' => 'ProdNotaEntrega_cantidad',
                'precioUnit' => 'ProdNotaEntrega_precioUnitario',
                'idArtVenta' => 'ProdNotaEntrega_idArtVenta',
                'descripcion' => 'ProdNotaEntrega_descripcion',
                'idPaquete' => '',
                'id_form' => 'prod-nota-credito-form',
                'urlSaveAsync' => Yii::app()->createUrl('ProdNotaEntrega/SaveAsync'),
                'id_grid' => 'prod-nota-entrega-grid',
                'aHiddenFields' => array(
                    'idNotaEntrega',
                    'idArtVenta'
                ),
                'conBuscador' => true,
                'alicuota' => $model->getAlicuotaItem(),
            )
        );
    }

?>



<?php

    $this->widget('bootstrap.widgets.TbGridView', array(
        'id' => 'prod-nota-entrega-grid',
        'dataProvider' => $aProdNotaEntrega->search(),
        //'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");refreshComprobanteDetails();}',
        'afterAjaxUpdate' => 'js:function(id,data){afterAjaxUpdateNE();try{refreshComprobanteDetails()}catch(e){};return true;}',
        'template' => '{items}',
        'columns' => array(
            array(
                'name' => 'codigo',
                'value' => '$data->oArtVenta->codigo',
                'htmlOptions' => array('style' => ''),
            ),
            'descripcion',
            array(
                'class' => 'editable.EditableColumn',
                'name' => 'precioUnitario',
                'value' => '"$ ".number_format($data->precioUnitario,"2",".","")',
                'editable' => array(
                    'apply' => '$data->oNotaEntrega->estado != NotaEntrega::iCerrada ',
                    'url' => $this->createUrl('ProdNotaEntrega/SaveFieldAsync'),
                    'placement' => 'right',
                    'mode'      => ($is_modal)? 'inline' : 'popup',
                    'display' => 'js: function(value, sourceData) {
                                value = value.replace("$",""); //
                                if(!isNaN(value) > 0){
                                    $(this).html("$ "+parseFloat(value).toFixed(2));
                                }else{
                                    console.log("Value is NaN:"+value);
                                    $(this).html("$ 0.00");
                                }								
							}',
                    // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                    'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                    // ACTUALIZO LA GRILLA
                    'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update(\'prod-nota-entrega-grid\');
                                    recalcularTotal();
                                }catch(e){
                                    console.log(e);
                                };
                             }',

                ),
                'htmlOptions' => array('style' => 'text-align:right;'),
                'headerHtmlOptions' => array('style' => 'text-align:right;'),

            ),

            array(
                'class' => 'editable.EditableColumn',
                'name' => 'cantidad',
                'headerHtmlOptions' => array('style' => 'width: 60px;text-align:right'),
                'editable' => array(    //editable section
                    'url'        => $this->createUrl('ProdNotaEntrega/SaveFieldAsync'),
                    'placement'  => 'right',
                    'mode'      => ($is_modal)? 'inline' : 'popup',
                    'apply' => '$data->oNotaEntrega->estado != NotaEntrega::iCerrada ',
                    'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update(\'prod-nota-entrega-grid\');
                                    recalcularTotal();
                                }catch(e){
                                    console.log(e);
                                };
                             }',
                    // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                    'onShown' => 'js: function(ev,editable) {                                 
                                     setTimeout(function() {
                                        editable.input.$input.select();
                                      },0);
                                }',
                )
            ),
            array(
                'name' => 'importe',
                'value' => '"$".$data->importe',
                'htmlOptions' => array('style' => 'text-align:right;'),
                'headerHtmlOptions' => array('style' => 'text-align:right;'),
            ),
            /*
            array(
                'name' => ,
                'value' => ,
                'htmlOptions' => array('style' => 'text-align:right;'),
                'headerHtmlOptions' => array('style' => 'text-align:right;'),
            ),
            */

            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}', /*{update} | */
                'buttons'=>array(
                    /*
                    'update' => array(
                        'label' => 'Modificar',
                        'url'=>'Yii::app()->controller->createUrl("ProdNotaEntrega/update", array("id"=>$data->id))',
                        'options'=>array('target'=>'_blank'),
                    ),
                     *
                     */

                    'delete' => array(
                        'label' => 'Borrar Item Solo de la Nota De Entrega',
                        'url'=>'Yii::app()->controller->createUrl("ProdNotaEntrega/delete", array("id"=>$data->id,"ajax"=>""))',
                        'visible' => '$data->oNotaEntrega->estado != NotaEntrega::iCerrada ',
                        'click'=>'function(e){
                                e.stopImmediatePropagation();
                                e.stopPropagation(); 
                                if (confirm("Borrar Item Solo de la Nota De Entrega?")){
                                        $.post($(this).attr("href"), function(data) {                                            
                                            $.fn.yiiGridView.update("prod-nota-entrega-grid"); 
                                            recalcularTotal();                                                                                                                                       
                                        }).fail(function(x) {
                                            console.error( "error" );
                                        });                                                                                
                                        return false;
                                }else{
                                    return false;                                
                                }
                             }',
                    ),
                ),
            ),

        ),
    ));

?>

<?php $form2=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'nota-entrega-form2',
    'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('NotaEntrega/SaveAsync'),
    'htmlOptions' => array(
        //'class' => 'SaveAsyncForm',
    ),
)); ?>

<div class="content-fluid bordeRedondo">
    <div class="row-fluid">
        <div class="span2">
            <?php

            if ($model->esEditable()) {
                echo CHtml::link('Descartar N.E.','#',
                    array('class'=>'btn btn-danger','id' => 'descartarNE','url' => Yii::app()->createUrl('notaEntrega/delete'),'style' => 'float:left;margin-top: 25px;' ));
            }
            ?>
        </div>

        <div class="span2" style="text-align: right;">
            <b><?=CHtml::label('Subtotal:','spanSubTotal')?></b>
            <b> $<span id="spanSubTotal"><?=$model->SubTotal ?></span></b>
            <?php
                echo $form2->hiddenField($model,'id');
                echo $form2->hiddenField($model,'clase');
                echo $form2->hiddenField($model,'fecha');
                echo $form2->hiddenField($model,'nroPuestoVenta');
            ?>
        </div>
        <div class="span1">
            <?php

                echo CHtml::label('Descuen.1','descuento1');
                echo CHtml::hiddenField('NotaEntrega_descuento1',$model->descuento1);
                $this->widget('editable.EditableField', array(
                    'model'       => $model,
                    'mode'      => ($is_modal)? 'inline' : 'popup',
                    'apply'      => $model->esEditable(),
                    'attribute'   => 'descuento1',
                    'url'         => $this->createUrl('NotaEntrega/SaveFieldAsync'),
                    'placement'   => 'right',
                    'display' => 'js: function(value, sourceData) {
                                    if(value > 0){
                                        $(this).html(parseFloat(value).toFixed(2)+" %");								
                                    }else{
                                        $(this).html("0 %");							
                                    }
                                }',
                    // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                    'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                    // ACTUALIZO LA GRILLA
                    'success' => "js: function(response, newValue) {
                                    console.log(newValue);                                    
                                    $('#NotaEntrega_descuento1').val(newValue).promise().done(function(){
                                        console.log('NotaEntrega_descuento1 promise'+$('#NotaEntrega_descuento1').val());
                                        refreshComprobanteDetails();
                                        recalcularTotal();                                    
                                    });
                                 }",
                ));


            ?>
        </div>
        <div class="span1">
            <?php

                echo CHtml::label('Descuen.2.','descuento2');
                echo CHtml::hiddenField('NotaEntrega_descuento2',$model->descuento2);
                $this->widget('editable.EditableField', array(
                    'model'       => $model,
                    'mode'        => ($is_modal)? 'inline' : 'popup',
                    'apply'       => $model->esEditable(),
                    'attribute'   => 'descuento2',
                    'url'         => $this->createUrl('NotaEntrega/SaveFieldAsync'),
                    'placement'   => 'right',
                    'display' => 'js: function(value, sourceData) {
                                    if(value > 0){
                                        $(this).html(parseFloat(value).toFixed(2)+" %");								
                                    }else{
                                        $(this).html("0 %");							
                                    }
                                }',
                    // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                    'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                    // ACTUALIZO LA GRILLA
                    'success' => "js: function(response, newValue) {
                                    
                                    
                                    $('#NotaEntrega_descuento2').val(newValue).promise().done(function(){
                                        
                                        refreshComprobanteDetails();
                                        recalcularTotal();
                                    });                                    
                                 }",
                ));


            ?>
        </div>
        <div class="span2" style="text-align: right;padding-top: 25px;font-size: x-large;"><b>Total $<span id="spanTotal"><?=$model->totalFinal?></span></b></div>
        <div class="span2">
            <?php
            if ($model->esEditable()) {
                echo CHtml::label('Imprimir sin Precio','impSinPrecio');
                $this->widget('editable.EditableField', array(
                    'type'      => 'select',
                    'model'       => $model,
                    'mode'        => ($is_modal)? 'inline' : 'popup',
                    'apply'     => ($model->estado < NotaEntrega::iCerrada), //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                    'attribute'   => 'impSinPrecio',
                    'url'         => $this->createUrl('NotaEntrega/SaveFieldAsync'),
                    'placement'   => 'right',
                    'source'    => NotaPedido::$aSiNo,

                    'htmlOptions' => array(
                        'style' => "float: margin-left: 5px;width:100%;text-align:center;",
                    ),
                ));
            }

            ?>
        </div>
        <div class="span2">

            <?php

            /*
            if ($model->esEditable()) {
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Cerrar Nota de Entrega',
                    'htmlOptions' => array(
                        'style' => "margin-top: 15px;",
                        'id' => 'guardarNE',
                    )
                ));
            }
            */
            ?>
        </div>
    </div>
</div>
<div class="modalLoading" ><!-- Place at bottom of page --></div>
<?php $this->endWidget(); ?>

