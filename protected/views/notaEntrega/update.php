<?php
$this->breadcrumbs=array(
    NotaEntrega::model()->getAttributeLabel('models')=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Ver '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.NotaEntrega::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);


?>

<h2>Modificando <?php echo NotaEntrega::model()->getAttributeLabel('model').' #'.$model->id; ?></h2>

<div id="cuerpo"></div>
<?php

$this->renderPartial('_formFila',array(
    'model'=>$model,
    'aProdNotaEntrega' => $aProdNotaEntrega,
    'modelProd' => $modelProd,
    'ajaxMode' => true,
    'is_modal' => false,
));

?>

<?php
$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => false));
?>
