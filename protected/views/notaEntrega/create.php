<?php
/* @var $this NotaEntregaController */
/* @var $model NotaEntrega */

$this->breadcrumbs=array(
    NotaEntrega::model()->getAttributeLabel('models') =>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de NotaEntrega', 'url'=>array('admin')),
);
Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        var modoEdicion = false;
		// PARA PONER LAS AYUDAS
		try{
			$('label').tooltip({track: true});
			$('input').tooltip({track: true});
		}catch(e){
			console.warn(e);
		}
		
		// ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		$('#idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
                        
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		
		$('#idCliente').change(function (ev) {
		    getNotaEntrega($('#idCliente').val());
		});
		
		
    }); 
	
	function getNotaEntrega(idCliente){
	    var datos = 'idNotaPedido=&idCliente='+idCliente;
        $.ajax({
            url: '".$this->createUrl('notaEntrega/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                
                
                $('#cuerpo').html(html).promise().done(function(){
                   window.location = '".Yii::app()->createUrl('NotaEntrega/update')."&id='+$('#NotaEntrega_id').val();
                    
                });
                $('#cuerpo').find('#fecha').datepicker();
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorFactura').html(htmlError);
                //$('#errorFactura').show('slow');
                
                
            }
        });
	    
	}
",CClientScript::POS_READY);

?>

<h3>Creando <?=NotaEntrega::model()->getAttributeLabel('model')?></h3>

<div class="alert alert-block alert-error" style="display:none;" id="errorCLiente"></div>
<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>

<?php
echo CHtml::label('Id Cliente','idCliente');
echo CHtml::textField('idCliente','',array('title' => 'Presionar "+" para abrir el buscador'));
?>

    <div id="fichaCliente"></div>
    <div id="cuerpo" style="display: none"></div>

<?php
$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => true));
?>