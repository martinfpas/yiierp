<?php /** @var $model NotaEntrega */ ?>
<style>
    .copia{
        font-style: normal;
        height: 540px;
        display: block;
    }
    .ne-header{
        border-top: 0.05em dotted black;
        border-bottom: 0.05em dotted black;
    }
    .ne-header-titles{
        font-style: normal;
        margin-top:10px;
        margin-bottom: 5px;
    }
    .items-grid{
        height: 380px;
        display: block;
    }
    .ne-footer{
        border-top: 0.05em dotted black;border-bottom: 0.05em dotted black;
        line-height:1.7;
    }

    #prod-nota-entrega-grid2  th {
        line-height:1.5;
    }

    #prod-nota-entrega-grid2 td {
        line-height:normal;
    }

    #prod-nota-entrega-grid  th {
        line-height:1.7;
    }

    #prod-nota-entrega-grid  td {
        line-height:normal;
    }

</style>
<div class="copia" style="">
    <div class="content-fluid ne-header-titles">
        <div class="row-fluid" style="font-style: normal;margin-bottom: 5px;" >
            <div class="span6" style="text-align: right;">
                <span >|<?=$model->clase?></span>|
            </div>
            <div class="span4"  style="text-align: right;">
                PRESUPUESTO
            </div>
            <div class="span2"  style="text-align: right;">
                Nº <span ><?=$model->nroComprobante?></span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align:left;">
            .
            </div>
            <div class="span3" style="text-align: center;padding-left: 5px">
                <span >Documento </span>
            </div>
            <div class="span2">
                Fecha
            </div>
            <div class="span3">
                <span ><?=ComponentesComunes::fechaFormateada($model->fecha)?></span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align: left; ">
            .
            </div>
            <div class="span3" style="text-align: center;padding-left: 5px;">
                <span >No Válido </span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align: left;">
            .
            </div>
            <div class="span3" style="text-align: center;padding-left: 5px;">
                <span >Como factura</span>
            </div>
        </div>
    </div>


    <div class="content-fluid ne-header" STYLE="">
        <div class="row-fluid">
            <div class="span2" style="text-align: right;">
                Cliente :
            </div>
            <div class="span8" >
                <span ><?=strtoupper($model->oCliente->title)?></span>
            </div>
            <!--
            <div class="span1" >
                -
            </div>
            -->
            <div class="span2" >
                <span ><?=ucfirst(strtolower($model->oCliente->id))?></span>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2" style="text-align: right;">
                :
            </div>
            <div class="span6" >
                <span ><?=strtoupper($model->oCliente->direccion)?></span>
            </div>
        </div>
        <div class="row-fluid">
            <div   class="span2" style="text-align: right;">
                :
            </div>
            <div class="span10" >
                <span ><?=$model->oCliente->cp?> <?=strtoupper($model->oCliente->localidad)?> (<?=$model->oCliente->provincia?>)</span>
            </div>
        </div>

    </div>

    <div class="items-grid" style="">
        <?php
        $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'prod-nota-entrega-grid2',
            'dataProvider'=>$aProdNotaEntrega->searchWP(),
            'template' => '{items}',
            'columns'=>array(
                array(
                    'name' => 'codigo',
                    'value' => '$data->oArtVenta->codigo',
                    'htmlOptions' => array('style' => 'text-align:left;width:80px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:left;width:80px;'),
                ),
                array(
                    'name' => 'descripcion',
                    'value' => 'strtoupper($data->descripcion)',
                    'htmlOptions' => array('style' => 'text-align:left;width:350px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:left;width:350px;'),
                ),

                array(
                    'name' => 'cantidad',
                    'header' => 'cant.',
                    'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                ),
                array(
                    'name' => 'precioUnitario',
                    'value' => 'number_format($data->precioUnitario,2,",",".")',
                    'header' => 'P.Unitario',
                    'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                ),
                array(
                    'name' => 'importe',
                    'value' => 'number_format($data->importe,2,",",".")',
                    'htmlOptions' => array('style' => 'text-align:right;width:130px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:130px;'),
                ),
                /*
                array(
                    'name' => ,
                    'value' => ,
                    'htmlOptions' => array('style' => 'text-align:right;'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;'),
                ),
                */

            ),
        ));

        ?>
    </div>

    <div class="content-fluid ne-footer" style="">
        <div class="row-fluid" style="font-family: 'Courier New'!important;text-align: right">
            <div class="span4">Subtotal: <?=number_format($model->SubTotal,2,",",".") ?></div>
            <div class="span4">Descuento <?=number_format($model->descuentoTotal,2,",",".")?></div>
            <div class="span4">Importe Total <span id="spanTotal"><?=number_format($model->totalFinal,2,",",".")?></span></div>
        </div>
    </div>
</div>

<div class="copia" style="margin-top: 25px;">

    <div class="content-fluid">
        <div class="row-fluid ne-header-titles" style="" >
            <div class="span6" style="text-align: right;">
                <span >|<?=$model->clase?></span>|
            </div>
            <div class="span4"  style="text-align: right;">
                PRESUPUESTO
            </div>
            <div class="span2"  style="text-align: right;">
                Nº <span ><?=$model->nroComprobante?></span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align:left;">
                .
            </div>
            <div class="span3" style="text-align: center;">
                <span >Documento </span>
            </div>
            <div class="span2">
                Fecha
            </div>
            <div class="span3">
                <span ><?=ComponentesComunes::fechaFormateada($model->fecha)?></span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align: left; ">
                .
            </div>
            <div class="span3" style="text-align: center;">
                <span >No Válido </span>
            </div>
        </div>
        <div class="row-fluid"  style="text-align: right;" >
            <div class="span4" style="text-align: left;">
                .
            </div>
            <div class="span3" style="text-align: center;">
                <span >Como factura</span>
            </div>
        </div>
    </div>


    <div class="content-fluid ne-header" STYLE="">
        <div class="row-fluid">
            <div class="span2" style="text-align: right;">
                Cliente :
            </div>
            <div class="span8" >
                <span ><?=strtoupper($model->oCliente->title)?></span>
            </div>
            <!--
            <div class="span1" >
                -
            </div>
            -->
            <div class="span2" >
                <span ><?=strtoupper($model->oCliente->id)?></span>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2" style="text-align: right;">
                :
            </div>
            <div class="span6" >
                <span ><?=strtoupper($model->oCliente->direccion)?></span>
            </div>
        </div>
        <div class="row-fluid">
            <div   class="span2" style="text-align: right;">
                :
            </div>
            <div class="span10" >
                <span ><?=$model->oCliente->cp?> <?=strtoupper($model->oCliente->localidad)?> (<?=$model->oCliente->provincia?>)</span>
            </div>
        </div>

    </div>

    <div class="items-grid" style="">
        <?php
        $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'prod-nota-entrega-grid',
            'dataProvider'=>$aProdNotaEntrega->searchWP(),
            'template' => '{items}',
            'columns'=>array(
                array(
                    'name' => 'codigo',
                    'value' => '$data->oArtVenta->codigo',
                    'htmlOptions' => array('style' => 'text-align:left;width:80px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:left;width:80px;'),
                ),
                array(
                    'name' => 'descripcion',
                    'value' => 'strtoupper($data->descripcion)',
                    'htmlOptions' => array('style' => 'text-align:left;width:600px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:left;width:600px;'),
                ),

                array(
                    'name' => 'cantidad',
                    'header' => 'cant.',
                    'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                ),

            ),
        ));

        ?>
    </div>

    <div class="content-fluid ne-footer" style="">
        <div class="row-fluid" style="text-align: right">
            
        </div>
    </div>
</div>

