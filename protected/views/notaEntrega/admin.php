<?php
/* @var $this NotaEntregaController */
/* @var $model NotaEntrega */

$this->breadcrumbs=array(
    NotaEntrega::model()->getAttributeLabel('models') => array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva '.NotaEntrega::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#nota-entrega-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de <?=NotaEntrega::model()->getAttributeLabel('models')?></h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">

</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'nota-entrega-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:80px;text-align:center;'),
        ),

        array(
            'name' => 'nroComprobante',
            'header' => 'nro comp.',
            'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
        ),

        array(
            'name' => 'idCliente',
            'value' => '$data->oCliente->Title',
            'htmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','Title'),
        ),

        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
        ),

        array(
            'name' => 'totalFinal',
			'value' => '"$".number_format($data->totalFinal,2,".","")',
			'htmlOptions' => array('style' => 'text-align:right;width:90px;'),
		),
		//'idNotaPedido',

		/*
		'nroPuestoVenta',
		'idCamion',
		'totalFinal',
		'enviada',
		'recibida',
		'fecha',
		'descuento1',
		'descuento2',
		'nroViajante',
		'observaciones',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
        */
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}',
		),

	),
)); ?>
