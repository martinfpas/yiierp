<?php
/* @var $this ClasifMovimientoCuentasController */
/* @var $model ClasifMovimientoCuentas */

$this->breadcrumbs=array(
	ClasifMovimientoCuentas::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.ClasifMovimientoCuentas::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=ClasifMovimientoCuentas::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>