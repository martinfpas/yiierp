<?php
$this->breadcrumbs=array(
	ClasifMovimientoCuentas::model()->getAttributeLabel('models') =>array('admin'),
	$model->codigo,
);

$this->menu=array(
	array('label'=>'Nuevo '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->codigo)),
	array('label'=>'Borrar '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.ClasifMovimientoCuentas::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=ClasifMovimientoCuentas::model()->getAttributeLabel('model') ?> #<?php echo $model->codigo; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'codigo',
		'Descripcion',

        array(
            'name' => 'Ingreso',
            'value' => ClasifMovimientoCuentas::$aSiNo[$model->Ingreso],
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
