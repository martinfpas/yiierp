
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoClasifMovimientoCuentas" id="masClasifMovimientoCuentas" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoClasifMovimientoCuentas" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'clasif-movimiento-cuentas-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ClasifMovimientoCuentas/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'clasif-movimiento-cuentas-grid',
			'idMas' => 'masClasifMovimientoCuentas',
			'idDivNuevo' => 'nuevoClasifMovimientoCuentas',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorClasifMovimientoCuentas"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okClasifMovimientoCuentas">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Ingreso',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'clasif-movimiento-cuentas-grid',
'dataProvider'=>$aClasifMovimientoCuentas->search(),
'columns'=>array(
		'codigo',
		'Descripcion',
		'Ingreso',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ClasifMovimientoCuentas/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ClasifMovimientoCuentas/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>