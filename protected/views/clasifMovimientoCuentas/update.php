<?php
$this->breadcrumbs=array(
	ClasifMovimientoCuentas::model()->getAttributeLabel('models') => array('admin'),
	ClasifMovimientoCuentas::model()->getAttributeLabel('model') => array('view','id'=>$model->codigo),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->codigo)),
	array('label'=>'Gestion de '.ClasifMovimientoCuentas::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=ClasifMovimientoCuentas::model()->getAttributeLabel('model') ?> <?php echo $model->codigo; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>