
/* @var $this ClasifMovimientoCuentasController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'ClasifMovimientoCuentas::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo ClasifMovimientoCuentas', 'url'=>array('create')),
	array('label'=>'Gestion de ClasifMovimientoCuentas', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>