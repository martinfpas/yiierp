<?php
/* @var $this ClasifMovimientoCuentasController */
/* @var $data ClasifMovimientoCuentas */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo),array('view','id'=>$data->codigo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ingreso')); ?>:</b>
	<?php echo CHtml::encode($data->Ingreso); ?>
	<br />


</div>