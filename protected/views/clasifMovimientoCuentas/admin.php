<?php
/* @var $this ClasifMovimientoCuentasController */
/* @var $model ClasifMovimientoCuentas */

$this->breadcrumbs=array(
	ClasifMovimientoCuentas::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva '.ClasifMovimientoCuentas::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#clasif-movimiento-cuentas-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?=ClasifMovimientoCuentas::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'clasif-movimiento-cuentas-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'codigo',
		'Descripcion',
        array(
            'name' => 'Ingreso',
            'value' => 'ClasifMovimientoCuentas::$aSiNo[$data->Ingreso]',
            'filter' => ClasifMovimientoCuentas::$aSiNo,
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
