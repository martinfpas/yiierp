<?php
/* @var $model FacturaProveedor */

if ($model->id > 0){
    $strModoEdicion = 'var modoEdicion = true;';
}else{
    $strModoEdicion = 'var modoEdicion = false;';
}

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScript('create4', "
    
    $strModoEdicion    
    
    $('#FacturaProveedor_id_Proveedor').change(function(){
        let id_Proveedor = $(this).val();        
        getProveedor(id_Proveedor);                 
                                    
    });
    
    /*
    $('#FacturaProveedor_id_Cuenta').change(function(){
        $('#guardarCabecera').focus();
    });
    */    
    
    $('#FacturaProveedor_id_Cuenta').on('select2:select', function () {
        console.log('guardarCabecera');
        $('#guardarCabecera').focus();
    });
    
    //$('#DetalleFacturaProv_idRubro').change(function(){
    $('#idRubro').change(function(){
        console.log('DetalleFacturaProv_idRubro change');
        $('#DetalleFacturaProv_idArticulo').select2('open');
    });
    $('#DetalleFacturaProv_idArticulo').change(function(){
        $('#DetalleFacturaProv_idMateriaPrima').select2('open');
        
    });
    
    $('#DetalleFacturaProv_idMateriaPrima').change(function(){
        $('#DetalleFacturaProv_descripcion').focus();        
    });
    
    $('#DetalleFacturaProv_idMateriaPrima').keyup(function(e){
        console.log(e);
        //$('#DetalleFacturaProv_descripcion').focus();        
    });
    
    function getProveedor(id_Proveedor){
        var datos = 'id='+id_Proveedor;
        $.ajax({
            url:'".$this->createUrl('proveedor/viewFicha')."',
            data: datos,
            type: 'GET',
            success: function (html){
                $('#idProveedor').html(html);
                $('#FacturaProveedor_Nro_Comprobante').focus();
                return false;                                    
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
                $('#idProveedor').html('');
            }                
        });
    }
    
    $('#FacturaProveedor_Fecha_Vencimiento').blur(function(e){
        e.stopPropagation();
        console.log('blur...');
        // ABRE EL COMBO PARA ELEJIR La Cuenta CUANDO PIERDE EL FOCO        
        //$('.select2-drop:eq(1)').select2('open');
        $('#FacturaProveedor_id_Cuenta').select2('open');
    });
    
    $(document).ready(function(){        
		
		$('#terminar').click(function(e){
		    e.stopPropagation();
		    e.preventDefault();
		    let datos = $('#factura-proveedor-form').serialize();	    
		    let id = $('#FacturaProveedor_id').val();
		    $.ajax({
                url:'".$this->createUrl('FacturaProveedor/Terminar',array('id'=>''))."'+id,
                data: datos,
                type: 'POST',
                success: function (html){
                    
                    window.location = '".$this->createUrl('FacturaProveedor/view',array('id'=>''))."'+id;
                    return false;                                    
                },
                error: function (x,y,z){
                    console.log(x.responseText);                
                
                    //TODO: MANEJAR EL ERROR                
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSummary');
                    //alert($(errorDiv).parent().next());
                    alert(respuesta[3].innerHTML);
                    
                }                
            });
		});
		
		let id_Proveedor = $('#FacturaProveedor_id_Proveedor').val();
		if(id_Proveedor > 0){
		    // TODO: VER COMO SE IMPLEMENTO EN GURU LA PRE CARGA DEL SELECT2
		    $('#s2id_FacturaProveedor_id_Proveedor span').html('".(($model->oProveedor != null)? $model->oProveedor->Title : '')."')
            getProveedor(id_Proveedor);		
        }
        
        /*
        // TODO: VER COMO SE IMPLEMENTO EN GURU LA PRE CARGA DEL SELECT2               
        $('#s2id_FacturaProveedor_id_Cuenta span').html('".(($model->oCuentaGastos != null)? $model->oCuentaGastos->denominacion : '') ."')
        */  
		
		// PARA PONER LAS AYUDAS		
		try{
			$('label').tooltip();
			$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		$('#FacturaProveedor_bPagoInmediado').change(function(e){
			if($(this).is(':checked')){
			    $('#terminar').show('slow');
			}else{
			    $('#terminar').hide('slow');
			}
		});
    });

",CClientScript::POS_READY);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'factura-proveedor-form',
    'enableAjaxValidation'=>false,
    'action' => (($model->isNewRecord)? '' : Yii::app()->createUrl('FacturaProveedor/SaveAsync')),
    'htmlOptions' => array(
        'class' => (($model->isNewRecord)? '' : 'SaveAsyncForm' ),
    ),
));


?>

    <div class="alert alert-block alert-error" style="display:none;" id="errorProveedor"></div>
    <div class="alert alert-block alert-success" style="display:none;" id="okProveedor">Datos Guardados Correctamente !</div>

    <div class="content-fluid">
        <div class="row-fluid">

            <div class="span6">
                <?php
                if(!$model->isNewRecord){
                    echo $form->hiddenField($model,'id');
                }
                echo $form->textFieldRow($model,'id_Proveedor',array('class'=>'span12 '));
                $this->widget('ext.select2.ESelect2',array(
                        'selector'=>'#FacturaProveedor_id_Proveedor',
                        'model' => $model,
                        'attribute' => 'id_Proveedor',
                        'options'=>array(
                            'ajax'=>array(
                                'url'=>Yii::app()->createUrl('proveedor/select2'),
                                'dataType'=>'json',
                                'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                'results'=>'js:function(data,page) { return {results: data}; }',
                            ),
                            'class' => 'span12',
                            'style' => 'margin-left:0px;',
                        ),
                    )
                );
                echo $form->error($model,'id_Proveedor');
                ?>
            </div>
            <div class="span2">
                Nro NP Emitida
            </div>
            <div class="span2">
                <?php
                echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12','maxlength'=>8,'tabindex' => 2,));
                ?>
            </div>
            <div class="span2">
                <?php
                echo $form->label($model,'TipoComprobante');
                echo $form->dropDownList($model,'TipoComprobante',FacturaProveedor::$aClase,array('tabindex' => 3,'class' => 'span12','empty' => 'Seleccionar'));
                echo $form->error($model,'TipoComprobante');
                ?>
            </div>
        </div>

        <div class="row-fluid">
            <div id="idProveedor" class="span6"></div>
            <div class="span2">
                <?php
                echo $form->labelEx($model,'FechaFactura',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->FechaFactura),
                    'name'=>'FacturaProveedor[FechaFactura]', // Cambiar 'NotaPedido por el modelo que corresponda
                    //'model' => $model,
                    //'attribute' => 'FechaFactura',
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 4
                    ),
                ));
                echo $form->error($model,'FechaFactura');

                echo $form->textFieldRow($model,'Clase',array('class'=>'span12','maxlength'=>2,'tabindex' => 7,'style' => 'display:inline;'));



                ?>
            </div>
            <div class="span2">
                <?php
                echo $form->textFieldRow($model,'Nro_Puesto_Vta',array('class'=>'span12','maxlength'=>2,'tabindex' => 5,));
                echo $form->textFieldRow($model,'Cant_alicuotas_iva',array('class'=>'span12','style' => '','maxlength'=>2,'tabindex' => 8,));
                ?>
            </div>
            <div class="span2">
                <?php
                echo $form->labelEx($model,'MesCarga',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> $model->MesCarga,
                    'name'=>'FacturaProveedor[MesCarga]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'model' => $model,
                    'attribute' => 'MesCarga',
                    'mask' => '99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 6
                    ),
                ));

                echo $form->labelEx($model,'Fecha_Vencimiento',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->Fecha_Vencimiento),
                    'name'=>'FacturaProveedor[Fecha_Vencimiento]', // Cambiar 'NotaPedido por el modelo que corresponda
                    //'model' => $model,
                    //'attribute' => 'Fecha_Vencimiento',
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 9
                    ),
                ));
                ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span2">
                <?php
                if($model->montoPagado == 0){
                    echo $form->labelEx($model,'bPagoInmediado',array());
                    echo $form->checkBox($model,'bPagoInmediado',array('class'=>'','maxlength'=>2,'tabindex' => 10,'style' => 'display:inline;'));
                }
                ?>
            </div>
            <div class="span3">
                <?php

                echo
                $form->textFieldRow($model,'id_Cuenta',array('class'=>'span12'));
                $this->widget('ext.select2.ESelect2',array(
                        'selector'=>'#FacturaProveedor_id_Cuenta',
                        'id'=>'id_Cuenta',
                        'options'=>array(
                            'ajax'=>array(
                                'url'=>Yii::app()->createUrl('CuentaGastos/select2'),
                                'dataType'=>'json',
                                'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                'results'=>'js:function(data,page) { return {results: data}; }',

                            ),
                            'initSelection'=>'js:function(element,callback){
                                var data={id:"'.$model->id_Cuenta.'",text:"'.(($model->oCuentaGastos != null)? $model->oCuentaGastos->denominacion : "" ).'"};
                                callback(data);
                            }',
                            'onSelect' => 'js:function(element,callback){
                                console.log("onSelect");
                            }',
                            'class' => 'span12',
                            'style' => 'margin-left:0px;',
                            'tabindex' => 11,
                        ),
                    )
                );
                ?>
            </div>
            <div class="span3">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'btn',
                    'label'=>$model->isNewRecord? 'Cargar Items' : 'Guardar cambios cabecera',
                    'htmlOptions' => array(
                        'style' => 'margin-top:25px;',
                        'id' => 'guardarCabecera',
                        'class' => 'span12',
                    ),
                )); ?>
            </div>
            <div class="span3">
                <?php
                if($model->montoPagado == 0) {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType' => 'link',
                        'type' => 'primary',
                        'label' => 'Terminar y computar pago',
                        'htmlOptions' => array(
                            'style' => 'margin-top:25px;',
                            'id' => 'terminar',
                            'name' => 'terminar',
                            'class' => 'span12',

                        ),
                    ));
                }
                ?>
            </div>

        </div>
    </div>
<?php $this->endWidget(); ?>