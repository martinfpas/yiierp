<?php
/* @var $this FacturaProveedorController */
/* @var $model FacturaProveedor */

$this->breadcrumbs=array(
	FacturaProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

//ComponentesComunes::print_array($model->attributes);

$this->menu=array(
	array('label'=>'Gestion de '.FacturaProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);

?>
<h3>Nueva <?=FacturaProveedor::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>


