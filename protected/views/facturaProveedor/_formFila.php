<style>
    #nuevoDetalleFacturaProv input,select,.select2-container {
        margin-right: 6px ;
    }
</style>
<div id="nuevoDetalleFacturaProv" style="">

	<?php


    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl."/js/tabindex.js");
    $cs->registerScriptFile($baseUrl."/js/SaveAsync.js");

    Yii::app()->clientScript->registerScript('general2',"
        function refreshComprobanteDetails(){
            
		    // TODO: MEJORAR LA SIGUIENTE LINEA PARA QUE idComprobante no esa necesario.
            var datos = 'id=".$model->idFacturaProveedor."';
            $.ajax({
                url:'".Yii::app()->createUrl('FacturaProveedor/Json')."',
                data: datos,
                type: 'GET',
                success: function (data){
                
                    let response = JSON.parse(data);
                   
                    let t = (isNaN(parseFloat(response.TotalNeto)))? 0 : parseFloat(response.TotalNeto); 
                    let s = (isNaN(parseFloat(response.Subtotal)))? 0 : parseFloat(response.Subtotal);
                   
                    $('#TotalFactura').html('$'+t.toFixed(2));
                 	$('#Subtotal').html('$'+s.toFixed(2)); 
                 	
                 	/*
                    $('#spanTotal').html(response.Total_Final);
                    $('#Ing_Brutos').html(response.Ing_Brutos);
                    $('#Otro_Iva').html(response.Otro_Iva);
                    $('#subtotalIva').html(response.subtotalIva);
                    $('#subtotalIva').html(response.subtotalIva);
                    $('#AlicuotaItem').html(response.AlicuotaItem);
                    */
                },
            });
        }
    ");

    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'detalle-factura-prov-form',

		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('DetalleFacturaProv/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'detalle-factura-prov-grid',
			'idMas' => 'masDetalleFacturaProv',
			'idDivNuevo' => 'nuevoDetalleFacturaProv',
		),
	));

    ?>

    <div class="content-fluid">
        <div class="row-fluid">
            <div class="span12">
                <?php echo $form->label($model,'idRubro',array('style' => 'float:left;width:12%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'idArticulo',array('style' => 'float:left;width:12%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'idMateriaPrima',array('style' => 'float:left;width:12%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'descripcion',array('style' => 'float:left;width:32%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'alicuota',array('style' => 'float:left;width:6%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'Precio',array('style' => 'float:left;width:10%;padding-left:5px;')); ?>
                <?php echo $form->label($model,'Cant',array('style' => 'float:left;width:4%;padding-left:5px;')); ?>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">
                <div>
                    <?php
                        echo CHtml::textField('idRubro','',array('style' => 'width:12%;','tabindexEs' => 1));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#idRubro',
                                'options'=>array(
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('rubro/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                        'results'=>'js:function(data,page) { return {results: data}; }',
                                    ),
                                    //'class' => 'span2',
                                    'style' => 'margin-left:0px;width:60px;',
                                ),
                            )
                        );
                        echo $form->textField($model,'idArticulo',array('style' => 'width:12%;','tabindex' => 2));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#DetalleFacturaProv_idArticulo',
                                'options'=>array(
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('articulo/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, id_rubro: $("#idRubro").val(), page_limit: 10, page: page}; }',
                                        'results'=>'js:function(data,page) { return {results: data}; }',
                                    ),
                                    //'class' => 'span2',
                                    'style' => 'margin-left:0px;width:60px;',
                                ),
                            )
                        );
                        echo $form->textField($model,'idMateriaPrima',array('style' => 'width:12%;','tabindex' => 3));
                        $this->widget('ext.select2.ESelect2',array(
                                'selector'=>'#DetalleFacturaProv_idMateriaPrima',
                                'options'=>array(
                                    'ajax'=>array(
                                        'url'=>Yii::app()->createUrl('MateriaPrima/select2'),
                                        'dataType'=>'json',
                                        'data'=>'js:function(term,page) { return {q: term, id_rubro: $("#idRubro").val(),id_articulo: $("#DetalleFacturaProv_idArticulo").val(), page_limit: 10, page: page}; }',
                                        'results'=>'js:function(data,page) { return {results: data}; }',
                                    ),
                                    //'class' => 'span2',
                                    'style' => 'margin-left:0px;width:60px;',
                                ),
                            )
                        );

                        echo $form->textField($model,'descripcion',array('maxlength'=>40,'style' => 'width:30%;','tabindex' => 4));
                        echo $form->dropDownList($model,'alicuota',FacturaProveedor::$aIvaAlicuotasMontos,array('empty' => 'Seleccionar','style' => 'width:6%;','tabindex' => 5));
                        echo $form->textField($model,'precioUnitario',array('style' => 'width:8%;','tabindex' => 6));
                        echo $form->textField($model,'cantidadRecibida',array('style' => 'width:4%;','tabindex' => 7));
                        echo $form->hiddenField($model,'idFacturaProveedor');
                    ?>
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType'=>'submit',
                        'type'=>'primary',
                        'label'=> 'Guardar',
                        'htmlOptions' => array(
                            'id' => 'cargarItem',
                        ),

                    )); ?>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span3">



            </div>
        </div>
    </div>
    <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorDetalleFacturaProv"></div>

	<?php echo $form->errorSummary($model); ?>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okDetalleFacturaProv">Datos Guardados Correctamente !</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'detalle-factura-prov-grid',
'dataProvider'=>$aDetalleFacturaProv->search(),
'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");refreshComprobanteDetails();}',
'enablePagination' => false,
'template' => '{items}',
'htmlOptions' => array(
    'style' => 'padding-top:0px;'
),
'columns'=>array(
        array(
            'name' => 'id_rubro',
            'value' => '($data->oArticulo != null)? $data->oArticulo->id_rubro : ""',
            'header' => 'Cod.Rub.',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'idArticulo',
            'value' => '($data->oArticulo != null)? $data->oArticulo->id_articulo : ""',
            'header' => 'Cod.Art.',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        'idMateriaPrima',
        'descripcion',

        array(
            'name' => 'alicuota',
            'value' => 'number_format($data->alicuota,2)."%"',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'cantidadRecibida',
            'header' => 'Cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'precioUnitario',
            'value' => '"$".number_format($data->precioUnitario,2)',
            //'header' => '',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
		/*
		'nroItem',

		'idFacturaProveedor',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
            'buttons'=>array(
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("DetalleFacturaProv/update", array("id"=>$data->id))',
                    'options'=>array('target'=>'_blank'),
                ),
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("DetalleFacturaProv/delete", array("id"=>$data->id))',
                ),
            ),
		),
	),
)); ?>


