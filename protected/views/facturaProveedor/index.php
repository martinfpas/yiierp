
/* @var $this FacturaProveedorController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'FacturaProveedor::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo FacturaProveedor', 'url'=>array('create')),
	array('label'=>'Gestion de FacturaProveedor', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>