<?php
/* @var $this FacturaProveedorController */
/* @var $data FacturaProveedor */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_Comprobante')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_Comprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_Proveedor')); ?>:</b>
	<?php echo CHtml::encode($data->id_Proveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaFactura')); ?>:</b>
	<?php echo CHtml::encode($data->FechaFactura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Clase')); ?>:</b>
	<?php echo CHtml::encode($data->Clase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_Puesto_Vta')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_Puesto_Vta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nograv')); ?>:</b>
	<?php echo CHtml::encode($data->Nograv); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('TotalNeto')); ?>:</b>
	<?php echo CHtml::encode($data->TotalNeto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('MesCarga')); ?>:</b>
	<?php echo CHtml::encode($data->MesCarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Subtotal')); ?>:</b>
	<?php echo CHtml::encode($data->Subtotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descuento')); ?>:</b>
	<?php echo CHtml::encode($data->Descuento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Iva')); ?>:</b>
	<?php echo CHtml::encode($data->Iva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('IngBrutos')); ?>:</b>
	<?php echo CHtml::encode($data->IngBrutos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Rg3337')); ?>:</b>
	<?php echo CHtml::encode($data->Rg3337); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('FechaCarga')); ?>:</b>
	<?php echo CHtml::encode($data->FechaCarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_Cuenta')); ?>:</b>
	<?php echo CHtml::encode($data->id_Cuenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('PercepIB')); ?>:</b>
	<?php echo CHtml::encode($data->PercepIB); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ImpGanancias')); ?>:</b>
	<?php echo CHtml::encode($data->ImpGanancias); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('TipoComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->TipoComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cai')); ?>:</b>
	<?php echo CHtml::encode($data->Cai); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Fecha_Vencimiento')); ?>:</b>
	<?php echo CHtml::encode($data->Fecha_Vencimiento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Cant_alicuotas_iva')); ?>:</b>
	<?php echo CHtml::encode($data->Cant_alicuotas_iva); ?>
	<br />

	*/ ?>

</div>