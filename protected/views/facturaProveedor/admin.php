<?php
/* @var $this FacturaProveedorController */
/* @var $model FacturaProveedor */

$this->breadcrumbs=array(
	FacturaProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva '.FacturaProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#factura-proveedor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?=FacturaProveedor::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'factura-proveedor-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'Nro_Comprobante',
        array(
            'name' => 'TipoComprobante',
            'value' => 'FacturaProveedor::$aClase[$data->TipoComprobante]',
            'filter' => FacturaProveedor::$aClase,
            //'header' => 'Cuenta',
            //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
        array(
            'filter' => CHtml::listData(Proveedor::model()->todos()->findAll(),'id','Title'),
            'name' => 'id_Proveedor',
            'value' => '$data->oProveedor->Title',
            'header' => 'Proveedor',
            /*
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            */
        ),

        array(
            'name' => 'TotalNeto',
            'value' => 'number_format($data->TotalNeto,2)',
            'header' => 'Importe',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),

        array(
            'name' => 'FechaFactura',
            'value' => 'ComponentesComunes::fechaFormateada($data->FechaFactura)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'name' => 'MesCarga',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'name' => 'montoPagado',
            'value' => 'number_format($data->montoPagado,2)',
            'header' => 'Pagado',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
		/*
		'Nograv',
		'TotalNeto',
		'Subtotal',
		'Descuento',
		'Iva',
		'IngBrutos',
		'Rg3337',
		'FechaCarga',
		'id_Cuenta',
		'PercepIB',
		'ImpGanancias',

		'Cai',
		'Fecha_Vencimiento',
		'Cant_alicuotas_iva',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
