

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12','maxlength'=>8)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'id_Proveedor',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'FechaFactura',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Clase',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Nro_Puesto_Vta',array('class'=>'span12','maxlength'=>1)); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Nograv',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'TotalNeto',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'MesCarga',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Subtotal',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Descuento',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Iva',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'IngBrutos',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Rg3337',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'FechaCarga',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'id_Cuenta',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'PercepIB',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'ImpGanancias',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'TipoComprobante',array('class'=>'span12')); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Cai',array('class'=>'span12','maxlength'=>15)); ?>
</div>
                    <div class='span4'><?php echo $form->textFieldRow($model,'Fecha_Vencimiento',array('class'=>'span12')); ?>
</div>
                    </div>
                    <div class="row-fluid">
                    <div class='span4'><?php echo $form->textFieldRow($model,'Cant_alicuotas_iva',array('class'=>'span12')); ?>
</div>
                    <div class='span4'>
                    <div class="form-actions">
                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>Yii::t('application','Buscar'),
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>

