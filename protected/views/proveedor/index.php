
/* @var $this ProveedorController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Proveedors',
);

$this->menu=array(
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
	array('label'=>'Gestion de Proveedor', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>