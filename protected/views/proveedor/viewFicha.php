<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 21/6/2019
 * Time: 13:09
 */
?>
<style>
    .bordeRedondo{
        border: #dddddd 1px solid;
        padding: 10px;
        border-radius: 0.7em;
    }
    .table {
        margin-bottom: 0px;
    }
</style>
<div class="span12 bordeRedondo" style="margin-left: 0px;">
    <?php
        $this->widget('bootstrap.widgets.TbDetailView',array(
            'data'=>$model,
            'attributes'=>array(
                array(
                    'name' => 'Title',
                    'type'=>'raw',
                    'value' => CHtml::link($model->id.' :: '.$model->Title,Yii::app()->createUrl('proveedor/view',array('id' => $model->id))),
                ),
                'cuit',
                'direccion',
                array(
                    'name' => 'cp',
                    'header' => 'Localidad',
                    'value' => $model->LocalidadYProv,
                ),
            ),
        ));
    ?>
</div>
