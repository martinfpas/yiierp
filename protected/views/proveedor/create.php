<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */

$this->breadcrumbs=array(
	'Proveedors'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Proveedor', 'url'=>array('admin')),
);
?>

<h1>Nuevo Proveedor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>