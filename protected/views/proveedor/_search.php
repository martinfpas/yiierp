<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nbreFantasia',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>60)); ?>
    </div>


    <div class="span4">
		<?php echo $form->textFieldRow($model,'cuit',array('class'=>'span12','maxlength'=>11)); ?>

		<?php echo $form->textFieldRow($model,'ivaResponsable',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'direccion',array('class'=>'span12','maxlength'=>60)); ?>
    </div>


    <div class="span4">
		<?php echo $form->textFieldRow($model,'pisoDto',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'cp',array('class'=>'span12','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'zp',array('class'=>'span12','maxlength'=>4)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'tel',array('class'=>'span12','maxlength'=>12)); ?>
    </div>
    <div class="span4" >
		<?php echo $form->textFieldRow($model,'fax',array('class'=>'span12','maxlength'=>12)); ?>
    </div>
    <div class="span4" style="padding-top:  24px">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Buscar',

        )); ?>
    </div>
    <div class="span4"></div>

</div>


<?php $this->endWidget(); ?>
