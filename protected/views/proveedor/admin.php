<?php
/* @var $this ProveedorController */
/* @var $model Proveedor */

$this->breadcrumbs=array(
	'Proveedores'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#proveedor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Proveedores</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'proveedor-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
            'name' => 'id',
			'htmlOptions' => array('style' => 'width:50px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:50px;text-align:right;'),
		),
		//'nbreFantasia',
		'razonSocial',
		'cuit',
		//
        //'ivaResponsable',
		'direccion',
        'Localidad',
        array(
            'name' => 'saldo',
            'value' => '"$".number_format($data->saldo,2,".","")',
            'htmlOptions' => array('style' => 'width:70px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:70px;text-align:right;'),
        ),
        /*

		'cp',
		'zp',
		'tel',
		'fax',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}'
		),
	),
)); ?>
