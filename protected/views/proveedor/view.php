<?php
/* @var $model Proveedor */

$this->breadcrumbs=array(
	'Proveedores'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Proveedor', 'url'=>array('create')),
	array('label'=>'Modificar Proveedor', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM Proveedores', 'url'=>array('admin')),
    array('label'=>'Imprimir Sobre', 'url'=>array('PdfCarta', 'id'=>$model->id),'linkOptions'=>array('target' => '_blank')),
);
?>

<h3>Ver Proveedor #<?php echo $model->id; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6 left">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'nbreFantasia',
                        'htmlOptions' => array('style' => 'font-weigth:bold'),
                    ),
                    array(
                        'name' => 'razonSocial',
                        'htmlOptions' => array('style' => 'font-weigth:bold'),
                    ),
                    'cuit',
                    array(
                        'name' => 'ivaResponsable',
                        'value' => $model->oIvaResponsable->nombre,
                    ),
                    'direccion',
                    'pisoDto',
                ),
            )); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'cp',
                        'header' => 'Localidad',
                        'value' => $model->LocalidadYProv,
                    ),
                    'zp',
                    'tel',
                    'fax',
                    'idTipo',
                    array(
                        'name' => 'saldo',
                        'value' => "$".number_format($model->saldo,2,',','.'),
                        'htmlOptions' => array('style' => 'font-weigth:bold'),
                    ),
                    'obs',
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>
</div>

<?php
$ctacte = new VCtaCteProv('search');
$ctacte->unsetAttributes();
$ctacte->id_Proveedor = $model->id;
/** @var $data VCtaCteProv */

$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'vcta-cte-prov-grid',
    'dataProvider'=>$ctacte->search(),
    'ajaxUrl'=>'VCtaCteProv/admin',
    'columns'=>array(
        array(
            'name' => 'TipoComprobante',
            'value' => 'VCtaCteProv::$aClase[$data->TipoComprobante]',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        'Nro_Comprobante',
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'TotalNeto',
            'value' => '"$".number_format(((VCtaCteProv::$aFactorMultip[$data->TipoComprobante]) * $data->TotalNeto),2)',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),


        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}',
            'buttons'=>array(
                'view' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("ComprobanteProveedor/view", array("id"=>$data->cp_id))',
                    'options'=>array('target'=>'_blank'),
                ),
            )
        ),
    ),
)); ?>