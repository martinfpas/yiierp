<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'proveedor-form',
	'enableAjaxValidation'=>false,
)); 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

Yii::app()->clientScript->registerScript('search', "
	function updateZpList(cp,numzp){
		//
		var data = 'cp='+cp
		$.ajax({
			url:'".Yii::app()->createUrl('CodigoPostal/ZpList')."',
			type: 'POST',
			data : data,
			success:function(html){
				$('#Proveedor_zp').html(html);
				if(numzp !== undefined){
				    console.log(numzp);
				    $('#Proveedor_zp').val(numzp);
				}
			},
			error:function(x,y,z){
				console.err(x);
			},
		});

	};
");

?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorProveedor"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okProveedor">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'nbreFantasia',array('class'=>'span12','maxlength'=>50,'tabindex'=>'1')); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>60,'tabindex'=>'2')); ?>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'direccion',array('class'=>'span12','maxlength'=>60,'tabindex'=>'3')); ?>
		</div>
		<div class="span1">
			<?php echo $form->textFieldRow($model,'pisoDto',array('class'=>'span12','maxlength'=>8,'tabindex'=>'4')); ?>
		</div>
		<div class="span1">
			<?php
				echo $form->labelEx($model,'cp');
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
						'name'=>'Proveedor[cp]',
						'id'=>'Proveedor_cp',
                        'value' => $model->cp,
                        'sourceUrl'=>$this->createUrl('codigoPostal/CodigosAsync'),
						// additional javascript options for the autocomplete plugin
						'options'=>array(
								'minLength'=>'2',
								'select' => 'js:function(event, ui){
							console.log(ui.item["value"]);
							updateZpList(ui.item["value"],ui.item["zp"]);
						}',
						),

						'htmlOptions'=>array(
								'style'=>'height:20px;',
								'tabindex'=>'5',
								'class'=>'span12',

						),
				));

			?>
		</div>
		<div class="span2">
			<?php
				echo $form->labelEx($model,'zp');//,array('class'=>'span12','maxlength'=>4,'tabindex'=>'6'));
				echo $form->dropDownList($model,'zp',CHtml::listData(CodigoPostal::model()->findAll(),'zp','IdZp'),array('class'=>'span12','tabindex'=>'6','empty'=>'---'));
			?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'cuit',array('class'=>'span12','maxlength'=>11,'tabindex'=>'7')); ?>
		</div>
	</div>
	<div class="row-fluid">
        <div class="span6">
            <?php echo $form->textAreaRow($model,'obs',array('rows'=>6, 'cols'=>50, 'class'=>'span12','tabindex'=>'8')); ?>
        </div>

		<div class="span2">
			<?php
                echo $form->labelEx($model,'ivaResponsable');
                echo $form->dropDownList($model,'ivaResponsable',CHtml::listData(CategoriaIva::model()->todas()->findAll(),'id_categoria','nombre'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'9'));
            ?>

            <?php echo $form->textFieldRow($model,'saldo',array('class'=>'span12','tabindex'=>'12')); ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'tel',array('class'=>'span12','maxlength'=>12,'tabindex'=>'10')); ?>
            <?php
                echo $form->textFieldRow($model,'idTipo',array('class'=>'span12','tabindex'=>'13'));
            ?>
        </div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'fax',array('class'=>'span12','maxlength'=>12,'tabindex'=>'11')); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                )
            )); ?>
        </div>

    </div>



</div>


<?php $this->endWidget(); ?>
