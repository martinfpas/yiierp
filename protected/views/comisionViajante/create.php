<?php
/* @var $this ComisionViajanteController */
/* @var $model ComisionViajante */

$this->breadcrumbs=array(
	'Comision Viajantes'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ComisionViajante', 'url'=>array('admin')),
);
?>

<h1>Creando ComisionViajante</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>