
/* @var $this ComisionViajanteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Comision Viajantes',
);

$this->menu=array(
	array('label'=>'Nuevo ComisionViajante', 'url'=>array('create')),
	array('label'=>'Gestion de ComisionViajante', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>