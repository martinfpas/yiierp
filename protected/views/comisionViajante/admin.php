<?php
/* @var $this ComisionViajanteController */
/* @var $model ComisionViajante */

$this->breadcrumbs=array(
	'Comision Viajantes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo ComisionViajante', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#comision-viajante-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Comision Viajantes</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'comision-viajante-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
        array(
            'name' => 'numeroViajante',
            'header' => 'Viajante',
            'value' => '$data->oViajante->IdTitulo',
        ),
        array(
            'name' => 'fechaInicio',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaInicio)',
            //'header' => '',
        ),
        array(
            'name' => 'fechaFinal',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaFinal)',
            //'header' => '',
        ),
    
        array(
            'name' => 'porcentaje',
            'value' => 'number_format($data->porcentaje,2)."%"',
            //'header' => '',
        ),
        array(
            'name' => 'importeTotal',
            'value' => '"$".number_format($data->importeTotal,2,",","")',
        ),
        array(
            'name' => 'fechaPago',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaPago)',
        ),
        array(
            'name' => 'importePago',
            'value' => '"$".number_format($data->importePago,2,",",".")',
        ),
        'modoPago',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
