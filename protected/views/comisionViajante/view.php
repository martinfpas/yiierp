<?php
/** @var  $model ComisionViajante */
$this->breadcrumbs=array(
	'Comision Viajantes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ComisionViajante', 'url'=>array('create')),
	array('label'=>'Modificar ComisionViajante', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ComisionViajante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM ComisionViajante', 'url'=>array('admin')),
);
?>

<h1>Ver ComisionViajante #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
        array(
            'name' => 'Viajante',
			'value' => $model->oViajante->IdTitulo,
		),
        array(
            'name' => 'fechaInicio',
            'value' => ComponentesComunes::fechaFormateada($model->fechaInicio),
            //'header' => '',
        ),
        array(
            'name' => 'fechaFinal',
            'value' => ComponentesComunes::fechaFormateada($model->fechaFinal),
            //'header' => '',
        ),

        array(
            'name' => 'porcentaje',
            'value' => $model->porcentaje.'%',
            //'header' => '',
        ),
        array(
            'name' => 'importeTotal',
            'value' => '$'.number_format($model->importeTotal,2,',',''),
            //'header' => '',
        ),

        array(
            'name' => 'fechaPago',
            'value' => ComponentesComunes::fechaFormateada($model->fechaPago),
            //'header' => '',
        ),
        array(
            'name' => 'importePago',
            'value' => '$'.number_format($model->importePago,2,',','.'),
            //'header' => '',
        ),
		'modoPago',

),
)); ?>
