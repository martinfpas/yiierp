<?php
$this->breadcrumbs=array(
	'Comision Viajantes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ComisionViajante', 'url'=>array('create')),
	array('label'=>'Ver ComisionViajante', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ComisionViajante', 'url'=>array('admin')),
);
?>

	<h1>Modificando ComisionViajante <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>