<?php
/* @var $this ComisionViajanteController */
/* @var $data ComisionViajante */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroViajante')); ?>:</b>
	<?php echo CHtml::encode($data->numeroViajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaInicio')); ?>:</b>
	<?php echo CHtml::encode($data->fechaInicio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaFinal')); ?>:</b>
	<?php echo CHtml::encode($data->fechaFinal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeTotal')); ?>:</b>
	<?php echo CHtml::encode($data->importeTotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaPago')); ?>:</b>
	<?php echo CHtml::encode($data->fechaPago); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('importePago')); ?>:</b>
	<?php echo CHtml::encode($data->importePago); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modoPago')); ?>:</b>
	<?php echo CHtml::encode($data->modoPago); ?>
	<br />

	*/ ?>

</div>