<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'comision-viajante-form',
	'enableAjaxValidation'=>false,
));

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorComisionViajante"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okComisionViajante">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
    <div class="row-fluid">

		<div class="span4">
			<?php
                echo $form->labelEx($model,'numeroViajante',array());
                echo $form->dropDownList($model,'numeroViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','tabindex'=>'1'));?>
		</div>
		<div class="span2">
			<?php

                echo $form->labelEx($model,'fechaInicio',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaInicio),
                    'name'=>'ComisionViajante[fechaInicio]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 2
                    ),
                ));
                echo $form->error($model,'fechaInicio');
            ?>
		</div>
		<div class="span2">
			<?php
                echo $form->labelEx($model,'fechaFinal',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaFinal),
                    'name'=>'ComisionViajante[fechaFinal]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 3
                    ),
                ));
                echo $form->error($model,'fechaFinal');
             ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'porcentaje',array('class'=>'span12','tabindex' => 4)); ?>
		</div>
        <div class="span2">
            <?php echo $form->textFieldRow($model,'importeTotal',array('class'=>'span12','tabindex' => 5)); ?>
        </div>
    </div>
    <div class="row-fluid">
		<div class="span2">
			<?php
                echo $form->labelEx($model,'fechaPago',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaPago),
                    'name'=>'ComisionViajante[fechaPago]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 6
                    ),
                ));
                echo $form->error($model,'fechaPago');
            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importePago',array('class'=>'span12','tabindex' => 7)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'modoPago',array('class'=>'span12','maxlength'=>10,'tabindex' => 8)); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
