<?php
$this->breadcrumbs=array(
	'Detalle Viaticoses'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo DetalleViaticos', 'url'=>array('create')),
	array('label'=>'Modificar DetalleViaticos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar DetalleViaticos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de DetalleViaticos', 'url'=>array('admin')),
);
?>

<h1>Ver DetalleViaticos #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'numeroViajante',
		'numeroViatico',
		'importe',
		'concepto',
		'fechaGasto',
		'numeroItem',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
