<?php
/* @var $this DetalleViaticosController */
/* @var $model DetalleViaticos */

$this->breadcrumbs=array(
	'Detalle Viaticoses'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de DetalleViaticos', 'url'=>array('admin')),
);
?>

<h1>Creando DetalleViaticos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>