<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'numeroViajante',array('class'=>'span12')); ?>
    </div>

    <div class="span4">
		<?php echo $form->textFieldRow($model,'numeroViatico',array('class'=>'span12')); ?>
    </div>
</div>
  <div class="row-fluid">
       <div class="span4">
		<?php echo $form->textFieldRow($model,'importe',array('class'=>'span12')); ?>
       </div>
      <div class="span4">
		<?php echo $form->textFieldRow($model,'concepto',array('class'=>'span12','maxlength'=>40)); ?>
      </div>
       <div class="span4">
		<?php echo $form->textFieldRow($model,'fechaGasto',array('class'=>'span12')); ?>
       </div>
   </div>
  <div class="row-fluid">
     <div class="span4">
		<?php echo $form->textFieldRow($model,'numeroItem',array('class'=>'span12')); ?>
     </div>
    <div class="span4" style="padding-top: 24px">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
    </div>
  </div>
<?php $this->endWidget(); ?>
