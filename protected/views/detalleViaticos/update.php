<?php
$this->breadcrumbs=array(
	'Detalle Viaticoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo DetalleViaticos', 'url'=>array('create')),
	array('label'=>'Ver DetalleViaticos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM DetalleViaticos', 'url'=>array('admin')),
);
?>

	<h1>Modificando DetalleViaticos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>