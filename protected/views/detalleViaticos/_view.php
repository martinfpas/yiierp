<?php
/* @var $this DetalleViaticosController */
/* @var $data DetalleViaticos */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroViajante')); ?>:</b>
	<?php echo CHtml::encode($data->numeroViajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroViatico')); ?>:</b>
	<?php echo CHtml::encode($data->numeroViatico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importe')); ?>:</b>
	<?php echo CHtml::encode($data->importe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('concepto')); ?>:</b>
	<?php echo CHtml::encode($data->concepto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaGasto')); ?>:</b>
	<?php echo CHtml::encode($data->fechaGasto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroItem')); ?>:</b>
	<?php echo CHtml::encode($data->numeroItem); ?>
	<br />


</div>