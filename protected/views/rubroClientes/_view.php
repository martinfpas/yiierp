<?php
/* @var $this RubroClientesController */
/* @var $data RubroClientes */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_rubro')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_rubro),array('view','id'=>$data->id_rubro)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />


</div>