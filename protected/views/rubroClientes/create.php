<?php
/* @var $this RubroClientesController */
/* @var $model RubroClientes */

$this->breadcrumbs=array(
	'Rubro Clientes'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

<h1>Creando Rubro Clientes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>