<?php
$this->breadcrumbs=array(
	'Rubro Clientes'=>array('index'),
	$model->id_rubro=>array('view','id'=>$model->id_rubro),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ', 'url'=>array('create')),
	array('label'=>'Ver ', 'url'=>array('view', 'id'=>$model->id_rubro)),
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

	<h1>Modificando Rubro Clientes <?php echo $model->id_rubro; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>