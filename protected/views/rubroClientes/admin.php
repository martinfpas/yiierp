<?php
/* @var $this RubroClientesController */
/* @var $model RubroClientes */

$this->breadcrumbs=array(
	'Rubro Clientes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#rubro-clientes-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Rubro Clientes</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'rubro-clientes-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id_rubro',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
