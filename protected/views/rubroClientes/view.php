<?php
$this->breadcrumbs=array(
	'Rubros Clientes'=>array('admin'),
	$model->id_rubro,
);

$this->menu=array(
	array('label'=>'Nuevo ', 'url'=>array('create')),
	array('label'=>'Modificar ', 'url'=>array('update', 'id'=>$model->id_rubro)),
	array('label'=>'Borrar ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_rubro),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

<h1>Ver Rubro Clientes #<?php echo $model->id_rubro; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_rubro',
		'nombre',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
