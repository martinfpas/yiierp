
/* @var $this RubroClientesController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Rubro Clientes',
);

$this->menu=array(
	array('label'=>'Nuevo RubroClientes', 'url'=>array('create')),
	array('label'=>'Gestion de RubroClientes', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>