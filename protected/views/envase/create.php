<?php
/* @var $this EnvaseController */
/* @var $model Envase */

$this->breadcrumbs=array(
	'Envases'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>

<h1>Creando Envase</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>