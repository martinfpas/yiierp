
/* @var $this EnvaseController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Envases',
);

$this->menu=array(
	array('label'=>'Nuevo Envase', 'url'=>array('create')),
	array('label'=>'Gestion de Envase', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>