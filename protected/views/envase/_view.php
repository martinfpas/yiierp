<?php
/* @var $this EnvaseController */
/* @var $data Envase */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id_envase')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_envase),array('view','id'=>$data->id_envase)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />


</div>