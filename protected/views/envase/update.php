<?php
$this->breadcrumbs=array(
	'Envases'=>array('index'),
	$model->id_envase=>array('view','id'=>$model->id_envase),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
	array('label'=>'Ver ', 'url'=>array('view', 'id'=>$model->id_envase)),
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

	<h1>Modificando Envase <?php echo $model->id_envase; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>