<?php
$this->breadcrumbs=array(
	'Envases'=>array('admin'),
	$model->id_envase,
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
	array('label'=>'Modificar', 'url'=>array('update', 'id'=>$model->id_envase)),
	array('label'=>'Borrar', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_envase),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

<h1>Ver Envase #<?php echo $model->id_envase; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_envase',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
