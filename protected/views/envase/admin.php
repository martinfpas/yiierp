<?php
/* @var $this EnvaseController */
/* @var $model Envase */

$this->breadcrumbs=array(
	'Envases'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#envase-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Envases</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'envase-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id_envase',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
