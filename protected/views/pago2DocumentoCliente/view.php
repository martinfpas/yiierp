<?php
$this->breadcrumbs=array(
	'Pago2 Documento Clientes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Pago2DocumentoCliente', 'url'=>array('create')),
	array('label'=>'Modificar Pago2DocumentoCliente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Pago2DocumentoCliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Pago2DocumentoCliente', 'url'=>array('admin')),
);
?>

<h1>Ver Pago2DocumentoCliente #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idPago',
		'idComprobante',
		'idNotaEntrega',
		'montoSaldado',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
