<?php
/* @var $this Pago2DocumentoClienteController */
/* @var $model Pago2DocumentoCliente */

$this->breadcrumbs=array(
	'Pago2 Documento Clientes'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Pago2DocumentoCliente', 'url'=>array('admin')),
);
?>

<h1>Nuevo Pago2DocumentoCliente</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>