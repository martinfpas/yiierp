
/* @var $this Pago2DocumentoClienteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Pago2 Documento Clientes',
);

$this->menu=array(
	array('label'=>'Nuevo Pago2DocumentoCliente', 'url'=>array('create')),
	array('label'=>'Gestion de Pago2DocumentoCliente', 'url'=>array('admin')),
);
?>

<h1>Pago2 Documento Clientes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
