<?php
$this->breadcrumbs=array(
	'Pago2 Documento Clientes'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Pago2DocumentoCliente', 'url'=>array('create')),
	array('label'=>'Ver Pago2DocumentoCliente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Pago2DocumentoCliente', 'url'=>array('admin')),
);
?>

	<h1>Modificando Pago2DocumentoCliente <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>