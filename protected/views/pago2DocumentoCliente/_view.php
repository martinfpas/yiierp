<?php
/* @var $this Pago2DocumentoClienteController */
/* @var $data Pago2DocumentoCliente */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPago')); ?>:</b>
	<?php echo CHtml::encode($data->idPago); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaEntrega')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaEntrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('montoSaldado')); ?>:</b>
	<?php echo CHtml::encode($data->montoSaldado); ?>
	<br />


</div>