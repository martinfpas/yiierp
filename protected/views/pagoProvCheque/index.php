
/* @var $this PagoProvChequeController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"PagoProvCheque::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo PagoProvCheque', 'url'=>array('create')),
	array('label'=>'ABM PagoProvCheque', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>