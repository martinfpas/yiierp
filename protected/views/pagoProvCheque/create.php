<?php
/* @var $this PagoProvChequeController */
/* @var $model PagoProvCheque */

$this->breadcrumbs=array(
	PagoProvCheque::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.PagoProvCheque::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=PagoProvCheque::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>