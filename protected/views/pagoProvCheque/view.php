<?php
$this->breadcrumbs=array(
	PagoProvCheque::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProvCheque::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.PagoProvCheque::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.PagoProvCheque::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM '.PagoProvCheque::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=PagoProvCheque::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idCheque3ro',
		'monto',
		'idPagoProv',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
