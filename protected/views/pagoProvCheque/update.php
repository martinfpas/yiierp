<?php
$this->breadcrumbs=array(
	PagoProvCheque::model()->getAttributeLabel('models') => array('admin'),
	PagoProvCheque::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProvCheque::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.PagoProvCheque::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM '.PagoProvCheque::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=PagoProvCheque::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>