
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoEmpresa" id="masEmpresa" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoEmpresa" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'empresa-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('Empresa/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'empresa-grid',
			'idMas' => 'masEmpresa',
			'idDivNuevo' => 'nuevoEmpresa',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorEmpresa"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okEmpresa">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'demominacion',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'actividad',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'publica',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'email2',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'calle',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'numero',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoPostal',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'zonaPostal',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'paginaWeb',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'empresa-grid',
'dataProvider'=>$aEmpresa->search(),
'columns'=>array(
		'id',
		'demominacion',
		'nombreFantasia',
		'actividad',
		'publica',
		'email',
		/*
		'email2',
		'calle',
		'numero',
		'codigoPostal',
		'zonaPostal',
		'paginaWeb',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("Empresa/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Empresa/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>