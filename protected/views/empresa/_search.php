<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'demominacion',array('class'=>'span12','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'actividad',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'publica',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'email2',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'calle',array('class'=>'span12','maxlength'=>30)); ?>

		<?php echo $form->textFieldRow($model,'numero',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'codigoPostal',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'zonaPostal',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'paginaWeb',array('class'=>'span12','maxlength'=>60)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
