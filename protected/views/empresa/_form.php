<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'empresa-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorEmpresa"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okEmpresa">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'demominacion',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'actividad',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'publica',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'email2',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'calle',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'numero',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoPostal',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'zonaPostal',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'paginaWeb',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
