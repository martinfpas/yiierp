<?php
$this->breadcrumbs=array(
	'Empresas'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Empresa', 'url'=>array('create')),
	array('label'=>'Modificar Empresa', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Empresa', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Empresa', 'url'=>array('admin')),
);
?>

<h1>Ver Empresa #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'demominacion',
		'nombreFantasia',
		'actividad',
		'publica',
		'email',
		'email2',
		'calle',
		'numero',
		'codigoPostal',
		'zonaPostal',
		'paginaWeb',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
