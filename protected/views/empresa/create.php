<?php
/* @var $this EmpresaController */
/* @var $model Empresa */

$this->breadcrumbs=array(
	'Empresas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Empresa', 'url'=>array('admin')),
);
?>

<h1>Creando Empresa</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>