<?php
/* @var $this EmpresaController */
/* @var $data Empresa */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('demominacion')); ?>:</b>
	<?php echo CHtml::encode($data->demominacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreFantasia')); ?>:</b>
	<?php echo CHtml::encode($data->nombreFantasia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('actividad')); ?>:</b>
	<?php echo CHtml::encode($data->actividad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publica')); ?>:</b>
	<?php echo CHtml::encode($data->publica); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email2')); ?>:</b>
	<?php echo CHtml::encode($data->email2); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('calle')); ?>:</b>
	<?php echo CHtml::encode($data->calle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::encode($data->numero); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoPostal')); ?>:</b>
	<?php echo CHtml::encode($data->codigoPostal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zonaPostal')); ?>:</b>
	<?php echo CHtml::encode($data->zonaPostal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paginaWeb')); ?>:</b>
	<?php echo CHtml::encode($data->paginaWeb); ?>
	<br />

	*/ ?>

</div>