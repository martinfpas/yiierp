<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

<div class="row-fluid">
    <div class="span1">
        <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span3">
		<?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12')); ?>
    </div>
    <div class="span3">
		<?php
            echo $form->labelEx($model,'id_rubro');
            echo $form->dropDownList($model,'id_rubro',CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','descripcion'),array('class'=>'span12','empty' => 'Todos'));
        ?>
    </div>
    <div class="span1">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
            'htmlOptions' => array(
                'class'=>'span12',
                'style' => 'margin-top:25px;',
            ),
		)); ?>
    </div>
</div>


<?php $this->endWidget(); ?>
