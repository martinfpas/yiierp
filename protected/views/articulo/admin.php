<?php
/* @var $this ArticuloController */
/* @var $model Articulo */

$this->breadcrumbs=array(
	'Articulos'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Articulo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#articulo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Articulos</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'articulo-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id_articulo',
		'descripcion',
		array(
				'name' => 'id_rubro',
				'value' => '$data->oRubro->descripcion',
				'filter' => CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','descripcion'),
				'htmlOptions' => array('style' => '','empty' => 'Todos'),
		),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}'
		),
	),
)); ?>
