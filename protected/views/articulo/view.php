<?php
$this->breadcrumbs=array(
	'Articulos'=>array('admin'),
	$model->id_articulo,
);

$this->menu=array(
	array('label'=>'Modificar Articulo', 'url'=>array('update', 'id'=>$model->id_articulo)),
	array('label'=>'Borrar Articulo', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_articulo),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Articulo', 'url'=>array('admin')),
);
?>

<h1>Ver Articulo #<?php echo $model->id_articulo; ?> del rubro <?php echo $model->oRubro->descripcion; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_articulo',
		'descripcion',
		array(
				'name' => 'id_rubro',
				'value' => $model->oRubro->descripcion,
				'htmlOptions' => array('style' => ''),
		),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
