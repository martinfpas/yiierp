<?php
/* @var $this ArticuloController */
/* @var $model Articulo */

$this->breadcrumbs=array(
	'Articulos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Articulo', 'url'=>array('admin')),
);
?>

<h1>Creando Articulo</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>