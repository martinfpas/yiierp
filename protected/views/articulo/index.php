
/* @var $this ArticuloController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Articulos',
);

$this->menu=array(
	array('label'=>'Nuevo Articulo', 'url'=>array('create')),
	array('label'=>'Gestion de Articulo', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>