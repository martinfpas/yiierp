<?php
$this->breadcrumbs=array(
	'Articulos'=>array('index'),
	$model->id_articulo=>array('view','id'=>$model->id_articulo),
	'Modificando',
);

$this->menu=array(

	array('label'=>'Gestion de Articulo', 'url'=>array('admin')),
);
?>

	<h1>Modificando Articulo <?php echo $model->id_articulo; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>