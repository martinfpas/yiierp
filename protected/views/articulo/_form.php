<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/tabindex.js");
	
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'articulo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorArticulo"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okArticulo">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
		<div class="span3">
            <?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12','tabindex'=>'1')); ?>
        </div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','tabindex'=>'2')); ?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_rubro',array()); 
				echo $form->dropDownList($model,'id_rubro',CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),array('empty'=>'--seleccionar--','tabindex'=>'3'));
			?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
