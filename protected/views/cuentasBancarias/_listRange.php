<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 1/12/2019
 * Time: 14:10
 *
 * @var $model MovimientoCuentasBanc
 */

    Yii::app()->clientScript->registerScript('print-button', "
        $('.print-button').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            let serial = $('#movimientoCBSearch-form').serialize();
            serial = serial.replace('r=cuentasBancarias%2Fview&',''); 
            let href = $(this).attr('href')+'&'+serial;
            console.log(href);
            let print = $(this).attr('print')+'&'+serial;
            $('#download-form').attr('action',href);
            $('#print-form').attr('action',print);        
            $('#modalPrint').modal();
        });
    ");
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'action' => Yii::app()->createUrl($this->route,array('id'=>$model->idCuenta)),
        'method' => 'POST',
        'id'=>'movimientoCBSearch-form',
        'htmlOptions' => array(
            'style' => 'margin-bottom:0px;',
        ),
    )); ?>

    <div class="content-fluid">
        <div class="row-fluid">
            <div class='span2'>
                <?php
                echo $form->hiddenField($model,'idCuenta');
                echo CHtml::label('Fecha Desde','fechaDesde',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaDesde),
                    'name'=>'MovimientoCuentasBanc[fechaDesde]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                    ),
                ));
                ?>

            </div>
            <div class='span2'>
                <?php
                echo CHtml::label('Fecha Hasta', 'fechaHasta',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaHasta),
                    'name'=>'MovimientoCuentasBanc[fechaHasta]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                    ),
                ));
                ?>

            </div>
            <div class='span1'>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => Yii::t('application', 'Buscar'),
                    'htmlOptions' => array(
                        'style' => 'margin-top:20px',
                    ),
                )); ?>
            </div>
            <div class="span1">
                <?php
                //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
                echo CHtml::link('Exportar',Yii::app()->createUrl('MovimientoCuentasBanc/listaPdf'),
                    array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;margin-top:20px;', 'print' => Yii::app()->createUrl('MovimientoCuentasBanc/listaPdf', array('print'=>true)) ));
                ?>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>