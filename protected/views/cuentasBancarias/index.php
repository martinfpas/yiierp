
/* @var $this CuentasBancariasController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Cuentas Bancarias',
);

$this->menu=array(
	array('label'=>'Nuevas CuentasBancarias', 'url'=>array('create')),
	array('label'=>'Gestion de CuentasBancarias', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>