<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id_banco',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id_sucursal',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'observaciones',array('class'=>'span12','maxlength'=>50)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'codigoTipoCta',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'saldo',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'fechaSaldo',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'codigoMoneda',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'fechaApertura',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'fechaCierre',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'cbu',array('class'=>'span12')); ?>
    </div>
    <div class="span4" style="padding-top: 24px">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
