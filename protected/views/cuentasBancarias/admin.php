<?php
/* @var $this CuentasBancariasController */
/* @var $model CuentasBancarias */

$this->breadcrumbs=array(
    CuentasBancarias::model()->getAttributeLabel('models')=>array('admin'),
    'Gestion',
);

$this->menu=array(
    array('label'=>'Nueva '.CuentasBancarias::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle('slow');
    return false;
});
$('.search-form form').submit(function(){
    $('#cuentas-bancarias-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h3>Gestion de <?=CuentasBancarias::model()->getAttributeLabel('models') ?></h3>


<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cuentas-bancarias-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'id',
			'htmlOptions' => array('style' => 'width:50px;'),
		),
        'numero',
        array(
            'name' => 'id_banco',
            'value' => '($data->oBanco != null)? $data->oBanco->nombre : ""',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(Banco::model()->todos()->findAll(),'id','nombre'),
        ),
        array(
            'name' => 'id_sucursal',
            'value' => '($data->oSucursal != null)? $data->oSucursal->nombre : ""',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(SucursalBanco::model()->todas()->findAll(),'id','nombre'),

        ),

		array(
		    'name' => 'codigoTipoCta',
            'value' => '($data->oCodigoTipoCta != null)? $data->oCodigoTipoCta->nombre : ""',
            'filter' => CHtml::listData(TipoCuenta::model()->findAll(),'id','nombre'),
        ),

        array(
            'name' => 'saldo',
			'value' => 'number_format($data->saldo,2,",",".")',
			'htmlOptions' => array('style' => 'text-align:right;'),
		),
        array(
            'name' => 'fechaSaldo',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaSaldo)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'), // ESTILO VALORES
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),  // ESTILO CABECERA
        ),



    /*

    'codigoMoneda',
    'fechaApertura',
    'fechaCierre',
    'cbu',
    */
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
