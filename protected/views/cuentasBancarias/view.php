<?php
/** @var $model CuentasBancarias */
$this->breadcrumbs=array(
    CuentasBancarias::model()->getAttributeLabel('models') =>array('admin'),
    $model->id,
);

$this->menu=array(
    array('label'=>'Nueva '.CuentasBancarias::model()->getAttributeLabel('model'), 'url'=>array('create')),
    array('label'=>'Modificar '.CuentasBancarias::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
    array('label'=>'Borrar '.CuentasBancarias::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
    array('label'=>'ABM '.CuentasBancarias::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);


?>

<h3>Ver <?=CuentasBancarias::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'id_banco',
                        'value' => ($model->oBanco != null)? $model->oBanco->nombre : '',
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'id_sucursal',
                        'value' => ($model->oSucursal)? $model->oSucursal->nombre : '',
                    ),
                    'observaciones',

                    array(
                        'name' => 'codigoTipoCta',
                        'value' => $model->oCodigoTipoCta->nombre,
                        'htmlOptions' => array('style' => ''),
                    ),
                    'saldo',
                ),
            )); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'fechaSaldo',
                        'value' => ComponentesComunes::fechaFormateada($model->fechaSaldo),
                    ),
                    'codigoMoneda',
                    array(
                        'name' => 'fechaApertura',
                        'value' => ComponentesComunes::fechaFormateada($model->fechaApertura),
                    ),

                    array(
                        'name' => 'fechaCierre',
                        'value' => ComponentesComunes::fechaFormateada($model->fechaCierre),
                    ),


                    'cbu',
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>
</div>
<?php
//


$this->renderPartial('_listRange',array(
    'model' => $aMovimientoCuentasBanc,

));
?>
<?php echo $this->renderPartial('_formFila', array('aMovimientoCuentasBanc' => $aMovimientoCuentasBanc)); ?>
<?php $this->widget('PrintWidget') ?>