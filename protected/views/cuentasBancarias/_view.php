<?php
/* @var $this CuentasBancariasController */
/* @var $data CuentasBancarias */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_banco')); ?>:</b>
	<?php echo CHtml::encode($data->id_banco); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_sucursal')); ?>:</b>
	<?php echo CHtml::encode($data->id_sucursal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoTipoCta')); ?>:</b>
	<?php echo CHtml::encode($data->codigoTipoCta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saldo')); ?>:</b>
	<?php echo CHtml::encode($data->saldo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaSaldo')); ?>:</b>
	<?php echo CHtml::encode($data->fechaSaldo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoMoneda')); ?>:</b>
	<?php echo CHtml::encode($data->codigoMoneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaApertura')); ?>:</b>
	<?php echo CHtml::encode($data->fechaApertura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaCierre')); ?>:</b>
	<?php echo CHtml::encode($data->fechaCierre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cbu')); ?>:</b>
	<?php echo CHtml::encode($data->cbu); ?>
	<br />

	*/ ?>

</div>