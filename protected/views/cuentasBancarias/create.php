<?php
/* @var $this CuentasBancariasController */
/* @var $model CuentasBancarias */

$this->breadcrumbs=array(
    CuentasBancarias::model()->getAttributeLabel('models')=>array('admin'),
    'Creando ',
);

$this->menu=array(
    array('label'=>'Gestion de '.CuentasBancarias::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

    <h3>Creando <?=CuentasBancarias::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>