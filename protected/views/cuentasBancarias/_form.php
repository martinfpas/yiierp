<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cuentas-bancarias-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCuentasBancarias"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCuentasBancarias">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
        <div class="span3">
            <?php echo $form->textFieldRow($model,'numero',array('class'=>'span12','maxlength'=>30)); ?>
        </div>

		<div class="span3">
            <?php
            echo $form->labelEx($model,'id_banco');
            echo $form->dropDownList($model,'id_banco',CHtml::listData(Banco::model()->todos()->findAll(),'id','nombre'),array(
                'empty' => 'Seleccionar Banco',
                'ajax' => array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('SucursalBanco/ListarSucursales'), //url to call.
                    'update'=>'#CuentasBancarias_id_sucursal', //selector to update
                ),

            ));
            ?>
		</div>
		<div class="span3">
            <?php
                echo $form->labelEx($model,'id_sucursal');
                if ($model->isNewRecord){
                    echo $form->dropDownList($model,'id_sucursal',CHtml::listData(array(),'',''),array('empty' => 'Seleccionar Banco'));
                }else{
                    echo $form->dropDownList($model,'id_sucursal',CHtml::listData($model->oBanco->aSucursales,'id','nombre'),array('empty' => 'Seleccionar Banco'));
                }
            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'observaciones',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'codigoTipoCta');
            echo $form->dropDownList($model,'codigoTipoCta',CHtml::listData(TipoCuenta::model()->findAll(),'id','nombre'),array('empty' => 'Seleccionar'));
            ?>

        </div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'saldo',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaSaldo',array('class'=>'span12')); ?>
		</div>
		<div class="span3">


            <?php
            echo $form->labelEx($model,'codigoMoneda');
            echo $form->dropDownList($model,'codigoMoneda',CHtml::listData(Moneda::model()->findAll(),'codigo_moneda','nombre'),array('empty' => 'Seleccionar'));
            ?>
		</div>
		<div class="span3">

            <?php
            echo $form->labelEx($model,'fechaApertura',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fechaApertura),
                'name'=>'CuentasBancarias[fechaApertura]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));
            ?>

        </div>
		<div class="span3">

            <?php
            echo $form->labelEx($model,'fechaCierre',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fechaCierre),
                'name'=>'CuentasBancarias[fechaCierrev]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));
            ?>

        </div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cbu',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
