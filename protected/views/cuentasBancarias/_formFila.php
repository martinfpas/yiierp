<?php
/** @var $aMovimientoCuentasBanc MovimientoCuentasBanc */

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'movimiento-cuentas-banc-grid',
'dataProvider'=>$aMovimientoCuentasBanc->searchWP(),
'htmlOptions' => array(
    'style' => 'padding-top:0px;',
),
'columns'=>array(
		'oTipoMovimiento.Descripcion',
		//'codigoMovimiento',
        array(
            'name' => 'fechaMovimiento',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaMovimiento)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'width:80px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
		'oMedio.Descripcion',
        'Nro_operacion',
        'Observacion',
        array(
            'name' => 'Importe',
            'value' => '($data->oTipoMovimiento->Ingreso)? number_format($data->Importe,2,",",".") : ""',
            'header' => 'Debe',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'Importe',
            'value' => '(!$data->oTipoMovimiento->Ingreso)? number_format($data->Importe,2,",",".") : ""',
            'header' => 'Haber',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header' => '$ Saldo',
            'name' => 'saldo_acumulado',
            'value' => 'number_format($data->saldo_acumulado,2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),

	),
)); ?>