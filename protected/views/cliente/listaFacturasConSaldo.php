<?php
/* @var $this ClienteController */
/* @var $model VDocumentosConSaldo */

$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Cliente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
    tr.odd td,tr.even td{
        border-bottom: 1px solid black;
        font-size: 8;
    }
    tr th{
        border-bottom: 0.03em solid black;
        font-size: 8;
    }
    
 
    tr.odd td.noBorder, tr.even td.noBorder {
        border-top: none;
        border-bottom: none;
    }

    tr.odd td.border, tr.even td.border {
        border-top: 1px solid black;
        border-bottom: none ;
        border-left: none;
        border-right: none;
    }

    tr.odd td:not(.noBorder), tr.even td:not(.noBorder) {
        border-top: 1px black solid;
    }
</style>

<h3>Listado de Clientes Deudores</h3>

<?php 

$dataRows = array();

$acumulado = 0;
$total = 0;
foreach ($model->searchWP()->getData() as $key => $data) {
    $dataRows[$key]['obj'] = $data;

    if ($key > 0 && $data->oCliente->id == $dataRows[$key - 1]['obj']->oCliente->id) {
        $dataRows[$key]['idCliente'] = '';
        $dataRows[$key]['razonSocial'] = '';
        $dataRows[$key]['localidad'] = '';
        $acumulado += $data->saldo;
    } else {
        $dataRows[$key]['idCliente'] = $data->oCliente->id;
        $dataRows[$key]['razonSocial'] = $data->oCliente->Title;
        $dataRows[$key]['localidad'] = $data->oCliente->Localidad;
        $acumulado = $data->saldo;
    }
    $dataRows[$key]['id'] = $data->getFullTitleDocumento();
    $dataRows[$key]['fecha'] = ComponentesComunes::fechaFormateada($data->fecha);
    $dataRows[$key]['saldo'] = number_format($data->saldo, 2, ",", ".");
    $dataRows[$key]['saldoActual'] = $acumulado;
    $total += $saldo;
}

$dataProvider = new CArrayDataProvider($dataRows, array(
    'pagination' => false,
));

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cliente-grid',
'dataProvider'=> $dataProvider,
'enableSorting' => false,
'htmlOptions' => array(
    'style' => 'font-size:small;margin-left:10px;',
),
'columns'=>array(
        array(
            'name' => 'idCliente',
            'header' => 'id',
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
        ),
        array(
            'name' => 'razonSocial',
            'header' => 'NOMBRE',
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
            'htmlOptions' => array('style' => 'width:250px'),
            'headerHtmlOptions' => array('style' => 'width:250px'),
        ),
        array(
            'name' => 'localidad',
            'header' => 'LOCALIDAD',
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
        ),
        array(
            'name' =>     'id',
            'header' => 'Nro',
            //'value' => '$data->getFullTitleDocumento()',
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
        ),
        array(
            'name' => 'fecha',
            'htmlOptions' => array('style' => ''),
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
        ),
        array(
            'name' => 'saldo',
            'header' => 'Saldo Factura',
            'htmlOptions' => array('style' => 'text-align:right'),
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
            'value' => 'number_format($data["saldo"],2,",",".")',
        ),

        array(
            'name' => 'saldoActual',
            'header' => 'Acumulado',
            'value' => 'number_format($data["saldoActual"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right'),
            'cssClassExpression' => '($data["idCliente"] != "")? "border" : "noBorder"',
        ),

	),
)); 

echo '<h3 style="float:rigth;">Total:'.number_format($total,2,",",".").'</h3>';
?>
