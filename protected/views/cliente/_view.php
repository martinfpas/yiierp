<?php
/* @var $this ClienteController */
/* @var $data Cliente */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razonSocial')); ?>:</b>
	<?php echo CHtml::encode($data->razonSocial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccion')); ?>:</b>
	<?php echo CHtml::encode($data->direccion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombreFantasia')); ?>:</b>
	<?php echo CHtml::encode($data->nombreFantasia); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contacto')); ?>:</b>
	<?php echo CHtml::encode($data->contacto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cuit')); ?>:</b>
	<?php echo CHtml::encode($data->cuit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefonos')); ?>:</b>
	<?php echo CHtml::encode($data->telefonos); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('PDpto')); ?>:</b>
	<?php echo CHtml::encode($data->PDpto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_rubro')); ?>:</b>
	<?php echo CHtml::encode($data->id_rubro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('direccionDeposito')); ?>:</b>
	<?php echo CHtml::encode($data->direccionDeposito); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codViajante')); ?>:</b>
	<?php echo CHtml::encode($data->codViajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cp')); ?>:</b>
	<?php echo CHtml::encode($data->cp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zp')); ?>:</b>
	<?php echo CHtml::encode($data->zp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('categoriaIva')); ?>:</b>
	<?php echo CHtml::encode($data->categoriaIva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoCliente')); ?>:</b>
	<?php echo CHtml::encode($data->tipoCliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('saldoActual')); ?>:</b>
	<?php echo CHtml::encode($data->saldoActual); ?>
	<br />

	*/ ?>

</div>