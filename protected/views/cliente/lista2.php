<h1>Gestion de Clientes</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cliente-grid',
'dataProvider'=>$model->searchWP2(),
'columns'=>array(
		'id',
		'razonSocial',
		'direccion',
		'nombreFantasia',
		'contacto',
		'cuit',
		/*
		'telefonos',
		'PDpto',
		'email',
		'fax',
		'observaciones',
		'id_rubro',
		'direccionDeposito',
		'codViajante',
		'cp',
		'zp',
		'categoriaIva',
		'tipoCliente',
		'saldoActual',
		*/
	),
)); ?>
