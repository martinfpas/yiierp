<style>
    .ui-widget-content.ui-autocomplete{
        width: 600px;
    }
</style>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cliente-form',
	'enableAjaxValidation'=>false,
)); 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/funcionesComunes.js");

Yii::app()->clientScript->registerScript('search', "
	function updateZpList(cp,zp){
		//
				
		var data = 'cp='+cp
		$.ajax({
			url:'".Yii::app()->createUrl('CodigoPostal/ZpList')."',
			type: 'POST',
			data : data,
			success:function(html){		        	    
				$('#Cliente_zp').html(html);
				if(zp !== undefined){
				    console.log(zp);
				    $('#Cliente_zp').val(zp);
				}
			},
			error:function(x,y,z){
			    dfd.reject( 'sorry' );
				console.err(x);
			},		
		});
        
	};	
	$(document).ready(function(){
        $('#Cliente_cuit').change(function(){
            console.log('Cliente_cuit');
            // SI NO ES CONSUMIDOR FINAL
            if($('#Cliente_categoriaIva').val() != 5){
                if(!validarCuit($(this).val())){
                    alert('CUIT INVALIDO');
                    $(this).focus();
                }
            }
        });
	
	});
");

?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCliente"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
        <?php
            if($model->isNewRecord){
                echo $form->textFieldRow($model,'id',array('class'=>'span2','tabindex'=>'0','maxlength'=>7));
            }
        ?>
	</div>	
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','tabindex'=>'1','maxlength'=>40)); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>60,'tabindex'=>'2')); ?>
		</div>
	</div>	
	<div class="row-fluid">		
		<div class="span6">
			<?php echo $form->textFieldRow($model,'direccion',array('class'=>'span12','maxlength'=>30,'tabindex'=>'3')); ?>
		</div>

		<div class="span2">
			<?php 
				echo $form->labelEx($model,'cp');
				$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
				    'name'=>'Cliente[cp]',
					'id'=>'Cliente_cp',
				    'value' => $model->cp,
				    'sourceUrl'=>$this->createUrl('codigoPostal/CodigosAsync'),
				    // additional javascript options for the autocomplete plugin
				    'options'=>array(
				        'minLength'=>'2',
				    	'select' => 'js:function(event, ui){
							console.log(ui.item["value"]);						
							updateZpList(ui.item["value"],ui.item["zp"]);
						}',
                        'appendTo'=> '#cliente-form',
				    ),

				    'htmlOptions'=>array(
				        'style'=>'height:20px;',
				    	'tabindex'=>'5',
				    	'class'=>'span12'	
				    ),
				));
			?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'zp');//,array('class'=>'span12','maxlength'=>4,'tabindex'=>'6')); 
				echo $form->dropDownList($model,'zp',CHtml::listData(CodigoPostal::model()->findAll(),'zp','IdZp'),array('tabindex'=>'6','empty'=>'---'));
			?>
		</div>
        <div class="span1">
            <?php echo $form->textFieldRow($model,'PDpto',array('class'=>'span12','maxlength'=>6,'tabindex'=>'4')); ?>
        </div>
	</div>	
	<div class="row-fluid">
		<div class="span6">
			<?php echo $form->textFieldRow($model,'telefonos',array('class'=>'span12','maxlength'=>15,'tabindex'=>'7')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fax',array('class'=>'span12','maxlength'=>15,'tabindex'=>'8')); ?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'codViajante');
				echo $form->dropDownList($model,'codViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'9'));
			?>
		</div>
		<div class="" >
		
		</div>
	</div>	
	<div class="row-fluid">		
		<div class="span4">
			<?php 
				echo $form->labelEx($model,'categoriaIva');
				echo $form->dropDownList($model,'categoriaIva',CHtml::listData(CategoriaIva::model()->todas()->findAll(),'id_categoria','nombre'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'10'));
			?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'cuit',array('class'=>'span12','maxlength'=>13,'tabindex'=>'11')); ?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_rubro');
				echo $form->dropDownList($model,'id_rubro',CHtml::listData(RubroClientes::model()->todos()->findAll(),'id_rubro','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'12'));
			?>
		</div>
		
		<div class="span3">
			<?php echo $form->textFieldRow($model,'saldoActual',array('class'=>'span12','disabled'=>'disabled')); ?>
		</div>
	</div>	
	<div class="row-fluid">		
		<div class="span6">
			<?php echo $form->textFieldRow($model,'contacto',array('class'=>'span12','maxlength'=>50,'tabindex'=>'13')); ?>
		</div>
		<div class="span6">
			<?php echo $form->textFieldRow($model,'direccionDeposito',array('class'=>'span12','maxlength'=>60,'tabindex'=>'14')); ?>
		</div>
	</div>	
	<div class="row-fluid">		
		<div class="span6">
            <?php echo $form->textAreaRow($model,'pieFactura',array('rows'=>6, 'cols'=>50, 'class'=>'span12','tabindex'=>'15')); ?>
            <?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>50,'tabindex'=>'17')); ?>
		</div>

		<div class="span6">
			<?php echo $form->textAreaRow($model,'observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span12','tabindex'=>'16')); ?>
            <?php echo $form->labelEx($model,'exeptuaIIB');?>
            <?php echo $form->checkBox($model,'exeptuaIIB',array('tabindex'=>'18'));?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">

</div>

<?php $this->endWidget(); ?>
