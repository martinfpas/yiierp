<style>
    tr.odd td,tr.even td{
        border-bottom: 1px solid black;
        font-size: 8;
    }
    tr th{
        border-bottom: 0.03em solid black;
        font-size: 8;
    }
</style>
<?php
/* @var $this ClienteController */
/* @var $model Cliente */

?>

<h3>Listado Parcial de Clientes.</h3>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cliente-grid',
'dataProvider'=>$model->searchWP(),
'enableSorting' => false,
'htmlOptions' => array(
    'style' => 'font-size:small;margin-left:10px;',
),
'columns'=>array(
        array(
            'name' => 'id',
            'header' => 'N° CLI',
        ),
        array(
            'name' => 'razonSocial',
            'header' => 'NOMBRE',
            'htmlOptions' => array('style' => 'width:250px'),
            'headerHtmlOptions' => array('style' => 'width:250px'),
        ),
        array(
            'name' => 'direccion',
            'header' => 'DIRECCION',
            'htmlOptions' => array('style' => 'width:130px'),
            'headerHtmlOptions' => array('style' => 'width:130px'),
        ),
        array(
            'name' => 'telefonos',
            'header' => 'TELEFONOS',
            'htmlOptions' => array('style' => 'width:70px'),
            'headerHtmlOptions' => array('style' => 'width:70px'),
        ),
        array(
            'name' => 'localidad',
            'header' => 'LOCALIDAD',
            'value'=> '$data->Localidad',

        ),
        array(
            'name' => 'categoriaIva',
            'header' => 'IVA',

        ),
        array(
            'name' => 'id_rubro',
            'header' => 'RUBRO',

        ),




		/*
		localidad, iva,
		'PDpto',
		'email',
		'fax',
		'observaciones',
		'id_rubro',
		'direccionDeposito',
		'codViajante',
		'cp',
		'zp',
		'categoriaIva',
		'tipoCliente',
		'saldoActual',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
