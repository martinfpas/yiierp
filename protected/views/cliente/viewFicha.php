<style>
.bordeRedondo{
	border: #dddddd 1px solid;
	padding: 10px; 
	border-radius: 0.7em;
}
.table {
    margin-bottom: 0px;
}
</style>
<div class="content-fluid">
	<div class="row-fluid">
		<div class="span7 bordeRedondo" >
		<?php
		$this->widget('bootstrap.widgets.TbDetailView',array(
		'data'=>$model,
		'attributes'=>array(
				'razonSocial',
				'direccion',
				'telefonos',
				'cuit',
			),
		)); ?>			
		</div>
		<div class="span5 bordeRedondo">
			<?php
				$this->widget('bootstrap.widgets.TbDetailView',array(
				'data'=>$model,
				'attributes'=>array(
						array(
								'name' => 'Rubro',
								'value' => ($model->oRubro != null )? $model->oRubro->nombre : 'NO TIENE RUBRO !!!',
								'header' => 'Rubro',
						),
						array(
								'name' => 'Provincia',
								'value' => $model->Provincia,
								'header' => 'Provincia',
						),
                        'Localidad',
                        array(
								'name' => 'Zona',
								'value' => $model->zp,
						),
						
						/*
						 * oRubro
						 * 
						array(
							'name' => ,
							'value' => ,
							'htmlOptions' => array('style' => ''),
						),
						*/
				),
				)); ?>
		</div>
	</div>
</div>

