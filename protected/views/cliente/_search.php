<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
    <div class="row-fluid">
        <div class="span4">
            <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

            <?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>40)); ?>

            <?php echo $form->textFieldRow($model,'direccion',array('class'=>'span12','maxlength'=>30)); ?>
        </div>
        <div class="span4">
            <?php echo $form->textFieldRow($model,'nombreFantasia',array('class'=>'span12','maxlength'=>60)); ?>

            <?php echo $form->textFieldRow($model,'contacto',array('class'=>'span12','maxlength'=>50)); ?>

            <?php echo $form->textFieldRow($model,'cuit',array('class'=>'span12','maxlength'=>13)); ?>
        </div>
        <div class="span4">
		<?php echo $form->textFieldRow($model,'telefonos',array('class'=>'span12','maxlength'=>40)); ?>

		<?php echo $form->textFieldRow($model,'PDpto',array('class'=>'span12','maxlength'=>13)); ?>

		<?php echo $form->textFieldRow($model,'email',array('class'=>'span12','maxlength'=>50)); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
		<?php echo $form->textFieldRow($model,'fax',array('class'=>'span12','maxlength'=>15)); ?>

		<?php echo $form->textAreaRow($model,'observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>

        <?php
        echo $form->labelEx($model,'id_rubro');
        echo $form->dropDownList($model,'id_rubro',CHtml::listData(RubroClientes::model()->todos()->findAll(),'id_rubro','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'12'));
        ?>
        </div>
        <div class="span4">
		<?php echo $form->textFieldRow($model,'direccionDeposito',array('class'=>'span12','maxlength'=>60)); ?>


        <?php
            echo $form->labelEx($model,'codViajante');
            echo $form->dropDownList($model,'codViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'9'));
        ?>


		<?php echo $form->textFieldRow($model,'cp',array('class'=>'span12','maxlength'=>4)); ?>
        </div>
        <div class="span4">
		<?php echo $form->textFieldRow($model,'zp',array('class'=>'span12','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'categoriaIva',array('class'=>'span12')); ?>


        <?php
            echo $form->labelEx($model,'tipoCliente');
            echo $form->dropDownList($model,'tipoCliente', Cliente::$tipoPersona,array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'12'));
        ?>


		<?php echo $form->textFieldRow($model,'saldoActual',array('class'=>'span12')); ?>
        </div>
    </div>
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
