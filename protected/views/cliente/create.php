<?php
/* @var $this ClienteController */
/* @var $model Cliente */

$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Cliente', 'url'=>array('admin')),
);
?>

<h2>Creando Cliente</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>