<style>
    #vcta-cte-cliente-grid{
        padding-top: 0px;
    }
</style>
<?php
/* @var  $oCtaCte VCtaCteCliente*/
/* @var  $model Cliente*/
$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Cliente', 'url'=>array('create')),
	array('label'=>'Modificar Cliente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Cliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Cliente', 'url'=>array('admin')),
    array('label'=>'Imprimir Sobre', 'url'=>array('PdfCarta', 'id'=>$model->id),'linkOptions'=>array('target' => '_blank')),
);

$cs = Yii::app()->getClientScript();
$cs->registerScript('pago',"
    $('#nuevoPago').click(function(){
        var idCliente = $('#idCliente').val();
        var datos = 'idCliente='+idCliente;
        
        var promiseGetFactura =  $.ajax({
            url: '".$this->createUrl('PagoCliente/NewBlank')."',
            type: 'Get',
            data: datos,
            success: function (idPago) {
                $('#idPagoWid').val(idPago);
                $('#modalPagoClientes').modal();
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());                
            }
        });        
    });    
    $('#PagoCLienteIdModal').change(function(){        
        try{
            //$.fn.yiiGridView.update('vcta-cte-cliente-grid');
            location.reload();
        }catch(e){
            console.log(e);
        };
    });
    
    $('#ClienteSearch-form').submit(function(){
        $('#vcta-cte-cliente-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
    
    $('#export').click(function(e){
        e.preventDefault();
        let url = $('#export').attr('url')+'&'+$('#ClienteSearch-form').serialize();
        console.log(url);
        $('#export').attr('href',url);
        window.open(url);        
    });
");
$saldoRecalculado = $model->saldoConciliado;
foreach ($oCtaCte->searchWP()->getData() as $key => $data) {
    $saldoRecalculado += $data->debe;
    $saldoRecalculado -= $data->haber;
}

?>

<h2>Conciliacion Cliente #<?php echo $model->id; ?></h2>
<input type="hidden" id="idCliente" value="<?=$model->id?>">
<input type="hidden" id="PagoCLienteIdModal" />
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'razonSocial',
                    'nombreFantasia',
                    array(
                        'name' => 'fechaConciliacion',
                        'value' => ComponentesComunes::fechaFormateada($model->fechaConciliacion),
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'label' => 'Saldo Conciliado',
                        'value' => "$".number_format($model->saldoConciliado,2,',','.'),
                        'type' => 'raw',
                        'htmlOptions' => array('style' => 'color:red;'),
                    ),
                    array(
                        'label' => 'Saldo Recalculado',
                        'value' => "<span style='color:red;'>$".number_format($saldoRecalculado,2,',','.')."</span>",
                        'type' => 'raw',
                        'htmlOptions' => array('style' => 'color:red;'),
                    ),
                ),
            )); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'direccion',
                    array(
                        'name' => 'cp',
                        'value' => $model->cp.' ('.$model->Localidad.' - '.$model->Provincia.')',
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'categoriaIva',
                        'value' => $model->oCategoriaIva->nombre,
                    ),
                    'tipoCliente',
                    'saldoActual',
                    /*
                    array(
                        'name' => ,
                        'value' => ,
                        'htmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>
</div>


<h4>Movimientos Cta Cte</h4>

<div class="content-fluid">
    <div class="row-fluid">
        <div class='span6 bordeRedondo'>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'link',
                'type'=>'primary',
                'label'=> 'Forzar Conciliacion de saldo',
                'url' => Yii::app()->createUrl('cliente/forzarSaldo',array('id'=>$model->id,'value'=>$saldoRecalculado)),
                'htmlOptions' => array(
                    'id' => 'conciliarSaldo',
                    'value' => $saldoRecalculado,
                )
            )); ?>
        </div>
        <div class='span6 bordeRedondo'>
            <?php
            $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                'action' => Yii::app()->createUrl($this->route),
                'method' => 'post',
                'id'=>'ClienteSearch-form',
            ));
            ?>
            <div class='span4'>
                <?php
                echo CHtml::label('Fecha Desde','fechaDesde',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($oCtaCte->desde),
                    'name'=>'VCtaCteCliente[desde]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                    ),
                ));
                ?>

            </div>
            <div class='span4'>
                <?php
                echo CHtml::label('Fecha Hasta', 'hasta',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($oCtaCte->hasta),
                    'name'=>'VCtaCteCliente[hasta]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                    ),
                ));
                ?>

            </div>
            <div class='span2'>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => Yii::t('application', 'Buscar'),
                    'htmlOptions' => array(
                        'style' => 'margin-top:20px',
                    ),
                )); ?>
            </div>
            <div class="span2">
                <?php
                //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
                echo CHtml::link('Exportar',Yii::app()->createUrl('Cliente/CtaCtePdf',array('id'=>$model->id)), array('class'=>'bulk-button btn print-button','id' => 'export','target' => '_blank','style' => 'float:left;margin-top:20px;','url' => Yii::app()->createUrl('Cliente/CtaCtePdf',array('id'=>$model->id))));

                ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'vcta-cte-cliente-grid',
    'dataProvider'=>$oCtaCte->searchWP(),
    'enableSorting' => false,
    'columns'=>array(
        array(
            'name' => 'neId',
            'type' => 'raw',
            'header' => 'Nota Entrega',
            'value' => '($data->tipo == "NE")? CHtml::link(CHtml::encode($data->getComprobante()->nroComprobante), array("NotaEntrega/view","id"=>$data->neId)) : ""',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right'),
        ),
        array(
            'name' => 'faId',
            'type' => 'raw',
            'header' => 'Factura',
            'value' => '($data->tipo == "F")? CHtml::link($data->getComprobante()->FullTitle, array("Factura/view","id"=>$data->faId), array("target"=>"_blank")) : ""',
            'htmlOptions' => array('style' => 'text-align:right;width: 110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width: 110px;'),
        ),
        array(
            'name' => 'pcId',
            'type' => 'raw',
            'header' => 'Pago',
            'value' => '($data->tipo == "PC")? CHtml::link(CHtml::encode("#".$data->pcId), array("PagoCliente/view","id"=>$data->pcId)) : ""',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'ncId',
            'type' => 'raw',
            'header' => 'Nota Crédito',
            'value' => '($data->tipo == "NC")? CHtml::link(CHtml::encode($data->getComprobante()->FullTitle), array("NotaCredito/view","id"=>$data->ncId)) : ""',
            'htmlOptions' => array('style' => 'text-align:right;width: 110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width: 110px;'),
        ),
        array(
            'name' => 'ndId',
            'type' => 'raw',
            'header' => 'Nota Débito',
            'value' => '($data->tipo == "ND")? CHtml::link(CHtml::encode($data->getComprobante()->FullTitle), array("NotaDebito/view","id"=>$data->ndId)) : ""',
            'htmlOptions' => array('style' => 'text-align:right;width: 110px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width: 110px;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'text-align:right;width: 80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width: 80px;'),
        ),
        array(
            'name' => 'debe',
            'value' => '"$".number_format($data->debe,2,".","")',
            'header' => 'Debe',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right'),
        ),
        array(
            'name' => 'haber',
            'value' => '"$".number_format($data->haber,2,".","")',
            'header' => 'Haber',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right'),
        ),
    ),
)); ?>

<?php
$this->widget('PagoClienteWid', array('js_comp'=>'#PagoCLienteIdModal','is_modal' => true));
?>