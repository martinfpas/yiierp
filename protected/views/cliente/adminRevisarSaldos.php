<style>
.red{
    color:red;
}
</style>
<?php
/* @var $this ClienteController */
/* @var $model Cliente */

$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	'Gestion',
);


$this->menu=array(
	array('label'=>'Nuevo Cliente', 'url'=>array('create')),
	array('label'=>'Listados', 'url'=>array('VClientesLista/admin')),
	array('label'=> 'Listados de Clientes Con Saldo','url' =>array('VClientesLista/adminConSaldo')),
	array('label'=> 'Listados de Documentos con Saldo','url' =>array('cliente/AdminDocumentosConSaldo')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#export-list').click(function(e){
    e.preventDefault();
    e.stopPropagation();
	var datos = $('.search-form form').serialize();
    $	
	return false;
});
");
?>

<h1>Gestion de Clientes</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<?php //echo CHtml::link('Exportar Seleccion','#',array('class'=>'search-button btn btn-primary','id'=>'export-list')); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cliente-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'text-align:right;width:50px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:50px;'),
		),
        'razonSocial',
		'direccion',
		//'nombreFantasia',
		'contacto',
		'cuit',
        array(
            'name' => 'saldoActual',
            'value' => '"$".number_format($data->saldoActual,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
			'header' => 'SaldoRecalculado',
            'value' => '"$".number_format($data->SaldoRecalculado,2,".","")',
            'cssClassExpression' => '($data->SaldoRecalculado != $data->saldoActual)? "red" : ""',
			'htmlOptions' => array('style' => ''),
		),
        /*
		'telefonos',
		'PDpto',
		'email',
		'fax',
		'observaciones',
		'id_rubro',
		'direccionDeposito',
		'codViajante',
		'cp',
		'zp',
		'categoriaIva',
		'tipoCliente',
		'saldoActual',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}',
            'buttons'=>array(
                'view' => array(
                    'label' => 'Ver',
                    'url'=>'Yii::app()->controller->createUrl("cliente/Conciliar", array("id"=>$data->id))',
                    'options'=>array('target'=>'_blank'),
                ),
            ),
		),
	),
)); ?>
