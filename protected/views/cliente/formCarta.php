<style>
.bordeRedondo{
	border: #dddddd 1px solid;
	padding: 10px; 
	border-radius: 0.7em;
}
.table {
    margin-bottom: 0px;
}
</style>
<?php

Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $('#cliente-form').attr('action')+'&'+$('#cliente-form').serialize();
        let print = $(this).attr('print')+'&'+$('#cliente-form').serialize();
        console.log(href);
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
    });
");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'cliente-form',
    'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('Cliente/ViewPdfCarta',array('id' => $model->id)),
));


?>
<div class="row-fluid">
    <div class="span6">

        <?php echo $form->textFieldRow($model,'razonSocial',array('class'=>'span12','maxlength'=>40,'tabindex'=>'1')); ?>

        <?php echo $form->textFieldRow($model,'direccion',array('class'=>'span12','maxlength'=>30,'tabindex'=>'2')); ?>

        <?php echo $form->textFieldRow($model,'cp',array('class'=>'span6','maxlength'=>30,'tabindex'=>'3')); ?>

        <?php echo $form->textFieldRow($model,'sLocalidad',array('class'=>'span12','tabindex'=>'4')); ?>
    </div>
    <div class="span6">
        <?php echo $form->textFieldRow($model,'sProvincia',array('class'=>'span12','tabindex'=>'5')); ?>

        <?php echo $form->textAreaRow($model,'atencionSobre',array('rows'=>6, 'cols'=>50, 'class'=>'span12','tabindex'=>'6')); ?>

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType'=>'submit',
            'type'=>'primary',
            'label'=>'Pdf',

            'htmlOptions' => array(
                'print' => Yii::app()->createUrl('Cliente/ViewPdfCarta', array('id'=>$model->id,'print'=>true)),
                'class' => 'btn-primary print-button',
            )
        )); ?>
    </div>
</div>




<?php $this->endWidget(); ?>
<?php $this->widget('PrintWidget') ?>
