<?php 
    Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let serial = $('#SearchDocs-form').serialize();
        serial = serial.replace('r=cliente%2FAdminDocumentosConSaldo&',''); 
        let href = $(this).attr('href')+'&'+serial;
        console.log(href);
        let print = $(this).attr('print')+'&'+serial;
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);
        $('#modalPrint').modal();
    });
    ");

$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id'=>'SearchDocs-form',
)); ?>
<div class="content-fluid">
    <div class="row-fluid">

        <div class='span4'>
            <?php 
                echo $form->labelEx($model, 'tipo', array('class' => 'span12', 'maxlength' => 2));
                echo $form->dropDownList($model, 'tipo', array('F' => 'F','NE' => 'NE'),array('class'=>'span12','empty'=>'--Todos--'));
            ?>
        </div>
        <div class='span4'>
            <?php echo $form->textFieldRow($model, 'nroComprobante', array('class' => 'span12', 'maxlength' => 8)); ?>
        </div>
        <div class='span4'>
            <?php echo $form->textFieldRow($model, 'Id_Cliente', array('class' => 'span12')); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class='span4'>
            <?php
                echo $form->labelEx($model,'codViajante');
                echo $form->dropDownList($model,'codViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'9'));
            ?>
        </div>
        <div class='span2'>
            <?php echo $form->textFieldRow($model, 'fecha', array('class' => 'span12')); ?>
        </div>
        <div class='span2'>
            <div class="form-actions" style="background-color: none;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => Yii::t('application', 'Buscar'),
                )); ?>
            </div>
        </div>
        <div class="span1">
            <?php
            //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
                echo CHtml::link('Exportar',Yii::app()->createUrl('cliente/listadoDeudores'),
                array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;margin-top:20px;', 'print' => Yii::app()->createUrl('cliente/listadoDeudores', array('print'=>true)) ));
            ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>