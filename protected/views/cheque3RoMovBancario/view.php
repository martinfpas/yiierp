<?php
$this->breadcrumbs=array(
	Cheque3RoMovBancario::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.Cheque3RoMovBancario::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.Cheque3RoMovBancario::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.Cheque3RoMovBancario::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.Cheque3RoMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=Cheque3RoMovBancario::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idCheque3ro',
		'idMovimiento',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
