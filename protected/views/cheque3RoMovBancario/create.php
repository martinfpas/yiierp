<?php
/* @var $this Cheque3RoMovBancarioController */
/* @var $model Cheque3RoMovBancario */

$this->breadcrumbs=array(
	Cheque3RoMovBancario::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.Cheque3RoMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=Cheque3RoMovBancario::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>