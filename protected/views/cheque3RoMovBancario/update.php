<?php
$this->breadcrumbs=array(
	Cheque3RoMovBancario::model()->getAttributeLabel('models') => array('admin'),
	Cheque3RoMovBancario::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.Cheque3RoMovBancario::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.Cheque3RoMovBancario::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.Cheque3RoMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=Cheque3RoMovBancario::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>