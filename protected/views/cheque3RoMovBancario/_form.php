<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cheque3-ro-mov-bancario-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCheque3RoMovBancario"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCheque3RoMovBancario">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'idCheque3ro',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'idMovimiento',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
