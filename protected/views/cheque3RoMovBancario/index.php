
/* @var $this Cheque3RoMovBancarioController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Cheque3RoMovBancario::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo Cheque3RoMovBancario', 'url'=>array('create')),
	array('label'=>'Gestion de Cheque3RoMovBancario', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>