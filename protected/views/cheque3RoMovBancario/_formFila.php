
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoCheque3RoMovBancario" id="masCheque3RoMovBancario" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoCheque3RoMovBancario" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'cheque3-ro-mov-bancario-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('Cheque3RoMovBancario/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'cheque3-ro-mov-bancario-grid',
			'idMas' => 'masCheque3RoMovBancario',
			'idDivNuevo' => 'nuevoCheque3RoMovBancario',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorCheque3RoMovBancario"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okCheque3RoMovBancario">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idCheque3ro',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idMovimiento',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque3-ro-mov-bancario-grid',
'dataProvider'=>$aCheque3RoMovBancario->search(),
'columns'=>array(
		'id',
		'idCheque3ro',
		'idMovimiento',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("Cheque3RoMovBancario/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Cheque3RoMovBancario/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>