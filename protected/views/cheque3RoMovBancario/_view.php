<?php
/* @var $this Cheque3RoMovBancarioController */
/* @var $data Cheque3RoMovBancario */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCheque3ro')); ?>:</b>
	<?php echo CHtml::encode($data->idCheque3ro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMovimiento')); ?>:</b>
	<?php echo CHtml::encode($data->idMovimiento); ?>
	<br />


</div>