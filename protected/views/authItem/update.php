<?php
$this->breadcrumbs=array(
	'Auth Items'=>array('index'),
	$model->name=>array('view','id'=>$model->name),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo AuthItem', 'url'=>array('create')),
	array('label'=>'Ver AuthItem', 'url'=>array('view', 'id'=>$model->name)),
	array('label'=>'ABM AuthItem', 'url'=>array('admin')),
);
?>

	<h1>Modificando AuthItem <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>