<?php

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/jquery.tree.min.js");
$cs->registerCssFile($baseUrl.'/css/jquery.tree.min.css');

Yii::app()->clientScript->registerScript('search', "
 	
	$(document).ready(function () {
		$('.jquery').each(function () {
			eval($(this).html());
		});
		
	});
	$('#arbol div').tree({
		onCheck: {
			ancestors: 'checkIfFull',
			descendants: 'check'
		},
	});	
");

$this->menu=array(
		array('label'=>'Nueva Relacion', 'url'=>array('nuevaRelacion')),
);

?>
<style>
ul{
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 40px;
}
</style>

<form method="post" action="">
	<input type="submit" class="btn" value="Guardar">
	<div id="arbol">
		<div>
			<?php 
			$this->renderPartial('treeCheck',array('aArbol' => $aArbol,'aAssign' => $aAssign));
			?>
		</div>
	</div>
</form>