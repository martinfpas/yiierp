<?php
/* @var $this AuthItemChildController */
/* @var $model AuthItemChild */
/* @var $form CActiveForm */

$this->menu=array(
	array('label'=>'Arbol de Relaciones', 'url'=>array('viewRelations')),
);

?>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'auth-item-child-nuevaRelacion-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // See class documentation of CActiveForm for details on this,
    // you need to use the performAjaxValidation()-method described there.
    'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model,'parent'); ?>
        <?php echo $form->dropDownList($model,'parent',CHtml::listData(AuthItem::model()->todos()->findAll(),'name','NameType')); ?>
        <?php echo $form->error($model,'parent'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'child'); ?>
        <?php echo $form->dropDownList($model,'child',CHtml::listData(AuthItem::model()->todos()->findAll(),'name','NameType')); ?>
        <?php echo $form->error($model,'child'); ?>
    </div>


    <div class="row buttons">
        <?php echo CHtml::submitButton('Submit'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->