<?php
$this->breadcrumbs=array(
	'Auth Items'=>array('admin'),
	$model->name,
);

$this->menu=array(
	array('label'=>'Nuevo AuthItem', 'url'=>array('create')),
	array('label'=>'Modificar AuthItem', 'url'=>array('update', 'id'=>$model->name)),
	array('label'=>'Borrar AuthItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->name),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de AuthItem', 'url'=>array('admin')),
);
?>

<h1>Ver AuthItem #<?php echo $model->name; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'name',
		'type',
		'description',
		'bizrule',
		'data',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
