
/* @var $this AuthItemController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Auth Items',
);

$this->menu=array(
	array('label'=>'Nuevo AuthItem', 'url'=>array('create')),
	array('label'=>'Gestion de AuthItem', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>