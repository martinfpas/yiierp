<?php
/* @var $this AuthItemController */
/* @var $model AuthItem */

$this->breadcrumbs=array(
	'Auth Items'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de AuthItem', 'url'=>array('admin')),
);
?>

<h1>Creando AuthItem</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>