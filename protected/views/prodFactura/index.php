
/* @var $this ProdFacturaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Facturas',
);

$this->menu=array(
	array('label'=>'Nuevo ProdFactura', 'url'=>array('create')),
	array('label'=>'Gestion de ProdFactura', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>