<?php
$this->breadcrumbs=array(
	'Prod Facturas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdFactura', 'url'=>array('create')),
	array('label'=>'Ver ProdFactura', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdFactura', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdFactura <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>