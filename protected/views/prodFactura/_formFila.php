
<?php
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoProdFactura" id="masProdFactura" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoProdFactura" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'prod-factura-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ProdFactura/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'prod-factura-grid',
			'idMas' => 'masProdFactura',
			'idDivNuevo' => 'nuevoProdFactura',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProdFactura"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProdFactura">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
        <div class="span3">
			<?php echo $form->textFieldRow($model,'idFactura',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
			<?php echo $form->textFieldRow($model,'idArtVenta',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
			<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
			<?php echo $form->textFieldRow($model,'fechaEntrega',array('class'=>'span12')); ?>
		</div>
    </div>
    <div class="row-fluid">
        <div class="span3">
			<?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>
		</div>
        <div class="span3">
            <?php echo $form->textFieldRow($model,'alicuotaIva',array('class'=>'span12')); ?>
        </div>
        <div class="span3">
			<?php echo $form->textFieldRow($model,'cantidadEntregada',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nroPuestoVenta',array('class'=>'span12')); ?>
		</div>
    </div>
    <div class="row-fluid">
        <div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
        </div>
	</div>
</div>	
	

	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'prod-factura-grid',
'dataProvider'=>$aProdFactura->search(),
'filter'=>$aprod-factura,
'columns'=>array(
		'id',
		'idFactura',
		'idArtVenta',
		'cantidad',
		'fechaEntrega',
		'precioUnitario',
		/*
		'cantidadEntregada',
		'nroPuestoVenta',
		'descripcion',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ProdFactura/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ProdFactura/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>