<?php
/* @var $this ProdFacturaController */
/* @var $data ProdFactura */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idFactura')); ?>:</b>
	<?php echo CHtml::encode($data->idFactura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArtVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArtVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaEntrega')); ?>:</b>
	<?php echo CHtml::encode($data->fechaEntrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioUnitario')); ?>:</b>
	<?php echo CHtml::encode($data->precioUnitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidadEntregada')); ?>:</b>
	<?php echo CHtml::encode($data->cantidadEntregada); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nroPuestoVenta')); ?>:</b>
	<?php echo CHtml::encode($data->nroPuestoVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	*/ ?>

</div>