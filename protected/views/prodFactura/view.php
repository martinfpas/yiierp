<?php
$this->breadcrumbs=array(
	'Prod Facturas'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdFactura', 'url'=>array('create')),
	array('label'=>'Modificar ProdFactura', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdFactura', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdFactura', 'url'=>array('admin')),
);
?>

<h1>Ver ProdFactura #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idFactura',
		'idArtVenta',
		'cantidad',
		'fechaEntrega',
		'precioUnitario',
		'cantidadEntregada',
		'nroPuestoVenta',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
