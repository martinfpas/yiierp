<?php
/* @var $this ProdFacturaController */
/* @var $model ProdFactura */

$this->breadcrumbs=array(
	'Prod Facturas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ProdFactura', 'url'=>array('admin')),
);
?>

<h1>Creando ProdFactura</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>