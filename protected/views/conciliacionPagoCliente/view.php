<?php
$this->breadcrumbs=array(
	ConciliacionPagoCliente::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.ConciliacionPagoCliente::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.ConciliacionPagoCliente::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.ConciliacionPagoCliente::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.ConciliacionPagoCliente::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=ConciliacionPagoCliente::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idComprobante',
		'idNotaEntrega',
		'monto',
		'idPago',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
