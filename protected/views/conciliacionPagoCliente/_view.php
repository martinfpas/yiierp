<?php
/* @var $this ConciliacionPagoClienteController */
/* @var $data ConciliacionPagoCliente */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaEntrega')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaEntrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monto')); ?>:</b>
	<?php echo CHtml::encode($data->monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPago')); ?>:</b>
	<?php echo CHtml::encode($data->idPago); ?>
	<br />


</div>