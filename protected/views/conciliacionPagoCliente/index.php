
/* @var $this ConciliacionPagoClienteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'ConciliacionPagoCliente::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo ConciliacionPagoCliente', 'url'=>array('create')),
	array('label'=>'Gestion de ConciliacionPagoCliente', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>