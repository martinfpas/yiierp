
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoConciliacionPagoCliente" id="masConciliacionPagoCliente" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoConciliacionPagoCliente" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'conciliacion-pago-cliente-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ConciliacionPagoCliente/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'conciliacion-pago-cliente-grid',
			'idMas' => 'masConciliacionPagoCliente',
			'idDivNuevo' => 'nuevoConciliacionPagoCliente',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorConciliacionPagoCliente"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okConciliacionPagoCliente">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idComprobante',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idNotaEntrega',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'monto',array('class'=>'span12','maxlength'=>9)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idPago',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'conciliacion-pago-cliente-grid',
'dataProvider'=>$aConciliacionPagoCliente->search(),
'columns'=>array(
		'id',
		'idComprobante',
		'idNotaEntrega',
		'monto',
		'idPago',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ConciliacionPagoCliente/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ConciliacionPagoCliente/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>