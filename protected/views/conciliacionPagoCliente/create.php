<?php
/* @var $this ConciliacionPagoClienteController */
/* @var $model ConciliacionPagoCliente */

$this->breadcrumbs=array(
	ConciliacionPagoCliente::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.ConciliacionPagoCliente::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=ConciliacionPagoCliente::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>