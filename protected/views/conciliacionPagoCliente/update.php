<?php
$this->breadcrumbs=array(
	ConciliacionPagoCliente::model()->getAttributeLabel('models') => array('admin'),
	ConciliacionPagoCliente::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.ConciliacionPagoCliente::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.ConciliacionPagoCliente::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.ConciliacionPagoCliente::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=ConciliacionPagoCliente::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>