<?php
/* @var $this ViajanteController */
/* @var $model Viajante */

$this->breadcrumbs=array(
	'Viajantes'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Viajante', 'url'=>array('admin')),
);
?>

<h1>Creando Viajante</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>