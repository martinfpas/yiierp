<?php
$this->breadcrumbs=array(
	'Viajantes'=>array('admin'),
	$model->numero,
);

$this->menu=array(
	array('label'=>'Nuevo Viajante', 'url'=>array('create')),
	array('label'=>'Modificar Viajante', 'url'=>array('update', 'id'=>$model->numero)),
	array('label'=>'Borrar Viajante', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->numero),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Viajante', 'url'=>array('admin')),
);
?>

<h1>Ver Viajante #<?php echo $model->numero; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'numero',
		'nroZona',
		'apellido',
		'nombres',
		'tipoYdoc',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
