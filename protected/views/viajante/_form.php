<?php 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'viajante-form',
	'enableAjaxValidation'=>false,
)); 



?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorViajante"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okViajante">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span2">
			<?php 
				echo $form->textFieldRow($model,'numero',array('class'=>'span12','maxlength'=>2,'tabindex'=>'1')); 
			?>
		</div>
		<div class="span2">
			<?php 
				echo $form->textFieldRow($model,'nroZona',array('class'=>'span12','maxlength'=>4,'tabindex'=>'2')); 
			?>
		</div>		
	</div>
	<div class="row-fluid">	
		<div class="span4">
			<?php echo $form->textFieldRow($model,'apellido',array('class'=>'span12','maxlength'=>45,'tabindex'=>'3')); ?>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($model,'nombres',array('class'=>'span12','maxlength'=>45,'tabindex'=>'4')); ?>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($model,'tipoYdoc',array('class'=>'span12','maxlength'=>15,'tabindex'=>'5')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
