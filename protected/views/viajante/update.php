<?php
$this->breadcrumbs=array(
	'Viajantes'=>array('index'),
	$model->numero=>array('view','id'=>$model->numero),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Viajante', 'url'=>array('create')),
	array('label'=>'Ver Viajante', 'url'=>array('view', 'id'=>$model->numero)),
	array('label'=>'Gestion de Viajante', 'url'=>array('admin')),
);
?>

	<h1>Modificando Viajante <?php echo $model->numero; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>