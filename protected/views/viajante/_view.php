<?php
/* @var $this ViajanteController */
/* @var $data Viajante */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('numero')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->numero),array('view','id'=>$data->numero)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroZona')); ?>:</b>
	<?php echo CHtml::encode($data->nroZona); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('apellido')); ?>:</b>
	<?php echo CHtml::encode($data->apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombres')); ?>:</b>
	<?php echo CHtml::encode($data->nombres); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipoYdoc')); ?>:</b>
	<?php echo CHtml::encode($data->tipoYdoc); ?>
	<br />


</div>