
/* @var $this ViajanteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Viajantes',
);

$this->menu=array(
	array('label'=>'Nuevo Viajante', 'url'=>array('create')),
	array('label'=>'Gestion de Viajante', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>