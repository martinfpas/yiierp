<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'numero',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'nroZona',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'apellido',array('class'=>'span12','maxlength'=>45)); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'nombres',array('class'=>'span12','maxlength'=>45)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'tipoYdoc',array('class'=>'span12','maxlength'=>15)); ?>
    </div>
    <div class="span4">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
