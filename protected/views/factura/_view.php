<?php
/* @var $this FacturaController */
/* @var $data Factura */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('clase')); ?>:</b>
	<?php echo CHtml::encode($data->clase); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_Puesto_Venta')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_Puesto_Venta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_Comprobante')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_Comprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Iva_Responsable')); ?>:</b>
	<?php echo CHtml::encode($data->Iva_Responsable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Nro_Viajante')); ?>:</b>
	<?php echo CHtml::encode($data->Nro_Viajante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id_Cliente')); ?>:</b>
	<?php echo CHtml::encode($data->Id_Cliente); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Moneda')); ?>:</b>
	<?php echo CHtml::encode($data->Moneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Total_Neto')); ?>:</b>
	<?php echo CHtml::encode($data->Total_Neto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Observaciones')); ?>:</b>
	<?php echo CHtml::encode($data->Observaciones); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Id_Camion')); ?>:</b>
	<?php echo CHtml::encode($data->Id_Camion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descuento1')); ?>:</b>
	<?php echo CHtml::encode($data->Descuento1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descuento2')); ?>:</b>
	<?php echo CHtml::encode($data->Descuento2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Total_Final')); ?>:</b>
	<?php echo CHtml::encode($data->Total_Final); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Enviada')); ?>:</b>
	<?php echo CHtml::encode($data->Enviada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Recibida')); ?>:</b>
	<?php echo CHtml::encode($data->Recibida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Otro_Iva')); ?>:</b>
	<?php echo CHtml::encode($data->Otro_Iva); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('CAE')); ?>:</b>
	<?php echo CHtml::encode($data->CAE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Vencim_CAE')); ?>:</b>
	<?php echo CHtml::encode($data->Vencim_CAE); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Ing_Brutos')); ?>:</b>
	<?php echo CHtml::encode($data->Ing_Brutos); ?>
	<br />

	*/ ?>

</div>