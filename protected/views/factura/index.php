
/* @var $this FacturaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Facturas',
);

$this->menu=array(
	array('label'=>'Nueva Factura', 'url'=>array('create')),
	array('label'=>'Gestion de Factura', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>