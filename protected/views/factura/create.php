<?php
/* @var $this FacturaController */
/* @var $model Factura */

$this->breadcrumbs=array(
	'Facturas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Factura', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        var modoEdicion = false;
		// PARA PONER LAS AYUDAS
		try{
			$('label').tooltip();
			$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		// ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		$('#idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
                        
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		
		$('#idCliente').change(function (ev) {
		    getFactura($('#idCliente').val());
		});
		
		
    });
    
    /*
    function clSelected(param){
        $('#errorCLiente').hide('slow');
		$('#okCliente').hide('slow');
		
		console.log('clSelected '+param);
		var datos = 'id='+param;
		$.ajax({
			url:'".$this->createUrl('Cliente/ViewFicha')."',
			type: 'Get',
            data: datos,
			success: function (html) {
               	$('#fichaCliente').html(html);
               	getFactura($('#idCliente').val());
			},
			error: function (x,y,z){
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$('#errorCLiente').html('Cliente No encontrado');
				$('#errorCLiente').show('slow');
				$('#NotaPedido_idCliente').focus();
			}
		});
	}
	*/
	function getFactura(idCliente){
	    var datos = 'idNotaPedido=&idCliente='+idCliente;
	    var htmlAjax = '';
        var promiseGetFactura =  $.ajax({
            url: '".$this->createUrl('factura/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                $('#cuerpo').html(html).promise().done(function(){
                    window.location = '".Yii::app()->createUrl('Factura/update')."&id='+$('#ProdFactura_idComprobante').val();
                    
                });
                
                //$('#cuerpo').html(html);
                //$('#cuerpo').find('#fecha').datepicker();
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
				console.log(x.responseText);
				//TODO: IMPLEMENTAR EN TODOS LADOS
				alert(respuesta.find(element => element.nodeName ==  'P').innerText);
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorFactura').html(htmlError);
                //$('#errorFactura').show('slow');
                
                
            }
        });
        	    
	}
	
",CClientScript::POS_READY);

?>


<h1>Nueva Factura</h1>
<div class="alert alert-block alert-error" style="display:none;" id="errorCLiente"></div>
<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>
<?php
echo CHtml::label('Id Cliente','idCliente');
echo CHtml::textField('idCliente','',array('title' => 'Presionar "+" para abrir el buscador'));
?>
<div id="fichaCliente"></div>
<div id="cuerpo" style="display: none"></div>
<?php
$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => true));
?>
