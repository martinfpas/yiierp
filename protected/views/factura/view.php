<?php
/* @var $model Factura */

if(!$is_modal){
    $this->breadcrumbs=array(
        'Factura'=>array('admin'),
        $model->id,
    );

    if($model->estado < Comprobante::iCerrada){
        $this->menu=array(
            array('label'=>'Modificar Factura', 'url'=>array('update', 'id'=>$model->id)),
            array('label'=>'Borrar Factura', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
            array('label'=>'Gestion de Factura', 'url'=>array('admin')),
        );
    }else{
        $this->menu=array(
            array('label'=>'Gestión de Facturas', 'url'=>array('admin')),
        );
    }

    Yii::app()->clientScript->registerScript('cerrarContenedor', "
        const isModal = false;   
        
        function cerrarContenedor(){
            window.location = '".Yii::app()->createUrl('factura/admin')."';        
        }
    ");

    echo '<h3>Factura </h3>';
}else{
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );
    Yii::app()->clientScript->registerScript('cerrarContenedor', "
        const isModal = true;    
        function cerrarContenedor(){
            try{                
                $('#prod-factura-grid').closest('.modal.fade.in').modal('hide');
            }catch(e){
                //TODO: avisar que no se puede cerrar
                console.error(e);
            }              
        }
                
    ");

}

Yii::app()->clientScript->registerScript('print-button', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
        
        $('#modalPrint button').click(function(){
            console.log('#modalPrint button');
            cerrarContenedor();
            $('#modalPrint').modal('hide');
        });
    });
    $('#descartar').click(function(e){
        console.log('#descartarFa');
        var datos = '&ajax=';
        var url = $(this).attr('url')+'&ajax=';
        
        $.ajax({
            url:url,
            type:'POST',
            data: datos,
            success:function(html) {
              //TODO: SE HACE ALGO CON EL html?
              console.log(html);
              cerrarContenedor();              
              return true;
            },
            error:function(x,y,z) {
              // TODO: MOSTRAR LOS ERRORES
              console.log(x);
              alert(x.responseText);
              return false;
            }
        });
    }); 
    
    $('#desvincularNP').click(function(e){
        console.log('#desvincularNP');
        var datos = '&ajax=';
        var url = $(this).attr('url')+'&ajax=';
        
        $.ajax({
            url:url,
            type:'POST',
            data: datos,
            success:function(html) {
              //TODO: SE HACE ALGO CON EL html?
              console.log(html);
              cerrarContenedor();              
              return true;
            },
            error:function(x,y,z) {
              // TODO: MOSTRAR LOS ERRORES
              console.log(x);
              alert(x.responseText);
              return false;
            }
        });
    });
");
?>

<div class="content-fluid">
    <div class="row-fluid">
        <div class="span5 bordeRedondo">

            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(

                    'razonSocial',
                    'direccion',
                    'localidad',
                    'provincia',                    
                    'Id_Cliente',
                    'Recibida'

                )));
            ?>
            <!--<span id="montoPagadoPatch">+</span>-->
            <div id="montoPagadoPatchBlock" class="row-fluid" style="width: 95%;display:block">
                <div class="span12">
                    <?php $this->widget('editable.EditableDetailView',array(
                        'data'=>$model,
                        //you can define any default params for child EditableFields
                        'url'        => $this->createUrl('Factura/SaveFieldAsync'), //common submit url for all fields
                        'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                        'attributes'=>array(
                            array( //select loaded from database
                                'name' => 'montoPagado',
                                'editable' => array(
                                    'mode'   => 'inline',
                                    'apply'  => true,
                                )
                            ),
                            array(
                                'name' => 'idPagoRelacionado',
                                'editable' => array(
                                    'mode'   => 'inline',
                                    'apply'  => $model->idPagoRelacionado == null,
                                )
                            ),
                        ),
                    )); 
                    ?>
                </div>
            </div>
        </div>
        <div class="span2 bordeRedondo" style="text-align: center">
            <div class="row-fluid">
                <b>Comprobante</b>
            </div>
            <div class="row-fluid">
                <h3><?=$model->clase?></h3>
            </div>
            <div class="row-fluid">
                <b>Pto Vta</b>
            </div>
            <div class="row-fluid">
                <h3><?=str_pad($model->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT)?></h3>
            </div>
            <div class="row-fluid">
                <b>Nro Comprobante</b>
            </div>
            <div class="row-fluid">
                <h3><?=($model->Nro_Comprobante > 0 )? str_pad($model->Nro_Comprobante,8,'0',STR_PAD_LEFT) : '-'?></h3>
            </div>
        </div>
        <div class="span5 bordeRedondo">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'tipoResponsable',
                    'cuit',
                    'perIIBB',
                    'rubro',
                    'zona',
                    'nroInscripIIBB',
                    array(
                        'name' => 'idNotaPedido',
                        'value' => ($model->oNotaPedido != null)? $model->oNotaPedido->Titulo : '',
                    ),
                    array(
                        'name' => 'condicion',
                        'value' => Comprobante::$aCondicion[$model->condicion],
                    ),
                    'Id_Camion',
                    array(
                        'name' => 'fecha',
                        'value' => ComponentesComunes::fechaFormateada($model->fecha),
                    )
                ),
            )); ?>
        </div>
    </div>
</div>

<?php

if($model->idNotaDeCredito == null){
    if (($model->CAE != '00000000000000' && $model->CAE != '')){
        $this->widget('bootstrap.widgets.TbButton',array(
            'buttonType' => 'link',
            'type' => 'primary',
            'label' => 'Crear Nota De Credito',
            'url' => Yii::app()->createUrl('Factura/CrearNotaCredito',array('id' => $model->id)),
        ));
    }
}else{
    $this->widget('bootstrap.widgets.TbButton',array(
        'buttonType' => 'link',
        'type' => 'success',
        'label' => 'Ver Nota De Credito asociada',
        'url' => Yii::app()->createUrl('NotaCredito/view',array('id' => $model->idNotaDeCredito)),
    ));
}

if(($model->CAE == '00000000000000' || $model->CAE == '') && $model->estado < Comprobante::iCerrada ) {
    echo CHtml::link('Descartar Factura', '#', array('class' => 'btn btn-danger', 'id' => 'descartar', 'url' => Yii::app()->createUrl('factura/delete',array('id'=>$model->id)), 'style' => 'float:left;'));
}
if($model->idNotaPedido > 0){
    echo CHtml::link('Desvincular de la Nota de Pedido.', '#', array('class' => 'btn btn-danger', 'id' => 'desvincularNP', 'url' => Yii::app()->createUrl('factura/desvincularNP',array('id'=>$model->id)), 'style' => 'float:left;'));
}

?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-factura-grid',
    'dataProvider'=>$aProdFactura->search(),
    'columns'=>array(
        // TODO: GRABAR
        //'nroItem',
        array(
            'name' => 'cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => ''),

        ),
        'descripcion',
        array(
            'name' => 'precioUnitario',
            'value' => '"$".number_format($data->precioUnitario,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        'precio',
        */
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("ProdFactura/delete", array("id"=>$data->id))',
                    'visible' => '$data->oFactura->estado == Comprobante::iBorrador || $data->oFactura->estado == Comprobante::iGuardada',
                ),
            ),
        ),
    ),
)); ?>



<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>

        <div class="span2"> Descuento 1 </div>

        <div class="span2">Descuento2</div>
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>
        <div class="span2">
            IIBB
        </div>
        <div class="span2" style="text-align: right;">
            <?php
                if ($model->oIvaResponsable->discIva) {
                    $this->widget('editable.EditableField', array(
                        'type' => 'select',
                        'model' => $model,
                        'apply' => false,
                        'attribute' => 'alicuotaIva',
                        'url' => $this->createUrl('Factura/SaveFieldAsync'),
                        'placement' => 'right',
                        'source' => Comprobante::$aIvaAlicuotasMontos,
                        'display' => 'js: function(value, sourceData) {
                                    if(value > 0){
                                        $(this).html(parseFloat(value).toFixed(2)+" %");								
                                    }else{
                                        $(this).html("0 %");							
                                    }
                                }',
                        'htmlOptions' => array(
                            'style' => "float: right;margin-left: 5px;",
                        ),
                        // ACTUALIZO LA GRILLA
                        'success' => 'js: function(response, newValue) {
                                    console.log("refreshFacturaDetails()");
                                    refreshComprobanteDetails();
                                 }',
                    ));
                }
            ?>
            &nbsp;<span style="float: right">IVA</span>&nbsp;
        </div>
        <div class="span2" ><b>Total $</b></div>

    </div>
    <div class="row-fluid" style="text-align: right;">

        <div class="span1">
            <b>$<span id="spanSubTotalNeto"><?=$model->SubTotalNeto ?></span></b>
            <?php
            /*
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            */
            ?>
        </div>
        <div class="span2">            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => false,
                'attribute'   => 'Descuento1',
                'url'         => $this->createUrl('Factura/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span2">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => false,
                'attribute'   => 'Descuento2',
                'url'         => $this->createUrl('Factura/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span1">
            <b>$<span id="spanSubTotal"><?=$model->subtotal ?></span></b>
            <?php
            /*
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            */
            ?>
        </div>
        <div class="span2" id="Ing_Brutos">
            <?php

            number_format($model->Ing_Brutos,2, '.', '');


            ?>
        </div>
        <div class="span2" id="subtotalIva">
            $<?=number_format($model->subtotalIva,2, '.', '')?>
        </div>
        <div class="span2" style=""><b>$<span id="spanTotal"><?=number_format($model->Total_Final,2, '.', '')?></span></b></div>
    </div>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span12" style="margin: 10px;">
            <?php
            echo CHtml::label('Observaciones','Observaciones');
            $this->widget('editable.EditableField', array(
                'type'      => 'textarea',
                'apply'      => false,
                'model'     => $model,
                'attribute' => 'Observaciones',
                'url'       => $this->createUrl('Factura/SaveFieldAsync'),
            ));
            ?>

        </div>
    </div>

</div>
<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span5" style="">
            <?php
            if($model->CAE != '00000000000000' & $model->CAE != ''){

            }
            $this->widget('ext.qrcode.QRCodeGenerator',array(
                'data' => $model->getQrUrl(),
                'subfolderVar' => 'qr',
                'matrixPointSize' => 5,
                'filename' => $model->id.".jpg",
                'filePath'=>Yii::getPathOfAlias("webroot")."/upload",
                //'fileUrl'=>Yii::getPathOfAlias("webroot")."/upload",
                'fileUrl'=>Yii::app()->baseUrl."/upload/",
                'displayImage'=>true, // default to true, if set to false display a URL path
                'errorCorrectionLevel'=>'L', // available parameter is L,M,Q,H
                'matrixPointSize'=>4, // 1 to 10 only
                'displayImage'=>true,
                'imageTagOptions' => ['width'=>"100px",'height'=>"100px"]
            ))
            ?>
        </div>

        <div class="span4" style="">
            
            <br>
            CAE :<?=$model->CAE?>
            <br>
            Vence CAE :<?=ComponentesComunes::fechaFormateada($model->Vencim_CAE)?>
        </div>
        <div class="span3">
            <?php


            if($model->esElectronica()){
                if($model->CAE == '00000000000000' || $model->CAE == ''){
                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idCae" href="'.Yii::app()->createUrl('afipVoucher/Emitir',array('id'=>$model->id)).'">Guardar y obtener CAE</a>';
                }else{
                    echo CHtml::link('Imprimir <i class="icon-white icon-print"></i>',Yii::app()->createUrl('factura/Pdf', array('id'=>$model->id)),
                        array('class'=>'btn btn-primary print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;margin-top:25px;', 'target' => 'blank',
                            'print' => Yii::app()->createUrl('factura/Pdf', array('id'=>$model->id,'print'=>true)))
                    );
                }
            }else{
                echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('factura/Pdf',array('id'=>$model->id)).'">Guardar e Imprimir <i class="icon-white icon-ok"></i> - <i class="icon-white icon-print"></i></a>';
            };
            ?>
        </div>
    </div>
</div>
<?php
if(!$is_modal) {
    $this->widget('PrintWidget');
}
?>