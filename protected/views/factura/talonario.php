<?php
$this->breadcrumbs=array(
	'Facturas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Gestion de Facturas', 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        var modoEdicion = true;
		// PARA PONER LAS AYUDAS
		try{
			$('label').tooltip();
			$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		//getFactura('','".$model->id."','');
		
		// ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		$('#idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
                        
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		
		$('#idCliente').change(function (ev) {
		    getFactura($('#idCliente').val());
		});
		
		
    }); 
	
	function getFactura(idCliente,idFactura,idNotaPedido){
	    var datos = 'idNotaPedido='+idNotaPedido+'&id='+idFactura+'&idCliente='+idCliente;
	    console.log(datos);
        $.ajax({
            url: '".$this->createUrl('Factura/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                $('#cuerpo').html(html);
                $('#cuerpo').find('#fecha').datepicker();
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorFactura').html(htmlError);
                //$('#errorFactura').show('slow');
                alert(respuesta);               
                
            },
        });	    
	}
	
",CClientScript::POS_READY);

if ($model->estado == Comprobante::iBorrador){
    echo '<h3>Creando Factura</h3>';
}else{
    echo '<h3>Modificando Factura'.$model->id.'</h3>';
}
?>

<div class="alert alert-block alert-error" style="display:none;" id="errorCLiente"></div>
<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>

<div id="fichaCliente"></div>
<div id="cuerpo"></div>

<?php

$this->renderPartial('_formFilaTalonario',array(
    'model'=>$model,
    'aProdFactura' => $aProdFactura,
    'modelProd' => $modelProd,
    'ajaxMode' => true,
    'is_modal' => $is_modal
), false,true);

$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => true));
?>


