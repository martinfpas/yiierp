<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'factura-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorFactura"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okFactura">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'clase',array('class'=>'span12','maxlength'=>1)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Puesto_Venta',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Iva_Responsable',array('class'=>'span12','maxlength'=>1)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Viajante',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Moneda',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Neto',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textAreaRow($model,'Observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Camion',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento1',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento2',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Final',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Enviada',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Recibida',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Otro_Iva',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'CAE',array('class'=>'span12','maxlength'=>14)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Vencim_CAE',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Ing_Brutos',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
