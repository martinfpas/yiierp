<style>
    .grid-view {
        padding-top: 0px;
    }
    .detail-view th {
        width: 80px;
        font-size: 85%;
    }
    .detail-view td {
        font-size: 85%;
    }

    .tip-yellowsimple {
        z-index: 1100;

    }
</style>
<?php

    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl."/js/tabindex.js");
    $cs->registerScriptFile($baseUrl."/js/SaveAsync.js");

    if ($ajaxMode){
        $cs->scriptMap=array(
            'jquery.ba-bbq.js'=>false,
            'jquery.js'=>false,
            'jquery.yiigridview.js'=>false,
        );
    }else{

    }



Yii::app()->clientScript->registerScript('tooltip', "
        $(document).ready(function(){
            showClientView(".$model->Id_Cliente.");
            
            (function ($) {
              $.each(['show', 'hide'], function (i, ev) {
                var el = $.fn[ev];
                $.fn[ev] = function () {
                  this.trigger(ev);
                  return el.apply(this, arguments);
                };
              });
            })(jQuery);
            
            try{
                $('label').tooltip();
                $('input').tooltip();
            }catch(e){
                console.warn(e);
            }

        });
        
        function refreshFacturaDetails(){
            var datos = 'id=".$model->id."';
            $.ajax({
                url:'".$this->createUrl('Factura/Json')."',
                data: datos,
                type: 'GET',
                success: function (data){
                    let response = JSON.parse(data);
                    
                    $('#spanTotal').html(response.Total_Final);
                    $('#Ing_Brutos').html(response.Ing_Brutos);
                    $('#Otro_Iva').html(response.Otro_Iva);
                                        
                },                
            });
        }
        
        $('#ProdFactura_cantidad').keypress(function (ev) {
            $('#error_id_rubro').hide('slow');
            $('#ok_id_rubro').hide('slow');
             
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
            if(keycode == '43'){
                $('#modalBuscaArticulos').modal();
                return false;
            }else{
                return true;
            }
        })
        
        
        //Add custom handler on show event and print message
        $('#nuevoProdFactura').on('show', function(){
            $('#ProdFactura_cantidad').focus();
            
        });
        
        $('#ProdFactura_idArtVenta').change(function(event){
   
            let idArt = $(this).val();
            var datos = 'id='+idArt;
            $.ajax({
                url: '".$this->createUrl('ArticuloVenta/GetAsyncById')."',
                data: {
                    id: idArt,
                },
                type: 'GET',
                success: function(data){
                    
                    let response = JSON.parse(data);
                    console.log(response);

                    $('#ok_id_rubro').html(response.Rubro);
                    $('#ok_id_rubro').show('slow');		
                    
                    $('#ok_id_articulo').html(response.Articulo);
                    $('#ok_id_articulo').show('slow');                   
                    
                    $('#ProdFactura_descripcion').val(response.descripcion);
                    		
                    $('#ProdFactura_cantidad').focus();
                    $('#ProdFactura_precioUnitario').val(parseFloat(response.precio));
                    $('#ProdNotaPedido_idArtVenta').val(response.id_articulo_vta);					
                },
                error: function(x,y,z){
                    ev.stopPropagation();
                    ev.preventDefault();
                    $('#ProdNotaPedido_idArtVenta').focus();
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    var htmlError = $(errorDiv).html();
                    $('#error_id_rubro').html(htmlError);
                    $('#error_id_rubro').show('slow');
                }
            });
        });
        
        
         $('#idCae').click(function(e){
            //var datos = 'id='+$('#idCarga').val()+'&ajax=';
            e.stopPropagation();
            e.preventDefault();
            
            
            var datos = '';
            var url = $(this).attr('href');
            console.log(url);
                        
            $.ajax({
                url:url,
                type:'GET',
                data: datos,
                beforeSend: function() { $('.modalLoading').show('slow'); },
                complete: function() { 
                  $('.modalLoading').hide('slow'); 
                  $('#idCae').closest('.modal.fade.in').modal('hide');
                  $('.modalLoading').hide('slow');
                },
                success:function(html) {
                  //TODO: SE HACE ALGO CON EL html?
                  console.log(html);
                  return false;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  respuesta =  $.parseHTML(x.responseText);
                  errorDiv = $(respuesta).find('.errorSpan');
                  console.log(errorDiv);
                  var htmlError = $(errorDiv).html();
                  
                  console.log(htmlError);
                  alert(htmlError);
                  $('.modalLoading').hide('slow');
                  return false;
                  
                }
            });
        });
        
         $('#descartarFa').click(function(e){
            //var datos = 'id='+$('#idCarga').val()+'&ajax=';
            
            var datos = 'id='+$('#idCarga').val()+'&ajax=';
            var url = $(this).attr('url')+'&id='+$('#Factura_id').val()+'&ajax=';
            
            $.ajax({
                url:url,
                type:'POST',
                data: datos,
                success:function(html) {
                  //TODO: SE HACE ALGO CON EL html?
                  console.log(html);
                  $('#descartarNE').closest('.modal.fade.in').modal('hide');
                  return true;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  console.log(x);
                  alert(x.responseText);
                  return false;
                }
            });
        });
            
        function showClientView(param){
            $('#errorNotaPedido').hide('slow');
            $('#okNotaPedido').hide('slow');
    
            console.log('clSelected '+param);
            var datos = 'id='+param;
            $.ajax({
                url:'".$this->createUrl('Cliente/ViewFicha')."',
                type: 'Get',
                data: datos,
                success: function (html) {
                    $('#fichaCliente').html(html);
                },
                error: function (x,y,z){
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    console.log($(errorDiv).parent().next());
                    var htmlError = $(errorDiv).parent().next().html();
                    $('#errorNotaPedido').html(htmlError);
                    $('#errorNotaPedido').show('slow');
                }
            });
        }
        
            
   ",CClientScript::POS_READY);
?>
<div class="content-fluid">
    <div class="row-fluid bordeRedondo" style="font-style: italic;margin-bottom: 5px;" >
        <div class="span2">
            Id Cliente :<span ><?=$model->Id_Cliente?></span>
        </div>
        <div class="span3">
            Nro Pedio: <span ><?=($model->oNotaPedido)? $model->oNotaPedido->nroCombrobante.'-'.$model->oNotaPedido->dig : '' ?></span>
        </div>
        <div class="span2">
            Camion <span ><?=$model->Id_Camion?></span>
        </div>
        <div class="span2">
            Puesto <span ><?=$model->Nro_Puesto_Venta?></span>
        </div>
        <div class="span3">
            Factura Nº <span ><?=$model->Nro_Comprobante?></span>
        </div>
    </div>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span8" id="fichaCliente">

        </div>
        <div class="span4">
            <div id="modifFactura" style="">

                <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    'id'=>'factura-form',
                    'enableAjaxValidation'=>false,
                    'action' => Yii::app()->createUrl('Factura/SaveAsync'),
                    'htmlOptions' => array(
                        'class' => 'SaveAsyncForm',
                        'idGrilla'=>'factura-grid',
                        'idMas' => 'masFactura',
                        'idDivNuevo' => 'nuevoFactura',
                    ),
                )); ?>

                <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorFactura"></div>
                <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okFactura">Datos Guardados Correctamente !</div>

                <?php echo $form->errorSummary($model); ?>
                <div class="content-fluid">
                    <div class="content-fluid">
                        <div class="span12">
                            <?php
                                echo $form->labelEx($model,'Observaciones');
                                $this->widget('editable.EditableField', array(
                                    'type'      => 'textarea',
                                    'model'     => $model,
                                    'attribute' => 'Observaciones',
                                    'url'       => $this->createUrl('Factura/SaveFieldAsync'),
                                ));
                            ?>

                        </div>

                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <label for="clase">Clase</label> : <span name="clase" ><?=$model->clase?></span>
                    </div>
                    <div class="span4">
                        <label for="ivaResp">Iva Resp:</label> <span name="ivaResp"><?=$model->oCliente->oCategoriaIva->nombre; ?></span>
                    </div>
                    <div class="span4">
                        <?php
                        echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
                        $this->widget('editable.EditableField', array(
                            'type'        => 'date',
                            'model'       => $model,
                            'attribute'   => 'fecha',
                            'url'         => $this->createUrl('Factura/SaveFieldAsync'),
                            'placement'   => 'right',
                            'format'      => 'yyyy-mm-dd', //format in which date is expected from model and submitted to server
                            'viewformat'  => 'dd/mm/yyyy', //format in which date is displayed
                        ));
                        ?>


                    </div>

                </div>




                <?php $this->endWidget(); ?>
            </div>

        </div>
    </div>

</div>

<?php
if($model->CAE == '00000000000000' || $model->CAE == '') {
    ?>
    <input idNuevo="nuevoProdFactura" id="masProdFactura" type="button" class="btn SaveAsyncMas" value="Agregar producto"
           style="margin-bottom: 10px;">
    <div id="nuevoProdFactura" style="display: none;">

        <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
            'id' => 'prod-factura-form',
            'enableAjaxValidation' => false,
            'action' => Yii::app()->createUrl('ProdFactura/SaveAsync'),
            'htmlOptions' => array(
                'class' => 'SaveAsyncForm',
                'idGrilla' => 'prod-factura-grid',
                'idMas' => 'masProdFactura',
                'idDivNuevo' => 'nuevoProdFactura',
            ),
        )); ?>

        <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProdFactura"></div>
        <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProdFactura">Datos
            Guardados Correctamente !
        </div>

        <?php echo $form->errorSummary($modelProd); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                <div class="span2">
                    <div class="alert alert-block alert-error" style="display:none;" id="error_id_rubro"></div>
                    <div class="alert alert-block alert-success" style="display:none;" id="ok_id_rubro"></div>
                </div>
                <div class="span3">
                    <?php echo $form->hiddenField($modelProd, 'idFactura'); ?>
                    <?php echo $form->hiddenField($modelProd, 'fechaEntrega'); ?>
                    <?php echo $form->hiddenField($modelProd, 'nroPuestoVenta'); ?>
                    <?php echo $form->hiddenField($modelProd, 'cantidadEntregada'); ?>
                    <?php echo $form->hiddenField($modelProd, 'alicuotaIva'); ?>
                    <?php echo $form->hiddenField($modelProd, 'idArtVenta'); ?>
                    <div class="alert alert-block alert-error" style="display:none;" id="error_id_articulo"></div>
                    <div class="alert alert-block alert-success" style="display:none;" id="ok_id_articulo"></div>

                    <div class="alert alert-block alert-error" style="display:none;" id="error_id_rubro"></div>
                    <div class="alert alert-block alert-success" style="display:none;" id="ok_id_rubro"></div>
                </div>
                <div class="span2">
                    <?php echo $form->textFieldRow($modelProd, 'cantidad', array('class' => 'span12', 'tabindex' => 1, 'title' => 'Presionar + para abrir el buscador')); ?>
                </div>
                <div class="span2">
                    <?php echo $form->textFieldRow($modelProd, 'precioUnitario', array('class' => 'span12', 'tabindex' => 2)); ?>
                </div>
                <div class="span3">
                    <?php echo $form->textFieldRow($modelProd, 'descripcion', array('class' => 'span12', 'maxlength' => 40, 'tabindex' => 3)); ?>
                </div>

            </div>
        </div>

        <div class="form-actions">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => 'Agregar',
            )); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div>
    <?php
}
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-factura-grid',
    'dataProvider'=>$aProdFactura->searchWP(),
    'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");}',
    'template' => '{items}',
    'columns'=>array(
        array(
            'name' => 'codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => ''),
        ),
        'descripcion',

        'cantidad',
        array(
            'name' => 'precioUnitario',
            'value' => '"$".$data->precioUnitario',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'alicuotaIva',
            'value' => '$data->alicuotaIva."%"',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'importe',
            'value' => '"$".$data->importe',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        'fechaEntrega',
        'cantidadEntregada',
        'nroPuestoVenta',
        'descripcion',
        */
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} | {delete}',
            'buttons'=>array(
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("ProdFactura/update", array("id"=>$data->id))',
                    'options'=>array('target'=>'_blank'),
                ),
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("ProdFactura/delete", array("id"=>$data->id))',
                ),
            ),
        ),
        */
    ),
)); ?>

<?php $form2=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'nota-entrega-form2',
    'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('Factura/SaveAsync'),
    'htmlOptions' => array(
        //'class' => 'SaveAsyncForm',
    ),
)); ?>

<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span2">

        </div>
        <div class="span2">

        </div>
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>

        <div class="span1"> Descuento 1 </div>

        <div class="span1">Descuento2</div>
        <div class="span2">
            IIBB
        </div>
        <div class="span1">
            %Iva2
        </div>
        <div class="span2" ><b>Total $</b></div>

    </div>
    <div class="row-fluid" style="text-align: right;">
        <div class="span2"></div>
        <div class="span2"></div>
        <div class="span1" style="">
            <b>$<span id="spanSubTotal"><?=$model->SubTotal ?></span></b>
            <?php
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            ?>
        </div>
        <div class="span1">            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => '$model->CAE == "00000000000000" || $model->CAE == ""', //can't edit deleted users
                'attribute'   => 'Descuento1',
                'url'         => $this->createUrl('Factura/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshFacturaDetails();
                             }',
            ));

            ?></div>
        <div class="span1">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => '$model->CAE == "00000000000000" || $model->CAE == ""', //can't edit deleted users
                'attribute'   => 'Descuento2',
                'url'         => $this->createUrl('Factura/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
            ));

            ?></div>
        <div class="span2" id="Ing_Brutos">
            <?=number_format($model->Ing_Brutos,2, '.', '')?>
        </div>
        <div class="span1" id="Otro_Iva">
            $<?=number_format($model->Otro_Iva,2, '.', '')?>
        </div>
        <div class="span2" style=""><b>$<span id="spanTotal"><?=number_format($model->Total_Final,2, '.', '')?></span></b></div>
    </div>
</div>
<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span1" style=""></div>
        <div class="span1"></div>
        <div class="span1"></div>
        <div class="span2"></div>
        <div class="span1"></div>
        <div class="span2" style=""></div>

        <div class="span2">
            <?php
            if($model->esElectronica()){
                
                if($model->CAE == '00000000000000' || $model->CAE == ''){

                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idCae" href="'.Yii::app()->createUrl('afipVoucher/Emitir',array('id'=>$model->id)).'">Enviar y obtener CAE</a>';
                }else{

                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('factura/Pdf',array('id'=>$model->id)).'">Guardar e Imprimir.</a>';

                }


            }else{

                echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('factura/Pdf',array('id'=>$model->id)).'">Guardar e Imprimir</a>';

            };
            ?>

        </div>
        <div class="span2">

            <?php
                if($model->CAE == '00000000000000' || $model->CAE == '') {
                    //TODO: VER CUANDO SE PUEDE DESCARTAR Y CUANDO SOLO SE HACE NOTA DE CREDITO
                    echo CHtml::link('Descartar Factura', '#', array('class' => 'btn btn-danger', 'id' => 'descartarFa', 'url' => Yii::app()->createUrl('factura/delete'), 'style' => 'float:left;margin-top: 25px;'));
                }
            ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

<?php

/*
if(!$ajaxMode){

    $this->widget('BuscaArtVenta', array('js_comp'=>'#ProdFactura_idArtVenta','is_modal' => true,'ajaxMode' => $ajaxMode));
}else{

    echo '<div id="buscaArt"></div>';

}
*/

if(!$is_modal){
    $this->widget('BuscaArtVenta', array('js_comp'=>'#ProdFactura_idArtVenta','is_modal' => true,'ajaxMode' => true));
}else{
    echo '<div class="modalLoading" ><!-- Place at bottom of page --></div>';
    echo '<div id="buscaArt"></div>';

}


?>
