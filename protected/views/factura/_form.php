<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        $('#Factura_Id_Cliente').change(function (ev) {
		    console.log('#NotaPedido_idCliente');
		    clSelected($(this).val());
		});
		
		$('#Factura_Id_Cliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
            
		    if (keycode == '13') {
                //$('#prod-nota-pedido-form').submit();
                let datos = 'idCliente='+$(this).val();
                $.ajax({
                    url: '".$this->createUrl('Cliente/GetAsync')."',
                    data: datos,
                    type: 'GET',
                    success: function(html){
                        //$('#ok_id_cliente').html(html);
                        //$('#ok_id_cliente').show('slow');		
                        //$('#ArticuloVenta_id_articulo').focus();					
                    },
                    error: function(x,y,z){
                        ev.stopPropagation();
                        ev.preventDefault();
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        var htmlError = $(errorDiv).html();
                        $('#error_id_cliente').html(htmlError);
                        $('#error_id_cliente').show('slow');
                    }
                });
                return false;
            }else if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                
                return true;
            }
		});
    });
    
    function clSelected(param){
        $('#errorFactura').hide('slow');
		$('#okFactura').hide('slow');
		
		console.log('clSelected '+param);
		var datos = 'id='+param;
		$.ajax({
			url:'".$this->createUrl('Cliente/ViewFicha')."',
			type: 'Get',
            data: datos,
			success: function (html) {
               	$('#fichaCliente').html(html);
			},
			error: function (x,y,z){
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$('#errorNotaPedido').html(htmlError);
				$('#errorNotaPedido').show('slow');
			}
		});
	}

");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'factura-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorFactura"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okFactura">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'clase',array('class'=>'span12','maxlength'=>1,'tabindex' => 1)); ?>
		</div>
		<div class="span3">
            <?php
                echo $form->labelEx($model,'Nro_Puesto_Venta');
                echo $form->dropDownList($model,'Nro_Puesto_Venta',CHtml::listData(PuntoDeVenta::model()->findAll(),'num','descripcion'),array());
            ?>

		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12','maxlength'=>8,'tabindex' => 3)); ?>
		</div>
		<div class="span3">
            <?php
                echo $form->labelEx($model,'Iva_Responsable');
                echo $form->dropDownList($model,'Iva_Responsable',CHtml::listData(CategoriaIva::model()->findAll(),'id_categoria','nombre'),array());
            ?>

		</div>
    </div>
    <div class="row-fluid">
        <!-- --->
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Viajante',array('class'=>'span12','tabindex' => 5)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12','tabindex' => 6)); ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_cliente"></div>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'Factura[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));

            ?>



		</div>

        <div class="row-fluid" id="fichaCliente">

        </div>
    </div>
    <div class="row-fluid">
        <!-- --->
        <div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Neto',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textAreaRow($model,'Observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
		</div>

        <div class="span3">
            <?php
                echo $form->labelEx($model,'Id_Camion');
                echo $form->dropDownList($model,'Id_Camion',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array());
            ?>

        </div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento1',array('class'=>'span12')); ?>
		</div>
    </div>
    <div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento2',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Final',array('class'=>'span12')); ?>
		</div>

    </div>
    <div class="row-fluid">
        <div class="span3">
			<?php echo $form->textFieldRow($model,'Otro_Iva',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Ing_Brutos',array('class'=>'span12')); ?>
		</div>

	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<div id='prods'>
</div>
<?php
$this->widget('BuscaCliente', array('js_comp'=>'#Factura_Id_Cliente','is_modal' => true));
?>