     <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'clase',array('class'=>'span12','maxlength'=>1)); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Nro_Puesto_Venta',array('class'=>'span12')); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12','maxlength'=>8)); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Iva_Responsable',array('class'=>'span12','maxlength'=>1)); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Nro_Viajante',array('class'=>'span12')); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
         </div>
        <div class="span4">
		    <?php echo $form->textFieldRow($model,'Moneda',array('class'=>'span12','maxlength'=>8)); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Total_Neto',array('class'=>'span12')); ?>
         </div>
        <div class="span4">
		    <?php echo $form->textAreaRow($model,'Observaciones',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
         </div>
        <div class="span4">
		    <?php echo $form->textFieldRow($model,'Id_Camion',array('class'=>'span12')); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Descuento1',array('class'=>'span12')); ?>
         </div>
        <div class="span4">
		    <?php echo $form->textFieldRow($model,'Descuento2',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Total_Final',array('class'=>'span12')); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Enviada',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Recibida',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Otro_Iva',array('class'=>'span12')); ?>
         </div>
     </div>
     <div class="row-fluid">
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'CAE',array('class'=>'span12','maxlength'=>14)); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Vencim_CAE',array('class'=>'span12')); ?>
         </div>
         <div class="span4">
		    <?php echo $form->textFieldRow($model,'Ing_Brutos',array('class'=>'span12')); ?>
         </div>
     </div>

            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'=>'primary',
                'label'=>'Buscar',
            )); ?>


<?php $this->endWidget(); ?>
