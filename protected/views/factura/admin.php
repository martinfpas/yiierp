<?php
/* @var $this FacturaController */
/* @var $model Factura */

$this->breadcrumbs=array(
	'Facturas'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nueva Factura', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#factura-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Facturas</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'factura-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(

        array(
            'name' => 'Id_Cliente',
            'value' => '$data->oCliente->title',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(Cliente::model()->todos()->findAll(),'id','title'),

        ),

        array(
            'name' => 'clase',
			'htmlOptions' => array('style' => 'width:50px;text-align:center'),
            'headerHtmlOptions' => array('style' => 'width:65px;text-align:center;'),
            'filter' => Factura::$aClase,
		),

        array(
            'name' => 'Nro_Puesto_Venta',
            'value' => '$data->oPuestoVenta->num',
            'header' => 'pto.vta',
            'filter' =>  CHtml::listData(PuntoDeVenta::model()->findAll(),'num','num'),
        ),
		'Nro_Comprobante',

        array(
            'name' => 'Iva_Responsable',
            'value' => '$data->oIvaResponsable->nombre',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(CategoriaIva::model()->findAll(),'id_categoria','nombre'),

        ),

        array(
            'name' => 'Total_Final',
            'value' => '"$".number_format($data->Total_Final,2)',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
        ),




		/*
		 * array(
            'name' => 'Nro_Viajante',
			'header' => 'Viajante',
			'filter' => CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),
			'htmlOptions' => array('style' => ''),
		),
		'Id_Cliente',
		'fecha',
		'Moneda',
		'Total_Neto',
		'Observaciones',
		'Id_Camion',
		'Descuento1',
		'Descuento2',

		'Enviada',
		'Recibida',
		'Otro_Iva',
		'CAE',
		'Vencim_CAE',
		'Ing_Brutos',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} | {update}', //{delete}
            'buttons'=>array(
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("factura/update", array("id"=>$data->id))',
                    'visible' => '$data->CAE == "00000000000000" || $data->CAE == ""',
                ),
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("Factura/delete", array("id"=>$data->id))',
                ),
            ),
        ),

	),
)); ?>
