<?php
/* @var $this AfipVoucherController */
/* @var $data AfipVoucher */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroPuntoVenta')); ?>:</b>
	<?php echo CHtml::encode($data->numeroPuntoVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoTipoComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->codigoTipoComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoTipoDocumento')); ?>:</b>
	<?php echo CHtml::encode($data->codigoTipoDocumento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoConcepto')); ?>:</b>
	<?php echo CHtml::encode($data->codigoConcepto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroDocumento')); ?>:</b>
	<?php echo CHtml::encode($data->numeroDocumento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('caea')); ?>:</b>
	<?php echo CHtml::encode($data->caea); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('numeroComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->numeroComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaComprobante')); ?>:</b>
	<?php echo CHtml::encode($data->fechaComprobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeOtrosTributos')); ?>:</b>
	<?php echo CHtml::encode($data->importeOtrosTributos); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaDesde')); ?>:</b>
	<?php echo CHtml::encode($data->fechaDesde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaHasta')); ?>:</b>
	<?php echo CHtml::encode($data->fechaHasta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fechaVtoPago')); ?>:</b>
	<?php echo CHtml::encode($data->fechaVtoPago); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoMoneda')); ?>:</b>
	<?php echo CHtml::encode($data->codigoMoneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cotizacionMoneda')); ?>:</b>
	<?php echo CHtml::encode($data->cotizacionMoneda); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeIVA')); ?>:</b>
	<?php echo CHtml::encode($data->importeIVA); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeNoGravado')); ?>:</b>
	<?php echo CHtml::encode($data->importeNoGravado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeExento')); ?>:</b>
	<?php echo CHtml::encode($data->importeExento); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeGravado')); ?>:</b>
	<?php echo CHtml::encode($data->importeGravado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeTotal')); ?>:</b>
	<?php echo CHtml::encode($data->importeTotal); ?>
	<br />

	*/ ?>

</div>