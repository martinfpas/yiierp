<?php
$this->breadcrumbs=array(
	'Afip Vouchers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucher', 'url'=>array('create')),
	array('label'=>'Ver AfipVoucher', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de AfipVoucher', 'url'=>array('admin')),
);
?>

	<h1>Modificando AfipVoucher <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array(
											'model'=>$model,
											'aTipoDocDatos' => $aTipoDocDatos,
		
)); ?>