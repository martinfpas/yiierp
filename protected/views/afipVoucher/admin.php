<?php
/* @var $this AfipVoucherController */
/* @var $model AfipVoucher */

$this->breadcrumbs=array(
	'Afip Vouchers'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucher', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#afip-voucher-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Afip Vouchers</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'afip-voucher-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'numeroPuntoVenta',
		'codigoTipoComprobante',
		'codigoTipoDocumento',
		'codigoConcepto',
		'numeroDocumento',
		/*
		'caea',
		'numeroComprobante',
		'fechaComprobante',
		'importeOtrosTributos',
		'fechaDesde',
		'fechaHasta',
		'fechaVtoPago',
		'codigoMoneda',
		'cotizacionMoneda',
		'importeIVA',
		'importeNoGravado',
		'importeExento',
		'importeGravado',
		'importeTotal',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
