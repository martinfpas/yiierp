<?php
$this->breadcrumbs=array(
	'Afip Vouchers'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucher', 'url'=>array('create')),
	array('label'=>'Modificar AfipVoucher', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar AfipVoucher', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de AfipVoucher', 'url'=>array('admin')),
);
?>

<h1>Ver AfipVoucher #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'numeroPuntoVenta',
		'codigoTipoComprobante',
		'codigoTipoDocumento',
		'codigoConcepto',
		'numeroDocumento',
		'caea',
		'numeroComprobante',
		'fechaComprobante',
		'importeOtrosTributos',
		'fechaDesde',
		'fechaHasta',
		'fechaVtoPago',
		'codigoMoneda',
		'cotizacionMoneda',
		'importeIVA',
		'importeNoGravado',
		'importeExento',
		'importeGravado',
		'importeTotal',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
