<?php
/* @var $this AfipVoucherController */
/* @var $model AfipVoucher */

$this->breadcrumbs=array(
	'Afip Vouchers'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de AfipVoucher', 'url'=>array('admin')),
);
?>

<h1>Creando AfipVoucher</h1>

<?php echo $this->renderPartial('_form',array(
											'model'=>$model,
											'aTipoDocDatos' => $aTipoDocDatos,
		
)); ?>