
/* @var $this AfipVoucherController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Afip Vouchers',
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucher', 'url'=>array('create')),
	array('label'=>'Gestion de AfipVoucher', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>