<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'afip-voucher-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorAfipVoucher"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okAfipVoucher">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'numeroPuntoVenta',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoTipoComprobante',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php
				//$aTipoDocDatos
				echo $form->labelEx($model,'codigoTipoDocumento');
				//,array('class'=>'span12')
				echo $form->dropDownList($model,'codigoTipoDocumento',$aTipoDocDatos,array('empty'=>'--Seleccionar--')); 
			?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoConcepto',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'numeroDocumento',array('class'=>'span12','maxlength'=>11)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'caea',array('class'=>'span12','maxlength'=>14)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'numeroComprobante',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaComprobante',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeOtrosTributos',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaDesde',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaHasta',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaVtoPago',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoMoneda',array('class'=>'span12','maxlength'=>3)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cotizacionMoneda',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeIVA',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeNoGravado',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeExento',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeGravado',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeTotal',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
