<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'numeroPuntoVenta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'codigoTipoComprobante',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'codigoTipoDocumento',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'codigoConcepto',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'numeroDocumento',array('class'=>'span12','maxlength'=>11)); ?>

		<?php echo $form->textFieldRow($model,'caea',array('class'=>'span12','maxlength'=>14)); ?>

		<?php echo $form->textFieldRow($model,'numeroComprobante',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'fechaComprobante',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'importeOtrosTributos',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'fechaDesde',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'fechaHasta',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'fechaVtoPago',array('class'=>'span12','maxlength'=>8)); ?>

		<?php echo $form->textFieldRow($model,'codigoMoneda',array('class'=>'span12','maxlength'=>3)); ?>

		<?php echo $form->textFieldRow($model,'cotizacionMoneda',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importeIVA',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importeNoGravado',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importeExento',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importeGravado',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importeTotal',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
