<style>
    .span6.left th {
        width: 100px;
    }

    .row-fluid.bordeRedondo.footer th {
        width: 100px;
    }
</style>
<?php
/**
 * @var $model ComprobanteProveedor
 * @var $aPago2FactProv Pago2FactProv
 */

$this->breadcrumbs=array(
	ComprobanteProveedor::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva '.ComprobanteProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.ComprobanteProveedor::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.ComprobanteProveedor::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.ComprobanteProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);

$cs = Yii::app()->getClientScript();
$cs->registerScript('viewFP',"
    
    $(document).ready(function(){
        getProveedor($model->id_Proveedor);
    });
    
    function getProveedor(id_Proveedor){
        var datos = 'id='+id_Proveedor;
        $.ajax({
            url:'".$this->createUrl('proveedor/viewFicha')."',
            data: datos,
            type: 'GET',
            success: function (html){
                $('#fichaProveedor').html(html);                
                return false;                                    
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
                $('#idProveedor').html('');
            }                
        });
    }
");

function IvaFooterHtml($aIvaFooter){
    $sHtml = '';
    foreach($aIvaFooter as $key => $value){
        $sHtml .= '<span>'.$value['value'].'</span><br>';
    }
    return $sHtml;
}
function IvaFooterTitleHtml($aIvaFooter){
    $sHtml = '';
    foreach($aIvaFooter as $key => $value){
        $sHtml .= '<span>'.$value['name'].'</span><br>';
    }
    return $sHtml;
}

?>

<h3>Ver <?=ComprobanteProveedor::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6 left">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'MesCarga',
                    array(
                        'name' => 'FechaCarga',
                        'value' => ComponentesComunes::fechaFormateada($model->FechaCarga),
                        'header' => 'Fecha de Carga',
                    ),
                    'Nro_Comprobante',
                ),
            )); ?>
            <div id="fichaProveedor">

            </div>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(

                    array(
                        'name' => 'FechaFactura',
                        'value' => ComponentesComunes::fechaFormateada($model->FechaFactura),
                        'header' => 'Fecha',
                    ),

                    array(
                        'name' => 'Clase',
                        'value' => $model->oCuentaGastos->denominacion,
                    ),
                    'Nro_Puesto_Vta',
                    array(
                        'name' => 'id_Cuenta',
                        'value' => $model->oCuentaGastos->id.' - '.$model->oCuentaGastos->denominacion,
                        'header' => 'Cuenta',
                    ),
                    array(
                        'name' => 'TipoComprobante',
                        'value' => ComprobanteProveedor::$aClase[$model->TipoComprobante],

                    ),
                    array(
                        'name' => 'Fecha_Vencimiento',
                        'value' => ComponentesComunes::fechaFormateada($model->Fecha_Vencimiento),
                        'header' => 'Vencimiento',
                    ),

                    'Cant_alicuotas_iva',
                    array(
                        'name' => 'montoPagado',
                        'value' => "$".number_format($model->montoPagado,2),
                        'header' => '',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                    /*
                        array(
                            'name' => '',
                            'value' => '',
                            'header' => '',
                            'htmlOptions' => array('style' => ''),
                            'headerHtmlOptions' => array('style' => ''),
                        ),
                    */
                ),
            )); ?>

        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <?php

            $dataProvider=new CArrayDataProvider($model->aDetalleFactura,array(
                //'sort'=>array('attributes'=>array('id','name','age','sex'),),
                'pagination'=> false,
                )
            );

            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'detalle-factura-prov-grid',
                'dataProvider'=>$dataProvider,
                'columns'=>array(
                    'nroItem',
                    'idArticulo',
                    'descripcion',
                    array(
                        'name' => 'alicuota',
                        'value' => 'ComprobanteProveedor::$aIvaAlicuotasMontos[$data->alicuota]."%"',
                        'htmlOptions' => array('style' => 'text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                    ),
                    //
                    array(
                        'name' => 'precioUnitario',
                        'value' => '"$".number_format($data->precioUnitario,2)',
                        'header' => 'Precio',
                        'htmlOptions' => array('style' => 'text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                    ),
                    array(
                        'name' => 'cantidadRecibida',
                        'header' => 'Cant.',
                        'htmlOptions' => array('style' => 'text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                    ),

                    array(
                        'name' => 'Subtotal',
                        'value' => '"$".$data->Subotal',
                        'htmlOptions' => array('style' => 'text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                    ),


                ),
            ));
            ?>
        </div>
    </div>

    <div class="row-fluid bordeRedondo footer" style="margin-top: 5px;">

        <div class="span2">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'Subtotal',
                        'value' => $model->getSubTotalItems(),
                        //'header' => 'No gravado',
                    ),
                    array(
                        'name' => 'Descuento',
                        'value' => '$'.number_format($model->Descuento,2),
                        //'header' => 'No gravado',
                    ),
                    /*
                        array(
                            'name' => '',
                            'value' => '',
                            'header' => '',
                            'htmlOptions' => array('style' => ''),
                            'headerHtmlOptions' => array('style' => ''),
                        ),
                    */
                ),
            )); ?>
        </div>
        <div class="span2">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'label' => IvaFooterTitleHtml($aIvaFooter),
                        'type' => 'raw',
                        'value' => IvaFooterHtml($aIvaFooter),
                    ),
                    array(
                        'name' => 'IngBrutos',
                        'value' => '$'.number_format($model->IngBrutos,2),
                        //'header' => 'No gravado',
                    ),
                ),
            )); ?>
        </div>
        <div class="span2">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'Rg3337',
                        'value' => '$'.number_format($model->Rg3337,2),
                        //'header' => 'No gravado',
                    ),
                    array(
                        'name' => 'PercepIB',
                        'value' => '$'.number_format($model->PercepIB,2),
                        //'header' => 'No gravado',
                    ),

                ),
            )); ?>
        </div>
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'ImpGanancias',
                        'value' => '$'.number_format($model->ImpGanancias,2),
                        //'header' => 'No gravado',
                    ),
                    'Cai',
                ),
            )); ?>
        </div>
        <div class="span3">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'Nograv',
                        'value' => '$'.number_format($model->Nograv,2),
                        'header' => 'No gravado',
                    ),
                    array(
                        'name' => 'Total',
                        'value' => '$'.number_format($model->TotalNeto,2),
                        'header' => 'Total',
                    ),
                    /*
                        array(
                            'name' => '',
                            'value' => '',
                            'header' => '',
                            'htmlOptions' => array('style' => ''),
                            'headerHtmlOptions' => array('style' => ''),
                        ),
                    */
                ),
            )); ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <br>
            <h4>Pagos:</h4>

            <?php


            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago2-fact-prov-grid',
                'dataProvider'=>$aPago2FactProv->search(),
                'columns'=>array(
                    array(

                        'value' => '$data->oPagoProveedor->TextMedios',
                        'header' => 'Medio',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'monto',
                        'value' => '"$".number_format($data->oPagoProveedor->monto,2,",",".")',
                    ),
                    array(
                        'name' => 'fecha',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oPagoProveedor->fecha)',
                    ),
                    /*
                    array(
                        'name' => '',
                        'value' => '',
                        'header' => '',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                    */
                    array(
                        'class'=>'bootstrap.widgets.TbButtonColumn',
                        'template' => '{view}',
                        'buttons'=>array(
                            'view' => array(
                                'label' => 'Ver Pago',
                                'url'=>'Yii::app()->controller->createUrl("PagoProveedor/view", array("id"=>$data->idPagoProveedor))',
                                'options'=>array('target'=>'_blank'),
                            ),
                        ),
                    ),
                ),
            )); ?>
        </div>
    </div>
</div>


