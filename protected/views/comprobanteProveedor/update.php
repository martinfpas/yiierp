<?php
/** @var $model ComprobanteProveedor */
$this->breadcrumbs=array(
	ComprobanteProveedor::model()->getAttributeLabel('models') => array('admin'),
	ComprobanteProveedor::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.ComprobanteProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.ComprobanteProveedor::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.ComprobanteProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=ComprobanteProveedor::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
<?php
    echo $this->renderPartial('_formFila',
        array(
            'model'=>$model,
            'model' => $oDetalleComprobanteProv,
            'aDetalleComprobanteProv' => $aDetalleComprobanteProv,
        )
    );
?>

<div class="container-fluid bordeRedondo">
    <div class="row-fluid">
        <div class="span1" style="width: 9%;"><?=CHtml::label('Subtotal','Subtotal')?></div>
        <div class="span1"  style="width: 9%;"><?=CHtml::label('Descuento','Descuento')?></div>
        <div  class="span1" style="width: 10%;"><?=CHtml::label('Iva','Iva')?></div>
        <div class="span1"  style="width: 8%;"><?=CHtml::label('No Grav','Nograv')?></div>
        <div class="span1"  style="width: 8%;"><?=CHtml::label('Ing Brutos','IngBrutos')?></div>
        <div class="span1"  style="width: 8%;"><?=CHtml::label('Rg3337','RG3337')?></div>
        <div class="span1"  style="width: 8%;"><?=CHtml::label('PercIB','PercepIB')?></div>
        <div  class="span1" style="width: 9%;"><?=CHtml::label('Imp Gcias','ImpGanancias')?></div>
        <div  class="span1" style="width: 9%;"><?=CHtml::label('Total Factura','TotalNeto')?></div>
    </div>
    <div class="row-fluid">
        <div style="width: 12%;float: left;" id="Subtotal">
            $ <?=$model->getSubTotalItems()?>
        </div>
        <div style="width: 10%;float: left;">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'Descuento',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 13%;float: left;" id="alicuotas">
            <?php
            foreach ($model->aAlicuotasCompra as $index => $aIva) {
                echo '<span>'.number_format($aIva->alicuota,2).'% : $'.number_format($aIva->impuestoLiquidado,2).'</span><br>';
            }
            ?>
        </div>
        <div style="width: 10%;float: left;">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'Nograv',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 10%;float: left; ">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'IngBrutos',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 11%;float: left;">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'Rg3337',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 11%;float: left;">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'PercepIB',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 9%;float: left">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'mode'      => ($is_modal)? 'inline' : 'popup',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'attribute'   => 'ImpGanancias',
                'url'         => $this->createUrl('ComprobanteProveedor/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html("$ "+parseFloat(value).toFixed(2));								
								}else{
									$(this).html("$ 0.00");							
								}
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?>
        </div>
        <div style="width: 9%;float: left;text-align: right;font-weight: bold" id="TotalFactura">
            $ <?=number_format($model->TotalNeto,2)?>
        </div>
    </div>
    <div class="row-fluid" style="margin-top: 10px">
        Terminar - Descartar
    </div>
</div>