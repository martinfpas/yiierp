<?php
/* @var $this ComprobanteProveedorController */
/* @var $model ComprobanteProveedor */
?>

<h3>Gestion de <?=ComprobanteProveedor::model()->getAttributeLabel('models') ?></h3>


<?php
$cs = Yii::app()->getClientScript();
$cs->scriptMap=array(
    'jquery.ba-bbq.js'=>false,
    'jquery.js'=>false,
    //'jquery.yiigridview.js'=>false,
);

$this->widget('ext.selgridview.BootSelGridView',array(
'id'=>'factura-proveedor-grid',
'dataProvider'=>$model->searchWP(),
'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");return true;}',
'selectableRows' => 2,
'enableSorting' => false,
'columns'=>array(
        array(
            'class' => 'CCheckBoxColumn',
        ),
		'Nro_Comprobante',
        array(
            'name' => 'TipoComprobante',
            'value' => 'ComprobanteProveedor::$aClase[$data->TipoComprobante]',
            'filter' => ComprobanteProveedor::$aClase,
            //'header' => 'Cuenta',
            //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
        array(
            'filter' => CHtml::listData(Proveedor::model()->todos()->findAll(),'id','Title'),
            'name' => 'id_Proveedor',
            'value' => '$data->oProveedor->Title',
            'header' => 'Proveedor',
            /*
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            */
        ),

        array(
            'name' => 'TotalNeto',
            'value' => 'number_format($data->TotalNeto,2)',
            'header' => 'Importe',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),

        array(
            'name' => 'FechaFactura',
            'value' => 'ComponentesComunes::fechaFormateada($data->FechaFactura)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'name' => 'MesCarga',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        array(
            'name' => 'montoPagado',
            'value' => 'number_format($data->montoPagado,2)',
            'header' => 'Pagado',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		

	),
)); ?>
