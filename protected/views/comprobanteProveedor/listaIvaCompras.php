<?php
/* @var $this ComprobanteController */
/* @var $model ComprobanteProveedor */

?>

<h3>Listado de IVA COMPRAS del mes <?=$mes?> </h3>

<?php

$dataProvider=new CArrayDataProvider($aModels,array(
        'pagination'=> false,
    )
);

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'comprobante-grid',
'dataProvider'=>$dataProvider,
    'template' => '{items}',
    'columns'=>array(
        array(
            'name' => 'fecha',
            'header' => 'FECHA',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'numero',
            'header' => 'NUMERO',
            //'value' => '"*".$data[\'numero\']."*"',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'doc',
            'header' => 'DOC',
            'htmlOptions' => array('style' => 'text-align:center;width:30px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:30px;'),
        ),

        array(
            'name' => 'Proveedor',
            'header' => 'Proveedor',
            'htmlOptions' => array('style' => 'width:140px;'),
            'headerHtmlOptions' => array('style' => 'width:140px;'),
            'footer' => '***TOTALES.....',
            'footerHtmlOptions' => array('style' => 'margin-top:15px;padding-top:20px;'),

        ),
        array(
            'name' => 'cuit',
            'header' => 'CUIT',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),

        array(
            'name' => 'netoGrav',
            'header' => 'NETO GRAV.',
            'value' => 'number_format($data["netoGrav"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['netoGrav'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),

        ),
        array(
            'name' => 'noGrav',
            'header' => 'NO GRAV.',
            'value' => 'number_format($data["noGrav"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['noGrav'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'iva',
            'header' => 'IVA',
            'value' => 'number_format($data["iva"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['iva'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'iibb',
            'header' => 'IIBB',
            'value' => 'number_format($data["iibb"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['iibb'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),


        array(
            'name' => 'percRet',
            'header' => 'Rg3337',
            'value' => 'number_format($data["percRet"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['percRet'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),


        array(
            'name' => 'total',
            'header' => 'TOTAL',
            'value' => 'number_format($data["total"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
            'footer' => '$'.number_format($aTotales['total'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

	),
));

echo '<br><br>NETO GRAV:'.number_format($aTotales['netoGrav'],2,',','.');
echo '<br>IVA:'.number_format($aTotales['iva'],2,',','.');
echo '<br>PREC/RET:'.number_format($aTotales['percRet'],2,',','.') ;
echo '<br>NO GRAV:'.number_format($aTotales['noGrav'],2,',','.');
echo '<br>IIBB:'.number_format($aTotales['iibb'],2,',','.');
echo '<br>Imp Ganancias:'.number_format($aTotales['impGanancias'],2,',','.') ;
echo '<br>Total:'.number_format($aTotales['total'],2,',','.') ;

?>


