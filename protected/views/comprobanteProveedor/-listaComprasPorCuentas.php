<?php
/* @var $this ComprobanteController */
/* @var $model ComprobanteProveedor */

?>

<h3>Listado de COMPRAS por Cuenta del mes <?=$model->MesCarga?> </h3>

<?php
$aTotales = array(
    'Subtotal' => 0,
    'Nograv' => 0,
    'Iva' => 0,
    'IngBrutos' => 0,
    'Rg3337' => 0,
    'TotalNeto' => 0,
);
$aTotalesPorCuenta = array();
foreach ($model->searchWP()->getData() as $key => $data) {
    $aTotales['NetoGrav']   += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->getNetoGravadoConDescuento();
    $aTotales['Nograv']     += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Nograv;
    $aTotales['Iva']        += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Iva;
    $aTotales['IngBrutos']  += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->IngBrutos;
    $aTotales['Rg3337']     += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Rg3337 ;
    $aTotales['TotalNeto']  += $data->getTotalConFactor();
    
    if(!isset($aTotalesPorCuenta[$data->id_Cuenta])){
        $aTotalesPorCuenta[$data->id_Cuenta]['denominacion']= $data->oCuentaGastos->denominacion;
        $aTotalesPorCuenta[$data->id_Cuenta]['id_Cuenta']= $data->id_Cuenta;
        $aTotalesPorCuenta[$data->id_Cuenta]['NetoGrav']    = 0;
        $aTotalesPorCuenta[$data->id_Cuenta]['Nograv']      = 0;
        $aTotalesPorCuenta[$data->id_Cuenta]['Iva']         = 0;
        $aTotalesPorCuenta[$data->id_Cuenta]['IngBrutos']   = 0;
        $aTotalesPorCuenta[$data->id_Cuenta]['Rg3337']      = 0;
        $aTotalesPorCuenta[$data->id_Cuenta]['TotalNeto']   = 0;
    }

    $aTotalesPorCuenta[$data->id_Cuenta]['NetoGrav']    += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->getNetoGravadoConDescuento();
    $aTotalesPorCuenta[$data->id_Cuenta]['Nograv']      += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Nograv;
    $aTotalesPorCuenta[$data->id_Cuenta]['Iva']         += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Iva;
    $aTotalesPorCuenta[$data->id_Cuenta]['IngBrutos']   += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->IngBrutos;
    $aTotalesPorCuenta[$data->id_Cuenta]['Rg3337']      += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->Rg3337;
    $aTotalesPorCuenta[$data->id_Cuenta]['TotalNeto']   += ComprobanteProveedor::$aFactorMultip[$data->TipoComprobante] * $data->getTotalConFactor();
}

$aData = array();
$i = 0;
foreach($aTotalesPorCuenta as $key => $value ){
    $aData[$i] = $value;
    $i++;
}

$dataProvider=new CArrayDataProvider($aData,array(
    'pagination'=> false,
));

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'comprobante-grid',
'dataProvider'=>$dataProvider,
    'template' => '{items}',
    'enablePagination' => false,
    'enableSorting' => false,
    'columns'=>array(
        array(
            'name' => 'id_Cuenta',
            'header' => 'Cuenta',
            'value' => '$data["id_Cuenta"]', 
            'htmlOptions' => array('style' => 'width:48px;'),
            'headerHtmlOptions' => array('style' => 'text-align:left;width:48px;text-decoration:underline;'),
        ),

        array(
            'name' => 'Cuenta',
            'header' => 'Cuenta',
            'value' => '$data["denominacion"]',
            'htmlOptions' => array('style' => 'width:240px;'),
            'headerHtmlOptions' => array('style' => 'width:240px;text-decoration:underline;'),
        ),


        array(
            'name' => 'NetoGrav',
            'header' => 'NETO GRAV.',
            'value' => 'number_format($data["NetoGrav"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['NetoGrav'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'Nograv',
            'header' => 'NO GRAV.',
            'value' => 'number_format($data["Nograv"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['Nograv'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'Iva',
            'header' => 'IVA',
            'value' => 'number_format($data["Iva"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['Iva'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'IngBrutos',
            'header' => 'IIBB',
            'value' => 'number_format($data["IngBrutos"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['IngBrutos'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),


        array(
            'name' => 'Rg3337',
            'header' => 'Rg3337',
            'value' => 'number_format($data["Rg3337"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['Rg3337'],2).' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),


        array(
            'name' => 'TotalNeto',
            'header' => 'Total',
            'value' => 'number_format($data["TotalNeto"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;text-decoration:underline;'),
            'footer' => number_format($aTotales['TotalNeto'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

	),
));
/*
echo '<br><br>NETO GRAV:'.number_format($aTotales['netoGrav'],2,',','.');
echo '<br>IVA:'.number_format($aTotales['iva'],2,',','.');
echo '<br>PREC/RET:'.number_format($aTotales['percRet'],2,',','.') ;
echo '<br>NO GRAV:'.number_format($aTotales['noGrav'],2,',','.');
echo '<br>IIBB:'.number_format($aTotales['total'],2,',','.');
echo '<br>Imp Ganancias:'.number_format($aTotales['impGanancias'],2,',','.') ;
echo '<br>Total:'.number_format($aTotales['total'],2,',','.') ;
*/

?>


