<?php
/* @var $this ComprobanteProveedorController */
/* @var $model ComprobanteProveedor */

$this->breadcrumbs=array(
	ComprobanteProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

//ComponentesComunes::print_array($model->attributes);

$this->menu=array(
	array('label'=>'Gestion de '.ComprobanteProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);

?>
<h3>Nueva <?=ComprobanteProveedor::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>


