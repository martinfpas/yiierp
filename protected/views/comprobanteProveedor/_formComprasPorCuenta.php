<?php
/** @var  $model ComprobanteProveedor */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$cs->registerScript('iva',"
    $(document).ready(function(){
        $('#ComprobanteProveedor_MesCarga').focus();
    });

");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'comprobante-form',
	'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('ComprobanteProveedor/ListadoComprasPorCuentas'),
));

?>

<h3>Listado de Compras Por Cuentas</h3>
<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
		<div class="span2">
			<?php

                echo $form->labelEx($model,'MesCarga');

                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->MesCarga),
                    'name'=>'ComprobanteProveedor[MesCarga]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'model' => $model,
                    'attribute' => 'MesCarga',
                    'mask' => '99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;width:75px;',
                        'class'=>'span12',
                        'tabindex' => 2
                    ),
                ));

            ?>
		</div>
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=> 'Filtrar',
                'htmlOptions' => array(
                    'style'=> 'margin-top:25px;'
                ),
            ));
            ?>
            </div>
    </div>
</div>


<?php $this->endWidget(); ?>
