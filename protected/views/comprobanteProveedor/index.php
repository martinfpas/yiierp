
/* @var $this ComprobanteProveedorController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"ComprobanteProveedor::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo ComprobanteProveedor', 'url'=>array('create')),
	array('label'=>'ABM ComprobanteProveedor', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>