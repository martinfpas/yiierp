<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Usuarios'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Usuario', 'url'=>array('create')),
	array('label'=>'Modificar Usuario:'.$model->username, 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Usuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'ESTA SEGURO QUE DESEA BORRAR EL USUARIO???')),
	array('label'=>'Administrar Usuarios', 'url'=>array('admin')),
);
?>

<h1>Usuario #<?php echo $model->id; ?> : <?php echo $model->username; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'first_name',
		'last_name',
		'email',
		'status',
		'created_date',
		'last_login',
		'user_type',
        array(
            'name' => 'puntoDeVenta',
            'value' => ($model->oPuntoDeVenta)? $model->oPuntoDeVenta->num.' - '.$model->oPuntoDeVenta->descripcion : '',
        ),
	),
)); ?>
<div class="content-fluid">
	<div class="row-fluid">
		<div class="form-actions span6" action="<?=Yii::app()->createUrl('user/resetPass',array('id'=>$model->id))?>">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=> 'Cambiar password',
					'url' => Yii::app()->createUrl('user/resetPass',array('id'=>$model->id)),
				)); ?>
		</div>	
		<div class="form-actions span6" action="<?=Yii::app()->createUrl('authItem/EditRelations',array('id'=>$model->id))?>">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=> 'Establecer permisos',
					'url' => Yii::app()->createUrl('authItem/EditRelations',array('id'=>$model->id)),
				)); ?>
		</div>	
	</div>
</div>



