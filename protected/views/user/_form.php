<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php 

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->labelEx($model,'username'); ?>
			<?php echo $form->textField($model,'username',array('size'=>60,'maxlength'=>100,'class'=>'span12','tabindex'=>'1')); ?>
			<?php echo $form->error($model,'username'); ?>		
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'first_name'); ?>
			<?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>100,'class'=>'span12','tabindex'=>'2')); ?>
			<?php echo $form->error($model,'first_name'); ?>		
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'last_name'); ?>
			<?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>100,'class'=>'span12','tabindex'=>'3')); ?>
			<?php echo $form->error($model,'last_name'); ?>		
		</div>
		<div class="span3">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>250,'class'=>'span12','tabindex'=>'4')); ?>
			<?php echo $form->error($model,'email'); ?>		
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->labelEx($model,'status'); ?>
			<?php echo $form->dropDownList($model,'status', array('active'=>'Activo', 'inactivo'=>'Inactivo'), array('prompt' => '-- Seleccionar --', 'class'=>'campos2', 'style'=>'width:200px;','class'=>'span12','tabindex'=>'5'));?>
			<?php echo $form->error($model,'status'); ?>
		</div>
	
		<div class="span3">
			<?php echo $form->labelEx($model,'user_type'); ?>
			<?php echo $form->dropDownList($model,'user_type', array('administrativo'=>'Administrativo', 'administrador'=>'Admin'), array('prompt' => '-- Seleccionar --', 'class'=>'campos2', 'style'=>'width:200px;','class'=>'span12','tabindex'=>'6'));?>
			<?php echo $form->error($model,'user_type'); ?>
		</div>
	
		<div class="span3">
			<?php echo $form->labelEx($model,'telephone'); ?>
			<?php echo $form->textField($model,'telephone',array('size'=>60,'maxlength'=>100,'class'=>'span12','tabindex'=>'7')); ?>
			<?php echo $form->error($model,'telephone'); ?>
		</div>
	
		<div class="span3">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<?php echo $form->textField($model,'mobile',array('size'=>30,'maxlength'=>30,'class'=>'span12','tabindex'=>'8')); ?>
			<?php echo $form->error($model,'mobile'); ?>
		</div>
	</div>
	<div class="row-fluid">
        <div class="span3">
            <?php echo $form->labelEx($model,'puntoDeVenta'); ?>
            <?php echo $form->dropDownList($model,'puntoDeVenta',CHtml::listData(PuntoDeVenta::model()->activos()->findAll(),'num','title')
                ,array('class'=>'span12')); ?>
        </div>
		<div class="span3">
			<?php echo $form->labelEx($model,'address'); ?>
			<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50,'class'=>'span12','tabindex'=>'9')); ?>
			<?php echo $form->error($model,'address'); ?>
		</div>
		
		<?php 
			if ($model->isNewRecord) {
		?>
			<div class="span3">
				<?php echo $form->labelEx($model,'password_1'); ?>
				<?php echo $form->passwordField($model,'password_1',array('size'=>60,'maxlength'=>250,'class'=>'span12','tabindex'=>'10')); ?>
				<?php echo $form->error($model,'password_1'); ?>
				<?php echo $form->hiddenField($model,'password',array('size'=>60,'maxlength'=>250)); ?>
			</div>
			
			<div class="span3">
				<?php echo $form->labelEx($model,'password_confirm'); ?>
				<?php echo $form->passwordField($model,'password_confirm',array('size'=>60,'maxlength'=>250,'class'=>'span12','tabindex'=>'11')); ?>
				<?php echo $form->error($model,'password_confirm'); ?>
			</div>
	
		<?php 
			}
		?>
	
		<div class="span3 buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
		</div>
	</div>	
</div>	




<?php $this->endWidget(); ?>

</div><!-- form -->