<div id="content" class="row-fluid" style="margin-left: 151px;">
<h2>Cambio de password</h2>
	<div class="span3" style="margin-left: 184px;">
		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'user-form',
			'enableAjaxValidation'=>true,
			'enableClientValidation'=>true,
		)); ?>
			<fieldset>
		  
		  	<?php echo $form->errorSummary($modeluser); ?>
		  
			<div class="row">
				
			</div>
		  
			<div class="row">
				<?php echo $form->labelEx($modeluser,'password_1'); ?>
				<?php echo $form->passwordField($modeluser,'password_1', array('class'=>'campos2', 'style'=>'width: 293px')); ?>
				<?php echo $form->error($modeluser,'password_1'); ?>
			</div>
			
			<div class="row">
				<?php echo $form->labelEx($modeluser,'password_confirm'); ?>
				<?php echo $form->passwordField($modeluser,'password_confirm', array('class'=>'campos2', 'style'=>'width: 293px')); ?>
				<?php echo $form->error($modeluser,'password_confirm'); ?>
			</div>
			.
			<?php $this->widget(
				'bootstrap.widgets.TbButton', 
				array(
					'buttonType'=>'submit', 
					'type'=>'primary', 
					'size'=>'large', 
					'label'=>'Cambiar',
					'htmlOptions' => array(
										//'disabled'=> 'disabled',
										'id'=> 'Register',
										'class'=>'green-button-peque',
										'style'=>'width: 100px; height: 50px; background-color: #555555; margin-left: 88px;background-position: 0px -50px;',
									),
				)); 
			?>			
		</fieldset>
		<?php $this->endWidget(); ?>
		
    </div>
		
		
    	<div class="clear"></div>
  </div>
  <!--/content-->  
</div>