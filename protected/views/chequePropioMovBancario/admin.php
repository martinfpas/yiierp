<?php
/* @var $this ChequePropioMovBancarioController */
/* @var $model ChequePropioMovBancario */

$this->breadcrumbs=array(
	ChequePropioMovBancario::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#cheque-propio-mov-bancario-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?=ChequePropioMovBancario::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-propio-mov-bancario-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'idChequePropio',
		'idMovimiento',
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
