<?php
$this->breadcrumbs=array(
	ChequePropioMovBancario::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.ChequePropioMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=ChequePropioMovBancario::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idChequePropio',
		'idMovimiento',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
