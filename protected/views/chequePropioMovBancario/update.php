<?php
$this->breadcrumbs=array(
	ChequePropioMovBancario::model()->getAttributeLabel('models') => array('admin'),
	ChequePropioMovBancario::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.ChequePropioMovBancario::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.ChequePropioMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=ChequePropioMovBancario::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>