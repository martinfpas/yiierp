<?php
/* @var $this ChequePropioMovBancarioController */
/* @var $data ChequePropioMovBancario */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idChequePropio')); ?>:</b>
	<?php echo CHtml::encode($data->idChequePropio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idMovimiento')); ?>:</b>
	<?php echo CHtml::encode($data->idMovimiento); ?>
	<br />


</div>