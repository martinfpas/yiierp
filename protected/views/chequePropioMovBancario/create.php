<?php
/* @var $this ChequePropioMovBancarioController */
/* @var $model ChequePropioMovBancario */

$this->breadcrumbs=array(
	ChequePropioMovBancario::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.ChequePropioMovBancario::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=ChequePropioMovBancario::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>