
/* @var $this MedioMovimientoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Medio Movimientos',
);

$this->menu=array(
	array('label'=>'Nuevo MedioMovimiento', 'url'=>array('create')),
	array('label'=>'ABM MedioMovimiento', 'url'=>array('admin')),
);
?>

<h1>Medio Movimientos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
