<?php
$this->breadcrumbs=array(
	'Medio Movimientos'=>array('admin'),
	$model->CodMedio=>array('view','id'=>$model->CodMedio),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo MedioMovimiento', 'url'=>array('create')),
	array('label'=>'Ver MedioMovimiento', 'url'=>array('view', 'id'=>$model->CodMedio)),
	array('label'=>'Gestion de MedioMovimiento', 'url'=>array('admin')),
);
?>

	<h1>Modificando MedioMovimiento <?php echo $model->CodMedio; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>