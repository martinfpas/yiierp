<?php
/* @var $this MedioMovimientoController */
/* @var $data MedioMovimiento */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('CodMedio')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->CodMedio),array('view','id'=>$data->CodMedio)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->Descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('Demora')); ?>:</b>
	<?php echo CHtml::encode($data->Demora); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('activo')); ?>:</b>
	<?php echo CHtml::encode($data->activo); ?>
	<br />


</div>