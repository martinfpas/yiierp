d
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoMedioMovimiento" id="masMedioMovimiento" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoMedioMovimiento" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'medio-movimiento-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('MedioMovimiento/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'medio-movimiento-grid',
			'idMas' => 'masMedioMovimiento',
			'idDivNuevo' => 'nuevoMedioMovimiento',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorMedioMovimiento"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okMedioMovimiento">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'CodMedio',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Demora',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php 
				if(!$model->isNewRecord){
					echo $form->labelEx($model,'activo'); 
	echo $form->dropDownList($model,'activo', array('0' => MedioMovimiento::inactivo,'1' => MedioMovimiento::activo),	array( 'class'=>'span3')); 
				}
				?> 		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'medio-movimiento-grid',
'dataProvider'=>$aMedioMovimiento->search(),
'columns'=>array(
		'CodMedio',
		'Descripcion',
		'Demora',

			array(
				'name' => 'activo',
				'value' => 'MedioMovimiento::$aEstado[$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => MedioMovimiento::$aEstado,
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("MedioMovimiento/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("MedioMovimiento/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>