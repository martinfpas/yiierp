<?php
/* @var $this MedioMovimientoController */
/* @var $model MedioMovimiento */

$this->breadcrumbs=array(
	'Medio Movimientos'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM MedioMovimiento', 'url'=>array('admin')),
);
?>

<h1>Nuevo MedioMovimiento</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>