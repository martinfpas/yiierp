<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'medio-movimiento-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorMedioMovimiento"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okMedioMovimiento">Datos Guardados Correctamente !</div>
	
<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

			<div class="span3"><?php echo $form->textFieldRow($model,'CodMedio',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>20)); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'Demora',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php 
				if(!$model->isNewRecord){
				echo $form->labelEx($model,'activo'); 
echo $form->dropDownList($model,'activo', array('0' => MedioMovimiento::inactivo,'1' => MedioMovimiento::activo),	array( 'style'=>'')); 
				}
				?> </div>	
		</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
