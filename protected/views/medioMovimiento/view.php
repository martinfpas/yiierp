<?php
$this->breadcrumbs=array(
	'Medio Movimientos'=>array('admin'),
	$model->CodMedio,
);

$this->menu=array(
	array('label'=>'Nuevo MedioMovimiento', 'url'=>array('create')),
	array('label'=>'Modificar MedioMovimiento', 'url'=>array('update', 'id'=>$model->CodMedio)),
	array('label'=>'Borrar MedioMovimiento', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->CodMedio),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de MedioMovimiento', 'url'=>array('admin')),
);
?>

<h1>Ver MedioMovimiento #<?php echo $model->CodMedio; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'CodMedio',
		'Descripcion',
		'Demora',

			array(
				'name' => 'activo',
				'header' => 'Estado', 
				'value' => MedioMovimiento::$aEstado[$model->activo],
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
