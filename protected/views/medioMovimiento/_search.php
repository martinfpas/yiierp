<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="content-fluid">
    <div class="row-fluid">
        <div class='span3'>
            <?php echo $form->textFieldRow($model,'CodMedio',array('class'=>'span12')); ?>
        </div>
        <div class='span3'>
            <?php echo $form->textFieldRow($model,'Descripcion',array('class'=>'span12','maxlength'=>20)); ?>
        </div>
        <div class='span3'>
            <?php echo $form->labelEx($model,'activo'); ?>
            <?php echo $form->dropDownList($model,'activo', array('0' => MedioMovimiento::inactivo,'1' => MedioMovimiento::activo),	array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ?>
        </div>
        <div class='span3'>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type'=>'primary',
                'label'=>'Buscar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                ),
            )); ?>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
