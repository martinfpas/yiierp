<?php
/* @var $this MedioMovimientoController */
/* @var $model MedioMovimiento */

$this->breadcrumbs=array(
	'Medio Movimientos'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo MedioMovimiento', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#medio-movimiento-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Medio Movimientos</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'medio-movimiento-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'CodMedio',
		'Descripcion',
		'Demora',

			array(
				'name' => 'activo',
				'value' => 'MedioMovimiento::$aEstado[$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => MedioMovimiento::$aEstado,
			),
				/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
