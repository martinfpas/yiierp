<?php
$this->breadcrumbs=array(
	'Entrega Gastos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo EntregaGtos', 'url'=>array('create')),
	array('label'=>'Modificar EntregaGtos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar EntregaGtos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de EntregaGtos', 'url'=>array('admin')),
);
?>

<h1>Ver EntregaGtos #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idEntregaDoc',
		'gasto',
		'montoGasto',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
