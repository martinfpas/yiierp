
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoEntregaGtos" id="masEntregaGtos" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoEntregaGtos" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'entrega-gtos-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('EntregaGtos/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'entrega-gtos-grid',
			'idMas' => 'masEntregaGtos',
			'idDivNuevo' => 'nuevoEntregaGtos',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorEntregaGtos"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okEntregaGtos">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
                <?php echo $form->textFieldRow($model,'idEntregaDoc',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'gasto',array('class'=>'span12','maxlength'=>20)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'montoGasto',array('class'=>'span12')); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'entrega-gtos-grid',
'dataProvider'=>$aEntregaGtos->search(),
'columns'=>array(
		'id',
		'idEntregaDoc',
		'gasto',
		'montoGasto',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("EntregaGtos/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("EntregaGtos/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>