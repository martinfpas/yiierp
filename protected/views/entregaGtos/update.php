<?php
$this->breadcrumbs=array(
	'Entrega Gtoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo EntregaGtos', 'url'=>array('create')),
	array('label'=>'Ver EntregaGtos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de EntregaGtos', 'url'=>array('admin')),
);
?>

	<h1>Modificando EntregaGtos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>