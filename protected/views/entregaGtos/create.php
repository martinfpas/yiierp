<?php
/* @var $this EntregaGtosController */
/* @var $model EntregaGtos */

$this->breadcrumbs=array(
	'Entrega Gtoses'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de EntregaGtos', 'url'=>array('admin')),
);
?>

<h1>Creando EntregaGtos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>