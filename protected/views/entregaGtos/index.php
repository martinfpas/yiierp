
/* @var $this EntregaGtosController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Entrega Gtoses',
);

$this->menu=array(
	array('label'=>'Nuevo EntregaGtos', 'url'=>array('create')),
	array('label'=>'Gestion de EntregaGtos', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>