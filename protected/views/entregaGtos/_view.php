<?php
/* @var $this EntregaGtosController */
/* @var $data EntregaGtos */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('idEntregaDoc')); ?>:</b>
    <?php echo CHtml::encode($data->idEntregaDoc); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasto')); ?>:</b>
	<?php echo CHtml::encode($data->gasto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('montoGasto')); ?>:</b>
	<?php echo CHtml::encode($data->montoGasto); ?>
	<br />


</div>