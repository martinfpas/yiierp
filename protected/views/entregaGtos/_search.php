<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

<?php echo $form->textFieldRow($model,'idEntregaDoc',array('class'=>'span12')); ?>


<?php echo $form->textFieldRow($model,'gasto',array('class'=>'span12','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'montoGasto',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
