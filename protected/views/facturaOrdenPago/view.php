<?php
$this->breadcrumbs=array(
	FacturaOrdenPago::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.FacturaOrdenPago::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.FacturaOrdenPago::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.FacturaOrdenPago::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.FacturaOrdenPago::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=FacturaOrdenPago::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idComprobanteProveedor',
		'idOrdeDePago',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
