<?php
/* @var $this FacturaOrdenPagoController */
/* @var $model FacturaOrdenPago */

$this->breadcrumbs=array(
	FacturaOrdenPago::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.FacturaOrdenPago::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=FacturaOrdenPago::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>