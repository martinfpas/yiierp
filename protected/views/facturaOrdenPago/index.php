
/* @var $this FacturaOrdenPagoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'FacturaOrdenPago::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo FacturaOrdenPago', 'url'=>array('create')),
	array('label'=>'Gestion de FacturaOrdenPago', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>