<?php
/* @var $this FacturaOrdenPagoController */
/* @var $data FacturaOrdenPago */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobanteProveedor')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobanteProveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idOrdeDePago')); ?>:</b>
	<?php echo CHtml::encode($data->idOrdeDePago); ?>
	<br />


</div>