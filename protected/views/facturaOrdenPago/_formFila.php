
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoFacturaOrdenPago" id="masFacturaOrdenPago" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoFacturaOrdenPago" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'factura-orden-pago-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('FacturaOrdenPago/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'factura-orden-pago-grid',
			'idMas' => 'masFacturaOrdenPago',
			'idDivNuevo' => 'nuevoFacturaOrdenPago',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorFacturaOrdenPago"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okFacturaOrdenPago">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idComprobanteProveedor',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idOrdeDePago',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'factura-orden-pago-grid',
'dataProvider'=>$aFacturaOrdenPago->search(),
'columns'=>array(
		'id',
		'idComprobanteProveedor',
		'idOrdeDePago',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("FacturaOrdenPago/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("FacturaOrdenPago/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>