
/* @var $this DepuracionController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Depuracions',
);

$this->menu=array(
	array('label'=>'Nuevo Depuracion', 'url'=>array('create')),
	array('label'=>'ABM Depuracion', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>