<?php
$this->breadcrumbs=array(
	'Depuracions'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Depuracion', 'url'=>array('create')),
	array('label'=>'Modificar Depuracion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Depuracion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Depuracion', 'url'=>array('admin')),
);
?>

<h1>Ver Depuracion #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'fecha',
		'idCarga',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
