<h4 style="font-weight: normal;padding-top: 10px;">Equivalencias COT.</h4>

<?php

$totalKilos = 0;
$aKilosCod = array();
$fKgCarga = 0;
/**
 * @var  $vArtCot VArtCotbyDep
 * @var  $model
 */
foreach ($vArtCot->searchNoGroupWp()->getData() as $index => $model) {
    if($model->CodCot != '000000'){
        if(isset($aKilosCod[$model->CodCot])){
            $aKilosCod[$model->CodCot] += number_format(floatval($model->kilos),2);
        }else{
            $aKilosCod[$model->CodCot] = number_format(floatval($model->kilos),2);
        }
        $totalKilos += number_format(floatval($model->kilos),2);
    }
}

$fKgCarga = $vArtCot->KgCarga;
$aKilosCod['000000'] = number_format(($fKgCarga - $totalKilos),2);
$totalKilos = $fKgCarga;


echo '<div style="padding-left: 20px;width: 100%">';

?>
<div id="codigos-cot-grid" class="grid-view">
    <table class="items table">
        <thead>
        <tr>
            <th id="codigos-cot-grid_c0">Cod. COT</th><th id="codigos-cot-grid_c1">descripcion</th><th id="codigos-cot-grid_c2" style="text-align: right;">Kilos</th></tr>
        </thead>
        <tfoot>
        <tr>
            <td>&nbsp;</td><td style="text-align:right;width:400px;">Total de KILOS COT:</td><td><?=number_format($fKgCarga,2)?></td></tr>
        </tfoot>
        <tbody>
        <?php
        $i = 0;
        foreach ($vArtCot->searchWp()->getData() as $index => $item) {
            echo '<tr class="'.(($i%2)? "odd" : "even").'">
                    <td style="width:90px;">'.$item->CodCot.'</td><td style="width:500px;">'.$item->descripcion.'</td><td style="text-align:right;">'.number_format($aKilosCod[$item->CodCot],2).'</td>
                </tr>';
            $i++;
        }
        ?>

        </tbody>
    </table>
</div>


