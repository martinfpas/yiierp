<style>

    tr.odd td,tr.even td{
        padding-top:5px;
        padding-bottom: 5px;
        padding-left: 3px;
        padding-right: 3px;
        border-bottom: 0.05em dotted black;
        border-right: 0.01em dashed black;

    }
    th
    {
        font-weight:normal;
        border-bottom: 0.05em dotted black;
    }

</style>
<div style="width: 90%; text-align: right">
    <h5 style="font-weight: normal;">Fecha: <?php echo ComponentesComunes::fechaFormateada($model->fecha) ; ?> </h5>
</div>
<h5 style="font-weight: normal;">Numero Depuracion : <?=$model->id; ?></h5>
<div style="width: 100%; text-align: center; margin-bottom: 15px;">
    <u>DEPURACION de Articulos :: Carga Nº<?=$model->idCarga; ?></u>
    <br>
    <br>
    <?php $this->widget('bootstrap.widgets.TbDetailView',array(
        'data'=>$model->oCarga,
        'htmlOptions' => array(
            'style' => 'font-size:larger;margin-left:10px;',
        ),
        'attributes'=>array(
            'observacion',
        ),
    ));
    ?>
</div>
<div style="padding-left: 0px;width: 100% ">

<?php
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-carga-grid',
    'dataProvider'=>$aProds->searchWP(),
    'enableSorting' => false,
    'template' => '{items}',
    'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => 'Codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => 'width:90px;text-align:center;'),
        ),
        array(
            'name' => 'idArtVenta',
            'header' => 'Articulo',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => 'width:400px;'),
            'footer' => 'Total de kilos ',
            'footerHtmlOptions' => array(
                'style' => 'text-align:right;padding-top:50px;padding-left:20px',
                //'colspan' => 2,
            ),
        ),

        array(
            'name' => 'CantXbulto',
            'header' => 'Cant/Bulto',
            'htmlOptions' => array('style' => 'text-align:center;padding-right:5px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:50px;'),

        ),
        array(
            'name' => 'Bultos',
            'header' => 'Bultos',
            'htmlOptions' => array('style' => 'text-align:center'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:80px;'),
            'footer' => number_format($model->getPesoTotal(),2,',','.'),
            'footerHtmlOptions' => array(
                'style' => 'text-align:right;padding-top:50px;',
            ),
        ),


        array(
            'name' => 'envase',
            'htmlOptions' => array('style' => 'text-align:right;border-right:none;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),

        ),

        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */

    ),
));
echo '</div>'

?>
    <div class="container-fluid" style="margin-top: 50px;">
        <div class="row-fluid">
            <div class="span6">
                RECIBI CONFORME EL TOTAL DE LA CARGA<br><br><br><br>
                <br>
                <span style="border-top: 0.05em dotted black;">FIRMA Y ACLARACION DEL EMPLEADO</span>
            </div>
            <div class="span6">
                VERIFICO CARGA DEL CAMION<br><br><br><br>
                <br>
                <span style="border-top: 0.05em dotted black;">FIRMA Y ACLARACION DEL ENCARGADO</span>
            </div>
        </div>
    </div>



