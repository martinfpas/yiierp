<?php
/* @var $this DepuracionController */
/* @var $model Depuracion */

$this->breadcrumbs=array(
	'Depuracions'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Depuracion', 'url'=>array('admin')),
);
?>

<h1>Nueva <?=Depuracion::model()->getAttributeLabel('model')?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>