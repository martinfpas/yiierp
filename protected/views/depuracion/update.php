<?php
$this->breadcrumbs=array(
	'Depuracions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Depuracion', 'url'=>array('create')),
	array('label'=>'Ver Depuracion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de Depuracion', 'url'=>array('admin')),
);
?>

	<h1>Modificando Depuracion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>