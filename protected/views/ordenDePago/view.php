<?php

/**
 * @var $aFactProvOP FacturaOrdenPago
 * @var $model OrdenDePago
 */

$this->breadcrumbs=array(
	OrdenDePago::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.OrdenDePago::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.OrdenDePago::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.OrdenDePago::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.OrdenDePago::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
$cs = Yii::app()->getClientScript();
$cs->registerScript('viewFP',"
    
    $(document).ready(function(){
        getProveedor(".$model->idProveedor.");
    });
    
    function getProveedor(id_Proveedor){
        var datos = 'id='+id_Proveedor;
        $.ajax({
            url:'".$this->createUrl('proveedor/viewFicha')."',
            data: datos,
            type: 'GET',
            success: function (html){
                $('#fichaProveedor').html(html);                
                return false;                                    
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
                $('#idProveedor').html('');
            }                
        });
    }
");

?>

<h3>Ver <?=OrdenDePago::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<div class="row-fluid">
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView',array(
            'data'=>$model,
            'attributes'=>array(
                'id',
                'observacion',
                array(
                    'name' => 'fecha',
                    'value' => ComponentesComunes::fechaFormateada($model->fecha),

                ),
            ),
        )); ?>
        <?php
            if ($model->idPagoProveedor > 0){
                echo CHtml::link('Ver Pago Asociado',Yii::app()->createUrl('PagoProveedor/view',array('id'=>$model->idPagoProveedor)),array('class'=>'btn btn-primary'));
            }else{
                echo CHtml::link('Crear Pago Provedor',Yii::app()->createUrl('OrdenDePago/CrearPago',array('id'=>$model->id)),array('class'=>'btn btn-primary',));
            }

        ?>
    </div>
    <div class="span6" id="fichaProveedor">

    </div>

</div>

<h4>Facturas</h4>

<?php

$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'pago2-fact-prov-grid',
    'dataProvider'=>$aFactProvOP->search(),
    'htmlOptions' => array(
        'style' => 'padding-top:0px;',
    ),
    'columns'=>array(
        'oComprobanteProveedor.Nro_Comprobante',
        array(
            'name' => 'oComprobanteProveedor.TipoComprobante',
            'value' => 'ComprobanteProveedor::$aClase[$data->oComprobanteProveedor->TipoComprobante]',
            'filter' => ComprobanteProveedor::$aClase,
            //'header' => 'Cuenta',
            //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
        array(

            'name' => 'oComprobanteProveedor.id_Proveedor',
            'value' => '$data->oComprobanteProveedor->oProveedor->Title',
            'header' => 'Proveedor',
            /*
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            */
        ),

        array(
            'name' => 'oComprobanteProveedor.TotalNeto',
            'value' => 'number_format($data->oComprobanteProveedor->TotalNeto,2)',
            'header' => 'Importe',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),

        array(
            'name' => 'oComprobanteProveedor.montoPagado',
            'value' => 'number_format($data->oComprobanteProveedor->montoPagado,2)',
            //'header' => 'montoPagado',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),


        array(
            'name' => 'oComprobanteProveedor.FechaFactura',
            'value' => 'ComponentesComunes::fechaFormateada($data->oComprobanteProveedor->FechaFactura)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => ' {delete}',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Desvincular Factura',
                    'url'=>'Yii::app()->controller->createUrl("FacturaOrdenPago/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>