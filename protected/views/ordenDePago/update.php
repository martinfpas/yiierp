<?php
$this->breadcrumbs=array(
	OrdenDePago::model()->getAttributeLabel('models') => array('admin'),
	OrdenDePago::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.OrdenDePago::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.OrdenDePago::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.OrdenDePago::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=OrdenDePago::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>