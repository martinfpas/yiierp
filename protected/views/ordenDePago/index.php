
/* @var $this OrdenDePagoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'OrdenDePago::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo OrdenDePago', 'url'=>array('create')),
	array('label'=>'Gestion de OrdenDePago', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>