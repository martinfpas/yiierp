<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'orden-de-pago-form',
	'enableAjaxValidation'=>false,
));

$cs = Yii::app()->getClientScript();
$cs->registerScript('formOP',"
    
    $(document).ready(function(){
        getGrilla();
    });
    
    $('#OrdenDePago_idProveedor').change(function(){
        getGrilla();
    });
    
    function getGrilla(){
        let id = $('#OrdenDePago_idProveedor').val();
        console.log(id);
        $('.modalLoading').show('slow');
        if(id > 0){
            let datos = 'ComprobanteProveedor[id_Proveedor]='+id+'&ComprobanteProveedor[condImpaga]=1';
            $.ajax({
                url: '".Yii::app()->createUrl('ComprobanteProveedor/adminSelect')."',
                data: datos,
                type: 'GET',
                success:function(html){
                    $('#facturas').html(html);
                    $('.modalLoading').hide('slow');                    
                },
                error:function(x,y,z){
                    //TODO: MANERJAR EL ERROR
                    $('.modalLoading').hide('slow');
                    alert(x);
                },
            });
        }        
    }
",CClientScript::POS_READY);
?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorOrdenDePago"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okOrdenDePago">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
        <div class="span5">
            <?php
                echo $form->textFieldRow($model,'idProveedor',array('class'=>'span12 '));
                $this->widget('ext.select2.ESelect2',array(
                        'selector'=>'#OrdenDePago_idProveedor',
                        'model' => $model,
                        'attribute' => 'idProveedor',
                        'options'=>array(
                            'ajax'=>array(
                                'url'=>Yii::app()->createUrl('proveedor/select2'),
                                'dataType'=>'json',
                                'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                'results'=>'js:function(data,page) { return {results: data}; }',
                            ),
                            'class' => 'span12',
                            'style' => 'margin-left:0px;',
                        ),
                    )
                );
                echo $form->error($model,'idProveedor');
            ?>
        </div>
		<div class="span2">
			<?php
                echo $form->labelEx($model,'fecha',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'OrdenDePago[FechaFactura]', // Cambiar 'NotaPedido por el modelo que corresponda
                    //'model' => $model,
                    //'attribute' => 'FechaFactura',
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 4
                    ),
                ));
                echo $form->error($model,'fecha');
            ?>
		</div>
    </div>
    <div class="row-fluid">
        <div class="span12" id="facturas">

        </div>
    </div>
    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->textAreaRow($model,'observacion',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
        </div>
		<div class="span2">
			<?php echo $form->hiddenField($model,'user_id'); ?>

            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                )
            )); ?>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
