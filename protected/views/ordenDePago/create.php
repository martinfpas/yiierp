<?php
/* @var $this OrdenDePagoController */
/* @var $model OrdenDePago */

$this->breadcrumbs=array(
	OrdenDePago::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.OrdenDePago::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=OrdenDePago::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>