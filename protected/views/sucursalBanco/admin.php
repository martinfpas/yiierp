<?php
/* @var $this SucursalBancoController */
/* @var $model SucursalBanco */

$this->breadcrumbs=array(
	'Sucursal Bancos'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva SucursalBanco', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#sucursal-banco-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Sucursal Bancos</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'sucursal-banco-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id',
		'nombre',
        array(
            'name' => 'id_banco',
			'value' => '$data->oBanco->nombre',
			'htmlOptions' => array('style' => ''),
		    'filter' =>  CHtml::listData(Banco::model()->findAll(),'id','nombre'),

		),
		'localidad',
            array(
                'name' => 'provincia',
                'value' => '$data->oProvincia->nombre',
                'htmlOptions' => array('style' => ''),
                'filter' =>  CHtml::listData(Provincia::model()->findAll(),'id_provincia','nombre'),

            ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		    'filter' =>  CHtml::listData(Vehiculo::model()->findAll(),'id','IdNombre'),

		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
