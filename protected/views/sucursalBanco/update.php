<?php
$this->breadcrumbs=array(
	'Sucursal Bancos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nueva SucursalBanco', 'url'=>array('create')),
	array('label'=>'Ver SucursalBanco', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de SucursalBanco', 'url'=>array('admin')),
);
?>

	<h1>Modificando SucursalBanco <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>