<?php
$this->breadcrumbs=array(
	'Sucursal Bancos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nueva SucursalBanco', 'url'=>array('create')),
	array('label'=>'Modificar SucursalBanco', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar SucursalBanco', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de SucursalBanco', 'url'=>array('admin')),
);
?>

<h1>Ver SucursalBanco #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nombre',
        array(
            'name' => 'id_banco',
            'value' => $model->oBanco->nombre,
        ),

    'localidad',
        array(
            'name' => 'provincia',
            'value' => $model->oProvincia->nombre,
        ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
