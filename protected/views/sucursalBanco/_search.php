<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>60)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id_banco',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'localidad',array('class'=>'span12','maxlength'=>60)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'provincia',array('class'=>'span12','maxlength'=>60)); ?>
    </div>
    <div class="span4">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>
</div>

<?php $this->endWidget(); ?>
