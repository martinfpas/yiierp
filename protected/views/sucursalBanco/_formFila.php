
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoSucursalBanco" id="masSucursalBanco" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoSucursalBanco" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'sucursal-banco-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('SucursalBanco/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'sucursal-banco-grid',
			'idMas' => 'masSucursalBanco',
			'idDivNuevo' => 'nuevoSucursalBanco',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorSucursalBanco"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okSucursalBanco">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
			<div class="span3">
                <?php
                echo $form->labelEx($model,'id_banco');
                echo $form->dropDownList($model,'id_banco',CHtml::listData(Banco::model()->findAll(),'id','nombre'),array());
                ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'localidad',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
			<div class="span3">
			<<?php
                echo $form->labelEx($model,'provincia');
                echo $form->dropDownList($model,'provincia',CHtml::listData(Provincia::model()->findAll(),'id','nombre'),array());
                ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'sucursal-banco-grid',
'dataProvider'=>$aSucursalBanco->search(),
'columns'=>array(
		'id',
		'nombre',
    array(
        'name' => 'id_banco',
        'value' => $model->oBanco->nombre,
    ),
		'localidad',
    array(
        'name' => 'provincia',
        'value' => $model->oProvincia->nombre,
    ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("SucursalBanco/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("SucursalBanco/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>