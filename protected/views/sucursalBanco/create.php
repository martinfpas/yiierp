<?php
/* @var $this SucursalBancoController */
/* @var $model SucursalBanco */

$this->breadcrumbs=array(
	'Sucursal Bancos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de SucursalBanco', 'url'=>array('admin')),
);
?>

<h1>Creando SucursalBanco</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>