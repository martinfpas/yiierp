
/* @var $this SucursalBancoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Sucursal Bancos',
);

$this->menu=array(
	array('label'=>'Nueva SucursalBanco', 'url'=>array('create')),
	array('label'=>'Gestion de SucursalBanco', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>