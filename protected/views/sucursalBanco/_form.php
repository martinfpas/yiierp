<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'sucursal-banco-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorSucursalBanco"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okSucursalBanco">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'id_banco');
            echo $form->dropDownList($model,'id_banco',CHtml::listData(Banco::model()->findAll(),'id','nombre'),array());
            ?>

        </div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'localidad',array('class'=>'span12','maxlength'=>60)); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'provincia');
            echo $form->dropDownList($model,'provincia',CHtml::listData(Provincia::model()->findAll(),'id','nombre'),array());
            ?>

        </div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
