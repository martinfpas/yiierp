<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
	<?php echo $content; ?>
</div><!-- content -->
<div class="modalLoading" ><!-- Place at bottom of page --></div>
<?php $this->endContent(); ?>