<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<!-- Fav icon -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.ico"></link>

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/postBootstrap.css" />
</head>

<body style="/*background:grey;*/">

<div class="container-fluid" id="page" style="max-width: 1350px;margin: auto;">

	<div id="header" class="row-fluid">
		<div id="logo" style="font-size: small;float:left;"  class="span6"><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"> - v 1.37.25 </div>
        <div style="font-size: small;float:left"  class="span5">Fecha Servidor: <?=date('d-m-Y')?></div>
    </div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Inicio', 'url'=>array('/site/index')),
				array(
						'label'=>'Ventas',
						'url'=>'#',

                        'submenuOptions' => array('class' => 'dropdown-menu'),
						'itemOptions' => array('class' => 'dropdown'),
						'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                        'visible'=>Yii::app()->user->checkAccess("mod_ventas"),
                        'items'=>array(
                            array(
                                'label'=>'Pedidos', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("mod_nota_pedido"),
                                'submenuOptions' => array('style' => 'min-width: 170px;list-style: circle;'),
                                'items'=>array(
                                    array('label'=>'Notas de Pedido sin Carga', 'url'=>array('/notaPedido/admin'),'visible'=>Yii::app()->user->checkAccess("mod_nota_pedido")),
                                    array('label'=>'Nueva Nota de Pedido', 'url'=>array('/notaPedido/create'),'visible'=>Yii::app()->user->checkAccess("mod_nota_pedido")),
                                    array('label'=>'Notas de Pedido Historicas', 'url'=>array('/notaPedido/adminHistorico'),'visible'=>Yii::app()->user->checkAccess("mod_nota_pedido")),
                                ),
                            ),
                            array(
                                'label'=>'Clientes', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("administrador"),
                                'submenuOptions' => array('style' => 'max-width: 140px;list-style: circle;'),
                                'items'=>array(
                                    array('label'=>'ABM Clientes', 'url'=>array('/cliente/admin'),'visible'=>Yii::app()->user->checkAccess("administrador")),
                                    array('label'=>'Pagos Clientes', 'url'=>array('/pagoCliente/admin'),'visible'=>Yii::app()->user->checkAccess("administrador")),
                                ),
                            ),
                            array(
                                'label'=>'Comprobantes', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("mod_ventas"),
                                'submenuOptions' => array('style' => 'min-width: 170px;list-style: circle;','class' => 'ul_mod_comprobantes'),
                                'items'=>array(
                                    array('label'=>'Facturas', 'url'=>array('/Factura/admin'),'visible'=>Yii::app()->user->checkAccess("mod_factura"),array('style' => 'display: block;')),
                                    array('label'=>NotaCredito::model()->getAttributeLabel('models'), 'url'=>array('/NotaCredito/admin'),'visible'=>Yii::app()->user->checkAccess("mod_nota_credito")),
                                    array('label'=>NotaDebito::model()->getAttributeLabel('models'), 'url'=>array('/notaDebito/admin'),'visible'=>Yii::app()->user->checkAccess("mod_nota_debito")),
                                    array('label'=>NotaEntrega::model()->getAttributeLabel('models'), 'url'=>array('/notaEntrega/admin'),'visible'=>Yii::app()->user->checkAccess("mod_notaEntrega")),
                                ),
                            ),
                        ),
				),
				array(
						'label'=>'Compras',
						'url'=>'#',
                        'visible'=>Yii::app()->user->checkAccess("mod_compras"),

						 'submenuOptions' => array('class' => 'dropdown-menu','id' => ''),
                            'itemOptions' => array('class' => 'dropdown'),
                            'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                            'items'=>array(
                                    array('label'=>'ABM Proveedores', 'url'=>array('/proveedor/admin'),'visible'=>Yii::app()->user->checkAccess("mod_proveedores")),
                                    array('label'=>'ABM Comprobantes de Compra', 'url'=>array('/ComprobanteProveedor/admin'),'visible'=>Yii::app()->user->checkAccess("mod_proveedores")),
                                    array('label'=>'ABM Remitos', 'url'=>array('/RemitoProveedor/admin'),'visible'=>Yii::app()->user->checkAccess("mod_proveedores")),
                                    array('label'=>'ABM Pago de Facturas', 'url'=>array('/PagoProveedor/admin'),'visible'=>Yii::app()->user->checkAccess("mod_proveedores")),
                                    array('label'=>'ABM Ordenes de Pago', 'url'=>array('/OrdenDePago/admin'),'visible'=>Yii::app()->user->checkAccess("mod_proveedores")),

                            ),

				),
				//array('label'=>'Empleados', 'url'=>array('/empleado/admin')),
				//array('label'=>'Obras', 'url'=>array('/obra/admin')),
				array(
						'label'=>'Listados',
						'url'=>'#',
                        'submenuOptions' => array('class' => 'dropdown-menu','style' => 'min-width: 240px;list-style: circle;'),
                        'itemOptions' => array('class' => 'dropdown-submenu'),
                        'linkOptions' => array('class' => 'dropdown-submenu', 'data-toggle' => 'dropdown'),
                        'visible'=>!Yii::app()->user->isGuest,
                        'items'=>array(
                            array(
                                'label'=>'Camiones', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("mod_carga_camiones"),
                                'submenuOptions' => array('style' => 'max-width: 170px;list-style: circle;'),
                                'items'=>array(
                                    array('label'=>'Carga de camiones', 'url'=>array('/carga/AdminHistoricas'),'visible'=>Yii::app()->user->checkAccess("mod_carga_camiones")),
                                    array('label'=>'Cargas No Despachadas', 'url'=>array('/carga/admin'),'visible'=>Yii::app()->user->checkAccess("mod_carga_camiones")),
                                    array('label'=>'Camiones', 'url'=>array('/vehiculo/admin'),'visible'=>Yii::app()->user->checkAccess("mod_vehiculo")),
                                ),
                            ),
                            array(
                                'label'=>'Documentos', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("administrador"),
                                'submenuOptions' => array('style' => 'max-width: 210px;list-style: circle;'),
                                'items'=>array(
                                    array('label'=>'Hojas De Ruta', 'url'=>array('/entregaDoc/HojasDeRuta'),'visible'=>Yii::app()->user->checkAccess("mod_carga_camiones")),
                                    array('label'=>'Planillas De Rendicion', 'url'=>array('/entregaDoc/PlanillasRendicion'),'visible'=>Yii::app()->user->checkAccess("mod_carga_camiones")),
                                    array('label'=>'Gestion Documentos a Entregar', 'url'=>array('/entregaDoc/admin'),'visible'=>Yii::app()->user->checkAccess("mod_carga_camiones")),
                                ),
                            ),

                        ),
				),
				array(
						'label'=>'Datos Basicos',
						'url'=>'#',
                        'visible'=>!Yii::app()->user->isGuest,
				),
                array(
                    'label'=>'Bancos',
                    'url'=>'#',
                    'submenuOptions' => array('class' => 'dropdown-menu','style' => 'min-width: 165px;list-style: circle;'),
                    'itemOptions' => array('class' => 'dropdown-submenu'),
                    'linkOptions' => array('class' => 'dropdown-submenu', 'data-toggle' => 'dropdown'),
                    'visible'=>Yii::app()->user->checkAccess("mod_banco"),
                    'items'=>array(
                        array(
                            'label'=>'Bancos', 'url'=>'#','visible'=>Yii::app()->user->checkAccess("administrador"),
                            'items'=>array(
                                array('label'=>'Banco           ', 'url'=>array('/banco/admin'),'visible'=>Yii::app()->user->checkAccess("mod_banco")),
                                array('label'=>'Sucursales       ', 'url'=>array('/sucursalBanco/admin'),'visible'=>Yii::app()->user->checkAccess("mod_banco")),
                                array('label'=>'Cuentas Bancarias', 'url'=>array('/CuentasBancarias/admin'),'visible'=>Yii::app()->user->checkAccess("mod_banco")),
                            ),
                        ),
                        array(
                            'label'=>'Movimientos Bancarios', 'url'=>array('/movimientoCuentasBanc/admin'),'visible'=>Yii::app()->user->checkAccess("administrador"),
                            'items'=>array(
                                array('label'=>'Cheques Emitidos', 'url'=>array('/chequePropio/admin'),'visible'=>Yii::app()->user->checkAccess("mod_banco")),
                                array('label'=>'Nuevo Movimiento', 'url'=>array('/movimientoCuentasBanc/create'),'visible'=>Yii::app()->user->checkAccess("mod_banco")),
                                array('label'=>'Actualizar Cuentas Bancarias', 'url'=>array('/CuentasBancarias/admin'),'visible'=>Yii::app()->user->checkAccess("administrador")),
                                array('label'=>'Anular/Modificar Movimiento', 'url'=>array('/movimientoCuentasBanc/admin'),'visible'=>false),
                            ),
                            'visible' => Yii::app()->user->checkAccess("mod_banco"),
                        ),



                    ),

                ),
//array('label'=>'Bancos', 'url'=>array('/banco/admin'),'visible'=>Yii::app()->user->checkAccess("admin")),

				array(
			    	'label'=>'Stock',
			    	'url'=>'#',
					'submenuOptions' => array('class' => 'dropdown-menu','style' => 'min-width: 210px;list-style: circle;'),
			      	'itemOptions' => array('class' => 'dropdown-submenu'),
            		'linkOptions' => array('class' => 'dropdown-submenu', 'data-toggle' => 'dropdown'),
                    'visible'=>!Yii::app()->user->isGuest,
			      	'items'=>array(
                        array('label'=>'Gestionar Rubros', 'url'=>array('/rubro/admin'),'visible'=>Yii::app()->user->checkAccess("mod_rubros")),
		      			array('label'=>'Gestionar Articulos', 'url'=>array('/articulo/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos")),
		      			array('label'=>'Gestionar Articulos Venta', 'url'=>array('/ArticuloVenta/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos_vta")),
                        array('label'=>'Nuevo Articulos Venta', 'url'=>array('/ArticuloVenta/create'),'visible'=>Yii::app()->user->checkAccess("mod_articulos_vta")),


				    ),
			    ),
				array(
						'label'=>'Contabilidad',
						'url'=>'#',
                        'submenuOptions' => array('class' => 'dropdown-menu','style' => 'min-width: 210px;list-style: circle;'),
                        'itemOptions' => array('class' => 'dropdown-submenu'),
                        'linkOptions' => array('class' => 'dropdown-submenu', 'data-toggle' => 'dropdown'),
                        'visible'=>!Yii::app()->user->isGuest,
                        'items'=>array(
                            array('label'=>'Carga de cuentas de gastos', 'url'=>array('/CuentaGastos/admin'),'visible'=>Yii::app()->user->checkAccess("administrador")),
                            array('label'=>'Iva Ventas', 'url'=>array('comprobante/IvaVentas'),'visible'=>Yii::app()->user->checkAccess("administrador"),'linkOptions'=>array('target' => '_blank')),
                            array('label' => 'Citi Compras y Ventas' ,'url'=> array('Contabilidad/citi'),'visible'=>Yii::app()->user->checkAccess("administrador")),
                            array('label'=>'Iva Compras', 'url'=>array('comprobanteProveedor/IvaCompras'),'visible'=>Yii::app()->user->checkAccess("administrador"),'linkOptions'=>array('target' => '_blank')),
                            array('label'=>'Compras Por Cuentas', 'url'=>array('comprobanteProveedor/ComprasPorCuentas'),'visible'=>Yii::app()->user->checkAccess("administrador"),'linkOptions'=>array('target' => '_blank')),
                            
                            array('label'=>'Meses De Carga', 'url'=>array('mesesCargaCompra/admin'),'visible'=>Yii::app()->user->checkAccess("administrador"),'linkOptions'=>array('target' => '_blank')),

                        ),

				),
				array(
						'label'=>'Estadisticas',
						'url'=>'#',
                        'visible'=>!Yii::app()->user->isGuest,
						/*
						 'submenuOptions' => array('class' => 'dropdown-menu','id' => ''),
                        'itemOptions' => array('class' => 'dropdown'),
                        'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                        'items'=>array(
                            array('label'=>'Gestionar Articulos', 'url'=>array('/articulo/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos")),
                            array('label'=>'Gestionar Articulos Venta', 'url'=>array('/ArticuloVenta/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos_vta")),


				        ),
				        */
				),
				array(
						'label'=>'Busquedas',
						'url'=>'#',
                        'visible'=>!Yii::app()->user->isGuest,
						/*
						 'submenuOptions' => array('class' => 'dropdown-menu','id' => ''),
				'itemOptions' => array('class' => 'dropdown'),
				'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
				'items'=>array(
						array('label'=>'Gestionar Articulos', 'url'=>array('/articulo/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos")),
						array('label'=>'Gestionar Articulos Venta', 'url'=>array('/ArticuloVenta/admin'),'visible'=>Yii::app()->user->checkAccess("mod_articulos_vta")),


				),
				*/
				),
				array(
			    	'label'=>'Configuracion',
			    	'url'=>'#',
					'submenuOptions' => array('class' => 'dropdown-menu','id' => 'configuracion'),
			      	'itemOptions' => array('class' => 'dropdown'),
            		'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                    'visible'=>Yii::app()->user->checkAccess("mod_configuraciones"),
			      	'items'=>array(
			      		array('label'=>'Gestionar Categoria Iva', 'url'=>array('categoriaIva/admin'),'visible'=>Yii::app()->user->checkAccess("mod_cat_iva")),
			      		array('label'=>'Gestionar Codigos Postales', 'url'=>array('codigoPostal/admin'),'visible'=>Yii::app()->user->checkAccess("mod_codigo_postal")),
			      		array('label'=>'Gestionar Envases', 'url'=>array('envase/admin'),'visible'=>Yii::app()->user->checkAccess("mod_envases")),
			      		array('label'=>'Gestionar Provincias', 'url'=>array('Provincia/admin'),'visible'=>Yii::app()->user->checkAccess("mod_provincias")),
			      		array('label'=>'Gestionar Rubros Clientes', 'url'=>array('/rubroClientes/admin'),'visible'=>Yii::app()->user->checkAccess("mod_rubro_cli")),
                        array('label'=>'Gestionar Equivalencias Cot', 'url'=>array('equivalenciaCot/admin'),'visible'=>Yii::app()->user->checkAccess("mod_rubro_cli")),
                        array('label'=>'Gestionar Medios Movimientos Banc', 'url' => array('medioMovimiento/admin'),'visible' => Yii::app()->user->checkAccess('mod_configuraciones') ),
                        array('label'=>'Gestionar Clasif Movimientos Banc', 'url' => array('clasifMovimientoCuentas/admin'),'visible' => Yii::app()->user->checkAccess('mod_configuraciones') ),
                        array('label'=>'Camioneros', 'url' => array('camionero/admin'),'visible' => Yii::app()->user->checkAccess('mod_configuraciones') ),
                        array('label'=>'Vehiculos', 'url' => array('vehiculo/admin'),'visible' => Yii::app()->user->checkAccess('mod_configuraciones') ),
				    ),
			    ),

				array(
						'label'=>'Seguridad',
						'url'=>'#',
						'submenuOptions' => array('class' => 'dropdown-menu','id' => ''),
						'itemOptions' => array('class' => 'dropdown'),
						'linkOptions' => array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'),
                        'visible'=>Yii::app()->user->checkAccess("mod_seguridad"),

						'items'=>array(
								array('label'=>'Gestionar Usuarios', 'url'=>array('user/admin'),'visible'=>Yii::app()->user->checkAccess("mod_usuarios")),
                                array('label'=>'Gestionar Autorizaciones ', 'url'=>array('AuthItem/admin'),'visible'=>Yii::app()->user->checkAccess("mod_auth_items")),
                                array('label'=>'Actualizar Fecha del sistema', 'url'=>array('AuthItem/ActualizaFecha'),'visible'=>Yii::app()->user->checkAccess("mod_usuarios")),

						),
				),
				array(
				    'label'=>'Backup',
                    'url'=>array('/user/backup'),
                    'visible'=>!Yii::app()->user->isGuest,
				),
				array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Cerrar sesion ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">

		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
<?php
    if (!(Yii::app()->user->isGuest) && (Yii::app()->components['user']->loginRequiredAjaxResponse)){

        Yii::app()->clientScript->registerScript('ajaxLoginRequired', '
            jQuery("body").ajaxComplete(
                function(event, request, options) {
                    if (request.responseText == "'.Yii::app()->components['user']->loginRequiredAjaxResponse.'") {
                        window.location.reload(true);
                        //location.reload();
                    }
                }
            );
        ');
    }
?>
</html>
