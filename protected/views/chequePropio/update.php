<?php
$this->breadcrumbs=array(
	ChequePropio::model()->getAttributeLabel('models') => array('admin'),
	ChequePropio::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM '.ChequePropio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=ChequePropio::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>