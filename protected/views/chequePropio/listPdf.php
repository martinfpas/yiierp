<style>
    tr.odd td,tr.even td{
        padding-top:10px;
        border-bottom: 0.05em dotted black;
        border-right: 0.01em dashed black;
    }
    th
    {
        font-weight:normal;
        border-bottom: 0.05em dotted black;
    }
    .grid-view tr td {
        /* border-left: 0.05em dotted black; */
        padding-left: 25px;
        padding-right: 5px;
    }

</style>
<?php
/* @var $this ChequePropioController */
/* @var $model ChequePropio */

?>

<h3><?=ChequePropio::model()->getAttributeLabel('models') ?></h3>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-propio-grid',
'dataProvider'=>$model->search(false),
'enableSorting' => false,
'htmlOptions' => array(
    'style' => 'padding-top:0px',
),
'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:60px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:60px;text-align:center;'),
        ),
        array(
            'name' => 'nroCheque',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:80px;'),
            'headerHtmlOptions' => array('style' => 'width:80px;'),
        ),
        array(
            'name' => 'id_cuenta',
            'value' => '$data->oCuenta->numero',
            'htmlOptions' => array('style' => 'width:100px;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),

        ),
        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2)',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
        ),
        array(
            'name' => 'fechaDebito',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaDebito)',
            'htmlOptions' => array('style' => 'width:100px;border-right:none;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),
        ),
	),
)); ?>
