<?php
/* @var $model ChequePropio*/
$this->breadcrumbs=array(
	ChequePropio::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM '.ChequePropio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=ChequePropio::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>
<div class="row">
    <div class="span6">
        <?php $this->widget('bootstrap.widgets.TbDetailView',array(
        'data'=>$model,
        'attributes'=>array(
                'id',
                'nroCheque',
                array(
                    'name' => 'fecha',
                    'value' => ComponentesComunes::fechaFormateada($model->fecha),
                    'htmlOptions' => array('style' => ''),
                    'headerHtmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'id_cuenta',
                    'value' =>  CHtml::link($model->oCuenta->title,Yii::app()->createUrl('cuentasBancarias/view',array('id' => $model->oCuenta->id))),
                    'header' => '',
                    'type'=>'raw',
                    'htmlOptions' => array('style' => ''),
                    'headerHtmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'importe',
                    'value' => '$'.number_format($model->importe,2),
                    'htmlOptions' => array('style' => ''),
                    'headerHtmlOptions' => array('style' => ''),
                ),
                array(
                    'name' => 'fechaDebito',
                    'value' => ComponentesComunes::fechaFormateada($model->fechaDebito),
                    'htmlOptions' => array('style' => ''),
                    'headerHtmlOptions' => array('style' => ''),
                ),
                'destinatario',
                /*
                    array(
                        'name' => '',
                        'value' => '',
                        'header' => '',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                */
        ),
        )); ?>
    </div>
    <div class="span6">
        <?php
            if(sizeof($model->aChequePropioMovBancario) > 0){
                $aAttributes = array();
                foreach($model->aChequePropioMovBancario as $key => $oChequePropioMovBancario){                    
                    array_push($aAttributes, array(
                        'name' => 'Movimiento',
                        'type'=>'raw',
                        'value' => ($oChequePropioMovBancario->oMovimiento != null)? CHtml::link('('.$oChequePropioMovBancario->oMovimiento->id.') - '.$oChequePropioMovBancario->oMovimiento->Observacion,Yii::app()->createUrl('MovimientoCuentasBanc/view',array('id' => $oChequePropioMovBancario->oMovimiento->id))) : 'No hay Movimientos asociados',
                        'htmlOptions' => array('style' => ''),
                    ));
                }
                $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model->aChequePropioMovBancario,
                    'attributes' => $aAttributes
                )
                );
            }
        ?>
    </div>
    
</div>