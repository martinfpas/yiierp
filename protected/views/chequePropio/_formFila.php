
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoChequePropio" id="masChequePropio" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoChequePropio" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'cheque-propio-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ChequePropio/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'cheque-propio-grid',
			'idMas' => 'masChequePropio',
			'idDivNuevo' => 'nuevoChequePropio',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorChequePropio"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okChequePropio">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'nroCheque',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id_cuenta',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'importe',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'fechaDebito',array('class'=>'span12')); ?>
		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-propio-grid',
'dataProvider'=>$aChequePropio->search(),
'columns'=>array(
		'id',
		'nroCheque',
		'fecha',
		'id_cuenta',
		'importe',
		'fechaDebito',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ChequePropio/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ChequePropio/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>