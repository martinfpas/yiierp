
/* @var $this ChequePropioController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"'ChequePropio::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo ChequePropio', 'url'=>array('create')),
	array('label'=>'ABM ChequePropio', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>