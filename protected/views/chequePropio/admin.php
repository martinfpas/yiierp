<?php
/* @var $this ChequePropioController */
/* @var $model ChequePropio */

$this->breadcrumbs=array(
	ChequePropio::model()->getAttributeLabel('models')=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo '.ChequePropio::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
    $('.search-form form').submit(function(){
        $('#cheque-propio-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
        return false;
    });
");
?>

<h3>ABM <?=ChequePropio::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-propio-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'htmlOptions' => array(
    'style' => 'padding-top:0px',
),
'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:60px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:60px;text-align:center;'),
        ),
        array(
            'name' => 'nroCheque',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:80px;'),
            'headerHtmlOptions' => array('style' => 'width:80px;'),
        ),
        array(
            'name' => 'id_cuenta',
            'value' => '$data->oCuenta->numero',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(CuentasBancarias::model()->findAll(),'id','numero'),
        ),
        array(
            'name' => 'destinatario',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2)',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
        ),
        array(
            'name' => 'fechaDebito',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaDebito)',
            'htmlOptions' => array('style' => 'width:100px;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
<?php $this->widget('PrintWidget') ?>