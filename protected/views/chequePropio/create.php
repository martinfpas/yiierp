<?php
/* @var $this ChequePropioController */
/* @var $model ChequePropio */

$this->breadcrumbs=array(
	ChequePropio::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.ChequePropio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=ChequePropio::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>