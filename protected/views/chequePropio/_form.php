<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cheque-propio-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorChequePropio"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okChequePropio">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

        <div class="span2" style="width: 8.5%;">
			<?php echo $form->textFieldRow($model,'nroCheque',array('class'=>'span12','tabindex' => 1)); ?>
		</div>
		<div class="span2" style="width: 9.5%;">
			<?php
                echo $form->labelEx($model,'fecha',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'ChequePropio[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;margin-top: 5px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 2
                    ),
                ));
                echo $form->error($model,'fecha');
            ?>
		</div>
		<div class="span3" style="width: 34%;">
			<?php
                echo $form->labelEx($model,'id_cuenta');
                echo $form->dropDownList($model,'id_cuenta',CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),array('class'=>'span12','empty'=>'Seleccionar' ,'tabindex' => 3));
            ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'importe',array('class'=>'span12','tabindex' => 4)); ?>
		</div>
        <div class="span2">
			<?php
                echo $form->labelEx($model,'fechaDebito',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fechaDebito),
                    'name'=>'ChequePropio[fechaDebito]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;margin-top: 5px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 5
                    ),
                ));
                echo $form->error($model,'fechaDebito');
            ?>
		</div>

    </div>
    <div class="row-fluid">

        <div class="span7" style="">
            <?php echo $form->textFieldRow($model,'destinatario',array('class'=>'span12', 'maxlength'=>'70','tabindex' => 6)); ?>
        </div>
        <div class="span1">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
                'htmlOptions' => array(
                    'style' => 'margin-top:25px;',
                )
            )); ?>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
