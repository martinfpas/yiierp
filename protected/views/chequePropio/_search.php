<?php
    Yii::app()->clientScript->registerScript('print-button', "
        $('.print-button').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            let serial = $('#chequePropioSearch-form').serialize();
            serial = serial.replace('r=chequePropio%2Fadmin&',''); 
            let href = $(this).attr('href')+'&'+serial;
            console.log(href);
            let print = $(this).attr('print')+'&'+serial;
            $('#download-form').attr('action',href);
            $('#print-form').attr('action',print);        
            $('#modalPrint').modal();
        });
    ");
    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'id'=>'chequePropioSearch-form',
    )); ?>
    <div class="content-fluid">
        <div class="row-fluid">
            <div class='span1'><?php echo $form->textFieldRow($model, 'id', array('class' => 'span12')); ?>
            </div>
            <div class='span2'><?php echo $form->textFieldRow($model, 'nroCheque', array('class' => 'span12')); ?>
            </div>
            <div class='span2'><?php echo $form->textFieldRow($model, 'fecha', array('class' => 'span12')); ?>
            </div>
            <div class='span2'>
                <?php
                    echo $form->labelEx($model, 'id_cuenta');
                    echo $form->dropDownList($model, 'id_cuenta', CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),array('class' => 'span12','empty' => ''));
                ?>
            </div>
            <div class='span2'><?php echo $form->textFieldRow($model, 'importe', array('class' => 'span12')); ?>
            </div>
            <div class='span2'><?php echo $form->textFieldRow($model, 'fechaDebito', array('class' => 'span12')); ?>
            </div>
            <div class='span1'>

                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => Yii::t('application', 'Buscar'),
                    'htmlOptions' => array(
                        'style' => 'margin-top:25px',
                    ),
                )); ?>

            </div>
        </div>
    </div>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class='span2'>
            <?php
            echo CHtml::label('Fecha Desde','fechaDesde',array('style'=>'display:inline;'));
            $this->widget('CMaskedTextField', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fechaDesde),
                'name'=>'ChequePropio[fechaDesde]', // Cambiar 'NotaPedido por el modelo que corresponda
                'mask' => '99-99-9999',
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Ingresar los numeros sin los guiones  ',
                ),
            ));
            ?>

        </div>
        <div class='span2'>
            <?php
            echo CHtml::label('Fecha Hasta', 'fechaHasta',array('style'=>'display:inline;'));
            $this->widget('CMaskedTextField', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fechaHasta),
                'name'=>'ChequePropio[fechaHasta]', // Cambiar 'NotaPedido por el modelo que corresponda
                'mask' => '99-99-9999',
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Ingresar los numeros sin los guiones  ',
                ),
            ));
            ?>

        </div>
        <div class='span1'>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType' => 'submit',
                'type' => 'primary',
                'label' => Yii::t('application', 'Buscar'),
                'htmlOptions' => array(
                    'style' => 'margin-top:20px',
                ),
            )); ?>
        </div>
        <div class="span1">
            <?php
            //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
                echo CHtml::link('Exportar',Yii::app()->createUrl('ChequePropio/listaPdf'),
                array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;margin-top:20px;', 'print' => Yii::app()->createUrl('ChequePropio/listaPdf', array('print'=>true)) ));
            ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
