<?php
/* @var $this ChequePropioController */
/* @var $model ChequePropio */

?>

<h3><?=ChequePropio::model()->getAttributeLabel('models') ?></h3>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-propio-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'htmlOptions' => array(
    'style' => 'padding-top:0px',
),
'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:60px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:60px;text-align:center;'),
        ),
        array(
            'name' => 'nroCheque',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:80px;'),
            'headerHtmlOptions' => array('style' => 'width:80px;'),
        ),
        array(
            'name' => 'id_cuenta',
            'value' => '$data->oCuenta->title',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),
        ),
        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2)',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
        ),
        array(
            'name' => 'fechaDebito',
            'value' => 'ComponentesComunes::fechaFormateada($data->fechaDebito)',
            'htmlOptions' => array('style' => 'width:100px;'),
            'headerHtmlOptions' => array('style' => 'width:100px;'),
        ),
	),
)); ?>
