<?php
$this->breadcrumbs=array(
	RemitoProveedor::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.RemitoProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.RemitoProveedor::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.RemitoProveedor::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM '.RemitoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=RemitoProveedor::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
        array(
            'name' => 'idProveedor',
            'value' => $model->oProveedor->title,
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'monto',
            'value' => '$'.number_format($model->monto,2),
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'fecha',
            'value' => ComponentesComunes::fechaFormateada($model->fecha),

            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),

		'detalle',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
