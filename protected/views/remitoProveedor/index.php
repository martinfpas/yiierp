
/* @var $this RemitoProveedorController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"RemitoProveedor::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo RemitoProveedor', 'url'=>array('create')),
	array('label'=>'ABM RemitoProveedor', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>