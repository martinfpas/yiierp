<?php
/* @var $this RemitoProveedorController */
/* @var $model RemitoProveedor */

$this->breadcrumbs=array(
	RemitoProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.RemitoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=RemitoProveedor::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>