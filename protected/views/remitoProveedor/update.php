<?php
$this->breadcrumbs=array(
	RemitoProveedor::model()->getAttributeLabel('models') => array('admin'),
	RemitoProveedor::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.RemitoProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.RemitoProveedor::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM '.RemitoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=RemitoProveedor::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>