<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'remito-proveedor-form',
	'enableAjaxValidation'=>false,
));

$cs = Yii::app()->getClientScript();
$baseUrl = Yii::app()->baseUrl;
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScript('x','
$(document).ready(function () {
    $("#RemitoProveedor_idProveedor").on("select2:select", function (e) {
        // Do something
        console.log("#RemitoProveedor_monto");
        $("#RemitoProveedor_monto").focus();
    });
});
', CClientScript::POS_READY);

?>

	<p class="note"> Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorRemitoProveedor"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okRemitoProveedor">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span8">
			<?php
                echo $form->textFieldRow($model,'idProveedor',array('class'=>'span12 '));
                $this->widget('ext.select2.ESelect2',array(
                        'selector'=>'#RemitoProveedor_idProveedor',
                        'model' => $model,
                        'attribute' => 'idProveedor',
                        'options'=>array(
                            'ajax'=>array(
                                'url'=>Yii::app()->createUrl('proveedor/select2'),
                                'dataType'=>'json',
                                'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                'results'=>'js:function(data,page) { return {results: data}; }',
                            ),
                            'class' => 'span12',
                            'style' => 'margin-left:0px;',
                            'tabindex' => 1
                        ),
                    )
                );
                echo $form->error($model,'idProveedor');
            ?>
		</div>
		<div class="span2">
			<?php echo $form->textFieldRow($model,'monto',array('class'=>'span12','tabindex' => 2)); ?>
		</div>
		<div class="span2">
			<?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'RemitoProveedor[fecha]',
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    'tabindex' => 3
                ),
            ));
            ?>
		</div>
    </div>
    <div class="row-fluid">
		<div class="span8">
			<?php echo $form->textAreaRow($model,'detalle',array('rows'=>6, 'cols'=>50, 'class'=>'span12')); ?>
		</div>
        <div class="span4">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
