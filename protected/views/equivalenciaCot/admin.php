<?php
/* @var $this EquivalenciaCotController */
/* @var $model EquivalenciaCot */

$this->breadcrumbs=array(
	'Equivalencia Cots'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo EquivalenciaCot', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#equivalencia-cot-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Equivalencia Cots</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'equivalencia-cot-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'id',
			'htmlOptions' => array('style' => 'width:50px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
		),

        array(
            'name' => 'idArticuloVenta',
			'header' => 'Articulo',
			'value' => '$data->oArticuloVenta->descripcion',
			'htmlOptions' => array('style' => ''),
		),

        array(
            'name' => 'idArticuloVenta',
            'header' => 'Codigo Art',
            'value' => '$data->oArticuloVenta->codigo',
            'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),

        array(
            'name' => 'codCot',
            'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
