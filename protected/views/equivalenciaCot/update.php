<?php
$this->breadcrumbs=array(
	'Equivalencia Cots'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo EquivalenciaCot', 'url'=>array('create')),
	array('label'=>'Ver EquivalenciaCot', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de EquivalenciaCot', 'url'=>array('admin')),
);
?>

	<h1>Modificando EquivalenciaCot <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>