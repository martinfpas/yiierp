
/* @var $this EquivalenciaCotController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Equivalencia Cots',
);

$this->menu=array(
	array('label'=>'Nuevo EquivalenciaCot', 'url'=>array('create')),
	array('label'=>'Gestion de EquivalenciaCot', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>