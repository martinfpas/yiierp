<?php
/* @var $this EquivalenciaCotController */
/* @var $model EquivalenciaCot */

$this->breadcrumbs=array(
	'Equivalencia Cots'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de EquivalenciaCot', 'url'=>array('admin')),
);
?>

<h1>Creando EquivalenciaCot</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>