<?php
/* @var $this EquivalenciaCotController */
/* @var $data EquivalenciaCot */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArticuloVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArticuloVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codCot')); ?>:</b>
	<?php echo CHtml::encode($data->codCot); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />


</div>