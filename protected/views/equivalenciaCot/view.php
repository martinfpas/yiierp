<?php
$this->breadcrumbs=array(
	'Equivalencia Cots'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo EquivalenciaCot', 'url'=>array('create')),
	array('label'=>'Modificar EquivalenciaCot', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar EquivalenciaCot', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de EquivalenciaCot', 'url'=>array('admin')),
);
?>

<h1>Ver EquivalenciaCot #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idArticuloVenta',
		'codCot',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
