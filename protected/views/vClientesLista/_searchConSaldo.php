

    <?php

    Yii::app()->clientScript->registerScript('print-button', "
        $('.print-button').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            let serial = $('#vClientesListaSearch-form').serialize();
            //LO SIGUIENTE DEPENDE DEL ACTION DESDE QUE SE LO EJECUTA.
            serial = serial.replace('r=vClientesLista%2FadminConSaldo&',''); 
            let href = $(this).attr('href')+'&'+serial;
            console.log(href);
            let print = $(this).attr('print')+'&'+serial;
            $('#download-form').attr('action',href);
            $('#print-form').attr('action',print);        
            $('#modalPrint').modal();
        });
    ");


    $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'id'=>'vClientesListaSearch-form',
    )); ?>
    <div class="content-fluid">
        <div class="row-fluid">

            <div class='span4'>
                <?php
                    echo $form->labelEx($model,'id_rubro');
                    echo $form->dropDownList($model,'id_rubro',CHtml::listData(RubroClientes::model()->todos()->findAll(),'id_rubro','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'12'));
                ?>
            </div>

            <div class='span4'>
                <?php
                    echo $form->labelEx($model,'codViajante');
                    echo $form->dropDownList($model,'codViajante',CHtml::listData(Viajante::model()->todos()->findAll(),'numero','IdTitulo'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'9'));
                ?>
            </div>


            <div class='span4'><?php echo $form->textFieldRow($model, 'nombrelocalidad', array('class' => 'span12', 'maxlength' => 65)); ?>
            </div>
        </div>
        <div class="row-fluid">

            <div class='span4'>
                <?php echo $form->labelEx($model, 'id_provincia'); ?>
                <?php echo $form->dropDownList($model, 'id_provincia',CHtml::listData(Provincia::model()->todas()->findAll(),'id_provincia','IdNombre'),array('empty'=>'--- Seleccionar ---','class' => 'span12', 'maxlength' => 2)); ?>
            </div>
            <div class='span4'>
                <?php
                    echo $form->labelEx($model,'categoriaIva');
                    echo $form->dropDownList($model,'categoriaIva',CHtml::listData(CategoriaIva::model()->todas()->findAll(),'id_categoria','nombre'),array('class'=>'span12','empty'=>'--Elegir--','tabindex'=>'10'));
                ?>
            </div>

            <div class='span2'>
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => Yii::t('application', 'Buscar'),
                    'htmlOptions' => array(
                        'style' => 'float:left;margin-top:20px;'
                    )
                )); ?>
            </div>
            <div class="span2">
                <?php
                //echo CHtml::link('Impresión Definitiva',Yii::app()->createUrl('carga/pdf', array('id'=>$model->id)),array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;', 'target' => '_blank;' ));
                echo CHtml::link('Exportar',Yii::app()->createUrl('VClientesLista/viewPdfConSaldo'),
                    array('class'=>'bulk-button btn print-button','id' => 'imprimir','style' => 'float:left;margin-top:20px;', 'print' => Yii::app()->createUrl('VClientesLista/viewPdfConSaldo', array('print'=>true)) ));
                ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

