<?php
/* @var $this VClientesListaController */
/* @var $model VClientesLista */

$this->breadcrumbs=array(
	VClientesLista::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo '.VClientesLista::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#vclientes-lista-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


?>

<h3>Gestion de <?=VClientesLista::model()->getAttributeLabel('models') ?></h3>

<div class="search-form" style="">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'vclientes-lista-grid',
'dataProvider'=>$model->search(),
'columns'=>array(
		'id',
		'razonSocial',
		'direccion',
        'telefonos',
        'nombrelocalidad',
        array(
            'name' => 'id_provincia',
            'value' => '($data->oProvincia != null)? $data->oProvincia->nombre :""',
            //'filter' => CHtml::listData(Provincia::model()->todas()->findAll(),'id_provincia','IdNombre'),
        ),

		/*
		'cp',
		'zp',
		'codViajante',
		'saldoActual',
		'rubro',
		'idIva',
		'categoriaIva',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

	),
)); ?>
<?php $this->widget('PrintWidget') ?>