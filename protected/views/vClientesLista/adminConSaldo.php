<?php
/* @var $this VClientesListaController */
/* @var $model VClientesLista */

$this->breadcrumbs=array(
	VClientesLista::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo '.VClientesLista::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#vclientes-lista-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");


?>

<h3>Resumen de Clientes Con saldo</h3>

<div class="search-form" style="">
<?php $this->renderPartial('_searchConSaldo',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'vclientes-lista-grid',
'dataProvider'=>$model->searchConSaldo(),
'columns'=>array(
		'id',
		'razonSocial',
		'direccion',
        'telefonos',
        'nombrelocalidad',
        array(
            'name' => 'id_provincia',
            'value' => '($data->oProvincia != null)? $data->oProvincia->nombre :""',
            //'filter' => CHtml::listData(Provincia::model()->todas()->findAll(),'id_provincia','IdNombre'),
        ),
        'saldoActual',
		/*
		'cp',
		'zp',
		'codViajante',
		
		'rubro',
		'idIva',
		'categoriaIva',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

	),
)); ?>
<?php $this->widget('PrintWidget') ?>