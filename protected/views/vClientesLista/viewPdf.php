<?php
/* @var $this VClientesListaController */
/* @var $model VClientesLista */
?>
<style>
    tr.odd td,tr.even td{
        /*border-bottom: 1px solid black;*/
        font-size: 8;
    }
    tr th{
        border-bottom: 0.03em solid black;
        font-size: 8;
    }
</style>

<h3>Listado Parcial de Clientes <?=($model->oProvincia != null)? $model->oProvincia->nombre : ''?></h3>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'vclientes-lista-grid',
'dataProvider'=>$model->search(),
'enableSorting' => false,
'template' => '{items}',
'columns'=>array(
        array(
            'name' => 'id',
            'header' => 'Nº CLI.',
            'htmlOptions' => array('style' => 'width:40px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'razonSocial',
            'header' => 'NOMBRE',
            'value' => '($data->razonSocial != null)? $data->razonSocial :$data->nombreFantasia',
            'htmlOptions' => array('style' => '/*width:45px;*/'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'direccion',
            'header' => 'DIRECCION',
            'htmlOptions' => array('style' => '/*width:45px;*/'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'telefonos',
            'header' => 'TELEFONOS',
            'htmlOptions' => array('style' => 'max-width:90px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'nombrelocalidad',
            'header' => 'LOCALIDAD',
            'htmlOptions' => array('style' => '/*width:45px;*/'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'idIva',
            'header' => 'IVA',
            'htmlOptions' => array('style' => 'width:35px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'rubro',
            'header' => 'RUBRO',
            'value' => 'substr($data->rubro,0,5)',
            'htmlOptions' => array('style' => 'width:30px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),


		/*
		'cp',
		'zp',
		'codViajante',
		'saldoActual',
		'rubro',


		'idIva',
		'categoriaIva',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

	),
)); ?>
