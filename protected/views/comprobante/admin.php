<?php
/* @var $this ComprobanteController */
/* @var $model Comprobante */

?>

<h3>ABM <?=Comprobante::model()->getAttributeLabel('models') ?></h3>



<?php

$dataProvider=new CArrayDataProvider($aModels,array(
        'pagination'=> false,
    )
);

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'comprobante-grid',
'dataProvider'=>$dataProvider,
    'template' => '{items}',
    'columns'=>array(
		'fecha',
		'numero',
		'doc',

        array(
            'name' => 'cliente',
            'header' => 'CLIENTE',
            'footer' => '***TOTALES.....',
            'footerHtmlOptions' => array('style' => 'margin-top:15px;'),

        ),
		'cuit',

        array(
            'name' => 'netoGrav',
            'header' => 'NETO GRAV.',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['netoGrav'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;margin-top:15px;'),

        ),
        array(
            'name' => 'iva',
            'header' => 'IVA',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['iva'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;margin-top:15px;'),
        ),
        array(
            'name' => 'percRet',
            'header' => 'PREC./RET.',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['percRet'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;margin-top:15px;'),
        ),
        array(
            'name' => 'noGrav',
            'header' => 'NO GRAV.',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['noGrav'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;margin-top:15px;'),
        ),

        array(
            'name' => 'total',
            'header' => 'TOTAL',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['total'],2),
            'footerHtmlOptions' => array('style' => 'text-align:right;margin-top:15px;'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

	),
));
?>


<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <h4>Resumen de IVA Debito Fiscal</h4>

            <?php
            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'iva-grid',
                'dataProvider'=>new CArrayDataProvider($aIvaDebito),
                'template' => '{items}',
                'columns'=>array(
                    array(
                        'name' => 'nombre',
                        'header' => '',
                        //'value' => '$data->nombre',
                    ),
                    array(
                        'name' => 'monto',
                        'header' => '',
                        'value' => 'number_format($data["monto"],2)',
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                        'htmlOptions' => array('style' => 'text-align:right;'),
                    ),

                ),
            ));
            ?>
        </div>
        <div class="span6">
            <h4>Base imponible discriminada por provincia</h4>

            <?php

            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'base-grid',
                'dataProvider'=>new CArrayDataProvider($aBasePcias),
                'template' => '{items}',
                'columns'=>array(
                    array(
                        'name' => 'nombre',
                        'header' => '',
                        //'value' => '$data->nombre',
                    ),
                    array(
                        'name' => 'base',
                        'header' => '',
                        'value' => 'number_format($data["base"],2)',
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                        'htmlOptions' => array('style' => 'text-align:right;'),
                    ),

                ),
            ));
            ?>
        </div>
    </div>
</div>


