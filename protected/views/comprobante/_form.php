<?php

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");

$cs->registerScript('iva',"
    $(document).ready(function(){
        $('#Comprobante_Nro_Puesto_Venta').focus();
    });

");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'comprobante-form',
	'enableAjaxValidation'=>false,
    'action' => Yii::app()->createUrl('comprobante/ListadoIva'),
));

?>

<h3>Listado de IVA ventas</h3>
<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
        <div class="span6">
            <?php
                echo $form->labelEx($model,'Nro_Puesto_Venta');
                echo $form->dropDownList($model,'Nro_Puesto_Venta',CHtml::listData(PuntoDeVenta::model()->findAll(),'num','Title'),array('empty' => 'Seleccionar un puesto','tabindex' => 1,'class' => 'span12'));
            ?>
        </div>
		<div class="span1">
			<?php

                echo $form->labelEx($model,'fecha');

                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'Comprobante[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'model' => $model,
                    'attribute' => 'fecha',
                    'mask' => '99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;width:75px;',
                        'class'=>'span12',
                        'title'=>'Hacer click en el campo y seleccionar la fecha',
                        'tabindex' => 2
                    ),
                ));

            ?>
		</div>
        <div class="span3">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=> 'Ver',
                'htmlOptions' => array(
                    'style'=> 'margin-top:25px;'
                ),
            ));
            ?>
            </div>
    </div>
</div>


<?php $this->endWidget(); ?>
