<?php
/* @var $this ComprobanteController */
/* @var $model Comprobante */

?>

<h3>Listado de IVA VENTAS del mes <?=$mes?><?=$puesto?> </h3>

<?php

$dataProvider=new CArrayDataProvider($aModels,array(
        'pagination'=> false,
    )
);

$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'comprobante-grid',
'dataProvider'=>$dataProvider,
    'template' => '{items}',
    'columns'=>array(
        array(
            'name' => 'fecha',
            'header' => 'FECHA',
            'htmlOptions' => array('style' => 'text-align:center;width:80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:80px;'),
        ),
        array(
            'name' => 'numero',
            'header' => 'NUMERO',
            //'value' => '"*".$data[\'numero\']."*"',
            'htmlOptions' => array('style' => 'text-align:center;width:50px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:50px;'),
        ),
        array(
            'name' => 'doc',
            'header' => 'DOC',
            'htmlOptions' => array('style' => 'text-align:center;width:30px;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;width:30px;'),
        ),

        array(
            'name' => 'cliente',
            'header' => 'CLIENTE',
            'footer' => '***TOTALES.....',
            'footerHtmlOptions' => array('style' => 'margin-top:15px;padding-top:20px;'),

        ),
        array(
            'name' => 'cuit',
            'header' => 'CUIT',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),



        array(
            'name' => 'netoGrav',
            'header' => 'NETO GRAV.',
            //'value' => 'number_format($data["netoGrav"],2,",",".")',
            'value' => 'number_format($data["netoGrav"],2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['netoGrav'],2,',','.').' | ',
            //'footer' => '{colsum2} | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),

        ),
        array(
            'name' => 'iva',
            'header' => 'IVA',
            //'value' => 'number_format($data["iva"],2,",",".")',
            'value' => 'number_format($data["iva"],2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;width:'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['iva'],2,',','.').' | ',
            //'footer' => '{colsum2}',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),
        array(
            'name' => 'percRet',
            'header' => 'PREC/RET',
            'value' => 'number_format($data["percRet"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['percRet'],2,',','.').' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),
        array(
            'name' => 'noGrav',
            'header' => 'NO GRAV.',
            'value' => 'number_format($data["noGrav"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => '$'.number_format($aTotales['noGrav'],2,',','.').' | ',
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),

        array(
            'name' => 'total',
            'header' => 'TOTAL',
            'value' => 'number_format($data["total"],2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:80px;'),
            'footer' => '$'.number_format($aTotales['total'],2,',','.'),
            'footerHtmlOptions' => array('style' => 'text-align:right;padding-top:20px;'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */

	),
));
?>


<div class="container-fluid" style="display: block;">
    <div class="row-fluid">
        <div class="span4">
            <h4>Resumen de IVA Debito Fiscal</h4>

            <?php
            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'iva-grid',
                'dataProvider'=>new CArrayDataProvider($aIvaDebito),
                'template' => '{items}',
                'columns'=>array(
                    array(
                        'name' => 'nombre',
                        'header' => 'Tipos:',
                        'headerHtmlOptions' => array('style' => 'text-align:left;'),
                    ),
                    array(
                        'name' => 'monto',
                        'header' => '',
                        'value' => 'number_format($data["monto"],2,",",".")',
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                        'htmlOptions' => array('style' => 'text-align:right;'),
                    ),

                ),
            ));
            ?>
        </div>
        <div class="span8">

            <h4>Base imponible discriminada por provincia</h4>
            <div class="span6">
                <?php

                $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'base-grid',
                    'dataProvider'=>new CArrayDataProvider($aBasePcias),
                    'template' => '{items}',
                    'columns'=>array(
                        array(
                            'name' => 'nombre',
                            'header' => '',
                            //'value' => '$data->nombre',
                        ),
                        array(
                            'name' => 'base',
                            'header' => 'Exentos',
                            'value' => 'number_format($data["base"],2,",",".")',
                            'headerHtmlOptions' => array('style' => 'text-align:right;'),
                            'htmlOptions' => array('style' => 'text-align:right;'),
                        ),

                    ),
                ));
                ?>
            </div>
            <div class="span6">
                <?php

                $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'base-grid',
                    'dataProvider'=>new CArrayDataProvider($aBasePcias),
                    'template' => '{items}',
                    'columns'=>array(
                        array(
                            'name' => 'nombre',
                            'header' => '',
                            //'value' => '$data->nombre',
                        ),
                        array(
                            'name' => 'baseOtros',
                            'header' => 'Otros',
                            'value' => 'number_format($data["baseOtros"],2,",",".")',
                            'headerHtmlOptions' => array('style' => 'text-align:right;'),
                            'htmlOptions' => array('style' => 'text-align:right;'),
                        ),

                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>


