<?php
/* @var $this ProdNotaDebitoController */
/* @var $model ProdNotaDebito */

$this->breadcrumbs=array(
	'Prod Nota Debitos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ProdNotaDebito', 'url'=>array('admin')),
);
?>

<h1>Creando ProdNotaDebito</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>