<?php
$this->breadcrumbs=array(
	'Prod Nota Debitos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaDebito', 'url'=>array('create')),
	array('label'=>'Modificar ProdNotaDebito', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdNotaDebito', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdNotaDebito', 'url'=>array('admin')),
);
?>

<h1>Ver ProdNotaDebito #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idNotaDebito',
		'nroItem',
		'idArticuloVta',
		'descripcion',
		'cantidad',
		'precio',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
