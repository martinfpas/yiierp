
/* @var $this ProdNotaDebitoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Nota Debitos',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaDebito', 'url'=>array('create')),
	array('label'=>'Gestion de ProdNotaDebito', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>