<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idNotaDebito',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nroItem',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idArticuloVta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>30)); ?>

		<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'precio',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
