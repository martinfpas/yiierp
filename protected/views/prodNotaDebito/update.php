<?php
$this->breadcrumbs=array(
	'Prod Nota Debitos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaDebito', 'url'=>array('create')),
	array('label'=>'Ver ProdNotaDebito', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdNotaDebito', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdNotaDebito <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>