<?php
/* @var $this ProdNotaDebitoController */
/* @var $data ProdNotaDebito */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaDebito')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaDebito); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroItem')); ?>:</b>
	<?php echo CHtml::encode($data->nroItem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArticuloVta')); ?>:</b>
	<?php echo CHtml::encode($data->idArticuloVta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio')); ?>:</b>
	<?php echo CHtml::encode($data->precio); ?>
	<br />


</div>