<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>


		<?php echo $form->textFieldRow($model,'id_presentacion',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'id_articulo',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'id_rubro',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>35)); ?>

		<?php echo $form->textFieldRow($model,'contenido',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'precio',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Costo_base',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'Unidad_Medida',array('class'=>'span12','maxlength'=>5)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
