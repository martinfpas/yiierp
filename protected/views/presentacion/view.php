<?php
$this->breadcrumbs=array(
	'Presentacions'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo Presentacion', 'url'=>array('create')),
	array('label'=>'Modificar Presentacion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar Presentacion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Presentacion', 'url'=>array('admin')),
);
?>

<h1>Ver Presentacion #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'id_presentacion',
		'id_articulo',
		'id_rubro',
		'descripcion',
		'contenido',
		'precio',
		'Costo_base',
		'Unidad_Medida',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
