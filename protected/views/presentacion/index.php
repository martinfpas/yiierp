
/* @var $this PresentacionController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Presentacions',
);

$this->menu=array(
	array('label'=>'Nuevo Presentacion', 'url'=>array('create')),
	array('label'=>'Gestion de Presentacion', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>