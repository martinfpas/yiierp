<?php
/* @var $this PresentacionController */
/* @var $model Presentacion */

$this->breadcrumbs=array(
	'Presentacions'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM Presentacion', 'url'=>array('admin')),
);
?>

<h1>Creando Presentacion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>