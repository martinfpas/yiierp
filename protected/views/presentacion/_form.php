<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'presentacion-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorPresentacion"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okPresentacion">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_rubro');
				echo $form->dropDownList(
						$model,'id_rubro',CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),
						array(
								'tabindex'=>'1','empty' => '--Seleccionar--',
								'ajax' => array(
										'type'=>'POST', //request type
										'url'=>CController::createUrl('ListaArticulos'), //url to call.
										'update'=>'#ArticuloVenta_id_articulo', //selector to update
								)
						)
				);
				
			?>
		</div>		
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_articulo');
				echo $form->dropDownList($model,'id_articulo',CHtml::listData(Articulo::model()->todos()->findAll(),'id_articulo','IdTitulo'),array('tabindex'=>'2','empty' => '--Seleccionar--'));
			?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'id_presentacion',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>35)); ?>
		</div>
	</div>
	<div class="row-fluid">		
		<div class="span3">
			<?php echo $form->textFieldRow($model,'contenido',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Unidad_Medida',array('class'=>'span12','maxlength'=>5)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'precio',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Costo_base',array('class'=>'span12')); ?>
		</div>
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
