<?php
/* @var $this PresentacionController */
/* @var $model Presentacion */

$this->breadcrumbs=array(
	'Presentacions'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Presentacion', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#presentacion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Presentacions</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'presentacion-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'name' => 'id_rubro',
			//'value' => '$data->oRubro->descripcion',
			'value' => '$data->id_rubro',
			'htmlOptions' => array('style' => ''),
			'filter' => CHtml::listData(Rubro::model()->todos()->findAll(),'id_rubro','IdTitulo'),	
		),
		array(
			'name' => 'id_articulo',
			//'value' => '$data->oArticulo->descripcion',
			'value' => '$data->id_articulo',
			'htmlOptions' => array('style' => ''),
		),
		'id_rubro',
		'descripcion',
		'contenido',
		/*
		'precio',
		'Costo_base',
		'Unidad_Medida',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
