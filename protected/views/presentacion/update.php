<?php
$this->breadcrumbs=array(
	'Presentacions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Presentacion', 'url'=>array('create')),
	array('label'=>'Ver Presentacion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM Presentacion', 'url'=>array('admin')),
);
?>

	<h1>Modificando Presentacion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>