<?php
/* @var $this MonedaController */
/* @var $data Moneda */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_moneda')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigo_moneda),array('view','id'=>$data->codigo_moneda)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre')); ?>:</b>
	<?php echo CHtml::encode($data->nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sigla')); ?>:</b>
	<?php echo CHtml::encode($data->sigla); ?>
	<br />


</div>