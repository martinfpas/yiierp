<?php
/* @var $this MonedaController */
/* @var $model Moneda */

$this->breadcrumbs=array(
	'Monedas'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Moneda', 'url'=>array('admin')),
);
?>

<h1>Creando Moneda</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>