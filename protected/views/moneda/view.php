<?php
$this->breadcrumbs=array(
	'Monedas'=>array('admin'),
	$model->codigo_moneda,
);

$this->menu=array(
	array('label'=>'Nuevo Moneda', 'url'=>array('create')),
	array('label'=>'Modificar Moneda', 'url'=>array('update', 'id'=>$model->codigo_moneda)),
	array('label'=>'Borrar Moneda', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->codigo_moneda),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Moneda', 'url'=>array('admin')),
);
?>

<h1>Ver Moneda #<?php echo $model->codigo_moneda; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'codigo_moneda',
		'nombre',
		'sigla',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
