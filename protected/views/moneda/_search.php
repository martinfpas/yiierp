<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'codigo_moneda',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nombre',array('class'=>'span12','maxlength'=>20)); ?>

		<?php echo $form->textFieldRow($model,'sigla',array('class'=>'span12','maxlength'=>4)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
