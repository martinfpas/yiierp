
/* @var $this MonedaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Monedas',
);

$this->menu=array(
	array('label'=>'Nuevo Moneda', 'url'=>array('create')),
	array('label'=>'Gestion de Moneda', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>