<?php
$this->breadcrumbs=array(
	'Monedas'=>array('index'),
	$model->codigo_moneda=>array('view','id'=>$model->codigo_moneda),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo Moneda', 'url'=>array('create')),
	array('label'=>'Ver Moneda', 'url'=>array('view', 'id'=>$model->codigo_moneda)),
	array('label'=>'Gestion de Moneda', 'url'=>array('admin')),
);
?>

	<h1>Modificando Moneda <?php echo $model->codigo_moneda; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>