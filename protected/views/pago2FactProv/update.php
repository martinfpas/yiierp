<?php
$this->breadcrumbs=array(
	Pago2FactProv::model()->getAttributeLabel('models') => array('admin'),
	Pago2FactProv::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.Pago2FactProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.Pago2FactProv::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM '.Pago2FactProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=Pago2FactProv::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>