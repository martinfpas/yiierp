
/* @var $this Pago2FactProvController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	"Pago2FactProv::model()->getAttributeLabel('models')",
);

$this->menu=array(
	array('label'=>'Nuevo Pago2FactProv', 'url'=>array('create')),
	array('label'=>'ABM Pago2FactProv', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>