<?php
/* @var $this Pago2FactProvController */
/* @var $data Pago2FactProv */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobanteProveedor')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobanteProveedor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPagoProveedor')); ?>:</b>
	<?php echo CHtml::encode($data->idPagoProveedor); ?>
	<br />


</div>