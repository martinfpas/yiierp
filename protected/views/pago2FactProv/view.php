<?php
$this->breadcrumbs=array(
	Pago2FactProv::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.Pago2FactProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.Pago2FactProv::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.Pago2FactProv::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.Pago2FactProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=Pago2FactProv::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idComprobanteProveedor',
		'idPagoProveedor',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
