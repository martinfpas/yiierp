<?php
/* @var $this Pago2FactProvController */
/* @var $model Pago2FactProv */

$this->breadcrumbs=array(
	Pago2FactProv::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.Pago2FactProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=Pago2FactProv::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>