<?php
/* @var $this ChequeDeTercerosController */
/* @var $model ChequeDeTerceros */

$this->breadcrumbs=array(
	'Cheques De Terceros'=>array('admin'),
	'Gestion',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#cheque-de-terceros-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

var ChequeDeTerceros_id_banco = '';

$('#ChequeDeTerceros_id_banco').change(function(e) { 
   ChequeDeTerceros_id_banco = $('#ChequeDeTerceros_id_banco').select2('data').text;
   
});

$('#s2id_ChequeDeTerceros_id_banco').change(function(e) { 
   ChequeDeTerceros_id_banco = $('#ChequeDeTerceros_id_banco').select2('data').text;
   
});

function afterAjaxUpdate(){
    let x = $('#ChequeDeTerceros_id_banco').select2('data').text;
    console.log(x);
    
    let id_banco = $('#ChequeDeTerceros_id_banco').val();
    $('#ChequeDeTerceros_id_banco').select2({
        ajax :{
            url: '".Yii::app()->createUrl('banco/select2')."',
            dataType :'json',
            data :function(term,page) { return {q: term, page_limit: 10, page: page}; },
            results :function(data,page) { return {results: data}; },        
        },        
         
         initSelection : function (element, callback) {            
            var data = {id: element.val(), text: ChequeDeTerceros_id_banco};
            console.log(data);
            callback(data);
         }    
    });
        
    
    if(id_banco > 0){
        console.log('id_banco:::::::'+id_banco);
        $('#ChequeDeTerceros_id_banco').select2('val', $('#ChequeDeTerceros_id_banco').val());    
    }    
    
    
    return true;
}



",CClientScript::POS_READY);
?>
<style>
    #s2id_ChequeDeTerceros_id_banco{
        width: 100%;
    }
</style>


<h1>Gestion de Cheque De Terceros</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<?php echo CHtml::link('Nuevo Cheque',Yii::app()->createUrl('chequeDeTerceros/create'),array('class'=>'btn btn-primary','style'=>'margin-left:5px')); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-de-terceros-grid',
'dataProvider'=>$model->search(),
'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");afterAjaxUpdate();}',
'filter'=>$model,

'columns'=>array(
        array(
            'name' => 'nroCheque',
            'htmlOptions' => array('style' => 'width:85px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:85px;text-align:right;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'), // ESTILO VALORES
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),  // ESTILO CABECERA
        ),

        array(
            'name' => 'id_banco',
            'value' =>'($data->oBanco)? $data->oBanco->nombre :""',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => 'width:180px;'),
            //'filter' => CHtml::listData(Banco::model()->findAll(),'id','nombre'),
        ),

        array(
            'name' => 'id_sucursal',
            'value' =>'($data->oSucursal)? $data->oSucursal->nombre :""',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(SucursalBanco::model()->findAll(),'id','nombre'),

        ),

        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2)',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),

        array(
            'name' => 'id_cliente',
            'value' => '($data->oCliente!= null)? $data->oCliente->title : $data->id_cliente',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(Cliente::model()->todos()->findAll(),'id','title'),

        ),
        array(
            'name' => 'estado',
			'value' => 'ChequeDeTerceros::$aEstado[$data->estado]' ,
			'filter' => ChequeDeTerceros::$aEstado,
			'htmlOptions' => array('style' => 'width:80px'),
		),
        array(
            'name' => 'propio',
            'value' => '($data->propio)? "Si" : "No"',
            'filter' => array('1'=>'Si','0'=>'No'),
        ),

		/*
		'id',
		*/
		/*
		array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
));


$this->widget('ext.select2.ESelect2',array(
    'selector'=>'#ChequeDeTerceros_id_banco',
    //'name'=>"ChequeDeTerceros[id_banco]",
    'options'=>array(
        'ajax'=>array(
            'url'=>Yii::app()->createUrl('banco/select2'),
            'dataType'=>'json',
            'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
            'results'=>'js:function(data,page) { return {results: data}; }',
        ),
        'class' => 'span12',
        //'tabindex' => 5,
    ),
));

?>
