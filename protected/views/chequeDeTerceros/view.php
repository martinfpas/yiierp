<?php
/* @var $model ChequeDeTerceros*/

$this->breadcrumbs=array(
	'Cheque De Terceros'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ChequeDeTerceros', 'url'=>array('create')),
	array('label'=>'Modificar ChequeDeTerceros', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ChequeDeTerceros', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ChequeDeTerceros', 'url'=>array('admin')),
);
?>

<h1>Ver ChequeDeTerceros #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nroCheque',
        array(
            'name' => 'fecha',
            'value' => ComponentesComunes::fechaFormateada($model->fecha),
        ),
        array(
            'name' => 'Banco',
            'value' => ($model->oBanco != null)? $model->oBanco->nombre : '',
        ),
        array(
            'name' => 'Sucursal',
            'value' => ($model->oSucursal != null)? $model->oSucursal->nombre : '',
        ),
		'importe',
        array(
            'name' => 'Id_Cliente',
            'value' => $model->oCliente->title,
            'htmlOptions' => array('style' => ''),

        ),
		'propio',
		'id',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
