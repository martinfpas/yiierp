
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoChequeDeTerceros" id="masChequeDeTerceros" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoChequeDeTerceros" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'cheque-de-terceros-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ChequeDeTerceros/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'cheque-de-terceros-grid',
			'idMas' => 'masChequeDeTerceros',
			'idDivNuevo' => 'nuevoChequeDeTerceros',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorChequeDeTerceros"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okChequeDeTerceros">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
    <div class="row-fluid">

        <div class="span3">
            <?php echo $form->textFieldRow($model,'nroCheque',array('class'=>'span12')); ?>
        </div>
        <div class="span2">
            <?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'NotaCredito[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));

            ?>
        </div>
        <div class="span3">
            <?php
            echo $form->labelEx($model,'id_banco');
            echo $form->dropDownList($model,'id_banco',CHtml::listData(Banco::model()->findAll(),'id','nombre'),array(
                'empty' => 'Seleccionar Banco',
                'ajax' => array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('SucursalBanco/ListarSucursales'), //url to call.
                    'update'=>'#ChequeDeTerceros_id_sucursal', //selector to update
                ),

            ));
            ?>

        </div>
        <div class="span3">
            <?php
            echo $form->labelEx($model,'id_sucursal');
            echo $form->dropDownList($model,'id_sucursal',CHtml::listData(array(),'',''),array('empty' => 'Seleccionar Banco'));
            ?>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3">
            <?php echo $form->textFieldRow($model,'importe',array('class'=>'span12')); ?>
        </div>

        <div class="span3">
            <?php
            echo $form->labelEx($model,'propio');
            echo $form->dropDownList($model,'propio',ChequeDeTerceros::$aSiNo,array('class'=>'span12')); ?>
        </div>

        <?php
        if ($model->id_pagoCliente != null){
            echo $form->hiddenField($model,'id_cliente');
            echo $form->hiddenField($model,'id_pagoCliente');
        }else{
            echo '<div class="span3">';
            echo $form->labelEx($model,'id_cliente');
            echo $form->dropDownList($model,'id_cliente',CHtml::listData(Cliente::model()->findAll(),'id','Title'),array());
            echo '</div>';
        }

        ?>
    </div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cheque-de-terceros-grid',
'dataProvider'=>$aChequeDeTerceros->search(),
'columns'=>array(
		'nroCheque',
		'fecha',
		'id_sucursal',
		'importe',
		'id_cliente',
		'propio',
		/*
		'id',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ChequeDeTerceros/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ChequeDeTerceros/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>