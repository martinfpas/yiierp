<?php
/* @var $model ChequeDeTerceros */

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'cheque-de-terceros-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorChequeDeTerceros"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okChequeDeTerceros">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroCheque',array('class'=>'span12')); ?>
		</div>
		<div class="span2">
            <?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'ChequeDeTerceros[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));

            ?>
        </div>
	    <div class="span3">
            <?php
            echo $form->labelEx($model,'id_banco');
            echo $form->dropDownList($model,'id_banco',CHtml::listData(Banco::model()->findAll(),'id','nombre'),array(
                'empty' => 'Seleccionar Banco',
                'ajax' => array(
                    'type'=>'POST', //request type
                    'url'=>CController::createUrl('SucursalBanco/ListarSucursales'), //url to call.
                    'update'=>'#ChequeDeTerceros_id_sucursal', //selector to update
                ),

            ));
            ?>

        </div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'id_sucursal');
            if($model->isNewRecord){
                echo $form->dropDownList($model,'id_sucursal',CHtml::listData(array(),'',''),array('empty' => 'Seleccionar Sucursal'));
            }else{
                echo $form->dropDownList($model,'id_sucursal',CHtml::listData(SucursalBanco::model()->findAllByAttributes(array('id_banco'=>$model->id_banco)),'id','nombre'),array('empty' => 'Seleccionar Sucursal'));
            }

            ?>
		</div>
    </div>
    <div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importe',array('class'=>'span12')); ?>
		</div>

        <div class="span3">
			<?php
            echo $form->labelEx($model,'propio');
            echo $form->dropDownList($model,'propio',ChequeDeTerceros::$aSiNo,array('class'=>'span12')); ?>
		</div>

        <?php
        if ($model->id_pagoCliente != null){
            echo $form->hiddenField($model,'id_cliente');
            echo $form->hiddenField($model,'id_pagoCliente   ');
        }else{
            echo '<div class="span3">';
            echo $form->labelEx($model,'id_cliente');
            echo $form->dropDownList($model,'id_cliente',CHtml::listData(Cliente::model()->findAll(),'id','Title'),array());
            echo '</div>';
        }

        ?>
	</div>
</div>


	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>


<?php $this->endWidget(); ?>
