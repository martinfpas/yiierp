
/* @var $this ChequeDeTercerosController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Cheque De Terceros',
);

$this->menu=array(
	array('label'=>'Nuevo ChequeDeTerceros', 'url'=>array('create')),
	array('label'=>'Gestion de ChequeDeTerceros', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>