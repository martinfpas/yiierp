<?php
$this->breadcrumbs=array(
	'Cheque De Terceros'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ChequeDeTerceros', 'url'=>array('create')),
	array('label'=>'Ver ChequeDeTerceros', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ChequeDeTerceros', 'url'=>array('admin')),
);
?>

	<h1>Modificando Cheque De Terceros <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>