<?php
/* @var $this ChequeDeTercerosController */
/* @var $model ChequeDeTerceros */

$this->breadcrumbs=array(
	'Cheque De Terceros'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ChequeDeTerceros', 'url'=>array('admin')),
);
?>

<h1>Creando ChequeDeTerceros</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>