<?php
/* @var $this AlicuotasCompraController */
/* @var $data AlicuotasCompra */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idComprobanteCompra')); ?>:</b>
	<?php echo CHtml::encode($data->idComprobanteCompra); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('impNetoGravado')); ?>:</b>
	<?php echo CHtml::encode($data->impNetoGravado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alicuota')); ?>:</b>
	<?php echo CHtml::encode($data->alicuota); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('impuestoLiquidado')); ?>:</b>
	<?php echo CHtml::encode($data->impuestoLiquidado); ?>
	<br />


</div>