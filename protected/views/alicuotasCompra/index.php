
/* @var $this AlicuotasCompraController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'AlicuotasCompra::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo AlicuotasCompra', 'url'=>array('create')),
	array('label'=>'Gestion de AlicuotasCompra', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>