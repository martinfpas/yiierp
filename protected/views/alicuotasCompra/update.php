<?php
$this->breadcrumbs=array(
	AlicuotasCompra::model()->getAttributeLabel('models') => array('admin'),
	AlicuotasCompra::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.AlicuotasCompra::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.AlicuotasCompra::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.AlicuotasCompra::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=AlicuotasCompra::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>