<?php
/* @var $this AlicuotasCompraController */
/* @var $model AlicuotasCompra */

$this->breadcrumbs=array(
	AlicuotasCompra::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.AlicuotasCompra::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=AlicuotasCompra::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>