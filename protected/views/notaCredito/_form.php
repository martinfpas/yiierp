<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'nota-credito-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorNotaCredito"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okNotaCredito">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'clase',array('class'=>'span12','maxlength'=>1)); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'Nro_Puesto_Venta');
            echo $form->dropDownList($model,'Nro_Puesto_Venta',CHtml::listData(PuntoDeVenta::model()->findAll(),'num','descripcion'),array());
            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'Iva_Responsable');
            echo $form->dropDownList($model,'Iva_Responsable',CHtml::listData(CategoriaIva::model()->findAll(),'id_categoria','nombre'),array());
            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Nro_Viajante',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'NotaCredito[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));

            ?>
		</div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'Id_Camion');
            echo $form->dropDownList($model,'Id_Camion',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array());
            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Final',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Enviada',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Recibida',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Observaciones',array('class'=>'span12','maxlength'=>30)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Otro_Iva',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento1',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Descuento2',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Total_Neto',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'CAE',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Vencim_CAE',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Ing_Brutos',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
