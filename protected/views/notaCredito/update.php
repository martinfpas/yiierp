<?php
/* @var $model NotaCredito */

$this->breadcrumbs=array(
	'Notas de Credito'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.NotaCredito::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.NotaCredito::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.NotaCredito::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);

Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        var modoEdicion = true;
		// PARA PONER LAS AYUDAS
		try{
			$('label').tooltip();
			$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		getNotaCredito('','".$model->id."','');
		
		// ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		$('#idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            console.log(keycode);
                        
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		
		$('#idCliente').change(function (ev) {
		    getNotaCredito($('#idCliente').val());
		});
		
		
    }); 
	
	function getNotaCredito(idCliente,idNotaCredito,idNotaPedido){
	    var datos = 'idNotaPedido='+idNotaPedido+'&id='+idNotaCredito+'&idCliente='+idCliente;
	    console.log(datos);
        $.ajax({
            url: '".$this->createUrl('notaCredito/FormAsync')."',
            type: 'Get',
            data: datos,
            success: function (html) {
                $('#cuerpo').html(html);
                $('#cuerpo').find('#fecha').datepicker();
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR
                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                //var htmlError = $(errorDiv).parent().next().html();
                //$('#errorFactura').html(htmlError);
                //$('#errorFactura').show('slow');
                alert(respuesta);
                
                
            }
        });
	    
	}
	
	
	
",CClientScript::POS_READY);

if ($model->estado == Comprobante::iBorrador){
    echo '<h3>Creando Nota de Credito</h3>';
}else{
    echo '<h3>Modificando Nota de Credito'.$model->id.'</h3>';
}
?>

<div class="alert alert-block alert-error" style="display:none;" id="errorCLiente"></div>
<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>

<div id="fichaCliente"></div>
<div id="cuerpo"></div>

<?php
$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => true));
?>