<style>
    table.detail-view th {
        text-align: right;
        width: 90px;
    }
    h3{
        margin: 0px;
    }
    .grid-view{
        padding-top: 0px;
    }
</style>
<?php
/* @var $model NotaCredito */

if($is_modal){
    Yii::app()->clientScript->registerScript('cerrarContenedor', "    
        const isModal = true;
        try{                
            $('#descartar').closest('.modal.fade.in').modal('hide');
        }catch(e){
            //TODO: avisar que no se puede cerrar
            console.error(e);
        }
    ");
}else{
    Yii::app()->clientScript->registerScript('cerrarContenedor', "    
        const isModal = false;
        $(document).ready(function(){            
            bindNCHandlers()    
        });
        function cerrarContenedor(){
            window.location = '".Yii::app()->createUrl('notaCredito/admin')."';        
        }
    ");
}

Yii::app()->clientScript->registerScript('notaCreditoHandlers', "
    $('.print-button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        let href = $(this).attr('href');
        let print = $(this).attr('print');
        $('#download-form').attr('action',href);
        $('#print-form').attr('action',print);        
        $('#modalPrint').modal();
   
        $('#modalPrint button').click(function(){
            if(isModal){            
                $('#modalPrint').modal('hide');
                cerrarContenedor();                
            }else{
                window.location = '".Yii::app()->createUrl(get_class($model).'/view',array('id'=>$model->id))."';
            }            
        });
    });
    
    $('#descartar').click(function(e){
        console.log('#descartarNC');
        var datos = '&ajax=';
        var url = $(this).attr('url')+'&ajax=';
        
        $.ajax({
            url:url,
            type:'POST',
            data: datos,
            success:function(html) {
              //TODO: SE HACE ALGO CON EL html?              
              cerrarContenedor();              
              return true;
            },
            error:function(x,y,z) {
              // TODO: MOSTRAR LOS ERRORES
              console.log(x);
              alert(x.responseText);
              return false;
            }
        });
    });
        
    function bindNCHandlers(){
        $('#idCae').click(function(e){
            //var datos = 'id='+$('#idCarga').val()+'&ajax=';
            e.stopImmediatePropagation();
            e.stopPropagation();
            e.preventDefault();            
            
            var datos = '';
            var url = $(this).attr('href');
            console.log(url);
                        
            $.ajax({
                url:url,
                type:'GET',
                data: datos,
                beforeSend: function() { $('.modalLoading').show('slow'); },
                complete: function() { 
                  $('.modalLoading').hide('slow'); 
                  $('#idCae').closest('.modal.fade.in').modal('hide');
                  $('.modalLoading').hide('slow');
                },
                success:function(html) {
                   try{
                    $('.print-button').show();
                    $('.print-button').click();
                    $('#idCae').hide();                                      
                  }catch(e){
                    console.error(e);
                  }
                  return false;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  respuesta =  $.parseHTML(x.responseText);
                  errorDiv = $(respuesta).find('.errorSpan');
                  console.log(errorDiv);
                  var htmlError = $(errorDiv).html();
                  
                  console.log(htmlError);
                  if (typeof value === 'undefined') {
                        alert($(respuesta).text());
                  }else{
                        alert(htmlError);
                  }
                  $('.modalLoading').hide('slow');
                  return false;
                  
                }
            });
        });
    }
",CClientScript::POS_READY);
?>
    <div class="content-fluid">
        <div class="row-fluid">
            <div class="span5 bordeRedondo">

                <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(

                        'razonSocial',
                        'direccion',
                        'localidad',
                        'provincia',
                        'rubro',
                        'zona',
                        'Id_Cliente',

                    )));
                ?>
            </div>
            <div class="span2 bordeRedondo" style="text-align: center">
                <div class="row-fluid">
                    <b>Comprobante</b>
                </div>
                <div class="row-fluid">
                    <h3><?=$model->clase?></h3>
                </div>
                <div class="row-fluid">
                    <b>Pto Vta</b>
                </div>
                <div class="row-fluid">
                    <h3><?=str_pad($model->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT)?></h3>
                </div>
                <div class="row-fluid">
                    <b>Nro Comprobante</b>
                </div>
                <div class="row-fluid">
                    <h3><?=($model->Nro_Comprobante > 0 )? str_pad($model->Nro_Comprobante,4,'0',STR_PAD_LEFT) : '-'?></h3>
                </div>
            </div>
            <div class="span5 bordeRedondo">
                <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'value' => $model->tipoResponsable,
                            'label' => 'Tipo Resp.'
                        ),
                        'cuit',
                        'perIIBB',
                        array(
                            'name' => 'nroInscripIIBB',
                            'label' => 'Nº Insc.IIBB',
                        ),
                        array(
                            'name' => 'idNotaPedido',
                            'value' => ($model->oNotaPedido != null)? $model->oNotaPedido->NroDig : 'No Vinculado',
                        )
                    ),
                )); ?>

                <?php
                $this->widget('editable.EditableDetailView', array(
                    'data'       => $model,

                    //you can define any default params for child EditableFields
                    'url'        => $this->createUrl('NotaCredito/SaveFieldAsync'), //common submit url for all fields
                    'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                    //'apply' => false, //you can turn off applying editable to all attributes

                    'attributes' => array(

                        array( //select loaded from database
                            'name' => 'Id_Camion',
                            'editable' => array(
                                'type'   => 'select',
                                'source' => Editable::source(Vehiculo::model()->findAll(), 'id_vehiculo', 'IdNombre')
                            )
                        ),
                        array(
                            'name' => 'fecha',
                            'editable' => array(
                                'type'       => 'date',
                                'format'      => 'yyyy-mm-dd', //format in which date is expected from model and submitted to server
                                'viewformat' => 'dd/mm/yyyy'
                            )
                        ),

                    )
                ));
                ?>
            </div>
        </div>
    </div>


    <?php
        $this->widget('ArticuloVentaCSearch',

            array(
                'js_comp' => 'ProdNotaCredito_idArticuloVta',
                'is_modal' => false,
                'model' => $modelProd ,
                'modelParent' => $model,
                'idDivNuevo' => "nuevoProdNotaCredito",
                'idMasProd'  => "masProdNotaCredito",
                'cantidad' => 'ProdNotaCredito_cantidad',
                'precioUnit'  => 'ProdNotaCredito_precioUnitario',
                'idArtVenta'  => 'ProdNotaCredito_idArtVenta',
                'descripcion'  => 'ProdNotaCredito_descripcion',
                'idPaquete' => '',
                'id_form'  => 'prod-nota-credito-form',
                'urlSaveAsync' => Yii::app()->createUrl('ProdNotaCredito/SaveAsync'),
                'id_grid'  => 'prod-nota-credito-grid',
                'aHiddenFields'  => array(
                    'idComprobante',
                    'idArtVenta'
                ),
                'conBuscador'  => true,
                'alicuota' => $model->getAlicuotaItem(),
            )
        );
    ?>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-nota-credito-grid',
    'dataProvider'=>$aProdNotaCredito->search(),
    'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");refreshComprobanteDetails();}',
    'columns'=>array(
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'cantidad',
            'value' => 'number_format($data->cantidad,"2",".","")',
            'editable' => array(    //editable section
                'apply'      => '$data->oNotaCredito->estado < Comprobante::iCerrada', //can't edit deleted users
                'url'        => $this->createUrl('ProdNotaCredito/SaveFieldAsync'),
                'placement'  => 'right',
                'display' => 'js: function(value, sourceData) {
                                console.log("sourceData");
                                console.log(sourceData);
                                console.log(value);
                                 if(!isNaN(value) > 0){
									$(this).html(parseFloat(value).toFixed(2));								
								}else{
									$(this).html("0.00");							
								}								
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                    //refreshComprobanteDetails();
                                    try{
                                        $.fn.yiiGridView.update("prod-nota-credito-grid");
                                    }catch(e){
                                        console.log(e);
                                    };
                             }',
            ),
            'htmlOptions' => array('style' => 'width: 70px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
        ),

        array(
            'name' => 'codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => ''),
        ),
        'descripcion',

        array(
            'name' => 'precioUnitario',
            'value' => '"$".$data->precioUnitario',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'importe',
            'value' => '"$".$data->importe',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons'=>array(
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("ProdNotaCredito/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>

<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>

        <div class="span2"> Descuento 1 </div>

        <div class="span2">Descuento2</div>
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>
        <div class="span2">
            IIBB
        </div>
        <div class="span2" style="text-align: right;">
            <?php
            $this->widget('editable.EditableField', array(
                'type'      => 'select',
                'model'       => $model,
                'apply'      => '$model->CAE == "00000000000000" || $model->CAE == ""', //can't edit deleted users
                'attribute'   => 'alicuotaIva',
                'url'         => $this->createUrl('NotaCredito/SaveFieldAsync'),
                'placement'   => 'right',
                'source'    => Comprobante::$aIvaAlicuotasMontos,
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                'htmlOptions' => array(
                    'style' => "float: right;margin-left: 5px;",
                ),
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));?>
            &nbsp;<span style="float: right">IVA</span>&nbsp;
        </div>
        <div class="span2" ><b>Total $</b></div>

    </div>
    <div class="row-fluid" style="text-align: right;">

        <div class="span1">
            <b>$<span id="spanSubTotalNeto"><?=$model->SubTotalNeto ?></span></b>
            <?php
            /*
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            */
            ?>
        </div>
        <div class="span2">            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => '$model->CAE == "00000000000000" || $model->CAE == ""', //can't edit deleted users
                'attribute'   => 'Descuento1',
                'url'         => $this->createUrl('NotaCredito/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span2">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => '$model->CAE == "00000000000000" || $model->CAE == ""', //can't edit deleted users
                'attribute'   => 'Descuento2',
                'url'         => $this->createUrl('NotaCredito/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span1">
            <b>$<span id="spanSubTotal"><?=$model->subtotal ?></span></b>
            <?php
            /*
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            */
            ?>
        </div>
        <div class="span2" id="Ing_Brutos">
            <?php

                number_format($model->Ing_Brutos,2, '.', '');


            ?>
        </div>
        <?php
        if ($model->oIvaResponsable->discIva) {
            echo '<div class="span2" id="subtotalIva">$'.number_format($model->subtotalIva, 2, '.', '')    .'</div>';
        }else{
            echo '<div class="span2" id=""></div>';
        }
        ?>
        <div class="span2" style=""><b>$<span id="spanTotal"><?=number_format($model->Total_Final,2, '.', '')?></span></b></div>
    </div>
</div>
<div class="content-fluid">
    <div class="content-fluid">
        <div class="span12" style="margin: 10px;">
            <?php
            echo CHtml::label('Observaciones','Observaciones');
            $this->widget('editable.EditableField', array(
                'type'      => 'textarea',
                'model'     => $model,
                'attribute' => 'Observaciones',
                'url'       => $this->createUrl('NotaCredito/SaveFieldAsync'),
            ));
            ?>

        </div>

    </div>
</div>
<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span3" style="">
            <?php
            if($model->CAE == '00000000000000' || $model->CAE == '') {
                //TODO: VER CUANDO SE PUEDE DESCARTAR Y CUANDO SOLO SE HACE NOTA DE CREDITO
                echo CHtml::link('Descartar Nota de Crédito', '#', array('class' => 'btn btn-danger', 'id' => 'descartar', 'url' => Yii::app()->createUrl('notaCredito/delete',array('id'=>$model->id)), 'style' => 'float:left;margin-top: 25px;'));
            }
            ?>
        </div>

        <div class="span2"></div>

        <div class="span4" style=""></div>
        <div class="span3">
            <?php


            if($model->esElectronica()){
                if($model->CAE == '00000000000000' || $model->CAE == ''){
                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idCae" href="'.Yii::app()->createUrl('afipVoucher/Emitir',array('id'=>$model->id)).'">Guardar y obtener CAE</a>';
                    echo CHtml::link('Imprimir <i class="icon-white icon-print"></i>',Yii::app()->createUrl('notaCredito/Pdf', array('id'=>$model->id)),
                        array('class'=>'btn btn-primary print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;display:none', 'target' => 'blank',
                            'print' => Yii::app()->createUrl('notaCredito/Pdf', array('id'=>$model->id,'print'=>true)))
                    );
                }else{
                    //echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('factura/Pdf',array('id'=>$model->id)).'">Imprimir.</a>';
                    echo CHtml::link('Imprimir <i class="icon-white icon-print"></i>',Yii::app()->createUrl('notaCredito/Pdf', array('id'=>$model->id)),
                        array('class'=>'btn btn-primary print-button','id' => 'rendir','style' => 'float:left;margin-right:5px;', 'target' => 'blank',
                            'print' => Yii::app()->createUrl('notaCredito/Pdf', array('id'=>$model->id,'print'=>true)))
                    );
                }
            }else{
                echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('notaCredito/Pdf',array('id'=>$model->id)).'">Guardar e Imprimir</a>';
            };
            ?>

        </div>

    </div>
</div>
<div class="modalLoading" ><!-- Place at bottom of page --></div>
<?php
if(!$is_modal) {
    $this->widget('PrintWidget');
}
?>