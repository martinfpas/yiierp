
/* @var $this NotaCreditoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Nota Creditos',
);

$this->menu=array(
	array('label'=>'Nuevo '.NotaCredito::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Gestion de '.NotaCredito::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>


<h1><?=NotaCredito::model()->getAttributeLabel('models')?></h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>