<?php
/* @var $this NotaCreditoController */
/* @var $model NotaCredito */

$this->breadcrumbs=array(
	'Notas de Credito'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nueva Nota de Credito', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#nota-credito-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Notas de Crédito</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'nota-credito-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(

        array(
            'name' => 'clase',
            'htmlOptions' => array('style' => 'width:50px'),
            'filter' =>  Comprobante::$aClase,
        ),
        array(
            'name' => 'Nro_Puesto_Venta',
            'value' => '$data->Nro_Puesto_Venta',
            'header' => 'N.P.Vta',
            'htmlOptions' => array('style' => 'width:50px'),
            'filter' =>  CHtml::listData(PuntoDeVenta::model()->activos()->findAll(),'num','num'),

        ),
		array(
			'name' => 'Nro_Comprobante',
			'header' => 'Nº',
			'htmlOptions' => array('style' => 'width:70px'),
        ),
        array(
            'name' => 'fecha',
			'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
			'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
		),

        'razonSocial',
        array(
            'name' => 'Iva_Responsable',
            'value' => '$data->oIvaResponsable->nombre',
            'htmlOptions' => array('style' => ''),
            'filter' =>  CHtml::listData(CategoriaIva::model()->findAll(),'id_categoria','nombre'),

        ),
        'localidad',
        //'provincia',
        array(
            'name' => 'Total_Final',
            'value' => '"$".number_format($data->Total_Final,2,".","")',
            'header' => 'Monto',
            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
			'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
        ),
		/*
		'Id_Cliente',
		'fecha',
		'Id_Camion',
		'Total_Final',
		'Enviada',
		'Recibida',
		'Observaciones',
		'Otro_Iva',
		'Descuento1',
		'Descuento2',
		'Total_Neto',
		'CAE',
		'Vencim_CAE',
		'Ing_Brutos',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}',
            'buttons' => array(
                'update' => array(
                    'visible' => '$data->estado == Comprobante::iBorrador || $data->estado == Comprobante::iGuardada',
                )
            )
		),
	),
)); ?>
