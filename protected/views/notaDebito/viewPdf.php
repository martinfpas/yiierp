<div class="framePpal" style="font-style: normal !important;" >
    <div class="container-fluid" style="width: 100%;margin: 0px;padding: 0px;">
        <div class="row-fluid" id="header1" style="">
            <div class="span5" style="padding-top: 10px;">
                <img src="<?=Yii::getPathOfAlias('webroot.images')?>/logoFactura.jpg" width="280px;">
            </div>
            <div class="span3" id="tipoFactura" style="text-align: center;">
                <h1><?=$model->clase?><br><?=$copia?></h1>
            </div>
            <div class="span4" style="font-weight: bold;font-size: 26;text-align: center;padding-top: 30px;">
                NOTA DE DEBITO
            </div>
        </div>
        <div class="row-fluid"  id="header2" style="height: 100px; vertical-align: bottom;border-bottom: 1px solid black;width: 100%">
            <?=$model->Header?>
        </div>
        <div class="row-fluid" id="header3" style="height: 70px; vertical-align: bottom;border-bottom: 1px solid black;width: 100%">
            <div class="span7" >
                Cliente:	<?=$model->oCliente->title?><br>
                Domicilio: <?=$model->oCliente->direccion?><br>
                Localidad: (<?=$model->oCliente->cp?>) <?=(strtoupper($model->oCliente->localidad))?> - <?=strtoupper($model->oCliente->provincia)?><br>
                Condicion de venta:<br>
            </div>
            <div class="span5">
                N° Cuenta: <?=$model->Id_Cliente?><br>
                Cond. Frente al IVA: <?=$model->oIvaResponsable->nombre?><br>
                CUIT: 	<?=$model->oCliente->cuit?><br>

            </div>
        </div>
        <div class="row-fluid" id="prods" style="">
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'prod-nota-debito-grid',
                'template' => '{items}',
                'dataProvider'=>$aProdComprobante->searchWP(),
                'enableSorting' => false,
                'htmlOptions' => array(
                    'style' => 'width:100%;',
                ),
                'columns'=>array(
                    array(
                        'name' => 'idArtVenta',
                        'header' => 'Codigo',
                        'value' => '$data->oArtVenta->codigo',
                        'htmlOptions' => array('style' => 'text-align:center;'),
                        'headerHtmlOptions' => array('style' => 'text-align:center;width:80px;'),
                    ),
                    array(
                        'name' => 'idArtVenta',
                        'header' => 'Producto',
                        'value' => '$data->oArtVenta->descripcion',
                        'headerHtmlOptions' => array('style' => 'text-align:left;'),
                        'htmlOptions' => array('style' => 'width:350px;'),
                    ),

                    array(
                        'name' => 'cantidad',
                        'header' => 'Cant.',
                        'value' => 'number_format($data->cantidad,2)',
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                        'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
                    ),
                    array(
                        'name' => 'precioUnitario',
                        'header' => 'P.U.',
                        'value' => 'number_format($data->precioUnitario,2)',
                        'headerHtmlOptions' => array('style' => 'text-align:right;'),
                        'htmlOptions' => array('style' => 'text-align:right;width:80px;'),
                    ),

                    array(
                        'name' => 'SubTotal',
                        //'value' => '"$".number_format($data->SubTotal,2)',
                        'value' => '$data->SubTotal',
                        'header' => 'Total',
                        'htmlOptions' => array('style' => 'text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;width:130px;'),
                        //'footerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),

                    ),

                ),
            )); ?>
        </div>
        <div class="row-fluid" style="width: 100%;padding: 3px;">
            <?=$model->getItemsFooter()?>
        </div>
        <div class="row-fluid" style="width: 100%;padding-left: 62px;font-size:14">
            <b>SON PESOS <?=ComponentesComunes::numtoletras($model->Total_Final)?><br/></b>
            <b>Recibí importe por ERCA S.R.L.</b>
        </div>
        <div class="row-fluid" style="width: 100%;padding: 3px;font-size: 10">
            <p id="losPagos"><span id="laMercaderia">LA MERCADERIA VIAJA POR CUENTA Y RIESGO DEL COMPRADOR. Rogamos revisar la mercadería antes de recibirla.</span><br>Los pagos deben efctuarse a la orden de ERCA SRL. Todo pago fuera de termino devengará intereses punitorios. Tipo de cambio de venta en dolares EEUU. El monto en pesos de esta factura se ajustará conforme a la variación del tipo de cambio vendedor del BNA de la fecha efectiva de acreditación del pago.</p>
            <br>
        </div>
        <div class="row-fluid" style="width: 100%;padding: 3px;">
            <div class="span6" style="padding-left: 15px;">
                <?=($model->CAE != '' & $model->CAE != '')? 'AFIP CPTE AUTORIZADO' : '' ?>
                <?php
                if($model->CAE != '' & $model->CAE != ''){
                    echo CHtml::image($model->getAliasCaePath(),$model->CAE,array('height'=>'20px','width'=>'328px'));
                }
                ?>
                <br>
                <?=$model->codigoDeBarras()?>
            </div>
            <div class="span5">
                CAE Nro: <?=$model->CAE?><br>
                Fecha Vto CAE: <?=($model->CAE != '' & $model->CAE != '')? ComponentesComunes::fechaFormateada($model->Vencim_CAE) : ''?>
            </div>
        </div>
        <div class="row-fluid" style="width: 100%;padding: 3px;text-align: left;">

        </div>
    </div>
</div>
