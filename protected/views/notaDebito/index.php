
/* @var $this NotaDebitoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Nota Debitos',
);

$this->menu=array(
	array('label'=>'Nueva Nota de Debito', 'url'=>array('create')),
	array('label'=>'ABM Notas de Debito', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>