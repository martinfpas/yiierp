<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
        <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model,'Id_Camion',array('class'=>'span12')); ?>
    </div>
    <!--
    <div class="span4">
		<?php
    echo $form->labelEx($model,'clase');
    echo $form->dropDownList($model,'clase',Comprobante::$aClase,array('class'=>'span12','maxlength'=>1));
    ?>
    </div>

    <div class="span4">
		<?php
    echo $form->labelEx($model,'Nro_Puesto_Venta');
    echo $form->dropDownList($model,'Nro_Puesto_Venta',CHtml::listData(PuntoDeVenta::model()->activos()->findAll(),'num','num'),array('class'=>'span12','maxlength'=>1));
    ?>
    </div>
    -->
</div>
<div class="row-fluid">
    <!--
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Nro_Comprobante',array('class'=>'span12')); ?>
    </div>

    <div class="span4">

        <?php
    echo $form->labelEx($model,'Iva_Responsable');
    echo $form->dropDownList($model,'Iva_Responsable',CHtml::listData(CategoriaIva::model()->findAll(),'id_categoria','nombre'),array('class'=>'span12'));
    ?>
    </div>
    -->
    <div class="span4">
        <?php echo $form->textFieldRow($model,'Nro_Viajante',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
        <?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
    </div>
    <div class="span4" style="padding-top: 24px">

        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'buttonType' => 'submit',
            'type'=>'primary',
            'label'=>'Buscar',
        )); ?>

    </div>
</div>
<div class="row-fluid">



</div>
<!--
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Total_Final',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Enviada',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Recibida',array('class'=>'span12')); ?>
    </div>
</div>

<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Observaciones',array('class'=>'span12','maxlength'=>30)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Otro_Iva',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Descuento1',array('class'=>'span12')); ?>
    </div>
</div>

<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Descuento2',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Total_Neto',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'CAE',array('class'=>'span12')); ?>
    </div>
</div>
-->
<div class="row-fluid">
    <!--
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Vencim_CAE',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'Ing_Brutos',array('class'=>'span12')); ?>
    </div>
    -->

</div>
<?php $this->endWidget(); ?>
