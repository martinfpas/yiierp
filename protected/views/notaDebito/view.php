<?php
/* @var $model NotaDebito */

$this->breadcrumbs=array(
    NotaDebito::model()->getAttributeLabel('models')=>array('admin'),
	$model->id,
);

if($model->estado < Comprobante::iCerrada){
	$this->menu=array(
		array('label'=>'Nueva '.NotaDebito::model()->getAttributeLabel('model'), 'url'=>array('create')),
		array('label'=>'Modificar '.NotaDebito::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Borrar '.NotaDebito::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
		array('label'=>'Gestion de '.NotaDebito::model()->getAttributeLabel('models'), 'url'=>array('admin')),
	);
}else{
	$this->menu=array(
		array('label'=>'Gestion de Notas de Débito', 'url'=>array('admin')),
	);
}


?>

<h3><?=NotaDebito::model()->getAttributeLabel('model')?></h3>

<div class="content-fluid">
        <div class="row-fluid">
            <div class="span5 bordeRedondo">

                <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(

                        'razonSocial',
                        'direccion',
                        'localidad',
                        'provincia',
                        'rubro',
                        'zona',
                        'Id_Cliente',

                    )));
                ?>
            </div>
            <div class="span2 bordeRedondo" style="text-align: center">
                <div class="row-fluid">
                    <b>Comprobante</b>
                </div>
                <div class="row-fluid">
                    <h3><?=$model->clase?></h3>
                </div>
                <div class="row-fluid">
                    <b>Pto Vta</b>
                </div>
                <div class="row-fluid">
                    <h3><?=str_pad($model->Nro_Puesto_Venta,4,'0',STR_PAD_LEFT)?></h3>
                </div>
                <div class="row-fluid">
                    <b>Nro Comprobante</b>
                </div>
                <div class="row-fluid">
                    <h3><?=($model->Nro_Comprobante > 0 )? str_pad($model->Nro_Comprobante,8,'0',STR_PAD_LEFT) : '-'?></h3>
                </div>
            </div>
            <div class="span5 bordeRedondo">
                <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'tipoResponsable',
                        'cuit',
                        'perIIBB',
                        'nroInscripIIBB',
                        'idNotaPedido',
                        'Id_Camion',
                        array(
                            'name' => 'fecha',
                            'value' => ComponentesComunes::fechaFormateada($model->fecha),
                        )
                    ),
                )); ?>
            </div>
        </div>
    </div>

<?php 

$oComprobante = Comprobante::model()->findByPk($model->idNotaDeCredito);
if($oComprobante->tipo == 'NC'){
    $this->widget('bootstrap.widgets.TbButton',array(
        'buttonType' => 'link',
        'type' => 'success',
        'label' => 'Ver Nota De Credito asociada',
        'url' => Yii::app()->createUrl('NotaCredito/view',array('id' => $model->idNotaDeCredito)),
    ));
}else{
    $this->widget('bootstrap.widgets.TbButton',array(
        'buttonType' => 'link',
        'type' => 'success',
        'label' => 'Ver Factura asociada',
        'url' => Yii::app()->createUrl('Factura/view',array('id' => $model->idNotaDeCredito)),
    ));
}



$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-nota-credito-grid',
    'dataProvider'=>$aProdNotaDebito->search(),
    'columns'=>array(
        // TODO: GRABAR
        //'nroItem',
        array(
            'name' => 'cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => ''),

        ),
        'descripcion',
        array(
            'name' => 'precioUnitario',
            'value' => '"$".number_format($data->precioUnitario,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'importe',
            'value' => '"$".number_format($data->importe,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        'precio',
        */
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{delete}',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("ProdNotaDebito/delete", array("id"=>$data->id))',
                    'visible' => '$data->oNotaDebito->estado == Comprobante::iBorrador || $data->oNotaDebito->estado == Comprobante::iGuardada',
                ),
            ),
        ),
    ),
)); ?>



<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>

        <div class="span2"> Descuento 1 </div>

        <div class="span2">Descuento2</div>
        <div class="span1" style="">
            <b>Subtotal: </b>
        </div>
        <div class="span2">
            IIBB
        </div>
        <div class="span2" style="text-align: right;">
            <?php
            $this->widget('editable.EditableField', array(
                'type'      => 'select',
                'model'       => $model,
                'apply'      => false,
                'attribute'   => 'alicuotaIva',
                'url'         => $this->createUrl('NotaDebito/SaveFieldAsync'),
                'placement'   => 'right',
                'source'    => Comprobante::$aIvaAlicuotasMontos,
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                'htmlOptions' => array(
                    'style' => "float: right;margin-left: 5px;",
                ),
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));?>
            &nbsp;<span style="float: right">IVA</span>&nbsp;
        </div>
        <div class="span2" ><b>Total $</b></div>

    </div>
    <div class="row-fluid" style="text-align: right;">

        <div class="span1">
            <b>$<span id="spanSubTotalNeto"><?=$model->SubTotalNeto ?></span></b>
            <?php
            /*
            echo $form2->hiddenField($model,'id');
            echo $form2->hiddenField($model,'clase');
            echo $form2->hiddenField($model,'fecha');
            echo $form2->hiddenField($model,'Nro_Puesto_Venta');
            */
            ?>
        </div>
        <div class="span2">            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => false,
                'attribute'   => 'Descuento1',
                'url'         => $this->createUrl('NotaDebito/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span2">
            <?php
            $this->widget('editable.EditableField', array(
                'model'       => $model,
                'apply'      => false,
                'attribute'   => 'Descuento2',
                'url'         => $this->createUrl('NotaDebito/SaveFieldAsync'),
                'placement'   => 'right',
                'display' => 'js: function(value, sourceData) {
								if(value > 0){
									$(this).html(parseFloat(value).toFixed(2)+" %");								
								}else{
									$(this).html("0 %");							
								}
							}',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                console.log("refreshFacturaDetails()");
                                refreshComprobanteDetails();
                             }',
            ));

            ?></div>
        <div class="span1">
            <b>$<span id="spanSubTotal"><?=$model->subtotal ?></span></b>

        </div>
        <div class="span2" id="Ing_Brutos">
            <?php
                number_format($model->Ing_Brutos,2, '.', '');
            ?>
        </div>
        <div class="span2" id="subtotalIva">
            $<?=number_format($model->subtotalIva,2, '.', '')?>
        </div>
        <div class="span2" style=""><b>$<span id="spanTotal"><?=number_format($model->Total_Final,2, '.', '')?></span></b></div>
    </div>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span12" style="margin: 10px;">
            <?php
            echo CHtml::label('Observaciones','Observaciones');
            $this->widget('editable.EditableField', array(
                'type'      => 'textarea',
                'apply'      => false,
                'model'     => $model,
                'attribute' => 'Observaciones',
                'url'       => $this->createUrl('NotaDebito/SaveFieldAsync'),
            ));
            ?>

        </div>
    </div>

</div>
<div class="content-fluid bordeRedondo">
    <div class="row-fluid" style="text-align: right;">
        <div class="span5" style="">
            <?php
                if($model->CAE != '00000000000000' & $model->CAE != ''){
                    echo '<img src="'.$model->getCaePath().'" >';
                }else{
                    //TODO: VER CUANDO SE PUEDE DESCARTAR Y CUANDO SOLO SE HACE NOTA DE CREDITO
                    echo CHtml::link('Descartar Nota de Crédito', '#', array('class' => 'btn btn-danger', 'id' => 'descartar', 'url' => Yii::app()->createUrl('notaDebito/delete',array('id'=>$model->id)), 'style' => 'float:left;margin-top: 25px;'));
                }
            ?>
        </div>

        <div class="span4" style="">

            <?=$model->codigoDeBarras()?>
            <br>
            CAE :<?=$model->CAE?>
            <br>
            Vence CAE :<?=ComponentesComunes::fechaFormateada($model->Vencim_CAE)?>
        </div>
        <div class="span3">
            <?php


            if($model->esElectronica()){

                if($model->CAE == '00000000000000' || $model->CAE == ''){

                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idCae" href="'.Yii::app()->createUrl('afipVoucher/Emitir',array('id'=>$model->id)).'">Guardar y obtener CAE</a>';
                }else{

                    echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('NotaDebito/Pdf',array('id'=>$model->id)).'">Imprimir.</a>';

                }


            }else{

                echo '<a class="btn btn-primary" target="_blank" style="margin-top: 25px;" id="idPdf" href="'.Yii::app()->createUrl('NotaDebito/Pdf',array('id'=>$model->id)).'">Guardar e Imprimir</a>';

            };
            ?>

        </div>

    </div>
</div>