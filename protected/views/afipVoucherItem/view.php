<?php
$this->breadcrumbs=array(
	'Afip Voucher Items'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucherItem', 'url'=>array('create')),
	array('label'=>'Modificar AfipVoucherItem', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar AfipVoucherItem', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'ABM AfipVoucherItem', 'url'=>array('admin')),
);
?>

<h1>Ver AfipVoucherItem #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idVoucher',
		'codigo',
		'descripcion',
		'cantidad',
		'codigoUnidadMedida',
		'precioUnitario',
		'impBonif',
		'importeItem',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
