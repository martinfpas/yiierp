<?php
/* @var $this AfipVoucherItemController */
/* @var $model AfipVoucherItem */

$this->breadcrumbs=array(
	'Afip Voucher Items'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM AfipVoucherItem', 'url'=>array('admin')),
);
?>

<h1>Creando AfipVoucherItem</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>