
/* @var $this AfipVoucherItemController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Afip Voucher Items',
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucherItem', 'url'=>array('create')),
	array('label'=>'Gestion de AfipVoucherItem', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>