<?php
$this->breadcrumbs=array(
	'Afip Voucher Items'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo AfipVoucherItem', 'url'=>array('create')),
	array('label'=>'Ver AfipVoucherItem', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'ABM AfipVoucherItem', 'url'=>array('admin')),
);
?>

	<h1>Modificando AfipVoucherItem <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>