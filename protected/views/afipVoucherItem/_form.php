<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'afip-voucher-item-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorAfipVoucherItem"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okAfipVoucherItem">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'idVoucher',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigo',array('class'=>'span12','maxlength'=>14)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoUnidadMedida',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'impBonif',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'importeItem',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
