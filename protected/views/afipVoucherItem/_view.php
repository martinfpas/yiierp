<?php
/* @var $this AfipVoucherItemController */
/* @var $data AfipVoucherItem */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idVoucher')); ?>:</b>
	<?php echo CHtml::encode($data->idVoucher); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigoUnidadMedida')); ?>:</b>
	<?php echo CHtml::encode($data->codigoUnidadMedida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioUnitario')); ?>:</b>
	<?php echo CHtml::encode($data->precioUnitario); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('impBonif')); ?>:</b>
	<?php echo CHtml::encode($data->impBonif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importeItem')); ?>:</b>
	<?php echo CHtml::encode($data->importeItem); ?>
	<br />

	*/ ?>

</div>