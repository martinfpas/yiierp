<?php
$this->breadcrumbs=array(
	MesesCargaCompra::model()->getAttributeLabel('models') => array('admin'),
	MesesCargaCompra::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.MesesCargaCompra::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.MesesCargaCompra::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.MesesCargaCompra::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=MesesCargaCompra::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>