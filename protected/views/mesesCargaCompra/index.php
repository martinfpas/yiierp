
/* @var $this MesesCargaCompraController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'MesesCargaCompra::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo MesesCargaCompra', 'url'=>array('create')),
	array('label'=>'Gestion de MesesCargaCompra', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>