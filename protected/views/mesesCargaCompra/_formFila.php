
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoMesesCargaCompra" id="masMesesCargaCompra" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoMesesCargaCompra" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'meses-carga-compra-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('MesesCargaCompra/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'meses-carga-compra-grid',
			'idMas' => 'masMesesCargaCompra',
			'idDivNuevo' => 'nuevoMesesCargaCompra',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorMesesCargaCompra"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okMesesCargaCompra">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'mesCarga',array('class'=>'span12','maxlength'=>7)); ?>
		</div>
			<div class="span3">
			<?php 
				if(!$model->isNewRecord){
					echo $form->labelEx($model,'activo'); 
	echo $form->dropDownList($model,'activo', array('0' => MesesCargaCompra::inactivo,'1' => MesesCargaCompra::activo),	array( 'class'=>'span3')); 
				}
				?> 		</div>
	
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'meses-carga-compra-grid',
'dataProvider'=>$aMesesCargaCompra->search(),
'columns'=>array(
		'id',
		'mesCarga',

			array(
				'name' => 'activo',
				'value' => 'MesesCargaCompra::$aEstado[$data->activo]',
				'headerHtmlOptions' => array('style' => 'width: 100px'),
				'filter' => MesesCargaCompra::$aEstado,
			),
				/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("MesesCargaCompra/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("MesesCargaCompra/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>