

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                    <div class='span3'>
                        <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
                    </div>
                    <div class='span3'>
                        <?php echo $form->textFieldRow($model,'mesCarga',array('class'=>'span12','maxlength'=>7)); ?>
                    </div>
                    <div class='span3'>
                        <?php echo $form->labelEx($model,'activo'); ?>
                        <?php echo $form->dropDownList($model,'activo', array('0' => MesesCargaCompra::inactivo,'1' => MesesCargaCompra::activo),    array('prompt' => '-- TODOS --', 'class'=>'campos2', 'style'=>'')); ?>
                    </div>
                    <div class='span3'>
                        <?php $this->widget('bootstrap.widgets.TbButton', array(
                            'buttonType' => 'submit',
                            'type'=>'primary',
                            'label'=>Yii::t('application','Buscar'),
                        )); ?>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>

