<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'meses-carga-compra-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorMesesCargaCompra"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okMesesCargaCompra">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'mesCarga',array('class'=>'span12','maxlength'=>7)); ?>
		</div>
		<div class="span3">
			<?php
                echo $form->labelEx($model,'activo');
                echo $form->dropDownList($model,'activo', array('0' => MesesCargaCompra::inactivo,'1' => MesesCargaCompra::activo),    array( 'style'=>''));
            ?>
		</div>
        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
