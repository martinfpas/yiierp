<?php
$this->breadcrumbs=array(
	MesesCargaCompra::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.MesesCargaCompra::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.MesesCargaCompra::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.MesesCargaCompra::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.MesesCargaCompra::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=MesesCargaCompra::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'mesCarga',
		'activo',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
