<?php
/* @var $this MesesCargaCompraController */
/* @var $model MesesCargaCompra */

$this->breadcrumbs=array(
	MesesCargaCompra::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.MesesCargaCompra::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=MesesCargaCompra::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>