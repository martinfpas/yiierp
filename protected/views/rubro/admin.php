<?php
/* @var $this RubroController */
/* @var $model Rubro */

$this->breadcrumbs=array(
	'Rubros'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo Rubro', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#rubro-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de Rubros</h3>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'rubro-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'id_rubro',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {update}'
		),
	),
)); ?>
