<?php
/* @var $this RubroController */
/* @var $model Rubro */

$this->breadcrumbs=array(
	'Rubros'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de Rubro', 'url'=>array('admin')),
);
?>

<h1>Creando Rubro</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>