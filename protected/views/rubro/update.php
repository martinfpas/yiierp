<?php
$this->breadcrumbs=array(
	'Rubros'=>array('index'),
	$model->id_rubro=>array('view','id'=>$model->id_rubro),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Gestion de Rubro', 'url'=>array('admin')),
);
?>

	<h1>Modificando Rubro <?php echo $model->id_rubro; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>