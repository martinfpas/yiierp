<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'rubro-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorRubro"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okRubro">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>40)); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
