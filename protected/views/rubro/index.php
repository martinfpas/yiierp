
/* @var $this RubroController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Rubros',
);

$this->menu=array(
	array('label'=>'Nuevo Rubro', 'url'=>array('create')),
	array('label'=>'Gestion de Rubro', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>