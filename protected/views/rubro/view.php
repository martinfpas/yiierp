<?php
$this->breadcrumbs=array(
	'Rubros'=>array('admin'),
	$model->id_rubro,
);

$this->menu=array(
	array('label'=>'Modificar Rubro', 'url'=>array('update', 'id'=>$model->id_rubro)),
	array('label'=>'Borrar Rubro', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_rubro),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de Rubro', 'url'=>array('admin')),
);
?>

<h1>Ver Rubro #<?php echo $model->id_rubro; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id_rubro',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
