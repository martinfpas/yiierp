<?php
$this->breadcrumbs=array(
	DetalleFacturaProv::model()->getAttributeLabel('models') => array('admin'),
	DetalleFacturaProv::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.DetalleFacturaProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.DetalleFacturaProv::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.DetalleFacturaProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=DetalleFacturaProv::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

