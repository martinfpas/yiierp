<?php
/* @var $this DetalleFacturaProvController */
/* @var $data DetalleFacturaProv */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArticulo')); ?>:</b>
	<?php echo CHtml::encode($data->idArticulo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioUnitario')); ?>:</b>
	<?php echo CHtml::encode($data->precioUnitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidadRecibida')); ?>:</b>
	<?php echo CHtml::encode($data->cantidadRecibida); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroItem')); ?>:</b>
	<?php echo CHtml::encode($data->nroItem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alicuota')); ?>:</b>
	<?php echo CHtml::encode($data->alicuota); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('idMateriaPrima')); ?>:</b>
	<?php echo CHtml::encode($data->idMateriaPrima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idFacturaProveedor')); ?>:</b>
	<?php echo CHtml::encode($data->idFacturaProveedor); ?>
	<br />

	*/ ?>

</div>