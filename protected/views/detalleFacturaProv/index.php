
/* @var $this DetalleFacturaProvController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'DetalleFacturaProv::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo DetalleFacturaProv', 'url'=>array('create')),
	array('label'=>'Gestion de DetalleFacturaProv', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>