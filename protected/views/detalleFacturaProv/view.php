<?php
$this->breadcrumbs=array(
	DetalleFacturaProv::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.DetalleFacturaProv::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.DetalleFacturaProv::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.DetalleFacturaProv::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.DetalleFacturaProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=DetalleFacturaProv::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idArticulo',
		'descripcion',
		'precioUnitario',
		'cantidadRecibida',
		'nroItem',
		'alicuota',
		'idMateriaPrima',
		'idFacturaProveedor',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
