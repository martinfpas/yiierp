<?php
/* @var $this DetalleFacturaProvController */
/* @var $model DetalleFacturaProv */

$this->breadcrumbs=array(
	DetalleFacturaProv::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.DetalleFacturaProv::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=DetalleFacturaProv::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>