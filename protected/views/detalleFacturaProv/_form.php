<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'detalle-factura-prov-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorDetalleFacturaProv"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okDetalleFacturaProv">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'idArticulo',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'cantidadRecibida',array('class'=>'span12')); ?>
		</div>

                    </div>
                    <div class="row-fluid">		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroItem',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'alicuota',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'idMateriaPrima',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'idFacturaProveedor',array('class'=>'span12')); ?>
		</div>

                    </div>
                    <div class="row-fluid">        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
