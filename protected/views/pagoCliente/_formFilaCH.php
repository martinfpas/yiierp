<style>
    .rowHidden{
        display: none;
    }
</style>
<?php
/* @var $this PagoClienteController */
/* @var $model ChequeDeTerceros */
/* @var $data PagoConCheque */
/* @var $aPagoConCheque PagoConCheque */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$idPagodeCliente = $aPagoConCheque->idPagodeCliente;

if(!empty($isAjaxMode) && $isAjaxMode){
    $cs->scriptMap=array(

        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    
    );
}

Yii::app()->clientScript->registerScript('die click', "

	      jQuery(function(){
	      	
	      	//if(jQuery('body').attr('deleteEventIsset') !== 'yes'){jQuery('body').attr('deleteEventIsset', 'yes');jQuery('#pago-con-cheque-grid a.PagoConCheque').live('click','');}
            //$('body').on('ajaxUpdate.editable',function(e){ if(e.target.id == 'pago-con-cheque-grid') yiiEditable2(); });
		 });

		", CClientScript::POS_END);

$cs->registerScript('formFilaCheque',"
    $('#ChequeDeTerceros_id_banco').change(function(){
        let idBanco = $(this).val();
        let nroCheque = $('#ChequeDeTerceros_nroCheque').val();
        var datos = 'id_banco='+idBanco;
        var datosSelect2 = 'q='+nroCheque+'&id_banco='+idBanco;
        
        $.ajax({
            url:'".$this->createUrl('ChequeDeTerceros/Select2')."',
            type: 'GET',
            data: datosSelect2,
            
            success: function (data){
                let cheques = JSON.parse(data);
                console.log(cheques.length);
                if(cheques.length > 0){
                    
                    var resp = confirm('Ya existe '+cheques.length+' cheque/s con ese numero y banco.\\n Seleccionar?');
                    if(resp){
                        buscarCheque(cheques[0].id);
                    }
                }  
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
                
            }
        });
        
        $.ajax({
            url:'".$this->createUrl('SucursalBanco/ListarSucursales')."',
            data: datos,
            type: 'POST',
            success: function (html){
                $('#ChequeDeTerceros_id_sucursal').html(html);
                $('#ChequeDeTerceros_id_sucursal').focus();                                    
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
                
            }                
        });        
    });
    
    function buscarCheque(id){
        // TODO: HACER LLAMADA AL JSON DE CHEQUE DE TERCEROS
        var datos = 'id='+id;
        $.ajax({
            url: '".$this->createUrl('ChequeDeTerceros/Json')."',
            data : datos,
            type: 'GET',
            success: function(data){
                var cheque = JSON.parse(data);
                console.log(cheque);
                $('#ChequeDeTerceros_id').val(id);
                $('#ChequeDeTerceros_importe').val(cheque.importe);
                $('#ChequeDeTerceros_importe').attr('disabled','disabled');
                let aFecha = cheque.fecha.split('-');
                $('#ChequeDeTerceros_fecha').val(aFecha[2]+'-'+aFecha[1]+'-'+aFecha[0]);
                $('#ChequeDeTerceros_fecha').attr('disabled','disabled');
                $('#ChequeDeTerceros_id_sucursal').val(cheque.id_sucursal);
                $('#ChequeDeTerceros_id_sucursal').attr('disabled','disabled');
                $('#ChequeDeTerceros_propio').val(cheque.propio);
                $('#ChequeDeTerceros_propio').attr('disabled','disabled');
            },
            error: function(x,y,z){
            
            },
        });
    }
    
    $('#ChequeDeTerceros_nroCheque').blur(function(e){
        e.stopPropagation();
        try{
        // ABRE EL COMBO PARA ELEJIR BANCO CUANDO PIERDE EL FOCO        
            $('#cheque-de-terceros-form .select2-drop').select2('open');
        }catch(e){
            console.warn(e);
        }
    });
    
    function descartarPagoConCheque(url){
        var datos = '&ajax=';
        var url = url+'&ajax=';
        $.ajax({
            url:url,
            data: datos,
            type: 'POST',
            success: function(html){
                console.log('success:...');
                $.fn.yiiGridView.update('pago-con-cheque-grid');
                return false;
            },
            error: function (x,y,z){
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                alert($(errorDiv).parent().next());
            }
        });
    }
    
    // ESTA LINEA PERMITE QUE EL SELECT2 DEL SELECTOR DE BANCO PUEDA TENER FOCO CUANDO ESTA EN MODAL. EVITA TENER QUE QUE SACAR EL tabindex='-1' 
    try{
        $.fn.modal.Constructor.prototype.enforceFocus = function() {};
    }catch(e){
        console.warn(e);
    }
                                
", CClientScript::POS_END);

?>

<div id="nuevoChequeDeTerceros" style="">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'cheque-de-terceros-form',
        'enableAjaxValidation'=>false,
        'action' => Yii::app()->createUrl('ChequeDeTerceros/SaveAsync'),
        'htmlOptions' => array(
            'class' => 'SaveAsyncForm',
            'idGrilla'=>'pago-con-cheque-grid',
            'idMas' => 'masChequeDeTerceros',
            'idDivNuevo' => 'nuevoChequeDeTerceros',
            'style' => 'margin-bottom:0px;'.(($bEditable)? '' : 'display:none;'),
        ),
    )); ?>

    <p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

    <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorChequeDeTerceros"></div>
    <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okChequeDeTerceros">Datos Guardados Correctamente !</div>

    <?php echo $form->errorSummary($model); ?>
    <div class="content-fluid">
        <div class="row-fluid">

            <div class="span2" style="width: 10%">
                <?php
                    echo $form->textFieldRow($model,'nroCheque',array('class'=>'span12','maxlength'=>'12','tabindex' => 1));
                ?>
            </div>
            <div class="span3"  style="width: 20%">
                <?php
                echo $form->hiddenField($model,'id');
                echo $form->textFieldRow($model,'id_banco',array('class'=>'span12'));
                //echo $form->hiddenField($model,'id_banco',array('class'=>'span12'));

                $this->widget('ext.select2.ESelect2',array(
                        'selector'=>'#ChequeDeTerceros_id_banco',
                        'options'=>array(
                            'ajax'=>array(
                                'url'=>Yii::app()->createUrl('banco/select2'),
                                'dataType'=>'json',
                                'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                'results'=>'js:function(data,page) { return {results: data}; }',
                            ),
                            'class' => 'span12',
                            'tabindex' => 2,
                        ),
                    )
                );
                ?>

            </div>
            <div class="span3"  style="width: 20%">
                <?php
                echo $form->labelEx($model,'id_sucursal');
                echo $form->dropDownList($model,'id_sucursal',CHtml::listData(array(),'',''),array('empty' => 'Seleccionar Sucursal','tabindex' => 3,'class' => 'span12'));
                ?>
            </div>
            <div class="span2" style="width: 100px;">
                <?php
                echo $form->labelEx($model,'fecha');

                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'ChequeDeTerceros[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                    'model' => $model,
                    'attribute' => 'fecha',
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Hacer click en el campo y seleccionar la fecha',
                        'tabindex' => 4
                    ),
                ));

                ?>
            </div>
            <div class="span2" style="width: 9%">
                <?php echo $form->textFieldRow($model,'importe',array('class'=>'span12','tabindex' => 5)); ?>
            </div>
            <div class="span1">
                <?php
                echo $form->labelEx($model,'propio');
                echo $form->dropDownList($model,'propio',ChequeDeTerceros::$aSiNo,array('class'=>'span12','tabindex' => 6)); ?>
            </div>


            <?php
            if ($model->id_pagoCliente != null){
                echo $form->hiddenField($model,'id_cliente');
                echo $form->hiddenField($model,'id_pagoCliente');
            }else{
                echo '</div>
                      <div class="row-fluid">
                        <div class="span3">';
                echo $form->labelEx($model,'id_cliente');
                echo $form->dropDownList($model,'id_cliente',CHtml::listData(Cliente::model()->findAll(),'id','Title'),array('tabindex' => 7));
                echo '</div>';
            }

            ?>

            <div class="span1">
                <?php
                if($bEditable) {
                    $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType' => 'submit',
                        'type' => 'primary',
                        'label' => 'Guardar',
                        'htmlOptions' => array(
                            'style' => 'margin-top:25px',
                        )
                    ));
                }
                ?>
            </div>
        </div>
    </div>

	<?php $this->endWidget(); ?>
</div>

<?php 

if(sizeof($aPagoConCheque->searchWP()->getData()) <= 0){
    try{
        /*
        $aPagoConCheque = new VPagosConChequeNulo('search');
        $aPagoConCheque->unsetAttributes();
        $aPagoConCheque->id = -1;
        */
        $aPagoConCheque = new PagoConCheque('search');
        $aPagoConCheque->unsetAttributes();
        $aPagoConCheque->id = -1;
    }catch(Exception $e){
        die($e->getMessage());
    }
    
}else{
    //ComponentesComunes::print_array($aPagoConCheque->searchWP()->getData());
}



$this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago-con-cheque-grid',
'dataProvider'=> $aPagoConCheque->searchWP(),
'template' => '{items}',
//'rowCssClassExpression' => '($data->id == -1)? "rowHidden" : "" ', 
'rowCssClassExpression' => '($data->idPagodeCliente != '.$idPagodeCliente.')? "rowHidden" : "" ', 
'afterAjaxUpdate'=>'js:function(id, data){
    actualizaPago(); 
    
    return true;
}',
'htmlOptions' => array(
    'style' => 'padding-top: 0px;',
),
'columns'=>array(

        array(
            'name' => 'oChequedeTercero.nroCheque',
			//'value' => '',
			'htmlOptions' => array('style' => ''),

		),
        array(
            'name' => 'oChequedeTercero.fecha',
            'value' => '($data->oChequedeTercero != null)? ComponentesComunes::fechaFormateada($data->oChequedeTercero->fecha) : ""',
            'htmlOptions' => array('style' => 'width:90px;'),
        ),
        array(
            'name' => 'oChequedeTercero.id_banco',
            'value' => '($data->oChequedeTercero != null && $data->oChequedeTercero->oBanco != null)? $data->oChequedeTercero->oBanco->nombre : "*"',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'oChequedeTercero.id_sucursal',
            'value' => '($data->oChequedeTercero != null && $data->oChequedeTercero->oSucursal != null)? $data->oChequedeTercero->oSucursal->nombre : $data->id',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'oChequedeTercero.propio',
            'value' => '($data->oChequedeTercero != null && $data->oChequedeTercero->propio ==1)? "Si":"No"',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'name' => 'oChequedeTercero.monto',
            'header' => 'Monto Del Cheque',
            'value' =>  '($data->oChequedeTercero != null)? "$".number_format($data->oChequedeTercero->importe,2,".","") : ""',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'Total Monto cheques $<span id="totalMontoCheques">'.(($aPagoConCheque->oPagodeCliente != null)?$aPagoConCheque->oPagodeCliente->getMontoCheque() : 0).'</span>',
            'footerHtmlOptions' => array(
                'style' => 'text-align:right;',
                //'id' => '',
            ),
        ),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'monto',
            'header' => 'Monto Pagado',
            'value' =>  '.number_format($data->monto,2,".","")',

            'editable' => array(    //editable section
                'params'     => array('YII_CSRF_TOKEN' => Yii::app()->request->csrfToken), //params for all fields
                'url'        => $this->createUrl('PagoConCheque/SaveFieldAsync'),
                'apply'      => $bEditable,
                'placement'  => 'right',
                'mode'      => 'inline' ,
                //'liveTarget' => 'pago-con-cheque-grid',
                'display' => 'js: function(value, sourceData) {
                                    value = value.replace("$",""); //
                                    if(!isNaN(value) > 0){
                                        $(this).html("$ "+parseFloat(value).toFixed(2));
                                    }else{
                                        console.log("Value is NaN:"+value);
                                        $(this).html("$ 0.00");
                                    }								
                                }',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                     setTimeout(function() {
                                        editable.input.$input.select();
                                      },0);
                                }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                    try{
                                        $.fn.yiiGridView.update(\'pago-con-cheque-grid\');
                                    }catch(e){
                                        console.log(e);
                                    };
                                 }',
            ),
            'htmlOptions' => array('style' => 'text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
            'footer' => 'Total de pagos en cheques $<span id="totalCheques">'.(($aPagoConCheque->oPagodeCliente != null)?$aPagoConCheque->oPagodeCliente->getImporteCheque():0).'</span>',
            'footerHtmlOptions' => array(
                'style' => 'text-align:right;',
                //'id' => 'totalCheques',
            ),

        ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
				'buttons'=>array(
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("PagoConCheque/deleteAll", array("id"=>$data->id))',
                        'visible' => '('.$bEditable.')',
                        'click'=>'function(e){
                                    e.preventDefault();
                                    e.stopImmediatePropagation(); 
                                    if (confirm("Desea borrar el cheque?")){
                                        descartarPagoConCheque($(this).attr("href"));
                                        return false;        
                                    }else{
                                        return false;                                
                                    }
                            }',
					),
				),
		),
	),
)); 

?>
