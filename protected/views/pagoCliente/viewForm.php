
<?php
/* @var $model PagoCliente */
//$isAjaxMode = false;
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScript('actualizaMontoASaldar',"
    var comprobantesSeleccionados;

    function actualizaPago(){
       console.log('actualizaPago()'); 
       let totalASaldar = 0;
       if($('#totalASaldar').length > 0){
            var totalDocumento = parseFloat($('#totalASaldar').html().replace('$',''),2);
            
            var totalEfectivo = parseFloat($('#pago-al-contado-grid #totalEfectivo').html(),2);
            //var totalCheques = parseFloat($('#totalCheques').html().replace('Total de pagos en cheques $',''),2);
            let totalCheques = parseFloat($('#pago-con-cheque-grid #totalCheques').html().replace('$',''),2);
            totalASaldar = (totalDocumento) - (totalEfectivo) - (totalCheques);            
            
            console.log(totalCheques.toFixed(2));  
       }
       
       $('#modalPagoClientes #montoASaldar').html('Saldo: $'+totalASaldar.toFixed(2));
    }                        
    $('#DescartarPago').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        var datos = '&ajax=';
        var url = $(this).attr('url')+'&ajax=';
        $.ajax({
            url:url,
            data: datos,
            type: 'POST',
            success: function(html){
                console.log(html);  
                cerrarContenedor();
            },
            error: function (x,y,z){
                alert(x);
            }
        });
    }); 
    $('#reloadPago').click(function(){
        try{
            verPago($(this).attr('idPago'));
        }catch(e){
            console.error(e);
        }    
    });


    $('#conciliar_comprobantes').click(function(){
        $('#form_conciliar_documentos').show('slow');
        handlerBotonVincular();
        return true;
        /*
        console.log('#conciliar_comprobantes');
        var datos = 'VDocumentosConSaldo[Id_Cliente]='+".$model->idCliente.";
        var url = $(this).attr('url')+'&ajax=';
        $.ajax({
            url:url,
            data: datos,
            type: 'GET',
            success: function(html){
                html = conciliarForm(html);
                
                $('#form_conciliar_documentos').html(html);
                handlerBotonVincular();
            },
            error: function (x,y,z){
                alert(x);
            }
        });
        */
    });
    function handlerBotonVincular(){
        
        $('#vincular_comprobantes_pagos').click(function(){

            comprobantesSeleccionados = [];
            let url = $(this).attr('url');
            let seleccionados = $('#vdocumentos-con-saldo-grid').selGridView('getAllSelection');
            console.log(seleccionados);
            seleccionados.forEach(function(element){
                let compId = $('[value=\"'+element+'\"]').closest('td').prop('className');
                comprobantesSeleccionados.push(compId.replace('checkbox-column','').trim());
            });
            console.log(comprobantesSeleccionados);
            let data = 'idPago=".$model->id."&comprobantes='+comprobantesSeleccionados;
            $.ajax({
                url: url,
                data: data,
                type: 'POST',
                success:function(html){
                    console.log(html);
                    $('#form_conciliar_documentos').slideToggle();
                    try{
                        $.fn.yiiGridView.update('pago2-documento-cliente-grid');
                    }catch(e){
                        console.log(e);
                    };
                    
                },
                error:function(x,y,z){
                    console.error(x);
                },
            });
        });
    }
    function conciliarForm(html){
        html = '".CHtml::link('Conciliar','#',array('class'=>'search-button btn','id' => 'vincular_comprobantes_pagos','url'=>Yii::app()->createUrl('Pago2DocumentoCliente/SaveAsync'))).' &nbsp; '.
        CHtml::link('Cancelar','#',array('class'=>'search-button btn btn-danger','onCLick' => '$("#form_conciliar_documentos").slideToggle()')).
        "'+html;
        return html;
    }
",CClientScript::POS_END);

$idPagodeCliente = $model->id;

if($isAjaxMode){
    Yii::app()->clientScript->registerScript('cerrarContenedor', "    
    function cerrarContenedor(){
        try{
            $('#DescartarPago').closest('.modal.fade.in').modal('hide');
        }catch(e){
            $('#DescartarPago').closest('.modal.fade.in').find('[data-dismiss=\"modal\"]').click();
        }
    }
",CClientScript::POS_END);
}else{
    Yii::app()->clientScript->registerScript('cerrarContenedor', "
    function cerrarContenedor(){
        try{
            window.location = '".Yii::app()->createUrl('PagoCliente/admin')."';
        }catch(e){
            
        }
    }
    ",CClientScript::POS_END);
}


$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
if ($isAjaxMode){
    //echo '$isAjaxMode$isAjaxMode$isAjaxMode$isAjaxMode$isAjaxMode';
    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        'jquery.yiigridview.js'=>false,
    );
}

?>

<style>
    .detail-view th {
        width: auto;
    }
    .rowHidden{
        display: none;
    }
</style>
<div class="content-fluid">
    <div class="row-fluid">

            <?php
            if ($model->id_factura > 0){
                echo '<div class="span1">';
                if($model->oFactura){
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                            array(
                                'name' => 'Factura',
                                'value' => $model->oFactura->Nro_Comprobante
                            )
                        ),
                    ));    
                }else if($model->oNotaDebito){
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                            array(
                                'name' => 'Nota Debito',
                                'value' => $model->oNotaDebito->Nro_Comprobante
                            )
                        ),
                    ));
    
                }else{
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                            array(
                                'name' => 'Nota Credito',
                                'value' => $model->oNotaCredito->Nro_Comprobante
                            )
                        ),
                    ));
    
                }
                echo '</div>';
            }else if ($model->idNotaEntrega > 0){
                echo '<div class="span1">';
                $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'name' => 'N E',
                            'value' => $model->oNotaEntrega->nroComprobante,
                        )
                    ),
                ));
                echo '</div>';
            }else{
                //echo '<div class="span2">';
                
                //echo '</div>';
            }
            ?>

        <div class="span2">
            <?php
            if ($model->id_factura > 0){
                if($model->oFactura){
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                             array(
                                'type'=>'raw',
                                'name' => 'Total',
                                'value' => '<span id="totalASaldar">$'.number_format(($model->oFactura->Total_Final - $devolucion) ,2,'.','').'</span>',
                            ),
                        ),
                    ));
                    $cs->registerScript('montoASaldar',"
                        $(document).ready(function(){
                            $('#montoASaldar').html('Saldo: $".number_format(($model->oFactura->Total_Final  - $devolucion - $model->monto),2)."');
                        });
                    ");
                }else if($model->oNotaDebito){
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                             array(
                                'type'=>'raw',
                                'name' => 'Total',
                                'value' => '<span id="totalASaldar">$'.number_format(($model->oNotaDebito->Total_Final - $devolucion) ,2,'.','').'</span>',
                            ),
                        ),
                    ));
                    $cs->registerScript('montoASaldar',"
                        $(document).ready(function(){
                            $('#montoASaldar').html('Saldo: $".number_format(($model->oNotaDebito->Total_Final  - $devolucion - $model->monto),2)."');
                        });
                    ");
                }else if($model->oNotaCredito){
                    $this->widget('bootstrap.widgets.TbDetailView',array(
                        'data'=>$model,
                        'attributes'=>array(
                             array(
                                'type'=>'raw',
                                'name' => 'Total',
                                'value' => '<span id="totalASaldar">$'.number_format(($model->oNotaCredito->Total_Final - $devolucion) ,2,'.','').'</span>',
                            ),
                        ),
                    ));
                    $cs->registerScript('montoASaldar',"
                        $(document).ready(function(){
                            $('#montoASaldar').html('Saldo: $".number_format(($model->oNotaCredito->Total_Final  - $devolucion - $model->monto),2)."');
                        });
                    ");
                }

            }else if ($model->oNotaEntrega != null){

                $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'type'=>'raw',
                            'name' => 'Total Nota de Entrega',
                            'value' => '<span id="totalASaldar">$'.number_format(($model->oNotaEntrega->totalFinal - $devolucion),2,'.','').'</span>',
                        ),
                    ),
                ));

                $cs->registerScript('montoASaldar',"
                    $(document).ready(function(){
                        $('#montoASaldar').html('Saldo: $".number_format(($model->oNotaEntrega->totalFinal - $devolucion - $model->monto),2)."');
                    });
                ");

            } else {
                $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        'idNotaEntrega',
                        array(
                            'type'=>'raw',
                            'name' => 'Saldo Cta Cte',
                            'value' => '<span id="totalASaldar">$'.number_format(($model->oCliente->saldoActual),2,'.','').'</span>',
                        ),
                    ),
                ));

                $cs->registerScript('montoASaldar',"
                    $(document).ready(function(){
                        $('#montoASaldar').html('Saldo: $".(number_format($model->oCliente->saldoActual,2))."');
                    });
                ");
            }

            ?>
        </div>
        <div class="span2">

            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'fecha',
                        'value' => ComponentesComunes::fechaFormateada($model->fecha),
                    ),
                    'idCliente',
                ),
            ));
            ?>
        </div>
        <div class="span3">

            <?php
            $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'Cliente',
                        'value' => $model->oCliente->title,
                    ),


                ),
            ));
            ?>
        </div>
        <?php
        if($model->id_factura == null && $model->idNotaEntrega  == null){

            echo '<div class="span3" style="width: 27%">';
            echo CHtml::link('Conciliar Comprobantes','#',array('class'=>'search-button btn','id' => 'conciliar_comprobantes','url'=>Yii::app()->createUrl('VDocumentosConSaldo/GridAsync')));
            echo CHtml::label('observacion','observacion');
            $this->widget('editable.EditableField', array(
                'type'      => 'textarea',
                //'mode'    => ($is_modal)? 'inline' : 'popup',
                'mode'      => 'inline' ,
                'model'     => $model,
                'attribute' => 'observacion',
                //'apply'      => '$model->estado < Comprobante::iCerrada', //NO SE PUEDEN MODIFICAR FACTURAS CERRADAS
                'url'       => $this->createUrl('PagoCliente/SaveFieldAsync'),
            ));
            echo '</div>';

        }
        ?>
        <div class="span3" style="">
            <?php
            if($bEditable){
            ?>
            <span class="btn btn-danger" url="<?=Yii::app()->createUrl('pagoCliente/delete',array('id'=>$model->id))?>" id="DescartarPago">Descartar Pago</span>
            <?php 
            }else{
                $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'name' => 'Observacion',
                            'value' => $model->observacion,
                        ),
                    ),
                ));
            }
            ?>
        </div>
        <?php
        if($model->id_factura != null || $model->idNotaEntrega  != null){
        ?>
        <div class="span1" style="cursor: pointer;">
            Actualizar <i class="icon-repeat" idPago="<?=$model->id?>" id="reloadPago"></i>
        </div>
        <?php }?>
    </div>
</div>
<div style="display: none;" id="form_conciliar_documentos">

<?php 

echo CHtml::link('Conciliar','#',array('class'=>'search-button btn','id' => 'vincular_comprobantes_pagos','url'=>Yii::app()->createUrl('Pago2DocumentoCliente/SaveAsync'))).' &nbsp; '.
        CHtml::link('Cancelar','#',array('class'=>'search-button btn btn-danger','onCLick' => '$("#form_conciliar_documentos").slideToggle()'));

$modelVDocumentosConSaldo=new VDocumentosConSaldo('search');
$modelVDocumentosConSaldo->unsetAttributes();  // clear any default values
$modelVDocumentosConSaldo->Id_Cliente = $model->idCliente;

$this->widget('ext.selgridview.BootSelGridView',array(
'id'=>'vdocumentos-con-saldo-grid',
'template' => '{items}',
'selectableRows' => 2,
'dataProvider'=>$modelVDocumentosConSaldo->searchWP(),
'ajaxUrl'=>'VDocumentosConSaldo/GridAsync',
'enablePagination' => false,
'columns'=>array(
        array(
            'class' => 'CCheckBoxColumn',
            'cssClassExpression' => '$data->tipo."_".$data->id',
        ),
		'tipo',
        'nroComprobante',
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        
        array(
            'name' => 'total',
            'value' => 'number_format($data->total,2,",",".")',            
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'saldo',
            'value' => 'number_format($data->saldo,2,",",".")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
		

        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
	),
)); ?>
</div>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span9 bordeRedondo" style="display: inline-table;" >
            <h4>Cheques</h4>
            <?php
            echo $this->renderPartial('_formFilaCH', array(
                    'model'=>$oChequeTerceros,
                    'aPagoConCheque' => $aPagoConCheque,
                    'bEditable' => $bEditable,
                    'isAjaxMode'=> $isAjaxMode
                )
            );
            ?>
        </div>
        <div class="span3 bordeRedondo" style="display: inline-table;">
            <h4>Efectivo</h4>
            <?php
            echo $this->renderPartial('_formFilaPE', array(
                    'model'=>$oPagoAlContado,
                    'aPagoAlContado' => $aPagoAlContado,
                    'bEditable' => $bEditable,
                )
            );

            ?>
        </div>
    </div>
</div>
<h4>Documentos Conciliados.</h4>
<?php 
$aPago2DocumentoCliente = new Pago2DocumentoCliente('search');
$aPago2DocumentoCliente->unsetAttributes();
$aPago2DocumentoCliente->idPago = $model->id;

if(sizeof($aPago2DocumentoCliente->search()->getData()) <= 0){
    try{
        $aPago2DocumentoCliente = new Pago2DocumentoCliente('search');
        $aPago2DocumentoCliente->unsetAttributes();
        $aPago2DocumentoCliente->id = 9;
    }catch(Exception $e){
        die($e->getMessage());
    }
    
}

$grid_compr = $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago2-documento-cliente-grid',
'rowCssClassExpression' => '($data->idPago != '.$idPagodeCliente.')? "rowHidden" : "" ', 
'afterAjaxUpdate'=>'js:function(id, data){console.log("afterupdate 1")}',
'dataProvider'=>$aPago2DocumentoCliente->search(),
'htmlOptions' => array(
    'style' => 'padding-top: 0px;',
),
'columns'=>array(
        array(
            'header' => 'Comprobante',
            'value' => '($data->oComprobante != null)? $data->oComprobante->FullTitle :"-"',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'header' => 'NotaEntrega',
            'value' => '($data->oNotaEntrega != null)? $data->oNotaEntrega->FullTitle :"-"',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'class' => 'editable.EditableColumn',
            'name'  => 'montoSaldado',             
            'value' => 'number_format($data->montoSaldado,"2",".","")',
            'editable' => array(    //editable section
                //'apply'      => '$data->pagado != 1 && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC && $data->tipoDoc != CargaDocumentos::ND',
                'mode'  => 'inline',
                'url'        => $this->createUrl('Pago2DocumentoCliente/SaveFieldAsync'),
                'placement'  => 'right',
                'display' => 'js: function(value, sourceData) {
                                            value = value.replace("$",""); 
                                            if(!isNaN(value) > 0){
                                                let n = parseFloat(value).toFixed(2);
                                                $(this).html(Number(n).toLocaleString("es"));
                                            }else{
                                                console.log("Value is NaN:"+value);
                                                $(this).html(" 0.00");
                                            }    
                                    }',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                         setTimeout(function() {
                                            editable.input.$input.select();
                                          },0);
                                    }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                        try{
                                            $.fn.yiiGridView.update("pago2-documento-cliente-grid");
                                        }catch(e){
                                            console.log(e);
                                        };
                                     }',
            ),
			'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
            /*
            'footer' => 'Total Cobrado: $'.number_format(($oEntregaDoc->getTotalEfectivo() + $oEntregaDoc->TotalCheques),2,'.',''),
            'footerHtmlOptions' => array(
                'colspan' => 2,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )
            */
		),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{delete}',
				'buttons'=>array(
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("Pago2DocumentoCliente/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); 

?>
<h3 id="montoASaldar"></h3>