<?php
/* @var $this PagoClienteController */
/* @var $aPagoAlContado PagoAlContado */
$cs = Yii::app()->getClientScript();
$cs->registerScript('formFilaCH',"
	function descartarPagoFT(url){
		var datos = '&ajax=';
		var url = url+'&ajax=';
		$.ajax({
			url:url,
			data: datos,
			type: 'POST',
			success: function(html){
				$.fn.yiiGridView.update('pago-al-contado-grid');
				return false;
			},
			error: function (x,y,z){
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				alert($(errorDiv).parent().next());
			}
		});
	}
", CClientScript::POS_END);

?>
<div id="nuevoPagoAlContado" style="">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'pago-al-contado-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('PagoAlContado/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'pago-al-contado-grid',
			'idMas' => 'masPagoAlContado',
			'idDivNuevo' => 'nuevoPagoAlContado',
            'style' => 'margin-bottom:0px;'.(($bEditable)? '' : 'display:none;'),
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorPagoAlContado"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okPagoAlContado">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
        <div class="span6">
			<?php echo $form->hiddenField($model,'idPagodeCliente',array('class'=>'span12')); ?>
            <?php echo $form->textFieldRow($model,'monto',array('class'=>'span12')); ?>
		</div>
        <div class="span6">
            <?php
            if($bEditable) {
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'type' => 'primary',
                    'label' => 'Guardar',
                    'htmlOptions' => array(
                        'style' => 'margin-top:25px',
                    )
                ));
            }
            ?>
        </div>
	
	</div>
</div>	


	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago-al-contado-grid',
'dataProvider'=>$aPagoAlContado->search(),
'htmlOptions' => array(
    'style' => 'padding-top: 0px;',
),
'template' => '{items}',
'afterAjaxUpdate'=>'function(id, data){actualizaPago()}',
'columns'=>array(
		array(
            'name' => 'monto',
            'value' =>  '"$".number_format($data->monto,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'Total Efectivo $<span id="totalEfectivo">'.$aPagoAlContado->oPagodeCliente->getMontoEfectivo().'</span>',
            'footerHtmlOptions' => array(
                'style' => 'text-align:right;',
                //'id' => '',
            ),
        ),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => ' {delete}',
				'buttons'=>array(

					'delete' => array(
	                	'label' => 'Borrar Item',
                        'visible' => '('.$bEditable.')',
						'url'=>'Yii::app()->controller->createUrl("PagoAlContado/delete", array("id"=>$data->id))',
						'click'=>'function(e){
							e.preventDefault();
							e.stopImmediatePropagation(); 
							if (confirm("Desea borrar el registro?")){
								descartarPagoFT($(this).attr("href"));
								return false;
							}else{
								return false;
							}
						}',
					),
				),
		),
	),
)); ?>
