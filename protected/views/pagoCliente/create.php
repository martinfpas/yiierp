<?php
/* @var $this PagoClienteController */
/* @var $model PagoCliente */

$this->breadcrumbs=array(
    PagoCliente::model()->getAttributeLabel('models')=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>PagoCliente::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
Yii::app()->clientScript->registerScript('tooltip', "
    $(document).ready(function(){
        var modoEdicion = false;
		// PARA PONER LAS AYUDAS
		try{
			$('label').tooltip();
			$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		// ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		$('#idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                        
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		
		$('#idCliente').change(function (ev) {
		    getPago($('#idCliente').val());
		});
		
		
    });
        
	function getPago(idCliente){
	    var datos = 'idCliente='+idCliente;
	    var htmlAjax = '';
        var promiseGetFactura =  $.ajax({
            url: '".$this->createUrl('PagoCliente/NewBlank')."',
            type: 'Get',
            data: datos,
            success: function (html) {                
                window.location = '".Yii::app()->createUrl('PagoCliente/update')."&id='+html;
            },
            error: function (x,y,z){
                //TODO: MANEJAR EL ERROR                
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());                
            }
        });
        	    
	}
	
",CClientScript::POS_READY);

?>

<h1>Nuevo <?=PagoCliente::model()->getAttributeLabel('model')?></h1>

<div class="alert alert-block alert-error" style="display:none;" id="errorCLiente"></div>
<div class="alert alert-block alert-success" style="display:none;" id="okCliente">Datos Guardados Correctamente !</div>
<?php
echo CHtml::label('Id Cliente','idCliente');
echo CHtml::textField('idCliente','',array('title' => 'Presionar "+" para abrir el buscador'));
?>
<div id="fichaCliente"></div>
<div id="cuerpo" style="display: none"></div>
<?php
$this->widget('BuscaCliente', array('js_comp'=>'#idCliente','is_modal' => true));
?>
