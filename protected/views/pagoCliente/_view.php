<?php
/* @var $this PagoClienteController */
/* @var $data PagoCliente */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_factura')); ?>:</b>
	<?php echo CHtml::encode($data->id_factura); ?>
	<br />

    <b><?php echo CHtml::encode($data->getAttributeLabel('idNotaEntrega')); ?>:</b>
    <?php echo CHtml::encode($data->idNotaEntrega); ?>
    <br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCliente')); ?>:</b>
	<?php echo CHtml::encode($data->idCliente); ?>
	<br />


</div>