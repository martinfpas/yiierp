<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 27/7/2019
 * Time: 10:19
 */
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->scriptMap=array(
    'jquery.ba-bbq.js'=>false,
    'jquery.js'=>false,
    'jquery.yiigridview.js'=>false,
);

//if($is_modal){
    Yii::app()->clientScript->registerScript('cerrarContenedorMulti', "    
        function cerrarContenedor(){
            try{
                $('#DescartarPago').closest('.modal.fade.in').modal('hide');
            }catch(e){
                $('#DescartarPago').closest('.modal.fade.in').find('[data-dismiss=\"modal\"]').click();
            }
        }
    ",CClientScript::POS_READY);
/*
}else {
    Yii::app()->clientScript->registerScript('cerrarContenedor', "
        $(document).ready(function(){
            console.log('');
            //bindFacturaHandlers()
        });
        function cerrarContenedor(){
            window.location = '".Yii::app()->createUrl('pagoCliente/admin')."';
        }
    ");
}
*/

$cs->registerScript('actualizaMontoASaldarMultipagos',"
    $(document).ready(function(){
        handlersCH();
        handlers();
    });
    
    function updateMultipago(){
        var datos = 'idPagoRel=".$model->id."';
        $.ajax({
            url: '".Yii::app()->createUrl('PagoCliente/JsonMulti')."',
            data:datos,
            type: 'GET',
            //TODO:TERMINAR
            success:function(html){
                let data = JSON.parse(html);
                data.saldo = (data.saldo).replace('.', '');
                data.saldo = (data.saldo).replace(',', '.');
                let saldo = (isNaN(parseFloat(data.saldo).toFixed(2)))? 0 : parseFloat(data.saldo).toFixed(2); 
                
                $('#saldo').html(saldo);
                if(saldo <= 0){
                    $('#AplicarPago').removeAttr('disabled');
                }else{
                    $('#AplicarPago').attr('disabled','disabled');
                }
            }
        });
    }
    
    function actualizaPago(){
       console.log('actualizaPago() :: multipago'); 
       let totalASaldar = 0;
       if($('#totalASaldar').length > 0){
            var totalDocumentos = parseFloat($('#totalASaldar').html().replace('$',''),2);
            var datos = 'idPagoRelacionado='+$('#ChequeDeTerceros_id_pagoCliente').val();
            $.ajax({
                url: '".Yii::app()->createUrl('PagoCliente/SumPagoRelacionado')."',
                data: datos,
                success: function(html){
                    updateMultipago();                   
                },
                error: function (x,y,z){
                    
                }
            });            
                        
       }
       handlersCH();
       handlers();
    }
    
    function handlers(){
       $('#DescartarPago').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            var datos = '&ajax=';
            var url = $(this).attr('url')+'&ajax=';
            $.ajax({
                url:url,
                data: datos,
                type: 'POST',
                success: function(html){
                    console.log(html);  
                    cerrarContenedor();
                },
                error: function (x,y,z){
                    
                }
            });
        });
    }   
    $('#reloadPagoM').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        console.log('#reloadPagoM');
        try{
            actualizaPago();
        }catch(e){
            console.error(e);
        }    
    });
    
    
    
",CClientScript::POS_READY);
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
?>
<style>
    #pago-al-contado-grid{
        padding-top: 0px;
    }
</style>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span9">
            <h4>Total Comprobantes: $<span id="totalASaldar"><?=$total?></span></h4>
        </div>
        <div class="span2" style="text-align: right">
            <?php
            if($bEditable){
                ?>
                <span class="btn btn-danger" url="<?=Yii::app()->createUrl('pagoCliente/delete',array('id'=>$model->id))?>" id="DescartarPago">Descartar Pago</span>
            <?php }?>
        </div>
        <div class="span1" style="cursor: pointer;" id="reloadPagoM">
            Actualizar <i class="icon-repeat" idPago="<?=$model->id?>" ></i>
        </div>
    </div>
</div>


<div class="content-fluid">
    <div class="row-fluid">
        <div class="span9 bordeRedondo" style="display: inline-table;" >
            <h4>Cheques</h4>
            <?php

            echo $this->renderPartial('_formFilaCHMulti', array(
                    'model'=>$oChequeTerceros,
                    'aPagoConCheque' => $aPagoConCheque,
                    'saldo' => $saldo,
                    'bEditable' => $bEditable,
                )
            );
            ?>
        </div>
        <div class="span3 bordeRedondo" style="display: inline-table;">
            <h4>Efectivo</h4>
            <?php
            echo $this->renderPartial('_formFilaPEMulti', array(
                    'model'=>$oPagoAlContado,
                    'aPagoAlContado' => $aPagoAlContado,
                    'bEditable' => $bEditable,
                )
            );

            ?>
        </div>
    </div>
</div>

<?php
$this->widget('ext.selgridview.BootSelGridView',array(

    'id'=>'carga-documentos-grid-multiple',
    'dataProvider'=>$aCargaDocumentos->searchIn($aIdCargaDocumentos),
    'template' => '{items}',
    'selectableRows' => 2,
    'afterAjaxUpdate'=>'js:function(id, data){console.log("carga-documentos-grid-multiple"); updateMultipago(); return true;}',
    'htmlOptions' => array(
        'style' => 'padding-top: 0px;'
    ),
    'columns'=>array(
        'id',
        array(
            'name' => 'tipoDoc',
            'header' => 'Tipo',
        ),
        array(
            'name' => 'Nro',
            //'value' => '($data->$oComprobante != null)? $data->$oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante',
            'value' => '($data->tipoDoc != CargaDocumentos::NE)? $data->oComprobante->Nro_Comprobante : $data->oNotaEntrega->nroComprobante',
            //'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'Clase',
            'value' => '($data->oComprobante != null)? $data->oComprobante->clase : $data->oNotaEntrega->clase',
            //'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'razonSocial',
            'value' => '$data->Cliente->razonSocial',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'Recibida',
            'header' => 'Ent.',
            'value' => '($data->tipoDoc != CargaDocumentos::NE)? CargaDocumentos::$aSiNo[$data->oComprobante->Recibida] : CargaDocumentos::$aSiNo[$data->oNotaEntrega->recibida]',
            //'htmlOptions' => array('style' => ''),
            'editable' => array(    //editable section
                'apply'      => $bEditable,
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'type'   => 'select',
                'mode' => 'inline',
                'source' => Editable::source(CargaDocumentos::$aSiNo),
            )
        ),

        array(
            'name' => 'pagado',
            'value' => 'CargaDocumentos::$aSiNo[$data->pagado]',
            //'htmlOptions' => array('style' => ''),
        ),



        array(

            'name' => 'totalFinal',
            'value' => '"$".number_format($data->totalFinal,"2",",","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
            'footer' => 'Total Cobrado: $'.number_format(($fTotalEfectivo + $fTotalCheques),2,'.',''),
            'footerHtmlOptions' => array(
                'colspan' => 2,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )
        ),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'devolucion',
            'value' => '"$".number_format($data->devolucion,"2",".","")',
            'editable' => array(    //editable section
                'apply'      => '$data->pagado != 1 && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC && $data->tipoDoc != CargaDocumentos::ND and '.$bEditable,
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'placement'  => 'right',
                'mode' => 'inline',
                'display' => 'js: function(value, sourceData) {
                                value = value.replace("$",""); //
                                if(!isNaN(value) > 0){
                                    $(this).html("$ "+parseFloat(value).toFixed(2));
                                }else{
                                    console.log("Value is NaN:"+value);
                                    $(this).html("$ 0.00");
                                }

							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update("carga-documentos-grid-multiple");
                                }catch(e){
                                    console.log(e);
                                };
                             }',
            ),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),

        array(
            'name' => 'Monto pagado',
            'value' => '($data->oPagoCliente != null)? "$".number_format($data->oPagoCliente->monto,"2",",","") : "$0.00"',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array(
                'style' => 'text-align:right;',
            ),
        ),
        /*
        array(
            'name' => 'Ver Pago',
            'value' => '($data->oPagoCliente != null)? $data->oPagoCliente->id : "-"',
            'htmlOptions' => array('style' => 'text-align:right;'),

        ),
        */

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'efectivo',
            'value' => '"$".number_format($data->efectivo,"2",".","")',
            'editable' => array(    //editable section
                //'apply'      => '$data->pagado != 1 && $data->esEditable() && $data->tipoDoc != CargaDocumentos::NC',
                'apply'      => $bEditable,
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'mode' => 'inline',
                'placement'  => 'right',
                'display' => 'js: function(value, sourceData) {
                                value = value.replace("$",""); //
                                if(!isNaN(value) > 0){
                                    $(this).html("$ "+parseFloat(value).toFixed(2));
                                }else{
                                    console.log("Value is NaN:"+value);
                                    $(this).html("$ 0.00");
                                }								
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update(\'carga-documentos-grid-multiple\');
                                }catch(e){
                                    console.log(e);
                                };
                             }',
            ),
            'htmlOptions' => array('style' => 'width: 75px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footer' => 'Total Cheques: $'.number_format($fTotalCheques,2,'.',''),
            'footerHtmlOptions' => array(
                'colspan' => 3,
                'style' => 'font-style: normal;font-weight:bold;font-size: larger;'
            )

        ),
        array(
            //'class' => 'editable.EditableColumn',
            'name' => 'cheque',
            'value' => '"$".number_format($data->cheque,"2",",","")',
            'htmlOptions' => array('style' => 'width: 70px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),

        array(
            'class' => 'editable.EditableColumn',
            'name' => 'notaCredito',
            'value' => '"$".number_format((($data->tipoDoc != CargaDocumentos::NC)? $data->notaCredito : $data->totalFinal),"2",".","")',
            'editable' => array(    //editable section
                'apply'      => $bEditable,
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'mode' => 'inline',
                'placement'  => 'right',
                'display' => 'js: function(value, sourceData) {
                                value = value.replace("$",""); //
                                if(!isNaN(value) > 0){
                                    $(this).html("$ "+parseFloat(value).toFixed(2));
                                }else{
                                    console.log("Value is NaN:"+value);
                                    $(this).html("$ 0.00");
                                }								
							}',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update(\'carga-documentos-grid-multiple\');
                                }catch(e){
                                    console.log(e);
                                };
                             }',
            ),
            'htmlOptions' => array('style' => 'width: 80px;text-align:right'),
            'headerHtmlOptions' => array(
                'style' => 'width:70px;text-align:right;',
            ),
        ),


        array(
            'class' => 'editable.EditableColumn',
            'name' => 'comision',
            'value' => 'CargaDocumentos::$aSiNo[$data->comision]',
            'editable' => array(
                'apply'      => '$data->pagado != 1 && $data->esEditable() and '.$bEditable,
                'type'      => 'select',
                'url'        => $this->createUrl('CargaDocumentos/SaveFieldAsync'),
                'source'    => Editable::source(CargaDocumentos::$aComision),
                'mode' => 'inline',
                // ACTUALIZO LA GRILLA
                'success' => 'js: function(response, newValue) {
                                try{
                                    $.fn.yiiGridView.update("carga-documentos-grid-multiple");
                                }catch(e){
                                    console.log(e);
                                };
                             }',
            ),
            'header' => 'com',
            'htmlOptions' => array('style' => 'width:30px;'),
            'footerHtmlOptions' => array(
                'style' => 'display:none;'
            )
        ),

        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */

    ),
)); ?>

<h3>Saldo $<span id="saldo"><?=$saldo?></span></h3>
