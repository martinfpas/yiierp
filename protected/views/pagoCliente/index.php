
/* @var $this PagoClienteController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Pago Clientes',
);

$this->menu=array(
	array('label'=>'Nuevo PagoCliente', 'url'=>array('create')),
	array('label'=>'Gestion de PagoCliente', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>