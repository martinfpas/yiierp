<?php
/* @var $this PagoClienteController */
/* @var $model PagoCliente */
/* @var $data PagoCliente */

$this->breadcrumbs=array(
	'Pago Clientes'=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo PagoCliente', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#pago-cliente-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Pago Clientes</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago-cliente-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'Fact_Nro_Comprobante',
            'header' => 'Factura',
            'value' => '($data->oFactura != null)? (($data->oFactura->Nro_Comprobante != "")? $data->oFactura->FullTitle : "SIN CAE" ) : ""',
			'htmlOptions' => array('style' => 'width:105px;'),
		),

        array(
            'name' => 'nroComprobante',
            'header' => 'N.Entrega',
            'value' => '($data->oNotaEntrega != null)? $data->oNotaEntrega->nroComprobante : ""', //$data->oNotaEntrega->nroComprobante
			'htmlOptions' => array('style' => 'width:80px;'),
		),

        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'width:75px;text-align:right;'), // ESTILO VALORES
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),  // ESTILO CABECERA
        ),

        array(
            'name' => 'clienteNombre',
            'header' => 'idCliente',
			'value' => '$data->oCliente->title',
			'htmlOptions' => array(),
            'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','title'),
		),
        array(
            'name' => 'monto',
			'value' => '"$".number_format($data->monto,2,".","")' ,
            'htmlOptions' => array('style' => 'text-align:right;width:75px;'),
		),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} | {delete}'
		),
	),
)); ?>
