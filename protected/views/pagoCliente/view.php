<?php
/*  @var $this PagoClienteController */
/*  @var $model PagoCliente */


$this->breadcrumbs=array(
    PagoCliente::model()->getAttributeLabel('models') => array('admin'),
	$model->id,
);


$this->menu=array(
	array('label'=>'Gestion de '.PagoCliente::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h1>Ver PagoCliente #<?php echo $model->id; ?></h1>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    'id_factura',
                    'idNotaEntrega',
                    array(
                        'name' => 'fecha',
                        'value' => ComponentesComunes::fechaFormateada($model->fecha),
                    ),

                ),
            ));

            ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(

                    array(
                        'name' => 'Cliente',
                        'value' => $model->oCliente->title,
                    ),
                    array(
                        'name' => 'Total',
                        'value' => '$'.number_format($model->monto,2,'.',''),
                        'htmlOptions' => array(
                            'id' => 'totalPago',
                        ),
                    ),
                    'observacion',

                ),
            ));

            ?>
        </div>
    </div>
</div>


<h3>Pagos En Efectivo</h3>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'pago-al-contado-grid',
    'dataProvider'=>$aPagoAlContado->search(),
    'htmlOptions' => array(
        'style' => 'padding-top: 0px;',
    ),
    'columns'=>array(
        'id',
        array(
            'name' => 'monto',
            'value' =>  '"$".number_format($data->monto,2,".","")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        /*
        array(

            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} | {delete}',
            'buttons'=>array(
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("PagoAlContado/update", array("id"=>$data->id))',
                    'options'=>array('target'=>'_blank'),
                ),
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("PagoAlContado/delete", array("id"=>$data->id))',
                ),
            ),
        ),
        */
    ),
)); ?>


<h3>Pagos Con Cheque</h3>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'pago-con-cheque-grid',
    'dataProvider'=>$aPagoConCheque->searchWP(),
    'template' => '{items}',
    'htmlOptions' => array(
        'style' => 'padding-top: 0px;',
    ),
    'columns'=>array(

        array(
            'name' => 'oChequedeTercero.nroCheque',
            //'value' => '',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'oChequedeTercero.fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->oChequedeTercero->fecha)',
            'htmlOptions' => array('style' => 'width:85px;'),
        ),
        array(
            'name' => 'oChequedeTercero.id_banco',
            'value' => '($data->oChequedeTercero->oSucursal != null && $data->oChequedeTercero->oSucursal->oBanco != null)? $data->oChequedeTercero->oSucursal->oBanco->nombre : "*"',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'oChequedeTercero.id_sucursal',
            'value' => '($data->oChequedeTercero->oSucursal != null)? $data->oChequedeTercero->oSucursal->nombre : ""',
            'htmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'oChequedeTercero.propio',
            'value' => '($data->oChequedeTercero->propio ==1)? "Si":"No"',
            'htmlOptions' => array('style' => 'width:50px;'),
        ),
        array(
            'name' => 'oChequedeTercero.monto',
            'value' =>  '"$".number_format($data->oChequedeTercero->importe,2,".","")',
            'header' => 'Monto del Cheque',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'monto',
            'value' =>  '"$".number_format($data->monto,2,".","")',
            'header' => 'Monto Aplicado',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        /*
        array(
            'name' => ,
            'value' => ,
            'htmlOptions' => array('style' => ''),
        ),
        */
        /*
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} | {delete}',
            'buttons'=>array(
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("ChequeDeTerceros/update", array("id"=>$data->idChequedeTercero))',
                    'options'=>array('target'=>'_blank'),
                ),
                'delete' => array(
                    'label' => 'Borrar Item',
                    'url'=>'Yii::app()->controller->createUrl("PagoConCheque/delete", array("id"=>$data->id))',
                ),
            ),
        ),
        */
    ),
)); ?>
