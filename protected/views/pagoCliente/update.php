<?php
$this->breadcrumbs=array(
	'Pago Clientes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo PagoCliente', 'url'=>array('create')),
	array('label'=>'Ver PagoCliente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de PagoCliente', 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=PagoCliente::model()->getAttributeLabel('model')?> #<?php echo $model->id; ?></h3>
    <!-- LO QUE SIGUE ES SOLO PARA MANIPULAR EL WIDGET -->
    <input type="hidden" id="idPagoWid" value="<?php echo $model->id; ?>" />

<?php
$this->widget('PagoClienteWid', array('js_comp'=>'#PagoCLienteIdModal','is_modal' => false));
?>