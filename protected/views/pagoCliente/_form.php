<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pago-cliente-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorPagoCliente"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okPagoCliente">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">

            <?php
            echo $form->labelEx($model,'id_factura');
            echo $form->dropDownList($model,'id_factura',CHtml::listData(Factura::model()->findAll(),'id','Nro_Comprobante'),array());
            ?>
		</div>
        <div class="span3">

            <?php
            echo $form->labelEx($model,'idNotaEntrega');
            echo $form->dropDownList($model,'idNotaEntrega',CHtml::listData(NotaEntrega::model()->findAll(),'id','Nro_Comprobante'),array());
            ?>
        </div>
        <div class="span3">

            <?php
            echo $form->labelEx($model,'idCliente');
            echo $form->dropDownList($model,'idCliente',CHtml::listData(Cliente::model()->findAll(),'id','Title'),array());
            ?>
        </div>
		<div class="span3">
            <?php
            echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                'name'=>'NotaCredito[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                'language'=>'es',
                'options'=>array(
                    'showAnim'=>'fold',
                    'monthRange'=>'-2:+2',
                    'dateFormat'=>'dd-mm-yy',
                    'changeYear' => true,
                    'changeMonth' => true,
                ),
                'htmlOptions'=>array(
                    'style'=>'height:25px;',
                    'class'=>'span12',
                    'title'=>'Hacer click en el campo y seleccionar la fecha',
                    //'tabindex' => 5
                ),
            ));

            ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'idCliente',array('class'=>'span12')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
