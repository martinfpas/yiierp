<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/tabindex.js");
	
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'codigo-postal-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorCodigoPostal"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okCodigoPostal">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'codigoPostal',array('class'=>'span12','maxlength'=>4,'tabindex'=>'1')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nombrelocalidad',array('class'=>'span12','maxlength'=>50,'tabindex'=>'2')); ?>
		</div>
		<div class="span3">
			<?php 
				echo $form->labelEx($model,'id_provincia');
				echo $form->dropDownList($model,'id_provincia',CHtml::listData(Provincia::model()->todas()->findAll(),'id_provincia','IdNombre'),array('empty'=>'--Seleccionar--','class'=>'span12','tabindex'=>'3'));
				
			?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'zp',array('class'=>'span12','maxlength'=>50,'tabindex'=>'2')); ?>
		</div>
		
	</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
