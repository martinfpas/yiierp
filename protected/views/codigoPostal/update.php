<?php
$this->breadcrumbs=array(
	'Codigos Postales'=>array('index'),
	$model->codigoPostal=>array('view','id'=>$model->codigoPostal),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
	array('label'=>'Ver ', 'url'=>array('view', 'id'=>$model->codigoPostal)),
	array('label'=>'Gestion ', 'url'=>array('admin')),
);
?>

	<h1>Modificando Codigo Postal <?php echo $model->codigoPostal; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>