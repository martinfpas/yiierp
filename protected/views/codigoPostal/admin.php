<?php
/* @var $this CodigoPostalController */
/* @var $model CodigoPostal */

$this->breadcrumbs=array(
	'Codigos Postales'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#codigo-postal-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ABM Codigos Postales</h1>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'codigo-postal-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		array(
			'name' => 'codigoPostal',
			'headerHtmlOptions' => array('style' => 'width:100px;'),
		),
		
		'nombrelocalidad',
		array(
			'name' => 'id_provincia',
			'value' => '$data->oProvincia->nombre',
			'htmlOptions' => array('style' => ''),
			'filter' => CHtml::listData(Provincia::model()->todas()->findAll(),'id_provincia','IdNombre'),
		),
		array(
			'name' => 'zp',
			'headerHtmlOptions' => array('style' => 'width:50px;'),
		),
		
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} | {update}',
            'buttons'=>array(
                'view' => array(
                    'label' => 'ver',
                    'url'=>'Yii::app()->controller->createUrl("CodigoPostal/view", array("codigoPostal"=>$data->codigoPostal,"zp"=>$data->zp))',
                    //'options'=>array('target'=>'_blank'),
                ),
                'update' => array(
                    'label' => 'Modificar',
                    'url'=>'Yii::app()->controller->createUrl("CodigoPostal/update", array("codigoPostal"=>$data->codigoPostal,"zp"=>$data->zp))',
                    //'options'=>array('target'=>'_blank'),
                ),
            ),
		),
	),
)); ?>
