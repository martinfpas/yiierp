
/* @var $this CodigoPostalController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Codigo Postals',
);

$this->menu=array(
	array('label'=>'Nuevo CodigoPostal', 'url'=>array('create')),
	array('label'=>'ABM CodigoPostal', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>