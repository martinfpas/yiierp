<?php
/* @var $this CodigoPostalController */
/* @var $model CodigoPostal */

$this->breadcrumbs=array(
	'Codigos Postales'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestionar', 'url'=>array('admin')),
);
?>

<h1>Creando Codigo Postal</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>