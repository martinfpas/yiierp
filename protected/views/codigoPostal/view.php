<?php
$this->breadcrumbs=array(
	'Codigos Postales'=>array('admin'),
	$model->codigoPostal,
);

$this->menu=array(
	array('label'=>'Nuevo', 'url'=>array('create')),
	array('label'=>'Modificar', 'url'=>array('update', 'codigoPostal'=>$model->codigoPostal,'zp'=>$model->zp )),
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>

<h1>Ver Codigo Postal #<?php echo $model->codigoPostal; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'codigoPostal',
		'nombrelocalidad',
		array(
				'name' => 'id_provincia',
				'value' => $model->oProvincia->nombre,
				'htmlOptions' => array('style' => ''),
		),
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
