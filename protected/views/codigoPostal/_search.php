<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'codigoPostal',array('class'=>'span12','maxlength'=>4)); ?>

		<?php echo $form->textFieldRow($model,'nombrelocalidad',array('class'=>'span12','maxlength'=>50)); ?>

		<?php echo $form->textFieldRow($model,'id_provincia',array('class'=>'span12','maxlength'=>1)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
