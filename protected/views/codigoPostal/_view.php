<?php
/* @var $this CodigoPostalController */
/* @var $data CodigoPostal */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('codigoPostal')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->codigoPostal),array('view','id'=>$data->codigoPostal)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombrelocalidad')); ?>:</b>
	<?php echo CHtml::encode($data->nombrelocalidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_provincia')); ?>:</b>
	<?php echo CHtml::encode($data->id_provincia); ?>
	<br />


</div>