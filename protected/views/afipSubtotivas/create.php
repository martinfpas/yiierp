<?php
/* @var $this AfipSubtotivasController */
/* @var $model AfipSubtotivas */

$this->breadcrumbs=array(
	'Afip Subtotivases'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de AfipSubtotivas', 'url'=>array('admin')),
);
?>

<h1>Creando AfipSubtotivas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>