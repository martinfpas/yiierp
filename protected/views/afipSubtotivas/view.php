<?php
$this->breadcrumbs=array(
	'Afip Subtotivases'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo AfipSubtotivas', 'url'=>array('create')),
	array('label'=>'Modificar AfipSubtotivas', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar AfipSubtotivas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de AfipSubtotivas', 'url'=>array('admin')),
);
?>

<h1>Ver AfipSubtotivas #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'codigo',
		'BaseImp',
		'importe',
		'idVoucher',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
