<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'codigo',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'BaseImp',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'importe',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idVoucher',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
