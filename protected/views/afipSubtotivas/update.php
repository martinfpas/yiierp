<?php
$this->breadcrumbs=array(
	'Afip Subtotivases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo AfipSubtotivas', 'url'=>array('create')),
	array('label'=>'Ver AfipSubtotivas', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de AfipSubtotivas', 'url'=>array('admin')),
);
?>

	<h1>Modificando AfipSubtotivas <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>