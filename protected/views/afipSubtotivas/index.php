
/* @var $this AfipSubtotivasController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Afip Subtotivases',
);

$this->menu=array(
	array('label'=>'Nuevo AfipSubtotivas', 'url'=>array('create')),
	array('label'=>'Gestion de AfipSubtotivas', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>