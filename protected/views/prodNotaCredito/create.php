<?php
/* @var $this ProdNotaCreditoController */
/* @var $model ProdNotaCredito */

$this->breadcrumbs=array(
	'Prod Nota Creditos'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ProdNotaCredito', 'url'=>array('admin')),
);
?>

<h1>Creando ProdNotaCredito</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>