<?php
$this->breadcrumbs=array(
	'Prod Nota Creditos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaCredito', 'url'=>array('create')),
	array('label'=>'Modificar ProdNotaCredito', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdNotaCredito', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdNotaCredito', 'url'=>array('admin')),
);
?>

<h1>Ver ProdNotaCredito #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idComprobante',
		'nroItem',
		'idArticuloVta',
		'descripcion',
		'cantidad',
		'precio',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
