<?php
$this->breadcrumbs=array(
	'Prod Nota Creditos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaCredito', 'url'=>array('create')),
	array('label'=>'Ver ProdNotaCredito', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdNotaCredito', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdNotaCredito <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>