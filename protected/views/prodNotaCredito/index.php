
/* @var $this ProdNotaCreditoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Nota Creditos',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaCredito', 'url'=>array('create')),
	array('label'=>'Gestion de ProdNotaCredito', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>