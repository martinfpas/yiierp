
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoCuentaGastos" id="masCuentaGastos" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoCuentaGastos" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'cuenta-gastos-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('CuentaGastos/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'cuenta-gastos-grid',
			'idMas' => 'masCuentaGastos',
			'idDivNuevo' => 'nuevoCuentaGastos',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorCuentaGastos"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okCuentaGastos">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id',array('class'=>'span12','maxlength'=>9)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'denominacion',array('class'=>'span12','maxlength'=>50)); ?>
		</div>
	
</div>
<div class="row-fluid">

        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'cuenta-gastos-grid',
'dataProvider'=>$aCuentaGastos->search(),
'columns'=>array(
		'id',
		'denominacion',
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("CuentaGastos/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("CuentaGastos/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>