
/* @var $this CuentaGastosController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'CuentaGastos::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo CuentaGastos', 'url'=>array('create')),
	array('label'=>'Gestion de CuentaGastos', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>