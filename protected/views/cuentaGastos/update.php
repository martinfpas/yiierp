<?php
$this->breadcrumbs=array(
	CuentaGastos::model()->getAttributeLabel('models') => array('admin'),
	CuentaGastos::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.CuentaGastos::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.CuentaGastos::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.CuentaGastos::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=CuentaGastos::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>