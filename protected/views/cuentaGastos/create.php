<?php
/* @var $this CuentaGastosController */
/* @var $model CuentaGastos */

$this->breadcrumbs=array(
	CuentaGastos::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.CuentaGastos::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=CuentaGastos::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>