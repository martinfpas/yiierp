<?php
/* @var $this CuentaGastosController */
/* @var $data CuentaGastos */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('denominacion')); ?>:</b>
	<?php echo CHtml::encode($data->denominacion); ?>
	<br />


</div>