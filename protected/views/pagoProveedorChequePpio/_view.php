<?php
/* @var $this PagoProveedorChequePpioController */
/* @var $data PagoProveedorChequePpio */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idChequePpio')); ?>:</b>
	<?php echo CHtml::encode($data->idChequePpio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monto')); ?>:</b>
	<?php echo CHtml::encode($data->monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPagoProv')); ?>:</b>
	<?php echo CHtml::encode($data->idPagoProv); ?>
	<br />


</div>