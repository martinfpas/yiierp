
/* @var $this PagoProveedorChequePpioController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'PagoProveedorChequePpio::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo PagoProveedorChequePpio', 'url'=>array('create')),
	array('label'=>'Gestion de PagoProveedorChequePpio', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>