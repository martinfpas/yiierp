<?php
$this->breadcrumbs=array(
	PagoProveedorChequePpio::model()->getAttributeLabel('models') => array('admin'),
	PagoProveedorChequePpio::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProveedorChequePpio::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.PagoProveedorChequePpio::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.PagoProveedorChequePpio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=PagoProveedorChequePpio::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>