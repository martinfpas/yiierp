<?php
/* @var $this PagoProveedorChequePpioController */
/* @var $model PagoProveedorChequePpio */

$this->breadcrumbs=array(
	PagoProveedorChequePpio::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM '.PagoProveedorChequePpio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=PagoProveedorChequePpio::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>