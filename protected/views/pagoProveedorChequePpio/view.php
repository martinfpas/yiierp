<?php
$this->breadcrumbs=array(
	PagoProveedorChequePpio::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProveedorChequePpio::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.PagoProveedorChequePpio::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.PagoProveedorChequePpio::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.PagoProveedorChequePpio::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=PagoProveedorChequePpio::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idChequePpio',
		'monto',
		'idPagoProv',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
