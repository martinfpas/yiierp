<?php
/* @var $this ProdDepuracionController */
/* @var $model ProdDepuracion */

$this->breadcrumbs=array(
	'Prod Depuracions'=>array('index'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ProdDepuracion', 'url'=>array('admin')),
);
?>

<h1>Creando ProdDepuracion</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>