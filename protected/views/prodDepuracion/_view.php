<?php
/* @var $this ProdDepuracionController */
/* @var $data ProdDepuracion */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idDepuracion')); ?>:</b>
	<?php echo CHtml::encode($data->idDepuracion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArtVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArtVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPaquete')); ?>:</b>
	<?php echo CHtml::encode($data->idPaquete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idProdCarga')); ?>:</b>
	<?php echo CHtml::encode($data->idProdCarga); ?>
	<br />


</div>