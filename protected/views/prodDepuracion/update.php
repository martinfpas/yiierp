<?php
$this->breadcrumbs=array(
	'Prod Depuracions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdDepuracion', 'url'=>array('create')),
	array('label'=>'Ver ProdDepuracion', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdDepuracion', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdDepuracion <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>