<?php
$this->breadcrumbs=array(
	'Prod Depuracions'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdDepuracion', 'url'=>array('create')),
	array('label'=>'Modificar ProdDepuracion', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdDepuracion', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdDepuracion', 'url'=>array('admin')),
);
?>

<h1>Ver ProdDepuracion #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idDepuracion',
		'idArtVenta',
		'idPaquete',
		'cantidad',
		'idProdCarga',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
