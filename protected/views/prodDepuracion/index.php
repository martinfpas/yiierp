
/* @var $this ProdDepuracionController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Depuracions',
);

$this->menu=array(
	array('label'=>'Nuevo ProdDepuracion', 'url'=>array('create')),
	array('label'=>'Gestion de ProdDepuracion', 'url'=>array('admin')),
);
?>


<h1>$label</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>