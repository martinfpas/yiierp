
/* @var $this ProdCargaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Cargas',
);

$this->menu=array(
	array('label'=>'Nuevo ProdCarga', 'url'=>array('create')),
	array('label'=>'Gestion de ProdCarga', 'url'=>array('admin')),
);
?>

<h1>Prod Cargas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
