<?php
/* @var $this ProdCargaController */
/* @var $model ProdCarga */

$this->breadcrumbs=array(
	'Prod Cargas'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de ProdCarga', 'url'=>array('admin')),
);
?>

<h1>Nuevo ProdCarga</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>