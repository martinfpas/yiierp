<?php
$this->breadcrumbs=array(
	'Prod Cargas'=>array('admin'),
	$model->id,
);

$this->menu=array(	
	array('label'=>'Volver a revisar la Carga', 'url'=>array('carga/revisar', 'id'=>$model->idCarga)),	
);
?>

<h1>Ver ProdCarga #<?php echo $model->id; ?></h1>

<div class="row-fluid">
	<div class="span4">
		<?php $this->widget('bootstrap.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
						array(
							'label' => 'Codigo',
							'value' => $model->oArtVenta->codigo,
						),
						array(
							'label' => 'Descripcion',
							'value' => $model->oArtVenta->descripcion,
						),
				),
			)); 
		?>
	</div>
	<div class="span4">
		<?php $this->widget('bootstrap.widgets.TbDetailView',array(
			'data'=>$model,
			'attributes'=>array(
					'cantXbulto',
					'cantidad',
				),
			)); 
		?>	
	</div>
	<div class="span4">
		<?php
			if($model->BultosRecalculados != $model->Bultos && $model->oCarga->estado != Carga::iDespachada && $model->oCarga->estado != Carga::iDescartada){
				echo CHtml::link('Recalcular',array('recalcular','id'=>$model->id),array('class' => 'btn btn-primary'));
			}
		?>
	</div>
</div>


<?php
$dataProvider=new CArrayDataProvider(
	$aProdNotas,array('pagination'=> false,)
);

$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-nota-pedido-grid',
	'dataProvider'=>$dataProvider,
	'htmlOptions' => array(
		'style' => 'padding-top: 0px;',
	),
    'columns'=>array(
        array(
            'header' => 'NotaPedido',
            'value' => '$data->oNotaPedido->nroCombrobante',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
		),
		array(            
            'header' => 'digito',
            'value' => '$data->oNotaPedido->dig',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
		),
		array(
            'header' => 'carga',
            'value' => '$data->oNotaPedido->idCarga',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
		),
		array(
            'header' => 'Cliente',
            'value' => '$data->oNotaPedido->oCliente->Title',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'idArtVenta',
            'header' => 'Articulo',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'cantidad',
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'Present.',
            'header' => 'x Bulto',
            'value' => '$data->oPaquete->cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{view}',
				'buttons'=>array(
					'view' => array(
						'label' => 'Ver',
						'url'=>'Yii::app()->controller->createUrl("notaPedido/view", array("id"=>$data->oNotaPedido->id))',
						'options'=>array('target'=>'_blank'),
					),
				),
		),
    ),
)); ?>

