<?php
$this->breadcrumbs=array(
	'Prod Cargas'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdCarga', 'url'=>array('create')),
	array('label'=>'Ver ProdCarga', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdCarga', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdCarga <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>