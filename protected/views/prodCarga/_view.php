<?php
/* @var $this ProdCargaController */
/* @var $data ProdCarga */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCarga')); ?>:</b>
	<?php echo CHtml::encode($data->idCarga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArtVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArtVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantXbulto')); ?>:</b>
	<?php echo CHtml::encode($data->cantXbulto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />


</div>