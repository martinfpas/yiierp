
/* @var $this PagoProvMovimientoBancController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'PagoProvMovimientoBanc::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo PagoProvMovimientoBanc', 'url'=>array('create')),
	array('label'=>'Gestion de PagoProvMovimientoBanc', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>