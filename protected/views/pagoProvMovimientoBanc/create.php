<?php
/* @var $this PagoProvMovimientoBancController */
/* @var $model PagoProvMovimientoBanc */

$this->breadcrumbs=array(
	PagoProvMovimientoBanc::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.PagoProvMovimientoBanc::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=PagoProvMovimientoBanc::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>