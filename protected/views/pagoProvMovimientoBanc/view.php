<?php
$this->breadcrumbs=array(
	PagoProvMovimientoBanc::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProvMovimientoBanc::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.PagoProvMovimientoBanc::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.PagoProvMovimientoBanc::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.PagoProvMovimientoBanc::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Ver <?=PagoProvMovimientoBanc::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idMovBanc',
		'idPagoProveedor',
		'monto',
		/*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
		*/
),
)); ?>
