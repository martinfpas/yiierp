
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>

<div id="nuevoProdNotaPedido" style="">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'prod-nota-pedido-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ProdNotaPedido/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'prod-nota-pedido-grid',
			'idMas' => 'masProdNotaPedido',
			'idDivNuevo' => 'nuevoProdNotaPedido',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProdNotaPedido"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProdNotaPedido">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<?php echo $form->hiddenField($model,'idNotaPedido'); ?>
	<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'idArtVenta',array('class'=>'span12')); ?>
		</div>
			<div class="span2">
			<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>
		</div>
			<div class="span2">
			<?php echo $form->textFieldRow($model,'precioUnit',array('class'=>'span12')); ?>
		</div>
			<div class="span2">
			<?php echo $form->textFieldRow($model,'subTotal',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'idPaquete',array('class'=>'span12')); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'prod-nota-pedido-grid',
'dataProvider'=>$aProdNotaPedido->search(),
'afterAjaxUpdate' => 'js:function(id,data){return true;}',
'columns'=>array(
		'id',
		'idArtVenta',
		'cantidad',
		'precioUnit',
		'subTotal',
		/*
		'idPaquete',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaPedido/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaPedido/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>