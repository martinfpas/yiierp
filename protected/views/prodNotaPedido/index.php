
/* @var $this ProdNotaPedidoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Nota Pedidos',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaPedido', 'url'=>array('create')),
	array('label'=>'Gestion de ProdNotaPedido', 'url'=>array('admin')),
);
?>

<h1>Prod Nota Pedidos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
