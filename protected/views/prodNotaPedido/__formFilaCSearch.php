<style>
#nombreArticulo{
	font-weight: bold;
	font-style: italic;
}
#nuevoProdNotaPedido .alert-success{
    padding-top: 8px;
    padding-bottom: 8px;
}
</style>
<?php
    /**
     * @var $model ProdNotaPedido
     * @var $aProdNotaPedido[] ProdNotaPedido
     * @var $oArticuloVenta ArticuloVenta
     */

    if ($model->oNotaPedido == null || ($model->oNotaPedido != null && $model->oNotaPedido->oCliente == null)){
        Yii::log('Prodnotapedido con nota de pedido o cliente nulo'.ComponentesComunes::print_array($model),'warning');
    }

	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/tabindex.js");
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
    $cs->registerScriptFile($baseUrl."/js/music.js");
    $cs->registerScriptFile($baseUrl."/js/shortcut.js");


    $cs->scriptMap=array(
        'jquery.ba-bbq.js'=>false,
        'jquery.js'=>false,
        //'jquery.yiigridview.js'=>false, // TODO: VER SI ANULANDO ESTA LINEA SE ROMPE TODO
    );
	//$cs->registerCssFile($baseUrl."/css/customFineUploader.css");

	Yii::app()->clientScript->registerScript('general',"
		$(document).ready(function(){

            // SELECCIONA EL CONTENIDO CUANDO RECIBE FOCO...
            window.prevFocus = $();

            $('#nuevoProdNotaPedido input[type=\"text\"]').focus(function(){
            	let selected = false;
                $(this).on('mouseup.a keyup.a', function(e){
                    //$(this).off('mouseup.a keyup.a').select();
                    if($(prevFocus).attr('id') != $(this).attr('id') && !selected){
                        //console.log($(prevFocus).attr('id'));
                        $(this).off('mouseup.a keyup.a').select();
                        selected = true;
                    };
                });
                $(this).on('keypress.a', function(e){
					selected = true;
                });
            });

            $('#nuevoProdNotaPedido input[type=\"text\"]').blur(function(){
                window.prevFocus = $(this);
            });


		    $('#ArticuloVenta_id_rubro').focus();
                console.log('focus ArticuloVenta_id_rubro');
                try{
                    $('label').tooltip();
                    $('input').tooltip();
                }catch(e){
                    console.warn(e);
                }
            });

        $('#ProdNotaPedido_idArtVenta').change(function(ev){
            let idArt = $(this).val();
            var datos = 'id='+idArt;
            $.ajax({
                url: '".$this->createUrl('ArticuloVenta/GetAsyncById')."',
                data: {
                    id: idArt,
                    //TODO: UBICAR BIEN LA SIGUIEENTE LINEA
                    ".((($model->oNotaPedido != null) && ($model->oNotaPedido->oCliente != null) && $model->oNotaPedido->oCliente->esInstitucion())? "alicuota:31.5," : "" )."
                },
                type: 'GET',
                success: function(data){

                    let response = JSON.parse(data);

                    $('#ok_id_rubro').html(response.Rubro);
                    $('#ok_id_rubro').show('slow');

                    $('#ok_id_articulo').html(response.Articulo);
                    $('#ok_id_articulo').show('slow');

                    $('#ok_id_presentacion').html(response.descripcion);
                    $('#ok_id_presentacion').show('slow');
                    $('#ProdNotaPedido_cantidad').focus();
                    $('#ProdNotaPedido_precioUnit').val(parseFloat(response.precio));
                    $('#ProdNotaPedido_idArtVenta').val(response.id_articulo_vta);
                },
                error: function(x,y,z){
                    ev.stopPropagation();
                    ev.preventDefault();
                    $('#ProdNotaPedido_idArtVenta').focus();
                    beep();
                    respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    var htmlError = $(errorDiv).html();
                    $('#error_id_rubro').html(htmlError);
                    $('#error_id_rubro').show('slow');
                }
            });
        });



         $('#ArticuloVenta_id_rubro').keypress(function (ev) {
            $('#error_id_rubro').hide('slow');
            $('#ok_id_rubro').hide('slow');

            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            //console.log(keycode);
            if (keycode == '13') {
                //$('#prod-nota-pedido-form').submit();
                let datos = 'idRubro='+$(this).val();
                $.ajax({
                    url: '".$this->createUrl('Rubro/GetAsync')."',
                    data: datos,
                    type: 'GET',
                    success: function(html){
                        $('#ok_id_rubro').html(html);
                        $('#ok_id_rubro').show('slow');
                        $('#ArticuloVenta_id_articulo').focus();
                    },
                    error: function(x,y,z){
                        ev.stopPropagation();
                        ev.preventDefault();
                        beep();
                        $('#ArticuloVenta_id_rubro').focus();
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        var htmlError = $(errorDiv).html();
                        $('#error_id_rubro').html(htmlError);
                        $('#error_id_rubro').show('slow');
                    }
                });
                return false;
            }else if(keycode == '43'){
                $('#modalBuscaArticulos').modal();
                return false;
            }else{
                return true;
            }
        })

         $('#ArticuloVenta_id_articulo').keypress(function (ev) {
            $('#error_id_articulo').hide('slow');
            $('#ok_id_articulo').hide('slow');

            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                //$('#prod-nota-pedido-form').submit();
                let datos = 'idRubro='+$('#ArticuloVenta_id_rubro').val()+'&idArticulo='+$(this).val();
                $.ajax({
                    url: '".$this->createUrl('Articulo/GetAsync')."',
                    data: datos,
                    type: 'GET',
                    success: function(html){
                        $('#ok_id_articulo').html(html);
                        $('#ok_id_articulo').show('slow');
                        $('#ArticuloVenta_id_presentacion').focus();
                    },
                    error: function(x,y,z){
                        ev.stopPropagation();
                        ev.preventDefault();
                        beep();
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        var htmlError = $(errorDiv).html();
                        $('#error_id_articulo').html(htmlError);
                        $('#error_id_articulo').show('slow');
                        $('#ArticuloVenta_id_articulo').focus();
                    }
                });
                return false;
            }else if(keycode == '43'){
                $('#modalBuscaArticulos').modal();
                return false;
            }else{
                return true;
            }
        })
        $('#ArticuloVenta_id_presentacion').focus(function(){
            if($('#error_id_articulo').is(':visible')){
                $('#ArticuloVenta_id_articulo').focus();
            }
        });

         $('#ArticuloVenta_id_presentacion').keypress(function (ev) {
            $('#error_id_presentacion').hide('slow');
            $('#ok_id_presentacion').hide('slow');

            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                //$('#prod-nota-pedido-form').submit();
                let datos = 'idRubro='+$('#ArticuloVenta_id_rubro').val()+'&idArticulo='+$(this).val();
                $.ajax({
                    url: '".$this->createUrl('ArticuloVenta/GetAsync')."',
                    data: {
                        articulo: $('#ArticuloVenta_id_articulo').val(),
                        rubro: $('#ArticuloVenta_id_rubro').val(),
                        presentacion: $(this).val(),
                        //TODO: UBICAR BIEN LA SIGUIEENTE LINEA
                        ".((($model->oNotaPedido != null) && ($model->oNotaPedido->oCliente != null) && $model->oNotaPedido->oCliente->esInstitucion())? "alicuota:31.5," : "" )."
                    },
                    //dataType: 'json',
                    success: function(data){
                        let response = JSON.parse(data);
                        ev.stopPropagation();
                        ev.preventDefault();

                        $('#ok_id_presentacion').html(response.descripcion);
                        $('#ok_id_presentacion').show('slow');
                        $('#ProdNotaPedido_cantidad').focus();
                        $('#ProdNotaPedido_precioUnit').val(parseFloat(response.precio));
                        $('#ProdNotaPedido_idArtVenta').val(response.id_articulo_vta);
                        /*
                        //$('#nombreArticulo').html($('#ArticuloVenta_id_presentacion option:selected').attr('art'));

                        //$('#ProdNotaPedido_precioUnit').val(parseFloat($('#ArticuloVenta_id_presentacion option:selected').val()));

                        */
                    },
                    error: function(x,y,z){
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        beep();
                        var htmlError = $(errorDiv).html();
                        $('#error_id_presentacion').html(htmlError);
                        $('#error_id_presentacion').show('slow');
                        $('#ArticuloVenta_id_presentacion').focus();
                    }
                });
                return false;
            }else if(keycode == '43'){
                $('#modalBuscaArticulos').modal();
                return false;
            }else{
                return true;
            }
        })

        $('#cantPaquete').keypress(function (ev) {
            $('#error_id_paquete').hide('slow');
            $('#ok_id_paquete').hide('slow');
            console.log('#cantPaquete');
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            var cantidad = (!isNaN(parseFloat($('#cantPaquete').val()))? parseFloat($('#ProdNotaPedido_cantidad').val()) : 0 );
            var cantPaquete = (!isNaN(parseFloat($('#cantPaquete').val()))? parseFloat($('#cantPaquete').val()) : 0 );
            if (keycode == '13') {
                $.ajax({
                    url: '".$this->createUrl('Paquete/GetAsync')."',
                    data: {
                        idArticulo: $('#ProdNotaPedido_idArtVenta').val(),
                        cantidad: $(this).val(),
                    },
                    //dataType: 'json',
                    success: function(data){
                        let response = JSON.parse(data);


                        $('#ok_id_paquete').html(response.envase);
                        console.log(response.cantidad);
                        
                        if(cantidad < cantPaquete){
                            $('#error_id_paquete').html('La cantidad no puede ser menor que el paquete');
                            $('#error_id_paquete').show('slow');
                            $('#ProdNotaPedido_cantidad').focus();
                            return false;
                        }
                        
                        $('#ok_id_paquete').show('slow');
                        $('#ProdNotaPedido_cantidad').focus();
                        $('#ProdNotaPedido_idPaquete').val(response.id);
                        //$('#guardaArticulo').focus();
                        $('#prod-nota-pedido-form').submit();
                    },
                    error: function(x,y,z){
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        beep();
                        var htmlError = $(errorDiv).html();
                        $('#error_id_paquete').html(htmlError);
                        $('#error_id_paquete').show('slow');
                        $('#cantPaquete').focus();
                    }
                });
                return false;

            }else{
                return true;
            }


		})

		function buscarPresentacAsync(request, response){
			console.log(request);
			$.ajax({
	           	url: '".$this->createUrl('ArticuloVenta/buscarPresentacAsync')."',
				dataType: 'json',
				data: {
					articulo: $('#ArticuloVenta_id_articulo').val(),
					presentacion: request.term
				},
				success: function (data) {
	                response(data);
				}
			})
		}
		
		function afterUpdate(){
		    console.log($('[rel=\"ProdNotaPedido_precioUnit\"]').length);
		    if($('[rel=\"ProdNotaPedido_precioUnit\"]').length = 10){
		        window.logation = '".Yii::app()->createUrl('NotaPedido/update',array('id'=> $model->idNotaPedido))."';
		    }
		}

	",CClientScript::POS_READY);

?>
<script>
    /****** CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
    function moveDown() {
        var rows = $('#prod-nota-pedido-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).next('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).next('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }

    function moveUp() {
        var rows = $('#prod-nota-pedido-grid table tr');
        var currentRow = $("tr.selected").get(0);

        if (rows.length > 2) {
            if (currentRow === undefined) {
                rows.eq(1).addClass('selected');
            } else if ($(currentRow).prev('tr').get(0) === undefined) {
                //do nothing
            } else {
                $(currentRow).prev('tr').addClass('selected');
                $(currentRow).removeClass('selected');
            }
        }
    }

    $(document).ready(function(){
        shortcut.add('Down', function() {
            moveDown();
        });

        shortcut.add('Up', function() {
            moveUp();
        });
    });

    /****** fin CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
</script>
<style>
.tooltip-inner {
    white-space: pre-wrap;
}
.tooltip {
    white-space: pre-wrap;
}

</style>
&#013;
<div id="nuevoProdNotaPedido" style="">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'prod-nota-pedido-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ProdNotaPedido/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'prod-nota-pedido-grid',
			'idMas' => 'masProdNotaPedido',
			'idDivNuevo' => 'nuevoProdNotaPedido',
            'style' => 'margin-bottom:0px;'
		),
	)); ?>


		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProdNotaPedido"></div>

	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<?php
		echo $form->hiddenField($model,'idNotaPedido');
		echo $form->hiddenField($model,'idArtVenta');
	?>
	<div class="row-fluid">
		<div class="span3">
			<?php
                echo $form->textFieldRow($oArticuloVenta,'id_rubro',array('class'=>'span12','tabindex'=>'1','title' => 'Ingrese el codigo o presione + para buscar'));
			?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_rubro"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_rubro"></div>
		</div>
		<div class="span4">
			<?php echo $form->textFieldRow($oArticuloVenta,'id_articulo',array('class'=>'span12','tabindex'=>'2','title' => 'Ingrese el codigo o presione + para buscar')); ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_articulo"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_articulo"></div>
		</div>
		<div class="span5">
			<?php echo $form->textFieldRow($oArticuloVenta,'id_presentacion',array('class'=>'span12','tabindex'=>'3','title' => 'Ingrese el codigo o presione + para buscar')); ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_presentacion"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_presentacion"></div>
		</div>
	</div>
	<div class="row-fluid">
		<!--<div class="span3" id="nombreArticulo">-->
        <div class="span3" >
            <?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12','tabindex'=>'4')); ?>
		</div>

		<div class="span2">
            <?php echo $form->textFieldRow($model,'precioUnit',array('class'=>'span12','tabindex'=>'5')); ?>
		</div>
		<div class="span2">
            <?php
            echo CHtml::label('Paquete','cantPaquete');
            echo CHtml::textField('cantPaquete','',array('class'=>'span12','tabindex'=>'6'));
            echo $form->hiddenField($model,'idPaquete');
            //echo $form->dropDownList($model,'idPaquete',CHtml::listData(array(),'id_presentacion','Presentacion'),array('tabindex'=>'6','style' => 'width:100%','empty' => '--Seleccionar--'));
            ?>
            <div class="alert alert-block alert-error" style="display:none;" id="error_id_paquete"></div>
            <div class="alert alert-block alert-success" style="display:none;" id="ok_id_paquete"></div>
		</div>
		<div class="span5">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Cargar' : 'Guardar',
                'htmlOptions' => array(
                    'id' => 'guardaArticulo',
                   'style' => 'margin-top:25px;'
                )
            )); ?>
        </div>

	</div>
</div>


    <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProdNotaPedido">Datos Guardados Correctamente !</div>
	<?php $this->endWidget(); ?>
</div>



<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'prod-nota-pedido-grid',
'dataProvider'=>$aProdNotaPedido->searchWP(),
    //'afterAjaxUpdate' => 'function(){$(\'[data-toggle="tooltip"]\').tooltip({ html:true, trigger:\'hover\'})}', // <-- this line does the trick!
    'afterAjaxUpdate'=>'function(id, data){console.log("_formFilaCSearch afterAjaxUpdate:"+id);afterUpdate();return true;$("#prod-nota-pedido-grid").trigger("change");}',
'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => 'Codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
		array(
				'name' => 'idArtVenta',
				'header' => 'Articulo',
				'value' => '$data->oArtVenta->descripcion',
				'htmlOptions' => array('style' => ''),
		),
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'cantidad',
            'headerHtmlOptions' => array('style' => 'width: 60px;text-align:right'),
            'editable' => array(    //editable section
                'url'        => $this->createUrl('prodNotaPedido/SaveFieldAsync'),
                'placement'  => 'right',
                'success' => 'js: function(response, newValue) {
                                $("#prod-nota-pedido-grid").yiiGridView("update", {
                                    });
                             }',
                    // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                    'onShown' => 'js: function(ev,editable) {                                 
                                     setTimeout(function() {
                                        editable.input.$input.select();
                                      },0);
                                }',
            )
        ),
		/*
		array(
            'name' => 'precioUnit',
            'value' => '"$".number_format($data->precioUnit,2)',
            //'value' => '"$".$data->precioUnit',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
		),
        */
        array(
            'class' => 'editable.EditableColumn',
            'name' => 'precioUnit',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width: 75px;text-align:right;'),
            'editable' => array(
                //'apply'      => '$data->precioSinIva != 4', //can't edit deleted users
                'url'        => $this->createUrl('prodNotaPedido/SaveFieldAsync'),
                'placement'  => 'right',
                'success' => 'js: function(response, newValue) {
                                $("#prod-nota-pedido-grid").yiiGridView("update", {
                                    });
                             }',
                // LO SIGUIENTE AUTOSELECCIONA EL TEXTO DENTRO DEL EDITABLE
                'onShown' => 'js: function(ev,editable) {                                 
                                 setTimeout(function() {
                                    editable.input.$input.select();
                                  },0);
                            }',
                'display' => 'js: function(value, sourceData) {
                     $(this).html("$ "+parseFloat(value).toFixed(2));
                }',
            )
        ),

        array(
        'name' => 'SubTotal',
			'value' => '"$".$data->SubTotal',
            'header' => 'Total Item',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => "<b>Total Estimado: $".$model->oNotaPedido->getSubTotal().'</b>',
            'footerHtmlOptions' => array('style' => 'text-align:right;font-size: initial;font-style: initial;'),

		),
        array(
            'name' => 'Present.',
            'header' => 'x Bulto',
            'value' => '$data->oPaquete->cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
		/*
		'idPaquete',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			//'template' => '{update} | {delete}',
            'template' => '{delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaPedido/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaPedido/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>
<div class="span3" style="">

    <div style="float: right">
        <?php
        $this->widget('editable.EditableField', array(
            'type'      => 'select',
            'model'       => $model->oNotaPedido,
            //'apply'      => false,
            'attribute'   => 'impSinPrecio',
            'url'         => $this->createUrl('NotaPedido/SaveFieldAsync'),
            'placement'   => 'right',
            'source'    => NotaPedido::$aSiNo,

            'htmlOptions' => array(
                'style' => "float: right;margin-left: 5px;",
            ),
        ));?>
    </div>
    <div style="float: right">Imprimir sin Precios :</div>
</div>
<?php
$this->widget('BuscaArtVenta', array('js_comp'=>'#ProdNotaPedido_idArtVenta','is_modal' => true));
?>
