<?php
/* @var $this ProdNotaPedidoController */
/* @var $data ProdNotaPedido */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaPedido')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaPedido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArtVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArtVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioUnit')); ?>:</b>
	<?php echo CHtml::encode($data->precioUnit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subTotal')); ?>:</b>
	<?php echo CHtml::encode($data->subTotal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idPaquete')); ?>:</b>
	<?php echo CHtml::encode($data->idPaquete); ?>
	<br />


</div>