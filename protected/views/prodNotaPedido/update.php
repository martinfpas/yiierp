<?php
$this->breadcrumbs=array(
	'Prod Nota Pedidos'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaPedido', 'url'=>array('create')),
	array('label'=>'Ver ProdNotaPedido', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdNotaPedido', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdNotaPedido <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>