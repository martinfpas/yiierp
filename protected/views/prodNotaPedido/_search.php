<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idNotaPedido',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idArtVenta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'precioUnit',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'subTotal',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idPaquete',array('class'=>'span12')); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
