<?php
/* @var $this ProdNotaPedidoController */
/* @var $model ProdNotaPedido */

$this->breadcrumbs=array(
	'Prod Nota Pedidos'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM ProdNotaPedido', 'url'=>array('admin')),
);
?>

<h1>Nuevo ProdNotaPedido</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>