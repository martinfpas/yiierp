<?php
$this->breadcrumbs=array(
	'Prod Nota Pedidos'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaPedido', 'url'=>array('create')),
	array('label'=>'Modificar ProdNotaPedido', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdNotaPedido', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdNotaPedido', 'url'=>array('admin')),
);
?>

<h1>Ver ProdNotaPedido #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'idNotaPedido',
		'idArtVenta',
		'cantidad',
		'precioUnit',
		'subTotal',
		'idPaquete',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
