
<?php $this->widget('ext.selgridview.BootSelGridView',array(
'id'=>'vdocumentos-con-saldo-grid',
'template' => '{items}',
'selectableRows' => 2,
'dataProvider'=>$model->searchWP(),
//'filter'=>$model,
'enablePagination' => false,
'columns'=>array(
        array(
            'class' => 'CCheckBoxColumn',
            'cssClassExpression' => '$data->tipo."_".$data->id',
        ),
		'tipo',
		'nroComprobante',
		'fecha',
		'total',
		'saldo',

        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
	),
)); ?>
