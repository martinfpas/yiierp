<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'vdocumentos-con-saldo-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorVDocumentosConSaldo"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okVDocumentosConSaldo">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span3">
			<?php echo $form->textFieldRow($model,'uid',array('class'=>'span12','maxlength'=>36)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'tipo',array('class'=>'span12','maxlength'=>2)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroComprobante',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
		</div>

                    </div>
                    <div class="row-fluid">		<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'total',array('class'=>'span12')); ?>
		</div>
		<div class="span3">
			<?php echo $form->textFieldRow($model,'saldo',array('class'=>'span12')); ?>
		</div>

                    </div>
                    <div class="row-fluid">        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
            )); ?>
            </div>
        </div>
    </div>
</div>


<?php $this->endWidget(); ?>
