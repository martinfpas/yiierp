<?php
/* @var $this VDocumentosConSaldoController */
/* @var $model VDocumentosConSaldo */

$this->breadcrumbs=array(
	VDocumentosConSaldo::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.VDocumentosConSaldo::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=VDocumentosConSaldo::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>