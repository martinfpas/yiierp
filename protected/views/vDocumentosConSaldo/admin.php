<?php
/* @var $this VDocumentosConSaldoController */
/* @var $model VDocumentosConSaldo */

$this->breadcrumbs=array(
	VDocumentosConSaldo::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo '.VDocumentosConSaldo::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#vdocumentos-con-saldo-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?=VDocumentosConSaldo::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'vdocumentos-con-saldo-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		'uid',
		'tipo',
		'nroComprobante',
		'id',
		'fecha',
		'Id_Cliente',
		/*
		'total',
		'saldo',
		*/
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
