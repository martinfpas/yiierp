
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoVDocumentosConSaldo" id="masVDocumentosConSaldo" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoVDocumentosConSaldo" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'vdocumentos-con-saldo-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('VDocumentosConSaldo/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'vdocumentos-con-saldo-grid',
			'idMas' => 'masVDocumentosConSaldo',
			'idDivNuevo' => 'nuevoVDocumentosConSaldo',
		),
	)); ?>
	
		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>
	
		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorVDocumentosConSaldo"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okVDocumentosConSaldo">Datos Guardados Correctamente !</div>
		
	<?php echo $form->errorSummary($model); ?>
<div class="content-fluid">
	<div class="row-fluid">
		
	
			<div class="span3">
			<?php echo $form->textFieldRow($model,'uid',array('class'=>'span12','maxlength'=>36)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'tipo',array('class'=>'span12','maxlength'=>2)); ?>
		</div>
	
</div>
<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'nroComprobante',array('class'=>'span12','maxlength'=>8)); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
		</div>
	
</div>
<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'Id_Cliente',array('class'=>'span12')); ?>
		</div>
	
</div>
<div class="row-fluid">
		<div class="span3">
			<?php echo $form->textFieldRow($model,'total',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($model,'saldo',array('class'=>'span12')); ?>
		</div>
	
</div>
<div class="row-fluid">

        <div class="span3">
            <div class="form-actions">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'vdocumentos-con-saldo-grid',
'dataProvider'=>$aVDocumentosConSaldo->search(),
'columns'=>array(
		'uid',
		'tipo',
		'nroComprobante',
		'id',
		'fecha',
		'Id_Cliente',
		/*
		'total',
		'saldo',
		*/
		/*
		array(
			'name' => '',
			'value' => '',
            'header' => '',
			'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("VDocumentosConSaldo/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("VDocumentosConSaldo/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>