<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'nroCombrobante',array('class'=>'span12','maxlength'=>6)); ?>
    </div>
    <div class="span4">
		<?php echo $form->textFieldRow($model,'dig',array('class'=>'span12')); ?>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'idCliente',array('class'=>'span12')); ?>
    </div>
    <div class="span4">
        <?php
        echo $form->labelEx($model,'camion');
        echo $form->dropDownList($model,'camion',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array('empty'=>'Todos','tabindex' => 4));
        ?>
    </div>
    <div class="span4">
        <?php
        echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'value'=> ComponentesComunes::fechaFormateada($model->fecha),
            'name'=>'NotaPedido[fecha]',
            'language'=>'es',
            'options'=>array(
                'showAnim'=>'fold',
                'monthRange'=>'-2:+2',
                'dateFormat'=>'dd-mm-yy',
                'changeYear' => true,
                'changeMonth' => true,
            ),
            'htmlOptions'=>array(
                'style'=>'height:25px;',
                'class'=>'span12',
                'title'=>'Hacer click en el campo y seleccionar la fecha',
                //'tabindex' => 5
            ),
        ));
        ?>
    </div>

</div>
<div class="row-fluid">
    <div class="span4">
		<?php echo $form->textFieldRow($model,'idCarga',array('class'=>'span12')); ?>
    </div>

    <div class="span4" style="padding-top: 24px">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
