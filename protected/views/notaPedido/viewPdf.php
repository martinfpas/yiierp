
<h4>Nota de Pedido #<?php echo $model->id; ?></h4>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'nroCombrobante',
		'dig',
		array(
            'name' => 'idCliente',
			'value' => $model->oCliente->Title,
		),
        array(
            'name' => 'camion',
            'value' => $model->oCamion->IdNombre,
        ),
        array(
            'name' => 'fecha',
			'value' => ComponentesComunes::fechaFormateada($model->fecha),

		),
        array(
            'name' => 'idCarga',
            'type'=>'raw',
			'value' => ($model->idCarga != null)? CHtml::link('Ir a carga Nº'.$model->idCarga,Yii::app()->createUrl('carga/view',array('id' => $model->idCarga))) : 'No hay cargas asociadas',
			'htmlOptions' => array('style' => ''),
		),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-nota-pedido-grid',
    'dataProvider'=>$aProdNotaPedido->search(),
    //'afterAjaxUpdate' => 'function(){$(\'[data-toggle="tooltip"]\').tooltip({ html:true, trigger:\'hover\'})}', // <-- this line does the trick!
    'afterAjaxUpdate'=>'function(id, data){}',
    'enableSorting' => false,
    'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => 'Codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'idArtVenta',
            'header' => 'Articulo',
            'value' => 'ucfirst(strtolower($data->oArtVenta->descripcion))',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'cantidad',
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'precioUnit',
            'value' => '"$".number_format($data->precioUnit,2)',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'SubTotal',
            //'value' => '"$".number_format($data->SubTotal,2)',
            'value' => '"$".$data->SubTotal',
            'header' => 'Total Item',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footer' => "<b>Total Estimado: $".$model->getSubTotal().'</b>',
            'footerHtmlOptions' => array('style' => 'text-align:right;'),

        ),
        array(
            'name' => 'Present.',
            'header' => 'x Bulto',
            'value' => '$data->oPaquete->cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

    ),
)); ?>
