<?php
/* @var $this NotaPedidoController */
/* @var $model NotaPedido */

$this->breadcrumbs=array(
	'Nota Pedidos'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nueva Nota de Pedido', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});

$('#crearCarga').click(function(e){
    e.preventDefault();
    let camion = $('#nota-pedido-grid .filters').find('#NotaPedido_camion').val();
    let urlBase = $(this).attr('urlBase');
    if(camion > 0){
        $(this).attr('href',urlBase+'&idCamion='+camion);
    }
    window.location = $(this).attr('href');
    return true;        
    
});
$('.search-form form').submit(function(){
	$('#nota-pedido-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Notas de Pedidos Sin Carga</h2>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>


<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn','id' => 'busquedaAvanzada')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<?php echo CHtml::link('Crear Carga',Yii::app()->createUrl('carga/create'),array('class'=>'btn btn-primary','id'=>'crearCarga','urlBase' => Yii::app()->createUrl('carga/create'))); ?>

<?php $this->widget('ext.selgridview.BootSelGridView',array(
'id'=>'nota-pedido-grid',
'dataProvider'=>$model->searchSinCarga(),
'filter'=>$model,
    'selectableRows' => 2,
'columns'=>array(

		array(
            'name' => 'nroCombrobante',
            'header' => 'nro comp.',
            'htmlOptions' => array('style' => 'width:73px;text-align:right;'),
			'headerHtmlOptions' => array('style' => 'width:73px;text-align:right;'),
        ),
		array(
            'name' => 'dig',
            'htmlOptions' => array('style' => 'text-align:center;'),
			'headerHtmlOptions' => array('style' => 'width:30px;text-align:center;'),
		),
        array(
            'name' => 'idCliente',
            'value' => '($data->oCliente != null)? $data->oCliente->Title : "CLIENTE NO DEFINIDO!!!"',
			'htmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','Title'),
		),
        array(
            'name' => 'sViajante',
            'value' => '($data->oCliente != null && $data->oCliente->oViajante != null)? $data->oCliente->oViajante->IdTitulo : "No Definido"',
            'htmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Viajante::model()->findAll(),'numero','IdTitulo'),
        ),
        array(
            'name' => 'camion',
			'value' => '($data->oCamion != null)? $data->oCamion->IdNombre : "CAMION NO DEFINIDO!!!"',
			'htmlOptions' => array('style' => ''),
		    'filter' =>  CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),
		),

        array(
            'name' => 'fecha',
			'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
			'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
		),

        array(
            'name' => 'PesoTotal',
			'header' => 'Peso',
			'value' => '$data->PesoTotal',
            'htmlOptions' => array('style' => 'width:50px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:50px;text-align:right;'),
            'footer' => "<b>Total:".$model->getSumaPeso($model->searchSinCarga()->getKeys()).'</b>',
            'footerHtmlOptions' => array('style' => 'text-align:right;font-size: initial;font-style: initial;'),
		),
        array(
            'name' => 'idCliente',
            'header' => 'localidad',
			'value' => '($data->oCliente != null)? $data->oCliente->localidad : "CLIENTE NO DEFINIDO!!!" ',
		),


		/*
		'idCarga',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),

		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
