<?php
/* @var $this NotaPedidoController */
/* @var $model NotaPedido */

$this->breadcrumbs=array(
	'Notas de Pedido'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM', 'url'=>array('admin')),
);
?>
<div class="content-fluid">
    <div class="row-fluid">
        <div class="span6">
            <h2>Nueva Nota de Pedido</h2>
        </div>
        <div class="span3">
        </div>
        <div class="span3" id="nNotaDePedido" style="font-size: 31.5px;margin-top: 20px;">

        </div>
    </div>
</div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>