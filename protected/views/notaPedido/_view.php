<?php
/* @var $this NotaPedidoController */
/* @var $data NotaPedido */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroCombrobante')); ?>:</b>
	<?php echo CHtml::encode($data->nroCombrobante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dig')); ?>:</b>
	<?php echo CHtml::encode($data->dig); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCliente')); ?>:</b>
	<?php echo CHtml::encode($data->idCliente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('camion')); ?>:</b>
	<?php echo CHtml::encode($data->camion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha')); ?>:</b>
	<?php echo CHtml::encode($data->fecha); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idCarga')); ?>:</b>
	<?php echo CHtml::encode($data->idCarga); ?>
	<br />


</div>