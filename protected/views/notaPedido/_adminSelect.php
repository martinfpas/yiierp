<?php
Yii::app()->clientScript->registerScript('armarCarga', "
    var armandoCarga = false;
    $(document).ready(function(){
        // DISPARA LA ACCION DE ARMADO DE CARGA DE LAS NOTAS DE PEDIDO SELECCIONADA
        $('#armarCarga').click(function(){
            if(armandoCarga){
                alert('Ya se esta procesando');
                return false;
            }
            armandoCarga = true;
            console.log('$(#armarCarga).click'+' armandoCarga:'+armandoCarga);
            // CARGA LA MASCARA PARA QUE NO SE TOQUE NADA
            $('.modalLoading').show();

            var seleccionados = $('#nota-pedido-grid').selGridView('getAllSelection');

            if(seleccionados.length < 1){
                alert('Seleccione una Nota de Pedido');
                $('.modalLoading').hide();
                return false;
            };


            let url = $(this).attr('url');
            let deferredCalls = notaPedidoAsignCargaDeferredCalls(url,seleccionados);

            var redireccion = $('#redireccion').val();

            $.when.apply($, deferredCalls).done(function() {
                $('.modalLoading').hide('slow');
                $('.modalLoading').html('');
                // REDIRIJE AL UPDATE CUANDO TERMINA DE ARMAR LA CARGA
                armandoCarga = false;
                window.location.href = redireccion;
            });

        });
        function notaPedidoAsignCargaDeferredCalls(url,arrSeleted){
            console.log(arrSeleted);
            let idCarga = $('#idCarga').val();
            let calls = [];
            let i = 1;
            $('.modalLoading').html('<div class=\"span12\" style=\"text-align: center;\"><h3>Procesando <span id=\"textProcesandoNP\">1</span> de '+arrSeleted.length+'</h3></div>');
            arrSeleted.map((nota) => {
                // REALIZAR LA LLAMADA PARA ASIGNAR LA CARGA
                //TODO: HACER ACTION EN NOTADEPEDIDO
                let datos = 'idNotaPedido='+nota+'&idCarga='+idCarga;
                calls.push( $.ajax({
                    url:url,
                    type:'GET',
                    data: datos,
                    success:function(html) {
                      //TODO: SE HACE ALGO CON EL html?
                      console.log(i+'=>'+nota);
                      $('#textProcesandoNP').html(i);
                      i++;
                    },
                    error:function(x,y,z) {
                      // TODO: MOSTRAR LOS ERRORES
                      console.log(x);
                      $('.modalLoading').hide('slow');
                      alert(x.responseText);
                      //return false;
                    }
                }));

            })
            console.log(calls);
            return calls;
        }
        $('#descartarCarga').click(function(e){
            //var datos = 'id='+$('#idCarga').val()+'&ajax=';
            var datos = 'id='+$('#idCarga').val()+'&ajax=';
            var url = $(this).attr('url')+'&id='+$('#idCarga').val();
            $.ajax({
                url:url,
                type:'POST',
                data: datos,
                success:function(html) {
                  //TODO: SE HACE ALGO CON EL html?
                  console.log(html);
                  document.location = '".Yii::app()->createUrl('/carga/admin')."';
                  return true;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  console.log(x);
                  alert(x.responseText);
                  return false;
                }
            });
        });
    });
");

?>
<div>
    <?php
        if (!$bAgregar) {
            echo CHtml::link('Armar Carga', '#', array('class' => 'bulk-button btn', 'id' => 'armarCarga', 'url' => Yii::app()->createUrl('NotaPedido/AsignCargaAsync'), 'style' => 'float:left;margin-right: 20px;'));
            echo CHtml::hiddenField('redireccion',Yii::app()->createUrl('/Carga/update/')."&id=".$idCarga);
        }else{
            echo CHtml::link('Agregar Notas a la carga', '#', array('class' => 'bulk-button btn btn-primary', 'id' => 'armarCarga', 'url' => Yii::app()->createUrl('NotaPedido/AsignCargaAsync'), 'style' => 'float:left;margin-right: 20px;'));
            echo CHtml::hiddenField('redireccion',Yii::app()->createUrl('/Carga/view/')."&id=".$idCarga);
        }
    ?>
<?php echo CHtml::hiddenField('idCarga',$idCarga);?>
<?php
    if (!$bAgregar){
        echo CHtml::link('Descartar Carga','#',array('class'=>'bulk-button btn','id' => 'descartarCarga','url' => Yii::app()->createUrl('carga/delete'),'style' => 'float:left;' ));
    }

?>
</div>

<?php $this->widget('ext.selgridview.BootSelGridView',array(
'id'=>'nota-pedido-grid',
    'selectableRows' => 2,
    'dataProvider'=>$aNotaPedido->searchSinCarga($aNotaPedido->camion),
    'afterAjaxUpdate'=>'function(id, data){}',
    //'afterAjaxUpdate' => 'function(){$(\'[data-toggle="tooltip"]\').tooltip({ html:true, trigger:\'hover\'})}', // <-- this line does the trick!
'columns'=>array(
        array(
            'class' => 'CCheckBoxColumn',
        ),
		'nroCombrobante',
		'dig',
        array(
            'name' => 'Cliente',
            'value' => '($data->oCliente)? $data->oCliente->title : "RETIRAR ESTA NOTA DE PEDIDO Y VERIFICAR CLIENTE"',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'Monto',
			'value' => '"$".$data->SubTotal',
			'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
		),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),

		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/

	),
)); ?>
