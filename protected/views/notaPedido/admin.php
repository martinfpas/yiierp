<?php
/* @var $this NotaPedidoController */
/* @var $model NotaPedido */

$this->breadcrumbs=array(
	'Nota Pedidos'=>array('admin'),
	'ABM',
);

$this->menu=array(
	array('label'=>'Nueva Nota de Pedido', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#nota-pedido-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h2>Notas de Pedidos Historicas</h2>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'nota-pedido-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(

		array(
            'name' => 'nroCombrobante',
            'header' => 'nro comp.',
            'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
			'headerHtmlOptions' => array('style' => 'width:90px;text-align:right;'),
        ),
		array(
            'name' => 'dig',
            'htmlOptions' => array('style' => 'text-align:center;'),
			'headerHtmlOptions' => array('style' => 'width:30px;text-align:center;'),
		),
        array(
            'name' => 'idCliente',
            'value' => '($data->oCliente)? $data->oCliente->Title : "ESTA NOTA DE PEDIDO Y NO TIENE CLIENTE"',
			'htmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Cliente::model()->todos()->findAll(),'id','Title'),
		),
        array(
            'name' => 'camion',
			'value' => '$data->oCamion->IdNombre',
			'htmlOptions' => array('style' => ''),
		    'filter' =>  CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),
		),
        array(
            'name' => 'fecha',
			'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
			'htmlOptions' => array('style' => 'width:75px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:75px;text-align:center;'),
		),

		/*
		'idCarga',
		*/

		array(
			'name' => 'idCarga',
			'htmlOptions' => array('style' => 'width:70px;text-align:right;'),
            'headerHtmlOptions' => array('style' => 'width:70px;text-align:right;'),
		),

        array(
            'name' => 'estadoCarga',
            'header' => 'Est.Carga',
            'value' => '($data->oCarga != null)? Carga::$aEstado[$data->oCarga->estado]  : "Sin carga vinculada"',
            'headerHtmlOptions' => array('style' => 'width:90px;text-align:center;'),
            'filter' => Carga::$aEstado,
        ),

		array(
            'name' => 'SubTotal',
            'value' => '$data->SubTotal',
            'headerHtmlOptions' => array('style' => 'width:90px;text-align:center;'),
            'htmlOptions' => array('style' => 'width:90px;text-align:right;'),
        ),

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{view} {delete}',
		),
	),
)); ?>
