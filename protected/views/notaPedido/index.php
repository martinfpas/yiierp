
/* @var $this NotaPedidoController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Nota Pedidos',
);

$this->menu=array(
	array('label'=>'Nueva '.NotaPedido::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'ABM '.NotaPedido::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h1>Nota Pedidos</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
