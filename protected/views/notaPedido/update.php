<?php
$this->breadcrumbs=array(
	'Nota Pedidos'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);


if($model->oCarga != null ){
    $this->menu=array(
        array('label'=>'ABM NotaPedido', 'url'=>array('admin')),
        array('label'=>'Ir a la Carga asociada', 'url'=>array('carga/view','id' => $model->oCarga->id)),
    );
}else{

    $this->menu=array(
        array('label'=>'Ver NotaPedido', 'url'=>array('view', 'id'=>$model->id)),
        array('label'=>'Borrar ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
        array('label'=>'ABM Notas de Pedido', 'url'=>array('admin')),
    );
}

?>

	<h2>Modificando NotaPedido <?php echo $model->id; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>