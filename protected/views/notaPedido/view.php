<?php
/** @var $model NotaPedido */
$this->breadcrumbs=array(
	'Nota Pedidos'=>array('admin'),
	$model->id,
);

if($model->oCarga != null ){
    $this->menu=array(
        //array('label'=>'Modificar ', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Ir a la Carga asociada', 'url'=>array('carga/view','id' => $model->oCarga->id)),
    );
}else{

    $this->menu=array(
        array('label'=>'Modificar ', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Borrar ', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
        array('label'=>'ABM Notas de Pedido', 'url'=>array('admin')),
    );
}


?>

<h2>Ver Nota de Pedido #.<?php echo $model->Titulo; ?></h2>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'idCliente',
                        'value' => ($model->oCliente)? $model->oCliente->title : "RETIRAR ESTA NOTA DE PEDIDO Y VERIFICAR CLIENTE",
                    ),
                    array(
                        'name' => 'camion',
                        'value' => $model->oCamion->IdNombre,
                    ),
                    array(
                        'name' => 'Nota Entrega',
                        'type'=>'raw',
                        'value' => ($model->oNotaEntrega != null)? CHtml::link('Nota de Entrega Nº'.$model->oNotaEntrega->FullTitle,Yii::app()->createUrl('NotaEntrega/view',array('id' => $model->oNotaEntrega->id))) : 'No hay NE asociadas',
                        'htmlOptions' => array('style' => ''),
                    ),
                ),
            )); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                'data'=>$model,
                'attributes'=>array(
                    array(
                        'name' => 'fecha',
                        'value' => ComponentesComunes::fechaFormateada($model->fecha),

                    ),
                    array(
                        'name' => 'idCarga',
                        'type'=>'raw',
                        'value' => ($model->idCarga != null)? CHtml::link('Ir a carga Nº'.$model->idCarga,Yii::app()->createUrl('carga/view',array('id' => $model->idCarga))) : 'No hay cargas asociadas',
                        'htmlOptions' => array('style' => ''),
                    ),
                    array(
                        'name' => 'Factura',
                        'type'=>'raw',
                        'value' => ($model->oFactura != null)? CHtml::link('Factura Nº'.$model->oFactura->FullTitle,Yii::app()->createUrl('Factura/view',array('id' => $model->oFactura->id))) : 'No hay Factura asociada',
                        'htmlOptions' => array('style' => ''),
                    ),
                ),
            )); ?>
        </div>
    </div>
</div>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'prod-nota-pedido-grid',
    'dataProvider'=>$aProdNotaPedido->searchWP(),
    //'afterAjaxUpdate' => 'function(){$(\'[data-toggle="tooltip"]\').tooltip({ html:true, trigger:\'hover\'})}', // <-- this line does the trick!
    'afterAjaxUpdate'=>'function(id, data){}',
    'columns'=>array(
        array(
            'name' => 'idArtVenta',
            'header' => 'Codigo',
            'value' => '$data->oArtVenta->codigo',
            'htmlOptions' => array('style' => 'text-align:center;'),
            'headerHtmlOptions' => array('style' => 'text-align:center;'),
        ),
        array(
            'name' => 'idArtVenta',
            'header' => 'Articulo',
            'value' => '$data->oArtVenta->descripcion',
            'htmlOptions' => array('style' => ''),
        ),

        array(
            'name' => 'cantidad',
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'precioUnit',
            'value' => '"$".number_format($data->precioUnit,2)',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

        array(
            'name' => 'SubTotal',
            //'value' => '"$".number_format($data->SubTotal,2)',
            'value' => '"$".$data->SubTotal',
            'header' => 'Total Item',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footer' => "<b>Total Estimado: $".$model->getSubTotal().'</b>',
            'footerHtmlOptions' => array('style' => 'text-align:right;'),

        ),
        array(
            'name' => 'Present.',
            'header' => 'x Bulto',
            'value' => '$data->oPaquete->cantidad',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),

    ),
)); ?>
