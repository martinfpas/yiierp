<?php

//TODO: TABS !!!!!
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/tabindex.js");
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
$cs->registerScriptFile($baseUrl."/js/shortcut.js");

$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'nota-pedido-form',
	'enableAjaxValidation'=>false,
));

Yii::app()->clientScript->registerScript('tooltip', "
    var editableOk;
    var prodReq;
	$(document).ready(function(){
        editableOk = isEditableOk();
        prodReq = 0;
	    //selectFieldOnFocus('#nota-pedido-form input[type=\"text\"]');	    
	    $('#NotaPedido_nroCombrobante').focus();	    				
		var modoEdicion = false;
		try{
			//$('label').tooltip();
			//$('input').tooltip();
		}catch(e){
			console.warn(e);
		}
		
		if($('#guardar').length == 1){
			//console.log('Modo edicion.');
			modoEdicion = true;
			//precargar();
			clSelected($model->idCliente);
			getProds();			
		}
		
		$('#NotaPedido_idCliente').change(function (ev) {
		    //console.log('#NotaPedido_idCliente');
		    clSelected($(this).val());
		});
		
		$('#NotaPedido_idCliente').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            //console.log(keycode);
                        
            // ABRE EL MODAL BUSCADOR DE CLIENTE CUANDO PRESIONA '+'
		    if(keycode == '43'){
                $('#modalBuscaClientes').modal();
                return false;
            }else{
                return true;
            }
		});
		
		// ENVIA ELIMINACION DE LA NOTA
		$('#descartarNota').click(function(e){
            if($('#NotaPedido_id').val() === undefined || $('#NotaPedido_id').val() == ''){
                document.location = '".Yii::app()->createUrl('/notaPedido/admin')."';
            };
            var datos = 'id='+$('#NotaPedido_id').val()+'&ajax=';
            var url = $(this).attr('url')+'&id='+$('#NotaPedido_id').val();
            $.ajax({
                url:url,
                type:'POST',
                data: datos,
                success:function(html) {
                  //TODO: SE HACE ALGO CON EL html?
                  //console.log(html);
                  document.location = '".Yii::app()->createUrl('/notaPedido/admin')."';
                  return true;
                },
                error:function(x,y,z) {
                  // TODO: MOSTRAR LOS ERRORES
                  console.error(x);
                  alert(x.responseText);
                  return false;
                }
            });
        });
        
		/// CHEQUEA SI LA NOTA DE PEDIDO EXISTE
		$('#NotaPedido_dig').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                var datos = 'nroCombrobante='+$('#NotaPedido_nroCombrobante').val()+'&dig='+$(this).val();
                //$('#nota-pedido-form').submit();
                $.ajax({
                    url: '".Yii::app()->createUrl('notaPedido/existe')."',
                    data: datos,
                    type: 'GET',
                    success: function(html){                        
                        if(html != '' ){
                            document.location = '".Yii::app()->createUrl('notaPedido/update',array('id'=>''))."'+html;
                        }                                        
                    },
                    error: function(x,y,z){
                        respuesta =  $.parseHTML(x.responseText);
                        errorDiv = $(respuesta).find('.errorSpan');
                        console.error($(errorDiv).parent().next());
                        var htmlError = $(errorDiv).parent().next().html();
                        $('#errorNotaPedido').html(htmlError);
                        $('#errorNotaPedido').show('slow');
                    } 
                });
            }
        })
		
		// EVITA QUE AL PRESIONAR ENTER EN CAMION SE PASE AL CAMPO DE FECHA
		$('#NotaPedido_camion').keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                $('#nota-pedido-form').submit();
            }
        })
       
        // ENVIA EL NUMERO DE CAMION PARA EMPEZAR UNA NUEVA NOTA Y REPETIR EL MISMO
        $('#finalizarYnueva').click(function(){
            document.location = ''+$(this).attr('url')+'&idCamion='+$('#NotaPedido_camion').val();
        });
        
        // ABRE EL ADMIN CUANDO FINALIZA LA NOTA
        $('#finalizarNota').click(function(){
            //document.location = '".Yii::app()->createUrl('notaPedido/view',array('id'=>''))."'+$('#NotaPedido_id').val();
            document.location = '".Yii::app()->createUrl('notaPedido/admin')."';
        });
		
		// ADMINISTRA EL ENVIO DEL FORMULARIO DE LA CABECERA DE LA NOTA		
		$('#nota-pedido-form').submit(function(e){
			e.preventDefault();
			
			$('#errorNotaPedido').hide('slow');
            $('#okNotaPedido').hide('slow');        
			
			
			let datos = $(this).serialize();
			$.ajax({
				url: '".Yii::app()->createUrl('notaPedido/SaveAsync')."',
				data: datos,
				type: 'POST',
				success: function(html){
					
					if(!modoEdicion){
					    window.location = '".Yii::app()->createUrl('notaPedido/update',array('id'=>''))."'+html;
						$('#NotaPedido_id').val(html);
						$('#nNotaDePedido').html(html);
						$('#cargarProductos').html('Guardar Cambios');
						getProds();
					}
									
				},
				error: function(x,y,z){
					respuesta =  $.parseHTML(x.responseText);
                    errorDiv = $(respuesta).find('.errorSpan');
                    
                    var htmlError = $(errorDiv).parent().next().html();
                    $('#errorNotaPedido').html(htmlError);
                    $('#errorNotaPedido').show('slow');
				} 
			});
			return false;
		});
	});
	
	
	// TODO: CHEQUEAR SI NO TIENE ERRORES VIEJOS
	function selectFieldOnFocus(input = 'input'){	    
	    // SELECCIONA EL CONTENIDO CUANDO RECIBE FOCO...
	    $('input').focus(function(){
            $(this).on('mouseup.a keyup.a', function(e){      
                $(this).off('mouseup.a keyup.a').select();
            });
        });
        
	}
	
	".((!$model->isNewRecord)? "
	
    // PRECARGA LOS DATOS DEL FORMULARIO CUANDO ES UNA EDICION
    function precargar(){
        //PRECARGA EL CAMPO CLIENTE
        $('#Cliente').val('".str_replace("'",'',$model->oCliente->razonSocial)."');
        //PRECARGA LA FICHA DEL CLIENTE
        clSelected(".$model->oCliente->id.");
    }
	" : '')."
	function getProds(){
		let id = $('#NotaPedido_id').val();
		if(id > 0){
			let datos = 'idNotaPedido='+id;
			$.ajax({
				url: '".Yii::app()->createUrl('prodNotaPedido/FormAsync')."',
				data: datos,
				type: 'GET',
				success: function(html){
				    prodReq = 1;
					$('#prods').html(html);
					//selectFieldOnFocus();					
				},
				error: function(x,y,z){
					console.warn(x);
					//TODO: MOSTRAR ERRORES
				}
			}).done(function(){
			    
			    if(!isEditableOk()){		        
			        handlerProdGridChange();
			    }else{			        
			        $('body').off('DOMSubtreeModified', '#prod-nota-pedido-grid');
			        //$('#prod-nota-pedido-grid').off('DOMSubtreeModified');
			        
			    }			    
			});
		}else{
			//TODO: MOSTRAR ERROR
		}
	};
	
	function gridItems(){
	    return $('[rel=\"ProdNotaPedido_precioUnit\"]').length;
	}
	
	function isEditableOk(){
	    //console.log('isEditableOk() '+editableOk);
	    //console.log('size '+$('[rel=\"ProdNotaPedido_precioUnit\"]').length);
	    
	    if(gridItems() > 0){
	        var linkPrecio = $('[rel=\"ProdNotaPedido_precioUnit\"]')[0];	        	  	    	    
            var cssClass = $(linkPrecio).prop('class');          
            if(cssClass.indexOf('editable') >= 0){
                editableOk = true;                
                $('body').off('DOMSubtreeModified', '#prod-nota-pedido-grid');                                   
            }else{
                editableOk = false;
            }    
	    }else{
	        editableOk = false;
	        if(prodReq == 0){                                        
                getProds();
            }else{
                console.log('handlerProdGridChange: getProds() NO LO VOY A CARGAR DE NUEVO');
            }
	    }
	    return editableOk;
        
	}
	
	function handlerProdGridChange(){
	    if(!isEditableOk()){
            $('body').on('DOMSubtreeModified', '#prod-nota-pedido-grid', function(event) {
            //$('#prod-nota-pedido-grid').on('DOMSubtreeModified', function(event) {
                //console.log('handlerProdGridChange:'+editableOk);
                event.stopImmediatePropagation();
                event.stopPropagation();
                //console.log('gridItems():'+gridItems());
                if(!editableOk){
                    if(prodReq == 0){                                        
                        getProds();
                    }else{
                        //console.log('handlerProdGridChange: getProds() NO LO VOY A CARGAR DE NUEVO');
                        location.reload();
                    }
                }                
                return true;                
            });
        }else{
            $('body').off('DOMSubtreeModified', '#prod-nota-pedido-grid');
            console.log('.off(DOMSubtreeModified');
            //$('#prod-nota-pedido-grid').off('DOMSubtreeModified');
            
        }    
	}

				
	function clSelected(param){
        $('#errorNotaPedido').hide('slow');
		$('#okNotaPedido').hide('slow');
				
		var datos = 'id='+param;
		$.ajax({
			url:'".$this->createUrl('Cliente/ViewFicha')."',
			type: 'Get',
            data: datos,
			success: function (html) {
               	$('#fichaCliente').html(html);
			},
			error: function (x,y,z){
				respuesta =  $.parseHTML(x.responseText);
				errorDiv = $(respuesta).find('.errorSpan');
				console.log($(errorDiv).parent().next());
				var htmlError = $(errorDiv).parent().next().html();
				$('#errorNotaPedido').html('Cliente No encontrado');
				$('#errorNotaPedido').show('slow');
				$('#NotaPedido_idCliente').focus();
			}
		});
	}		
",CClientScript::POS_READY);

?>



	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorNotaPedido"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okNotaPedido">Datos Guardados Correctamente !</div>
	<?php echo $form->hiddenField($model,'id')?>
<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">

	<div class="row-fluid">
		<div class="span2"><?php echo $form->textFieldRow($model,'nroCombrobante',array('class'=>'span12','maxlength'=>6,'tabindex' => 1)); ?></div>
		<div class="span1"><?php echo $form->textFieldRow($model,'dig',array('class'=>'span12','tabindex' => 2)); ?></div>
		<div class="span4">
			<?php //echo $form->labelEx($model,'idCliente',array('title'=>'Ingresar busqueda por Razon social, Nombre Fantasia o ID')); ?>

            <?php echo $form->textFieldRow($model,'idCliente',array('class'=>'span12','tabindex' => 3,'title' => 'Presionar "+" para abrir el buscador')); ?>

			<?php echo $form->error($model,'idCliente'); ?>
		</div>
		<div class="span3">
			<?php
				echo $form->labelEx($model,'camion');
				echo $form->dropDownList($model,'camion',CHtml::listData(Vehiculo::model()->findAll(),'id_vehiculo','IdNombre'),array('tabindex' => 4));
			?>
		</div>
		<div class="span2">
			<?php
				echo $form->labelEx($model,'fecha',array('title'=>'Hacer click en el campo y seleccionar la fecha'));
				$this->widget('zii.widgets.jui.CJuiDatePicker', array(
						'value'=> ComponentesComunes::fechaFormateada($model->fecha),
						'name'=>'NotaPedido[fecha]',
						'language'=>'es',
						'options'=>array(
								'showAnim'=>'fold',
								'monthRange'=>'-2:+2',
								'dateFormat'=>'dd-mm-yy',
								'changeYear' => true,
								'changeMonth' => true,
						),
						'htmlOptions'=>array(
								'style'=>'height:25px;',
								'class'=>'span12',
								'title'=>'Hacer click en el campo y seleccionar la fecha',
                                //'tabindex' => 5
						),
				));
			?>
		</div>

	</div>
	<div class="row-fluid" id="fichaCliente">

	</div>
</div>

<div class="" style="text-align: right;">
    <div class="content-fluid">
        <div class="row-fluid">
            <div class="span10">
            </div>
            <div class="span2">
                <?php
                $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=>$model->isNewRecord ? 'Cargar Productos' : 'Guardar',
                    'htmlOptions' => array(
                        'id' => $model->isNewRecord ? 'cargarProductos': 'guardar',
                    )
                )); ?>
            </div>
        </div>
    </div>

</div>

<?php $this->endWidget(); ?>
<div id='prods'>
</div>
<div class="form-actions" style="text-align: right;">
    <div class="content-fluid">
        <div class="row-fluid">
            <div class="span6">
                <?php
                    if($model->oCarga == null ) {
                        echo CHtml::link('Descartar Nota', '#', array('class' => 'bulk-button btn', 'id' => 'descartarNota', 'url' => Yii::app()->createUrl('notaPedido/delete'), 'style' => ''));
                    }
                ?>

            </div>
            <div class="span6">
                <?php
                    if($model->oCarga != null && $model->oCarga->estado != Carga::iBorrador){
                        echo CHtml::link('Finalizar','#',array('class'=>'bulk-button btn','id' => 'finalizar','url' => Yii::app()->createUrl('notaPedido/view',array('id'=>$model->id)),'style' => '' ));
                    }else{
                        echo CHtml::link('Finalizar','#',array('class'=>'bulk-button btn','id' => 'finalizarYnueva','url' => Yii::app()->createUrl('notaPedido/create'),'style' => '' ));
                    }

                ?>
            </div>

            <?php //echo CHtml::link('Finalizar','#',array('class'=>'bulk-button btn','id' => 'finalizarNota','url' => Yii::app()->createUrl('notaPedido/view'),'style' => '' )); ?>

        </div>
    </div>

</div>

<?php
$this->widget('BuscaCliente', array('js_comp'=>'#NotaPedido_idCliente','is_modal' => true));
?>

