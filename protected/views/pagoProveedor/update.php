<?php
$this->breadcrumbs=array(
	PagoProveedor::model()->getAttributeLabel('models') => array('admin'),
	PagoProveedor::model()->getAttributeLabel('model') => array('view','id'=>$model->id),
	'Modificando',
);

/*
*
'pagoCH' => $pagoCH,
'pagoEf' => $pagoEf,
'pagoMB' => $pagoMB,
*
 */

$this->menu=array(
	array('label'=>'Nuevo '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Ver '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de '.PagoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

	<h3>Modificando <?=PagoProveedor::model()->getAttributeLabel('model') ?> <?php echo $model->id; ?></h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<br>
<?php
$aPagoProvCheque=new CArrayDataProvider($model->aPagoProvCheque,array(
        'pagination'=> false,
    )
);
$aPagoProvEfectivo=new CArrayDataProvider($model->aPagoProvEfectivo,array(
        'pagination'=> false,
    )
);
$aPagoProvMovimientoBanc=new CArrayDataProvider($model->aPagoProvMovimientoBanc,array(
        'pagination'=> false,
    )
);
$aPagoProveedorChequePpio=new CArrayDataProvider($model->aPagoProveedorChequePpio,array(
        'pagination'=> false,
    )
);
?>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#cheque3Ro">Cheques de terceros</a></li>
    <li><a data-toggle="tab" href="#efectivo">Efectivo</a></li>
    <li><a data-toggle="tab" href="#chequePropio">Cheques propios </a></li>
    <li><a data-toggle="tab" href="#transferencia">Transferencias </a></li>
    <li><a data-toggle="tab" href="#facturas">Facturas Conciliadas </a></li>
</ul>
<div class="tab-content">
    <div id="cheque3Ro" class="tab-pane fade in active">
        <?php
        $this->renderPartial('_pagoCH',array(
                'aPagoProvCheque' => $aPagoProvCheque,
                'model' => $pagoCH,
            )
        );
        ?>
    </div>
    <div id="efectivo" class="tab-pane fade">
        <?php
            $this->renderPartial('_pagoEF',array(
                    'aPagoProvEfectivo' => $aPagoProvEfectivo,
                    'model' => $pagoEf,

                )
            );
        ?>
    </div>
    <div id="chequePropio" class="tab-pane fade">
        <?php
        $this->renderPartial('_pagoChPpio',array(
                'aPagoProveedorChequePpio' => $aPagoProveedorChequePpio,
                'model' => $pagoCHPpio,
            )
        );
        ?>
    </div>
    <div id="transferencia" class="tab-pane fade">
        <?php
        $this->renderPartial('_pagoMB',array(
                'aPagoProvMovimientoBanc' => $aPagoProvMovimientoBanc,
                'model' => $pagoMB,
            )
        );
        ?>
    </div>
    <div id="facturas" class="tab-pane fade">
        <?php
        $this->renderPartial('_facturas', array(
            'aPago2FactProv' => $aPago2FactProv,
            'oPago2FactProv' => $oPago2FactProv,
        ));

        ?>
    </div>
</div>