<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10/11/2019
 * Time: 23:57
 */
$cs = Yii::app()->getClientScript();
$cs->registerScript('pagoProvCh3Ro',"$('#agregarCh3ro').click(function(e){
        e.preventDefault();
        ele = $('#MovimientoCuentasBanc_idsCheques3Ro');
        console.log($('#ChequeDeTerceros_nroCheque').select2('data').id);
        let datos = 'PagoProvCheque[idCheque3ro]='+$('#ChequeDeTerceros_nroCheque').select2('data').id+'&PagoProvCheque[idPagoProv]=".$model->idPagoProv."&PagoProvCheque[monto]='+$('#ChequeDeTerceros_nroCheque').select2('data').importe;
        $.ajax({
            url: '".Yii::app()->createUrl('PagoProvCheque/SaveAsync')."',
            type: 'POST',
            data: datos,
            success: function(html){                
                $.fn.yiiGridView.update('pago-prov-cheque-grid');
                $('#ChequeDeTerceros_id_banco').select2('val','');
                $('#ChequeDeTerceros_nroCheque').select2('val','');
            },
            error: function(x,y,z){
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                var htmlError = $(errorDiv).parent().next().html();
                alert(htmlError);                
            },
            
        });        
        return false;
        //alert();
    });
    
");
?>

<div class="row-fluid">
    <div class="span10">
        <div class="row-fluid" id="cheques" class="cheques" style="">
            <div class="span8 bordeRedondo deTerceros" id="deTerceros" style="">
                <?php
                $modelCheque3ro = new ChequeDeTerceros();
                $formCheque3ro=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                    'id'=>'cheque-de-terceros-form',
                    'enableAjaxValidation'=>false,
                )); ?>
                <div class="span5">
                    <?php
                    echo $formCheque3ro->textFieldRow($modelCheque3ro,'id_banco',array('class'=>'span12'));
                    $this->widget('ext.select2.ESelect2',array(
                            'selector'=>'#ChequeDeTerceros_id_banco',
                            'options'=>array(
                                'elementTabIndex' => 1,
                                'openOnEnter' => false,
                                'ajax'=>array(
                                    'url'=>Yii::app()->createUrl('banco/select2'),
                                    'dataType'=>'json',
                                    'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                                    'results'=>"js:function(data,page) { return {results: data}; }",
                                ),
                                'initSelection' => 'function (element, callback) {
                                                        var data = {id: element.val(), text: ChequeDeTerceros_id_banco};
                                                        console.log(data);
                                                        callback(data);
                                     }',
                                'class' => 'span12',
                                'tabindex' => 1,
                            ),
                        )
                    );
                    ?>
                </div>
                <div class="span5">
                    <?php
                    echo $formCheque3ro->textFieldRow($modelCheque3ro,'nroCheque',array('class'=>'span12'));
                    $this->widget('ext.select2.ESelect2',array(
                            'selector'=>'#ChequeDeTerceros_nroCheque',
                            'options'=>array(
                                'elementTabIndex' => 2,
                                'openOnEnter' => false,
                                'ajax'=>array(
                                    'url'=>Yii::app()->createUrl('chequeDeTerceros/select2'),
                                    'dataType'=>'json',
                                    'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true,id_banco: $("#ChequeDeTerceros_id_banco").val()}; }',
                                    'results'=>'js:function(data,page) { return {results: data}; }',
                                ),
                                'initSelection' => 'function (element, callback) {
                                                        var data = {id: element.val(), text: ChequeDeTerceros_nroCheque};
                                                        console.log(data);
                                                        callback(data);
                                     }',
                                'class' => 'span12',
                                'tabindex' => 2,
                            ),
                        )
                    );
                    ?>
                </div>
                <div class="span2">
                    <input class="btn btn-primary" id="agregarCh3ro" type="submit" value="Agregar" style="margin-top:25px">
                </div>

                <?php $this->endWidget(); ?>

            </div>
        </div>
    </div>

</div>

<div class="row-fluid">
    <div class="span12">

        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'pago-prov-cheque-grid',
            'dataProvider'=>$aPagoProvCheque,
            'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
            'columns'=>array(
                array(
                    'name' => 'oCheque3ro.oCliente',
                    'header' => 'Cliente',
                    'value' => '$data->oCheque3ro->oCliente->Title',
                ),
                array(
                    'name' => 'oCheque3ro.oBanco.nombre',
                    'header' => 'Banco',
                    'value' => '$data->oCheque3ro->oBanco->nombre',
                ),
                array(
                    'name' => 'oCheque3ro.nroCheque',
                    'header' => 'nro',
                    'value' => '$data->oCheque3ro->nroCheque',
                ),
                array(
                    'name' => 'monto',
                    'value' => '"$".number_format($data->monto,2,",",".")',
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                    'buttons'=>array(
                        'delete' => array(
                            'label' => 'Borrar Item',
                            'url'=>'Yii::app()->controller->createUrl("PagoProvCheque/delete", array("id"=>$data->id))',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>
