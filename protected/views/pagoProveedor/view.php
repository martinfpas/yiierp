<?php
/**
 * @var $model PagoProveedor
 * @var $aPagoProvMovimientoBanc PagoProvMovimientoBanc
 * @var $aPagoProvCheque PagoProvCheque
 */
$this->breadcrumbs=array(
	PagoProveedor::model()->getAttributeLabel('models') =>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
	array('label'=>'Modificar '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de '.PagoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);

?>

<h3>Ver <?=PagoProveedor::model()->getAttributeLabel('model') ?> #<?php echo $model->id; ?></h3>

<div class="container-fluid">
    <div class="row-fluid">
        <div class="span6">
            <?php

            $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'name' => 'monto',
                            'value' => "$".number_format($model->monto,2,',','.'),
                        ),
                        array(
                            'name' => 'fecha',
                            'value' => ComponentesComunes::fechaFormateada($model->fecha),
                        ),
                        /*
                        array(
                            'type' => 'raw',
                            'name' => 'Factura',
                            'value' => ($model->oPago2Factura != null && $model->oPago2Factura->oComprobanteProveedor != null)?
                                CHtml::link(Yii::app()->createUrl('ComprobanteProveedor/view',array('id' => $model->oPago2Factura->oComprobanteProveedor->Nro_Comprobante))) : '',
                            'visible' => ($model->oPago2Factura != null && $model->oPago2Factura->oComprobanteProveedor != null),
                        ),
                        */
                    ),
                )
            ); ?>
        </div>
        <div class="span6">
            <?php $this->widget('bootstrap.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                        array(
                            'name' => 'idProveedor',
                            'value' => $model->oProveedor->Title,
                        ),
                        array(
                            'name' => 'estado',
                            'value' => PagoProveedor::$aEstado[$model->estado],
                        ),
                    ),
                )
            ); ?>
        </div>
    </div>

    <?php
        echo CHtml::link('Imprimir',Yii::app()->createUrl('pagoProveedor/viewPdf',array('id'=> $model->id)),array('class' => 'btn btn-primary'));
    ?>

    <br>
    <?php
    $aPagoProvCheque=new CArrayDataProvider($model->aPagoProvCheque,array(
            'pagination'=> false,
        )
    );
    $aPagoProvEfectivo=new CArrayDataProvider($model->aPagoProvEfectivo,array(
            'pagination'=> false,
        )
    );
    $aPagoProvMovimientoBanc=new CArrayDataProvider($model->aPagoProvMovimientoBanc,array(
            'pagination'=> false,
        )
    );
    $aPagoProveedorChequePpio=new CArrayDataProvider($model->aPagoProveedorChequePpio,array(
            'pagination'=> false,
        )
    );

    $aFacturas = array();
    if (sizeof($model->aPago2FactProv) > 0){
        $aPago2FactProv = new Pago2FactProv('search');
        $aPago2FactProv->unsetAttributes();
        $aPago2FactProv->idPagoProveedor = $model->id;
    ?>
    <div class="row-fluid ">
        <div class="span12 bordeRedondo">
            <h5>Facturas Canceladas</h5>
            <?php
            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago2-fact-prov-grid',
                'dataProvider'=>$aPago2FactProv->search(),
                'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
                'template' => '{items}',
                'columns'=>array(
                    'oComprobanteProveedor.Nro_Comprobante',
                    array(
                        'name' => 'oComprobanteProveedor.TipoComprobante',
                        'value' => 'ComprobanteProveedor::$aClase[$data->oComprobanteProveedor->TipoComprobante]',
                        'filter' => ComprobanteProveedor::$aClase,
                        //'header' => 'Cuenta',
                        //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    ),
                    array(

                        'name' => 'oComprobanteProveedor.id_Proveedor',
                        'value' => '$data->oComprobanteProveedor->oProveedor->Title',
                        'header' => 'Proveedor',
                        /*
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                        */
                    ),

                    array(
                        'name' => 'oComprobanteProveedor.TotalNeto',
                        'value' => 'number_format($data->oComprobanteProveedor->TotalNeto,2)',
                        'header' => 'Importe',
                        'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    ),

                    array(
                        'name' => 'oComprobanteProveedor.montoPagado',
                        'value' => 'number_format($data->oComprobanteProveedor->montoPagado,2)',
                        //'header' => 'montoPagado',
                        'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    ),


                    array(
                        'name' => 'oComprobanteProveedor.FechaFactura',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oComprobanteProveedor->FechaFactura)',
                        'header' => 'Fecha',
                        'htmlOptions' => array('style' => 'width:80px'),
                        'headerHtmlOptions' => array('style' => 'width:80px'),
                    ),

                ),
            )); ?>
        </div>
    </div>
    <?php
    }
    //$aFacturas =

    ?>
    <div class="row-fluid ">
        <h4>Medios:</h4>
        <div class="span12 bordeRedondo" style="<?=(sizeof($model->aPagoProvCheque) < 1)? 'display:none;' : ''?>">
            <span>Cheques:</span>
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago-prov-cheque-grid',
                'dataProvider'=>$aPagoProvCheque,
                'columns'=>array(
                    array(
                        'name' => 'oCheque3ro.oCliente',
                        'header' => 'Cliente',
                        'value' => '$data->oCheque3ro->oCliente->Title',
                    ),
                    array(
                        'name' => 'oCheque3ro.oBanco.nombre',
                        'header' => 'Banco',
                        'value' => '$data->oCheque3ro->oBanco->nombre',
                    ),
                    array(
                        'name' => 'oCheque3ro.nroCheque',
                        'header' => 'nro',
                        'value' => '$data->oCheque3ro->nroCheque',
                    ),
                    array(
                        'name' => 'monto',
                        'value' => '"$".number_format($data->monto,2,",",".")',
                    ),
                ),
            )); ?>
        </div>
        <div class="span12 bordeRedondo" style="<?=(sizeof($model->aPagoProveedorChequePpio) < 1)? 'display:none;' : ''?>margin-left: 0px;margin-top: 5px;">
            <span>Cheques Propios:</span>
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago-proveedor-cheque-ppio-grid',
                'dataProvider'=>$aPagoProveedorChequePpio,
                'columns'=>array(

                    //'oChequePpio.',
                    //'monto',

                    array(
                        'name' => 'oChequePpio.nroCheque',
                        'header' => 'nroCheque',
                        'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                    ),
                    array(
                        'name' => 'oChequePpio.fecha',
                        'header' => 'fecha',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oChequePpio->fecha)',
                        'htmlOptions' => array('style' => 'width:80px;'),
                        'headerHtmlOptions' => array('style' => 'width:80px;'),
                    ),
                    array(
                        'name' => 'oChequePpio.id_cuenta',
                        'header' => 'cuenta',
                        'value' => '$data->oChequePpio->oCuenta->numero',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                        'filter' => CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),
                    ),
                    array(
                        'name' => 'oChequePpio.importe',
                        'header' => 'importe',
                        'value' => '"$".number_format($data->oChequePpio->importe,2)',
                        'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                        'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
                    ),
                    array(
                        'name' => 'oChequePpio.fechaDebito',
                        'header' => 'fecha debito',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oChequePpio->fechaDebito)',
                        'htmlOptions' => array('style' => 'width:100px;'),
                        'headerHtmlOptions' => array('style' => 'width:100px;'),
                    ),

                ),
            )); ?>
        </div>
        <div class="span6 bordeRedondo" style="<?=(sizeof($model->aPagoProvEfectivo) < 1)? 'display:none;' : ''?>margin-left:0px;margin-top: 5px;">
            <span>Efectivo:</span>
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago-prov-efectivo-grid',
                'dataProvider'=>$aPagoProvEfectivo,
                'columns'=>array(
                    array(
                        'name' => 'monto',
                        'value' => '"$".number_format($data->monto,2,",",".")',
                    ),
                ),
            )); ?>
        </div>

        <div class="span6 bordeRedondo" style="<?=(sizeof($model->aPagoProvMovimientoBanc) < 1)? 'display:none;' : ''?>margin-left:0px;margin-top: 5px;">
            <span>Movimientos Bancarios:</span>
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago-prov-movimiento-banc-grid',
                'dataProvider'=>$aPagoProvMovimientoBanc,
                'columns'=>array(
                    array(
                        'name' => 'oMovBanc.oTipoMovimiento.Descripcion',
                        'header' => 'Tipo',
                        'value' => '$data->oMovBanc->oTipoMovimiento->Descripcion',
                    ),
                    array(
                        'name' => 'oMovBanc.fechaMovimiento',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oMovBanc->fechaMovimiento)',
                        'header' => 'Fecha Movimiento',
                    ),
                    array(
                        'name' => 'monto',
                        'value' => '"$".number_format($data->monto,2,",",".")',
                    ),
                    array(
                        'name' => 'oMovBanc.oCuenta.numero',
                        'header' => 'Cuenta',
                    ),
                    /*
                    array(
                        'name' => '',
                        'value' => '',
                        'header' => '',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>
</div>



