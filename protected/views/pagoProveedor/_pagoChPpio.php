<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10/11/2019
 * Time: 23:58
 */

$cs = Yii::app()->getClientScript();
$cs->registerScript('pagoProvChPpio',"$('#agregarChPpio').click(function(e){
        e.preventDefault();        
        console.log($('#ChequePropio_nroCheque').select2('data').id);
        
        let datos = 'PagoProveedorChequePpio[idChequePpio]='+$('#ChequePropio_nroCheque').select2('data').id+'&PagoProveedorChequePpio[idPagoProv]=".$model->idPagoProv."&PagoProveedorChequePpio[monto]='+$('#ChequePropio_nroCheque').select2('data').importe;
        $.ajax({
            url: '".Yii::app()->createUrl('PagoProveedorChequePpio/SaveAsync')."',
            type: 'POST',
            data: datos,
            success: function(html){                
                $.fn.yiiGridView.update('pago-prov-cheque-ppio-grid');
                $('#ChequePropio_id_cuenta').select2('val','');
                $('#ChequePropio_nroCheque').select2('val','');
            },
            error: function(x,y,z){
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                var htmlError = $(errorDiv).parent().next().html();
                alert(htmlError);                
            },
            
        }); 
               
        return false;
        //alert();
    });
    
");
?>

<div class="row-fluid">
    <div class="span8 bordeRedondo " id="propios" style="">

        <?php
        $modelChequePropio = new ChequePropio();
        $formChequePropio = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id' => 'cheque-propio-form',
            'enableAjaxValidation'=>false,
        ));
        echo $formChequePropio->hiddenField($modelChequePropio,'id_cuenta');
        ?>
        <div class="span8 " style="">
            <?php
            echo $formChequePropio->textFieldRow($modelChequePropio,'nroCheque',array('class'=>'span12'));
            $this->widget('ext.select2.ESelect2',array(
                    'selector'=>'#ChequePropio_nroCheque',
                    'options'=>array(
                        'ajax'=>array(
                            'url'=>Yii::app()->createUrl('ChequePropio/select2'),
                            'dataType'=>'json',
                            'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true,id_cuenta: $("#MovimientoCuentasBanc_idCuenta").val()}; }', /*,sinPagos:true*/
                            'results'=>"js:function(data,page) { return {results: data}; }",
                        ),
                        'initSelection' => 'function (element, callback) {
                                                            var data = {id: element.val(), text: ChequePropio_nroCheque,partial: true,id_cuenta: $("#MovimientoCuentasBanc_idCuenta").val(),sinPagos:true};
                                                            console.log(data);
                                                            callback(data);
                                         }',
                        'class' => 'span12',
                        'tabindex' => 1,
                    ),
                )
            );

            ?>
        </div>
        <div class="span2">
            <input class="btn btn-primary" id="agregarChPpio" type="submit" value="Agregar" style="margin-top:25px">
        </div>
        <?php $this->endWidget(); ?>
    </div>


    <div class="span12" style="margin-left:10px;margin-top: 5px;">

        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'pago-prov-cheque-ppio-grid',
            'dataProvider'=>$aPagoProveedorChequePpio,
            'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
            'columns'=>array(

                array(
                    'name' => 'oChequePpio.nroCheque',
                    'header' => 'Numero',
                    'value' => '$data->oChequePpio->nroCheque',
                ),
                array(
                    'name' => 'oChequePpio.importe',
                    'header' => 'Importe',
                    'value' => '"$".number_format($data->oChequePpio->importe,2,",",".")',
                ),
                array(
                    'name' => 'oChequePpio.fecha',
                    'header' => 'fecha',
                    'value' => 'ComponentesComunes::fechaFormateada($data->oChequePpio->fecha)',
                ),
                array(
                    'name' => 'oChequePpio.oCuenta.Title',
                    'header' => 'Cuenta',
                    'value' => '$data->oChequePpio->oCuenta->Title',
                ),

                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                    'buttons'=>array(
                        'delete' => array(
                            'label' => 'Borrar Item',
                            'url'=>'Yii::app()->controller->createUrl("PagoProveedorChequePpio/delete", array("id"=>$data->id))',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>