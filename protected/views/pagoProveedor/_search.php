

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
    )); ?>
        <div class="content-fluid">
            <div class="row-fluid">
                <div class='span1'>
                    <?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>
                </div>
                <div class='span5'>
                    <?php
                    echo $form->labelEx($model,'idProveedor');
                    echo $form->dropDownList($model,'idProveedor',CHtml::listData(Proveedor::model()->todos()->findAll(),'id','title'),array('class'=>'span12'));
                    ?>
                </div>
                <div class='span2'>
                    <?php echo $form->textFieldRow($model,'monto',array('class'=>'span12')); ?>
                </div>
                <div class='span2'>
                    <?php echo $form->textFieldRow($model,'fecha',array('class'=>'span12')); ?>
                </div>
                <div class='span2'>
                    <?php $this->widget('bootstrap.widgets.TbButton', array(
                        'buttonType' => 'submit',
                        'type'=>'primary',
                        'label'=>Yii::t('application','Buscar'),
                        'htmlOptions' => array(
                            'style' => 'margin-top:25px;'
                        )
                    )); ?>
                </div>
            </div>
        </div>
    <?php $this->endWidget(); ?>

