<?php
/**
 * @var $model PagoProveedor
 * @var $aPagoProvMovimientoBanc PagoProvMovimientoBanc
 * @var $aPagoProvCheque PagoProvCheque
 */

?>
<style>
/*
#pago-prov-cheque-grid tr, #pago-prov-cheque-grid td, #pago-prov-cheque-grid th {
    border: 1px solid black;
}
*/
tr, td, th {
    border: 1px solid black;
}
</style>
<div class="bordeRedondo">
    <div style="text-align: center">
        <h5><b>ERCA S.R.L.</b></h5>
        <h5>Ruta Provincial Nº 2 Altura 2350 - (3014) Monte Vera – Provincia de Santa Fe</h5>
        <h5>Telefax: 0342-4904299 Teléfono: 0342-4904164 - e-mail: daubysrl@gmail.com</h5>
        <h2 style="color:red">dauby  El Sabor Justo…</h2>
    </div>
<br>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span9" style="font-weight: bold">
                <span>Señor/es</span><br>
                <span><?=$model->oProveedor->Title?></span>
            </div>
            <div class="span3" style="font-weight: bold">
                Fecha: <?=ComponentesComunes::fechaFormateada($model->fecha)?>
            </div>
        </div>
    </div>
    <br>
    <br>
    <span>De nuestra mayor consideración:</span><br>
    <span>Adjuntamos a la presente los siguientes pagos:</span><br>


    <?php
    if (sizeof($model->aPagoProvCheque) > 0){
        $aPagoProvCheque=new CArrayDataProvider($model->aPagoProvCheque,array(
                'pagination'=> false,
            )
        );
        ?>
    <div class="row-fluid" style="margin-top: 25px;" >
        <div class="span12">
            <span>Cheques de Teceros:</span>
            <?php $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago-prov-cheque-grid',
                'dataProvider'=>$aPagoProvCheque,
                'template' => '{items}',
                'columns'=>array(
                    array(
                        'name' => 'oCheque3ro.oBanco.nombre',
                        'header' => 'Banco',
                        'value' => '$data->oCheque3ro->oBanco->nombre',
                    ),
                    array(
                        'name' => 'oCheque3ro.oBanco.nombre',
                        'header' => 'Banco',
                        'value' => '$data->oCheque3ro ->oBanco->nombre',
                    ),
                    array(
                        'name' => 'oCheque3ro.nroCheque',
                        'header' => 'nro',
                        'value' => '$data->oCheque3ro->nroCheque',
                        'footer' => "<b>Total: </b>",
                    ),
                    array(
                        'name' => 'monto',
                        'value' => '"$".number_format($data->monto,2,",",".")',
                        'footer' => "<b>$".number_format($model->TotalCH3ros,2,",",".")."</b>",
                        'footerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),

                    ),
                ),
            )); ?>
        </div>
    </div>
    <?php
    }
    ?>

    <?php
    if (sizeof($model->aPagoProveedorChequePpio) > 0){
        $aPagoProveedorChequePpio=new CArrayDataProvider($model->aPagoProveedorChequePpio,array(
                'pagination'=> false,
            )
        );
        ?>
        <div class="row-fluid" style="margin-top: 25px;" >
            <div class="span12">
                <span>Cheques:</span>
                <?php $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'pago-proveedor-cheque-ppio-grid',
                    'dataProvider'=>$aPagoProveedorChequePpio,
                    'template' => '{items}',
                    'columns'=>array(
                        array(
                            'name' => 'oChequePpio.nroCheque',
                            'header' => 'nroCheque',
                            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                            'headerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                        ),
                        array(
                            'name' => 'oChequePpio.fechaDebito',
                            'header' => 'fecha',
                            'value' => '($data->oChequePpio->fechaDebito != null)? ComponentesComunes::fechaFormateada($data->oChequePpio->fechaDebito) : ""',
                            'htmlOptions' => array('style' => 'width:80px;'),
                            'headerHtmlOptions' => array('style' => 'width:80px;'),
                        ),
                        array(
                            'name' => 'oChequePpio.id_cuenta',
                            'header' => 'cuenta',
                            'value' => '$data->oChequePpio->oCuenta->numero',
                            'htmlOptions' => array('style' => ''),
                            'headerHtmlOptions' => array('style' => ''),
                            'filter' => CHtml::listData(CuentasBancarias::model()->findAll(),'id','title'),
                            'footer' => "<b>Total: </b>",
                        ),
                        array(
                            'name' => 'oChequePpio.importe',
                            'header' => 'importe',
                            'value' => '"$".number_format($data->oChequePpio->importe,2)',
                            'htmlOptions' => array('style' => 'width:100px;text-align:right;'),
                            'headerHtmlOptions' => array('style' => 'width:100px;text-align:center;'),
                            'footer' => "<b>$".number_format($model->TotalChequesPpios,2,",",".")."</b>",
                            'footerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                        ),
                    ),
                )); ?>
            </div>
        </div>
        <?php
    }
    ?>

    <?php
    if (sizeof($model->aPagoProvMovimientoBanc) > 0){
        $aPagoProvMovimientoBanc=new CArrayDataProvider($model->aPagoProvMovimientoBanc,array(
                'pagination'=> false,
            )
        );
        ?>
        <div class="row-fluid" style="margin-top: 25px;" >
            <div class="span12">
                <span>Movimientos Bancarios:</span>
                <?php $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'pago-prov-movimiento-banc-grid',
                    'dataProvider'=>$aPagoProvMovimientoBanc,
                    'template' => '{items}',
                    'columns'=>array(
                        array(
                            'name' => 'oMovBanc.oTipoMovimiento.Descripcion',
                            'header' => 'Tipo',
                            'value' => '$data->oMovBanc->oTipoMovimiento->Descripcion',
                        ),
                        array(
                            'name' => 'oMovBanc.fechaMovimiento',
                            'value' => 'ComponentesComunes::fechaFormateada($data->oMovBanc->fechaMovimiento)',
                            'header' => 'Fecha Movimiento',
                            'footer' => "<b>Total: </b>",
                        ),
                        array(
                            'name' => 'monto',
                            'value' => '"$".number_format($data->monto,2,",",".")',
                            'footer' => "<b>$".number_format($model->TotalCH3ros,2,",",".")."</b>",
                            'footerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                        ),
                        array(
                            'name' => 'oMovBanc.oCuenta.numero',
                            'header' => 'Cuenta',
                        ),
                    ),
                )); ?>
            </div>
        </div>
        <?php
    }
    ?>
    <?php
    if (sizeof($model->aPagoProvEfectivo) > 0){
        $aPagoProvMovimientoBanc=new CArrayDataProvider($model->aPagoProvEfectivo,array(
                'pagination'=> false,
            )
        );
        ?>
        <div class="row-fluid" style="margin-top: 25px;" >
            <div class="span12">
                <span>Efectivo:</span>
                <?php $this->widget('bootstrap.widgets.TbGridView',array(
                    'id'=>'pago-prov-movimiento-banc-grid',
                    'dataProvider'=>$aPagoProvMovimientoBanc,
                    'template' => '{items}',
                    'columns'=>array(

                        array(
                            'name' => 'monto',
                            'value' => '"$".number_format($data->monto,2,",",".")',
                        ),
                    ),
                )); ?>
            </div>
        </div>
        <?php
    }
    ?>

    <h4><b>Total: <?="$".number_format($model->monto,2,",",".")?></b></h4>
    <h4><b>Importe total que agradecemos aplicar a la cancelación de los siguientes comprobantes</b></h4>

    <div class="row-fluid ">
        <div class="span12">
            <?php
            /** @var $aPago2FactProv Pago2FactProv[] */
            /** @var $oPago2FactProv Pago2FactProv */
            $totalFacturas = 0;
            $xPago2FactProv = Pago2FactProv::model()->findAll($aPago2FactProv->search()->criteria);
            foreach ($xPago2FactProv as $index => $oPago2FactProv) {
                $totalFacturas += ($oPago2FactProv->oComprobanteProveedor != null)? $oPago2FactProv->oComprobanteProveedor->TotalNeto : 0;
            }



            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'pago2-fact-prov-grid',
                'dataProvider'=>$aPago2FactProv->search(),
                'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
                'template' => '{items}',
                'columns'=>array(
                    array(
                        'name' => 'oComprobanteProveedor.TipoComprobante',
                        'value' => 'ComprobanteProveedor::$aClase[$data->oComprobanteProveedor->TipoComprobante]',
                        'filter' => ComprobanteProveedor::$aClase,
                        'header' => 'Comprobante',
                        //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                    ),
                    array(
                        'name' => 'oComprobanteProveedor.Nro_Comprobante',
                        'header' => 'Número',
                    ),

                    array(
                        'name' => 'oComprobanteProveedor.FechaFactura',
                        'value' => 'ComponentesComunes::fechaFormateada($data->oComprobanteProveedor->FechaFactura)',
                        'header' => 'Fecha',
                        'htmlOptions' => array('style' => 'width:80px'),
                        'headerHtmlOptions' => array('style' => 'width:80px'),
                        'footer' => "<b>Total: </b>",
                    ),

                    array(
                        'name' => 'oComprobanteProveedor.TotalNeto',
                        'value' => 'number_format($data->oComprobanteProveedor->TotalNeto,2)',
                        'header' => 'Importe',
                        'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
                        'footer' => "<b>$".number_format($totalFacturas,2,",",".")."</b>",
                        'footerHtmlOptions' => array('style' => 'width:100px;text-align:right;'),
                    ),

                    /*
                    array(
                        'name' => '',
                        'value' => '',
                        'header' => '',
                        'htmlOptions' => array('style' => ''),
                        'headerHtmlOptions' => array('style' => ''),
                    ),
                    */
                ),
            )); ?>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12" style="">
                <h5>A la espera del correspondiente recibo oficial, aprovechamos la oportunidad para saludarlo muy atentamente.</h5>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12" style="text-align: right">
                <h5>ERCA SRL</h5>
            </div>
        </div>
    </div>
</div>
<div class="span12" style="text-align: right">
