<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10/11/2019
 * Time: 23:58
 *
 * @var $oPago2FactProv Pago2FactProv
 */

$cs = Yii::app()->getClientScript();
$cs->registerScript('agregarFP',"$('#agregarFP').click(function(e){
        e.preventDefault();        
        console.log($('#Pago2FactProv_Nro_Comprobante').select2('data').id);
        
        let datos = 'Pago2FactProv[idComprobanteProveedor]='+$('#Pago2FactProv_idComprobanteProveedor').select2('data').id+'&Pago2FactProv[idPagoProveedor]=".$oPago2FactProv->idPagoProveedor."';
        $.ajax({
            url: '". Yii::app()->createUrl('Pago2FactProv/SaveAsync')."',
            type: 'POST',
            data: datos,
            success: function(html){                
                $.fn.yiiGridView.update('pago2-fact-prov-grid');
                $('#Pago2FactProv_idComprobanteProveedor').select2('val','');                
            },
            error: function(x,y,z){
                //TODO: HANDLE ERROR
            },            
        });                
        return false;
        //alert();
    });
    
");
?>

<div class="row-fluid">
    <div class="span8 bordeRedondo " id="propios" style="">

        <?php

        $formFP = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id' => 'factura-proveedor-form',
            'enableAjaxValidation'=>false,
        ));

        ?>
        <div class="span8 " style="">
            <?php
            echo $formFP->textFieldRow($oPago2FactProv,'idComprobanteProveedor',array('class'=>'span12'));
            $this->widget('ext.select2.ESelect2',array(
                    'selector'=>'#Pago2FactProv_idComprobanteProveedor',
                    'options'=>array(
                        'ajax'=>array(
                            'url'=>Yii::app()->createUrl('ComprobanteProveedor/select2'),
                            'dataType'=>'json',
                            'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true,id_Proveedor: '.$oPago2FactProv->oPagoProveedor->idProveedor.',condImpaga:true}; }',
                            'results'=>"js:function(data,page) { return {results: data}; }",
                        ),
                        'initSelection' => 'function (element, callback) {
                                                var data = {id: element.val(), text: Pago2FactProv_Nro_Comprobante};
                                                console.log(data);
                                                callback(data);
                         }',
                        'class' => 'span12',
                        'tabindex' => 1,
                    ),
                )
            );

            ?>
        </div>
        <div class="span2">
            <input class="btn btn-primary" id="agregarFP" type="submit" value="Agregar" style="margin-top:25px">
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<?php 
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'pago2-fact-prov-grid',
    'dataProvider'=>$aPago2FactProv->search(),
    'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
    'columns'=>array(
        'oComprobanteProveedor.Nro_Comprobante',
        array(
            'name' => 'oComprobanteProveedor.TipoComprobante',
            'value' => 'ComprobanteProveedor::$aClase[$data->oComprobanteProveedor->TipoComprobante]',
            'filter' => ComprobanteProveedor::$aClase,
            //'header' => 'Cuenta',
            //'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            //'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),
        array(

            'name' => 'oComprobanteProveedor.id_Proveedor',
            'value' => '$data->oComprobanteProveedor->oProveedor->Title',
            'header' => 'Proveedor',
            /*
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            */
        ),

        array(
            'name' => 'oComprobanteProveedor.TotalNeto',
            'value' => '"$".number_format($data->oComprobanteProveedor->TotalConFactor,2,",",".")',
            'header' => 'Importe',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),

        array(
            'name' => 'oComprobanteProveedor.montoPagado',
            'value' => 'number_format($data->oComprobanteProveedor->montoPagado,2)',
            //'header' => 'montoPagado',
            'htmlOptions' => array('style' => 'text-align:right;width:100px;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;width:100px;'),
        ),


        array(
            'name' => 'oComprobanteProveedor.FechaFactura',
            'value' => 'ComponentesComunes::fechaFormateada($data->oComprobanteProveedor->FechaFactura)',
            'header' => 'Fecha',
            'htmlOptions' => array('style' => 'width:80px'),
            'headerHtmlOptions' => array('style' => 'width:80px'),
        ),
        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => ' {delete}',
            'buttons'=>array(

                'delete' => array(
                    'label' => 'Desvincular Factura',
                    'url'=>'Yii::app()->controller->createUrl("Pago2FactProv/delete", array("id"=>$data->id))',
                ),
            ),
        ),
    ),
)); ?>