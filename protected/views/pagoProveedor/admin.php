<?php
/* @var $this PagoProveedorController */
/* @var $model PagoProveedor */

$this->breadcrumbs=array(
	PagoProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Nuevo '.PagoProveedor::model()->getAttributeLabel('model'), 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle('slow');
	return false;
});
$('.search-form form').submit(function(){
	$('#pago-proveedor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h3>Gestion de <?=PagoProveedor::model()->getAttributeLabel('models') ?></h3>

<p>
Usted puede utilizar los operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) al principio de cada valor.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'pago-proveedor-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
        array(
            'name' => 'id',
            'htmlOptions' => array('style' => 'width:50px;'),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'monto',
            'value' => '"$".number_format($data->monto,2,".",",")',
            'htmlOptions' => array('style' => 'text-align:right;'),
            'headerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'fecha',
            'value' => 'ComponentesComunes::fechaFormateada($data->fecha)',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        array(
            'name' => 'idProveedor',
            'value' => '$data->oProveedor->title',
            'header' => 'Proveedor',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            'filter' => CHtml::listData(Proveedor::model()->todos()->findAll(),'id','title'),
        ),
        array(
            'name' => 'estado',
            'value' => 'PagoProveedor::$aEstado[$data->estado]',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
            'filter' => PagoProveedor::$aEstado,
        ),


        /*
        array(
            'name' => '',
            'value' => '',
            'header' => '',
            'htmlOptions' => array('style' => ''),
            'headerHtmlOptions' => array('style' => ''),
        ),
        */
		
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
