
/* @var $this PagoProveedorController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'PagoProveedor::model()->getAttributeLabel('models')',
);

$this->menu=array(
	array('label'=>'Nuevo PagoProveedor', 'url'=>array('create')),
	array('label'=>'Gestion de PagoProveedor', 'url'=>array('admin')),
);
?>


<h3>$label</h3>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>