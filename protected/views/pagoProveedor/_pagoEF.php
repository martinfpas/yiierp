<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10/11/2019
 * Time: 23:58
 */
?>


<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>

<div id="nuevoPagoProvEfectivo" style="">

    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
        'id'=>'pago-prov-efectivo-form',
        'enableAjaxValidation'=>false,
        'action' => Yii::app()->createUrl('PagoProvEfectivo/SaveAsync'),
        'htmlOptions' => array(
            'class' => 'SaveAsyncForm',
            'idGrilla'=>'pago-prov-efectivo-grid',
            'idMas' => 'masPagoProvEfectivo',
            'idDivNuevo' => 'nuevoPagoProvEfectivo',
        ),
    )); ?>

    <div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorPagoProvEfectivo"></div>
    <div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okPagoProvEfectivo">Datos Guardados Correctamente !</div>

    <?php echo $form->errorSummary($model); ?>
    <div class="content-fluid">
        <div class="row-fluid">

            <div class="span3">
                <?php echo $form->hiddenField($model,'idPagoProveedor'); ?>
                <?php echo $form->textFieldRow($model,'monto',array('class'=>'span12')); ?>
            </div>
            <div class="span3" style="padding-top: 25px;">
                <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=> 'Guardar',
                )); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>

<div class="row-fluid">
    <div class="span6" style="margin-left:10px;">

        <?php $this->widget('bootstrap.widgets.TbGridView',array(
            'id'=>'pago-prov-efectivo-grid',
            'dataProvider'=>$aPagoProvEfectivo,
            'template' => '{items}',
            'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
            'htmlOptions' => array(
                'style' => 'padding-top:0px',
            ),
            'columns'=>array(

                array(
                    'name' => 'monto',
                    'value' => '"$".number_format($data->monto,2,",",".")',
                ),
                array(
                    'class'=>'bootstrap.widgets.TbButtonColumn',
                    'template' => '{delete}',
                    'buttons'=>array(
                        'delete' => array(
                            'label' => 'Borrar Item',
                            'url'=>'Yii::app()->controller->createUrl("PagoProvEfectivo/delete", array("id"=>$data->id))',
                        ),
                    ),
                ),
            ),
        )); ?>
    </div>
</div>
