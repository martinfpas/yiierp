<?php
/**
 * Created by PhpStorm.
 * User: martin
 * Date: 10/11/2019
 * Time: 23:58
 */

$cs = Yii::app()->getClientScript();
$cs->registerScript('pagoProvMB',"$('#agregarMB').click(function(e){
        e.preventDefault();        
        console.log($('#MovimientoCuentasBanc_Nro_operacion').select2('data').id);
        
        let datos = 'PagoProvMovimientoBanc[idMovBanc]='+$('#MovimientoCuentasBanc_Nro_operacion').select2('data').id+'&PagoProvMovimientoBanc[idPagoProveedor]=".$model->idPagoProveedor."&PagoProvMovimientoBanc[monto]='+$('#MovimientoCuentasBanc_Nro_operacion').select2('data').importe;
        $.ajax({
            url: '".Yii::app()->createUrl('PagoProvMovimientoBanc/SaveAsync')."',
            type: 'POST',
            data: datos,
            success: function(html){                
                $.fn.yiiGridView.update('pago-prov-movimiento-banc-grid');                                         
                $('#MovimientoCuentasBanc_Nro_operacion').select2('val','');                
            },
            error: function(x,y,z){
                respuesta =  $.parseHTML(x.responseText);
                errorDiv = $(respuesta).find('.errorSpan');
                console.log($(errorDiv).parent().next());
                var htmlError = $(errorDiv).parent().next().html();
                alert(htmlError);                
            },            
        });                
        return false;
        //alert();
    });
    
");
?>

<div class="row-fluid">
    <div class="span8 bordeRedondo " id="propios" style="">

        <?php
        $modelMovimiento = new MovimientoCuentasBanc();
        $formChequePropio = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
            'id' => 'movimiento-bancario-form',
            'enableAjaxValidation'=>false,
        ));
        echo $formChequePropio->hiddenField($modelMovimiento,'tipoMovimiento');
        ?>
        <div class="span8 " style="">
            <?php
            echo $formChequePropio->textFieldRow($modelMovimiento,'Nro_operacion',array('class'=>'span12'));
            $this->widget('ext.select2.ESelect2',array(
                    'selector'=>'#MovimientoCuentasBanc_Nro_operacion',
                    'options'=>array(
                        'ajax'=>array(
                            'url'=>Yii::app()->createUrl('MovimientoCuentasBanc/select2'),
                            'dataType'=>'json',
                            'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page,partial: true}; }',
                            'results'=>"js:function(data,page) { return {results: data}; }",
                        ),
                        'initSelection' => 'function (element, callback) {
                                                var data = {id: element.val(), text: MovimientoCuentasBanc_Nro_operacion};
                                                console.log(data);
                                                callback(data);
                         }',
                        'class' => 'span12',
                        'tabindex' => 1,
                    ),
                )
            );

            ?>
        </div>
        <div class="span2">
            <input class="btn btn-primary" id="agregarMB" type="submit" value="Agregar" style="margin-top:25px">
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<div class="span12" style="margin-left:10px;margin-top: 5px;">

    <?php $this->widget('bootstrap.widgets.TbGridView',array(
        'id'=>'pago-prov-movimiento-banc-grid',
        'dataProvider'=>$aPagoProvMovimientoBanc,
        'afterAjaxUpdate' => 'js:function(id,data){console.log("afterAjaxUpdate");getTotales();return true;}',
        'columns'=>array(
            //'idMovBanc',
            array(
                'name' => 'oMovBanc.oTipoMovimiento.Descripcion',
                'header' => 'Tipo',
                'value' => '$data->oMovBanc->oTipoMovimiento->Descripcion',
            ),
            array(
                'name' => 'oMovBanc.fechaMovimiento',
                'value' => 'ComponentesComunes::fechaFormateada($data->oMovBanc->fechaMovimiento)',
                'header' => 'Fecha Movimiento',
            ),
            array(
                'name' => 'monto',
                'value' => '"$".number_format($data->monto,2,",",".")',
            ),
            array(
                'name' => 'oMovBanc.oCuenta.numero',
                'header' => 'Cuenta',
            ),
            array(
                'class'=>'bootstrap.widgets.TbButtonColumn',
                'template' => '{delete}',
                'buttons'=>array(
                    'delete' => array(
                        'label' => 'Borrar Item',
                        'url'=>'Yii::app()->controller->createUrl("PagoProvMovimientoBanc/delete", array("id"=>$data->id))',
                    ),
                ),
            ),
            /*
            array(
                'name' => '',
                'value' => '',
                'header' => '',
                'htmlOptions' => array('style' => ''),
                'headerHtmlOptions' => array('style' => ''),
            ),
            */
        ),
    )); ?>
</div>
