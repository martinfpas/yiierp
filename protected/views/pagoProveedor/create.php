<?php
/* @var $this PagoProveedorController */
/* @var $model PagoProveedor */

$this->breadcrumbs=array(
	PagoProveedor::model()->getAttributeLabel('models')=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'Gestion de '.PagoProveedor::model()->getAttributeLabel('models'), 'url'=>array('admin')),
);
?>

<h3>Creando <?=PagoProveedor::model()->getAttributeLabel('model') ?></h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>