<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'pago-proveedor-form',
	'enableAjaxValidation'=>false,
));

/** @var $model PagoProveedor */

Yii::app()->clientScript->registerScript('general2',"
        $(document).ready(function (){
                getTotales();
        });
                
        function getTotales(){
            
		    // TODO: MEJORAR LA SIGUIENTE LINEA PARA QUE idComprobante no esa necesario.
            var datos = 'id=".$model->id."';
            $.ajax({
                url:'".Yii::app()->createUrl('PagoProveedor/Json')."',
                data: datos,
                type: 'GET',
                success: function (data){
                
                    let response = JSON.parse(data);
                   
                    //let t = (isNaN(parseFloat(response.monto)))? 0 : parseFloat(response.monto); 
                    //let mf = (isNaN(parseFloat(response.montoFacturas)))? 0 : parseFloat(response.montoFacturas);
                   
                    $('#PagoProveedor_monto').val(response.monto);
                 	$('#montoFacturas').html('$'+response.montoFacturas);                  	

                },
            });
        }
    ");

?>
<script>
    function cerrarContenedor(){
        window.location = '<?=Yii::app()->createUrl('pagoProveedor/admin')?>';
    }
    $(document).ready(function (){

        $('#DescartarPago').click(function(e){
            e.stopPropagation();
            e.preventDefault();
            var datos = '&ajax=';
            var url = $(this).attr('url')+'&ajax=';
            console.log(url);
            $.ajax({
                url:url,
                data: datos,
                type: 'POST',
                success: function(html){
                    console.log(html);
                    cerrarContenedor();
                },
                error: function (x,y,z){
                    alert(x);
                }
            });
        });
    });


</script>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorPagoProveedor"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okPagoProveedor">Datos Guardados Correctamente !</div>

<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

		<div class="span2">
			<?php echo $form->textFieldRow($model,'monto',array('class'=>'span12','disabled' => 'disabled')); ?>
		</div>
		<div class="span2">
			<?php
                echo $form->labelEx($model,'fecha',array('style'=>'display:inline;'));
                $this->widget('CMaskedTextField', array(
                    'value'=> ComponentesComunes::fechaFormateada($model->fecha),
                    'name'=>'PagoProveedor[fecha]', // Cambiar 'NotaPedido por el modelo que corresponda
                    //'model' => $model,
                    //'attribute' => 'FechaFactura',
                    'mask' => '99-99-9999',
                    'htmlOptions'=>array(
                        'style'=>'height:25px;',
                        'class'=>'span12',
                        'title'=>'Ingresar los numeros sin los guiones  ',
                        'tabindex' => 4
                    ),
                ));
            ?>
		</div>
		<div class="span4">
			<?php
            echo $form->textFieldRow($model,'idProveedor',array('class'=>'span12 '));
            $this->widget('ext.select2.ESelect2',array(
                    'selector'=>'#PagoProveedor_idProveedor',
                    'model' => $model,
                    'attribute' => 'idProveedor',
                    'options'=>array(
                        'ajax'=>array(
                            'url'=>Yii::app()->createUrl('proveedor/select2'),
                            'dataType'=>'json',
                            'data'=>'js:function(term,page) { return {q: term, page_limit: 10, page: page}; }',
                            'results'=>'js:function(data,page) { return {results: data}; }',
                        ),
                        'initSelection'=>'js:function(element,callback){
                                var data={id:"'.$model->idProveedor.'",text:"'.(($model->oProveedor != null)? $model->oProveedor->Title: "" ).'"};
                                callback(data);
                            }',
                        'class' => 'span12',
                        'style' => 'margin-left:0px;',
                    ),
                )
            );
            echo $form->error($model,'idProveedor');
            ?>
		</div>
		<div class="span2">
			<?php echo $form->hiddenField($model,'idPago2Factura',array()); ?>
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'primary',

                'label'=>$model->isNewRecord ? 'Crear' : 'Guardar y Cerrar',
                'htmlOptions' => array(
                    'name' => 'cerrar',
                    'style' => 'margin-top:25px;'
                )
            )); ?>
        </div>
        <div class="span2">
            <span class="btn btn-danger" style="margin-top: 25px;" url="<?=Yii::app()->createUrl('pagoProveedor/delete',array('id'=>$model->id))?>" id="DescartarPago">Descartar Pago</span>
        </div>
    </div>
</div>

<span >Total Comprobantes conciliados: </span> <span id="montoFacturas"></span>

<?php $this->endWidget(); ?>
