
/* @var $this ProdNotaEntregaController */
/* @var $dataProvider CActiveDataProvider */

<?php
$this->breadcrumbs=array(
	'Prod Nota Entregas',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaEntrega', 'url'=>array('create')),
	array('label'=>'Gestion de ProdNotaEntrega', 'url'=>array('admin')),
);
?>

<h1>Prod Nota Entregas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
