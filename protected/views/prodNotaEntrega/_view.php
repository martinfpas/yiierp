<?php
/* @var $this ProdNotaEntregaController */
/* @var $data ProdNotaEntrega */
?>

<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nroItem')); ?>:</b>
	<?php echo CHtml::encode($data->nroItem); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idNotaEntrega')); ?>:</b>
	<?php echo CHtml::encode($data->idNotaEntrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('idArtVenta')); ?>:</b>
	<?php echo CHtml::encode($data->idArtVenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precioUnitario')); ?>:</b>
	<?php echo CHtml::encode($data->precioUnitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />


</div>