<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'prod-nota-entrega-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

	<div class="alert alert-block alert-error" style="display:none;" id="errorProdNotaEntrega"></div>
	<div class="alert alert-block alert-success" style="display:none;" id="okProdNotaEntrega">Datos Guardados Correctamente !</div>
	
<?php echo $form->errorSummary($model); ?>

<div class="content-fluid">
	<div class="row-fluid">

			<div class="span3"><?php echo $form->textFieldRow($model,'nroItem',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'idNotaEntrega',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'idArtVenta',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>
</div>	
			<div class="span3"><?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70)); ?>
</div>	
		</div>
</div>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Crear' : 'Guardar',
		)); ?>
</div>

<?php $this->endWidget(); ?>
