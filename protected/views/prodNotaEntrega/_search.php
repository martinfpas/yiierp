<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldRow($model,'id',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'nroItem',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idNotaEntrega',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'idArtVenta',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'precioUnitario',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'cantidad',array('class'=>'span12')); ?>

		<?php echo $form->textFieldRow($model,'descripcion',array('class'=>'span12','maxlength'=>70)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType' => 'submit',
			'type'=>'primary',
			'label'=>'Buscar',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
