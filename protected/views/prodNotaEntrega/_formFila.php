
<?php 
	$baseUrl = Yii::app()->baseUrl;
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl."/js/SaveAsync.js");
	$cs->registerCssFile($baseUrl."/css/customFineUploader.css");
?>
<input idNuevo="nuevoProdNotaEntrega" id="masProdNotaEntrega" type="button" class="btn SaveAsyncMas" value="Agregar" style="margin-bottom: 10px;">
<div id="nuevoProdNotaEntrega" style="display: none;">

	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'prod-nota-entrega-form',
		'enableAjaxValidation'=>false,
		'action' => Yii::app()->createUrl('ProdNotaEntrega/SaveAsync'),
		'htmlOptions' => array(
			'class' => 'SaveAsyncForm',
			'idGrilla'=>'prod-nota-entrega-grid',
			'idMas' => 'masProdNotaEntrega',
			'idDivNuevo' => 'nuevoProdNotaEntrega',
		),
	)); ?>

		<p class="note">Los campos <span class="required">*</span> son obligatorios.</p>

		<div class="alert alert-block alert-error SaveAsyncError" style="display:none;" id="errorProdNotaEntrega"></div>
		<div class="alert alert-block alert-success SaveAsyncOk" style="display:none;" id="okProdNotaEntrega">Datos Guardados Correctamente !</div>

	<?php echo $form->errorSummary($modelProd); ?>
<div class="content-fluid">
	<div class="row-fluid">


			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'nroItem',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'idNotaEntrega',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'idArtVenta',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'precioUnitario',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'cantidad',array('class'=>'span12')); ?>
		</div>
			<div class="span3">
			<?php echo $form->textFieldRow($modelProd,'descripcion',array('class'=>'span12','maxlength'=>70)); ?>
		</div>
	
	</div>
</div>	
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$modelProd->isNewRecord ? 'Crear' : 'Guardar',
			)); ?>
	</div>
	
	<?php $this->endWidget(); ?>
</div>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'prod-nota-entrega-grid',
'dataProvider'=>$aProdNotaEntrega->search(),
'columns'=>array(
		'id',
		'nroItem',
		'idNotaEntrega',
		'idArtVenta',
		'precioUnitario',
		'cantidad',
		/*
		'descripcion',
		*/
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template' => '{update} | {delete}',
				'buttons'=>array(
					'update' => array(
	                	'label' => 'Modificar',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaEntrega/update", array("id"=>$data->id))',
						'options'=>array('target'=>'_blank'),
					),					
					'delete' => array(
	                	'label' => 'Borrar Item',
						'url'=>'Yii::app()->controller->createUrl("ProdNotaEntrega/delete", array("id"=>$data->id))',
					),
				),
		),
	),
)); ?>