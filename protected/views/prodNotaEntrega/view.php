<?php
$this->breadcrumbs=array(
	'Prod Nota Entregas'=>array('admin'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaEntrega', 'url'=>array('create')),
	array('label'=>'Modificar ProdNotaEntrega', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar ProdNotaEntrega', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Esta seguro que desea borrar?')),
	array('label'=>'Gestion de ProdNotaEntrega', 'url'=>array('admin')),
);
?>

<h1>Ver ProdNotaEntrega #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nroItem',
		'idNotaEntrega',
		'idArtVenta',
		'precioUnitario',
		'cantidad',
		'descripcion',
		/*
		array(
			'name' => ,
			'value' => ,
			'htmlOptions' => array('style' => ''),
		),
		*/
),
)); ?>
