<?php
$this->breadcrumbs=array(
	'Prod Nota Entregas'=>array('admin'),
	$model->id=>array('view','id'=>$model->id),
	'Modificando',
);

$this->menu=array(
	array('label'=>'Nuevo ProdNotaEntrega', 'url'=>array('create')),
	array('label'=>'Ver ProdNotaEntrega', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Gestion de ProdNotaEntrega', 'url'=>array('admin')),
);
?>

	<h1>Modificando ProdNotaEntrega <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>