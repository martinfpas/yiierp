<?php
/* @var $this ProdNotaEntregaController */
/* @var $model ProdNotaEntrega */

$this->breadcrumbs=array(
	'Prod Nota Entregas'=>array('admin'),
	'Creando ',
);

$this->menu=array(
	array('label'=>'ABM ProdNotaEntrega', 'url'=>array('admin')),
);
?>

<h1>Nuevo ProdNotaEntrega</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>