<?php
require '/var/www/erca/protected/vendors/google-api-php-client-PHP7.0/vendor/autoload.php';

class GDriveCommand extends CConsoleCommand
{
    
    public function run($args)
    {
        
        Yii::log('actionCronBCK 2 ','warning');	

        Yii::import('ext.dumpDB.dumpDB');		
		$this->bckDb();

		$gd = new GDrive();
		$gd->saveDBs();
    }

/* 
        // Print the names and IDs for up to 10 files.
        $optParams = array(
        'pageSize' => 10,
        'fields' => 'nextPageToken, files(id, name)'
        );
        $results = $service->files->listFiles($optParams);

        if (count($results->getFiles()) == 0) {
            print "No files found.\n";
        } else {
            print "Files:\n";
            foreach ($results->getFiles() as $file) {
                printf("%s (%s)\n", $file->getName(), $file->getId());
            }
        }
*/

    function bckDb(){
        $dumper = new dumpDB();
        $bk_file = Yii::app()->basePath.'/bck/erca-prod'.date('YmdHis').'.sql';
        echo $bk_file;
        $fh = fopen($bk_file, 'w+') or die("can't open file");
        fwrite($fh, $dumper->getDump(FALSE));
        fclose($fh);

        $filename = $bk_file.".zip";
        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
            exit("cannot open <$filename>\n");
        }
        $zip->addFile($bk_file);
        $zip->close();
        unlink($bk_file);
    }
}






