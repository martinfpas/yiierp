<?php
class MysqlDumpCommand extends CConsoleCommand
{
    public function run($args)
    {

        Yii::import('ext.dumpDB.dumpDB');
        $dumper = new dumpDB();
        $bk_file = Yii::getPathOfAlias('webroot').'/upload/FILE_NAME-'.date('YmdHis').'.sql';
        echo $bk_file;
        $fh = fopen($bk_file, 'w') or die("can't open file");
        fwrite($fh, $dumper->getDump(FALSE));
        fclose($fh);
    }
}