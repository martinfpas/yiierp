<?php

if(isset($_SERVER['SERVER_NAME'])){
    // This is the database connection configuration.
    return array(
        //'connectionString' => (preg_match('(conmacsrl.com.ar)',$_SERVER['SERVER_NAME'])!= false)? 'mysql:host=localhost;dbname=conmac_sis' : 'mysql:host=localhost;dbname=ercaprod130220220',
        'connectionString' => (preg_match('(sysfe.ddns.net)',$_SERVER['SERVER_NAME'])!= false)? 'mysql:host=localhost;dbname=ercaprod' : 'mysql:host=localhost;dbname=erca_prod',
        'emulatePrepare' => true,
        'username' => (preg_match('(conmacsrl.com.ar)',$_SERVER['SERVER_NAME'])!= false)? 'phpmyadminuser' : 'phpmyadminuser',
        'password' => (preg_match('(conmacsrl.com.ar)',$_SERVER['SERVER_NAME'])!= false)? 'P@ssw0rd' : 'P@ssw0rd',
        'charset' => 'utf8',

    );
}else{
    // This is the database connection configuration.
    return array(
        //'connectionString' => (preg_match('(conmacsrl.com.ar)',$_SERVER['SERVER_NAME'])!= false)? 'mysql:host=localhost;dbname=conmac_sis' : 'mysql:host=localhost;dbname=ercaprod130220220',
        'connectionString' => 'mysql:host=localhost;dbname=erca_prod',
        'emulatePrepare' => true,
        'username' => 'phpmyadminuser',
        'password' => 'P@ssw0rd',
        'charset' => 'utf8',
    );
}


