<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');


// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Erca',

	'language'=>'es',
	//language for messages and views
	'sourceLanguage'=>'es_ar',
		
	// preloading 'log' component
	'preload'=>array(
		'log',
		'bootstrap', // preload the bootstrap component
	),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.models.migra.*',
		'application.components.*',
        'application.components.PagoClienteWid.*',
        'application.components.PrintWidget.*',
        'application.components.ArticuloVentaCSearch.*',
		'application.vendors.phpexcel1-8.PHPExcel',
		'application.vendors.neofactura.*',
        'application.vendors.mpdf60.*',
        'application.vendors.CITI.*',
		'ext.yiireport.*',
		'editable.*',
		'ext.ECompositeUniqueValidator',	
	),

	'modules'=>array(
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'p@ssw0rd',
			 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('*','::1'),
				'generatorPaths'=>array(
					'application.gii',
			        'bootstrap.gii', // since 0.9.1
			 		'bootstrap.gii2', // since 0.9.1
			     ),
			),
	),

	// application components
	'components'=>array(

		'authManager'=>array(
			'class'=>'CDbAuthManager',
			'connectionID'=>'db',
			'itemTable'=>'AuthItem', // Tabla que contiene los elementos de autorizacion
			'itemChildTable'=>'AuthItemChild', // Tabla que contiene los elementos padre-hijo
			'assignmentTable'=>'AuthAssignment', // Tabla que contiene la signacion usuario-autorizacion
		),
		'bootstrap'=>array(
			//'class'=>'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
			'class'=>'ext.YiiBooster-master.src.components.Bootstrap', // assuming you extracted bootstrap under extensions
	    ),
		/*
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),
		'editable' => array(
				'class'     => 'editable.EditableConfig',
				'form'      => 'plain',        //form style: 'bootstrap', 'jqueryui', 'plain'
				'mode'      => 'popup',            //mode: 'popup' or 'inline'
				'defaults'  => array(              //default settings for all editable elements
						'emptytext' => 'Click para editar'
				)
		),
        'ePdf' => array(
            'class'         => 'ext.yii-pdf.EYiiPdf',
            'params'        => array(
                'mpdf'     => array(
                    //'librarySourcePath' => 'application.vendors.mpdf.*',
                    'librarySourcePath' => 'application.vendors.mpdf60.*',
                    'constants'         => array(
                        '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                    ),
                    'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder

                ),
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap

                )
            ),
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning,info,trace',
                    'logFile'=>'custom.log',
                    'categories'=>'custom.*',
                ),
				// uncomment the following to show log messages on web pages
                /*
                array(
					'class'=>'CWebLogRoute',
                    'showInFireBug'=>true,
				),
                */

			),
		),
		'user'=>array(
				// enable cookie-based authentication
				'allowAutoLogin'=>true,
				// PERMITE DETECTAR CUANDO SE PIERDE SESION DURANTE UNA LLAMADA AJAX
				'loginRequiredAjaxResponse' => 'YII_LOGIN_REQUIRED',
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
/*
        'cuit' => '20285260427',
        'modoAfip' => 0,
        'ConceptoAfip' => 1,  // 1- PRODUCTOS 2-SERVICIOS
  */
        'cuit' => '33634445809',//'27274879810',
        'modoAfip' => 1,    // 0 HOMOLOGACION 1 PRODUCCION
        'ConceptoAfip' => 1,  // 1- PRODUCTOS 2-SERVICIOS

	),
	'timeZone' => 'America/Argentina/Buenos_Aires',
	'theme'=>'bootstrap',
);
