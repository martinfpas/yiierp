<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dbase
 *
 * @author Ale
 */
class DBase {
        private static $conexion=NULL;
        private function __construct (){}


        public static function conectar(){
            $oConnParams = Yii::app()->getDb();

            try {
                $pdo_options[PDO::ATTR_ERRMODE]=PDO::ERRMODE_EXCEPTION;
                //self::$conexion= new PDO('mysql:host=sysfe.ddns.net;port=3360;dbname=erca','phpmyadminuser','P@ssw0rd',$pdo_options);
                self::$conexion= new PDO($oConnParams->connectionString,$oConnParams->username,$oConnParams->password,$pdo_options); //
                return self::$conexion;
            } catch (Exception $e) {
                 echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }               
        }		
} 

