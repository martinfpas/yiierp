<?php

/**
 * Description of CitiCompras
 *
 * @author Ale
 * 
 * v1.1
 */
//require_once './Compras.php';
//require_once './AlicCompras.php';
//require_once './Ventas.php';
//require_once './AlicVentas.php';

class Citi
{
    //put your code here

    public $periodo;
    private $datosMes;
    private $mes;
    private $archivo_v;
    private $archivo_av;
    private $archivo_c;
    private $archivo_ac;
    private $archivo;
    private $usa_equivalencia;
    public $filesPath = '/var/www/erca/upload/';

    /* Constructor de la clase CITI 
    
    $extension // .txt .cvs 
    $tipo C -> compra , AC -> COMPRAS_ALICUOTAS
    $query
    $usa_equivalencia 
    $mmaa
    */



    public function __construct($tipo, $extension, $query, $usa_equiva , $mmaa)
    {
        $this->usa_equivalencia =  $usa_equiva;
        $this->mes = $mmaa;
        $this->datosMes=null;
        $this->cabecera=null;
        //CitiVentas.txt
        try{
            $db=DBase::conectar();
            switch ($tipo){
            case 'V':
                    $query = "SELECT * FROM vComprobantesVenta where periodo ='". $mmaa ."'";
                    $this->datosMes= $db->query($query );
                    $this->archivo_v = 'REGINFO_CV_VENTAS_CBTE-'.$mmaa.'.'.$extension;
                    if ($this->datosMes !=null)
                        $this->ProcesarVenta();
                    break;
            case 'AV':
                    $query = "Select * from vComprobantesVenta where  periodo ='". $mmaa."'";
                    $this->datosMes= $db->query($query );
                    $this->archivo_av = 'REGINFO_CV_VENTAS_ALICUOTAS-'.$mmaa.'.'.$extension;
                    if ($this->datosMes !=null)
                        $this->ProcesarAliVenta();
                    break;
            case 'Cab':

                    $query = "Select * from datos_cabecera where periodo ='". $mmaa."'";
                    $this->cabecera = $db->query($query );
                    $this->archivo = 'REGINFO_CV_CABECERA-'.$mmaa.'.'.$extension;
                    if ($this->cabecera !=null)
                        $this->ProcesarCabecera();
                    break;
            case 'C':
                    $query = "Select * from vComprobantesCompras where MesCarga ='". $mmaa."'";
                    $this->datosMes= $db->query($query );
                    $this->archivo_c = 'REGINFO_CV_COMPRAS_CBTE-'.$mmaa.'.'.$extension;
                    if ($this->datosMes !=null)
                        $this->ProcesarCompra();
                    break;
            case 'AC':
                    $query = "Select * from vComprobantesComprasAlicuotas where MesCarga ='". $mmaa."'";
                      $this->datosMes= $db->query($query );
                    $this->archivo_ac = 'REGINFO_CV_COMPRAS_ALICUOTAS-'.$mmaa.'.'.$extension;
                    if ($this->datosMes !=null)
                        $this->ProcesarAlicCompra();
                    break;
            }
            if ($this->datosMes ==null)
                 echo 'No se obtuvieron datos: ', false, "\n";
        } catch (Exception $e){
             echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }


    public function GetArchivoV(){
            return $this->archivo_v;
        }
    public function GetArchivoAV(){
            return $this->archivo_av;
        }
    public function GetArchivoC(){
            return $this->archivo_c;
        }
    public function GetArchivoAC(){
            return $this->archivo_ac;
        }
    public function GetArchivo(){
            return $this->archivo;
        }



    public function ProcesarCabecera(){
        $file = fopen( $this->filesPath.$this->archivo, "w");
        $bandera = true;
        $objeto = new Cabecera($row);
        $renglon = $objeto->getStringRenglon();

        if($renglon == false){
                $bandera = false;

        }else {
                fwrite($file, $renglon . PHP_EOL);
        }

        fclose($file);
        if($bandera == false){
               unlink($this->filesPath.$this->archivo);
               echo "Error al generar los renglones de Cabecera de Cab";
        }else{
            return $this->filesPath.$this->archivo;
        }
    }

    public function ProcesarVenta(){
        $file = fopen( $this->filesPath.$this->archivo_v, "w");
        $bandera = true;
        foreach( $this->datosMes->fetchAll() as $row){
           $objeto = new Ventas($row,$this->usa_equivalencia);
           $renglon = $objeto->getStringRenglon();
            if($renglon == false){
                $bandera = false;
                break;
            }
            else {
                //fwrite($file, $renglon . PHP_EOL);
                fwrite($file, $renglon . '
');
            }
        }
        fclose($file);
        if($bandera == false){
               unlink($this->filesPath.$this->archivo_v);
               echo "Error al generar los renglones de V";
        }else{
            return $this->filesPath.$this->archivo_v;
        }
    }

    public function ProcesarAliVenta(){
        $file = fopen( $this->filesPath.$this->archivo_av, "w");
        $bandera = true;

        foreach( $this->datosMes->fetchAll() as $row){
            $objeto = new AlicVentas($row , $this->usa_equivalencia);
            $renglon = $objeto->getStringRenglon();
            if($renglon == false){
                $bandera = false;
                break;
            }
            else {
                //fwrite($file, $renglon . PHP_EOL);
                fwrite($file, $renglon . '
');
            }
        }
        fclose($file);
        if($bandera == false){
               unlink($this->filesPath.$this->archivo_av);
               echo "Error al generar los renglones de AV";

        }else{
            return $this->filesPath.$this->archivo_av;
        }
    }

    public function ProcesarCompra(){
        $file = fopen( $this->filesPath.$this->archivo_c, "w");
        $bandera = true;

        foreach( $this->datosMes->fetchAll() as $row){
            $objeto = new Compras($row , $this->usa_equivalencia);
            $renglon = $objeto->getStringRenglon();
           if($renglon == false){
               Yii::log(' $objeto:'.ComponentesComunes::print_array($objeto,true),'error');
                $bandera = false;
                break;
            }
            else {
                //fwrite($file, $renglon . PHP_EOL);
                fwrite($file, $renglon . '
');
            }
        }
        fclose($file);
        if($bandera == false){
               unlink($this->filesPath.$this->archivo_c);
               echo "Error al generar los renglones de CC";
        }else{
            return $this->filesPath.$this->archivo_c;
        }
    }
    
    public function ProcesarAlicCompra(){
        $file = fopen( $this->filesPath.$this->archivo_ac, "w");
        $bandera = true;

        foreach( $this->datosMes->fetchAll() as $row){
            $objeto = new AlicCompras($row , $this->usa_equivalencia);
            $renglon = $objeto->getStringRenglon();
            if($renglon == false){
                $bandera = false;
                break;
            }
            else {
                //fwrite($file, $renglon . PHP_EOL);
                fwrite($file, $renglon . '
');
            }
        }
        fclose($file);
        if($bandera == false){
               unlink($this->filesPath.$this->archivo_ac);
               echo "Error al generar los renglones de AC";
        }else{
            return $this->filesPath.$this->archivo_ac;
        }
    }
    
}