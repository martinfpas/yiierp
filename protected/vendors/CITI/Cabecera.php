<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ventas
 *
 * @author Ale
 */

/*
  $this->venta['codigo_de_documento_del_comprador']= $datos[5];                 //Según tabla tipo de Documentos debe venir asi
  $this->venta['codigo_de_moneda']= $datos[16];                                 //Según tabla  debe venir
  $this->venta['codigo_de_operacion']= $datos[19]== 'A'? '':$datos[19];         //Según tabla debe venir  si es A se blanquea
  [codOperacion] => A [19] => "" 
  Código de operación: Si la alícuota de IVA  es igual a cero o la operación responde a 
  una operación de Canje se deberá completar de acuerdo con la tabla AFIP
  verifcar en $tipoCondigoOpe en Tablas.php
 
 */


require_once '.\Conversion.php';

class Cabecera {
        
    private $cab;
    
    public function __construct($datos)
    {
        $this->cab = array();
        $this->cargarValores($datos);
        
    }

    private function cargarValores(array $datos)
    {
           
        
        $this->cab['cuitInformante']=     $datos[0];   //CUIT Informante	
        $this->cab['aaaamm'] =            $datos[0];   //Período	AAAAMM
        $this->cab['secuencia'] =         $datos[0];   //Secuencia	Original (00), Rectificativas (01, 02..)
        $this->cab['inMovimiento'] =      $datos[0]  ; //Sin Movimiento	SI (S) / NO (N)
        $this->cab['prorrateaCFC'] =      $datos[0] ;  //Prorratear Crédito Fiscal Computable	SI (S) / NO (N)
        $this->cab['creditoFC']   =       $datos[0];   //Crédito Fiscal Computable Global ó Por Comprobante	Global (1)  ó Por Comprobante (2)
        
        $this->cab['importeCFCG']=        $datos[0]; //Importe Crédito Fiscal Computable Global	13 enteros 2 decimales sin punto decimal 
        $this->cab['importeCFCad']=       $datos[0]; //Importe Crédito Fiscal Computable, con asignación directa.	13 enteros 2 decimales sin punto decimal 
        $this->cab['importeCFCpro']=      $datos[0] ; //Importe Crédito Fiscal Computable, determinado por prorrateo.	13 enteros 2 decimales sin punto decimal 
        $this->cab['importeCFnoC']=       $datos[0]; //Importe Crédito Fiscal no Computable Global	13 enteros 2 decimales sin punto decimal 
        $this->cab['creditoFCSeS']=       $datos[0]; //Crédito Fiscal Contrib. Seg. Soc. y Otros Conceptos	13 enteros 2 decimales sin punto decimal 
        $this->cab['creditoFCCSecSoc']=   $datos[0]; //Crédito Fiscal Computable Contrib. Seg. Soc. y Otros Conceptos	13 enteros 2 decimales sin punto decimal 
        
              
      //  print_r($this->venta);
      //  echo ('<br>');
    }
    
     
    //$venta_codigo_de_operacion   
    
    
    public function  getStringRenglon(){
        $stringVenta= "";
        
        
        $stringVenta .= Conversion::quitarGuionesCuit($this->cab['cuitInformante']);                           // numerico   11
        $stringVenta .= $this->cab['aaaamm'];                                                              // numerico   6
        $stringVenta .= Conversion::agregarCeros_izquierda(2 , $this->cab['secuencia']);                   // numerico   2
        $stringVenta .=$this->cab['inMovimiento'] ;                                                        //Alfanumerico 1 Sin Movimiento	SI (S) / NO (N)
        $stringVenta .=$this->cab['prorrateaCFC'] ;                                                        //Alfanumerico 1 Prorratear Crédito Fiscal Computable	SI (S) / NO (N)
        $stringVenta .=$this->cab['creditoFC'] ;                                                           //Alfanumerico 1      Global (1)  ó Por Comprobante (2)

        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec( $this->cab['importeCFCG']));       // numerico  13 enteros 2 decimales sin punto decimal
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->cab['importeCFCad']));       // numerico  13 enteros 2 decimales sin punto decimal
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->cab['importeCFCpro']));      // numerico 13 enteros 2 decimales sin punto decimal
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->cab['importeCFnoC']));       // numerico  13 enteros 2 decimales sin punto decimal
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->cab['creditoFCSeS']));       // numerico 13 enteros 2 decimales sin punto decimal
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->cab['creditoFCCSecSoc']));   // numerico 13 enteros 2 decimales sin punto decimal
            
        $longitud = strlen($stringVenta);
        
        
          
        if ($longitud == 112) return $stringVenta;
        else return false;
        
    }
  
}
