<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AlicVentas
 *
 * @author Ale
 * 
 * v1.1
 */

//require_once '.\Conversion.php';

class AlicVentas extends Conversion{
    //put your code here
    
    private $ali_ventas;
     private $usa_equivalencia;
    
    public function __construct($datos, $usa_equivale)
    {
        $this->usa_equivalencia = $usa_equivale;
        $this->ali_ventas = array();
        $this->cargarValores($datos);
    }
    
    public function cargarValores(array $datos)
    {                  
        $this->ali_ventas['tipo_de_comprobante']= $datos[0];
        $this->ali_ventas['punto_de_venta']= $datos[1];
       
        
        $venta_tipo_de_comprobante                = Conversion::optenerTipoComprobante($datos[1],$this->usa_equivalencia);             //Según tabla Comprobantes Ventas 
        if (!$venta_tipo_de_comprobante) {
            print_r("Existen conprobantes que no se pueden convertir".$datos[0].'->'.$datos[1].'->'.$datos[2].'->'.$datos[3]);
            die;                    
        }else{
        
            $this->ali_ventas['tipo_de_comprobante']=             $venta_tipo_de_comprobante;        
            $this->ali_ventas['punto_de_venta']=                  Conversion::optenerPtoVta($venta_tipo_de_comprobante, $datos[2]);
            $this->ali_ventas['numero_de_comprobante']=                                            $datos[3];

            // solo si son 
            if ($venta_tipo_de_comprobante == '033' || 
                    $venta_tipo_de_comprobante =='331' || 
                    $venta_tipo_de_comprobante == '332'){
                $this->venta['numero_de_comprobante']=                                        $datos[24];          
                // [codigoOperacionElectronica] => "" [24]=> ""      no esta 
                // Numero de Comprobante: Para los comprobantes correspondientes a los códigos '033', '331' y '332' este
                // campo deberá completarse con el "Código de Operación Electrónica    -COE-"
            }
            
            $this->ali_ventas['importe_neto_gravado']=            $datos[25];
            $this->ali_ventas['alicuota_de_iva']=                 Conversion::obtenerCodigoALicuotaIva($datos[23]);
            $this->ali_ventas['impuesto_liquidado']=              $datos[26];        
        }
    }

     public function  getStringRenglon(){
        $stringAlicVenta= "";
               
        $stringAlicVenta .= Conversion::agregarCeros_izquierda(3 , $this->ali_ventas['tipo_de_comprobante']);
        $stringAlicVenta .= Conversion::agregarCeros_izquierda(5 , $this->ali_ventas['punto_de_venta']);
        
        $stringAlicVenta .= Conversion::agregarCeros_izquierda(20 , $this->ali_ventas['numero_de_comprobante']);
        $stringAlicVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->ali_ventas['importe_neto_gravado']));
        $stringAlicVenta .= $this->ali_ventas['alicuota_de_iva'];
        $stringAlicVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->ali_ventas['impuesto_liquidado']));
         
         $longitud = strlen($stringAlicVenta);
        
        if ($longitud == 62) return $stringAlicVenta;
        else return false;
        
     }
                

}
