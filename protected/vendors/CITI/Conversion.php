<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * v1.1
 */

/**
 * Description of Conversion
 *
 * @author Ale
 */



class Conversion  { 
    
        
    public static function obtenerCodigoALicuotaIva($porcentajeIva){
        require 'Tablas.php';
        $parteEntera = intval($porcentajeIva); 
        $parteDecimal = str_replace('0.','',$porcentajeIva-intval($porcentajeIva)); 
        $stringIndice = str_pad($parteEntera,4,0,STR_PAD_LEFT).str_pad($parteDecimal,6,0);                                                                // numerico
        
        return $alicuotasIvaInverso[$stringIndice];
    }
    
    public static function convertirFecha($originalDate){
        $newDate = date("Y/m/d", strtotime($originalDate));
        $string =str_replace('/', '', $newDate );
        $longitud = strlen($string);
        
        if ($longitud == 8) 
            return $string;
        else 
            return false;
    }
    
     
    //agrega ceros a la izquierda de la expresion 
    //public function agregarCeros_izquierda(int $longitud, string $dato){
    public static function agregarCeros_izquierda($longitud, $dato){
    
        $long_actual = strlen($dato);
        while($long_actual < $longitud){
             $dato = "0".$dato;
             $long_actual = strlen($dato);
        }      
        return $dato;  
    }
    
    
    //agrega espacios en blaco a la derecha 
   // public function agregarBlancos_derecha(int $longitud, string $dato){   
    public static function agregarBlancos_derecha( $longitud, $dato){            
        $long_actual = strlen($dato);
        
        while($long_actual < $longitud){
             $dato = $dato." "; 
             $long_actual = strlen($dato);
        } 
        
        return substr("$dato", 0, 30);
    }
    
    /*
     * recbe un float y redondea en valores menores o = 0.004->0.00 hacia abajo y maroyes o iguales
     * a 0.005->0.01 hacia riba con 2 decimales
     */
    //public function sustituirPunto2Dec(float $original){
     public static function sustituirPunto2Dec($numero){
        $valornumerico = number_format($numero, 2, ',', '');
        $newDato = str_replace(',', '', $valornumerico);
        return str_replace('.', '', $newDato);
    }
    
    //public function tipodeCambio(float $original){
    public static function tipodeCambio($numero){
        $valornumerico = number_format($numero,0, '.', '');
        $newDato = str_replace(',', '', $valornumerico);
        return str_replace('.', '', $newDato);
    }
    
    
    
    
    
    
    
    /*********************************************************************************************************************************/
    /*********************************************************************************************************************************/
    /*********************************************************************************************************************************/
    // si $equivalencia es S entonces $valor corresponde a la clave par abuscar en equivalencias*
    // si es N corresponde al valor para obtener la clave y verificar el resultado.
    
    
    public static function optenerTipoComprobante($key, $equi){    
        require 'Tablas.php';
        require 'Equivalencias.php';
        
        if ($equi == 'S'){
            if (array_key_exists($key , $tipoComprobanteEquivale)){
                $valor = $tipoComprobanteEquivale[$key]; // busca el valor en la tabala de equivalencia a partir de la clave
                $codigo = array_search($valor, $tipoComprobante); // devuelve la clave correspondiente
            }
            else {
                $codigo  = false ; 
            }
        }else{   
            $valor = $key;
            $codigo  = array_search($valor, $tipoComprobante);            
        }
                    
        return $codigo;  
    }
     
      
    // Punto de venta: Para los comprobantes correspondientes a los códigos '033', '099', '331' y '332' 
    // este campo deberá completarse con ceros.
    public static function optenerPtoVta($venta_tipo_de_comprobante, $valor ){
        
        switch ($venta_tipo_de_comprobante){
            case '033':
                $valor = 0;
                break;
            case '099':
                $valor = 0;
                break;
            case '331':
                $valor = 0;
                break;
            case '332':
                $valor = 0;           
                break;
        }
        
        return $valor;
    }    
    
    // El campo Cantidad de Alícuotas de IVA no puede ser cero para los comprobantes A y 
    // deberá ser igual a cero en el caso de los comprobantes B o C.
    public static function esBoC($key){
        require 'Tablas.php';
        require 'Equivalencias.php';
            if(array_key_exists ($key ,$tipoComprobanteBoC)) return true;
            else return false;  
        }
        
        
        
        
    //Transforma un cuit con guiones en un cuit sin guiones.
        
    public static function quitarGuionesCuit($cuit){
            $findme   = '-';
            $pos = strpos($mystring, $cuit);

            // Nótese el uso de ===. Puesto que == simple no funcionará como se espera
            // porque la posición de 'a' está en el 1° (primer) caracter.
            if ($pos === false) {
                return $cuit;
            } else {
                return str_replace('-', '', $cuit);
            }
        }
    
    
   
        
        
        
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    



   //Operación_Condición IVA
    private static function optenerCondicionIVA($val, $equi){    
        
        if ($equi == 'S'){
            if (array_key_exists($val, $tipoCondicionIvaEquivale)){
                $clave = $tipoCondicionIvaEquivale[$val];
                $valor = array_search($clave, $tipoCondicionIva);
            } else {
                $valor = false ; 
            }
        }else{            
            $valor = array_search($val, $tipoCondicionIva);            
        }                    
        return $valor; 
    }
        
  
        
   


    
    
    
    
    
    
    /*
    * TABLA CUIT PAIS
    * Cod. Tipo Sujeto   0=JURIDICA   1=FISICA 2=OTRA TIPO DE ENTIDAD
    *  conformacion de la tabla cuit es la clave y Cod. Tipo Sujeto + | + Descrip CUIT Pais el valor
    */
    private static function optenerCuitPais($valor){
        
            $clave = array_search($valor, $tipoCuitPais);
            return $clave;  
        }       
        
        
        
        
        
        
        
    /*********************************************************************************************************************************/
    /********************************** Las siguientes funciones optienen una valor a partir de la clave *****************************/
    /*********************************************************************************************************************************/
    
    // - Tipos de Comprobantes
    
    private static function optenerComprobante($key){
           if(array_key_exists ($key , $tipoComprobante)) return $tipoComprobante[$key];
           else return '';  
    }
        
    //Documento identificatorio del comprador
    private static function optenerDocumento($key){
            if(array_key_exists ($key , $tipoDocumento)) return $tipoDocumento[$key];
            else return '';  
        }

    //Operación_Condición IVA
    private static function optenerAlicuotaIVA($key){
            if(array_key_exists ($key , $tipoCondicionIva)) return $tipoCondicionIva[$key];
            else return '';  
        }
        
   // Codigo de operación (solo cuando alicuota sea igual a cero) 
    private static function optenerOperacion($key){
            // Solo cuando alicuota sea igual a cero
            if(array_key_exists ($key ,  $tipoCondigoOpe )) return  $tipoCondigoOpe[$key];
            else return '';   
        }
        
    // Moneda
    private static function optenerCodigoMoneda($key){
            if(array_key_exists ($key , $tipoMoneda )) return $tipoMoneda[$key];
            else return '';  
        }
        
    
    /*
    * TABLA CUIT PAIS
    * Cod. Tipo Sujeto 0 = JURIDICA     1 = FISICA   2 =OTRA TIPO DE ENTIDAD
    *  conformacion de la tabla cuit es la clave y Cod. Tipo Sujeto + | + Descrip CUIT Pais el valor
    */
    private static function optenerTipoSujeto_Pais($key){
            if(array_key_exists ($key , $tipoCuitPais )) return $tipoCuitPais[$key];
            else return '';
             
        }   
        
        
}
