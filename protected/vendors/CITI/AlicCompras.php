<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AlicCompras
 *
 * @author Ale
 * 
 * v1.1
 */
/*
require_once '.\Conversion.php';
require_once '.\DBase.php';
require_once '.\Equivalencias.php';
*/


class AlicCompras extends Conversion {
    private $ali_compra;
    private $usa_equivalencia;
    
    public function __construct($datos, $usa_equivale)
    {
        $this->usa_equivalencia = $usa_equivale;
        $this->ali_compra = array();
        $this->cargarValores($datos);
    }
    
    public function cargarValores(array $datos)
    {                  
        
        $this->ali_compra['tipo_de_comprobante']= $datos[0];
        $this->ali_compra['punto_de_venta']= $datos[1];
        $this->ali_compra['numero_de_comprobante']= $datos[2];
        $this->ali_compra['codigo_de_documento_del_vendedor']= $datos[3];
        $this->ali_compra['numero_de_identificacion_del_vendedor']= $datos[4];
        $this->ali_compra['importe_neto_gravado']= $datos[5];// falta 
        $this->ali_compra['alicuota_de_IVA']= Conversion::obtenerCodigoALicuotaIva($datos[6]);  //    falta 
        $this->ali_compra['impuesto_liquidado']= $datos[7];  // falta

    }
     
    public function  getStringRenglon(){
        $stringAlicCompra= "";
        
        $stringAlicCompra .= Conversion::agregarCeros_izquierda( 3 ,$this->ali_compra['tipo_de_comprobante']);                                                        // numerico
        $stringAlicCompra .= Conversion::agregarCeros_izquierda( 5 ,$this->ali_compra['punto_de_venta']);                                                             // numerico
        $stringAlicCompra .= Conversion::agregarCeros_izquierda( 20 ,$this->ali_compra['numero_de_comprobante']);          
        $stringAlicCompra .= Conversion::agregarCeros_izquierda(2,$this->ali_compra['codigo_de_documento_del_vendedor']);
        $stringAlicCompra .= Conversion::agregarCeros_izquierda(20, $this->ali_compra['numero_de_identificacion_del_vendedor']);
        $stringAlicCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->ali_compra['importe_neto_gravado']));
        $stringAlicCompra .= $this->ali_compra['alicuota_de_IVA'];
        $stringAlicCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->ali_compra['impuesto_liquidado']));
        
        $longitud = strlen($stringAlicCompra);
        $resultado = str_replace(".", " ",$stringAlicCompra);
        
        //echo $resultado;
        if ($longitud == 84) return $resultado;
        else return false;
    }
    
    
}
