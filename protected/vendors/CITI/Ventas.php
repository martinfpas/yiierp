<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Ventas
 *
 * @author Ale
 * v1.1
 */

/*
  $this->venta['codigo_de_documento_del_comprador']= $datos[5];                 //Según tabla tipo de Documentos debe venir asi
  $this->venta['codigo_de_moneda']= $datos[16];                                 //Según tabla  debe venir
  $this->venta['codigo_de_operacion']= $datos[19]== 'A'? '':$datos[19];         //Según tabla debe venir  si es A se blanquea
  [codOperacion] => A [19] => "" 
  Código de operación: Si la alícuota de IVA  es igual a cero o la operación responde a 
  una operación de Canje se deberá completar de acuerdo con la tabla AFIP
  verifcar en $tipoCondigoOpe en Tablas.php
 
 */


//require_once '.\Conversion.php';

class Ventas {
        
    private $venta;
    private $usa_equivalencia;
    
    public function __construct($datos, $usa_equivale)
    {
        $this->usa_equivalencia = $usa_equivale;
        $this->venta = array();
        $this->cargarValores($datos);
        
    }

    private function cargarValores(array $datos)
    {
        $venta_tipo_de_comprobante                = Conversion::optenerTipoComprobante($datos[1],$this->usa_equivalencia);             //Según tabla Comprobantes Ventas 
       
        
               
        if (!$venta_tipo_de_comprobante) {
            print_r("Existen conprobantes que no se pueden convertir".$datos[0].'->'.$datos[1].'->'.$datos[2].'->'.$datos[3]);
            die;                    
        }else{
        
            $this->venta['fecha_de_comprobante']=                                             $datos[0];
            //[fecha] => 2019-02-21 [0] => 2019-02-21 
            $this->venta['tipo_de_comprobante']=                                              $venta_tipo_de_comprobante;        
            //  [tipo] => F|B [1] => F|B 
            
            $this->venta['punto_de_venta']=                                                   Conversion::optenerPtoVta($venta_tipo_de_comprobante, $datos[2]);
            // [Nro_Puesto_Venta] => 9 [2] => 9 
            
            $this->venta['numero_de_comprobante']=                                            $datos[3];

            // solo si son 
            if ($venta_tipo_de_comprobante == '033' || 
                    $venta_tipo_de_comprobante =='331' || 
                    $venta_tipo_de_comprobante == '332'){
                $this->venta['numero_de_comprobante']=                                        $datos[24];          
                // [codigoOperacionElectronica] => "" [24]=> ""      no esta 
                // Numero de Comprobante: Para los comprobantes correspondientes a los códigos '033', '331' y '332' este
                // campo deberá completarse con el "Código de Operación Electrónica    -COE-"
            }

            $this->venta['numero_de_comprobante_hasta']=                                        $this->venta['numero_de_comprobante'];
            // [hasta] => 0 [4] => 0 

            $this->venta['codigo_de_documento_del_comprador']=                                  $datos[5];                
            //[codigoTipoDocumento] => 80 [5] => 80 
            
            $this->venta['numero_de_identificacion_del_comprador']=                             $datos[6];
            //[cuit] => [6] => 

            $this->venta['apellido_y_nombre_o_denominacion_del_comprador']=                     $datos[7];
            // [razonSocial] => [7] =>  

            $this->venta['importe_total_de_la_operacion']=                                      $datos[8];
            // [Total_Final] => 846.00 [8] => 846.00 

            $this->venta['importe_total_de_conceptos_que_no_integran_el_precio_neto_gravado']=  $datos[9];
            // [totalConc] => 0 [9] => 0 

            $this->venta['percepcion_a_no_categorizados']=                                      $datos[10];
            //  [percepNoCateg] => 0 [10] => 0 

            $this->venta['importe_de_operaciones_exentas']=                                     $datos[11];
            // [impOpExentas] => 0 [11] => 0

            $this->venta['importe_de_percepciones_o_pagos_a_cuenta_de_impuestos_nacionales']=   $datos[12];
              // [impPercImpNac] => 0 [12] => 0 

            $this->venta['importe_de_percepciones_de_ingresos_brutos']=                         $datos[13];
            // [Ing_Brutos] => 0.00 [13] => 0.00 

            $this->venta['importe_de_percepciones_impuestos_municipales']=                      $datos[14];
            //[impMunic] => 0 [14] => 0 

            $this->venta['importe_impuestos_internos']=                                         $datos[15];
            // [impInternos] => 0 [15] => 0

            $this->venta['codigo_de_moneda']=                                                   $datos[16];                                                                                //  Segun tabla  debe venir
              //[Moneda] => PES [16] => PES

            $this->venta['tipo_de_cambio']=                                                     $datos[17];
             // [tipoCambio] => 1 [17] => 1

            $this->venta['cantidad_de_alicuotas_de_iva']=                                       Conversion::esBoC($venta_tipo_de_comprobante)? 1:  $datos[18];
            // [cantAlicIva] => 1[18] => 1      
        
            $this->venta['codigo_de_operacion']=                                                $datos[19]== 'A'? ' ':$datos[19];                                                       // segun tabla debe venir  si es A se blanquea
            
            $this->venta['otros_tributos']=                                                     $datos[20];
            //[otrosTributos] => 0 [20] => 0 

            $this->venta['fecha_de_vencimiento_de_pago']=                                       $datos[21];  
            //[fechaVtoPago] => [21] => 


            //[periodo] => 02-2019[22] => 02-2019
            
            //[alicuotaIVA] =>    [23] 
            
             $this->venta['alicuotaIVA']=                                       Conversion::obtenerCodigoALicuotaIva($datos[23]); 
            //[codigoOperacionElectronica] => "" [24]=>
             $this->venta['codigoOperacionElectronica']=                        $datos[24];
            // [importeNetoGravado] => [25]
             $this->venta['importeNetoGravado']=                                $datos[25];
             // [impuestoLiquidado] => [26]
             $this->venta['impuestoLiquidado']=                                 $datos[26];
        }
        
               
      //  print_r($this->venta);
      //  echo ('<br>');
    }
    
     
    //$venta_codigo_de_operacion   
    
    
    public function  getStringRenglon(){
        $stringVenta= "";
        
        
        $stringVenta .= Conversion::convertirFecha($this->venta['fecha_de_comprobante']);                                                                               // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(3 , $this->venta['tipo_de_comprobante']);                                                                    // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(5 , $this->venta['punto_de_venta']);                                                                         // numerico
        //14
        $stringVenta .= Conversion::agregarCeros_izquierda(20 , $this->venta['numero_de_comprobante']);                                                                 // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(20 , $this->venta['numero_de_comprobante_hasta']);                                                           // numerico     
        $stringVenta .= Conversion::agregarCeros_izquierda(2 , $this->venta['codigo_de_documento_del_comprador']);
        $stringVenta .= Conversion::agregarCeros_izquierda(20 , $this->venta['numero_de_identificacion_del_comprador']);                //Alfanumerico
        $stringVenta .= Conversion::agregarBlancos_derecha(30 , $this->venta['apellido_y_nombre_o_denominacion_del_comprador']);        //Alfanumerico
        //90
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_total_de_la_operacion']));                                      // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_total_de_conceptos_que_no_integran_el_precio_neto_gravado']));  // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['percepcion_a_no_categorizados']));                                      // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_de_operaciones_exentas']));                                     // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_de_percepciones_o_pagos_a_cuenta_de_impuestos_nacionales']));   // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_de_percepciones_de_ingresos_brutos']));                         // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_de_percepciones_impuestos_municipales']));                      // numerico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['importe_impuestos_internos']));                                         // numerico
        // 15 *8 = 120
        $stringVenta .= Conversion::agregarCeros_izquierda(3 , $this->venta['codigo_de_moneda']);                                       //Alfanumerico
        $parteEnteraTipoCambio = intval($this->venta['tipo_de_cambio']); 
        $parteDecimalTipoCambio = $this->venta['tipo_de_cambio']-intval($this->venta['tipo_de_cambio']); 
        $stringVenta .= str_pad($parteEnteraTipoCambio,4,0,STR_PAD_LEFT).str_pad($parteDecimalTipoCambio,6,0);
        $stringVenta .= str_pad($this->venta['cantidad_de_alicuotas_de_iva'],1,0,STR_PAD_LEFT);                                                           // numerico
        $stringVenta .= Conversion::agregarBlancos_derecha(1 , $this->venta['codigo_de_operacion']);                                    //Alfabetico
        $stringVenta .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->venta['otros_tributos']));                                                     // numerico
        if($this->venta['fecha_de_vencimiento_de_pago'] == null){
            $this->venta['fecha_de_vencimiento_de_pago'] = $this->venta['fecha_de_comprobante'];
        }
        $stringVenta .= str_pad(Conversion::convertirFecha($this->venta['fecha_de_vencimiento_de_pago']),8,' ',STR_PAD_LEFT);                                                                   // numerico
        //38        
        $longitud = strlen($stringVenta);
        
        
        $resultado = str_replace(".", " ", $stringVenta);

        if ($longitud == 266)
            return $resultado;
        else return false;


    }
  
}
