<?php
/**
* Description_of_Compras
* 
* @author_Ale
*/

/*
require_once '.\Conversion.php';
require_once '.\DBase.php';
require_once '.\Equivalencias.php';
*/

class Compras extends Conversion{
    private $compra;
     private $usa_equivalencia;
    public function __construct($datos, $usa_equivale)
    {
        $this->usa_equivalencia = $usa_equivale;
        $this->compra = array();
        $this->cargarValores($datos);
    }


    public function cargarValores(array $datos)
    {                  
        $this->compra['fecha_de_comprobante']= $datos[0];                          //   1	fecha	varchar(8)	utf8mb4_unicode_ci		Sí	NULL	
        $this->compra['tipo_de_comprobante']= $datos[1];                           //2	tipo	varchar(3)	latin1_swedish_ci		No	Ninguna	
        $this->compra['punto_de_venta']= $datos[2];                                //3	Nro_Puesto_Vta	varchar(4)	latin1_swedish_ci		No	Ninguna
        $this->compra['numero_de_comprobante']= $datos[3];                         //4	Nro_Comprobante	varchar(8)	latin1_swedish_ci		No	Ninguna	
        

        $this->compra['despacho_de_importacion']= $datos[4];                       //5	despImp	char(0)	utf8mb4_general_ci    No	
        $this->compra['codigo_de_documento_del_vendedor']= $datos[5];              //6	tipoDoc	varchar(2)	utf8mb4_general_ci		No
        $this->compra['numero_de_identificacion_del_vendedor']= $datos[6];         // 7	cuit	varchar(11)	latin1_swedish_ci		Sí	NULL
        $this->compra['apellido_y_nombre_o_denominacion_del_vendedor']= $datos[7]; //8	razonSocial	varchar(60)	latin1_swedish_ci		Sí	NULL
        $this->compra['importe_total_de_la_operacion']= $datos[8];                 //9	TotalNeto	decimal(9,2)			No	0.00
        $this->compra['importe_total_de_conceptos_que_no_integran_el_precio_neto_gravado']= $datos[9]; //10	Nograv	decimal(9,2)			No	0.00
        $this->compra['importe_de_operaciones_exentas']= $datos[10];                                   //11	exentas	decimal(3,2)			No	0.00
        $this->compra['importe_de_percepciones_o_pagos_a_cuenta_del_iva']= $datos[11];                 //12	perIVA	decimal(9,2)			No	0.00
        $this->compra['importe_de_percepciones_o_pagos_a_cuenta_de_otros_impuestos_nacionales']= $datos[12]; //13	perNac	decimal(9,2)			No	0.00
        $this->compra['importe_de_percepciones_de_ingresos_brutos']= $datos[13];                      //14	PercepIB	decimal(9,2)			No	0.00
        $this->compra['importe_de_percepciones_de_impuestos_municipales']= $datos[14];                //15	percMun	decimal(3,2)			No	0.00
        $this->compra['importe_de_impuestos_internos']= $datos[15];                                   //16	impInte	decimal(3,2)			No	0.00
        $this->compra['codigo_de_moneda']= $datos[16];                                   //17	moneda	varchar(3)	utf8mb4_general_ci		No
        $this->compra['tipo_de_cambio']= $datos[17];                                     //18	tipoCambio	int(1)			No	0
        
      //  Este campo es un solo digito
        $this->compra['cantidad_de_alicuotas_de_iva']= $datos[18];                       //19	Cant_alicuotas_iva	int(3)			No	Ninguna	
        
        
        $this->compra['codigo_de_operacion']= $datos[19];                                //20	codOpera	char(0)	utf8mb4_general_ci		No
        $this->compra['credito_fiscal_computable']= $datos[20];                          //21	Iva	decimal(9,2)			No	0.00
        $this->compra['otros_tributos']= $datos[21];                                     //22	otros	decimal(3,2)			No	0.00
        $this->compra['cuit_emisor_corredor']= $datos[22];                               //23	cuitCorredor	char(0)	utf8mb4_general_ci		No
        $this->compra['denominacion_del_emisor_corredor']= $datos[23];                   //24	denomCorredor	char(0)	utf8mb4_general_ci		No
        
        $dato24 = $datos[24]=="" ? 0:$datos[24];
        $this->compra['iva_comision']= $dato24;                                          //25	ivaComision	char(0)	utf8mb4_general_ci		No
        
                                                                                         // 26	MesCarga	varchar(7)	latin1_swedish_ci		No	Ninguna
        
     	
	
				
	
        
    }

    public function  getStringRenglon(){
        $stringCompra= "";

        $stringCompra .= Conversion::convertirFecha($this->compra['fecha_de_comprobante']); 

                                                                                        // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda( 3 ,$this->compra['tipo_de_comprobante']);                                                                      // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda( 5 ,$this->compra['punto_de_venta']);                                                                           // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda( 20 ,$this->compra['numero_de_comprobante']);                                                                   // numerico
        
        $stringCompra .= Conversion::agregarBlancos_derecha(16 , $this->compra['despacho_de_importacion']);                                   //Alfanumerico
        
        $stringCompra .= Conversion::agregarCeros_izquierda(2, $this->compra['codigo_de_documento_del_vendedor']);                                                          // numerico
       
        $stringCompra .= Conversion::agregarCeros_izquierda(20, $this->compra['numero_de_identificacion_del_vendedor']);                      //Alfanumerico
        $stringCompra .= Conversion::agregarBlancos_derecha(30, $this->compra['apellido_y_nombre_o_denominacion_del_vendedor']);              //Alfanumerico
        
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_total_de_la_operacion']));                                          // numerico              
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_total_de_conceptos_que_no_integran_el_precio_neto_gravado']));      // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_operaciones_exentas']));                                         // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_percepciones_o_pagos_a_cuenta_del_iva']));                       // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_percepciones_o_pagos_a_cuenta_de_otros_impuestos_nacionales'])); // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_percepciones_de_ingresos_brutos']));                             // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_percepciones_de_impuestos_municipales']));                       // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['importe_de_impuestos_internos']));                                          // numerico
        
        $stringCompra .= Conversion::agregarCeros_izquierda(3, $this->compra['codigo_de_moneda']);                                          //Alfanumerico
        
        $parteEnteraTipoCambio = intval($this->compra['tipo_de_cambio']); 
        $parteDecimalTipoCambio = str_replace('0.','',$this->compra['tipo_de_cambio']-intval($this->compra['tipo_de_cambio'])); ; 
        $stringCompra .= str_pad($parteEnteraTipoCambio,4,0,STR_PAD_LEFT).str_pad($parteDecimalTipoCambio,6,0);                                                               // numerico

        $stringCompra .= Conversion::agregarCeros_izquierda(1, $this->compra['cantidad_de_alicuotas_de_iva']);                                                                // numerico
        $stringCompra .= Conversion::agregarBlancos_derecha(1, $this->compra['codigo_de_operacion']);                                      //Alfabetico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['credito_fiscal_computable']));                                              // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['otros_tributos']));                                                         // numerico
        $stringCompra .= Conversion::agregarCeros_izquierda(11, $this->compra['cuit_emisor_corredor']);                                                                       // numerico
        $stringCompra .= Conversion::agregarBlancos_derecha(30, $this->compra['denominacion_del_emisor_corredor']);                        //Alfanumerico
        $stringCompra .= Conversion::agregarCeros_izquierda(15, Conversion::sustituirPunto2Dec($this->compra['iva_comision']));                                                           // numerico
        
        $longitud = strlen($stringCompra);
        
        $resultado = str_replace(".", " ", $stringCompra);
        
       // echo $resultado;
        if ($longitud == 325) 
            return $resultado;
        else{
            Yii::log(' $stringCompra: >'.$longitud.'<'.$stringCompra,'error');
            return false;
        } 
        
    }

}
