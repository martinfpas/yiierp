<?php
$name='SerifDotDigital-7';
$type='TTF';
$desc=array (
  'CapHeight' => 850.0,
  'XHeight' => 425.0,
  'FontBBox' => '[0 -200 950 1150]',
  'Flags' => 4,
  'Ascent' => 1150.0,
  'Descent' => -200.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 600.0,
);
$unitsPerEm=1000;
$up=-175;
$ut=50;
$strp=250;
$strs=50;
$ttffile='/var/www/erca/protected/vendors/mpdf60/ttfonts/DejaVuSerif-Italic.ttf';
$TTCfontID='0';
$originalsize=81716;
$sip=false;
$smp=false;
$BMPselected=true;
$fontkey='dejavuserifI';
$panose=' 0 0 2 0 0 0 0 0 0 0 0 0';
$haskerninfo=true;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 750, -250, 0
// usWinAscent/usWinDescent = 1150, -200
// hhea Ascent/Descent/LineGap = 750, -250, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>