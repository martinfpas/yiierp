<?php
$name='ModernDotDigital-7';
$type='TTF';
$desc=array (
  'CapHeight' => 750.0,
  'XHeight' => 275.0,
  'FontBBox' => '[0 -300 750 1000]',
  'Flags' => 4,
  'Ascent' => 1000.0,
  'Descent' => -300.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 600.0,
);
$unitsPerEm=1000;
$up=-175;
$ut=50;
$strp=250;
$strs=50;
$ttffile='/var/www/erca/protected/vendors/mpdf60/ttfonts/modern_dot_digital-7.ttf';
$TTCfontID='0';
$originalsize=67848;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='moderndot';
$panose=' 0 0 2 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 750, -250, 0
// usWinAscent/usWinDescent = 1000, -300
// hhea Ascent/Descent/LineGap = 750, -250, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>