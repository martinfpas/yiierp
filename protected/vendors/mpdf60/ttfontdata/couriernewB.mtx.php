<?php
$name='CourierNewPS-BoldMT';
$type='TTF';
$desc=array (
  'CapHeight' => 592.0,
  'XHeight' => 443.0,
  'FontBBox' => '[-46 -359 702 1016]',
  'Flags' => 262149,
  'Ascent' => 833.0,
  'Descent' => -300.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 165.0,
  'MissingWidth' => 600.0,
);
$unitsPerEm=2048;
$up=-233;
$ut=100;
$strp=259;
$strs=50;
$ttffile='/var/www/erca/protected/vendors/mpdf60/ttfonts/Courier New Bold.ttf';
$TTCfontID='0';
$originalsize=174596;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='couriernewB';
$panose=' 5 5 2 7 6 9 2 2 5 2 4 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 633, -209, 0
// usWinAscent/usWinDescent = 833, -300
// hhea Ascent/Descent/LineGap = 833, -300, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>