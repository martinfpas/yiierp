<?php
$name='CourierNewPSMT';
$type='TTF';
$desc=array (
  'CapHeight' => 571.0,
  'XHeight' => 423.0,
  'FontBBox' => '[-32 -274 625 783]',
  'Flags' => 5,
  'Ascent' => 783.0,
  'Descent' => -274.0,
  'Leading' => 0.0,
  'ItalicAngle' => 0.0,
  'StemV' => 87.0,
  'MissingWidth' => 600.0,
);
$unitsPerEm=2048;
$up=-233;
$ut=41;
$strp=259;
$strs=50;
$ttffile='/var/www/erca/protected/vendors/mpdf60/ttfonts/cour.ttf';
$TTCfontID='0';
$originalsize=72356;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='couriernew';
$panose=' 5 5 2 7 3 9 2 2 5 2 4 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 613, -188, 0
// usWinAscent/usWinDescent = 833, -300
// hhea Ascent/Descent/LineGap = 833, -300, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>