<?php
$name='CourierNewPS-ItalicMT';
$type='TTF';
$desc=array (
  'CapHeight' => 572.0,
  'XHeight' => 423.0,
  'FontBBox' => '[-67 -274 800 1000]',
  'Flags' => 69,
  'Ascent' => 833.0,
  'Descent' => -274.0,
  'Leading' => 0.0,
  'ItalicAngle' => -12.0,
  'StemV' => 87.0,
  'MissingWidth' => 600.0,
);
$unitsPerEm=2048;
$up=-233;
$ut=41;
$strp=259;
$strs=50;
$ttffile='/var/www/erca/protected/vendors/mpdf60/ttfonts/Courier New Italic font.ttf';
$TTCfontID='0';
$originalsize=238088;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='couriernewI';
$panose=' 5 5 2 7 4 9 2 2 5 9 4 4';
$haskerninfo=false;
$haskernGPOS=false;
$hassmallcapsGSUB=false;
$fontmetrics='win';
// TypoAscender/TypoDescender/TypoLineGap = 613, -188, 0
// usWinAscent/usWinDescent = 833, -300
// hhea Ascent/Descent/LineGap = 833, -300, 0
$useOTL=0x0000;
$rtlPUAstr='';
?>