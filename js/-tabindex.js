/**
 * v 1.1
 */
$(document).ready(function(){
	
	$('form [tabindex]').bind('keypress', function (eInner) {
		try {
		if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            var form = $(this).closest('form');
            var idform = $(form).attr('id');
            console.log(idform);
            tabindex++; //increment tabindex
            // SI NO ES EL ULTIMO ELEMENTO CON TABINDEX VA AL SIGUIENTE ELEMENTO
            if($('#'+idform+' [tabindex='+tabindex+']').length > 0){
            	$('#'+idform+' [tabindex='+tabindex+']').focus();	
            }else{ 
            	//CASO CONTRARIO HACE FOCO EN EL BOTON SUBMIT QUE TIENE EL FORM
            	
            	$(form).find('[type=submit]').focus();
            }
            
            return false; // to cancel out Onenter page postback in asp.net
        }
		}catch (e) {
			// TODO: handle exception
			console.warn(e);
		}
	});

    $('.select2-container').on('keyup', function(e) {
        if(e.keyCode === 13){
            console.log($(this).data('select2').opts.tabindex);

        }
    });
});
