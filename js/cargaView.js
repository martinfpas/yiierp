function updateSelectedRow(){
    
    $.ajax({
        url:urlViewPartial+idRow,
        success:function(html){
            var dataT = $('tbody tr',$(html));
            console.log($(dataT).html());
            selectedRow.html($(dataT).html());
        },
        error:function(x,y,z){

        },
    });
}

$(document).on('hidden', '#modalNotaEntrega', function() {
    //console.log('modalNotaEntrega L '+$('#nota-pedido-grid2').length);
    if($('#nota-pedido-grid2').length > 0){
        try {
            $('#modalNotaEntrega .modal-body').html('');
            //FALLA SI NO VIENE EL FORMULARIO
            //LA FALLA SE DA SI NO SE EXCLUYEN LOS JQUERY DE LOS HTML QUE VIENEN POR AJAX
            updateSelectedRow();
            //$.fn.yiiGridView.update('nota-pedido-grid2');
        } catch (e){
            alert('La planilla no se actualizó. Presione F5');
            console.log(e);
        }
    }
});

$(document).on('hidden', '#modalFactura', function() {
    //console.log('modalFactura'+$('#nota-pedido-grid2').length);
    if($('#nota-pedido-grid2').length > 0) {
        try{
            $('#modalFactura .modal-body').html('');
            //FALLA SI NO VIENE EL FORMULARIO
            updateSelectedRow();
            //$.fn.yiiGridView.update('nota-pedido-grid2');
        } catch (e){
            console.log(e);
            alert('La planilla no se actualizó. Presione F5');
        }
    }
});
/****** CONTROLA EL MOVIMIENTO CON LA FLECHA *******/
function moveDown() {
    var rows = $('#prod-carga-grid table tr');
    var currentRow = $("tr.selected").get(0);

    if (rows.length > 2) {
        if (currentRow === undefined) {
            rows.eq(1).addClass('selected');
        } else if ($(currentRow).next('tr').get(0) === undefined) {
            //do nothing
        } else {
            $(currentRow).next('tr').addClass('selected');
            $(currentRow).removeClass('selected');
        }
    }
}

function moveUp() {
    var rows = $('#prod-carga-grid table tr');
    var currentRow = $("tr.selected").get(0);

    if (rows.length > 2) {
        if (currentRow === undefined) {
            rows.eq(1).addClass('selected');
        } else if ($(currentRow).prev('tr').get(0) === undefined) {
            //do nothing
        } else {
            $(currentRow).prev('tr').addClass('selected');
            $(currentRow).removeClass('selected');
        }
    }
}

$(document).ready(function(){
    shortcut.add('Down', function() {
        moveDown();
    });

    shortcut.add('Up', function() {
        moveUp();
    });
    shortcut.add('F5', function() {
        console.log('F5');
        $.fn.yiiGridView.update('nota-pedido-grid2');
    });

});
/****** fin CONTROLA EL MOVIMIENTO CON LA FLECHA *******/