var cargaDocumentos_Recibida_options;
var cargaDocumentos_devolucion_options;
var cargaDocumentos_efectivo_options;
var cargaDocumentos_comision_options;


/////////// *  begin cargaDocumentos_Recibida_options

function CargaDocumentos_Recibida_init(ev, editable){
    if(cargaDocumentos_Recibida_options === undefined){
        cargaDocumentos_Recibida_options = editable.options;
    }
}

function CargaDocumentos_Recibida_editable(pk){
    $('[rel="CargaDocumentos_Recibida"]').filter('[data-pk="'+pk+'"]').editable(cargaDocumentos_Recibida_options);
}

///////////////////////////////////////// * end cargaDocumentos_Recibida_options

/////////// *  begin cargaDocumentos_devolucion_options
function CargaDocumentos_devolucion_display(value, sourceData){
    if(!isNaN(value)){
        $(this).html("$ "+parseFloat(value).toFixed(2));
    }else{
        $(this).html("$ 0.00");
    }
}

function CargaDocumentos_devolucion_init(ev, editable){
    if(cargaDocumentos_devolucion_options === undefined){
        cargaDocumentos_devolucion_options = editable.options;
    }
    
}

function CargaDocumentos_devolucion_shown(ev,editable){
    setTimeout(function() {
        editable.input.$input.select();
        rowsCargaDocs = [$(ev.currentTarget).parent().parent()];
      },0);
}

function CargaDocumentos_devolucion_success(response, newValue) {
    try{
        //$.fn.yiiGridView.update("carga-documentos-grid");
        updateRow(rowsCargaDocs[0]);
    }catch(e){
        console.log(e);
    };
}

function CargaDocumentos_devolucion_editable(pk){
    //let pk = $(obj).attr('data-pk');
    $('[rel="CargaDocumentos_devolucion"]').filter('[data-pk="'+pk+'"]').on('shown',function(ev,editable) {CargaDocumentos_devolucion_shown(ev,editable)});
    $('[rel="CargaDocumentos_devolucion"]').filter('[data-pk="'+pk+'"]').editable(cargaDocumentos_devolucion_options);
}

///////////////////////////////////////// * end cargaDocumentos_devolucion_options

/////////// *  begin cargaDocumentos_efectivo_options
function CargaDocumentos_efectivo_display(value, sourceData){
    if(!isNaN(value)){
        $(this).html("$ "+parseFloat(value).toFixed(2));
    }else{
        $(this).html("$ 0.00");
    }
}

function CargaDocumentos_efectivo_init(ev, editable){
    if(cargaDocumentos_efectivo_options === undefined){
        cargaDocumentos_efectivo_options = editable.options;
    }
    
}

function CargaDocumentos_efectivo_shown(ev,editable){
    setTimeout(function() {
        editable.input.$input.select();
        rowsCargaDocs = [$(ev.currentTarget).parent().parent()];
      },0);
}

function CargaDocumentos_efectivo_success(response, newValue) {
    try{
        //$.fn.yiiGridView.update("carga-documentos-grid");
        updateRow(rowsCargaDocs[0]);
    }catch(e){
        console.log(e);
    };
}

function CargaDocumentos_efectivo_editable(pk){
    //let pk = $(obj).attr('data-pk');
    $('[rel="CargaDocumentos_efectivo"]').filter('[data-pk="'+pk+'"]').on('shown',function(ev,editable) {CargaDocumentos_efectivo_shown(ev,editable)});
    $('[rel="CargaDocumentos_efectivo"]').filter('[data-pk="'+pk+'"]').editable(cargaDocumentos_efectivo_options);
}

///////////////////////////////////////// * end cargaDocumentos_efectivo_options

/////////// *  begin cargaDocumentos_comision_options

function CargaDocumentos_comision_init(ev, editable){
    if(cargaDocumentos_comision_options === undefined){
        cargaDocumentos_comision_options = editable.options;
    }
}

function CargaDocumentos_comision_editable(pk){
    $('[rel="CargaDocumentos_comision"]').filter('[data-pk="'+pk+'"]').editable(cargaDocumentos_comision_options);
}

///////////////////////////////////////// * end cargaDocumentos_comision_options

/////////////////////////////////////////

function updateRow(rowCargaDoc){
    let pk = $(rowCargaDoc).find('[name=\"carga-documentos-grid_c1[]\"]').val();

    if(pk === undefined){
        pk = $(rowCargaDoc).find('.verPago').attr('href');
    }

    let data = 'id='+pk+'&idEntregaDoc='+entregaDocId;
    $.ajax({
        url:urlRendirPartial,
        data: data,
        success:function(html){
            var dataT = $('tbody tr',$(html));
            let dataEfTotal = $('#totalEfectivo',$(html));
            let dataChTotal = $('#totalCheques',$(html));
            let totalCobrado = $('#totalCobrado',$(html));
            console.log($(dataEfTotal).html());
            $('#totalEfectivo').html($(dataEfTotal).html());
            $('#totalCheques').html($(dataChTotal).html());
            $('#totalCobrado').html($(totalCobrado).html());
            $(rowCargaDoc).html(dataT.html());
            //TODO: UPDATE TOTALES
            if($('[rel="CargaDocumentos_efectivo"]').filter('[data-pk="'+pk+'"]').length){
                CargaDocumentos_efectivo_editable(pk);
            }
            if($('[rel="CargaDocumentos_devolucion"]').filter('[data-pk="'+pk+'"]').length){
                CargaDocumentos_devolucion_editable(pk);
            }
            if($('[rel="CargaDocumentos_Recibida"]').filter('[data-pk="'+pk+'"]').length){
                CargaDocumentos_Recibida_editable(pk);
            }
            if($('[rel="CargaDocumentos_comision"]').filter('[data-pk="'+pk+'"]').length){
                CargaDocumentos_comision_editable(pk);
            }

            
        },
        error:function(x,y,z){

        },
    });


    
}