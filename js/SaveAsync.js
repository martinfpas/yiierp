/**
 * v 1.7
 * Codigo para los formularios autogenerados personalizados con Gii
 * se agrega e.stopImmediatePropagation();
 * se agrega e.stopPropagation();
 * RECARGA LA PAGINA SI SE VENCIO LA SESION
 * hace foco en el tabindex=1
 * se agrega e.stopImmediatePropagation() en saveAsyncMas;
 * esconde los alert cuando resetea
 * agrega un control de existencia en idNuevo
 */

	function bindSaveAsync(){
		$('.SaveAsyncMas').click(function(e){
			e.stopImmediatePropagation();
			var idNuevo = $(this).attr('idNuevo');
			$('#'+idNuevo).toggle('slow');
			if($(this).val() === 'Agregar'){
		    	$(this).val('Cerrar');
			}else{
				$(this).val('Agregar');
			}	
		});
		
		$('.SaveAsyncForm').submit(function(e){
			e.stopImmediatePropagation();
			e.stopPropagation();
			e.preventDefault();
			var datos = $(this).serialize();
			//console.log(datos);
			var respuesta ;
			var url = $(this).attr('action');
			var divError = $(this).find('.SaveAsyncError');
			var divOk = $(this).find('.SaveAsyncOk');
			var idGrid =  $(this).attr('idGrilla');
			var idMas = $(this).attr('idMas');
			var idNuevo = $(this).attr('idNuevo');
			
			var form = $(this);
			
			$(divError).hide('slow');
			$(divOk).hide('slow');
			
			
			$.ajax({
				url: url,
	            type: 'Post',
	            data: datos,
	            success: function (html) {
	            	//RECARGA LA PAGINA SI SE VENCIO LA SESION
	            	if(html == 'YII_LOGIN_REQUIRED'){
                        window.location.reload(true);
	            	}
	               	$(divOk).show('slow');
	               	//TODO: DEBERIA CHEQUEAR SI EXISTE 	idGrid
	               	if($('#'+idGrid).length){
	               		$.fn.yiiGridView.update(idGrid);
	               	}else{
	               		console.warn('El parametro grilla no se encuentra');
	               	}
					if($('#'+idNuevo).length > 0){
                        $('#'+idNuevo).hide('slow');
                    }
					console.log($('#'+idNuevo).length);

					if($('#'+idMas).length){
						$('#'+idMas).val('Agregar');
					}
	               	$(form)[0].reset();
                    $(form).find('[tabindex="1"]').focus()
                    $('#'+$(form).attr('id')+' .alert.alert-block.alert-success').hide('slow');
                    $('#'+$(form).attr('id')+' .alert.alert-block.alert-error').hide('slow');


				},
				error: function (x,y,z){
					// TODO: MANEJAR LOS ERRORES
					respuesta =  $.parseHTML(x.responseText);
					errorDiv = $(respuesta).find('.errorSpan');
					console.log($(errorDiv).parent().next());
					var htmlError = $(errorDiv).parent().next().html();
					$(divError).html(htmlError);
					$(divOk).hide('slow');
					$(divError).show('slow');
				}
			});
			return false;
		});		
	}

$(document).ready(function(){
	bindSaveAsync();
});

