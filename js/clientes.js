function clSelected(param,url_Cliente_ViewFicha){
    $('#errorCLiente').hide('slow');
    $('#okCliente').hide('slow');

    console.log('clSelected '+param);
    var datos = 'id='+param;
    $.ajax({
        url:"'"+url_Cliente_ViewFicha+"'",
        type: 'Get',
        data: datos,
        success: function (html) {
        $('#fichaCliente').html(html);
        getFactura($('#idCliente').val());
    },
    error: function (x,y,z){
        respuesta =  $.parseHTML(x.responseText);
        errorDiv = $(respuesta).find('.errorSpan');
        console.log($(errorDiv).parent().next());
        var htmlError = $(errorDiv).parent().next().html();
        $('#errorCLiente').html('Cliente No encontrado');
        $('#errorCLiente').show('slow');
        $('#NotaPedido_idCliente').focus();
    }
});
}